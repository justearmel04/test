<div class="page-header navbar navbar-fixed-top ">
   <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo" style="background-color:white">
            <a href="index.php">
            <img alt="" src="../assets/img/logo/logo1.png">
            </a>

        </div>
        <!-- logo end -->
<ul class="nav navbar-nav navbar-left in">
  <li><img alt="" src="<?php echo $_SESSION['user']['LogoEtab'] ?>" style="width:27%;margin-top:4px"></li>
</ul>
         <!-- Start Apps Dropdown -->

        <!-- start mobile menu -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
       <!-- end mobile menu -->
        <!-- start header menu -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <!-- end message dropdown -->
                <li class="dropdown language-switch">
            <!--a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                src="../assets2/img/flags/gb.png" class="position-left" alt=""> <?php echo L::English  ?> <span
                class="fa fa-angle-down"></span>
            </a-->
            <?php

              if($_SESSION['user']['lang']=="fr")
              {
                ?>
                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                    src="../assets2/img/flags/french_flag.jpg" style="width:17px;"  class="position-left" alt=""> <?php echo L::Francais  ?> <span
                    class="fa fa-angle-down"></span>
                </a>
                <?php
              }else if($_SESSION['user']['lang']=="en")
              {
                ?>
                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                    src="../assets2/img/flags/gb.png" style="width:17px;"  class="position-left" alt=""> <?php echo L::English  ?> <span
                    class="fa fa-angle-down"></span>
                </a>

                <?php
              }

             ?>

            <ul class="dropdown-menu">
              <li onclick="addFrench()">
                <a class="french"><img src="../assets2/img/flags/french_flag.jpg" alt="" style="width:17px;" > <?php echo L::Francais  ?></a>
              </li>

              <li onclick="addEnglish()">
                <a class="english"><img src="../assets2/img/flags/gb.png" alt="" > <?php echo L::English  ?></a>
              </li>


            </ul>
          </li>
    <!-- start manage user dropdown -->
    <li class="dropdown dropdown-user">

                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                          <img alt="" class="img-circle " src="<?php echo $lienphoto?>" />

                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="profile.php">
                                <i class="fa fa-user"></i><?php echo  L::ProfileLib ?>  </a>
                        </li>


                        <li class="divider"> </li>

                        <li>
                            <a href="../">
                                <i class="fa fa-sign-out"></i> <?php echo L::Deconnexion  ?> </a>
                        </li>
                    </ul>
                </li>
                <!-- end manage user dropdown -->

            </ul>
        </div>
    </div>
    <div class="navbar-custom">
<div class="hor-menu hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
                <li class="mega-menu-dropdown ">
                    <a href="index.php"> <i class="material-icons">dashboard</i>  <?php echo L::dashb ?>  </a>


                </li>
                <li class="classic-menu-dropdown mega-menu-dropdown">
                        <a href="#" class=" megamenu-dropdown" data-close-others="true"> <i class="material-icons">dashboard</i>  <?php echo L::studMenu ?>
                        <i class="fa fa-angle-down"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="dropdown-menu pull-left">

                      <li>
                          <div class="mega-menu-content">
                              <div class="row">
                                  <div class="col-md-12">
                                      <ul class="mega-menu-submenu">

                                <li class="nav-item  ">
                                    <a href="listeall.php" class="nav-link ">
                                        <span class="title"><?php echo L::FichestudMenu ?></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="classes.php" class="nav-link ">
                                        <span class="title"><?php echo L::ClassesMenu ?></span>
                                    </a>
                                </li>


                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </li>
                    </ul>
                </li>
                <li class="mega-menu-dropdown mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">dashboard</i><?php echo L::EnsigneMenu ?>
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                              <li>
                                  <a href="sessions.php" class="nav-link "> <span class="title"><?php echo L::ScolaryyearMenu ?></span></a>
                              </li>
                             <li>
                                  <a href="programmes.php" class="nav-link "> <span class="title"><?php echo L::SyllabrMenu ?></span></a>
                              </li>
                              <li>
                                   <a href="routines.php" class="nav-link "> <span class="title"><?php echo L::RoutinesMenu ?></span></a>
                               </li>


                                <li>
                                     <a href="listcourses.php" class="nav-link "> <span class="title"><?php echo L::CourseMenu  ?></span></a>
                                 </li>
                                 <li>
                                     <a href="fiches.php" class="nav-link "><span class="title"><?php echo L::CahierMenu  ?></span></a>
                                 </li>

                              </ul>
                        </li>


                <li class="mega-menu-dropdown mega-menu-dropdown">
                    <a href="#" class="dropdown-toggle"> <i class="material-icons">dashboard</i> <?php echo L::AbsMenu  ?>
                        <i class="fa fa-angle-down"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px;">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="mega-menu-submenu">
                                          <li>
                                              <a href="dailyattendance.php" class="nav-link "> <span class="title"><?php echo L::AttendanceManaged  ?></span></a>
                                          </li>
                                            <li>
                                                <a href="recapattendanceclasse.php" class="nav-link "> <span class="title"><?php echo L::Reacpsdesabsences ?></span></a>
                                            </li>
                                           <li>
                                                <a href="attendancegraph.php" class="nav-link "> <span class="title"><?php echo L::RecapCourbeMenu ?></span></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <!--li class="mega-menu-dropdown ">
                    <a href="#" class="dropdown-toggle"> <i class="material-icons">dashboard</i>Evaluations et notes
                        <i class="fa fa-angle-down"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 200px;">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="mega-menu-submenu">
                                           <li class="nav-item  ">
                                      <a href="#" class="nav-link "> <span class="title"><?php echo L::Evaluations ?></span>
                                      </a>
                                  </li>

                                  <li class="nav-item  ">
                                      <a href="#" class="nav-link "> <span class="title">Notes</span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="#" class="nav-link "> <span class="title"><?php echo L::QuizLib ?></span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="#" class="nav-link "> <span class="title"><?php echo L::DevoirsMenu ?></span>
                                      </a>
                                  </li>
                                  <li class="nav-item  ">
                                      <a href="#" class="nav-link "> <span class="title"><?php echo L::CurvesNotes ?></span>
                                      </a>
                                  </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li-->
                <li class="mega-menu-dropdown mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">dashboard</i> <?php echo L::EvalnotesMenu ?>
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu pull-left">

                              <li class="nav-item  ">
                         <a href="controles.php" class="nav-link "> <span class="title"><?php echo L::Evaluations ?></span>
                         </a>
                     </li>


                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                         Notes</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="addnotes.php" class="nav-link ">
		                                        <span class="title"><?php echo L::Consigner ?></span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="notes.php" class="nav-link ">
		                                        <span class="title"><?php echo L::Reacap ?></span>
		                                    </a>
		                                </li>


                                    </ul>
                                </li>
                                <li class="nav-item  ">
                                    <a href="listquizs.php" class="nav-link "> <span class="title"><?php echo L::QuizLib ?></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="listdevoirs.php" class="nav-link "> <span class="title"><?php echo L::DevoirsMenu ?></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="courbesnotes.php" class="nav-link "> <span class="title"><?php echo L::CurvesNotes ?></span>
                                    </a>
                                </li>
                                <!-- <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                         Courbes des notes</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
                                        <a href="courbesnotesclasses.php" class="nav-link ">
                                            <span class="title">Courbe par classe</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="courbesnotes.php" class="nav-link ">
                                            <span class="title">Courbe par élève</span>
                                        </a>
                                    </li>


                                    </ul>
                                </li> -->
                                <!-- <li class="nav-item  ">
                                    <a href="#" class="nav-link "> <span class="title"><?php echo L::CurvesNotes ?></span>
                                    </a>
                                </li> -->

                              </ul>
                        </li>
                <li class="mega-menu-dropdown">
                    <a href="allparascolaires.php" class="dropdown-toggle"> <i class="material-icons">dashboard</i> <?php echo L::parascoMenu ?>


                    </a>

                </li>




            </ul>
        </div>
</div>
</div>
