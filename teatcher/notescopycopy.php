<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);
//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);
$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];

  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}




 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <!--link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" /-->

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste Notes - Classe :</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.html">Gestion des Notes</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Liste Notes </li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addattendailyok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addattendailyok']);
                }

                 ?>

<br/><br/>
          <div class="row">



            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header>Gestion des Notes : Classe</header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormNotes" action="notes.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Classe</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)" >
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option <?php if(isset($_POST['search'])&&($_POST['classeEtab']==$value->id_classe)){echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::MatiereMenusingle ?></label>
                           <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                           <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
                               <option value=""><?php echo L::SelectSubjects ?></option>


                           </select>
                       </div>


                   </div>
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Type Note</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="notetype" name="notetype" style="width:100%;" onchange="searchDesignation()">
                                   <option value="">Selectionner un type de Note</option>

                                     <option <?php if(isset($_POST['search'])&&($_POST['notetype']==1)){echo "selected";} ?> value="1">CONTROLE</option>
                                     <option <?php if(isset($_POST['search'])&&($_POST['notetype']==2)){echo "selected";} ?> value="2">EXAMEN</option>


                               </select>
                           </div>




                       </div>

                   <div class="col-md-6 col-sm-6">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label>Désignation</label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <select class="form-control input-height" id="libctrl" name="libctrl" style="width:100%;">
                           <option value="">Selectionner une désignation</option>

                       </select>
                   </div>


               </div>

                       <div class="col-md-3 col-sm-3">
                       <!-- text input -->
                       <!--div class="form-group">
                           <label style="margin-top:3px;">Date</label>
                           <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                           <input type="hidden" name="search" id="search" />
                       </div-->
                       <input type="hidden" name="search" id="search" />
                       <input type="hidden" name="codeEtab" id="codeEtab" value="" />
                       <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                       <button type="submit" class="btn btn-success" onclick="affichage()" style="width:200px;height:35px;margin-top:35px;text-align:center;">Afficher les Notes</button>


                   </div>


                         </div>


                     </form>
                   </div>
               </div>
                        </div>

                </div>

                <div class="row" style="" id="affichage">
                  <?php
                      if(isset($_POST['search']))
                      {
                          if(isset($_POST['notetype'])&&isset($_POST['classeEtab'])&&isset($_POST['libctrl']))
                          {
                              //nous devons recupérer la liste des elèves de cette classe ainsi que leur note

                              $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

                              if(strlen($_POST['libctrl'])>0)
                              {
                                if($_POST['notetype']==1)
                                {
                                  //cas d'un controle
                                $data=explode("-",$_POST['libctrl']);
                                $controleid=$data[0];
                                $matiereid=$data[1];
                                $teatcherid=$data[2];
                                  $studentsX=$student->getAllstudentNotesofthisclasses($_POST['classeEtab'],$matiereid,$controleid,$teatcherid,$_POST['codeEtab']);
                                }else if($_POST['notetype']==2){
                                  //cas d'un examen
                                  $data=explode("-",$_POST['libctrl']);
                                  $examid=$data[0];
                                  $matiereid=$data[1];
                                  $teatcherid=$data[2];
                                  //$examid=$_POST['libctrl'];
                                $studentsX=$student->getAllstudentNotesExamofthisclasses($_POST['classeEtab'],$examid,$_POST['codeEtab']);
                                }
                              }else {
                                // code...
                              }




                              $classeInfos=$classe->getInfosofclassesbyId($_POST['classeEtab']);

                              ///var_dump($students);

                          }
                          ?>

                          <div class="offset-md-4 col-md-4"  id="affichage1">
                            <div class="card" style="">
                            <div class="card-body">
                              <h5 class="card-title"></h5>
                              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Note de Classe</h4>
                              <p class="card-text" style="text-align:center;font-weight: bold;"><?php echo $classeInfos; ?></p>
                              <p class="card-text" style="text-align:center;"></p>

                            </div>
                          </div>
                          </div>
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                                  <div class="card card-topline-green">
                                                      <div class="card-head">
                                                          <header>Liste des Notes - classe : <?php echo $classeInfos; ?></header>
                                                          <div class="tools">
                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                          </div>
                                                      </div>
                                                      <div class="card-body ">

                                                        <form method="post" action="#" id="FormAttendance">
                                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example5"  id="affichage3">
                                                      <thead>
                                                          <tr>
                                                            <!--th style="width:15%"> Matricule </th-->
                                                            <th style="width:50%"> Nom & Prénoms </th>
                                                            <th style="width:35%;text-align:center"> Note </th>
                                                            <th style="text-align:center"> Observation </th>


                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                        <?php
                                                        $matricules="";
                                                        $j=0;
                                                        $i=1;

                                                        foreach ($students as $value):

                                                          $matricules=$matricules."*".$value->matricule_eleve;
                                                          $datanotesStudent="";
                                                          $nbdata="";
                                                          //determinantion de la note de l'eleve pour ce controle ou examens

                                                          if($_POST['notetype']==1)
                                                          {
                                                            //cas d'un controle $libellesessionencours
                                                          $data=explode("-",$_POST['libctrl']);
                                                          $controleid=$data[0];
                                                          $matiereid=$data[1];
                                                          $teatcherid=$data[2];
                                                          $nbdata=$student->getNotesAndObservControleNb($value->idcompte_eleve,$_POST['notetype'],$_POST['classeEtab'],$controleid,$matiereid,$teatcherid,$libellesessionencours,$_POST['codeEtab']);
                                                            //$studentsX=$student->getAllstudentNotesofthisclasses($_POST['classeEtab'],$matiereid,$controleid,$teatcherid,$_POST['codeEtab']);

                                                          }else if($_POST['notetype']==2){
                                                            //cas d'un examen
                                                            $data=explode("-",$_POST['libctrl']);
                                                            $examid=$data[0];
                                                            $matiereid=$data[1];
                                                            $teatcherid=$data[2];
                                                            //$examid=$_POST['libctrl'];
                                                          //$studentsX=$student->getAllstudentNotesExamofthisclasses($_POST['classeEtab'],$examid,$_POST['codeEtab']);
                                                            $nbdata=$student->getNotesAndObservExamNb($value->idcompte_eleve,$_POST['notetype'],$_POST['classeEtab'],$examid,$matiereid,$teatcherid,$libellesessionencours,$_POST['codeEtab']);
                                                          }



                                                          if($nbdata==0)
                                                          {
                                                            $noteobtenu=0;
                                                            $observobtenu="";
                                                          }else {

                                                            if($_POST['notetype']==1)
                                                            {
                                                              $datanotesStudent=$student->getNotesAndObservControle($value->idcompte_eleve,$_POST['notetype'],$_POST['classeEtab'],$controleid,$matiereid,$teatcherid,$libellesessionencours,$_POST['codeEtab']);
                                                              $tabdata=explode("*",$datanotesStudent);
                                                              $noteobtenu=$tabdata[0];
                                                              $observobtenu=$tabdata[1];

                                                            }else if($_POST['notetype']==2)
                                                            {
                                                              $datanotesStudent=$student->getNotesAndObservExam($value->idcompte_eleve,$_POST['notetype'],$_POST['classeEtab'],$examid,$matiereid,$teatcherid,$libellesessionencours,$_POST['codeEtab']);
                                                              $tabdata=explode("*",$datanotesStudent);
                                                              $noteobtenu=$tabdata[0];
                                                              $observobtenu=$tabdata[1];
                                                            }
                                                          }
                                                            //var_dump($datanotesStudent);
                                                        ?>
                                                        <tr class="odd gradeX">
                                                            <!--td>
                                                              <?php //echo $i;?>
                                                            </td-->
                                                            <!--td> <?php //echo $value->matricule_eleve;?></td-->
                                                            <td style="text-align:center">
                                                                <a href="#"><?php echo $value->nom_eleve." ".$value->prenom_eleve;?> </a>
                                                            </td>
                                                            <td style="text-align:center">
                                                                <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $noteobtenu; ?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/>
                                                                <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                            </td>
                                                            <td>

                                                                  <textarea  name="obserE<?php echo $value->idcompte_eleve;?>" id="obserE<?php echo $value->idcompte_eleve;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $value->idcompte_eleve;?>)" readonly><?php echo $observobtenu ?></textarea>
                                                                  <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                            </td>
                                                            <!-- td>
                                                              <a href="updatenotes.php?noteid=<?php //echo $value->id_notes ?>&tyepenote=<?php echo $value->idcompte_eleve;?>&eleveid=<?php echo $value->type_notes;?>"  class="btn btn-warning btn-xs">
                                                                <i class="material-icons">edit</i>
                                                              </a>
                                                               <!-- <a href="#"  class="btn btn-warning btn-xs" data-toggle="modal" data-target="#largeModel">
                                                                <i class="material-icons">edit</i>
                                                              </a> -->

                                                            <!-- </td-->

                          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                          <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                          <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                          <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                          <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                          <script type="text/javascript">

                          function check(id)
                          {

                            var note=$("#note"+id).val();
                            var obser=$("#obserE"+id).val();

                              if(note==""||obser=="")
                              {
                                  if(note=="")
                                  {
                  document.getElementById("messageCont"+id).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";
                                  }

                                  if(obser=="")
                                  {
                document.getElementById("messageObservE"+id).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";
                                  }
                                  //event.preventDefault();
                              }else {
                                //document.location.href="../controller/notes.php?etape=3";
                                //authorise();
                                //alert("bonjour");
                                $("#UpdateNote"+id).submit();
                              }




                          }

                          function erasedNote(id)
                          {
                            document.getElementById("messageCont"+id).innerHTML = "";
                          }

                          function erasedObser(id)
                          {
                            document.getElementById("messageObservE"+id).innerHTML = "";
                          }
                          function authorise()
                          {
                            //$("#UpdateNote<?php //echo $value->id_notes;?>").submit();
                          }

                          </script>

                                                        </tr>

                                                          <?php
                                                             $i++;
                                                             $j++;
                                                                 endforeach;
                                                               ?>



                                                      </tbody>
                                                  </table>

                                                  <?php
                                                  //echo $matricules;
                                                  $tabMat=explode("*",$matricules);
                                                  $nb=count($tabMat);



                                                  ?>
                                                  <input type="hidden" name="studentmat" id="studentmat" value="<?php echo $matricules;?>"/>
                                                  <input type="hidden" name="etape" id="etape" value="1"/>
                                                  <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nb;?>"/>
                                                  <input type="hidden" name="allpresent" id="allpresent" value=""/>
                                                  <input type="hidden" name="classeId" id="classeId" value="<?php echo $_POST['classeEtab']?>"/>
                                                  <input type="hidden" name="datePresence" id="datePresence" value="<?php echo $_POST['datepre']?>"/>
                                                  <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                  <center>
                                                    <button type="button"  class="btn btn-primary btn-xs"> <i class="fa fa-print"></i> <?php echo L::Printer ?> </button>
                                                    <?php
                                                    if($_POST['notetype']==1)
                                                    {
                                                      ?>
                  <a href="updatenoteclasses.php?classe=<?php  echo $_POST['classeEtab'] ?>&controle=<?php echo $controleid ?>&matiere=<?php echo $matiereid ?>&teatcher=<?php echo $teatcherid ?>&codeEtab=<?php echo $_POST['codeEtab'];  ?>"  class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Modifier </a>
                                                      <?php
                                                    }else if($_POST['notetype']==2)
                                                    {
                                                      ?>
                                                        <a href="updatenoteclasses.php?classe=<?php  echo $_POST['classeEtab'] ?>&examen=<?php echo $examid ?>&matiere=<?php echo $matiereid ?>&teatcher=<?php echo $teatcherid ?>&codeEtab=<?php echo $_POST['codeEtab'];  ?>"  class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> Modifier </a>
                                                      <?php

                                                    }

                                                     ?>

                                                  </center>
                                                </form>

                                              </div>
                                                  </div>
                                              </div>


                          <?php
                      }
                   ?>



                          </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <!--script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script-->
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   $("#classeEtab").select2();
   $("#notetype").select2();
   $("#libctrl").select2();
   $("#matclasse").select2();

   function searchDesignation()
   {
     var classeEtab=$("#classeEtab").val();
     var codeEtab=$("#codeEtab").val();
     var matiere=$("#matclasse").val();
     var typenote=$("#notetype").val();
     var etape=2;
     //nous allons verifier si nous avons des notes pour ce controle ou examen
     $.ajax({

       url: '../ajax/designation.php',
       type: 'POST',
       async:true,
        data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&typenote='+typenote,
        dataType: 'text',
        success: function (content, statut) {




          $("#libctrl").html("");
          $("#libctrl").html(content);

        }

     });

   }

   function searchcodeEtab(id)
   {
     var classe=$("#classeEtab").val();
     var teatcherId=id;
     var etape=7;
     var matiere=$("#matclasse").val();

     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
          dataType: 'text',
          success: function (content, statut) {

             $("#FormNotes #codeEtab").val(content);

          }
        });

   }

   function searchmatiere(id)
   {

       var classe=$("#classeEtab").val();
       var teatcherId=id;
       var etape=6;


     $.ajax({

          url: '../ajax/matiere.php',
          type: 'POST',
          async:true,
          data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
          dataType: 'text',
          success: function (content, statut) {


            $("#matclasse").html("");
            $("#matclasse").html(content);

          }
        });
   }


   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
