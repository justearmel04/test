<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Teatcher.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$teatcher=new Teatcher();
$parents=new ParentX();
$student=new Student();
$imageprofile=$user->getImageProfile($emailUti);
$logindata=$user->getLoginProfile($emailUti);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
// $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$tabstudentmat="";
$nb=0;
$concatids="";
$typeEvaluation="";

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}

$nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
$students=$student->getAllstudentofthisclassesSession($_GET['classe'],$libellesessionencours);


if(isset($_GET['examen']))
{
$typeEvaluation=2;
}else if(isset($_GET['controle']))
{
$typeEvaluation=1;
}


$tabstudents=explode(",",$_GET['matriculedelete']);
$datastudents=implode(',',$tabstudents);
$nbstudent=count($tabstudents);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Modification Notes - Classe :</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.html">Gestion des Notes</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Modification Notes </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header>Gestion des Notes : Classe</header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormUpdateNotes" action="../controller/notes.php" >
                       <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"  id="affichage3">
                           <thead>
                               <tr>
                                 <!--th style="width:15%"> Matricule </th-->
                                 <th style="width:40%"> Nom & Prénoms </th>
                                 <th style="width:25%;text-align:center"> Note </th>
                                 <th style="text-align:center"> Observation </th>


                               </tr>
                           </thead>
                           <tbody>
                             <?php
                             $ids="";
                             $oldnotesstudent="";
                             for($i=0;$i<$nbstudent;$i++)
                             {
                               if(isset($_GET['examen']))
                               {
                                 $type=2;
                                 $examenid=htmlspecialchars(addslashes($_GET['examen']));
                                 $classe=htmlspecialchars(addslashes($_GET['classe']));
                                 $matiere=htmlspecialchars(addslashes($_GET['matiere']));
                                 $teatcher=htmlspecialchars(addslashes($_GET['teatcher']));

                                 //verifier si l'eleve à une note
                                 $nbnotes=$etabs->getAllnotesOfExamensClassesOfStudentNB($classe,$matiere,$teatcher,$examenid,$type,$tabstudents[$i]);

                                 if($nbnotes==0)
                                 {
                                   $ids=$ids.$tabstudents[$i]."*";
                                   $oldnotesstudent=$oldnotesstudent.$nbnotes."*";
                                   $currentnote=$nbnotes;
                                   $currentobserv="";
                                   $currentid=$tabstudents[$i];
                                 }else if($nbnotes>0)
                                 {
                                   $notes=$etabs->getAllnotesOfExamensClassesOfStudent($classe,$matiere,$teatcher,$examenid,$type,$tabstudents[$i]);
                                   $array=json_encode($notes,true);
                                   $someArray = json_decode($array, true);

                                   $ids=$ids.$tabstudents[$i]."*";
                                   $oldnotesstudent=$oldnotesstudent.$someArray[0]["valeur_notes"]."*";
                                   $currentnote=$someArray[0]["valeur_notes"];
                                   $currentobserv=$someArray[0]["obser_notes"];
                                   $currentid=$tabstudents[$i];

                                 }



                               }else if(isset($_GET['controle']))
                               {
                                 $type=1;
                                 $classe=htmlspecialchars(addslashes($_GET['classe']));
                                 $matiere=htmlspecialchars(addslashes($_GET['matiere']));
                                 $teatcher=htmlspecialchars(addslashes($_GET['teatcher']));
                                 $controleid=htmlspecialchars(addslashes($_GET['controle']));

                                 //verifier si l'eleve à une note
                                 $nbnotes=$etabs->getAllnoteControlesClassesOfStudentNB($classe,$matiere,$teatcher,$controleid,$type,$tabstudents[$i]);
                                 if($nbnotes==0)
                                 {
                                   $ids=$ids.$tabstudents[$i]."*";
                                   $oldnotesstudent=$oldnotesstudent.$nbnotes."*";
                                   $currentnote=$nbnotes;
                                   $currentobserv="";
                                   $currentid=$tabstudents[$i];
                                 }else if($nbnotes>0)
                                 {
                                   $notes=$etabs->getAllnoteControlesClassesOfStudent($classe,$matiere,$teatcher,$controleid,$type,$tabstudents[$i]);
                                   $array=json_encode($notes,true);
                                   $someArray = json_decode($array, true);
                                   $ids=$ids.$tabstudents[$i]."*";

                                   $oldnotesstudent=$oldnotesstudent.$someArray[0]["valeur_notes"]."*";
                                   $currentnote=$someArray[0]["valeur_notes"];
                                   $currentobserv=$someArray[0]["obser_notes"];
                                   $currentid=$tabstudents[$i];
                                 }

                                 // $donnees=$someArray[0]["nom_eleve"]." ".$someArray[0]["prenom_eleve"];
                               }

                              ?>
                             <tr>
                               <td style="text-align:center">
                                 <?php
                               echo $student->getNameInfos($tabstudents[$i]);
                                 ?></td>
                               <td style="text-align:center">
                     <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $currentid;?>" id="noteE<?php echo $currentid;?>" value="<?php echo $currentnote; ?>" style="width:100px" onclick="erasedNote(<?php echo $currentid;?>)" />
                     <p id="messageNoteE<?php echo $currentid;?>"></p>
                               </td>
                               <td>
                                 <textarea  name="obserE<?php echo $currentid;?>" id="obserE<?php echo $currentid;?>" rows="3" cols="50" onclick="erasedObserv(<?php echo $currentid;?>)" ><?php echo $currentobserv; ?></textarea>
                                  <p id="messageObservE<?php echo $currentid;?>"></p>

                               </td>
                             </tr>
                             <?php
                           }
                              ?>
                           </tbody>
                       </table>
                       <?php
                       //echo $matricules;
                       // $tabMat=explode("*",$matricules);
                       // $nb=count($tabMat);



                       ?>
                       <input type="hidden" name="studentids" id="studentids" value="<?php echo $ids;?>"/>
                       <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                       <input type="hidden" name="etape" id="etape" value="5"/>
                       <?php
                       if(isset($_GET['examen']))
                       {
                         ?>
                         <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nbstudent;?>"/>
                         <input type="hidden" name="classeId" id="classeId" value="<?php echo $classe?>"/>
                         <input type="hidden" name="typenote" id="typenote" value="<?php echo $type?>"/>
                          <input type="hidden" name="idtypenote" id="idtypenote" value="<?php echo $examenid;?>"/>
                         <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                         <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $matiere;?> ">
                         <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo $teatcher;?> ">
                         <?php

                       }else if(isset($_GET['controle']))
                       {
                        ?>
                        <input type="hidden" name="nbstudent" id="nbstudent" value="<?php echo $nbstudent;?>"/>
                        <input type="hidden" name="classeId" id="classeId" value="<?php echo $classe;?>"/>
                        <input type="hidden" name="typenote" id="typenote" value="<?php echo $type;?>"/>
                         <input type="hidden" name="idtypenote" id="idtypenote" value="<?php echo $controleid;?>"/>
                        <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabAssigner;?>"/>
                        <input type="hidden" name="matiereid" id="matiereid" value=" <?php echo $matiere;?> ">
                        <input type="hidden" name="teatcherid" id="teatcherid" value=" <?php echo $teatcher;?> ">
                        <?php

                       }
                        ?>
                          <center><button type="submit"  onclick="Bonjour()" class="btn btn-success"><i class="fa fa-check-circle"></i>Mofifier Notes</button></center>
                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->


<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

    <script>

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function present(id)
    {
     $("statut"+id).val(1);
    }

    function absent(id)
    {
     $("statut"+id).val(0);
    }

    $('#example5').DataTable( {
        "scrollX": true

    } );
    $("#classeEtab").select2();
    $("#notetype").select2();
    $("#libctrl").select2();
    $("#matclasse").select2();
    $('#datepre').bootstrapMaterialDatePicker
    ({
      date: true,
      time: false,
      format: 'DD/MM/YYYY',
      lang: 'fr',
      minDate : new Date(),
     cancelText: '<?php echo L::AnnulerBtn ?>',
     okText: 'OK',
     clearText: '<?php echo L::Eraser ?>',
     nowText: '<?php echo L::Now ?>'

    });

    function searchDesignation()
    {
      var classeEtab=$("#classeEtab").val();
      var codeEtab="<?php echo $codeEtabAssigner;  ?>";
      var matiere=$("#matclasse").val();
      var typenote=$("#notetype").val();
      var etape=1;
      //nous allons verifier si nous avons des notes pour ce controle ou examen
      $.ajax({

        url: '../ajax/designation.php',
        type: 'POST',
        async:true,
         data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab+'&matiere='+matiere+'&typenote='+typenote,
         dataType: 'text',
         success: function (content, statut) {

           $("#libctrl").html("");
           $("#libctrl").html(content);

         }

      });

    }

    function searchmatiere()
    {
      var classeEtab=$("#classeEtab").val();
      var codeEtab="<?php echo $codeEtabAssigner;  ?>";
      var etape=2;

      $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
         data:'etape=' + etape+'&classe='+classeEtab+'&code='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#matclasse").html("");
           $("#matclasse").html(content);

         }

      });

    }
    function Bonjour()
    {
      var tabidstudent=$("#studentids").val();
      var nbligne=$("#nbstudent").val();
      var tab=$("#studentids").val().split("*");
      var i;
      var statusnote=1;
      var statusobserv=1;
      var datastatus="";



      for(i=0;i<nbligne;i++)
      {
          var note=$("#noteE"+tab[i]).val();
          var obser=$("#obserE"+tab[i]).val();
           event.preventDefault();

          if(note==""||obser=="")
          {
            if(note=="")
            {
              document.getElementById("messageNoteE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";
              statusnote=statusnote+1;
            }

            if(obser=="")
            {
              document.getElementById("messageObservE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";
              statusobserv=statusobserv+1;
            }

          }else {
            statusnote=statusnote-1;
            statusobserv=statusobserv-1;

            // autorise1();
          }

      }

       if(statusnote==statusobserv)
       {
         modificationexistance();
         // autorise1();
       }

       // alert(statusnote+" "+statusobserv);
    }

    function modificationexistance()
    {
      //recuperation des variables

      var typeEvaluation="<?php echo $typeEvaluation; ?>";

      if(typeEvaluation==1)
      {
         //il est question d'un controle
         var classe="<?php echo $_GET['classe'] ?>";
         var controle="<?php echo @$_GET['controle'] ?>";
         var matiere="<?php echo $_GET['matiere'] ?>";
         var teatcher="<?php echo $_GET['teatcher'] ?>";
         var matriculedelete="<?php echo $_GET['matriculedelete'] ?>";
         var codeEtab="<?php echo $codeEtabAssigner; ?>";
         var session="<?php echo $libellesessionencours; ?>";

         //nous allons verifier si nous avons au moins une modification de note en attente pour cet controle

         var etape=4;

         $.ajax({

           url: '../ajax/designation.php',
           type: 'POST',
           async:true,
            data:'etape=' + etape+'&classe='+classe+'&codeEtab='+codeEtab+'&session='+session+'&matriculedelete='+matriculedelete+'&teatcher='+teatcher+'&matiere='+matiere+'&controle='+controle,
            dataType: 'text',
            success: function (content, statut) {

              if(content==0)
              {
                autorise1();
              }else {

                Swal.fire({
                type: 'error',
                title: '<?php echo L::WarningLib ?>',
                text: "Une modification de note est dejà en attente de validation",

              })
              }

            }

         });

      }else {
        //il est question d'un examen
        var classe="<?php echo $_GET['classe'] ?>";
        var examen="<?php echo @$_GET['examen'] ?>";
        var matiere="<?php echo $_GET['matiere'] ?>";
        var teatcher="<?php echo $_GET['teatcher'] ?>";
        var matriculedelete="<?php echo $_GET['matriculedelete'] ?>";
        var codeEtab="<?php echo $codeEtabAssigner; ?>";
        var session="<?php echo $libellesessionencours; ?>";

        //nous allons verifier si nous avons au moins une modification de note en attente pour cet controle

        var etape=5;

        $.ajax({

          url: '../ajax/designation.php',
          type: 'POST',
          async:true,
           data:'etape=' + etape+'&classe='+classe+'&codeEtab='+codeEtab+'&session='+session+'&matriculedelete='+matriculedelete+'&teatcher='+teatcher+'&matiere='+matiere+'&examen='+examen,
           dataType: 'text',
           success: function (content, statut) {

             if(content==0)
             {
               autorise1();
             }else {

               Swal.fire({
               type: 'error',
               title: '<?php echo L::WarningLib ?>',
               text: "Une modification de note est dejà en attente de validation",

             })
             }

           }

        });



      }



    }

    function check1()
    {
      var tabidstudent=$("#studentids").val();
      var nbligne=$("#nbstudent").val();
      var tab=$("#studentids").val().split("*");
      var i;

      // var note=$("#note"+tab[i]).val()

      for(i=0;i<nbligne;i++)
      {
          var note=$("#noteE"+tab[i]).val();
          var obser=$("#obserE"+tab[i]).val();

          if(note==""||obser=="")
          {
            if(note=="")
            {
              document.getElementById("messageNoteE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner la Note SVP !</font>";

            }

            if(note=="")
            {
              document.getElementById("messageObservE"+tab[i]).innerHTML = "<font color=\"red\">Merci de renseigner l'observation SVP !</font>";

            }

          }else {
            // autorise1();
          }


      }
    }
    function autorise1()
    {
        $("#FormUpdateNotes").submit();
    }



    function erasedNote(id)
    {
    document.getElementById("messageNote"+id).innerHTML = "";
    }

    function erasedObserv(id)
    {
    document.getElementById("messageObserv"+id).innerHTML = "";
    }

 function searchNotesClasses()
 {
   var notetype=$("#notetype").val();
   var classeEtab=$("#classeEtab").val();
   var codeEtab="<?php echo $codeEtabAssigner;  ?>";

   //nous allons rechercher la liste des designation de notes par classe

     if(notetype==""||classeEtab=="")
     {
       if(notetype=="")
       {
         Swal.fire({
         type: 'warning',
         title: '<?php echo L::WarningLib ?>',
         text: "Merci de selectionner le type de note",

       })
       }

       if(classeEtab=="")
       {
         Swal.fire({
         type: 'warning',
         title: '<?php echo L::WarningLib ?>',
         text: "Merci de selectionner une classe",

       })
       }
     }else {

         if(notetype==1)
         {
           var etape=1;
         }else if(notetype==2){
           var etape=2;
         }

       $.ajax({

         url: '../ajax/notesSearch.php',
         type: 'POST',
         async:true,
          data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
          dataType: 'text',
          success: function (content, statut) {

            $("#libctrl").html("");
            $("#libctrl").html(content);

          }

       });
     }



 }

    $(document).ready(function() {
 // check1();
 //
 $("#FormUpdateNotesxx").validate({
   errorPlacement: function(label, element) {
   label.addClass('mt-2 text-danger');
   label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
   $(element).parent().addClass('has-danger')
   $(element).addClass('form-control-danger')
  },
  success: function (e) {
       $(e).closest('.control-group').removeClass('error').addClass('info');
       $(e).remove();
   },
   rules:{


     classeEtab:"required",
     datepre:"required",
     notetype:"required",
     classeEtab:"required",
     libctrl:"required",
     matclasse:"required"



   },
   messages: {
     classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
     datepre:"Merci de choisir la date de présence",
     notetype:"Merci de selectionner le type de note",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     libctrl:"Merci de selectionner une désignation",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>"

   },
   submitHandler: function(form) {
     // form.submit();
   }
 });




    });

    </script>
    <!-- end js include path -->
  </body>

</html>
