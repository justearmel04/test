<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();




$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$matiere=new Matiere();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$_SESSION['user']['codeEtab']=$codeEtabsession;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $_SESSION['user']['session']=$libellesessionencours;
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::dashb ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::dashb ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <div class="col-md-12 ">
                <div class="alert blue-bgcolor alert-dismissible fade show" role="alert">
                <?php echo L::TeatcherDashMsg ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                 </a>
                </div>
              </div>
					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>

                <div class="row">

                       <!-- /.col -->
                      <div class="col-lg-4 col-sm-4">
                     <div class="overview-panel orange">
                       <div class="symbol">
                         <i class=" fa fa-building"></i>
                       </div>
                       <div class="value white">
                         <p class="sbold addr-font-h1" data-counter="counterup" data-value=""><?php echo count($classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte'])); ?></p>
                         <p><?php echo strtoupper(L::ClassesMenu) ?></p>
                       </div>
                     </div>
                   </div>
                       <!-- /.col -->
                       <div class="col-lg-4 col-sm-4">
                     <div class="overview-panel deepPink-bgcolor">
                       <div class="symbol">
                         <i class="fa fa-graduation-cap"></i>
                       </div>
                       <div class="value white">
                         <p class="sbold addr-font-h1" data-counter="counterup" data-value=""><?php echo $matiere->NbTeaMatiere($codeEtabsession,$libellesessionencours,$_SESSION['user']['IdCompte']);  ?></p>
                         <p><?php echo strtoupper(L::MatiereBold) ?></p>
                       </div>
                     </div>
                   </div>
                       <!-- /.col -->
                       <div class="col-lg-4 col-sm-4">
                     <div class="overview-panel blue-bgcolor">
                       <div class="symbol">
                           <i class="fa fa-group"></i>
                       </div>
                       <div class="value white">
                         <p class="sbold addr-font-h1" data-counter="counterup" data-value=""><?php echo $classe->getNumberStudentTeaclasses($_SESSION['user']['IdCompte']); ?></p>
                       <p><?php echo strtoupper(L::EleveMenu) ?></p>
                       </div>
                     </div>
                   </div>

                     </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">
            <div class="col-md-12 col-sm-12">
                 <div class="card-box">
                     <div class="card-head">
                         <header></header>
                     </div>
                     <div class="card-body ">
                      <div class="panel-body">
                                <div id="calendar1" class="has-toolbar"> </div>
                            </div>
                     </div>
                 </div>
             </div>

                    </div>

                    <button type="button" style="display:none" class="btn btn-primary" id="parentOld" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-plus"></i> Ancien parent </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="exampleModalLabel"><?php echo L::DetailsParasc  ?></h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<form id="#" class="" >
     <div class="row">
  <div class="col-md-12">
           <div class="form-group">
    <label for="oldparentname"><?php echo L::ActiviteLib  ?><span class="required"></span> :</label>
<input type="text" name="oldparentname" class="form-control" id="oldparentname" value="" size="32" maxlength="225" />

              </div>
            </div>
            <div class="col-md-12">
             <div class="form-group">
      <label for="oldparentprename"><?php echo L::DatedebLib  ?><span class="required"></span> :</label>
<input type="text" name="oldparentprename" class="form-control" id="oldparentprename" value="" size="32" maxlength="225" />

                </div>
              </div>
              <div class="col-md-12">
               <div class="form-group">
        <label for="oldparentprename"><?php echo L::DatefinLib  ?><span class="required"></span> :</label>
 <input type="text" name="oldparentsexe" class="form-control" id="oldparentsexe" value="" size="32" maxlength="225" />

                  </div>
                </div>
              <div class="col-md-12">
               <div class="form-group">
        <label for="oldparentphone"><?php echo L::LieuLib  ?><span class="required"></span> :</label>
 <input type="text" name="oldparentphone" class="form-control" id="oldparentphone" value="" size="32" maxlength="225" />

                  </div>
                </div>
                <div class="col-md-12" id="rowgratuit">
                 <div class="form-group">
          <!-- <label for="oldparentphone"><?php echo L::LieuLib  ?><span class="required"></span> :</label> -->
   <span class="label label-lg label-success" style=""><?php echo L::GratLib  ?></span>

                    </div>
                  </div>

                  <div class="col-md-12" id="rowpayant">
                   <div class="form-group">
            <!-- <label for="oldparentphone"><?php echo L::LieuLib  ?><span class="required"></span> :</label> -->
     <span class="label label-lg label-info" style=""><?php echo L::PaiLib  ?></span> <span class="label label-lg label-info" style=""> <span id="montpayant"></span> </span>

                      </div>
                    </div>

                <div class="col-md-12">
                 <div class="form-group">
          <label for="oldparentphone"><?php echo L::DescriLib  ?><span class="required"></span> :</label>
          <textarea  name="descri" id="descri" rows="8" cols="56"></textarea>
   <!-- <input type="text" name="oldparentphone" class="form-control" id="oldparentphone" value="" size="32" maxlength="225" /> -->

                    </div>
                  </div>
<!-- <button type="submit" name="KT_Insert1" id="KT_Insert1" class="btn btn-primary pull-right" style="">Valider <i class="icon-check position-right"></i></button> -->


     </div>

   </form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn  ?></button>
</div>
</div>
</div>
</div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

    <script>

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function loader()
    {
      var etape=1;
      var codeEtab="<?php echo $codeEtabsession;?>";
      var session="<?php echo $libellesessionencours ; ?>";

      $.ajax({
        url: '../ajax/load.php',
        type: 'POST',
        async:false,
        data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
        dataType: 'text',
        success: function (content, statut) {

          var data=content;

          // callback(data);

        }
      });

    }

    $(document).ready(function() {

      // alert(session);


      var calendar = $('#calendar1').fullCalendar({
        monthNames: ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'],
        monthNamesShort: ['<?php echo L::JanvLibshort ?>','<?php echo L::FevLibshort ?>','<?php echo L::MarsLibshort ?>','<?php echo L::AvriLibshort ?>','<?php echo L::MaiLibshort ?>','<?php echo L::Juinbshort ?>','<?php echo L::JulLibshort ?>','<?php echo L::AoutLibshort ?>','<?php echo L::SeptLibshort?>','<?php echo L::OctobLibshort?>','<?php echo L::NovbLibshort?>','<?php echo L::DecemLibshort?>'],
        dayNamesShort: ['<?php echo L::Dayslibshort7 ?>', '<?php echo L::Dayslibshort1 ?>', '<?php echo L::Dayslibshort2 ?>', '<?php echo L::Dayslibshort3 ?>', '<?php echo L::Dayslibshort4 ?>', '<?php echo L::Dayslibshort5 ?>', '<?php echo L::Dayslibshort6 ?>'],
        dayNames: ['<?php echo L::Dayslibs7 ?>', '<?php echo L::Dayslibs1 ?>', '<?php echo L::Dayslibs2 ?>', '<?php echo L::Dayslibs3 ?>', '<?php echo L::Dayslibs4 ?>', '<?php echo L::Dayslibs5 ?>', '<?php echo L::Dayslibs6 ?>'],
        timeFormat:'H(:mm)',
        timeZone: 'UTC',
        allDayText:'<?php echo L::Daysall ?>',
        // minTime: "08:00",
        // maxTime: "20:00",
        buttonText:{
   today:    '<?php echo L::TodayLib ?>',
   month:    '<?php echo L::MonthLib ?>',
   week:     '<?php echo L::SemaineLib ?>',
   day:      '<?php echo L::jourLib ?>',
   list:     'list'
 },
     locale: 'fr',
     editable:true,
     header:{
      left:'prev,next today',
      center:'title',
      right:'month,agendaWeek,agendaDay'
     },
     events:'../ajax/load.php?view=2',
    // events:loader(),
     // events: 'load.php?codeEtab='+codeEtab+'&session='+session+'&etape='+etape,
     selectable:false,
     selectHelper:false,
     select: function(start, end, allDay)
     {
      var title = prompt("Enter Event Title");
      if(title)
      {
       var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
       var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
       $.ajax({
        url:"insert.php",
        type:"POST",
        data:{title:title, start:start, end:end},
        success:function()
        {
         calendar.fullCalendar('refetchEvents');
         alert("Added Successfully");
        }
       })
      }
     },
     editable:false,
     eventResize:function(event)
     {
      var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
      var title = event.title;
      var id = event.id;
      $.ajax({
       url:"update.php",
       type:"POST",
       data:{title:title, start:start, end:end, id:id},
       success:function(){
        calendar.fullCalendar('refetchEvents');
        alert('Event Update');
       }
      })
     },

     eventDrop:function(event)
     {
      var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
      var title = event.title;
      var id = event.id;
      $.ajax({
       url:"update.php",
       type:"POST",
       data:{title:title, start:start, end:end, id:id},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Event Updated");
       }
      });
    },
    eventClick:function(event)
    {
      var id = event.id;
      var etape=7;
      var codeEtab="<?php echo $codeEtabsession;?>";
      var session="<?php echo $libellesessionencours ; ?>";
      // alert(id);
      $.ajax({
        url: '../ajax/login.php',
        type: 'POST',
        async:false,
        data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&parascolaire='+id,
        dataType: 'text',
        success: function (content, statut) {

          // var data=content;
          var cut = $.trim(content);
          $tabcontent=cut.split("*");

          $("#oldparentname").val($tabcontent[0]);
          $("#oldparentprename").val($tabcontent[1]+" "+$tabcontent[2]);
          $("#oldparentsexe").val($tabcontent[3]+" "+$tabcontent[4]);
          $("#oldparentphone").val($tabcontent[5]);
          $("#descri").html($tabcontent[10]);

          var gratuit=$tabcontent[7];
          var montant=$tabcontent[8];

          if(gratuit==0)
          {
            $("#rowgratuit").show();
            $("#rowpayant").hide();

          }else {
            $("#rowpayant").show();
            $("#rowgratuit").hide();
            $("#montpayant").html(montant+" FCFA");
          }

          // alert($tabcontent[11]);

          // callback(data);

           $("#parentOld").click();

        }
      });

    },

     // ,
     //
     // eventClick:function(event)
     // {
     //  if(confirm("Are you sure you want to remove it?"))
     //  {
     //   var id = event.id;
     //   $.ajax({
     //    url:"delete.php",
     //    type:"POST",
     //    data:{id:id},
     //    success:function()
     //    {
     //     calendar.fullCalendar('refetchEvents');
     //     alert("Event Removed");
     //    }
     //   })
     //  }
     // },

    });

    });

    </script>
    <!-- end js include path -->
  </body>

</html>
