<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
}else {
  $nbclasse=0;
}

$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Programmes Academiques</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Programmes académiques</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                     <div class="col-md-12 col-sm-12">
                         <div class="card card-box">
                             <div class="card-head">
                                 <header></header>

                             </div>

                             <div class="card-body" id="bar-parent">
                               <form class="form-horizontal" method="post" action="../controller/academiques.php">
                                 <div class="row">
                 <div class="col-md-12">

           <fieldset style="background-color:#007bff;">

           <center><legend style="color:white;">SYLLABUS</legend></center>
           </fieldset>


           </div>
         </div> <br>
                                      <div class="form-group row">
                                          <label for="horizontalFormEmail" class="col-sm-4 control-label" style="margin-left:-176px;"><b>Année académique</b> </label>
                                          <div class="col-sm-8">
                                              <input type="session" class="form-control" id="horizontalFormEmail" placeholder="" style="width:200px;">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label for="horizontalFormPassword" class="col-sm-2 control-label"><b>Nom Enseignant</b> </label>
                                          <div class="col-sm-10">
                                              <input type="text" class="form-control" id="horizontalFormPassword" placeholder="" style="width:700px;">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label for="horizontalFormPassword" class="col-sm-2 control-label"><b>Classe<span class="required"> * </span></b> </label>
                                          <div class="col-sm-10">
                                              <input type="text" class="form-control" id="horizontalFormPassword" placeholder="" style="width:700px;">
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label for="horizontalFormPassword" class="col-sm-2 control-label"><b>Matière<span class="required"> * </span>: </b></label>
                                          <div class="col-sm-10">
                                              <input type="text" class="form-control" id="horizontalFormPassword" placeholder="" style="width:700px;">
                                          </div>
                                      </div>
                                      <div class="row">
                      <div class="col-md-12">

                <fieldset style="background-color:#007bff;">

                <center><legend style="color:white;">Informations générales</legend></center>
                </fieldset>


                </div>
              </div> <br>

              <div class="row">
                                  <div class="col-md-12">
              <div class="form-group">
              <label for=""><b>Description du cours<span class="required"> * </span>: </b></label>
                 <textarea class="form-control" rows="8" name="descri" id="descri" placeholder="Entrer la description du cours"></textarea>

              </div>

              </div>
             </div>

             <div class="row">
                                 <div class="col-md-12">
             <div class="form-group">
             <label for=""><b>Objectifs du cours <span class="required"> * </span>: </b></label>

             </div>
<input type="hidden" id="nb" name="nb" value="1"> <br>
             </div>
            </div>





              <fieldset class="todos_labels">

    <div class="repeatable "></div>
    <br>

    <a href="#" class="add btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouveau Objectif</a>

    <!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

  </fieldset> <br>

  <div class="row">
                      <div class="col-md-12">
  <div class="form-group">
  <label for=""><b>Contenu <span class="required"> * </span>: </b></label>

  </div>
<input type="hidden" id="nbcont" name="nbcont" value="1"> <br>
  </div>
 </div>

 <fieldset class="todos_labels1">

<div class="repeatable1 "></div>
<br>

<a href="#" class="addcont btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouveau Thème</a>

<!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

</fieldset> <br>

<div class="row">
                    <div class="col-md-12">
<div class="form-group">
<label for=""><b>Prérequis nécessaires<span class="required"> * </span>: </b></label>

</div>
<input type="hidden" id="nbreq" name="nbreq" value="1"> <br>
</div>
</div>

<fieldset class="todos_labels2">

<div class="repeatable2 "></div>
<br>

<a href="#" class="addreq btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouveau Prérequis</a>

<!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

</fieldset> <br>

<div class="row">
                    <div class="col-md-12">
<div class="form-group">
<label for=""><b>Compétences visées  <span class="required"> * </span>: </b></label>

</div>
<input type="hidden" id="nbcomp" name="nbcomp" value="1"> <br>
</div>
</div>

<fieldset class="todos_labels3">

<div class="repeatable3 "></div>
<br>

<a href="#" class="addcomp btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouvelle compétence</a>

<!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

</fieldset> <br>
<div class="row">
<div class="col-md-12">

<fieldset style="background-color:#007bff;">

<center><legend style="color:white;">Documents de cours</legend></center>
</fieldset>


</div>
</div> <br>

<div class="row">
                    <div class="col-md-12">
<div class="form-group">
<label for=""><b>Documents obligatoires  <span class="required"> * </span>: </b></label>
</div>
<input type="hidden" id="nbdoc" name="nbdoc" value="1"> <br>
</div>
</div>
<fieldset class="todos_labels4">

<div class="repeatable4 "></div>
<br>

<a href="#" class="adddoc btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouveau document</a>

<!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

</fieldset> <br>

<div class="row">
                    <div class="col-md-12">
<div class="form-group">
<label for=""><b>Documents facultatifs <span class="required"> * </span>: </b></label>
</div>
<input type="hidden" id="nbdocfac" name="nbdocfac" value="1"> <br>
</div>
</div>

<fieldset class="todos_labels5">

<div class="repeatable5 "></div>
<br>

<a href="#" class="adddocfac btn btn-warning btn-md"><i class="fa fa-plus"></i> Nouveau document</a>

<!--input type="button" value="Add" class="add btn btn-primary btn-md" id=""-->

</fieldset> <br>








                                      <div class="form-group">
                                          <div class="offset-md-3 col-md-9">
                                              <button type="submit" class="btn btn-info">Submit</button>
                                              <button type="button" class="btn btn-default">Cancel</button>
                                          </div>
                                      </div>
                                  </form>
                             </div>
                         </div>
                     </div>
                 </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script src="../assets2/js/jquery.repeatable.js"></script>





    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->


    <script type="text/template" id="todos_labels">



    <div class="field-group">

     <label for="task_{?}" class="col-md-2"> Objectif {?}  </label>

     <input type="text" name="todos_labels[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="delete btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>

    <script type="text/template" id="todos_labels1">



    <div class="field1-group">

     <label for="task_{?}" class="col-md-2"> Thème {?}  </label>

     <input type="text" name="todos_labels1[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="deletecont btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>


    <script type="text/template" id="todos_labels2">



    <div class="field2-group">

     <label for="task_{?}" class="col-md-2"> Prérequis {?}  </label>

     <input type="text" name="todos_labels1[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="deletereq btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>

    <script type="text/template" id="todos_labels3">



    <div class="field3-group">

     <label for="task_{?}" class="col-md-2"> Compétence {?}  </label>

     <input type="text" name="todos_labels1[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="deletecomp btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>


    <script type="text/template" id="todos_labels4">



    <div class="field4-group">

     <label for="task_{?}" class="col-md-2"> Document {?}  </label>

     <input type="text" name="todos_labels1[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="deletedoc btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>

    <script type="text/template" id="todos_labels5">



    <div class="field5-group">

     <label for="task_{?}" class="col-md-2"> Document {?}  </label>

     <input type="text" name="todos_labels1[{?}][task]" value="{task}" id="task_{?}" class="col-md-6">

     <a href="#" class="deletedocfac btn btn-danger btn-md" class="col-md-2"><i class="fa fa-trash"></i> Supprimer</a>


     <!--input type="button" class="delete btn-danger btn-md" id="" value="Supprimer"-->

    </div>

    </script>


   <script>
   $(".todos_labels .repeatable").repeatable({

     addTrigger:".todos_labels .add",

     deleteTrigger:".todos_labels .delete",

     template:"#todos_labels"

   });

   $(".todos_labels1 .repeatable1").repeatable({

     addTrigger:".todos_labels1 .addcont",

     deleteTrigger:".todos_labels1 .deletecont",

     template:"#todos_labels1"

   });

   $(".todos_labels2 .repeatable2").repeatable({

     addTrigger:".todos_labels2 .addreq",

     deleteTrigger:".todos_labels2 .deletereq",

     template:"#todos_labels2"

   });

   $(".todos_labels3 .repeatable3").repeatable({

     addTrigger:".todos_labels3 .addcomp",

     deleteTrigger:".todos_labels3 .deletecomp",

     template:"#todos_labels3"

   });

   $(".todos_labels4 .repeatable4").repeatable({

     addTrigger:".todos_labels4 .adddoc",

     deleteTrigger:".todos_labels4 .deletedoc",

     template:"#todos_labels4"

   });

   $(".todos_labels5 .repeatable5").repeatable({

     addTrigger:".todos_labels5 .adddocfac",

     deleteTrigger:".todos_labels5 .deletedocfac",

     template:"#todos_labels5"

   });

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }


   $(document).ready(function() {



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
