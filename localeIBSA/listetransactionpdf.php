<?php

session_start();

require_once('../class/User.php');

require_once('../class/Etablissement.php');

require_once('../class/LocalAdmin.php');

require_once('../class/Parent.php');

require_once('../class/Teatcher.php');

require_once('../class/Classe.php');

require_once('../class/Student.php');

require_once('../class/Matiere.php');

require_once('../class/Sessionsacade.php');

require_once('../controller/functions.php');

require_once('../intl/i18n.class.php');



if(!isset($_SESSION['user']['lang']))

{

  $_SESSION['user']['lang']="fr";

}



$i18n = new i18n();

$i18n->setCachePath('../langcache');

$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path

$i18n->setFallbackLang($_SESSION['user']['lang']);

$i18n->setPrefix('L');

$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available

$i18n->setSectionSeperator('_');

$i18n->setMergeFallback(false);

$i18n->init();





$etabs=new Etab();

$student=new Student();

$classe=new Classe();

$parentX=new ParentX();

$matieres=new Matiere();





$codeEtab=$_SESSION['user']['codeEtab'];

$sessionEtab=$_SESSION['user']['session'];

$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

$libelleEtab=$etabs->getEtabNamebyCodeEtab($codeEtab);



if(isset($_GET['datedeb'])&&isset($_GET['datefin'])&&isset($_GET['motif']))

{

  $datedeb=date_format(date_create($_GET['datedeb']),"Y-m-d");

  $datefin=date_format(date_create($_GET['datefin']),"Y-m-d");

  $motif=$_GET['motif'];

  $alltransactions=$etabs->getAllLastTransactionsNotLimitedPeriodes($datedeb,$datefin,$codeEtab,$sessionEtab,$motif);

}else {

  $alltransactions=$etabs->getAllLastTransactionsNotLimited($codeEtab,$sessionEtab);

}



$montanttotal=0;



require('fpdf/fpdf.php');



class PDF extends FPDF

{


function Header()
{


    // Arial gras 15
    $this->SetFont('Arial','B',15);
    // Calcul de la largeur du titre et positionnement
    $this->SetX((210-$w)/2);
    // Couleurs du cadre, du fond et du texte
    $this->SetDrawColor(0,80,180);
    $this->SetFillColor(230,230,0);
    $this->SetTextColor(220,50,50);
    // Epaisseur du cadre (1 mm)
    $this->SetLineWidth(1);
    // Titre
    // Saut de ligne
    $this->Ln(10);
}




function Footer()

{

    // Positionnement à 1,5 cm du bas

    $this->SetY(-15);

    // Police Arial italique 8

    $this->SetFont('Arial','I',8);

    // Numéro et nombre de pages

    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');

    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');

}



// Tableau simple

function BasicTable($header, $data)

{

    // En-tête

    foreach($header as $col)

        $this->Cell(40,7,$col,1);

    $this->Ln();

    // Données

    foreach($data as $row)

    {

        foreach($row as $col)

            $this->Cell(40,6,$col,1);

        $this->Ln();

    }

}

}



$pdf = new PDF();

$pdf->AliasNbPages();


$pdf->SetMargins(3,0);

$pdf->SetFont('Times','B',  16);

$pdf->AddPage("P");

$pdf->Ln(10);

$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,2,4,45);

$pdf->Ln(20);

$pdf -> SetX(20);

$pdf-> SetFont('Times','B',  15);

$pdf->Cell(176,15,$libelleEtab,0,0,'C');

$pdf->Ln(25);

if(isset($_GET['datedeb'])&&isset($_GET['datefin'])&&isset($_GET['motif']))

{

  // $periodenames=strtoupper(L::transactionstories)." ".strtoupper(L::DuPaiementMode)." ".$_GET['datedeb']." ".strtoupper(L::PeriodAu)." ".$_GET['datefin'];

  $periodenames=strtoupper(L::transactionstories)." ".strtoupper(L::DuPaiementMode)." ".date_format(date_create($_GET['datedeb']),"d/m/Y")." ".strtoupper(L::PeriodAu)." ".date_format(date_create($_GET['datefin']),"d/m/Y");

  $pdf -> SetX(20);

  $pdf-> SetFont('Times','',  13);

  $pdf->Cell(176,5,$periodenames,0,0,'C');

  $pdf->Ln(12);

  $pdf -> SetX(5);

}else {

  $pdf -> SetX(20);

  $pdf-> SetFont('Times','',  13);

  $pdf->Cell(176,5,strtoupper(L::transactionstories),0,0,'C');

  $pdf->Ln(12);

  $pdf -> SetX(5);

}





// $pdf->Ln(30);

$pdf->SetFillColor(255,255,255);

$pdf->SetLineWidth(.3);

$pdf->SetFont('Times','B',12);

$pdf->Cell(46,8,strtoupper(L::DateVersementss),1,0,'L','1');

$pdf->Cell(50,8,strtoupper(L::Versements),1,0,'C','1');

$pdf->Cell(54,8,strtoupper(L::MotifVersementss),1,0,'C','1');

$pdf->Cell(50,8,strtoupper(L::MontantSection),1,0,'C','1');

$pdf->Ln();

$montantscola=0;

$montantinscri=0;

$montantreins=0;

$montantaes=0;

foreach ($alltransactions as  $value):

$montanttotal=$montanttotal+$value->montant_versement;



if($value->motif_versement=="INSCRIPTIONS")

{

$montantinscri=$montantinscri+$value->montant_versement;

}else if($value->motif_versement=="SCOLARITES")

{

$montantscola=$montantscola+$value->montant_versement;

}else if($value->motif_versement=="REINSCRIPTIONS")

{

$montantreins=$montantreins+$value->montant_versement;

}else if($value->motif_versement=="AES")

{

$montantaes=$montantaes+$value->montant_versement;

}



$pdf->SetX(5);

$pdf->Cell(46,8,date_format(date_create($value->date_versement),"d/m/Y"),1,0,'L');

$pdf->Cell(50,8,$value->code_versement,1,0,'C');

$pdf->Cell(54,8,$value->motif_versement,1,0,'C');

$pdf->Cell(50,8,prixMill($value->montant_versement),1,0,'C');

$pdf->Ln();

endforeach;

$pdf->SetX(5);

$pdf->SetFont('Times','B',12);

$pdf->Cell(150,8,L::MontantTotal,1,0,'C');

$pdf->Cell(50,8,strrev(wordwrap(strrev($montanttotal), 3, ' ', true)),1,0,'C');

$pdf->Ln(20);



// $pdf->Ln();

if(isset($_GET['datedeb'])&&isset($_GET['datefin'])&&isset($_GET['motif']))

{



if($_GET['motif']=="INSCRIPTIONS")

{

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::InscriptionScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantinscri), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

}else if($_GET['motif']=="REINSCRIPTIONS")

{

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::ReInscriptionScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantreins), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

}else if($_GET['motif']=="SCOLARITES")

{

  $pdf -> SetX(101);

  $pdf->SetFillColor(230,230,0);

  $pdf->SetLineWidth(.3);

  $pdf->SetFont('Times','B',11);

  $pdf->Cell(54,8,L::ScolaritiesScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantscola), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

}else if($_GET['motif']=="AES")

{

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::AesScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantaes), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

}





}else {



  $pdf -> SetX(101);

  $pdf->SetFillColor(230,230,0);

  $pdf->SetLineWidth(.3);

  $pdf->SetFont('Times','B',11);

  $pdf->Cell(54,8,L::ScolaritiesScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantscola), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::InscriptionScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantinscri), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::ReInscriptionScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantreins), 3, ' ', true)),1,0,'C');

  $pdf->Ln();

  $pdf -> SetX(101);

  $pdf->Cell(54,8,L::AesScream,1,0,'C');

  $pdf->Cell(50,8,strrev(wordwrap(strrev($montantaes), 3, ' ', true)),1,0,'C');

  $pdf->Ln(20);



}







$pdf->Output();









 ?>

