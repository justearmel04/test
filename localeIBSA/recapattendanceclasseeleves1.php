<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');
require_once('../class/Student.php');


if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="DAF"||$_SESSION['user']['fonctionuser']=="Intendance"||$_SESSION['user']['fonctionuser']=="Marketing"||$_SESSION['user']['fonctionuser']=="CAES"||$_SESSION['user']['fonctionuser']=="CD"||$_SESSION['user']['fonctionuser']=="CC")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);
        }

    }
  }

  // var_dump($allcodeEtabs);
  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Reacpsdesabsences ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Presences ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::Reacpsdesabsences ?> </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header><?php echo L::Reacpsdesabsences ?></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="recapattendanceclasseeleves1.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label><?php echo L::ClasseMenu ?></label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searstudent()">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::EleveMenusingle ?></label>
                           <select class="form-control" name="eleveid" id="eleveid">

                           </select>

                       </div>


                   </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::DatedebLib ?></label>
                           <input type="text" id="datedeb" name="datedeb" class="form-control" placeholder="<?php echo L::EntersDatedebLib ?>">

                           <input type="hidden" name="search" id="search" />
                           <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $_SESSION['user']['codeEtab'] ?>">
                       </div>


                   </div>
                   <div class="col-md-6 col-sm-6">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label><?php echo L::DatefinLib ?></label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <input type="text" id="datefin" name="datefin" class="form-control" placeholder="<?php echo L::EntersDatefinLib ?>">

                   </div>


               </div>
                   <button type="submit" class="btn btn-success"  style="width:150px;height:35px;margin-top:33px;"><?php echo L::AfficherleRecap ?></button>

                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['classeEtab'])&&isset($_POST['datedeb'])&&isset($_POST['datefin'])&&isset($_POST['eleveid'])&&isset($_POST['codeEtab']))
          {
              //nous devons recupérer la liste des jours du mois selectionner

               //$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);

               // $moisconcat=$_POST['month'];
               // $annee=$_POST['yearsession'];
               // $tabmoisconcat=explode("-",$moisconcat);
               // $mois=$tabmoisconcat[0];
               // $numbermois=$tabmoisconcat[1];
               // $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);

               // $concat=$annee."-".regiveMois($numbermois)."-";

               $datetime1 = date_create($_POST['datedeb']); // Date fixe
               $datetime2 = date_create($_POST['datefin']); // Date fixe
               $interval = date_diff($datetime1, $datetime2);
               $nb= $interval->format('%a');
               // $datedebut=new DateTime($_POST['datedeb']);
               // $datefin=new DateTime($_POST['datefin']);
               //
               // $interval = date_diff($datedebut,$datefin);

               $infosclasses=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

               //nous allons chercher la liste des eleves de cette classe
               // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
               // $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

               // $datas=$student->getTheSpecificStudentForSchool($codeEtabLocal,$_POST['classeEtab'],$_POST['eleveid']);
               // $datas=$student->getEmailParentOfThisStudentByID($_POST['eleveid'],$libellesessionencours);
               // $tabdatas=explode("*",$datas);
               $matriculesStudent=$student->getstudentMatriculebyid($_POST['eleveid']);

               echo $matriculesStudent;


               // getTheSpecificStudentForSchool($compteEtab,$classeId,$idstudent)

              ///var_dump($students);

          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::Reacpsdesabsences ?></h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php //echo @$infosclasses; ?></p>
              <p class="card-text" style="text-align:center;font-weight: bold"><?php echo $_POST['datedeb'].' au '.$_POST['datefin'] ?></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card card-box">
                  <div class="card-head">
                      <header></header>
                      <div class="tools">
                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                      </div>
                  </div>
                  <div class="card-body ">
                    <div class="pull-right">
                      <a href="#" class="btn btn-md btn-warning" onclick="recapattendanceDay('<?php echo $_POST['datedeb']; ?>','<?php echo $nb;  ?>',<?php echo $_POST['classeEtab']  ?>,'<?php echo $libellesessionencours ?>','<?php echo $_SESSION['user']['codeEtab']  ?>','<?php echo $matriculesStudent ?>')"><i class="fa fa-print"></i>Imprimer </a>
                    </div>
                      <table id="tableGroup" class="display" style="width: 100%">
          <thead>
              <tr>
                <th><?php echo L::MatriculestudentTab ?></th>
                <th><?php echo L::NamestudentTab ?></th>
                <th><?php echo L::Office ?></th>
                <th><?php echo L::Etat ?></th>
                <th><?php echo L::HoursStart ?></th>
                <!-- <th><?php //echo L::HoursEnd ?></th> -->
              </tr>
          </thead>
          <tbody>
            <?php
            echo $_SESSION['user']['codeEtab'];
            for($x=0;$x<=$nb;$x++)
            {
                $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($_POST['datedeb'])));

                echo $libellesessionencours;

                //la liste des absences du jour
                //nb absences du jour

                $nbabsebnces=$student->getAbsencesthisdaynbStudent($dateday,$_SESSION['user']['codeEtab'],$libellesessionencours,$_POST['classeEtab'],$matriculesStudent);

                if($nbabsebnces>0)
                {
                  //nous allons chercher la liste

                  $datas=$student->getlisteabsencesdayStudents($dateday,$_SESSION['user']['codeEtab'],$libellesessionencours,$_POST['classeEtab'],$matriculesStudent);

                  foreach ($datas as $value):
                    ?>
                    <tr>
                      <td><?php echo $value->matricule_presence ?></td>
                      <td><?php echo $value->nom_eleve." ".$value->prenom_eleve ?></td>
                      <td><?php echo date_format(date_create($dateday), "d-m-Y"); ?></td>
                      <td><?php
                        if($value->statut_presence==0)
                        {
                          echo L::Absent;
                        }else {
                          echo L::RetardLibelle;
                        }
                       ?></td>
                      <td>
                        <?php
                          if($value->statut_presence==0)
                          {
                            // echo L::Absent;
                          }else {
                            echo $value->retardh_presence;
                          }
                         ?>
                      </td>
                      <!-- <td><?php //echo returnHours($value->heurefin_heure); ?></td> -->
                    </tr>
                    <?php
                  endforeach;

                }
            }
             ?>



          </tbody>
      </table>
                  </div>
              </div>
          </div>


          <?php
              }
          ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
<?php
  if(isset($_POST))
  {
    ?>

<?php
  }
 ?>

 function searstudent()
 {
   var classe=$("#classeEtab").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
   var etape=3;

   $.ajax({
     url: '../ajax/admission.php',
     type: 'POST',
     async:false,
     data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classe,
     dataType: 'text',
     success: function (response, statut) {

         $("#eleveid").html("");

         $("#eleveid").html(response);
     }
   });

 }

 var date = new Date();
var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

 $('#datedeb').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 $('#datefin').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
   cancelText: '<?php echo L::AnnulerBtn ?>',
   okText: '<?php echo L::Okay ?>',
   clearText: '<?php echo L::Eraser ?>',
   nowText: '<?php echo L::Now ?>'

 });

 function recapattendanceDay(datedeb,nb,classeEtab,session,codeEtab,matricule)
 {
     // var etape=8;
     // $.ajax({
     //   url: '../ajax/etat.php',
     //   type: 'POST',
     //   async:false,
     //   data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classeEtab='+classeEtab+'&datedeb='+datedeb+'&nb='+nb+'&matricule='+matricule,
     //   dataType: 'text',
     //   success: function (response, statut) {
     //
     //       window.open(response, '_blank');
     //
     //   }
     // });

     var url='recapclasseliste.php?codeEtab='+codeEtab+'&session='+session+'&classeEtab='+classeEtab+'&datedeb='+datedeb+'&nb='+nb+'&matricule='+matricule;

     window.open(url, '_blank');
 }


function recapattendance(moisconcat,annee,classeEtab,session,codeEtab)
{
  var etape=4;
  $.ajax({
    url: '../ajax/etat.php',
    type: 'POST',
    async:false,
    data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classeEtab='+classeEtab+'&moisconcat='+moisconcat+'&annee='+annee,
    dataType: 'text',
    success: function (response, statut) {

        window.open(response, '_blank');

    }
  });
}

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#month").select2();
   $("#yearsession").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
     cancelText: '<?php echo L::AnnulerBtn ?>',
     okText: '<?php echo L::Okay ?>',
     clearText: '<?php echo L::Eraser ?>',
     nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

//
$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required",
    datedeb:"required",
    datefin:"required",
    eleveid:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selection l'année de session",
    datedeb:"<?php echo L::PleaseEnterDatedebLib ?>",
    datefin:"<?php echo L::PleaseEnterDatefinLib ?>",
    eleveid:"<?php echo L::PleaseSelectOneStudent ?>"

  },
  submitHandler: function(form) {
    form.submit();
  }
});




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
