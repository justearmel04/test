<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
// $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$examens=$etabs->getAllexamensOfSchool($codeEtabAssigner);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  $matieres=$matiere->getAllControleMatiereOfThisSchool($codeEtabAssigner,$libellesessionencours);
}
  //recuperation des variables get

  // message=7&parascolaire=1&precis=1&addby=2

  $messagedid=htmlspecialchars(addslashes($_GET['message']));
  $parascolaire=htmlspecialchars(addslashes($_GET['parascolaire']));
  $precis=htmlspecialchars(addslashes($_GET['precis']));
  $addby=htmlspecialchars(addslashes($_GET['addby']));


  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);


  $onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
  $offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::EtatNotification ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>

                              <li class="active"><?php echo L::EtatNotification ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addlocalok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: '<?php echo L::Felicitations ?>',
  text: "<?php echo $_SESSION['user']['addlocalok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::AddNews ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addlocalok']);
                    }

                     ?>
<br/>

                </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                  <div class="card card-box">
                      <div class="card-head">
                          <header></header>

                      </div>


                      <div class="card-body" id="bar-parent">
                          <form  id="FormAddLocalAd" class="form-horizontal" action="#" method="post" >
                              <div class="form-body">

                                <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::Denomination ?>
                                         <span class="required"> * </span>
                                     </label>
                                     <div class="col-md-5">
                                         <input type="text" name="libelle" id="libelle" data-required="1" placeholder="" class="form-control input-height"  readonly /> </div>
                                 </div>
                                 <div class="form-group row">
                                     <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                         <span class="required"> * </span>
                                     </label>

                                     <div class="col-md-5">
                                       <select name="classechoice[]" id="classechoice" multiple="multiple"  class="form-control input-height" disabled>

                                       </select>

                                     </div>
                                     <div class="col-md-4" id='allclassescontener' style="display:none">
                                         <button type="button" name="button" class="btn btn-success" id="buttonallclasses" onclick="selectallclasses()"><?php echo L::AllClassesMenu ?></button>
                                     </div>
                                 </div>
                                 <div class="form-group row">
                                   <label class="control-label col-md-3"><?php echo L::studMenu ?>
                                       <span class="required"> * </span>
                                   </label>
                                   <div class="col-md-5">
                                     <select name="studentchoice[]" id="studentchoice" multiple="multiple" class="form-control input-height">

                                     </select>
                                   </div>
                                   <div class="col-md-4" id="allstudentcontener" style="display:none">
                                       <button type="button" name="button" class="btn btn-success" id="buttonallstudent" onclick="selectallstudent()"><?php echo L::Tousleseleves ?></button>
                                   </div>
                                   </div>






              <div class="form-actions">
                                  <div class="row">
                                      <div class="offset-md-3 col-md-9">
                                          <button type="button" class="btn btn-info" onclick="afficherEtat()"><?php echo L::DisplayEtat ?></button>
                                          <button type="button" class="btn btn-danger"><?php echo L::TurnBack ?></button>
                                      </div>
                                    </div>
                                 </div>
              </div>
                          </form>
                      </div>
                  </div>
              </div>

            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
       <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
  <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
  <script src="../assets2/js/dropify.js"></script>

 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript">
  // function SetcodeEtab(codeEtab)
  // {
  //   var etape=3;
  //   $.ajax({
  //     url: '../ajax/sessions.php',
  //     type: 'POST',
  //     async:false,
  //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
  //     dataType: 'text',
  //     success: function (content, statut) {
  //
  // window.location.reload();
  //
  //     }
  //   });
  // }
  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function afficherEtat()
  {
    var classe=$("#classechoice").val();
    var session="<?php echo $libellesessionencours ?>";
    var codeEtab="<?php echo $codeEtabAssigner ?>";
    var eleves=$("#studentchoice").val();
    var message="<?php echo $_GET['message']; ?>";
    var addby="<?php echo $_GET['addby']; ?>";
    var precis="<?php echo $_GET['precis']; ?>";

    var etape=6;

    $.ajax({
      url: '../ajax/message.php',
      type: 'POST',
      async:false,
      data: 'classe=' + classe+ '&session=' + session +'&codeetab=' + codeEtab + '&etape=' + etape+'&eleves='+eleves+'&message='+message+'&addby='+addby+'&precis='+precis,
      dataType: 'text',
      success: function (content, statut) {

        // alert(content);
        window.open(content);

      }
    });


  }

  function selectallclasses()
  {
    $("#classechoice > option").prop("selected",true).trigger("change");

    var ligne="<button type=\"button\" name=\"button\" class=\"btn btn-danger\" id=\"buttonanyclasses\" onclick=\"deselectallclasses()\"><?php echo L::AllClassesMenu ?></button>";
    $("#buttonallclasses").remove();
    $("#allclassescontener").append(ligne);

  }

  function deselectallclasses()
  {
    $("#classechoice > option").prop("selected",false).trigger("change");
    var ligne="<button type=\"button\" name=\"button\" class=\"btn btn-success\" id=\"buttonallclasses\" onclick=\"selectallclasses()\"><?php echo L::AllClassesMenu ?></button>";
    $("#buttonanyclasses").remove();
    $("#allclassescontener").append(ligne);
  }

  function selectallstudent()
  {
    $("#studentchoice > option").prop("selected",true).trigger("change");
    var ligne="<button type=\"button\" name=\"button\" class=\"btn btn-danger\" id=\"buttonanystudent\" onclick=\"deselectallstudent()\"><?php echo L::Tousleseleves ?></button>";
      $("#buttonallstudent").remove();
      $("#allstudentcontener").append(ligne);
  }

  function deselectallstudent()
  {
    $("#studentchoice > option").prop("selected",false).trigger("change");
    var ligne="<button type=\"button\" name=\"button\" class=\"btn btn-success\" id=\"buttonallstudent\" onclick=\"selectallstudent()\"><?php echo L::Tousleseleves ?></button>";
      $("#buttonanystudent").remove();
      $("#allstudentcontener").append(ligne);
  }

  $("#classechoice").select2({
    placeholder: "<?php echo L::Selectclasses ?>",
    allowClear: true
  });
  $("#studentchoice").select2({
    placeholder: "<?php echo L::SelectOneStudent ?>",
    allowClear: true
  });

  jQuery(document).ready(function() {

//determiner la denomination

  var message="<?php echo $_GET['message']; ?>";
  var addby="<?php echo $_GET['addby']; ?>";
  var parascolaire="<?php echo $_GET['parascolaire']; ?>";
  var precis="<?php echo $_GET['precis']; ?>";
  var addby="<?php echo $_GET['addby']; ?>";
  var session="<?php echo $libellesessionencours ?>";
  var codeEtab="<?php echo $codeEtabAssigner ?>";
  var etape=6;

  $.ajax({
    url: '../ajax/etat.php',
    type: 'POST',
    async:false,
    data: 'message=' + message+ '&parascolaire=' + parascolaire +'&precis=' + precis + '&etape=' + etape+'&addby='+addby,
    dataType: 'text',
    success: function (content, statut) {

      var tabdata=content.split("/");
      var denomination=tabdata[0];
      var classeselected=tabdata[1];

      $("#libelle").val("");
      $("#libelle").val(denomination);

      //nous allons determiner les classes selectionner
      var etape1=8;
      var etape2=9;

      $.ajax({
        url: '../ajax/classe.php',
        type: 'POST',
        async:false,
        data: 'classe=' + classeselected+ '&session=' + session +'&codeetab=' + codeEtab + '&etape=' + etape1,
        dataType: 'text',
        success: function (content, statut) {

          $("#classechoice").html("");
          $("#classechoice").html(content);

           selectallclasses();

             $.ajax({
               url: '../ajax/classe.php',
               type: 'POST',
               async:false,
               data: 'classe='+ classeselected+'&session=' + session +'&codeetab=' + codeEtab + '&etape=' + etape2+'&precis='+precis+'&parascolaire='+parascolaire+'&message='+message,
               dataType: 'text',
               success: function (content, statut) {
                 $("#studentchoice").html("");
                 $("#studentchoice").html(content);

                 selectallstudent();
               }
             });

        }
      });


    }
  });



  });

  </script>

    <!-- end js include path -->
  </body>

</html>
