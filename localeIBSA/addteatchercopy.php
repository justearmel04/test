<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::AddaTeatcher ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#"><?php echo L::ProfsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::AddaTeatcher ?></li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->

          <?php

                if(isset($_SESSION['user']['addteaok']))
                {

                  ?>

  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            // alert("bonjour");
            Swal.fire({
title: '<?php echo L::Felicitations ?> ',
text: "<?php echo $_SESSION['user']['addteaok']; ?>",
type: 'success',
showCancelButton: false,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
// confirmButtonText: 'Ajouter les diplomes',
cancelButtonText: '<?php echo L::Okay ?>',
}).then((result) => {
if (result.value) {
// document.location.href="adddiplomes.php?compte="+<?php //echo $_GET['compte'] ?>;
}else {
// document.location.href="index.php";
}
})
            </script>
                  <?php
                  unset($_SESSION['user']['addteaok']);
                }

                 ?>


                 <div class="row">
                     <div class="col-md-12 col-sm-12">
                         <div class="card card-box">
                             <div class="card-head">
                                 <header><?php echo L::TeatcherInfos ?>r</header>
                                  <!--button id = "panel-button"
                                class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                data-upgraded = ",MaterialButton">
                                <i class = "material-icons">more_vert</i>
                             </button>
                             <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                data-mdl-for = "panel-button">
                                <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                             </ul-->
                             </div>

                             <div class="card-body" id="bar-parent">
                                 <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                     <div class="form-body">
                                       <div class="row">
                       <div class="col-md-12">

                 <fieldset style="background-color:#007bff;">

                 <center><legend style="color:white;"><?php echo L::TeatcherInfosPerso ?></legend></center>
                 </fieldset>


                 </div>
                     </div>
                     <br/>
                     <div class="row">
                                         <div class="col-md-6">
                     <div class="form-group">
                     <input type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-show-errors="true" data-errors-position="outside" data-default-file="../photo/user5.jpg" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                     <label for=""><b>Image: </b></label>
                     </div>

                     </div>


                                   </div>
                     <div class="row">
                                         <div class="col-md-6">
                     <div class="form-group">
                     <label for=""><b><?php echo L::Name ?><span class="required"> * </span>: </b></label>

                   <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control" />
                     </div>

                     </div>
                     <div class="col-md-6">
                          <div class="form-group">
                            <label for=""><b><?php echo L::PreName ?> <span class="required"> * </span>: </b></label>
                             <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control" />
                           </div>


                      </div>

                                   </div>
                                   <div class="row">
                                                       <div class="col-md-6">
                                   <div class="form-group">
                                   <label for=""><b><?php echo L::BirthstudentTab ?>: </b></label>

                                   <input type="text" placeholder="<?php echo L::EnterBirthstudentTab ?>" name="datenaisTea" id="datenaisTea"  class="form-control ">
                                     <span class="help-block"><?php echo L::Datesymbole ?></span>
                                   </div>

                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                          <label for=""><b><?php echo L::BirthLocation ?>: </b></label>
                                           <input type="text" name="lieunaisTea" id="lieunaisTea" data-required="1" placeholder="<?php echo L::BirthLocation ?>" class="form-control " />
                                         </div>


                                    </div>

                                                 </div>
                                                 <div class="row">
                                                                     <div class="col-md-6">
                                                 <div class="form-group">
                                                 <label for=""><b><?php echo L::Gender ?> : </b></label>

                                                 <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                                     <option selected value=""><?php echo L::SelectGender ?></option>
                                                     <option value="M"><?php echo L::SexeM ?></option>
                                                     <option value="F"><?php echo L::SexeF ?></option>
                                                 </select>
                                                 </div>

                                                 </div>
                                                 <div class="col-md-6">
                                                   <div class="form-group">
                                                   <label for=""><b><?php echo L::EmailstudentTab ?> : </b></label>

                                                 <input type="text" class="form-control " name="emailTea" id="emailTea" placeholder="<?php echo L::EnterEmailAdress ?>">
                                                   </div>


                                                  </div>

                                                               </div>
                                                               <div class="row">
                                                                 <div class="col-md-6">
                                             <div class="form-group">
                                             <label for=""><b><?php echo L::Nationalite ?> : </b></label>

                                       <input name="natTea" id="natTea" type="text" placeholder="<?php echo L::EnterNationalite ?> " class="form-control " />
                                             </div>

                                             </div>
                                                                 <div class="col-md-6">
                                                                      <div class="form-group">
                                                                        <label for=""><b><?php echo L::PhonestudentTab ?> </b></label>
                                                                       <input name="contactTea" id="contactTea" type="text" placeholder="<?php echo L::EnterPhoneNumber ?> " class="form-control" />
                                                                       </div>


                                                                  </div>



                                                                             </div>
                                                                             <div class="row">
                                                                                                 <div class="col-md-6">
                                                                             <div class="form-group">
                                                                             <label for=""><b><?php echo L::MatrimonialeSituation ?> : </b></label>

                                                                             <select class="form-control " name="situationTea" id="situationTea">

                                                                                 <option selected value=""><?php echo L::SelectMatrimonialeSituation ?></option>
                                                                                 <option value="CELIBATAIRE"><?php echo L::Celib ?></option>
                                                                                 <option value="MARIE(E)"><?php echo L::Mariee ?></option>
                                                                                 <option value="DIVORCE(E)"><?php echo L::Divorcee ?></option>
                                                                                 <option value="VEUF(VE)"><?php echo L::Veufvee ?></option>
                                                                             </select>
                                                                             </div>

                                                                             </div>
                                                                             <div class="col-md-6">
                                                                                  <div class="form-group">
                                                                                    <label for=""><b><?php echo L::ChildNb ?></b></label>
                                                                                   <input name="nbchildTea" id="nbchildTea" type="text"  placeholder="<?php echo L::EnterChildNb ?> " class="form-control" onchange="addchild()" value="0" />

                                                                                   <input type="hidden" name="oldnumber" id="oldnumber" value="0">
                                                                                   <input type="hidden" name="newnumber" id="newnumber" value="0">
                                                                                   </div>


                                                                              </div>
                                                                              <div class="col-md-6">
                                                          <div class="form-group">
                                                          <label for=""><b><?php echo L::TeatcherType ?>: </b></label>

                                                          <select class="form-control " name="typeTea" id="typeTea">

                                                              <option selected value=""><?php echo L::SelectTeatcherType ?></option>
                                                              <option value="1"><?php echo L::Titulaire ?></option>
                                                              <option value="2"><?php echo L::Interimaire ?></option>
                                                          </select>
                                                          </div>

                                                          </div>
                                                          <?php
                                                            if($etablissementType==1||$etablissementType==3)
                                                            {
                                                           ?>
                                                           <div class="col-md-6">
                                       <div class="form-group">
                                       <label for=""><b><?php echo L::AffectationClasse ?> : </b></label>

                                       <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%">
                                           <option value=""><?php echo L::Selectclasses ?></option>
                                           <?php
                                           $i=1;
                                             foreach ($classes as $value):
                                             ?>
                                             <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                             <?php
                                                                              $i++;
                                                                              endforeach;
                                                                              ?>

                                       </select>
                                       </div>

                                       </div>
                                                           <?php
                                                         }
                                                            ?>
                                                                                           </div>
                                                                                           <div class="row" id="infantilesRow">
                                                                           <div class="col-md-12">

                                                                 <fieldset style="background-color:#007bff;">

                                                                 <center><legend style="color:white;"><?php echo L::ChildsInfos ?></legend></center>
                                                                 </fieldset>


                                                                 </div>

                                                               </div><br/></br>
                                                               <div id="dynamic_field">

                                                               </div>
                                                                                           <div class="row">
                                                                           <div class="col-md-12">

                                                                 <fieldset style="background-color:#007bff;">

                                                                 <center><legend style="color:white;"><?php echo L::ProfessionalInfos ?></legend></center>
                                                                 </fieldset>


                                                                 </div>
                                                               </div><br/>
                                                                         <div class="row">
                                                                                             <div class="col-md-6">
                                                                         <div class="form-group">
                                                                         <label for=""><b><?php echo L::Cnps ?>: </b></label>

                                                                       <input type="text" name="cnpsTea" id="cnpsTea" data-required="1" placeholder="<?php echo L::EnterCnps ?>" class="form-control" />
                                                                         </div>

                                                                         </div>
                                                                         <div class="col-md-6">
                                                                              <div class="form-group">
                                                                                <label for=""><b><?php echo L::MatriculestudentTab ?>: </b></label>
                                                                                 <input type="text" name="matTea" id="matTea" data-required="1" placeholder="<?php echo L::EnterMatriculestudentTab ?>" class="form-control" />
                                                                               </div>


                                                                          </div>

                                                                                       </div>
                                                                                       <div class="row">
                                                                                                           <div class="col-md-6">
                                                                                       <div class="form-group">
                                                                                       <label for=""><b><?php echo L::Mutuelle ?>: </b></label>

                                                                                     <input type="text" name="mutuelTea" id="mutuel" data-required="1" placeholder="<?php echo L::EnterMutuelle ?>" class="form-control" />
                                                                                       </div>

                                                                                       </div>
                                                                                       <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                              <label for=""><b><?php echo L::EmbaucheDate ?> : </b></label>
                                                                                              <input type="text" placeholder="<?php echo L::EnterEmbaucheDate ?>" name="dateEmbTea" id="dateEmbTea" data-mask="99/99/9999" class="form-control">
                                                                                                <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                                                             </div>


                                                                                        </div>

                                                                                                     </div>
                                                                                                     <div class="row">


                                                                                                                   </div>
                                                                                                                   <div class="row">
                                                                                                   <div class="col-md-12">

                                                                                         <fieldset style="background-color:#007bff;">

                                                                                         <center><legend style="color:white;"><?php echo L::AccountInfos ?></legend></center>
                                                                                         </fieldset>


                                                                                         </div>
                                                                                       </div><br/>


                                                                                           <div class="form-group row">
                                                                                               <label class="control-label col-md-3"><?php echo L::Logincnx ?>

                                                                                               </label>
                                                                                               <div class="col-md-5">
                                                                                                   <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control" />
                                                                                                   <input type="hidden" id="libetab" name="libetab" value="<?php echo $codeEtabAssigner?>"/>
                                                                                                   <input type="hidden" name="typeetablissement" id="typeetablissement" value="<?php echo $etablissementType; ?>">
                                                                                                   <input type="hidden" name="netTea" id="netTea" value="0">
                                                                                                   <input type="hidden" name="brutTea" id="brutTea" value="0">
                                                                                                   <input type="hidden" name="session" id="session" value="<?php echo $libellesessionencours; ?>">
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="form-group row">
                                                                                               <label class="control-label col-md-3"><?php echo L::Passcnx ?>

                                                                                               </label>
                                                                                               <div class="col-md-5">
                                                                                                   <input name="passTea" id="passTea" type="password" placeholder="<?php echo L::PasswordEnter ?> " class="form-control " /> </div>
                                                                                           </div>
                                                                                           <div class="form-group row">
                                                                                               <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx ?>

                                                                                               </label>
                                                                                               <div class="col-md-5">
                                                                                                   <input name="confirmTea" id="confirmTea" type="password" placeholder="<?php echo L::ConfirmPassword ?> " class="form-control" /> </div>
                                                                                                   <input type="hidden" name="etape" id="etape" value="5"/>
                                                                                                   <input type="hidden" name="comptead" id="comptead" value="ENSEIGNANT"/>
                                                                                           </div>
                                                                                           <div class="form-actions">
                                                                                                                 <div class="row">
                                                                                                                     <div class="offset-md-3 col-md-9">
                                                                                                                         <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                                                                                         <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                                                                     </div>
                                                                                                                   </div>
                                                                                                                </div>

                                     </div>
                                 </form>
                             </div>
                         </div>
                     </div>
                 </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <script src="../assets2/js/pages/material_select/getmdl-select.js" ></script>
   <script src="../assets/js/formatter/jquery.formatter.min.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>


 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
 <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
 <!-- Common js-->
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

    <script>
    function SetcodeEtab(codeEtab)
    {
      var etape=3;
      $.ajax({
        url: '../ajax/sessions.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }
    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    $("#sexeTea").select2();
    $("#situationTea").select2();
    $("#typeTea").select2();
    $("#classeEtab").select2();

    function addchild()
    {
      var nbchildTea=$("#nbchildTea").val();
      if(nbchildTea==0||nbchildTea=="")
      {
        // $('#dynamic_field').append("");
        var oldnumber=$("#oldnumber").val();

        for(var i=1;i<=oldnumber;i++)
        {

          $("#libelle"+i).remove();
          $("#childrow"+i).remove();

        }

          $("#infantilesRow").hide();

      }else if(nbchildTea>0)
      {
        var oldnumber=$("#oldnumber").val();

        for(var i=1;i<=oldnumber;i++)
        {
          // $("#infantilesRow").hide();
          $("#libelle"+i).remove();
          $("#childrow"+i).remove();

        }


        $("#infantilesRow").show();
        for(var i=1;i<=nbchildTea;i++)
        {
          // var ligne="<div class=\" form-grouprow\" id=\"ligneEnteteold"+i+"\">";
          // ligne=ligne+"<label class=\"control-label col-md-7\" style=\"color:#e40d22\">INFORMATIONS ENFANT "+i+" <span class=\"required\">  </span></label>";
          // ligne=ligne+"</div>";

          $('#dynamic_field').append("");

          var ligne="<div class=\"row\" id=\"libelle"+i+"\">";
              ligne=ligne+"<div class=\"col-md-12\" style=\"text-align:center\">";
              ligne=ligne+"<span class=\"label label-lg label-info\" style=\"text-align:center\"><?php echo L::ChildInfos ?> "+i+"</span>";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";
              ligne=ligne+"<div class=\"row\" id=\"childrow"+i+"\">";

              ligne=ligne+"<div class=\"col-md-6\">";
              ligne=ligne+"<div class=\"form-group\">";
              ligne=ligne+"<label for=\"\"><b>Nom & prénoms: </b></label>";
              ligne=ligne+"<input type=\"text\" name=\"nomcomplet"+i+"\" id=\"nomcomplet"+i+"\" data-required=\"1\" placeholder=\"<?php echo L::EnterChildName ?>\" class=\"form-control\" />";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";

              ligne=ligne+"<div class=\"col-md-6\">";
              ligne=ligne+"<div class=\"form-group\">";
              ligne=ligne+"<label for=\"\"><b>Date de naissance : </b></label>";
              ligne=ligne+"<input type=\"text\" name=\"datenais"+i+"\" id=\"datenais"+i+"\" data-required=\"1\" placeholder=\"<?php echo L::EnterBirthstudentTab ?>\" class=\"form-control\" />";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";

              ligne=ligne+"<div class=\"col-md-6\">";
              ligne=ligne+"<div class=\"form-group\">";
              ligne=ligne+"<label for=\"\"><b>Lieu de naissance : </b></label>";
              ligne=ligne+"<input type=\"text\" name=\"lieunais"+i+"\" id=\"lieunais"+i+"\" data-required=\"1\" placeholder=\"<?php echo L::EnterBirthLocation ?>\" class=\"form-control\" />";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";

              ligne=ligne+"<div class=\"col-md-6\">";
              ligne=ligne+"<div class=\"form-group\">";
              ligne=ligne+"<label for=\"\"><b>Genre : </b></label>";
              ligne=ligne+"<select class=\"form-control \" id=\"sexe"+i+"\" name=\"sexe"+i+"\" style=\"width:100%;text-align:center\">";
              ligne=ligne+"<option value=\"\"><?php echo L::SelectGender ?></option>";
              ligne=ligne+"<option value=\"M\"><?php echo L::SexeM ?></option>";
              ligne=ligne+"<option value=\"F\"><?php echo L::SexeF ?></option>";

              ligne=ligne+"</select>";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";


              ligne=ligne+"<div class=\"col-md-6\">";
              ligne=ligne+"<div class=\"form-group\">";
              ligne=ligne+"<label for=\"\"><b><?php echo L::BirthPaper ?> : </b></label>";
              ligne=ligne+"<input type=\"file\" name=\"extraitnais"+i+"\" id=\"extraitnais"+i+"\" class=\"default\" class=\"dropify\" data-show-errors=\"true\" data-errors-position=\"outside\" data-show-loader=\"true\" data-max-file-size=\"1mb\" data-default-file=\"../photo/images_files1.jpg\" data-allowed-file-extensions=\"gif png jpg jpeg pjpeg pdf docx doc\" />";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";
              ligne=ligne+"</div>";




              ligne=ligne+"</div>";

          //bouton de suppression


          $('#dynamic_field').append(ligne);

          $('#nomcomplet'+i).rules( "add", {
              required: true,
              messages: {
              required: "<?php echo L::PleaseEnterChildName ?>"
      }
            });

            $('#datenais'+i).rules( "add", {
                required: true,
                messages: {
                required: "<?php echo L::PleaseEnterPhonestudentTab ?>"
        }
              });

              $('#lieunais'+i).rules( "add", {
                  required: true,
                  messages: {
                  required: "<?php echo L::PleaseEnterBirthLocation ?>"
          }
                });

                $('#sexe'+i).rules( "add", {
                    required: true,
                    messages: {
                    required: "<?php echo L::PleaseSelectGender ?>"
            }
                  });

                  $("#sexe"+i).select2();

                  $("#extraitnais"+i).dropify({
                    messages: {
                        "default": "<?php echo L::PleaseSelectBirthPaper ?>",
                        "replace": "<?php echo L::ChangeFileBirthPaper ?>",
                        "remove" : "<?php echo L::removeFileBirthPaper ?>",
                        "error"  : "<?php echo L::Error ?>"
                    }
                  });

            $('#datenais'+i).bootstrapMaterialDatePicker
            ({
              date:true,
              shortTime: false,
              time:false,
              format: 'DD/MM/YYYY',
              lang: 'fr',

             okText: '<?php echo L::Okay ?>',
             clearText: '<?php echo L::Eraser ?>',
             // nowText: '<?php echo L::Now ?>'
             weekStart : 1,
             cancelText : '<?php echo L::AnnulerBtn ?>'

            });
        }

        $("#oldnumber").val(nbchildTea);
      }
    }



    jQuery(document).ready(function() {

      $("#datenaisTea").formatter({pattern:"{{99}}-{{99}}-{{9999}}"});
      $("#contactTea").formatter({pattern:"{{99}}{{99}}{{99}}{{99}}"});
      $("#nbchildTea").formatter({pattern:"{{999}}"});

      $("#infantilesRow").hide();

      $("#FormAddLocalAd").validate({

        errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      },
      success: function (e) {
            $(e).closest('.control-group').removeClass('error').addClass('info');
            $(e).remove();
        },
         rules:{
           // lieunaisTea:"required",
   // natTea:"required",
   // situationTea:"required",
   // nbchildTea:"required",
   /*cnpsTea:"required",
   matTea:"required",
   mutuelTea:"required",*/
   // brutTea:"required",
   // netTea:"required",
       classeEtab:"required",
       typeTea:"required",
           passTea: {
             required:function(element){
                     return $("#confirmTea").val()!="" && $("#loginTea").val()!="";
                 },
               minlength: 6
           },
           confirmTea:{
               required:function(element){
                       return $("#passTea").val()!="" && $("#loginTea").val()!="";
                   },
               minlength: 6,
               equalTo:'#passTea'
           },
           // fonctionad:"required",

           // loginTea:"required",
           loginTea: {
    required: function(element){
            return $("#passTea").val()!="" && $("#confirmTea").val()!="";
        }
},
           // emailTea: {
           //            required: true,
           //            email: true
           //        },
           contactTea:{
                   required: true,
                   digits:true,
                   minlength: 8,
                   maxlength:12,
               },
           // datenaisTea:"required",
           prenomTea:"required",
           nomTea:"required",
           // gradeTea:"required",
           // dateEmbTea:"required",
           sexeTea:"required",
           libetab:"required"




         },
         messages: {
           lieunaisTea:"<?php echo L::PleaseEnterBirthLocation ?>",
           natTea:"<?php echo L::PleaseEnterNationalite ?>",
           situationTea:"<?php echo L::PleaseSelectMatrimonialeSituation ?>",
           nbchildTea:"<?php echo L::PleaseEnterChildNb ?>",
           cnpsTea:"<?php echo L::PleaseEnterCnps ?>",
           matTea:"<?php echo L::PleaseEnterMatriculeTea ?>",
           mutuelTea:"<?php echo L::PleaseEnterMutuelleTea ?>",
           brutTea:"<?php echo L::PleaseEnterSalaireBrutTea ?>",
           netTea:"<?php echo L::PleaseEnterSalaireNetTea ?>",
           confirmTea:{
               required:"<?php echo L::Confirmcheck ?>",
               minlength:"<?php echo L::Confirmincheck ?>",
               equalTo: "<?php echo L::ConfirmSamecheck ?>"
           },
           passTea: {
               required:"<?php echo L::Passcheck ?>",
               minlength:"<?php echo L::Confirmincheck ?>"
           },
           loginTea:"<?php echo L::Logincheck ?>",
           emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
           contactTea:{
             required: "<?php echo L::PleaseEnterPhoneNumber ?>",
             minlength: "<?php echo L::PhoneNumberCaracteres ?>",
             digits: "<?php echo L::PleaseEnterDigitsOnly ?> ",
             maxlength: "<?php echo L::NumbercaractersMax ?>",
           },
           datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
           prenomTea:"<?php echo L::PleaseEnterPrename ?>",
           nomTea:"<?php echo L::PleaseEnterName ?> ",
           fonctionad:"<?php echo L::PleaseEnterFonction ?>",
           gradeTea:"<?php echo L::EnterGrade ?>",
           dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
           sexeTea:"<?php echo L::PleaseEnterSexe ?>",
           libetab:"<?php echo L::PleaseSelectSchoolEnseignement ?>",
           classeEtab:"<?php echo L::PleaseEnterAffectationClasse ?>",
           typeTea:"<?php echo L::PleaseEnterTeatcherType ?>",
         },
         submitHandler: function(form) {
           //verifier si ce compte n'existe pas encore dans la base de données
              var etape=1;
              $.ajax({
                url: '../ajax/teatcher.php',
                type: 'POST',
                async:false,
                data: 'login=' + $("#loginTea").val()+ '&email=' + $("#emailTea").val()+'&codeetab=' + $("#libetab").val()+'&etape=' + etape,
                dataType: 'text',
                success: function (content, statut) {


                  if(content==0)
                  {
                    //le compte n'existe pas dans la base on peut l'ajouter

                    form.submit();
                  }else if(content==1) {
                    //le compte existe dejà dans la base de données
                    form.submit();
                    // Swal.fire({
                    // type: 'warning',
                    // title: '<?php echo L::WarningLib ?>',
                    // text: 'Cet Enseignant existe dejà dans la base de données',
                    //
                    // })

                  }else if(content==2) {

                    Swal.fire({
      title: '<?php echo L::WarningLib ?>',
      text: "<?php echo L::TeatcherAllreadyExist ?>",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<?php echo L::AssignerLe ?>',
      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
    }).then((result) => {
      if (result.value) {
         $("#etape").val(2);

         form.submit();
      }else {

      }
    })


                  }
                  /*
                  if(content==0)
                  {
                    //le compte n'existe pas dans la base on peut l'ajouter

                    form.submit();
                  }else {
                    //le compte existe dejà dans la base de données
                    Swal.fire({
                    type: 'warning',
                    title: '<?php echo L::WarningLib ?>',
                    text: 'Cet Enseignant existe dejà dans la base de données',

                    })

                  }*/

                }


              });
         }


      });


    });
    </script>
    <!-- end js include path -->
  </body>

</html>
