<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Classe.php');
$classe=new Classe();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$pays=$etabs->getAllCountriesOfSystem();
//$allclasses=$etabs->getAllclasses();
//la liste des établissements qui ne sont pas dans un groupe

$etabsnongrouper=$etabs->getAllEtabsnongroupe();

//liste des groupes

$groupelistes=$etabs->getAllgroupeListes();

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Gestion des groupes d'établissements</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Etablissements</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Gestion des groupes d'établissements</li>
                            </ol>
                        </div>
                    </div>

                    <?php

                          if(isset($_SESSION['user']['addclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['addclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['addclasseok']);
                          }

                           ?>

                    <?php

                          if(isset($_SESSION['user']['updateclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['updateclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['updateclasseok']);
                          }

                           ?>



                    <?php

                          if(isset($_SESSION['user']['delclasseok']))
                          {

                            ?>
                            <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                          <?php
                          //echo $_SESSION['user']['addetabok'];
                          ?>
                          <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                             </a>
                          </div-->
                  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                  <script src="../assets/js/sweetalert2.min.js"></script>

                      <script>
                      Swal.fire({
                      type: 'success',
                      title: 'Félicitations',
                      text: '<?php echo $_SESSION['user']['delclasseok']; ?>',

                      })
                      </script>
                            <?php
                            unset($_SESSION['user']['delclasseok']);
                          }

                           ?>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <div class="row">

                         <div class="col-md-12 col-sm-12">
                              <div class="panel tab-border card-box">
                                 <header class="panel-heading panel-heading-gray custom-tab">
                                     <ul class="nav nav-tabs">

                                         <li class="nav-item" >
                                             <a href="#about-2" data-toggle="tab" class="active">
                                                 <i class="fa fa-list"></i> Liste des groupes
                                             </a>
                                         </li>
                                         <li class="nav-item">
                                             <a href="#contact-2" data-toggle="tab">
                                                 <i class="fa fa-plus"></i> Nouveau groupe
                                             </a>
                                         </li>
                                     </ul>
                                 </header>
                                 <div class="panel-body">
                                     <div class="tab-content">

                                         <div class="tab-pane active" id="about-2">
                                           <div class="table-scrollable">
                                             <table class="table table-hover table-checkable order-column full-width" id="example4">
                                                 <thead>
                                                     <tr>

                                                         <th style="text-align:center">N°</th>
                                                         <th style="text-align:center"> Groupes </th>
                                                         <th style="text-align:center;width:100px"> Nb Etablissements </th>
                                                        <th style="text-align:center;width:80px"> Actions </th>
                                                     </tr>
                                                 </thead>
                                                 <tbody>
                                                   <?php
                                                   $i=1;
                                                   foreach ($groupelistes as $value):
                                                     ?>
                                                     <tr>
                                                       <td><?php echo $i; ?></td>
                                                       <td style="text-align:center"> <span class="label label-primary"><?php echo $value->libelle_groups ?></span> </td>
                                                       <td style="text-align:center"><?php echo $etabs->getEtabgroupNumber($value->id_groups) ?></td>
                                                       <td style="text-align:center">
                                                         <a href="#" class="btn btn-xs btn-warning"> <i class="fa fa-info-circle"></i> </a>
                                                         <a href="#" class="btn btn-xs btn-primary"> <i class="fa fa-pencil"></i> </a>
                                                       </td>
                                                     </tr>
                                                     <?php
                                                     $i++;
                                                   endforeach;
                                                    ?>

                                               </tbody>
                                             </table>
                                             </div>
                                        </div>
                                         <div class="tab-pane " id="contact-2">
                                           <form  id="FormAddCtrl" class="form-horizontal" action="../controller/groupes.php" method="post">
                                               <div class="form-body">
                                                 <div class="form-group row">
                                                         <label class="control-label col-md-3">Etablissements
                                                             <span class="required"> * </span>
                                                         </label>
                                                         <div class="col-md-5">
                                                             <select class="form-control" name="etabscodes[]" id="etabscodes" multiple="multiple"  style="width:100%">

                                                                 <?php
                                                                 $i=1;
                                                                   foreach ($etabsnongrouper as $value):
                                                                   ?>
                                                                   <option value="<?php echo $value->code_etab; ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                                   <?php
                                                                                                    $i++;
                                                                                                    endforeach;
                                                                                                    ?>

                                                             </select>
                                                     </div>
                                                   </div>



                                                  <div class="form-group row">
                                                          <label class="control-label col-md-3">Nom du groupe
                                                              <span class="required"> * </span>
                                                          </label>
                                                          <div class="col-md-5">
                                                              <input type="text" name="groupename" id="groupename" data-required="1" placeholder="Entrer la classe" class="form-control" />
                                                              <input type="hidden" name="etape" id="etape" value="1"/>
                                                            </div>

                                                   </div>


                             <div class="form-actions">
                                                   <div class="row">
                                                       <div class="offset-md-3 col-md-9">

                                                           <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>
                                                           <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                       </div>
                                                     </div>
                                                  </div>
                           </div>
                                           </form>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>



                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>



    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>


   $("#section").select2();
   $("#libetab").select2();
   $("#classex").select2();
   $("#etabscodes").select2({
     tags: true,
     minimumSelectionLength:2,
   tokenSeparators: [',', ' ']
   });


   $(document).ready(function() {

     $('#example5').DataTable( {
         "scrollX": true

     } );

     $("#FormAddCtrl").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{
         groupename:"required",
       },
       messages: {
         groupename:"Merci de renseigner le nom du groupe",
       },
       submitHandler: function(form) {

         // var etabscodes=$("#etabscodes").val();
         // var nbetabscodes=etabscodes.length;
         // // var tab=etabscodes.split(",");
         // // var nbtab=tab.length;
         // alert(etabscodes);
         // if(nbetabscodes==0)
         // {
         //
         // }else {
         //
         //
         //
         //   if(nbtab==1)
         //   {
         //     alert("égale à 0 et 1");
         //   }else {
         //     alert("superieur à 0 et 1");
         //   }
         //
         // }

      form.submit();


       }
     });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
