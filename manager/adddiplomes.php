<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Admin.php');
require_once('../class/Classe.php');
require_once('../class/Diplome.php');

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$diplome=new Diplome();
$localadmins= new Localadmin();
$parents=new ParentX();
$admin=new Admin();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}
$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();

$diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte'])
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Diplômes Professeur</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Professeurs</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Ajouter Diplômes</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['adddiplomeok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['adddiplomeok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter un nouveau ',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="detailsteatcher.php?compte="+<?php echo $_GET['compte']?>;
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['adddiplomeok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>
                               <!--button id = "panel-button"
                             class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                             data-upgraded = ",MaterialButton">
                             <i class = "material-icons">more_vert</i>
                          </button>
                          <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                             data-mdl-for = "panel-button">
                             <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                          </ul-->
                          </div>

                          <div class="card-body" id="bar-parent">

                              <div class="row">
              <div class="col-md-12">

          <fieldset style="background-color:#007bff;">

          <center><legend style="color:white;">LISTE DES DIPLOMES</legend></center>
          </fieldset>


          </div>
          </div><br/>
          <a href="#" class="mdl-button  mdl-button--raised mdl-js-ripple-effect  btn-warning"
											data-toggle="modal" data-target="#exampleModal">

      <span class="fa fa-plus"> Diplôme</span>

  </a></br></br>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">Ajouter Diplôme</h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
                        <form action="../controller/diplomes.php" id="FormDiplAdd" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                                <label class="control-label col-md-3">Intitulé
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="libellediplo" id="libellediplo" data-required="1" placeholder="Entrer l'intitulé du diplôme'" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Ecole
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="libecole" id="libecole" data-required="1" placeholder="Entrer l'Ecole" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Spécialité
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="speciale" id="speciale" data-required="1" placeholder="Entrer la spécialité" class="form-control input-height" /> </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Niveau
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="nivodiplo" id="nivodiplo" data-required="1" placeholder="Entrer le Niveau" class="form-control input-height" />
                                    <input type="hidden" name="etape" id="etape" value="1"/>
                                    <input type="hidden" name="idcompe" id="idcompte" value="<?php echo $_GET['compte']?>"/>
                                  </div>

                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Date Obtention
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                  <input type="date" placeholder="Entrer la date d'Obtention" name="dateo" id="dateo" data-mask="99/99/9999" class="form-control input-height">
                                    <span class="help-block">JJ-MM-AAAA</span></div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3">Fichier
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-8">
                                      <input type="file" id="fichier" name="fichier" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb"  data-allowed-file-extensions="pdf doc docx" />
                                    </div>
                                        </div>
                                        <div class="form-actions">
                                                              <div class="row">
                                                                  <div class="offset-md-3 col-md-9">
                                                                      <button type="submit" class="btn btn-info">Ajouter</button>
                                                                      <button type="button" class="btn btn-danger">Fermer</button>
                                                                  </div>
                                                                </div>
                                                             </div>
                          </form>
					            </div>

					        </div>
					    </div>
					</div>
  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
      <thead>
          <tr>


              <th> Intitulé </th>
              <th> Ecole </th>
              <th> Spécialité </th>
              <th> Niveau</th>
              <th> Date obtention </th>
              <th> Fichier </th>
              <th> Actions </th>
          </tr>
      </thead>
      <tbody>
        <?php
        $i=1;
          foreach ($diplomes as $value):
          ?>
          <tr class="odd gradeX">

            <td>
              <?php echo $value->libelle_diplo;?>
            </td>
              <td>
<?php echo $value->ecole_diplo;?>
              </td>
              <td>
                  <span class="label label-sm label-default"><?php echo $value->specialite_diplo;?>  </span>
              </td>
              <td>
<?php echo $value->niveau_diplo;?>
              </td>
              <td>
                <?php echo date_format(date_create($value->dateobt_diplo),"d/m/Y");?>
              </td>
                <td>
                  <?php
                  if(strlen($value->fichier_diplo)>0)
                  {
                    $emailTea=$diplome->getTeatcherMail($_GET['compte']);
                    $lien="../diplomes/professeur/".$emailTea."/".$value->fichier_diplo;
                  ?>
                <center>  <a href="<?php echo $lien;?>" target="_blank" class="btn btn-success" title="Voir le fichier"><span class="fa fa-eye"></span></a></center>
                  <?php
                  }else {
                    ?>
                <center>  <a href="#" target="_blank" class="btn btn-danger" title="Voir le fichier"><span class="fa fa-times"></span></a></center>
                    <?php
                  }
                   ?>
                </td>
              <td class="">
                  <div class="btn-group">
                    <a href="" class="btn btn-primary" title="Détails" ><i class="fa fa-exclamation-circle"></i></a>
                    <a href="#" onclick="modify('<?php echo $_GET['compte']?>')"class="btn btn-warning" title="Modifier"><i class="fa fa-pencil"></i></a>
                    <a href="#" onclick="" class="btn btn-danger" title="Supprimer"><i class="fa fa-trash-o"></i></a>

                  </div>
              </td>
          </tr>
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
          <script>

          function myFunction(idcompte)
          {
            //var url="detailslocal.php?compte="+idcompte;
          document.location.href="detailsdiplome.php?compte="+idcompte;
          }

          function modify(id)
          {


            Swal.fire({
title: 'Attention !',
text: "Voulez vous vraiment modifier ce diplôme",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Modifier',
cancelButtonText: 'Annuler',
}).then((result) => {
if (result.value) {
document.location.href="updateteatcher.php?compte="+id;
}else {

}
})
          }

          function deleted(id)
          {

            Swal.fire({
title: 'Attention !',
text: "Voulez vous vraiment supprimer ce diplôme",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Supprimer',
cancelButtonText: 'Annuler',
}).then((result) => {
if (result.value) {
document.location.href="../controller/teatcher.php?etape=3&compte="+id;
}else {

}
})
          }

          </script>
          <?php
                                           $i++;
                                           endforeach;
                                           ?>


      </tbody>
  </table>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <!-- Material -->
 <!--script src="../assets2/plugins/material/material.min.js"></script-->
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>


 <script>
 jQuery(document).ready(function() {

   $("#FormDiplAdd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{


        libellediplo:"required",
        libecole:"required",
        speciale:"required",
        nivodiplo:"required",
        dateo:"required",
        fichier:"required"

      },
      messages: {

        libellediplo:"Merci de renseigner l'intitulé du diplôme",
        libecole:"Merci de renseigner l'école ",
        speciale:"Merci de renseigner la spécialité",
        nivodiplo:"Merci de renseigner le niveau du diplôme",
        dateo:"Merci de renseigner la date obtention diplôme",
        fichier:"Merci de selectionner le fichier"
       },

       submitHandler: function(form) {

        form.submit();


       }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
