<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Classe.php');
$classe=new Classe();
$etabs=new Etab();
$user=new User();
$admin= new Localadmin();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$alletab=$etabs->getAllEtab();
$admins=$admin->getAdminlocalbyId($_GET['compte']);

$tabAdmin=explode("*",$admins);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Détails Administrateur Local</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Admin Local</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Détails Administrateur Local</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addlocalok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
  title: 'Félicitations !',
  text: "<?php echo $_SESSION['user']['addlocalok']; ?>",
  type: 'success',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Ajouter un nouveau',
  cancelButtonText: 'Annuler',
}).then((result) => {
  if (result.value) {

  }else {
    document.location.href="index.php";
  }
})
                </script>
                      <?php
                      unset($_SESSION['user']['addlocalok']);
                    }

                     ?>

              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header>Informations Admin Local</header>
                               <!--button id = "panel-button"
                             class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                             data-upgraded = ",MaterialButton">
                             <i class = "material-icons">more_vert</i>
                          </button>
                          <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                             data-mdl-for = "panel-button">
                             <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                             <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                          </ul-->
                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddLocalAd" class="form-horizontal" action="../controller/localadmin.php" method="post" enctype="multipart/form-data">
                                  <div class="form-body">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Compte
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" readonly class="form-control input-height" name="comptead" id="comptead" value="ADMIN LOCAL" readonly></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="control-label col-md-3">Etablissement
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-5">
                                          <input type="text" readonly value="<?php echo $tabAdmin[0]?>" name="libetab" id="libetab" data-required="1" placeholder="Entrer le Nom" class="form-control input-height" /></div>
                                    </div>

                                  <div class="form-group row">
                                          <label class="control-label col-md-3">Nom
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" readonly value="<?php echo $tabAdmin[1]?>" name="nomad" id="nomad" data-required="1" placeholder="Entrer le Nom" class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Prénoms
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input type="text" readonly value="<?php echo $tabAdmin[2]?>" name="prenomad" id="prenomad" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" /> </div>
                                      </div>

                                      <div class="form-group row">
                                        <label class="control-label col-md-3">Date de naissance
                                            <span class="required"> * </span>
                                        </label>
                                            <div class="col-md-5">
                                                <input readonly  type="text" value="<?php echo date_format(date_create($tabAdmin[3]),"d/m/Y")?>" placeholder="Entrer la date de naissance" name="datenaisad" id="datenaisad" data-mask="99/99/9999" class="form-control input-height">
                                                  <span class="help-block">JJ/MM/AAAA</span>
                                            </div>
                                        </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Contact
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input readonly value="<?php echo $tabAdmin[4]?>" name="contactad" id="contactad" type="text" placeholder="Entrer le contact " class="form-control input-height" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Fonction
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input readonly value="<?php echo $tabAdmin[5]?>" name="fonctionad" id="fonctionad" type="text" placeholder="Entrer la fonction " class="form-control input-height" /> </div>
                                      </div>

                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Email
                                            <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-5">
                                              <div class="input-group">
                                                  <span class="input-group-addon">
                                                          <i class="fa fa-envelope"></i>
                                                      </span>
                                                  <input readonly type="text" value="<?php echo $tabAdmin[6]?>" class="form-control input-height" name="emailad" id="emailad" placeholder="Entrer votre Adresse email"> </div>
                                          </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Login
                                              <span class="required">*  </span>
                                          </label>
                                          <div class="col-md-5">
                                              <input readonly value="<?php echo $tabAdmin[7]?>" name="loginad" id="loginad" type="text" placeholder="Entrer le Login " class="form-control input-height" /> </div>
                                      </div>


                                      <div class="form-group row">
                                          <label class="control-label col-md-3">Photo
                                            <span class="required">  </span>
                                          </label>
                                          <div class="compose-editor">
                                            <?php
                                              if(strlen($tabAdmin[8])>0)
                                              {
                                                $lien="../photo/".$tabAdmin[6]."/".$tabAdmin[8];
                                              }else {
                                                  $lien="../photo/user5.jpg";
                                              }



                                            ?>
                                            <input type="file" id="photoad" name="photoad" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien;?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" disabled="disabled" />
                                            <input type="hidden" name="etape" id="etape" value="1"/>
                                        </div>
                                      </div>

                <div class="form-actions">
                                      <div class="row">
                                          <div class="offset-md-3 col-md-9">
                                            <a class="btn btn-info" href="updatelocal.php?compte=<?php echo $_GET['compte']?>">Modifier</a>
                                            <a class="btn btn-danger" href="localadmins.php" >Annuler</a>

                                          </div>
                                        </div>
                                     </div>
              </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>

function modify(id)
{
  Swal.fire({
title: 'Attention !',
text: "Voulez-vous vraiment modifier ce compte",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Modifier',
cancelButtonText: 'Annuler',
}).then((result) => {
if (result.value) {
document.location.href="updatelocal.php?compte="+id;
}else {

}
})
}

 jQuery(document).ready(function() {

 $("#libetab").select2();

   $("#FormAddLocalAd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{
        passad: {
            required: true,
            minlength: 6
        },
        confirmtad:{
            required: true,
            minlength: 6,
            equalTo:'#passad'
        },
        fonctionad:"required",

        loginad:"required",
        emailad: {
                   required: true,
                   email: true
               },
        contactad:"required",
        datenaisad:"required",
        prenomad:"required",
        nomad:"required",
        libetab:"required"


      },
      messages: {
        confirmtad:{
            required:"Merci de confirmer le mot de passe",
            minlength:"Le mot de passe doit contenir au moins 6 caractères",
            equalTo: "Merci de renseigner des mots de passe identique"
        },
        passad: {
            required:"Merci de renseigner le mot de passe",
            minlength:"Le mot de passe doit contenir au moins 6 caractères"
        },
        loginad:"Merci de renseigner le Login",
        emailad:"Merci de renseigner une adresse email",
        contactad:"Merci de renseigner un contact",
        datenaisad:"Merci de renseigner la date de naissance",
        prenomad:"Merci de renseigner le prénom",
        nomad:"Merci de renseigner le nom ",
        fonctionad:"Merci de renseigner la fonction",
        libetab:"Merci de selectionner l'etablissement d'enseignement"
      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
           var etape=1;
           $.ajax({
             url: '../ajax/localadmin.php',
             type: 'POST',
             async:false,
             data: 'login=' + $("#loginad").val()+ '&email=' + $("#emailad").val() +'&codeetab=' + $("#libetab").val() + '&etape=' + etape,
             dataType: 'text',
             success: function (content, statut) {

               if(content==0)
               {
                 //le compte n'existe pas dans la base on peut l'ajouter

                 form.submit();
               }else if(content==1) {
                 //le compte existe dejà dans la base de données
                 Swal.fire({
                 type: 'warning',
                 title: 'Attention',
                 text: 'Cet Administrateur existe dejà dans la base de données et assigner à cet Etablissement',

                 })

               }else if(content==2) {

                 Swal.fire({
   title: 'Attention !',
   text: "Cet Admistrateur existe deja dans la base mais n'est pas assigner a cet Etablissement",
   type: 'warning',
   showCancelButton: true,
   confirmButtonColor: '#3085d6',
   cancelButtonColor: '#d33',
   confirmButtonText: 'Assigner le',
   cancelButtonText: 'Annuler',
 }).then((result) => {
   if (result.value) {
      $("#etape").val(2);

      form.submit();
   }else {

   }
 })


               }

             }


           });
      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
