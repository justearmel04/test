<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$datasinscriptions=$etabs->getCodeEtabOfStudentInscript($compteuserid);
foreach ($datasinscriptions as $datasinscription):
  $codeEtabsessionInscript=$datasinscription->codeEtab_inscrip;
  $sessionEtabInscript=$datasinscription->session_inscrip;
  $classeidInscript=$datasinscription->idclasse_inscrip;
endforeach;
$_SESSION['user']['session']=$sessionEtabInscript;

$_SESSION['user']['codeEtab']=$codeEtabsessionInscript;

//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$codeEtabsessionInscript;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


}else {
  $nbclasse=0;
}

//la liste des cours de ce professeur

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);

$courses=$etabs->getAllDevoirsofClasses($classeidInscript,$codeEtabsession,$libellesessionencours);

  $studentInfos=$student->getAllInformationsOfStudentOne($compteuserid,$libellesessionencours);

  foreach ($studentInfos as $personnal):
    $tabStudent[0]= $personnal->id_compte;
    $tabStudent[1]=$personnal->matricule_eleve;
    $tabStudent[2]= $personnal->nom_eleve;
    $tabStudent[3]=$personnal->prenom_eleve;
    $tabStudent[4]= $personnal->datenais_eleve;
    $tabStudent[5]=$personnal->lieunais_eleve;
    $tabStudent[6]= $personnal->sexe_eleve;
    $tabStudent[7]=$personnal->email_eleve;
    $tabStudent[8]=$personnal->email_eleve;
    $tabStudent[9]= $personnal->libelle_classe;
    $tabStudent[10]=$personnal->codeEtab_classe;
    $tabStudent[11]= $personnal->photo_compte;
    $tabStudent[12]=$personnal->tel_compte;
    $tabStudent[13]= $personnal->login_compte;
    $tabStudent[14]=$personnal->codeEtab_inscrip;
    $tabStudent[15]= $personnal->id_classe;
    $tabStudent[16]=$personnal->allergie_eleve;
    $tabStudent[17]=$personnal->condphy_eleve;

  endforeach;

  if(strlen($imageprofile)>0)
  {
    $lienphoto="../photo/".$tabStudent[1]."/".$imageprofile;
  }else {
    $lienphoto="../photo/user5.jpg";
  }

$_SESSION['user']['photolink']=$lienphoto;

$allLibellesHours=$etabs->getHoursAllLibs($codeEtabsession,$libellesessionencours);


// var_dump($courses);

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <!-- BEGIN HEAD -->
 <head>
     <meta charset="utf-8" />
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta content="width=device-width, initial-scale=1" name="viewport" />
     <title><?php echo L::Titlesite ?></title>
     <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
     <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

     <!-- google font -->
     <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
   <!-- icons -->
     <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
   <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
     <!-- Material Design Lite CSS -->
   <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
   <link href="../assets2/css/material_style.css" rel="stylesheet">
   <!-- morris chart -->
     <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

   <!-- Theme Styles -->
     <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

   <!-- favicon -->
     <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
     <style media="screen">
     .navbar-custom {
   background: #71d40f;
   float: left;
   width: 100%;
 }


 //theme color css

 .header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
   color: #f8f9fa;
 }

 .header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
     color: #f8f9fa;
 }

 .header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
     color: #f8f9fa;
 }
     </style>
  </head>
 <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
     <div class="page-wrapper">
         <!-- start header -->
         <?php
 include("menu1.php")
          ?>
         <!-- end header -->
         <!-- start page container -->
       <?php
       include('submenu.php');
        ?>
			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                  <div class="page-bar">

                      <div class="page-title-breadcrumb">

                          <div class=" pull-left">

                              <div class="page-title">Emploi du temps classe : <?php echo $classe->getInfosofclassesbyId($tabStudent[15],$libellesessionencours) ?></div>

                          </div>

                          <ol class="breadcrumb page-breadcrumb pull-right">

                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                              </li>

                              <li class="active">Emploi du temps</li>

                          </ol>

                      </div>

                  </div>

					<!-- start widget -->

					<div class="state-overview">

						<div class="row">



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->

					      </div>

						</div>

					<!-- end widget -->

          <?php



                if(isset($_SESSION['user']['addetabexist']))

                {



                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>







                  <?php

                  unset($_SESSION['user']['addetabexist']);

                }



                 ?>



   <div class="row" style="">





<div class="col-md-12">

    <div class="card  card-box">

        <div class="card-head">

            <header></header>

            <div class="tools">

                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

            </div>

        </div>

        <div class="card-body ">

          <div class="pull-right">

             <a class="btn btn-warning" target="_blank" href="scheduletabclasse.php?classe=<?php echo $tabStudent[15]; ?>&codeEtab=<?php echo $codeEtabsession; ?>&session=<?php echo $libellesessionencours; ?>&compte=<?php echo $tabStudent[0] ?>" ><i class="fa fa-print"> Emploi du temps</i></a>




          </div>
          <br><br>

<div class="table-responsive">

<table class="table table-striped custom-table table-hover">

    <thead>



    </thead>

    <tbody>
      <tr>
        <td>Horaires</td>
        <?php
          $dataday=$etabs->getAllweeks();
          $tabLibday=array();
          $i=0;
            foreach ($dataday as $value):
              $tabLibday[$i]=$value->short_days;
              ?>
              <td>
                <?php echo $value->libelle_days;?>
              </td>
              <?php
              $i++;
            endforeach;
         ?>
      </tr>
      <?php
        $nblibday=count($tabLibday);

          foreach ($allLibellesHours as $value1):
              $idheure=$value1->id_heure;
              ?>
                <tr>
                    <td><?php echo returnHours($value1->heuredeb_heure). " - ".returnHours($value1->heurefin_heure) ?></td>
                    <?php
                      for($j=0;$j<$nblibday;$j++)
                      {
                        ?>
                        <td>
                          <?php
                            $nbmatiereday=$etabs->getnumberofmatierethisday($idheure,$tabLibday[$j],$codeEtabsession,$libellesessionencours,$tabStudent[15]);
                            // echo $nbmatiereday;
                            if($nbmatiereday>0)
                            {
                              $datas=$etabs->getMatierethisday($idheure,$tabLibday[$j],$codeEtabsession,$libellesessionencours,$tabStudent[15]);
                              $tabdatas=explode("*",$datas);
                              ?>
                              <div class="btn-group" role="group" aria-label="Button group with nested dropdown">


<div class="btn-group" role="group">
<button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <?php echo $tabdatas[0];?>
</button>

</div>
</div>
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
<script>

function myFunction(idcompte)
{
//var url="detailslocal.php?compte="+idcompte;
document.location.href="detailsadmin.php?compte="+idcompte;
}

function modify(id)
{
var classeid="<?php echo $tabStudent[15]; ?>";

Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment modifier ce cours",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="updateroutine.php?compte="+id+"&classeid="+classeid;
}else {

}
})
}

function deleted(id,classeid)
{
var codeEtab="<?php echo $codeEtabsession; ?>";
var sessionEtab="<?php echo $libellesessionencours; ?>";

Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment supprimer ce cours",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {

document.location.href="../controller/routine.php?etape=5&compte="+id+"&classe="+classeid+"&codeEtab="+codeEtab+"&sessionEtab="+sessionEtab;
}else {

}
})
}

</script>
                              <?php
                            }
                           ?>
                        </td>
                        <?php
                      }
                     ?>
                </tr>
              <?php
          endforeach;

       ?>
    </tbody>

</table>

</div>



</div>

    </div>

</div>









                </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 	<script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

     <!-- calendar -->

     <!--script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script-->

     <!-- Common js-->

 	<script src="../assets2/js/app.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

 	<script src="../assets2/js/theme-color.js" ></script>

 	<!-- Material -->

 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript">
  function addFrench()
  {
    var etape=1;
    var lang="fr";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }

  function addEnglish()
  {
    var etape=1;
    var lang="en";
    $.ajax({
      url: '../ajax/langue.php',
      type: 'POST',
      async:false,
      data: 'etape=' + etape+ '&lang=' +lang,
      dataType: 'text',
      success: function (content, statut) {

  window.location.reload();

      }
    });
  }
  </script>





    <!-- end js include path -->

  </body>



</html>
