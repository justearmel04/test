<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$datasinscriptions=$etabs->getCodeEtabOfStudentInscript($compteuserid);
foreach ($datasinscriptions as $datasinscription):
  $codeEtabInscript=$datasinscription->codeEtab_inscrip;
  $sessionEtabInscript=$datasinscription->session_inscrip;
  $classeidInscript=$datasinscription->idclasse_inscrip;
endforeach;
$_SESSION['user']['session']=$sessionEtabInscript;

$_SESSION['user']['codeEtab']=$codeEtabInscript;

//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$codeEtabInscript;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


}else {
  $nbclasse=0;
}
//la liste des cours de ce professeur

$courseid=$_GET['course'];
$classeid=$_GET['classeid'];



// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getAllquizsdetailstudent($courseid,$classeid,$codeEtabsession,$libellesessionencours);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
  $teatcheridcourses=$datacourses->teatcher_quiz;
  $filescourses="";

endforeach;



// Nous allons recuperer les questions du quiz

  $questions=$etabs->getAllquizQuestion($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);

  $questiontrueorfalse=$etabs->getAllquizQuestionTrueOrfalse($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);

  $questionmultiplechoice=$etabs->getAllquizQuestionMultiplechoice($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);


// $coursesSections=$etabs->getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $coursescompetences=$etabs->getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $courseshomeworks=$etabs->getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
// var_dump($coursescompetences);

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <!-- BEGIN HEAD -->
 <head>
     <meta charset="utf-8" />
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta content="width=device-width, initial-scale=1" name="viewport" />
     <title><?php echo L::Titlesite ?></title>
     <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
     <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

     <!-- google font -->
     <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
   <!-- icons -->
     <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
   <!--bootstrap -->
    <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <!-- data tables -->
    <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
     <!-- Material Design Lite CSS -->
   <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
   <link href="../assets2/css/material_style.css" rel="stylesheet">
   <!-- morris chart -->
     <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

   <!-- Theme Styles -->
     <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
     <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

   <!-- favicon -->
     <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
     <style media="screen">
     .navbar-custom {
   background: #71d40f;
   float: left;
   width: 100%;
 }


 //theme color css

 .header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
   color: #f8f9fa;
 }

 .header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
     color: #f8f9fa;
 }

 .header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
     color: #f8f9fa;
 }
     </style>
  </head>
 <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
     <div class="page-wrapper">
         <!-- start header -->
         <?php
 include("menu1.php")
          ?>
         <!-- end header -->
         <!-- start page container -->
       <?php
       include('submenu.php');
        ?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::QuizResultBtn ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="quizs.php"><?php echo L::QuizMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::QuizResultBtn ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       <?php echo L::RequiredScolaireYear ?>

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


                      <div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->

							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">

								<div class="row">


                    <div class="col-md-12">
                      <div class="card">
    										<div class="card-topline-aqua">
    											<header></header>
    										</div>
    										<div class="card-body" id="bar-parent">
                          <!-- <span id="countdowntimer" class="label label-lg label-primary pull-right"  style="text-align:center;">  <i class="fa fa-clock-o fa-2x"> </i>  <span id="h_timer" style="font-size:19px;" onchange="alert('bonjour')"> </span></span> -->
                          <br>
                            <form  id="FormAddAcademique" class="form-horizontal" action="#" method="post"  >
                              <div class="row pull-right">

                                <div class="col-lg-6">

 								               <!-- Basic Chip -->
 												<span class="mdl-chip">

 												    <span class="mdl-chip__text"> <?php echo $etabs->getcompteNotequiz($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid) ?> / <?php echo $etabs->getquiztotalnotes($courseid,$codeEtabsession,$libellesessionencours); ?> </span>
 												</span>
 								            </div>
                              </div>
                              <div class="form-body">
                                <div class="col-md-12">
                                <span class="label label-lg label-primary"  style="text-align:center;"> <i class="fa fa-list"> </i> <?php echo L::TrueOrFalseQuestions ?></span>
                              </div></br>

                          <div class="col-md-12">
                            <div class="row">


                            <?php
                            $concattrueorfalse="";
                              $i=1;

                              foreach ($questiontrueorfalse as  $value):
                                $concattrueorfalse=$concattrueorfalse.$value->id_quest."@";

                                $solutions="";
                                $solutionids="";
                                $solutionspropositions=$etabs->getsolutionsPropoReponses($courseid,$value->id_quest);

                                foreach ($solutionspropositions as $valuesolution):
                                  $solutions=$solutions." ".$valuesolution->libelle_proprep;
                                  $solutionids=$solutionids.$valuesolution->id_proprep."@";
                                endforeach;


                                //
                                ?>
                              <div class="col-md-6">


                              <p style="font-size:17px"><b><?php echo $i ?> .  <?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></b></p>

                              <div class="form-group">
                                <?php
                                //nous allons les propositions de réponses de cet eleve
                                $propositionsAndreponses=$etabs->getPropositionsAndreponsesChild($compteuserid,$value->id_quest,$courseid,$codeEtabsession,$libellesessionencours);

                                // var_dump($propositionsAndreponses);
                                $solutionsidStudent="";
                                foreach ($propositionsAndreponses as $valueRep):
                                  ?>
                                  <div class="radio p-0">
                                      <input type="radio" name="answer<?php echo $value->id_quest; ?>" id="optionsRadios1<?php echo $value->id_quest; ?>" value="<?php echo $valueRep->reponse_repquiz ?>"  <?php if($valueRep->reponse_repquiz==1){ echo "checked";}else{} ?> disabled>
                                      <label for="optionsRadios1<?php echo $value->id_quest; ?>">
                                          <?php
                                          echo $etabs->getlibellepropositionbyId($valueRep->propositionid_repquiz);

                                          if($valueRep->reponse_repquiz==1)
                                          {
                                            $solutionsidStudent=$solutionsidStudent.$valueRep->propositionid_repquiz."@";
                                          }

                                           ?>
                                      </label>
                                  </div>


                                  <?php
                                endforeach;
                                 ?>


                                       <span class="help-block" style="color:#51f376;display:inline" id="helpblock<?php echo $value->id_quest;  ?>"> <i class="fa fa-check-circle" style="color:#51f376;"></i> <?php echo L::Solutions ?> <?php echo $solutions; ?></span> <br>
                                       <?php
                                       // echo $solutionsidStudent."/".$solutionids;
                                       $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));
                                       $tabsolutionids=explode("@",substr($solutionids,0,-1));
                                       $nbcont=count($tabsolutionsStudent);
                                       // echo $nbcont;
                                       for($i=0;$i<$nbcont;$i++)
                                       {
                                         if($tabsolutionids[$i]==$tabsolutionsStudent[$i])
                                         {
                                           // echo "point obtenu";
                                           //Nous allons verifier si nous avons une note pour cette question
                                           $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours);
                                           if($nbnotes==0)
                                           {
                                             $etabs->AddnotesQuizquestion($value->id_quest,$compteuserid,$courseid,$value->point_quest,$codeEtabsession,$libellesessionencours);

                                           }

                                           ?>
                                            <span class="help-block" style="color:#51f376;display:inline" id="pointblock<?php echo $value->id_quest;  ?>"> <i class="fa fa-check-circle" style="color:#51f376;"></i> <?php echo $value->point_quest; ?> <?php echo L::Points ?></span>
                                           <?php
                                         }else {

                                            $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours);
                                              $point=0;
                                            if($nbnotes==0)
                                            {

                                              $etabs->AddnotesQuizquestion($value->id_quest,$compteuserid,$courseid,$point,$codeEtabsession,$libellesessionencours);

                                            }

                                            ?>
                                            <span class="help-block" style="color:#e2172b;display:inline" id="pointblock<?php echo $value->id_quest;  ?>"> <i class=" fa fa-window-close" style="color:#e2172b;"></i> <?php echo $point;  ?> <?php echo L::Points ?></span>
                                           <?php

                                         }
                                       }
                                        ?>

                                   </div>
                                   </div>
                                <?php
                                $i++;
                              endforeach;
                               ?>
                               <input type="hidden" name="concattrueorfalseid" id="concattrueorfalseid" value="<?php echo $concattrueorfalse; ?>">
                               <input type="hidden" name="etape" id="etape" value="5">
                               <input type="hidden" name="studentid" id="studentid" value="<?php echo $compteuserid; ?>">
                               <input type="hidden" name="quizid" id="quizid" value="<?php echo $courseid; ?>">
                                <input type="hidden" name="classeid" id="classeid" value="<?php echo $classeidcourses; ?>">
                                <input type="hidden" name="matiereid" id="matiereid" value="<?php echo $matiereidcourses; ?>">

                                <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabsession; ?>">
                                <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours; ?>">

                           </div>
                           </div>
                           </br>
                           <div class="col-md-12">
                           <span class="label label-lg label-primary"  style="text-align:center;"> <i class="fa fa-list"> </i> <?php echo L::MultichoiceQuestions ?></span>
                         </div></br>
                         <div class="col-md-12">
                           <div class="row">

                             <?php
                             $i=1;
                             $concatqestmulti="";
                               foreach ($questionmultiplechoice as  $value):
                                 $solutions="";
                                 $solutionids="";
                                 $solutionspropositions=$etabs->getsolutionsPropoReponses($courseid,$value->id_quest);

                                 foreach ($solutionspropositions as $valuesolution):
                                   $solutions=$solutions." ".$valuesolution->libelle_proprep;
                                   $solutionids=$solutionids.$valuesolution->id_proprep."@";
                                 endforeach;

                                 $concatqestmulti=$concatqestmulti.$value->id_quest."@";
                                 ?>
                                 <div class="col-md-6">
                                   <p style="font-size:17px"><b><?php echo $i ?> .  <?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></b></p>
                                   <div class="form-group">
                                     <input type="hidden" name="concatquestmulticocher<?php echo $value->id_quest;  ?>" id="concatquestmulticocher<?php echo $value->id_quest;  ?>" value="">
                                     <input type="hidden" name="nbconcatquestmulticocher<?php echo $value->id_quest;  ?>" id="nbconcatquestmulticocher<?php echo $value->id_quest;  ?>" value="0">
                                     <?php
                                     $propositionsreponses=$etabs->getPropositionsOfQuestions($value->id_quest);
                                     $j=1;
                                     $solutionsidStudent="";
                                     foreach ($propositionsreponses as $propositions):
                                       //retrouvons la reponse de l'etudiant face a cette proposition
                                       $valeurpropositionchoice=$etabs->getValeurOfpropositionchoice($value->id_quest,$courseid,$propositions->id_proprep,$codeEtabsession,$libellesessionencours,$compteuserid);
                                       if($valeurpropositionchoice==1)
                                       {
                                         $solutionsidStudent=$solutionsidStudent.$propositions->id_proprep."@";
                                       }
                                      ?>
                                        <div class="checkbox checkbox-icon-black p-0">
                                            <input id="checkbox<?php echo $propositions->id_proprep; ?>" type="checkbox" name="checkbox<?php echo $propositions->id_proprep; ?>" onclick="cocherordecocher(<?php echo $value->id_quest;  ?>,<?php echo $propositions->id_proprep; ?>)" <?php if($valeurpropositionchoice==1){echo "checked";}else{} ?> disabled>
                                            <label for="checkbox<?php echo $propositions->id_proprep; ?>">
                                                <?php echo utf8_decode(utf8_encode($propositions->libelle_proprep)) ?>
                                            </label>
                                        </div>

                                        <?php
                                      endforeach;
                                         ?>

  <span class="help-block" style="color:#51f376;display:inline" id="helpblock<?php echo $value->id_quest;  ?>"> <i class="fa fa-check-circle" style="color:#51f376;"></i> <?php echo L::Solutions ?> <?php echo $solutions; ?> </span> <br>

  <?php
  $tabsolutionids=explode("@",substr($solutionids,0,-1));
  $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));

  $cmp = array_diff($tabsolutionids, $tabsolutionsStudent);
  $nbcont=count($cmp);
  if($nbcont==0)
  {

    $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours);
    if($nbnotes==0)
    {
      $etabs->AddnotesQuizquestion($value->id_quest,$compteuserid,$courseid,$value->point_quest,$codeEtabsession,$libellesessionencours);

    }
?>
    <span class="help-block" style="color:#51f376;display:inline" id="pointblock<?php echo $value->id_quest;  ?>"> <i class="fa fa-check-circle" style="color:#51f376;"></i> <?php echo $value->point_quest; ?> <?php echo L::Points ?></span>
<?php

    // echo "point obtenu";
  }else {
    // echo "point non obtenu";
    $nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$compteuserid,$courseid,$codeEtabsession,$libellesessionencours);
    $point=0;
    if($nbnotes==0)
    {
      $etabs->AddnotesQuizquestion($value->id_quest,$compteuserid,$courseid,$point,$codeEtabsession,$libellesessionencours);

    }
    ?>
    <span class="help-block" style="color:#e2172b;display:inline" id="pointblock<?php echo $value->id_quest;  ?>"> <i class=" fa fa-window-close" style="color:#e2172b;"></i> <?php echo $point;  ?> <?php echo L::Points ?></span>
    <?php
  }

   ?>

                                    </div>
                                 </div>

                                 <?php
                                $i++;
                               endforeach;
                              ?>
 <input type="hidden" name="concatqestmulti" id="concatqestmulti" value="<?php echo $concatqestmulti; ?>">
                           </div>

                         </div>
                             </div>

                          </form>
                          </div>

    									</div>
                    </div>

								</div>
							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/js/jQuery.countdownTimer.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script type="text/javascript">

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function cocherordecocher(questionid,propositionid)
    {
      // alert(questionid+" "+propositionid);

      if( $('input[name=checkbox'+propositionid+']').is(':checked') ){
          addpropositionsMulti(propositionid,questionid);
      } else {
          deletepropositionsMulti(propositionid,questionid);
      }
    }

    function addpropositionsMulti(propositionid,questionid)
    {
      var concat=$("#concatquestmulticocher"+questionid).val();

      $("#concatquestmulticocher"+questionid).val(concat+propositionid+"@");

      var concatroutines=$("#concatquestmulticocher"+questionid).val();

      var tab=concatroutines.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#nbconcatquestmulticocher"+questionid).val(nbtabnew);

      if(nbtabnew==0)
      {
        $("#helpblock"+questionid).css("display", "inline");
      }else {
        $("#helpblock"+questionid).css("display", "none");
      }

    }

    function deletepropositionsMulti(propositionid,questionid)
    {
      var concat=$("#concatquestmulticocher"+questionid).val();

      $("#concatquestmulticocher"+questionid).val($("#concatquestmulticocher"+questionid).val().replace(propositionid+"@", ""));

      var concatroutines=$("#concatquestmulticocher"+questionid).val();

      var tab=concatroutines.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#nbconcatquestmulticocher"+questionid).val(nbtabnew);

      if(nbtabnew==0)
      {
        $("#helpblock"+questionid).css("display", "inline");
      }else {
        $("#helpblock"+questionid).css("display", "none");
      }

    }

   $(document).ready(function() {

     var duree="<?php echo $durationcourses; ?>";
     var tabduree=duree.split(":");

     $("#h_timer").countdowntimer({
       hours:tabduree[0],
       minutes:tabduree[1],
       seconds:tabduree[2],
      // hours : 10,
      size : "lg",
      padZeroes : false,
       timeUp : timeIsUp
     });

     function beforeExpiryFunc() {
           //Your code
       }
       function timeIsUp() {
           alert("finish");
       }


       $("#FormAddAcademique").validate({

              errorPlacement: function(label, element) {
              label.addClass('mt-2 text-danger');
              label.insertAfter(element);
             },
             highlight: function(element, errorClass) {
              $(element).parent().addClass('has-danger')
              $(element).addClass('form-control-danger')
             },
             success: function (e) {
                  $(e).closest('.control-group').removeClass('error').addClass('info');
                  $(e).remove();
              },
              rules:{
                libellecourse:"required"
              },
              messages:{
                libellecourse:"Merci de renseigner l'exercice"
              },
              submitHandler: function(form) {

                var multi="<?php echo $concatqestmulti ?>";

                var content=0;

                var tab=multi.split("@");

                var nbtab=tab.length;
                var nbtabnew=parseInt(nbtab)-1;

                for(var i=0;i<nbtabnew;i++)
                {
                  var nbselectchoice=$("#nbconcatquestmulticocher"+tab[i]).val();
                  // alert(nbselectchoice);

                  if(nbselectchoice==0)
                  {

                    $("#helpblock"+tab[i]).css("display", "inline");
                  }else {
                    $("#helpblock"+tab[i]).css("display", "none");
                  }
                  content=parseInt(content)+parseInt(nbselectchoice);
                }

                // alert(content);

                if(content>0)
                {
                  form.submit();
                }



              }
       });


   });
  //  $(function() {
  //
  //    var duree="<?php //echo $durationcourses; ?>";
  //    var tabduree=duree.split(":");
  //
 	// 	$("#h_timer").countdowntimer({
  //      hours:tabduree[0],
  //      minutes:tabduree[1],
  //      seconds:tabduree[2],
 	// 		// hours : 10,
 	// 		size : "lg",
 	// 		padZeroes : false,
  //      timeUp : timeIsUp
 	// 	});
  //
  //    function beforeExpiryFunc() {
  //          //Your code
  //      }
  //      function timeIsUp() {
  //          alert("finish");
  //      }
  //
 	// });
</script>
    <!-- end js include path -->
  </body>

</html>
