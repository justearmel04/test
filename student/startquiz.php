<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$datasinscriptions=$etabs->getCodeEtabOfStudentInscript($compteuserid);
foreach ($datasinscriptions as $datasinscription):
  $codeEtabInscript=$datasinscription->codeEtab_inscrip;
  $sessionEtabInscript=$datasinscription->session_inscrip;
  $classeidInscript=$datasinscription->idclasse_inscrip;
endforeach;
$_SESSION['user']['session']=$sessionEtabInscript;

$_SESSION['user']['codeEtab']=$codeEtabInscript;

//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$codeEtabInscript;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];


}else {
  $nbclasse=0;
}
//la liste des cours de ce professeur

$courseid=$_GET['course'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getAllquizsdetailstudent($courseid,$classeid,$codeEtabsession,$libellesessionencours);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
  $teatcheridcourses=$datacourses->teatcher_quiz;
  $filescourses="";

endforeach;



// Nous allons recuperer les questions du quiz

  $questions=$etabs->getAllquizQuestion($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);

  $questiontrueorfalse=$etabs->getAllquizQuestionTrueOrfalse($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);

  $questionmultiplechoice=$etabs->getAllquizQuestionMultiplechoice($courseid,$classeid,$codeEtabsession,$libellesessionencours,$teatcheridcourses);


// $coursesSections=$etabs->getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $coursescompetences=$etabs->getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
//
// $courseshomeworks=$etabs->getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
// var_dump($coursescompetences);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
  <!--bootstrap -->
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Détails du quiz</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" href="#">Quizs</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Détails du quiz</li>
                          </ol>
                      </div>
                  </div>
                  <?php

                        if(isset($_SESSION['user']['addclasseok']))
                        {

                          ?>
                          <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php
                        //echo $_SESSION['user']['addetabok'];
                        ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                           </a>
                        </div-->
      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
      <script src="../assets/js/sweetalert2.min.js"></script>

                    <script>
                    Swal.fire({
                    type: 'success',
                    title: 'Félicitations',
                    text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                    })
                    </script>
                          <?php
                          unset($_SESSION['user']['addclasseok']);
                        }

                         ?>

                         <?php
                         if($nbsessionOn==0)
                         {
                           ?>
                           <div class="alert alert-danger alert-dismissible fade show" role="alert">

                           Vous devez definir la Sessionn scolaire

                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                            </a>
                           </div>
                           <?php
                         }
                          ?>


                          <div class="row">
    						<div class="col-md-12">
    							<!-- BEGIN PROFILE SIDEBAR -->

    							<!-- END BEGIN PROFILE SIDEBAR -->
    							<!-- BEGIN PROFILE CONTENT -->
    							<div class="profile-content">

    								<div class="row">


                        <div class="col-md-12">
                          <div class="card">
        										<div class="card-topline-aqua">
        											<header></header>
        										</div>
        										<div class="card-body" id="bar-parent">
                              <span id="countdowntimer" class="label label-lg label-primary pull-right"  style="text-align:center;">  <i class="fa fa-clock-o fa-2x"> </i>  <span id="h_timer" style="font-size:19px;" onchange="alert('bonjour')"> </span></span>
                              <br>
                                <form  id="FormAddAcademique" class="form-horizontal" action="../controller/quizs.php" method="post"  >
                                  <div class="form-body">
                                    <div class="col-md-12">
                                    <span class="label label-lg label-primary"  style="text-align:center;"> <i class="fa fa-list"> </i> questions VRAI / FAUX</span>
                                  </div></br>

                              <div class="col-md-12">
                                <div class="row">


                                <?php
                                $concattrueorfalse="";
                                  $i=1;
                                  foreach ($questiontrueorfalse as  $value):
                                    $concattrueorfalse=$concattrueorfalse.$value->id_quest."@";
                                    ?>
                                  <div class="col-md-6">


                                  <p style="font-size:17px"><b><?php echo $i ?> .  <?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></b></p>

                                  <div class="form-group">
                                           <div class="radio p-0">
                                               <input type="radio" name="answer<?php echo $value->id_quest; ?>" id="optionsRadios1<?php echo $value->id_quest; ?>" value="1" checked>
                                               <label for="optionsRadios1<?php echo $value->id_quest; ?>">
                                                   VRAI
                                               </label>
                                           </div>
                                           <div class="radio p-0">
                                               <input type="radio" name="answer<?php echo $value->id_quest; ?>" id="optionsRadios2<?php echo $value->id_quest; ?>" value="0">
                                               <label for="optionsRadios2<?php echo $value->id_quest; ?>">
                                                   FAUX
                                               </label>
                                           </div>

                                       </div>
                                       </div>
                                    <?php
                                    $i++;
                                  endforeach;
                                   ?>
                                   <input type="hidden" name="concattrueorfalseid" id="concattrueorfalseid" value="<?php echo $concattrueorfalse; ?>">
                                   <input type="hidden" name="etape" id="etape" value="5">
                                   <input type="hidden" name="studentid" id="studentid" value="<?php echo $compteuserid; ?>">
                                   <input type="hidden" name="quizid" id="quizid" value="<?php echo $courseid; ?>">
                                    <input type="hidden" name="classeid" id="classeid" value="<?php echo $classeidcourses; ?>">
                                    <input type="hidden" name="matiereid" id="matiereid" value="<?php echo $matiereidcourses; ?>">

                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabsession; ?>">
                                    <input type="hidden" name="sessionEtab" id="sessionEtab" value="<?php echo $libellesessionencours; ?>">

                               </div>
                               </div>
                               </br>
                               <div class="col-md-12">
                               <span class="label label-lg label-primary"  style="text-align:center;"> <i class="fa fa-list"> </i> question a choix multiples</span>
                             </div></br>
                             <div class="col-md-12">
                               <div class="row">

                                 <?php
                                 $i=1;
                                 $concatqestmulti="";
                                   foreach ($questionmultiplechoice as  $value):
                                     $concatqestmulti=$concatqestmulti.$value->id_quest."@";
                                     ?>
                                     <div class="col-md-6">
                                       <p style="font-size:17px"><b><?php echo $i ?> .  <?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></b></p>
                                       <div class="form-group">
                                         <input type="hidden" name="concatquestmulticocher<?php echo $value->id_quest;  ?>" id="concatquestmulticocher<?php echo $value->id_quest;  ?>" value="">
                                         <input type="hidden" name="nbconcatquestmulticocher<?php echo $value->id_quest;  ?>" id="nbconcatquestmulticocher<?php echo $value->id_quest;  ?>" value="0">
                                         <?php
                                         $propositionsreponses=$etabs->getPropositionsOfQuestions($value->id_quest);
                                         $j=1;
                                         foreach ($propositionsreponses as $propositions):

                                          ?>
                                            <div class="checkbox checkbox-icon-black p-0">
                                                <input id="checkbox<?php echo $propositions->id_proprep; ?>" type="checkbox" name="checkbox<?php echo $propositions->id_proprep; ?>" onclick="cocherordecocher(<?php echo $value->id_quest;  ?>,<?php echo $propositions->id_proprep; ?>)">
                                                <label for="checkbox<?php echo $propositions->id_proprep; ?>">
                                                    <?php echo utf8_decode(utf8_encode($propositions->libelle_proprep)) ?>
                                                </label>
                                            </div>

                                            <?php
                                          endforeach;
                                             ?>

      <span class="help-block" style="color:red;display:inline" id="helpblock<?php echo $value->id_quest;  ?>"> <i class="fa fa-warning" style="color:red;"></i> Vous devez cocher au moins une réponse</span>

                                        </div>
                                     </div>

                                     <?php
                                    $i++;
                                   endforeach;
                                  ?>
     <input type="hidden" name="concatqestmulti" id="concatqestmulti" value="<?php echo $concatqestmulti; ?>">
                               </div>

                             </div>
                                 </div>
                                      <div class="form-actions">
                                                            <div class="row">
                                                                <div class="offset-md-3 col-md-9">
                                                                  <button type="submit" class="btn btn-info">Soumettre</button>

                                                                    <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                </div>
                                                              </div>
                                                           </div>

                              </form>
                              </div>

        									</div>
                        </div>

    								</div>
    							</div>
    							<!-- END PROFILE CONTENT -->
    						</div>
    					</div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/js/jQuery.countdownTimer.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script type="text/javascript">

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function cocherordecocher(questionid,propositionid)
    {
      // alert(questionid+" "+propositionid);

      if( $('input[name=checkbox'+propositionid+']').is(':checked') ){
          addpropositionsMulti(propositionid,questionid);
      } else {
          deletepropositionsMulti(propositionid,questionid);
      }
    }

    function addpropositionsMulti(propositionid,questionid)
    {
      var concat=$("#concatquestmulticocher"+questionid).val();

      $("#concatquestmulticocher"+questionid).val(concat+propositionid+"@");

      var concatroutines=$("#concatquestmulticocher"+questionid).val();

      var tab=concatroutines.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#nbconcatquestmulticocher"+questionid).val(nbtabnew);

      if(nbtabnew==0)
      {
        $("#helpblock"+questionid).css("display", "inline");
      }else {
        $("#helpblock"+questionid).css("display", "none");
      }

    }

    function deletepropositionsMulti(propositionid,questionid)
    {
      var concat=$("#concatquestmulticocher"+questionid).val();

      $("#concatquestmulticocher"+questionid).val($("#concatquestmulticocher"+questionid).val().replace(propositionid+"@", ""));

      var concatroutines=$("#concatquestmulticocher"+questionid).val();

      var tab=concatroutines.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#nbconcatquestmulticocher"+questionid).val(nbtabnew);

      if(nbtabnew==0)
      {
        $("#helpblock"+questionid).css("display", "inline");
      }else {
        $("#helpblock"+questionid).css("display", "none");
      }

    }

   $(document).ready(function() {

     var duree="<?php echo $durationcourses; ?>";
     var tabduree=duree.split(":");

     $("#h_timer").countdowntimer({
       hours:tabduree[0],
       minutes:tabduree[1],
       seconds:tabduree[2],
      // hours : 10,
      size : "lg",
      padZeroes : false,
       timeUp : timeIsUp
     });

     function beforeExpiryFunc() {
           //Your code
       }
       function timeIsUp() {
           alert("finish");
       }


       $("#FormAddAcademique").validate({

              errorPlacement: function(label, element) {
              label.addClass('mt-2 text-danger');
              label.insertAfter(element);
             },
             highlight: function(element, errorClass) {
              $(element).parent().addClass('has-danger')
              $(element).addClass('form-control-danger')
             },
             success: function (e) {
                  $(e).closest('.control-group').removeClass('error').addClass('info');
                  $(e).remove();
              },
              rules:{
                libellecourse:"required"
              },
              messages:{
                libellecourse:"Merci de renseigner l'exercice"
              },
              submitHandler: function(form) {

                var multi="<?php echo $concatqestmulti ?>";

                var content=0;

                var tab=multi.split("@");

                var nbtab=tab.length;
                var nbtabnew=parseInt(nbtab)-1;

                for(var i=0;i<nbtabnew;i++)
                {
                  var nbselectchoice=$("#nbconcatquestmulticocher"+tab[i]).val();
                  // alert(nbselectchoice);

                  if(nbselectchoice==0)
                  {

                    $("#helpblock"+tab[i]).css("display", "inline");
                  }else {
                    $("#helpblock"+tab[i]).css("display", "none");
                  }
                  content=parseInt(content)+parseInt(nbselectchoice);
                }

                // alert(content);

                if(content>0)
                {
                  form.submit();
                }



              }
       });


   });
  //  $(function() {
  //
  //    var duree="<?php //echo $durationcourses; ?>";
  //    var tabduree=duree.split(":");
  //
 	// 	$("#h_timer").countdowntimer({
  //      hours:tabduree[0],
  //      minutes:tabduree[1],
  //      seconds:tabduree[2],
 	// 		// hours : 10,
 	// 		size : "lg",
 	// 		padZeroes : false,
  //      timeUp : timeIsUp
 	// 	});
  //
  //    function beforeExpiryFunc() {
  //          //Your code
  //      }
  //      function timeIsUp() {
  //          alert("finish");
  //      }
  //
 	// });
</script>
    <!-- end js include path -->
  </body>

</html>
