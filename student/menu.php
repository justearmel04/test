<div class="white-sidebar-color sidebar-container">
 				<div class="sidemenu-container navbar-collapse collapse fixed-menu">
	                <div id="remove-scroll" class="left-sidemenu">
	                    <ul class="sidemenu  page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
	                        <li class="sidebar-toggler-wrapper hide">
	                            <div class="sidebar-toggler">
	                                <span></span>
	                            </div>
	                        </li>
	                        <li class="sidebar-user-panel">
	                            <div class="user-panel">
	                                <div class="pull-left image">
	                                    <img src="<?php echo $_SESSION['user']['photolink'];?>" class="img-circle user-img-circle" alt="User Image" />
	                                </div>
	                                <div class="pull-left info">
	                                    <p> <?php echo $tablogin[0];?></p>
	                                    <small>
                                        <?php

                                          if($tablogin[1]=="Admin_globale")
                                          {
                                            echo "Administrateur";
                                          }else if($tablogin[1]=="Admin_local")
                                          {
                                            echo "Admin Local";
                                          }else if($tablogin[1]=="Teatcher")
                                          {
                                            echo "Professeur";
                                          }else if($tablogin[1]=="Parent")
                                          {
                                            echo "Parent";
                                          }
                                        ?>
                                      </small>
	                                </div>
	                            </div>
	                        </li>
                          <li class="nav-item active open">
	                            <a href="index.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title"><?php echo L::dashb ?></span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>

                          <li class="nav-item">
	                            <a href="fiche.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title"><?php echo L::FichestudMenu ?></span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <!--li class="nav-item">
                             <a href="#" class="nav-link nav-toggle">
                                 <i class="material-icons">dashboard</i>
                                 <span class="title">Enseignement</span>
                                 <span class="arrow"></span>
                             </a>
                             <ul class="sub-menu">
                                 <li class="nav-item  ">
                                     <a href="#" class="nav-link ">
                                         <span class="title">Syllabus</span>
                                     </a>
                                 </li>
                                 <li class="nav-item  ">
                                     <a href="#" class="nav-link ">
                                         <span class="title">Emplois du temps</span>
                                     </a>
                                 </li>

                             </ul>
                         </li>
                          <li class="nav-item">
                             <a href="#" class="nav-link nav-toggle">
                                 <i class="material-icons">dashboard</i>
                                 <span class="title">Absences</span>
                                 <span class="arrow"></span>
                             </a>
                             <ul class="sub-menu">
                                 <li class="nav-item  ">
                                     <a href="#" class="nav-link ">
                                         <span class="title">Syllabus</span>
                                     </a>
                                 </li>
                                 <li class="nav-item  ">
                                     <a href="#" class="nav-link ">
                                         <span class="title">Emplois du temps</span>
                                     </a>
                                 </li>

                             </ul>
                         </li-->

                            <li class="nav-item">
	                            <a href="courses.php" class="nav-link nav-toggle">
	                                <i class="material-icons">dashboard</i>
	                                <span class="title"><?php echo L::CourseMenu ?></span>
	                                <span class="selected"></span>

	                            </a>

	                        </li>
                          <li class="nav-item">
                            <a href="devoirs.php" class="nav-link nav-toggle">
                                <i class="material-icons">dashboard</i>
                                <span class="title"><?php echo L::DevoirsMenu ?></span>
                                <span class="selected"></span>

                            </a>

                        </li>
                        <li class="nav-item">
                          <a href="quizs.php" class="nav-link nav-toggle">
                              <i class="material-icons">dashboard</i>
                              <span class="title"><?php echo L::QuizMenu ?></span>
                              <span class="selected"></span>

                          </a>

                      </li>
                      <li class="nav-item">
                        <a href="controles.php" class="nav-link nav-toggle">
                            <i class="material-icons">dashboard</i>
                            <span class="title"><?php echo L::ControlsMenu ?></span>
                            <span class="selected"></span>

                        </a>

                    </li>



















                          <!--li class="nav-item ">
	                            <a href="exams.php" class="nav-link nav-toggle">
	                                <i class="material-icons">hotel</i>
	                                <span class="title">Examens de contrôles</span>
	                                <span class="selected"></span>

	                            </a>

	                        </li-->
                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Examens de contrôles</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Examens de contrôles</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Examen de contrôle</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Absences</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Absences</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Absence</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item ">
                              <a href="attendances.php" class="nav-link nav-toggle">
                                  <i class="material-icons">hotel</i>
                                  <span class="title">Présences</span>
                                  <span class="selected"></span>

                              </a>

                          </li-->

                          <!--li class="nav-item  ">
	                            <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
	                                <span class="title">Emploi du temps</span> <span class="arrow"></span>
	                            </a>
	                            <ul class="sub-menu">
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Toutes les Emplois du temps</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="#" class="nav-link "> <span class="title">Ajouter Emploi du temps</span>
	                                    </a>
	                                </li>

	                            </ul>
	                        </li-->

                          <!--li class="nav-item ">
                              <a href="times.php" class="nav-link nav-toggle">
                                  <i class="material-icons">hotel</i>
                                  <span class="title">Emploi du temps</span>
                                  <span class="selected"></span>

                              </a>

                          </li-->



                </div>
            </div>
