<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');

$student= new Student();
$session= new Sessionacade();
$etabs= new Etab();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$libetab=htmlspecialchars(addslashes($_POST['libetab']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$section=htmlspecialchars(addslashes($_POST['section']));
$session=htmlspecialchars(addslashes($_POST['session']));

  $check=$classex->ExisteClasses($libetab,$classe,$session,$section);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));

  $datas=$classex->getAllClassesbyschoolCode($code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune Classe </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner une classe</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $content="";

  $classeId=htmlspecialchars(addslashes($_POST['classe']));

  $datas=$classex->getAllStudentOfThisClasses($classeId);

  foreach ($datas as $value):
      $content .= "<option  selected value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_eleve." ".$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables
$classeId=htmlspecialchars(addslashes($_POST['classe']));

$content="";

$content=$classex->getCodeEtabOfClassesByClasseId($classeId);

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  $code=htmlspecialchars(addslashes($_POST['code']));
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));
   $content="";
   $content.="<option value='' selected>Selectionner une classe</option>";

  $classeSchool=$classex->getAllClassesOfParentHadStudent($code,$parentid,$libellesessionencours);

  foreach ($classeSchool as $value):
      $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
  endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //rechercher la liste des eleves du parent et etant inscrit dans cette classe
  //recuperation des variables

  $code=htmlspecialchars(addslashes($_POST['code']));
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $parentid=htmlspecialchars(addslashes($_POST['parentid']));
  $classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
  $content="";

  $students=$classex->getAllStudentOfParentSchool($code,$classeEtab,$parentid,$libellesessionencours);

  $content.="<option value='' selected>Selectionner un Elève</option>";

  foreach ($students as $value):
      $content .= "<option value='". $value->idcompte_eleve ."' >" . utf8_encode(utf8_decode($value->nom_eleve.' '.$value->prenom_eleve)). "</option>";
  endforeach;

  echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $section=htmlspecialchars(addslashes($_POST['section']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $content="";

  $dataclasse=$classex->getAllsectionClasses($codeEtab,$section,$session);

  $nb=count($dataclasse);

  if($nb==0)
  {
    $content.="<option value='' selected>Aucune classe</option>";
  }else if($nb>0) {
    $content.="<option value='' selected>Selectionner une classe</option>";

    foreach ($dataclasse as $value):
      $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;

  }

echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  //recuperation des variables

  $classeSchool=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));
  $a=1;
  $b=2;
  $classeSchool=substr($classeSchool, 0, -1);
  $classeSchool=str_replace("-",",",$classeSchool);
  $tek=$a.",".$b;

  $classeSchools=trim($classeSchool);
  $content="";

  $classeSchoolsdatas=$etabs->getAllSchoolmessageselected($classeSchools,$session,$codeetab);

  $tabclasses=explode(",",$classeSchools);
  $nbtable=count($tabclasses);

    for($i=0;$i<$nbtable;$i++)
    {


      $classeSchoolsdatas=$etabs->getAllSchoolmessageselected($tabclasses[$i],$session,$codeetab);

      foreach ($classeSchoolsdatas as $value):
        $content .= "<option value='". $value->id_classe ."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";

      endforeach;

      // foreach ($classeSchoolsdatas as $value):
      //   $content=$content."<option value=".utf8_encode(utf8_decode($value->id_classe)).">".utf8_encode(utf8_decode($value->libelle_classe))."</option>"
      // endforeach

    }
    $content1="<option value='allclasses'>Toutes les classes</option>";

    echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));
  $parascolaire=htmlspecialchars(addslashes($_POST['parascolaire']));
  $message=htmlspecialchars(addslashes($_POST['message']));



  $content="";

  //determiner la liste des eleves dans les classes Selectionner


  //nous allons voir si la selection est précise ou collective


  if($precis==0)
  {
    $datas=$student->getAllstudentofMulticlassesSession($classe,$session);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option value=''>Aucun Elève </option>";
    }else {
      //var_dump($datas);
      $content1="<option value='allstudent'>Tous les Elèves</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
      endforeach;
    }
  }else if($precis==1)
  {
    //nous allons recuperer les identifiants de(s) eleves

    $datasids=$student->getallidsofstudentprecis($message,$codeetab,$session);
    $datasids=substr($datasids, 0, -1);

    $tabids=explode("-",$datasids);
    $nbids=count($tabids);

    for($i=0;$i<$nbids;$i++)
    {
      $studentid=$tabids[$i];
      $donnees=$student->getEmailParentOfThisStudentByID($studentid,$session);
      $tabdonnees=explode("*",$donnees);

        $content .= "<option value='".$tabdonnees[6]."' >" . utf8_encode(utf8_decode($tabdonnees[1]." - ".$tabdonnees[2])). "</option>";

    }

  }




echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  //recuperation de la variable

  $section=htmlspecialchars(addslashes($_POST['section']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

  $content="";

  $datas=$classex->getAllclassesofsection($section,$session,$codeEtab);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucune classe </option>";
  }else {
    $content="<option value=''>Selectionner une classe</option>";

    foreach ($datas as $value):
        $content .= "<option value='".$value->id_classe."' >" . utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;

}



 ?>
