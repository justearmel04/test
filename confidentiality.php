<?php
session_start();
require_once('intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('langcache');
$i18n->setFilePath('intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
 ?>
<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title> Application de communication ecoles et parents </title>

<meta name="description" content=" Xschool'ing est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle � �t� con�ue pour r�pondre � des probl�mes que nous observons et pour suivre de pr�s l'�volution de nos enfants, alors commen�ons maintenant.">

<meta name="keywords" content=" Application du domaine éducatif, Application de communication parents- établissments , Application mobile  et Desktop pour Etablissement">

<meta name="author" content="Xschool'ing | xschool'ing.com">

 <meta property="og:url" content="" />
 <meta property="og:title" content="Xschool'ing - Application de communication ecoles et parents" />
 <meta property="og:description" content="Application du domaine éducatif, Application de communication parents établissment , Application android et Desktop pour Etablissement." />

<!-- Mobile Specific -->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<!-- CSS -->
<link href="" type="image/x-icon" rel="icon" /><link href="" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/component43a0.css?v3" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/bootstrap.min1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/animate1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/simple-line-icons1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/icomoon-soc-icons1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/magnific-popup1b26.css?v2" />
	 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
	  <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/style43a0.css?v3" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/style-gold1b26.css?v2" />
	<link rel="stylesheet" type="text/css" href="xchool'ink/css/v2/home/custom43a0.css?v3" />
<script type="text/javascript" src="xchool'ink/js/vendor/jquery-1.11.0.min1b26.js?v2"></script>
<!--[if lt IE 10]>
<link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lt IE 9]>

	<link rel="stylesheet" type="text/css" href="/css/vendor/html5shiv.js.css" />
<![endif]-->
	<style>
    #preloader2 {position: fixed;top: 0;left: 0;right: 0;bottom: 0;background-color: #fff;
        /* change if the mask should have another color then white */
        z-index:9999;
        /* makes sure it stays on top */}
    #status {width: 64px;height: 64px;position: absolute;left: 50%;
        /* centers the loading animation horizontally one the screen */
        top: 50%;
        /* centers the loading animation vertically one the screen */
        background-image: url(img/v2/home/preloader1b26.gif?v2);
        /* path to your loading animation */
        background-repeat: no-repeat;
        background-position: center;
        margin: -100px 0 0 -100px;
        /* is width and height divided by two */
    }
</style>
</head>
<body data-spy="scroll" data-target=".navMenuCollapse">
<!--[if lt IE 7]>
    <<![endif]-->
<div id="preloader2">
    <div id="status">&nbsp;</div>
</div>

<div id="wrap" class="wrapper_all">
	<div id="home"></div>

 <nav class="navbar navbar-fixed-top navbar-slide">
    	<script type="text/javascript">


</script>
	<div class="container_fluid header_custom_tab">
								<a class="navbar-brand goto" href="#wrap" style="padding-right:0px;">
					<img src="xchool'ink/img/v2/home/logo_nav1b26.png?v2" height="20" width="91" alt="Xchool'ing" />				</a>
								<a class="contact-btn icon-envelope" data-toggle="modal" data-target="#modalContact" title=" Cliquer ici pour commander une démo par mail "></a>
				<button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<ul class="nav" style="padding-left:0px;">
									<li><a href="#home" style=" font-size:15px;"><?php echo L::Homestartindex ?></a> </li>
						<li><a href="#features" style=" font-size:12px;">Fonctionnalités</a> </li>

						<li><a href="#benefits1" style=" font-size:12px;">Bénéfices</a></li>

						<li><a href="#online" style=" font-size:12px;"> Cours  en ligne </a> </li>

						<li><a href="#benefits2" style=" font-size:12px;"> Pourquoi Xschool’ink ?  </a></li>




							<li class="dropdown language-switch">
                <?php

                  if($_SESSION['user']['lang']=="fr")
                  {
                    ?>
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                        src="assets2/img/flags/french_flag.jpg" style="width:17px;"  class="position-left" alt=""> Francais <span
                        class="fa fa-angle-down"></span>
                    </a>
                    <?php
                  }else if($_SESSION['user']['lang']=="en")
                  {
                    ?>
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img
                        src="assets2/img/flags/gb.png" style="width:17px;"  class="position-left" alt=""> English <span
                        class="fa fa-angle-down"></span>
                    </a>

                    <?php
                  }

                 ?>

							<ul class="dropdown-menu">
								<li onclick="addFrench()">
									<a class="french"><img src="assets2/img/flags/french_flag.jpg" alt="" style="width:17px;" > Francais</a>
								</li>

								<li onclick="addEnglish()">
									<a class="english"><img src="assets2/img/flags/gb.png" alt="" > English</a>
								</li>


							</ul>
						</li>


					<ul class="reg-log-common">


					<li class="reg-buttns">
						<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">

						  <button type="button" class="actual-reg-btn"> <a href="signup.php" style="margin-top:-10px;padding-left:7px; font-size:12px;"> MEMBRES</a>

						 </button>

						</div>
					</li>

					<li class="reg-buttns">
						<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">

						  <button type="button" class="actual-reg-btn"> <a href="subscribe.php" style="margin-top:-10px;padding-left:10px;font-size:12px;"> PARENTS</a>

						 </button>

						</div>
					</li>
					</ul>



					</ul>
				</div>
			</div>
   </nav>
   <script type="text/javascript">
$(document).ready(function(){
	$("#errorMsg").fadeIn()
	.css({top:0,position:'fixed'})
	.animate({top:0}, 1000, function() {
	    //callback
	});
});
</script>
<script type="text/javascript">setTimeout(function(){ $('#errorMsg').slideUp(700).delay(3000);}, 3000);</script>


<!-- INTRO BEGIN -->
	<header id="blog-intro" class="intro-block bg-color-main" >
		<div class="container">
			<h1 class="slogan">POLITIQUE DE CONFIDENTIALITÉ</h1>
		</div>
				<div class="terms-bg" data-stellar-ratio="0.5"></div>
	</header>
	<!-- INTRO END -->

	<!-- BLOG BEGIN -->
	<section id="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12 blog-content-sidebar">
					<div class="post-content">


<h4></h4>

<p><?php echo L::Policyparagraph1 ?></p>

<h4><?php echo L::Intoparagraph2 ?></h4>

<p><?php echo L::Policyparagraph2 ?></p>

<p><?php echo L::Policyparagraph3 ?></p>

<h4><?php echo L::Introparagraph4 ?></h4>

<p><?php echo L::Datacollect ?><br />
	<ul>
		<li><?php echo L::Nomprecollection ?></li>
		<li><?php echo L::Electroniquecollection ?></li>
		<li><?php echo L::Sexecollection ?></li>
		<li><?php echo L::Agecollection ?></li>
		<li><?php echo L::Academicdata ?></li>
	</ul>
</p>
<p><?php echo L::Policyparagraph5 ?></p>

<h4><?php echo L::FichierJournauxcollection ?></h4>

<p><?php echo L::FichierDetailscollection ?></p>
<p>
<ul>
	<li><?php echo L::Fichier1collection ?></li>
	<li><?php echo L::Fichier2collection ?></li>
	<li><?php echo L::Fichier3collection ?></li>
</ul>
</p>

<h4><?php echo L::Policyparagraph6 ?></h4>

<p><?php echo L::Avanceparagraph6 ?></p>
<p>
<ul>
	<li><?php echo L::ParagraphList1 ?></li>
	<li><?php echo L::ParagraphList2 ?></li>
	</ul>
</p>
<p><?php echo L::ParagraphEntete4 ?></p>
<p>
	<ul>
		<li><?php echo L::ParagraphList4 ?></li>
		<li><?php echo L::ParagraphList5 ?></li>
		<li><?php echo L::ParagraphList6 ?></li>
		<li><?php echo L::ParagraphList7 ?></li>
	</ul>
</p>
<p><?php echo L::ParagraphEntete8 ?></p>
<p>
	<li><?php echo L::ParagraphList8 ?></li>
	<li><?php echo L::ParagraphList9 ?></li>
</p>
<p><?php echo L::ParagraphEntete10 ?></p>
<p>
	<li><?php echo L::ParagraphList10 ?></li>
	<li><?php echo L::ParagraphList11 ?></li>
	<li><?php echo L::ParagraphList12 ?></li>
</p>

<p><?php echo L::ParagraphList13 ?></p>
<h4><?php echo L::Droitpolicy ?></h4>
<p>
	<?php echo L::Droitpolicy1 ?> <br> <?php echo L::Droitpolicy2 ?> <br> <?php echo L::Droitpolicy3 ?> <br> <?php echo L::Droitpolicy4 ?> <br> <?php echo L::AdresseDroit1 ?>
</p>
<h4><?php echo L::AccessDroit ?></h4>
<p><?php echo L::AccessDroitpara1 ?> <br> <?php echo L::AccessDroitpara2 ?> <br> <?php echo L::AccessDroitpara3 ?> <br> <?php echo L::AccessDroitpara4 ?> </p>
<h4><?php echo L::IntroDivulgation ?></h4>
<p>
<?php echo L::divulgationText1 ?> <br> <?php echo L::divulgationText2 ?> <br> <?php echo L::divulgationText3 ?>
</p>
<h4><?php echo L::securityText ?></h4>
<p>
<?php echo L::securityText1 ?> <br>
<ul>
	<li><?php echo L::securityText2 ?></li>
	<li><?php echo L::securityText3 ?></li>
	<li><?php echo L::securityText4 ?></li>
	<li><?php echo L::securityText5 ?></li>
	<li><?php echo L::securityText6 ?></li>
	<li><?php echo L::securityText7 ?></li>
	<li><?php echo L::securityText8 ?></li>
</ul>
<br>
<?php echo L::securityText9 ?>
</p>

<h4><?php echo L::Updatetext ?></h4>
<p>
<?php echo L::Updatetext1 ?>
</p>




	<!-- DOWNLOAD BEGIN -->

	<!-- DOWNLOAD END -->     <!-- FOOTER BEGIN -->
	<footer id="footer">
       <div class="container">
<div class="row">
	<div class="col-md-6 footer-left">
		<p class="copyright">

			<a href=""> © 2019 All rights reserved - PROXIMITY SA </a> <br>

		</p>
			 <a> (+237) 233 43 45 74 - info@proximity-cm.com </a> <br>

			 <a> 76 AVENUE DE L'INDÉPENDANCE, BP 4791 DOUALA CAMEROUN </a>
			 <ul class="soc-list footer_links">
				 <li><a href="confidentiality.php">Terms of Use</a></li>

			 </ul>

		<!-- <p class="madewithlove">
			Powered by <a href="https://www.2basetechnologies.com" target="_blank"  alt="mobile application development company">2Base Technologies Pvt. Ltd</a>
		</p> -->
	</div>

<div class="col-md-6 footer-right">
	<ul class="soc-list social wow flipInX">
		<li><a href="https://twitter.com/ProximitySA" target="_blank"><i class="icon soc-icon-twitter"></i></a></li>
		<li><a href="https://www.facebook.com/Proximity-SA-168936656574928" target="_blank"><i class="icon soc-icon-facebook"></i></a></li>
		<!-- <li><a href="https://plus.google.com/113125530968548562855/" target="_blank"><i class="icon soc-icon-googleplus"></i></a></li> -->
		<li><a href="https://www.linkedin.com/company/proximity-sa/" target="_blank"><i class="icon soc-icon-linkedin"></i></a></li>
	</ul>
</div>
</div>
</div>
<!--<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63066262-1', 'auto');
  ga('send', 'pageview');

</script><script type="text/javascript">

// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='/js/v2/tawkto.js';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();


var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a8aa3f7d7591465c707ca41/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); -->

</script>
  	</footer>
	<!-- FOOTER END -->
</div>



	<script type="text/javascript" src="xchool'ink/js/bootstrap.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/owl.carousel.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/classiedeea.js?new_theme"></script>

	<script type="text/javascript" src="xchool'ink/js/v2/home/modernizr.customdeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/uiMorphingButton_fixeddeea.js?new_theme"></script>


<script type="text/javascript">
// $(function() {
//     $(".rslides").responsiveSlides({
// 		pager: true,
// 		});
//     if($('#flashMessage').length){
// 	    var onclick1 = "$('#errorMsg').slideUp(500).delay(7000)";
// 		jQuery('header').prepend('<div id="errorMsg" class="multimsg" style="position: absolute; top: 0px; display: block;z-index: 10;"><div class="flash flash_success">'+$('#flashMessage').html()+'</div><a href="javascript:void(0);" onclick='+onclick1+' class="msg_close" alt=""><span></span></a></div>');
// 		$("#errorMsg").fadeIn()
// 		.css({top:-100,position:'fixed'})
// 		.animate({top:0}, 800, function() {
// 		    //callback
// 			setTimeout(function() {
// 				$("#errorMsg").slideUp(500).delay(7000);
// 			}, 5000);
// 		});
//     }
//   });


(function() {
	var docElem = window.document.documentElement, didScroll, scrollPosition;

	// trick to prevent scrolling when opening/closing button
	function noScrollFn() {
		window.scrollTo( scrollPosition ? scrollPosition.x : 0, scrollPosition ? scrollPosition.y : 0 );
	}

	function noScroll() {
		window.removeEventListener( 'scroll', scrollHandler );
		window.addEventListener( 'scroll', noScrollFn );
	}

	function scrollFn() {
		window.addEventListener( 'scroll', scrollHandler );
	}

	function canScroll() {
		window.removeEventListener( 'scroll', noScrollFn );
		scrollFn();
	}

	function scrollHandler() {
		if( !didScroll ) {
			didScroll = true;
			setTimeout( function() { scrollPage(); }, 60 );
		}
	};

	function scrollPage() {
		scrollPosition = { x : window.pageXOffset || docElem.scrollLeft, y : window.pageYOffset || docElem.scrollTop };
		didScroll = false;
	};

	scrollFn();

	[].slice.call( document.querySelectorAll( '.morph-button' ) ).forEach( function( bttn ) {
		new UIMorphingButton( bttn, {
			closeEl : '.close',
			onBeforeOpen : function() {
				// don't allow to scroll
				noScroll();
			},
			onAfterOpen : function() {
				// can scroll again
				canScroll();
			},
			onBeforeClose : function() {
				// don't allow to scroll
				noScroll();
			},
			onAfterClose : function() {
				// can scroll again
				canScroll();
			}
		} );
	} );

	// for demo purposes only
	[].slice.call( document.querySelectorAll( 'form button' ) ).forEach( function( bttn ) {
		bttn.addEventListener( 'click', function( ev ) { ev.preventDefault(); } );
	} );
})();

$(function(){
$('.add_more_btn').click(function(){
	 html= ' <div class="block clearfix"><div class="col-md-6"><div class="input text cool"><input type="text" placeholder="Student Name" name="data[User][student_name][]"></div></div><div class="col-md-6"><a href="javascript:;" onclick="$(this).parent(\'div\').parent(\'div\').remove();" title="Remove" class="add_more_btn">-</a>	</div></div>';
	 var n = $( "div.cool" ).length;
	 if(n>=5){
    	alert("You can add only 6 students");
	 }else{
     	$("div#student_names").append(html);
	 }
     return false;
});

	// the element inside of which we want to scroll
        var $elem = $('#UserRegisterForm');
       // clicking the "down" button will make the page scroll to the $elem's height
	$('.add_more_btn').click(
		function (e) {
			$('#UserRegisterForm').animate({scrollTop: $elem.height()}, 800);
		}
	);

});
</script>

	<script type="text/javascript" src="xchool'ink/js/v2/home/wow.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.smooth-scroll.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.superslides.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/placeholders.jquery.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.magnific-popup.mindeea.js?new_theme"></script>

	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.stellar.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/retina.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.validate.mindeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/customdeea.js?new_theme"></script>
	<script type="text/javascript" src="xchool'ink/js/v2/home/jquery.slimscroll.mindeea.js?new_theme"></script>

<!-- Preloader -->
<script type="text/javascript">
    //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader2').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow':'visible'});
        })
    //]]>
</script>
<!--[if lte IE 9]>
	<script src="/js/v2/home/respond.min.js"></script>
<![endif]-->
</body>
</html>
