<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();



$emailUti=$_SESSION['user']['email'];



$user=new User();

$etabs=new Etab();

$localadmins= new Localadmin();

$parents=new ParentX();

$classe=new Classe();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);

$datastat=$user->getStatis();

$tabstat=explode("*",$datastat);

$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}

// tdllezie recuperation du jour en cour
setlocale(LC_TIME, "fr_FR");


// echo "Premier jour de cette semaine est: ", $premierJour ." / ".$deadLine;
// tdllezie recuperation du jour en cour

$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);


$concatstudentids="";

// var_dump($parentlyStudent);
foreach ($parentlyStudent as $value):
  $concatstudentids=$concatstudentids.$value->id_compte.",";
endforeach;

$concatstudentids=substr($concatstudentids, 0, -1);

$montantAttente=$parents->getAllversementOfParentAttente($concatstudentids,$compteuserid);
$montantpaiements=$parents->getAllversementOfParent($concatstudentids,$compteuserid);
$versementHisto=$parents->getHistoriqueversement($concatstudentids,$compteuserid);



// $alletab=$etabs->getAllEtab();

// $locals=$localadmins->getAllAdminLocal();

// $allparents=$parents->getAllParent();

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);
//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


 ?>

<!DOCTYPE html>

<html lang="en">

<!-- BEGIN HEAD -->



<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <title><?php echo L::Titlepage?></title>

    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



    <!-- google font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

	<!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!--bootstrap -->

   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- data tables -->

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- morris chart -->

    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />



	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style media="screen">

    </style>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>

 <!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">

    <div class="page-wrapper">

        <!-- start header -->

		<?php

include("header.php");

    ?>
        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

 			<!-- start sidebar menu -->

 			<?php

				include("menu.php");

			?>

			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title"><?php echo L::dashb  ?></div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="active"><?php echo L::dashb  ?></li>

                            </ol>

                        </div>

                    </div>

					<!-- start widget -->

					<div class="state-overview">

						<div class="row">
					        <!-- /.col -->
					        <!-- /.col -->
					        <!-- /.col -->
					        <!-- /.col -->
					      </div>

						</div>
					<!-- end widget -->
          <?php
          if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>
                <?php

                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>



                <div class="row">

                  <div class=" col-md-6 col-sm-12 col-12">
                     <div class="card  card-box">
                           <div class="card-head" style="background-color:#f68a24">
                               <header style="color:#fff"><?php echo L::listeDevoirEleveParent  ?></header>
                               <div class="tools">
                                   <a style="color:#fff" class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                 <a style="color:#fff" id="devoirschevron" class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                 <a style="color:#fff" class="t-close btn-color fa fa-times" href="javascript:;"></a>
                               </div>
                           </div>
                           <div class="card-body no-padding height-9">
                             <?php
                             foreach ($parentlyStudent as $value):

                               ?>
                               <div class="row">
                                 <div class="offset-md-4">
                                   <?php echo $value->nom_eleve." ".$value->prenom_eleve ?>
                                 </div>
                               </div><br>
                               <div class="row">
                                   <div class="noti-information notification-menu">
                                       <div class="notification-list mail-list not-list small-slimscroll-style">
                                         <?php
                                         $courses=$parents->getAllStudentDevoirsofParent($_SESSION['user']['IdCompte'],$value->idcompte_eleve);
                                         // var_dump($courses); exist;
                                        foreach ($courses as $value):
                                          $tabdate=explode("-",$value->datelimite_dev);
                                          ?>

                                          <a href="javascript:;" class="single-mail">

                                           <span class="label label-info" style="margin-left:10px;"><?php echo  $value->libelle_mat?></span>  <span class=" pull-right"> <?php echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0] ?> </span>
                                          </a>

                                             <?php
                                           endforeach;
                                              ?>

                                       </div>

                                   </div>
                               </div>
                               <?php
                             endforeach;
                                ?>

                           </div>
                       </div>
                   </div>

                    <!-- tdllezie2 -->
                          <div class=" col-md-6 col-sm-12 col-12">
                             <div class="card  card-box">
                                   <div class="card-head" style="background-color:#58b947">
                                       <header style="color:#fff"><?php echo L::assidute  ?></header>
                                       <div class="tools" >
                                           <a style="color:#fff" class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                         <a style="color:#fff" id="assuiditechevron" class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                         <a style="color:#fff" class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                       </div>
                                   </div>
                                   <div class="card-body no-padding height-9">
                                     <?php
                                     foreach ($parentlyStudent as $value):

                                       ?>
                                       <div class="row">
                                         <div class="offset-md-4">
                                           <?php echo $value->nom_eleve." ".$value->prenom_eleve ?>
                                         </div>
                                       </div><br>
                                       <div class="row">
                                           <div class="noti-information notification-menu">
                                               <div class="notification-list mail-list not-list small-slimscroll-style">
                                                 <?php
                                                 $premierJour = strftime("%Y-%m-%d", strtotime("this week"));
                                                 $deadLine = date("Y-m-d", strtotime("+5 day", strtotime($premierJour)));
                                                 $GetPresenceStudent=$parents->getAllStudentAssiduteofParent($_SESSION['user']['IdCompte'],$value->idcompte_eleve,$premierJour,$deadLine);
                                                 // var_dump($GetPresenceStudent);
                                                foreach ($GetPresenceStudent as $value):
                                                  $tabdate=explode("-",$value->date_presence);
                                                  ?>

                                                <a href="javascript:;" class="single-mail">
                                                  <?php
                                                  if ($value->statut_presence==0)
                                                  {
                                                   ?>
                                                   <span class="label label-info" style="margin-left:10px;"><?php echo L::EtatAssiduteAbsence  ?></span> &nbsp; &nbsp; <?php echo L::Determinant  ?> &nbsp; <span class=" pull-right"> <?php echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0] ?> </span>

                                                   <?php
                                                 }else {
                                                      if ($value->statut_presence==2)
                                                      {
                                                       ?>
                                                         <span class="label label-info" style="margin-left:10px;"><?php echo L::EtatAssiduteRetard  ?> </span>
                                                       <?php
                                                     }
                                                 }
                                                   ?>
                                                      <span class="notificationtime">
                                                          <small></small>
                                                      </span>
                                                  </a>


                                                     <?php
                                                   endforeach;
                                                      ?>

                                               </div>

                                           </div>
                                       </div>
                                       <?php
                                     endforeach;
                                        ?>

                                   </div>
                               </div>
                           </div>
                           <!-- tdllezie 3 -->

                           <div class=" col-md-6 col-sm-12 col-12">
                              <div class="card  card-box" >
                                    <div class="card-head" style="background-color:#3a5ae2">
                                        <header style="color:#fff" ><?php echo L::listeNte  ?></header>
                                        <div class="tools">
                                            <a style="color:#fff" class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                          <a style="color:#fff" id="noteschevron" class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                          <a style="color:#fff"  class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="card-body no-padding height-9">
                                      <?php
                                      foreach ($parentlyStudent as $value):

                                        ?>
                                        <div class="row">
                                          <div class="offset-md-4">
                                            <?php echo $value->nom_eleve." ".$value->prenom_eleve ?>
                                          </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="noti-information notification-menu">
                                                <div class="notification-list mail-list not-list small-slimscroll-style">
                                                  <?php
                                                  $courses=$parents->getAllStudentNoteofParent($_SESSION['user']['IdCompte'],$value->idcompte_eleve);
                                                  // var_dump($courses); exist;
                                                 foreach ($courses as $value):
                                                   ?>
                                                   <a href="javascript:;" class="single-mail">

                                                    <span class="label label-success" style="margin-left:10px;"><?php echo  $value->libelle_notes?></span>
                                                    <span class=" pull-right"> <?php echo L::ListeNotesEleveParent  ?>: <?php echo  $value->valeur_notes?> </span>
                                                   </a>
                                                      <?php
                                                    endforeach;
                                                       ?>
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                      endforeach;
                                         ?>

                                    </div>
                                </div>
                            </div>

                                   <div class=" col-md-6 col-sm-12 col-12">
                                      <div class="card  card-box" >
                                            <div class="card-head" style="background-color:#f64f5f">
                                                <header style="color:#fff"> <?php echo L::EmploiDuTemps  ?></header>
                                                <div class="tools">
                                                    <a style="color:#fff" class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                                  <a style="color:#fff" id="emploischevron" class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                                  <a style="color:#fff" class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                </div>
                                            </div>
                                            <div class="card-body no-padding height-9">
                                              <?php
                                              foreach ($parentlyStudent as $value):

                                                ?>
                                                <div class="row">
                                                  <div class="offset-md-4">
                                                    <?php echo $value->nom_eleve." ".$value->prenom_eleve ?>
                                                  </div>
                                                </div><br>
                                                <div class="row">
                                                    <div class="noti-information notification-menu">
                                                        <div class="notification-list mail-list not-list small-slimscroll-style">
                                                          <?php
                                                          // date_default_timezone_get('Africa/Abidjan'); // fuseau horaire
                                                          setlocale(LC_TIME,"fr_FR", "fra", "fre"); // locale

                                                          $dateday=date("Y-m-d");
                                                          // $dateday="2020-09-21";

                                                          $libellejour=strtoupper(strftime('%a' ,strtotime($dateday)));
                                                          $date_jour= substr($libellejour, 0, -1);



                                                          $courses=$parents->getAllAbssanceStudentofParent($_SESSION['user']['IdCompte'],$value->idcompte_eleve,$value->codeEtab_eleve,$date_jour);
                                                          // var_dump($courses); exist;
                                                         foreach ($courses as $value):

                                                           ?>
                                                           <a href="javascript:;" class="single-mail">

                                                            <span class="label" style="margin-left:10px;background-color:#f68a24"><?php echo  $value->libelle_routine?></span>
                                                            <span class=" pull-right"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<?php echo  returnHours($value->debut_route)?>  -  &nbsp;<?php echo  returnHours($value->fin_route)?>  </span>
                                                              <!-- &nbsp; &nbsp; &nbsp; &nbsp; <?php //echo  $value->day_route?> -->
                                                               <span class="notificationtime">
                                                                   <small></small>
                                                               </span>
                                                           </a>

                                                              <?php
                                                            endforeach;
                                                               ?>

                                                        </div>

                                                    </div>
                                                </div>
                                                <?php
                                              endforeach;
                                                 ?>

                                            </div>
                                        </div>
                                    </div>
                                  <!-- tdllezie4 -->
                              </div>

                              <div class="row">

                                <div class="col-md-12 col-sm-12">
                                  <div class="state-overview">

                                   <div class="row">
                                         <!-- /.col -->

                                         <div class="col-lg-6 col-sm-12">

                                       <div  style="color: #fff" class="overview-panel orange">

                                         <div class="symbol">

                                           <i class="fa fa-money"></i>

                                         </div>

                                         <div class="value white">

                                           <p style="color: white" class="sbold addr-font-h1" data-counter="counterup" data-value=""><?php echo strrev(wordwrap(strrev($montantAttente), 3, ' ', true)); ?></p>

                                           <p style="color: white"><?php echo L::StandbyPaiement ?></p>

                                         </div>

                                       </div>

                                     </div>

                                         <!-- /.col -->

                                          <div class="col-lg-6 col-sm-12">

                                       <div class="overview-panel blue-bgcolor">

                                         <div class="symbol">

                                            <i class="fa fa-money"></i>

                                         </div>

                                         <div class="value white">

                                           <p style="color: white" class="sbold addr-font-h1" data-counter="counterup" data-value=""><?php echo strrev(wordwrap(strrev($montantpaiements), 3, ' ', true)); ?></p>

                                           <p style="color: white"><?php echo strtoupper(L::Paiements) ?></p>

                                         </div>

                                       </div>

                                     </div>

                                         <!-- /.col -->
                                         <!-- /.col -->

                                       </div>

                                   </div>
                                </div>


                                    </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                    <div class="card  card-box">

                                          <div class="card-body ">
                                            <div class="heading-layout1">
                                                <div class="item-title">
                                                    <h3><?php echo L::Paiementstories ?></h3>
                                                </div>

                                            </div>

                                              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                  <thead>
                                                      <tr>
                                                        <th style="text-align:center"><?php echo L::Versements ?> </th>
                                                        <th style="text-align:center"><?php echo L::LittleDateVersements ?></th>
                                                        <th style="text-align:center"><?php echo L::AmountVerser ?></th>
                                                        <th style="text-align:center"><?php echo L::MotifVersementss ?></th>
                                                          <!--th> Actions </th-->
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                    <?php
                                                    foreach ($versementHisto as $value):
                                                     ?>
                                                     <tr>
                                                       <td style="text-align:center"><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                                       <td style="text-align:center"><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                                       <td style="text-align:center"><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                                       <td style="text-align:center"><span class="label label-sm label-warning"><?php echo $value->motif_versement;?></span></td>
                                                     </tr>

                                                      <?php
                                                    endforeach;
                                                       ?>


                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                    </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 	<script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

     <!-- Common js-->

 	<script src="../assets2/js/app.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

 	<script src="../assets2/js/theme-color.js" ></script>

 	<!-- Material -->

 	<script src="../assets2/plugins/material/material.min.js"></script>









    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   $(document).ready(function() {

$("#emploischevron").click();
$("#assuiditechevron").click();
$("#devoirschevron").click();
$("#noteschevron").click();





   });



   </script>

    <!-- end js include path -->

  </body>



</html>
