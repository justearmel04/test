<?php
session_start();

require_once('single/Etablissement.php');
$etabs=new Etab();
//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$pays=$etabs->getAllCountriesOfSystem();
$alletab=$etabs->getAllEtab();

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Application de communication écoles et parents</title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
	<link href="assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="assets2/css/pages/steps.css">
	<link href="assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->
    <link href="assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
<div class="page-wrapper">
  <div class="container">
    <div class="row" style="margin-top:15px;" id="programmepdf">

      <div class="col-sm-12 col-md-12">
                             <div class="card-box">
                                 <div class="card-head">
                                     <header>Formulaire d'abonnement aux Notifications XSCHOOL</header>
                                 </div>
                                 <div class="card-body ">
                                   <form class="" action="controller/subscription.php" method="post" id="subscribeform">

                                     <h3>Enregistrement Parent</h3>
                                     <fieldset>
 									        <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Informations Personnelles du parent ou du tuteur de l'élève  </div>
                        <br>
                        <div class="row">
                                            <div class="col-md-6">
                        <div class="form-group">
                        <label for=""><b>Nom<span class="required"> * </span>: </b></label>

                      <input type="text" name="nomTea" id="nomTea" data-required="1" placeholder="Entrer le Nom" class="form-control input-height " />
                        </div>

                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                               <label for=""><b>Prénoms <span class="required"> * </span>: </b></label>
                                <input type="text" name="prenomTea" id="prenomTea" data-required="1" placeholder="Entrer le prénoms" class="form-control input-height" />
                              </div>


                         </div>

                                      </div>
                                      <div class="row">
                                                          <div class="col-md-6">
                                      <div class="form-group">
                                      <label for=""><b>Pays de résidence <span class="required"> * </span>: </b></label>

                                      <select class="form-control input-height" name="pays" id="pays" style="width:100%;" onchange="researchdata()" >
                                          <option value="">Selectionner une Pays</option>
                                          <?php
                                          $i=1;
                                            foreach ($pays as $value):
                                            ?>
                                            <option value="<?php echo utf8_encode(utf8_decode($value->id_pays)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_pays)); ?></option>

                                            <?php
                                                                             $i++;
                                                                             endforeach;
                                                                             ?>

                                      </select>
                                      </div>

                                      </div>

                                      <div class="col-md-6">
                  <div class="form-group">
                  <label for=""><b>Téléphone  <span class="required"> * </span>: </b></label>

                  <input type="text" name="telTea" id="telTea" data-required="1" placeholder="Téléphone" onchange="telchecked()" class="form-control input-height" />
                  </div>

                  </div>


                                                    </div>
                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                    <div class="form-group">
                                                    <label for=""><b>Date de naissance<span class="required">  </span>: </b></label>

                                                    <input type="date" placeholder="Entrer la date de naissance" name="datenaisTea" id="datenaisTea" class="form-control input-height ">

                                                    </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                         <div class="form-group">
                                                           <label for=""><b>CNI<span class="required">  </span>: </b></label>
                                                            <input type="text" name="cniTea" id="cniTea" data-required="1" placeholder="Numéro CNI" class="form-control input-height" />
                                                          </div>


                                                     </div>
                                                    <div class="col-md-6">
                                                         <div class="form-group">
                                                           <label for=""><b>Fonction<span class="required">  </span>: </b></label>
                                                            <input type="text" name="fonctionTea" id="fonctionTea" data-required="1" placeholder="Fonction" class="form-control input-height" />
                                                          </div>


                                                     </div>

                                                     <div class="col-md-6">
                                 <div class="form-group">
                                 <label for=""><b>Genre <span class="required"> * </span>: </b></label>

                                 <select class="form-control input-height" name="sexeTea" id="sexeTea">
                                     <option selected value="">Selectionner le genre</option>
                                     <option value="M">Masculin</option>
                                     <option value="F">Feminin</option>
                                 </select>
                                 </div>

                                 </div>

                                                                  </div>
                                                                  <div class="row">

                                                                  <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label for=""><b>Email <span class="required">  </span>: </b></label>

                                                                  <input type="text" class="form-control input-height " name="emailTea" id="emailTea" placeholder="Entrer l'adresse email">
                                                                  <input type="hidden" name="etape" id="etape" value="1"/>
                                                                  <input type="hidden" id="parentid" name="parentid" value="">
                                                                    </div>


                                                                   </div>

                                                                                </div>
                                                                                <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;" id="header-compte"> Informations de compte  </div>
                                                                                <br/>


                                                                                    <div class="form-group row" id="Login-compte">
                                                                                        <label class="control-label col-md-3">Login
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="loginTea" id="loginTea" type="text" placeholder="Entrer le Login " class="form-control input-height" />

                                                                                           </div>
                                                                                    </div>
                                                                                    <div class="form-group row" id="pass-compte">
                                                                                        <label class="control-label col-md-3">Mot de passe
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="passTea" id="passTea" type="password" placeholder="Entrer le Mot de passe " class="form-control input-height"  /> </div>
                                                                                    </div>
                                                                                    <div class="form-group row" id="repass-compte">
                                                                                        <label class="control-label col-md-3">Confirmation
                                                                                            <span class="required">*  </span>
                                                                                        </label>
                                                                                        <div class="col-md-5">
                                                                                            <input name="confirmTea" id="confirmTea" type="password" placeholder="Confirmer le Mot de passe " class="form-control input-height" /> </div>


                                                                                    </div>
                                     </fieldset>
                                        <h3>Choix Elève</h3>
                                        <fieldset>
                                          <div class=" col-md-12 panel-headin" style="background-color:#009fe3; font-size:20px;color: white;text-align:center;"> Selectionner vos Enfants  </div>
                                          <br/>
                                          <div class="row" style="display:none">
                                                              <div class="col-md-6">
                                          <div class="form-group">
                                          <label for=""><b>Matricule<span class="required"> * </span>: </b></label>

                                        <input type="text"  name="matricule" id="matricule" data-required="1" placeholder="Matricule" class="form-control input-height" />

                                          </div>

                                          </div>
                                          <div class="col-md-6">
                                               <div class="form-group">
                                                 <label for=""><b>Etablissement : </b></label>

                                              <!--select class="form-control input-height" id="etablissement" name="etablissement" style="width:100%" onchange="determinatestudent()"-->
                                                <select class="form-control input-height" id="etablissement" name="etablissement" style="width:100%" >

                                                  <option>Selectionner un etablissement</option>
                                                  <?php
                                                  $i=1;
                                                    foreach ($alletab as $value):
                                                    ?>
                                                    <option value="<?php echo utf8_encode(utf8_decode($value->code_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                                    <?php
                                                                                     $i++;
                                                                                     endforeach;
                                                                                     ?>

                                              </select>

                                                </div>


                                           </div>

                                                        </div>
                                                        <div class="row">
                                                                            <div class="col-md-6" style="display:none">
                                                        <div class="form-group">
                                                        <label for=""><b>Nom & Prénoms<span class="required"> * </span>: </b></label>

                                                      <input type="text"  name="nomcomplet" id="nomcomplet" data-required="1" placeholder="" class="form-control input-height" readonly/>
                                                        </div>

                                                        </div>
                                                        <div class="col-md-6" style="display:none">
                                                             <div class="form-group">
                                                               <label for=""><b>Classe<span class="required"> * </span>: </b></label>
                                                            <input type="text"  name="classeTea" id="classeTea" data-required="1" placeholder="" class="form-control input-height" readonly/>
                                                              </div>


                                                         </div>
                                                         <div class="col-md-4">
                                                           <!--button id="addbutton"  class="btn btn-md btn-warning" data-toggle="modal" data-target="#largeModel"><i class="fa fa-plus"></i> AJOUTER ENFANT </button-->
                                                           <a  id="addbutton" class="btn btn btn-warning btn-sm " data-toggle="modal" data-target="#largeModel" data-animation="zoomInUp">

      <span class="fa fa-plus-circle"></span>
			AJOUTER ENFANT

</a>

<div class="modal fade" id="largeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog modal-lg" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">Selectionner vos enfants</h4>
					                <button type="button" id="bntnclose" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
					                <div id="divrecherchestation">
                            <p>  Rechercher</p>



                              <div class="form-group">

                                <div class="row">

                                  <div class="col-lg-4">
                                    <input type="text"  name="matriculecountry" id="matriculecountry" data-required="1" placeholder="Matricule" class="form-control" onclick="erasedMatriculeSms()" />
                                    <p id="messageMatricule"></p>
                                  </div>
                                  <div class="col-lg-6">
                                    <select class="form-control " id="etablissementcontry" name="etablissementcountry" style="width:100%" onchange="eraseEtabSms()" >

                                      <option value="">Selectionner un etablissement</option>
                                      <?php
                                      $i=1;
                                        foreach ($alletab as $value):
                                        ?>
                                        <option value="<?php echo utf8_encode(utf8_decode($value->code_etab)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_etab)); ?></option>

                                        <?php
                                                                         $i++;
                                                                         endforeach;
                                                                         ?>

                                    </select>
                                    <p id="messageEtab"></p>
                                  </div>
                                  <div class="col-lg-2">
                                    <button type="button" id="btnRechercherStation"  onclick="verification()" class="btn btn-md btn-danger" style="margin-left:-5px;">
                                     <span class="fa fa-search"></span>
                                   </button>
                                  </div>



                                </div>



                              </div>
                              <br/>
                              <table class="table table-striped" style="display:none;">
                          <thead style="background-color:#009fe3; color:white; font-weight: bold;">
                          <tr>
                          <th style="">Nom </th>
                          <th class="visible-lg">Prénoms  </th>
                          <th> Classe</th>
                          <th style="">Etablissemnt</th>
                        </tr>
                         </thead>
                         <tbody id="tabStationBody">

                        <tr>

                          <td colspan="4">Aucun Elève </td>

                        </tr>

                                          </tbody>
                                           </table>
                                           <div id="divafficherinfosstation" style="display:none;">

                                             <div class="panel panel-default">
                                               <div class="panel-heading" style="background-color:#009fe3;color:white;">Informations elève</div>
                                             </div>

                                              <div class="panel-body">

                                                <div class="form-group">

                                                  <div class="row">

                                                    <div class="col-lg-6">
                                                      <input type="text"  name="nomstudent" id="nomstudent" data-required="1" placeholder="Nom et Prénoms" class="form-control" readonly />
                                                      <input type="hidden" name="libelleEtab" id="libelleEtab" value="">
                                                      <input type="hidden" name="studentIdent" id="studentIdent" value="">
                                                    </div>
                                                    <div class="col-lg-6">
                                                      <input type="text"  name="datenaisstudent" id="datenaisstudent"  data-required="1" placeholder="Date de Naissance" class="form-control" readonly />
                                                    </div>

                                                  </div> <br>

                                                  <div class="row">

                                                    <div class="col-lg-6">
                                                      <input type="text"  name="lieunaisstudent" id="lieunaisstudent" data-required="1" placeholder="Lieu de Naissance" class="form-control" readonly/>
                                                    </div>
                                                    <div class="col-lg-6">
                                                      <input type="text"  name="classestudent" id="classestudent" data-required="1" placeholder="Classe" class="form-control"  readonly/>
                                                    </div>

                                                  </div> <br>

                                                  <div class="row">


                                                    <div class="col-lg-6">
                                                      <select class="form-control " id="choicesubscribe" name="choicesubscribe" style="width:100%;text-align:center;" onchange="determineAb()" >

                                                        <option value="">Selectionner un Abonnement</option>
                                                        <option value="1-1000">Abonnement mensuel</option>
                                                        <option value="2-3000">Abonnement trimestriel</option>
                                                        <option value="3-6000">Abonnement semestriel</option>
                                                        <option value="3-12000">Abonnement annuel</option>
                                                      </select>
                                                      <p id="messageAbonnement"></p>
                                                    </div>

                                                    <div class="col-lg-6">
                                                    <input type="text"   name="montantab" id="montantab" data-required="1" placeholder="Montant Abonnement" class="form-control"  readonly/>
                                                    <input type="hidden" name="devisecontry" id="devisecontry" value="">
                                                    </div>


                                                  </div> <br>

                                                  <div class="row">
                                                    <div class="col-md-6">


<button type="button" id="btnAccepter"  onclick="valideAbonnement()" class="btn btn-md btn-primary" >Valider</button>
<button type="button"  id="btnAnnuler"  class="btn btn-md btn-danger" >Annuler</button>


                                    </div>
                                                  </div>

                                                </div>
                                              </div>

                                           </div>


                          </div>
					            </div>
					            <div class="modal-footer">
					                <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal">Close</button>
					                <button type="button" class="btn btn-primary">Save</button>
					            </div>
					        </div>
					    </div>
					</div>

                                                         </div>

                                                       </div> <br>
                                                                      <div class="row">




                              					                        <div class="col-md-12">
                              					                            <div class="card  card-box">
                              					                                <div class="card-head">
                              					                                    <header></header>
                              					                                    <div class="tools">
                              					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                              						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                              						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                              					                                    </div>
                              					                                </div>
                              					                                <div class="card-body ">

                              					                                  <div class="table-scrollable">
                                                                            <input type="hidden" name="studentsarray" id="studentsarray" value="">
                                                                            <input type="hidden" name="subscribearray" id="subscribearray" value="">
                                                                            <input type="hidden" name="subscribeIdarray" id="subscribeIdarray" value="">
                                                                            <input type="hidden" name="nbselect" id="nbselect" value="0">
                                                                            <input type="hidden" name="codeEtabarray" id="codeEtabarray" value="">
                                                                              <input type="hidden" name="studentIdentarray" id="studentIdentarray" value="">
                                                                            <input type="hidden" name="montanttotale" id="montanttotale" value="0">
                                                                            <table class="table table-hover table-checkable order-column full-width" id="example4">
                              					                                        <thead>
                              					                                            <tr>

                                                                                        <th style="text-align:center;">Matricule</th>
                              					                                                <th style="text-align:center;">Nom & prénoms </th>
                                                                                        <th style="text-align:center;">Etablissement </th>
                                                                                        <th style="text-align:center;">Classe </th>
                                                                                        <th style="text-align:center;">Montant </th>
                                                                                         <th> Actions </th>
                              					                                            </tr>
                              					                                        </thead>
                              					                                        <tbody id="tabStationSelectBody">
                                                                                  <tr id="aucuneLinge">
                                                                                    <td colspan="6">Aucune selection</td>
                                                                                  </tr>
                              															                    </tbody>
                                                                                <tfoot>
                                                                                  <tr>
                                                                                    <td colspan='4'> Total Montant</td>

                                                                                    <td colspan="2" id="amount"> <span id="totalamounttab"></span>  <span id="deviseamount"></span> </td>

                                                                                  </tr>
                                                                                </tfoot>
                              					                                    </table>
                              					                                    </div>
                              					                                </div>
                              					                            </div>
                              					                        </div>
                              					                    </div>
                                        </fieldset>
                                        <h3>Paiement</h3>
                                        <fieldset>
                                          <div class="row">
                                             <div class="col-md-8 col-lg-8 col-xs-8">
                                               <div class="card m-b-20">
                                                 <div class="card-body" id="bar-parent">
                                    <form class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Téléphone
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" name="paymentno" id="paymentno" data-required="1" placeholder="entrer le numéro ayant servit au paiement" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Transaction Id
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="text" name="transacId" id="transacId" data-required="1" placeholder="entrer le numéro de transaction Id" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4">Opérateur Mobile
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class="form-control input-height" name="selectmobileop" id="selectmobileop">
                                                        <option value="">Selectionnez un opérateur</option>
                                                        <option value="Category 1">Dr. Rajesh</option>
                                                        <option value="Category 2">Dr. Sarah Smith</option>
                                                        <option value="Category 3">Dr. John Deo</option>
                                                        <option value="Category 3">Dr. Jay Soni</option>
                                                        <option value="Category 3">Dr. Jacob Ryan</option>
                                                        <option value="Category 3">Dr. Megha Trivedi</option>
                                                    </select>
                                                </div>
                                            </div>





										</div>
                                        <!--div class="form-actions">
                                            <div class="row">
                                                <div class="offset-md-3 col-md-9">
                                                    <button type="submit" class="btn btn-info">Submit</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div>
                                            	</div>
                                        	</div-->
                                    </form>

                                    <div class="row" id="mobileInfo">

                                      <div class="col-lg-4">
                        <div class="card m-b-30">
                            <img class="card-img-top img-fluid" src="#" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title font-20 mt-0"><?php //echo utf8_encode($dataquest['numero_mob']);?></h4>
                                <p class="card-text"><?php //echo utf8_encode($dataquest['synthaxe_mob']);?></p>
                                <p class="card-text">
                                </p>
                            </div>
                        </div>
                    </div>

                                    </div>

                                </div>
                                               </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xl-4">
                                              <div class="card bg-white m-b-30">
                            <div class="card m-b-30">
                            <div class="card-header" style="background-color:#dd4b39;color:white;">
                                INFORMATIONS
                            </div>
                            <div class="card-body" >


                                <h5> Pour procéder au paiement mobile vous devez vous référer à la syntaxe correspondant à votre opérateur mobile. Ensuite, entrez le numéro de téléphone utilisé pour le paiement ainsi que le numéro de la transaction Id et cliquez sur valider.
                                  <br> <strong>NB :</strong>  Vous devez inclure les frais de paiement mobile dans le montant de la transaction.</h5>


                            </div>
                        </div>
                        </div>

                                            </div>

                                          </div>
                                        </fieldset>

                                   </form>

                                 </div>
                             </div>
                         </div>

    </div>
  </div>

</div>

    <!-- start js include path -->
    <script src="assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="assets2/plugins/popper/popper.min.js" ></script>
     <script src="assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
  <script src="assets2/plugins/jquery-validation/js/jquery.validate.min.js"></script>

     <!-- bootstrap -->
     <script src="assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- wizard -->
        <script src="assets2/plugins/steps/jquery.steps.js" ></script>
        <script src="assets2/js/pages/steps/steps-data.js" ></script>

 	<script src="assets2/js/app.js" ></script>
     <script src="assets2/js/layout.js" ></script>
 	<script src="assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="assets/js/sweetalert2.min.js"></script>
  <script type="text/javascript" src="assets/js/libs/form-validator/jquery.validate.min.js"></script>


    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function telchecked()
   {
     var telephone=$("#telTea").val();
     var etape=2;
     $.ajax({
       url: 'ajax/login.php',
       type: 'POST',
       async:true,
        data: 'telephone='+telephone+'&etape='+etape,
       dataType: 'text',
       success: function (response, statut) {

        if(response==0)
        {
          $('#header-compte').show();
          $('#Login-compte').show();
          $('#pass-compte').show();
          $('#repass-compte').show();


          //nous allons rechercher le compte id du parent


        }else if(response>0)
        {
          var etape=3;
           $.ajax({
             url: 'ajax/login.php',
             type: 'POST',
             async:true,
              data: 'telephone='+telephone+'&etape='+etape,
             dataType: 'text',
             success: function (response, statut) {
               // parentid
                 $("#etape").val(2);
                 $("#parentid").val(response);

                 // alert(response);

             }
           });
        }

       }});
   }

function researchdata()
{
  //nous allons rechercher la liste des etablissements du pays selectionner

  var pays=$("#pays").val();
  var etape=5;

  if(pays!="")
  {
    $.ajax({
      url: 'ajax/determination.php',
      type: 'POST',
      async:true,
       data: 'pays='+pays+'&etape='+etape,
      dataType: 'text',
      success: function (response, statut) {
        $("#etablissementcontry").html("");
        $("#etablissementcontry").html(response);

        //nous allons rechercher les opérateurs mobile de ce pays

        var etape1=6;

        $.ajax({
          url: 'ajax/determination.php',
          type: 'POST',
          async:true,
           data: 'pays='+pays+'&etape='+etape1,
          dataType: 'text',
          success: function (response, statut) {

            $("#mobileInfo").html("");
            $("#mobileInfo").html(response);


            var etape2=7;

            $.ajax({
              url: 'ajax/determination.php',
              type: 'POST',
              async:true,
               data: 'pays='+pays+'&etape='+etape2,
              dataType: 'text',
              success: function (response, statut) {

                $("#selectmobileop").html("");
                $("#selectmobileop").html(response);

                var etape3=8;

                $.ajax({
                  url: 'ajax/determination.php',
                  type: 'POST',
                  async:true,
                   data: 'pays='+pays+'&etape='+etape3,
                  dataType: 'text',
                  success: function (response, statut) {

                    $("#choicesubscribe").html("");
                    $("#choicesubscribe").html(response);

                  }

                });

              }
            });


          }
        });

      }
    });
  }


}


$("#addbutton").disabled =true;
   function determinatestudent(){

    var matricule=$("#matricule").val();
    var codeEtab=$("#etablissement").val();
    var etape=1;

   $.ajax({

         url: 'ajax/determination.php',
         type: 'POST',
         async:true,
          data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,
         dataType: 'text',
         success: function (response, statut) {

          if(response==0)
        {
          //cet eleve n'existe pas ou n'est pas inscrit pour la session encours

          //nous allons fermer le modal en simulant le click du bouton

          $("#bntnclose").click();



          //$("#largeModel").modal('hide');

           //$("#divafficherinfosstation").slideUp();

          Swal.fire({
          type: 'warning',
          title: 'Attention',
          text: "cet eleve n'existe pas ou n'est pas inscrit pour la session encours",

          })
        }else if(response!=0) {
          var tabdata=response.split("*");
          //nous pouvons recuperer de facon individuelle les variables nom,prenoms et classe

          var nom=tabdata[0];
          var prenom=tabdata[1];
          var classe=tabdata[2];




     $("#nomcomplet").val(nom+" "+prenom);

     //$("#prenom").html(prenom);

     $("#classeTea").val(classe);

     $("#addbutton").prop('disabled', false);

        }

         }
       });
   }

    function erasedMatriculeSms()
   {
   document.getElementById("messageMatricule").innerHTML = "";
   }

   function  eraseEtabSms()
   {
     var codeEtab=$("#etablissementcontry").val();

     if(codeEtab=="")
     {
       document.getElementById("messageEtab").innerHTML = "<font color=\"red\">Merci de selectionner un Etablissement !</font>";

     }else {
       document.getElementById("messageEtab").innerHTML = "";
     }
   }

   function verification()
   {
     var matricule=$("#matriculecountry").val();
     var codeEtab=$("#etablissementcontry").val();

     if(matricule==""||codeEtab=="")
     {
       if(matricule=="")
       {
  document.getElementById("messageMatricule").innerHTML = "<font color=\"red\">Merci de renseigner le Matricule SVP !</font>";
       }

       if(codeEtab=="")
       {
document.getElementById("messageEtab").innerHTML = "<font color=\"red\">Merci de selectionner un Etablissement SVP !</font>";

       }

     }else {
       //nous allons faire la requete

       var etape=1;
       $.ajax({

             url: 'ajax/determination.php',
             type: 'POST',
             async:true,
              data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,
             dataType: 'text',
             success: function (response, statut) {

              if(response==0)
            {
              //cet eleve n'existe pas ou n'est pas inscrit pour la session encours
// $("#bntnclose").click();
            $("#largeModel").modal('hide');
              Swal.fire({
              type: 'warning',
              title: 'Attention',
              text: "cet eleve n'existe pas ou n'est pas inscrit pour la session encours",

              })
            }else if(response!=0) {
              var tabdata=response.split("*");
              //nous pouvons recuperer de facon individuelle les variables nom,prenoms et classe

              var nom=tabdata[0];
              var prenom=tabdata[1];
              var classe=tabdata[2];
              var datenais=tabdata[3];
              var lieunais=tabdata[4];
              var libelleEtab=tabdata[5];
              var studentIdent=tabdata[6];

              $("#divafficherinfosstation").show();;

              $("#nomstudent").val(nom+" "+prenom);
              $("#datenaisstudent").val(datenais);
              $("#lieunaisstudent").val(lieunais);
              $("#classestudent").val(classe);
              $("#libelleEtab").val(libelleEtab);
              $("#studentIdent").val(studentIdent);
         // $("#nomcomplet").val(nom+" "+prenom);
         //
         // //$("#prenom").html(prenom);
         //
         // $("#classeTea").val(classe);
         //
         // $("#addbutton").prop('disabled', false);

            }

             }
           });

     }
   }

   function addmontant(montanttotale,montant)
   {
       var etape=3;
        $.ajax({
          url: 'ajax/determination.php',
          type: 'POST',
          async:true,
           data: 'montanttotale='+montanttotale+'&etape='+etape+'&montant='+montant,
          dataType: 'text',
          success: function (response, statut) {

            return response;
          }
        });
   }

   function retiremontant(montanttotale,montant)
   {
       var etape=4;
        $.ajax({
          url: 'ajax/determination.php',
          type: 'POST',
          async:true,
           data: 'montanttotale='+montanttotale+'&etape='+etape+'&montant='+montant,
          dataType: 'text',
          success: function (response, statut) {

            return response;
          }
        });
   }

   function separateurMillier(val) {

		var montant =Math.round(val*100)/100;

      return montant;

    }

   function valideAbonnement()
   {
     var choicesubscribe=$("#choicesubscribe").val();
     if(choicesubscribe=="")
     {
       document.getElementById("messageAbonnement").innerHTML = "<font color=\"red\">Merci de choisir un Abonnement</font>";

     }else {
       //nous avons selectionner le choix d'abonment

      var  subscribearray=$("#subscribearray").val();
      var  subscribeIdarray=$("#subscribeIdarray").val();
      var  studentsarray=$("#studentsarray").val();
      var  nbselect=$("#nbselect").val();
      var abonnementval=$("#choicesubscribe").val();
      var matricule=$("#matriculecountry").val();
      var codeEtab=$("#etablissementcontry").val();
      var nomcomplet=$("#nomstudent").val();
      var EtabLibelle=$("#libelleEtab").val();
      var classe=$("#classestudent").val();
      var montanttotale=$("#montanttotale").val();
      var studentIdent=$("#studentIdent").val();
      var devisecountry=$("#devisecontry").val();

      var datasubscribe=abonnementval.split("-");
      var montant=datasubscribe[1];
      var idsouscription=datasubscribe[0];
      var concatsouscription=idsouscription+"@"+matricule;

      // var newmontant=addmontant(montanttotale,montant);

      var etape=3;
       $.ajax({
         url: 'ajax/determination.php',
         type: 'POST',
         async:true,
          data: 'montanttotale='+montanttotale+'&etape='+etape+'&montant='+montant,
         dataType: 'text',
         success: function (response, statut) {

           var newmontant=response;

           var id=parseInt(nbselect.trim())+1;


            var ligne = "<tr id=\"ligneselect"+studentIdent+"\">";

            ligne = ligne + $("#ligne" +studentIdent).html();
            ligne = ligne + "<td align=\"center\">"+ matricule+"</td>"
            ligne = ligne + "<td align=\"center\">"+ nomcomplet+"</td>"
            ligne = ligne + "<td align=\"center\">"+ EtabLibelle+"</td>"
            ligne = ligne + "<td align=\"center\">"+ classe+"</td>"
            ligne = ligne + "<td align=\"center\" id=\"montant"+studentIdent+"\">"+ montant+" "+devisecountry+"</td>"
            ligne = ligne + "<td class=\"visible-lg\"><a onclick=\"retirerStation("+studentIdent+",'"+matricule+"','"+codeEtab+"',"+montant+",'"+concatsouscription+"')\" style=\"color:red;font-weight:normal; cursor:pointer\"><span class=\"fa fa-minus-circle\"></span>&nbsp;Retirer</a> </td>";
            ligne = ligne + "</tr>"

            //$("#montanttotale").val(parseInt(montanttotale.trim())+parseInt(montant.trim()));
            $("#montanttotale").val(newmontant);
            $("#subscribearray").val(subscribearray+parseInt(montant.trim())+",");
            $("#subscribeIdarray").val(subscribeIdarray+concatsouscription+",");

            $("#studentsarray").val(studentsarray+matricule+",");
            $("#nbselect").val(id);
            $("#codeEtabarray").val($("#codeEtabarray").val()+codeEtab+",");
            $("#studentIdentarray").val($("#studentIdentarray").val()+studentIdent+",");

            // $("#totalamounttab").val($("#montanttotale").val());
            // $("#deviseamount").val($("#devisecontry").val());


            // amount

            var data="<span>"+parseInt($("#montanttotale").val().trim())+"</span> <span>"+$("#devisecontry").val()+"</span>";
            $("#amount").html(data);

            //nous allons vider les différents champs

            $("#matriculecountry").val("");



             $("#aucuneLinge").slideUp();

            $("#tabStationSelectBody").append(ligne);

            $("#divafficherinfosstation").css("display", "none");
            $("#largeModel .close").click();
         }
       });




     }
   }

   function retirerStation(studentIdent,matricule,codeEtab,montant,concatsouscription)
   {
      //données et ligne à retirer

    var lignearetirer=$("#ligneselect"+studentIdent).val();
    //var montantaretirer=$("#montant"+studentIdent).val();
    var montanttotale=$("#montanttotale").val();

    //var nouveaumontant=retiremontant(montanttotale,montantaretirer);

    var etape=4;
     $.ajax({
       url: 'ajax/determination.php',
       type: 'POST',
       async:true,
        data: 'montanttotale='+montanttotale+'&etape='+etape+'&montant='+montant,
       dataType: 'text',
       success: function (response, statut) {

         var nouveaumontant=response;

         $("#subscribearray").val($("#subscribearray").val().replace(montant+",", ""));
         $("#subscribeIdarray").val($("#subscribeIdarray").val().replace(concatsouscription+",", ""));
         $("#studentsarray").val($("#studentsarray").val().replace(matricule+",", ""));
         $("#studentIdentarray").val($("#studentIdentarray").val().replace(studentIdent+",", ""));
         $("#codeEtabarray").val($("#codeEtabarray").val().replace(codeEtab+",", ""));

         var devisecountry=$("#devisecontry").val();

         $("#montanttotale").val(nouveaumontant);

         var data="<span>"+parseInt($("#montanttotale").val().trim())+"</span> <span>"+$("#devisecontry").val()+"</span>";
         $("#amount").html(data);

         //  $("#totalamounttab").val($("#montanttotale").val());
         //
         // $("#deviseamount").val($("#devisecontry").val());

         $("#ligneselect"+studentIdent).remove();
       }
     });

    // var nouveaumontant=parseInt($("#montanttotale").val().trim())-parseInt(montantaretirer.trim());



    // //$("#montanttotale").val(montanttotale+montant);

    // $("#studentsarray").val(studentsarray+matricule+",");
    //
    // $("#codeEtabarray").val($("#codeEtabarray").val()+codeEtab+",");
    // $("#studentIdentarray").val($("#studentIdentarray").val()+studentIdent+",");

   }

   function determineAb()
   {
     var choicesubscribe=$("#choicesubscribe").val();
     //alert(choicesubscribe);
     var datachoice=choicesubscribe.split("-");
     var montantchoice=datachoice[1];
     var pays=$("#pays").val();
     var etape=2;

     $.ajax({
       url: 'ajax/determination.php',
       type: 'POST',
       async:true,
        data: 'pays='+pays+'&etape='+etape,
       dataType: 'text',
       success: function (response, statut) {

         $("#montantab").val(montantchoice+" "+response);
         $("#devisecontry").val(response);

       }
     });




     //$("#montantab").val(montantchoice);
   }

   $(document).ready(function() {
     "use strict";

     $('#header-compte').hide();
     $('#Login-compte').hide();
     $('#pass-compte').hide();
     $('#repass-compte').hide();




//      $("telTea").change(function() {
//   alert('Handler for .change() called.');
// //ici le code que je voudrai executer
//  });

     var wizard = $("#wizard_test").steps();
     var form = $("#subscribeform").show();

     form.validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
     },
     highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
     },
     success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{
         nomTea:"required",
         prenomTea:"required",
          pays:"required",
          telTea:{
                  required: true,
                  digits:true,
                  minlength: 8,
                  maxlength:12,
              },
        //   datenaisTea:"required",
        //   fonctionTea:"required",
          sexeTea:"required",
        //   emailTea:"required",
        //   loginTea:"required",
        //   passTea: {
        //       required: true,
        //       minlength: 6
        //   },
        //   cniTea:"required",
        //   confirmTea:{
        //       required: true,
        //       minlength: 6,
        //       equalTo:'#passTea'
        //   },
        //
        //  paymentno: {
        //     required: true,
        //     number: true
        // },
        //  transacId:"required",
        //  selectmobileop:"required"

       },
       messages:{
         nomTea:"Merci de renseigner le nom",
         prenomTea:"Merci de renseigner le prénoms",
          pays:"Merci de selectionner le pays",
          telTea:{
            required: "Merci de renseigner un numéro de téléphone",
            minlength: "Le numéro doit contenir au moins 8 caractères",
            digits: "Merci de renseigner des chiffres uniquement ",
            maxlength: "Le nombre de caractère ne peut excéder 12",
              },
          datenaisTea:"Merci de renseigner la date de naissance",
          fonctionTea:"Merci de renseigner la fonction",
          sexeTea:"Merci de selectionner le genre",
          emailTea: {
                     required:"Merci de renseigner l'adresse email",
                     email:"Merci de renseigner une adresse email valide"
                 },
          loginTea:"Merci de renseigner le Login",
          confirmTea:{
              required:"Merci de confirmer le mot de passe",
              minlength:"Le mot de passe doit contenir au moins 6 caractères",
              equalTo: "Merci de renseigner des mots de passe identique"
          },
          passTea: {
              required:"Merci de renseigner le mot de passe",
              minlength:"Le mot de passe doit contenir au moins 6 caractères"
          },
          paymentno: {
             required:"Merci de renseigner le numéro ayant servi au paiement",
             number:"Merci de renseigner un numéro valide"
         },
         cniTea:"Merci de renseigner le numéro CNI",
          transacId:"Merci de renseigner le numéro de transaction Id du paiement mobile",
          selectmobileop:"Merci de selectionner l'opérateur mobile"
       }
     });

     form.steps({
         headerTag: "h3",
         bodyTag: "fieldset",
         transitionEffect: "slideLeft",
         onStepChanging: function (event, currentIndex, newIndex)
          {
                      // Allways allow previous action even if the current form is not valid!
               if (currentIndex > newIndex)
               {
                   return true;
               }

               // Forbid next action on "Warning" step if the user is to young
               if (newIndex === 2 && Number($("#nbselect").val()) < 0)
               {
                   return false;
               }
               // Needed in some cases if the user went back (clean up)
               if (currentIndex < newIndex)
               {
                   // To remove error styles
                   form.find(".body:eq(" + newIndex + ") label.error").remove();
                   form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
               }
               form.validate().settings.ignore = ":disabled,:hidden";
               return form.valid();


          },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#nbselect").val()) >0)
        {
            form.steps("next");
        }

    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
      // form.submit();
      if($("#nbselect").val()>0)
      {
        // form.submit();
        //nous allons verifier si le compte parent existe deja (login et password
        var etape=1;
        var login=$("#loginTea").val();
        var password=$("#passTea").val()
        $.ajax({
          url: 'ajax/login.php',
          type: 'POST',
          async:true,
           data: 'login='+login+'&etape='+etape+'&password='+password,
          dataType: 'text',
          success: function (response, statut) {

          if(response==1)
          {
            Swal.fire({
            type: 'warning',
            title: 'Attention',
            text: 'Un utilisateur existe dejà avec ce login',

          })

          }else if(response==0){
            form.submit();
          }

          }});



      }else {
        Swal.fire({
        type: 'warning',
        title: 'Attention',
        text: 'Vous devez ajouter au moins un enfant dans la liste',

      })
      }
    }


     });




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
