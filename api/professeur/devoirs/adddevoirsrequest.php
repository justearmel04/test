<?php
session_start();
// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


require_once('../../class1/User.php');

require_once('../../class1/cnx.php');

require_once('../../class1/Parent.php');

require_once('../../class1/Etablissement.php');
require_once('../../class1/functions.php');



//$users = new User();

$parents=new ParentX();

$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Récuperation des données de la premiére étape : Infos Client
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$libelledevoir = $request->libellecourse;
$classeEtab = $request->classeEtab;
$codeEtab = $request->codeEtab;
$sessionEtab = $request->sessionEtab;
$instructionsdevoir = $request->detailscourse;
$matiereid = $request->matiereid;
$teatcherid = $request->teatcherid;
$datelimitedevoir=$request->datecourse;
$verouillerdevoir=$request->verouiller;

 $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
 $libelleclasse=$etabs->getInfosofclassesbyId($classeEtab,$sessionEtab);
 $destinataires="";

 $date=date("Y-m-d");

 $tabdate=explode("-",$date);
 $years=$tabdate[0];
 $mois=$tabdate[1];
 $days=$tabdate[2];
 $libellemois=obtenirLibelleMois($mois);

 $devoirid=$etabs->AddDevoirs($libelledevoir,$datelimitedevoir,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$instructionsdevoir,$verouillerdevoir,$destinataires,$statutdevoir);
 $transactionId =  "DEVOIR_".date("Y")."_".$codeEtab."_".$classeEtab."_".$devoirid;
 $dossier="../devoirs/Etablissements/";
 $dossier1="../devoirs/Etablissements/".$codeEtab."/";
 $dossier2="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere);
 $dossier3="../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/";
 
 if(!is_dir($dossier)) {
              @mkdir($dossier);
              if(!is_dir($dossier1)) {
                  @mkdir($dossier1);

                  if(!is_dir($dossier2)) {
                      @mkdir($dossier2);

                  }

                  if(!is_dir($dossier3)) {
                      @mkdir($dossier3);

                  }

              }
          }else {
            if(!is_dir($dossier1)) {
                @mkdir($dossier1);

                if(!is_dir($dossier2)) {
                    @mkdir($dossier2);

                }

                if(!is_dir($dossier3)) {
                    @mkdir($dossier3);

                }

            }
          }
 
//gestion du fichier 

   $file_name = $_FILES['file']['name'];
   @$_SESSION['file'] = $file_name;
   $file_size =$_FILES['file']['size'];
   $file_tmp =$_FILES['file']['tmp_name'];
   $file_type=$_FILES['file']['type'];
   $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));
   $typesupport="DEVOIRS";
   $fichierad=$transactionId.$i.".".$file_ext;
   @rename($file_tmp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

  $etabs->Addsupports($devoirid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);

 



}else if ($_SERVER['REQUEST_METHOD'] === 'GET'){


if(isset($_GET['id'])&& isset($_GET['sessionEtab'])&& isset($_GET['codeEtab'])&& isset($_GET['classeid']))
{
$teacherid=$_GET['id'];
$sessionEtab=$_GET['sessionEtab'];
$codeEtab=$_GET['codeEtab'];
$classeid=$_GET['classeid'];

$data = $etabs->getAllTeatcherdevoirs($teacherid,$codeEtab,$sessionEtab,$classeid);

echo $data;

}else
{

if(isset($_GET['id']))

{

$teacherid=$_GET['id'];
  
  //echo $courseid." / ".$classeid." / ".$codeEtab." / ".$sessionEtab." / ".$teacherid;
  
$data = $etabs->getAllControleMatiereOfThisTeatcherId($teacherid);

  //$data = $etabs->getAllCoursesTea($teacherid,$codeEtab,$sessionEtab);
  echo $data;

}
}


  

}



?>

