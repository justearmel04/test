<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}





require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab_quiz);
$sessionEtab=utf8_decode($request->sessionEtab_quiz);
$matiereid=utf8_decode($request->matiere_quiz);
$teatcherid=utf8_decode($request->teatcher_quiz);
$classeid=utf8_decode($request->classe_quiz);
$quizid=utf8_decode($request->id_quiz);
$studentid=utf8_decode($request->studentid);
$trueorfalse=$request->trueorfalse;
$multiplechoice=$request->multiplechoice;

//nous allons gerer les trueorfalse
if(count($trueorfalse)>0)
{
 foreach ($trueorfalse as $value):

 $questionid=$value->questionid;
 $propositions=$value->propositions;

 if(count($propositions)>0)
{ 
   foreach ($propositions as $values):
 
 $propositionid=$values->propositionid;
 $answer=$values->answer;

 $code2="INSERT INTO reponsequiz SET idquiz_repquiz=?,studentid_repquiz=?,questionid_repquiz=?,propositionid_repquiz=?,reponse_repquiz=?,matiereid_repquiz=?,classeid_repquiz=?,codeEtab_repquiz=?,sessionEtab_repquiz=?";
 $req2=$db->dataBase->prepare($code2);
 $req2->execute([$quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab]);


   endforeach;
}



 endforeach;
}


if(count($multiplechoice)>0)
{

foreach ($multiplechoice as $value):

 $questionid=$value->questionid;
 $propositions=$value->propositions;

 if(count($propositions)>0)
{ 
foreach ($propositions as $values):
$propositionid=$values->propositionid;
 $answer=$values->answer;


  $codex="INSERT INTO reponsequiz SET idquiz_repquiz=?,studentid_repquiz=?,questionid_repquiz=?,propositionid_repquiz=?,reponse_repquiz=?,matiereid_repquiz=?,classeid_repquiz=?,codeEtab_repquiz=?,sessionEtab_repquiz=?";
  $reqx=$db->dataBase->prepare($codex);
  $reqx->execute([$quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab]);

endforeach;

}

endforeach;

}

//nous allons faire le resultat de l'eleve au quiz


 $codey="SELECT * FROM quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=compte.id_compte and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=?";
 $reqy=$db->dataBase->prepare($codey);
 $reqy->execute([$quizid,$classeid,$codeEtab,$sessionEtab]);
 $coursesdetails=$reqy->fetchAll();

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
  $teatcheridcourses=$datacourses->teatcher_quiz;
  $filescourses="";

endforeach;


 $codeyes="SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=?";
 $reqyes=$db->dataBase->prepare($codeyes);
 $reqyes->execute([$quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses]);
 $questions=$reqyes->fetchAll();

 $codeyup="SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=1";
 $reqyup=$db->dataBase->prepare($codeyup);
 $reqyup->execute([$quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses]);
 $questiontrueorfalse=$reqyup->fetchAll();

 $codeyang="SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=2";
 $reqyang=$db->dataBase->prepare($codeyang);
 $reqyang->execute([$quizid,$classeid,$codeEtab,$sessionEtab,$teatcheridcourses]);
 $questionmultiplechoice=$reqyang->fetchAll();

 $concattrueorfalse="";
 $i=1;

   foreach ($questiontrueorfalse as  $value):
    $concattrueorfalse=$concattrueorfalse.$value->id_quest."@";

    $solutions="";
    $solutionids="";
    //$solutionspropositions=$etabs->getsolutionsPropoReponses($quizid,$value->id_quest);
    $codeprop="SELECT * FROM propositionrep where idquest_proprep=? and valeur_proprep=1";
    $reqprop=$db->dataBase->prepare($codeprop);
    $reqprop->execute([$value->id_quest]);
    $solutionspropositions=$reqprop->fetchAll();


    foreach ($solutionspropositions as $valuesolution):
      $solutions=$solutions." ".$valuesolution->libelle_proprep;
      $solutionids=$solutionids.$valuesolution->id_proprep."@";
    endforeach;

    //$propositionsAndreponses=$etabs->getPropositionsAndreponsesChild($studentid,$value->id_quest,$quizid,$codeEtab,$sessionEtab);
    $codepropsol="SELECT * FROM reponsequiz where  reponsequiz.studentid_repquiz=? and reponsequiz.questionid_repquiz=? and reponsequiz.idquiz_repquiz=? and reponsequiz.codeEtab_repquiz=? and reponsequiz.sessionEtab_repquiz=? ";
    $reqpropsol=$db->dataBase->prepare($codepropsol);
    $reqpropsol->execute([$studentid,$value->id_quest,$quizid,$codeEtab,$sessionEtab]);
    $propositionsAndreponses=$reqpropsol->fetchAll();


    // var_dump($propositionsAndreponses);
    $solutionsidStudent="";
    foreach ($propositionsAndreponses as $valueRep):
      if($valueRep->reponse_repquiz==1)
      {
        $solutionsidStudent=$solutionsidStudent.$valueRep->propositionid_repquiz."@";
      }
    endforeach;

    $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));
    $tabsolutionids=explode("@",substr($solutionids,0,-1));
    $nbcont=count($tabsolutionsStudent);

    for($i=0;$i<$nbcont;$i++)
    {
      if($tabsolutionids[$i]==$tabsolutionsStudent[$i])
      {
        // echo "point obtenu";
        //Nous allons verifier si nous avons une note pour cette question
        //$nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
    $codecount="SELECT * FROM notesquestions where questid_notesquest=? and studentid_notesquest=? and quizid_notesquest=? and codEtab_notesquest=? and sessionEtab_notesquest=? ";
    $reqcount=$db->dataBase->prepare($codecount);
    $reqcount->execute([$value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab]);
    $datacount=$reqcount->fetchAll();
    $nbnotes=count($datacount);


        if($nbnotes==0)
        {
          //$etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab);
$codeAdd="INSERT INTO notesquestions SET questid_notesquest=?,studentid_notesquest=?,quizid_notesquest=?,valeur_notesquest=?,codEtab_notesquest=?,sessionEtab_notesquest=?";
$reqAdd=$db->dataBase->prepare($codeAdd);
$reqAdd->execute([$value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab]);
        }


      }else {

         //$nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);
         $codecount="SELECT * FROM notesquestions where questid_notesquest=? and studentid_notesquest=? and quizid_notesquest=? and codEtab_notesquest=? and sessionEtab_notesquest=? ";
    $reqcount=$db->dataBase->prepare($codecount);
    $reqcount->execute([$value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab]);
    $datacount=$reqcount->fetchAll();
    $nbnotes=count($datacount);

           $point=0;
         if($nbnotes==0)
         {

           //$etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab);
$codeAdd="INSERT INTO notesquestions SET questid_notesquest=?,studentid_notesquest=?,quizid_notesquest=?,valeur_notesquest=?,codEtab_notesquest=?,sessionEtab_notesquest=?";
$reqAdd=$db->dataBase->prepare($codeAdd);
$reqAdd->execute([$value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab]);


         }


      }
    }

    $i++;
  endforeach;



$i=1;
  $concatqestmulti="";
    foreach ($questionmultiplechoice as  $value):
      $solutions="";
      $solutionids="";
      //$solutionspropositions=$etabs->getsolutionsPropoReponses($quizid,$value->id_quest);
    $codepropsol="SELECT * FROM propositionrep where idquest_proprep=? and valeur_proprep=1 ";
    $reqpropsol=$db->dataBase->prepare($codepropsol);
    $reqpropsol->execute([$value->id_quest]);
    $solutionspropositions=$reqpropsol->fetchAll();


      foreach ($solutionspropositions as $valuesolution):
        $solutions=$solutions." ".$valuesolution->libelle_proprep;
        $solutionids=$solutionids.$valuesolution->id_proprep."@";
      endforeach;

      $concatqestmulti=$concatqestmulti.$value->id_quest."@";

      //$propositionsreponses=$etabs->getPropositionsOfQuestions($value->id_quest);
$codereponses="SELECT * FROM question,propositionrep where question.id_quest=propositionrep.idquest_proprep and question.id_quest=?";
    $reqreponses=$db->dataBase->prepare($codereponses);
    $reqreponses->execute([$value->id_quest]);
    $propositionsreponses=$reqpropsol->fetchAll();

      $j=1;
      $solutionsidStudent="";
      foreach ($propositionsreponses as $propositions):
        //retrouvons la reponse de l'etudiant face a cette proposition
        //$valeurpropositionchoice=$etabs->getValeurOfpropositionchoice($value->id_quest,$quizid,$propositions->id_proprep,$codeEtab,$sessionEtab,$studentid);
        
$codec="SELECT * FROM reponsequiz where questionid_repquiz=? and idquiz_repquiz=? and propositionid_repquiz=?  and codeEtab_repquiz=? and sessionEtab_repquiz=? and studentid_repquiz=? ";
 $reqc=$db->dataBase->prepare($codec);
 $reqc->execute([$value->id_quest,$quizid,$propositions->id_proprep,$codeEtab,$sessionEtab,$studentid]);
 $datac=$req1->fetchAll();
 $arrayc=json_encode($datac,true);
 $someArrayc = json_decode($arrayc, true);
 $valeurpropositionchoice=$someArrayc[0]["reponse_repquiz"];


if($valeurpropositionchoice==1)
        {
          $solutionsidStudent=$solutionsidStudent.$propositions->id_proprep."@";
        }

      endforeach;

      $tabsolutionids=explode("@",substr($solutionids,0,-1));
      $tabsolutionsStudent=explode("@",substr($solutionsidStudent,0,-1));

      $cmp = array_diff($tabsolutionids, $tabsolutionsStudent);
      $nbcont=count($cmp);

      if($nbcont==0)
      {

        //$nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);

$codecount="SELECT * FROM notesquestions where questid_notesquest=? and studentid_notesquest=? and quizid_notesquest=? and codEtab_notesquest=? and sessionEtab_notesquest=? ";
    $reqcount=$db->dataBase->prepare($codecount);
    $reqcount->execute([$value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab]);
    $datacount=$reqcount->fetchAll();
    $nbnotes=count($datacount);


        if($nbnotes==0)
        {
          //$etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab);
$codeAdd="INSERT INTO notesquestions SET questid_notesquest=?,studentid_notesquest=?,quizid_notesquest=?,valeur_notesquest=?,codEtab_notesquest=?,sessionEtab_notesquest=?";
$reqAdd=$db->dataBase->prepare($codeAdd);
$reqAdd->execute([$value->id_quest,$studentid,$quizid,$value->point_quest,$codeEtab,$sessionEtab]);


        }
      }else {
        // echo "point non obtenu";
        //$nbnotes=$etabs->getNumberOfstudentquizquestion($value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab);

$codecount="SELECT * FROM notesquestions where questid_notesquest=? and studentid_notesquest=? and quizid_notesquest=? and codEtab_notesquest=? and sessionEtab_notesquest=? ";
    $reqcount=$db->dataBase->prepare($codecount);
    $reqcount->execute([$value->id_quest,$studentid,$quizid,$codeEtab,$sessionEtab]);
    $datacount=$reqcount->fetchAll();
    $nbnotes=count($datacount);


        $point=0;
        if($nbnotes==0)
        {
          //$etabs->AddnotesQuizquestion($value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab);
$codeAdd="INSERT INTO notesquestions SET questid_notesquest=?,studentid_notesquest=?,quizid_notesquest=?,valeur_notesquest=?,codEtab_notesquest=?,sessionEtab_notesquest=?";
$reqAdd=$db->dataBase->prepare($codeAdd);
$reqAdd->execute([$value->id_quest,$studentid,$quizid,$point,$codeEtab,$sessionEtab]);



        }
     }

     $i++;
    endforeach;

//nous allons rechercher la note obtenue par l'eleve a ce quiz


$codek="SELECT SUM(notesquestions.valeur_notesquest) as notequiz from notesquestions where notesquestions.quizid_notesquest=? and notesquestions.studentid_notesquest=? and notesquestions.codEtab_notesquest=? and notesquestions.sessionEtab_notesquest=?";
$reqk=$db->dataBase->prepare($codek);
$reqk->execute([$quizid,$studentid,$codeEtab,$sessionEtab]);
$data1=$reqk->fetchAll();
 $array1=json_encode($data1,true);
 $someArray1 = json_decode($array1, true);
 $noteobtain=$someArray1[0]["notequiz"];


$cook="SELECT SUM(question.point_quest) as point from question where question.idquiz_quest=?";
$reqck=$db->dataBase->prepare($cook);
$reqck->execute([$quizid]);
$data2=$reqck->fetchAll();
 $array2=json_encode($data2,true);
 $someArray2 = json_decode($array2, true);
 $notetotal=$someArray2[0]["point"];


$noteeleves=$noteobtain."/".$notetotal;


echo json_encode($noteeleves);


}


?>

