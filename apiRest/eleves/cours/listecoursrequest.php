<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


require_once('../../class1/User.php');

require_once('../../class1/cnx.php');

require_once('../../class1/Parent.php');

require_once('../../class1/Etablissement.php');



//$users = new User();

$parents=new ParentX();

$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$parentid=$_POST['id'];

//echo $parentid;

  $data = $parents->getallstudentsByParentId($parentid);
  echo $data;

}else if ($_SERVER['REQUEST_METHOD'] === 'GET'){


if(isset($_GET['id'])&& isset($_GET['sessionEtab'])&& isset($_GET['codeEtab']))
{

  $teacherid=$_GET['id'];
  $sessionEtab=$_GET['sessionEtab'];
  $codeEtab=$_GET['codeEtab'];
  
  $data = $etabs->getAllCoursesTea($teacherid,$codeEtab,$sessionEtab);
  echo $data;

} else if(isset($_GET['sessionEtab'])&& isset($_GET['codeEtab'])&& isset($_GET['classeid']))
{
  //$teacherid=$_GET['id'];
  $sessionEtab=$_GET['sessionEtab'];
  $codeEtab=$_GET['codeEtab'];
  $classeid=$_GET['classeid'];
  
  $data = $etabs->getAllCoursesClasseStudent($codeEtab,$sessionEtab,$classeid);
  echo $data;

}

  

}



?>

