<?php

class ParentX{



public $db;

function __construct() {

  require_once('../class/cnx.php');



  $db = new mysqlConnector();

  $this->db= $db->dataBase;

}

function getallprofsofstudents($studentid)
{
  // $this->db->query('SET SQL_BIG_SELECTS=1');
  $encours=1;
  //$req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser,matiere,inscription WHERE compte.id_compte=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=dispenser.id_enseignant AND dispenser.id_cours=matiere.id_mat AND inscription.codeEtab_inscrip=dispenser.codeEtab AND dispenser.idclasse_disp=(SELECT DISTINCT classe.id_classe FROM classe,inscription,etablissement,sessions WHERE classe.id_classe=inscription.idclasse_inscrip AND etablissement.code_etab=inscription.codeEtab_inscrip AND sessions.codeEtab_sess=etablissement.code_etab AND sessions.encours_sess=? AND inscription.ideleve_inscrip=?)");
   $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser,matiere WHERE compte.id_compte=enseignant.idcompte_enseignant AND dispenser.id_enseignant=compte.id_compte AND dispenser.id_cours=matiere.id_mat AND dispenser.idclasse_disp=(SELECT DISTINCT classe.id_classe FROM classe,inscription,etablissement,sessions WHERE classe.id_classe=inscription.idclasse_inscrip AND etablissement.code_etab=inscription.codeEtab_inscrip AND sessions.codeEtab_sess=etablissement.code_etab AND sessions.encours_sess=? AND inscription.ideleve_inscrip=?)");  
$req->execute([$encours,$studentid]);
  $data= $req->fetchAll();

  //return json_encode($data, JSON_FORCE_OBJECT);
  return json_encode($data);

}


function getAlletabOfStudentParentNew($IdCompte)

{

  $session="2019-2020";

  $encours=1;

  $req = $this->db->prepare("SELECT  distinct 	code_etab,libelle_etab FROM etablissement,classe,eleve,inscription,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and parent.idcompte_parent=? and inscription.session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) ");

  $req->execute([$IdCompte,$encours]);

  $data= $req->fetchAll();

  return json_encode($data, JSON_FORCE_OBJECT);
      
}

function getallstudentsByParentId($IdCompte)
{
  $encours=1;
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from compte,eleve,parent,parenter,inscription,classe,etablissement,pays where compte.id_compte=eleve.idcompte_eleve and  parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve IN (SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve IN (SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip IN(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?))");
  $req->execute([$IdCompte,$encours,$IdCompte]);
   $data= $req->fetchAll();

  //return json_encode($data, JSON_FORCE_OBJECT);
  return json_encode($data);
}



function getDifferentStudentByParentId($IdCompte)

{

    $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parent.idcompte_parent=? ");

    $req->execute([$IdCompte]);

   $data= $req->fetchAll();

  return json_encode($data, JSON_FORCE_OBJECT);
}

function getStudentCurrentlyinscription($IdCompte,$ideleve)
{
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from eleve,parent,parenter,inscription,classe,etablissement,pays where   parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve=? order by id_inscrip DESC limit 1  ");
  $req->execute([$IdCompte,$ideleve]);
   return $req->fetchAll();
}

function determineparentsons($IdCompte)
{
$req = $this->db->prepare("select DISTINCT compte.id_compte from parenter,compte where parenter.eleveid_parenter=compte.id_compte and parenter.parentid_parenter=?");
$req->execute([$IdCompte]);
$data= $req->fetchAll();

 $outp = "";
 foreach ($data as  $value):
 

  $studentid=$value->id_compte;

  $req1 = $this->db->prepare("select DISTINCT compte.photo_compte,compte.id_compte,compte.nom_compte,compte.prenom_compte,compte.datenais_compte,compte.lieunais_compte,eleve.matricule_eleve,classe.libelle_classe,classe.id_classe,etablissement.code_etab,etablissement.libelle_etab,classe.session_classe FROM compte,eleve,classe,inscription,etablissement where compte.id_compte=eleve.idcompte_eleve and inscription.ideleve_inscrip=compte.id_compte and classe.id_classe=inscription.idclasse_inscrip and etablissement.code_etab=inscription.codeEtab_inscrip and inscription.ideleve_inscrip=? and inscription.id_inscrip=(select inscription.id_inscrip from inscription where inscription.ideleve_inscrip=? order by inscription.id_inscrip desc limit 1)");
  $req1->execute([$studentid,$studentid]);
  $datas= $req1->fetchAll();

  foreach ($datas as  $values):
  if ($outp != "") {$outp .= ",";}
  $matricule=$values->matricule_eleve;
  $studentid=$values->id_compte;
  $nom=$values->nom_compte;
  $prenoms=$values->prenom_compte;
  $datenais=$values->datenais_compte;
  $lieunais=$values->lieunais_compte;
  $classeid=$values->id_classe;
  $libelleclasse=$values->libelle_classe;
  $libelleetab=$values->libelle_etab;
  $codeEtab=$values->code_etab;
  $sessionEtab=$values->session_classe;
  $photo=$values->photo_compte;

  $outp .= '{"matricule":"'.$matricule.'",';
  $outp .= '"studentid":"'.$studentid.'",';
  $outp .= '"nom":"'.$nom.'",';
  $outp .= '"prenoms":"'.$prenoms.'",';
  $outp .= '"datenais":"'.$datenais.'",';
  $outp .= '"lieunais":"'.$lieunais.'",';
  $outp .= '"classeid":"'.$classeid.'",';
  $outp .= '"libelleclasse":"'.$libelleclasse.'",';
  $outp .= '"libelleetab":"'.$libelleetab.'",';
  $outp .= '"codeEtab":"'.$codeEtab.'",';
  $outp .= '"sessionEtab":"'.$sessionEtab.'",';

  $outp .= '"photo":"'.$photo. '"}';


  endforeach;
  


 endforeach;

echo "[".$outp."]" ;

}



function getsouscriptionStudentParent($IdCompte)

{

	$parents= new ParentX();

   /* $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parent.idcompte_parent=? ");

    $req->execute([$IdCompte]);

   $data= $req->fetchAll();*/
   $encours=1;
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from compte,eleve,parent,parenter,inscription,classe,etablissement,pays where compte.id_compte=eleve.idcompte_eleve and  parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve in(SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?))");
  $req->execute([$IdCompte,$encours,$IdCompte]);
   $data= $req->fetchAll();


  //return json_encode($data, JSON_FORCE_OBJECT);

  $outp = "";

    foreach ($data as  $value):
    if ($outp != "") {$outp .= ",";}
   $dataSchool=$parents->getStudentCurrentlyinscription($IdCompte,$value->idcompte_eleve);
   $dateday=date("Y-m-d");
   $datasouscriptions=$parents->getsouscriptionInfosActivenb($IdCompte,$value->idcompte_eleve,$dateday);
   $nbsouscriptionactive=count($datasouscriptions);
                                            // if($nbsouscriptionactive)
   $array=json_encode($dataSchool,true);
   $someArray = json_decode($array, true);
   $donnees=$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["pays_etab"]."*".$someArray[0]["devises_pays"];
   $escampe=explode("*",$donnees);
   $codeEtab=$escampe[0];
   $idclasse=$escampe[1];
   $libelleEtab=$escampe[2];
   $libelleclasse=$escampe[3];
   $idpays=$escampe[4];
   $devisespays=$escampe[5];
   $date_paiab="";
   $datevalide="";
   $statut=0;

   if($nbsouscriptionactive>0)
   {
     //nous allons verifier le statut du paiement

       $array1=json_encode($datasouscriptions,true);
       $someArray1 = json_decode($array1, true);

       if($someArray1[0]["statut_paiab"]==1)
       {
         $dataslastsous=$parents->getsouscriptionInfosActive($IdCompte,$value->idcompte_eleve,$dateday);
         $array=json_encode($dataslastsous,true);
         $someArray = json_decode($array, true);
         $date_paiab=date_format(date_create($someArray[0]["datedeb_histoabn"]),"d/m/Y");
         $datevalide=date_format(date_create($someArray[0]["datefin_histoabn"]),"d/m/Y");
         $statut=1;
       }else if($someArray1[0]["statut_paiab"]==0){
         $date_paiab=date_format(date_create($someArray1[0]["date_paiab"]),"d/m/Y");
         $datevalide="";
         $statut=0;
       }

   }

   $outp .= '{"matricule_eleve":"'.$value->matricule_eleve.'",';
   $outp .= '"photo_eleve":"'.$value->photo_compte.'",';
   $outp .= '"nom_eleve":"'.$value->nom_eleve.'",';
   $outp .= '"prenom_eleve":"'.$value->prenom_eleve.'",';
   $outp .= '"classeid_eleve":"'.$value->id_classe.'",';
   $outp .= '"classe_eleve":"'.$value->libelle_classe.'",';
   $outp .= '"codeEtab_eleve":"'.$value->code_etab.'",';
   $outp .= '"libelleEtab_eleve":"'.$value->libelle_etab.'",';
   $outp .= '"sessionEtab_eleve":"'.$value->session_inscrip.'",';
   $outp .= '"typeEtab_eleve":"'.$value->type_etab.'",';
   $outp .= '"paysidEtab_eleve":"'.$value->id_pays.'",';
   $outp .= '"devisepaysEtab_eleve":"'.$value->devises_pays.'",';
   $outp .= '"datedeb_sous":"'.$date_paiab.'",';
   $outp .= '"datefin_sous":"'.$datevalide.'",';
   $outp .= '"parent_id":"'.$IdCompte.'",';
   $outp .= '"statut_sous":"'.$statut.'",';
   $outp .= '"eleve_id":"'.$value->idcompte_eleve. '"}';

    endforeach;

   echo "[".$outp."]" ;



}
function getsouscriptionInfosActivenb($parentid,$eleveid,$dateday)
{
  //$req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=4 and parentid_paiab=? and studentsid_paiab=? and datevalide_paiab>? order by id_paiab desc");
  $req = $this->db->prepare("SELECT * FROM paiementab where (statut_paiab=1 or statut_paiab=0) and parentid_paiab=? and studentsid_paiab=? order by id_paiab desc");
  //$req->execute([$parentid,$eleveid,$dateday]);
  $req->execute([$parentid,$eleveid]);
  return $req->fetchAll();
}

function getsouscriptionInfosActive($parentid,$eleveid,$dateday)
{
  //$req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=4 and parentid_paiab=? and studentsid_paiab=? and datevalide_paiab>? order by id_paiab desc limit 1");
   $req = $this->db->prepare("SELECT * FROM paiementab,abonnementhisto WHERE paiementab.id_paiab=abonnementhisto.paieid_histoabn and statut_paiab=1 and parentid_paiab=? and studentsid_paiab=?  order by id_paiab desc limit 1");
  //$req->execute([$parentid,$eleveid,$dateday]);
  $req->execute([$parentid,$eleveid]);
  return $req->fetchAll();
}

function AddParentPaiement($datecrea,$parentidcompte,$studentidcompte,$montantchoice,$selectmobileop,$transacId,$paymentno,$statutpaie,$nbselect,$subscribeIdarray,$devisecontry)
{

$req = $this->db->prepare("INSERT INTO  paiementab SET date_paiab=?,parentid_paiab=?,studentsid_paiab=?,montant_paiab=?,operateur_paiab=?,transacid_paiab=?,number_paiab=?,statut_paiab=?,nbstudent_paiab=?,subscribes_paiab=?,devises_paiab=?");
  $req->execute([
  $datecrea,
  $parentidcompte,
  $studentidcompte,
  //$idparentcpte,
  //$studentIdentarray,
  //$montanttotale,
  $montantchoice,
  $selectmobileop,
  $transacId,
  $paymentno,
  $statutpaie,
  $nbselect,
  $subscribeIdarray,
  $devisecontry
  ]);

 $idlastcompte=$this->db->lastInsertId();

return $idlastcompte;

}

function getcountryoperators($paysid)
{
  $req = $this->db->prepare("SELECT * FROM mobileoperator,pays WHERE mobileoperator.pays_mob=pays.id_pays and pays_mob=? ");
  $req->execute([$paysid]);
  $data=$req->fetchAll();
  return json_encode($data);
}

function geNumberOfStudentByParentId($IdCompte)

{
$this->db->query('SET SQL_BIG_SELECTS=1');
  $encours=1;

    $req = $this->db->prepare("SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip IN (SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

    $req->execute([$encours,$IdCompte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

}



function getnumberofstudentparent($IdCompte)

{

  $encours=1;



  $req = $this->db->prepare("SELECT DISTINCT parenter.parentid_parenter from parenter where parenter.eleveid_parenter in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

  $req->execute([$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}



function getnumberofstudentparentpresences($IdCompte)

{

  $encours=1;

  $day=date("Y-m-d");

  $req = $this->db->prepare("SELECT DISTINCT presences.matricule_presence from presences where presences.statut_presence=1 and presences.date_presence=? and presences.matricule_presence in(SELECT DISTINCT eleve.matricule_eleve FROM parenter,inscription,eleve where parenter.eleveid_parenter=inscription.ideleve_inscrip and parenter.eleveid_parenter=eleve.idcompte_eleve and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

  $req->execute([$day,$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}



function getnumberofprofByparentId($IdCompte)

{

  $encours=1;

  $req = $this->db->prepare("SELECT DISTINCT enseignant.idcompte_enseignant from enseignant,dispenser WHERE enseignant.idcompte_enseignant=dispenser.id_enseignant and dispenser.idclasse_disp=(SELECT DISTINCT inscription.idclasse_inscrip FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?)");

  $req->execute([$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}



}


 ?>

