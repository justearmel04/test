<?php
session_start();
// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');
//require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Récuperation des données de la premiére étape : Infos Client
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$syllabusid = $request->syllabusid;
$libelleregle= $request->libelletheme;

$code="INSERT INTO   syllabtheme SET 	idsyllab_syllabth='$syllabusid',libelle_syllabth='$libelleregle'";
$req=$db->dataBase->prepare($code);
$req->execute([]);

echo json_encode('success');
}


?>

