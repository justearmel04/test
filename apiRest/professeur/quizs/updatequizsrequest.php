<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

//$postdata =$_POST['devoir'];
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$datelimitedevoir=utf8_decode($request->datelimite);
$libelledevoir=utf8_decode($request->libelle);
$verouillerdevoir=utf8_decode($request->verouillerquiz);
$instructionsdevoir=utf8_decode($request->instructionsquiz);
$durationquiz=utf8_decode($request->durationquiz);
$idquiz=utf8_decode($request->quizid);

$code="UPDATE quiz SET libelle_quiz=?,duree_quiz=?,instruction_quiz=?,datelimite_quiz=?,matiere_quiz=?,verouiller_quiz=?,classe_quiz=? where teatcher_quiz=? and codeEtab_quiz=? and sessionEtab_quiz=? and id_quiz=?";
$req=$db->dataBase->prepare($code);
$req->execute([$libelledevoir,$durationquiz,$instructionsdevoir,$datelimitedevoir,$matiereid,$verouillerdevoir,$classeid,$teatcherid,$codeEtab,$sessionEtab,$idquiz]);

echo json_encode('success');


}



?>
