<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}





require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$courseid=utf8_decode($request->devoirid);

 $code3="UPDATE devoirs SET statut_dev=1 where	id_dev=? and teatcher_dev=? and classe_dev=? and matiere_dev=? and codeEtab_dev=? and sessionEtab_dev=?";
 $req3=$db->dataBase->prepare($code3);
 $req3->execute([$courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab]);

echo json_encode('success');
//echo json_encode($teatcherid);

}


?>

