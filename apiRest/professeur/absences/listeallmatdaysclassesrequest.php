<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


require_once('../../class1/User.php');

require_once('../../class1/cnx.php');

require_once('../../class1/Parent.php');

require_once('../../class1/Etablissement.php');

require_once('../../class1/functions.php');



//$users = new User();

$parents=new ParentX();

$etabs=new Etab();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$parentid=$_POST['id'];

//echo $parentid;

  $data = $parents->getallstudentsByParentId($parentid);
  echo $data;

}else if ($_SERVER['REQUEST_METHOD'] === 'GET'){


  $teacherid=$_GET['id'];
  $classeid=$_GET['classeid'];
  $codeEtab=$_GET['codeEtab'];
  $sessionEtab=$_GET['sessionEtab'];
  $datechoice=$_GET['datechoice'];

 $tabdays=explode("/",$datechoice);

  $jourday=$tabdays[0];
  $moisday=$tabdays[1];
  $anneeday=$tabdays[2];

  $nouvelledate=$anneeday."-".$moisday."-".$jourday;

  //$jour=$etabs->nom_jour($nouvelledate);
  $jour=nom_jour($nouvelledate);

  //echo $jour;

  $libellejour=$etabs->obtenirLibelleJours($jour);

  //echo $libellejour;

  //$data = $etabs->getAllMatiereOfDaysTeaNew($teacherid,$classeid,$codeEtab,$sessionEtab,$libellejour);

//echo $classeid."/".$codeEtab."/".$sessionEtab."/".$libellejour."/".$teacherid;

  $data = $etabs->getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classeid,$codeEtab,$sessionEtab,$libellejour,$teacherid);


  //$data = $etabs->getAllMatiereOfDaysTea($teacherid,$classeid,$codeEtab,$datechoice);
  echo json_encode($data);

}



?>

