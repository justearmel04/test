<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}



require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();


if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

//$postdata = file_get_contents("php://input");
$postdata =$_POST['support'];
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$matiereid=utf8_decode($request->matiereid);
$libellematcourses=utf8_decode($request->matierelib);
$teatcherid=utf8_decode($request->teatcherid);
$classeid=utf8_decode($request->classeid);
$classecourses=utf8_decode($request->classelib);
$datercourses=utf8_decode($request->datecourse);
$courseid=utf8_decode($request->courseid);

$typecourses="COURSES";

  $target_path = "../../../temp/";
$target_path = $target_path . basename($_FILES['file']['name']);
if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

$file_name = @$_FILES['file']['name'];

  $_SESSION["file"] = $file_name;

  $file_size =@$_FILES['file']['size'];

  $file_tmp =@$_FILES['file']['tmp_name'];

  $file_type=@$_FILES['file']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['file']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

$transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematcourses, 0, 3)).$courseid;

   $dossier="../../../courses/".$classecourses."/";

   $dossier1="../../../courses/".$classecourses."/".$datercourses."/";

   $dossier2="../../../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procède à ssa création
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }
  //@rename($target_path , "../../../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$transactionId);


$code="SELECT * FROM supports where courseid_support=? and codeEtab_support=? and sessionEtab_support=? and classeid_support=? and type_support=? order by id_support DESC limit 1";
$req=$db->dataBase->prepare($code);
$req->execute([$courseid,$codeEtab,$sessionEtab,$classeid,$typecourses]);
$data=$req->fetchAll();
foreach ($data as $value):
            $files=$value->fichier_support;
            $tabfiles=explode(".",$files);
            $namesfiles=$tabfiles[0];
            $search=strtoupper(substr($libellematcourses, 0, 3));
            $tabnamesfiles=explode($search,$namesfiles);
            $indicefiles=$tabnamesfiles[1];
          endforeach;
$i=1;
$somme=$indicefiles+$i;

 $fichierad=$transactionId.$somme.".".$file_ext;

 @rename($target_path , "../../../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$fichierad);
             

      //@unlink($target_path);
$typesupport="COURSES";

  $codeinsert="INSERT INTO supports set courseid_support=?,matiereid_support=?,teatcherid_support=?,codeEtab_support=?,sessionEtab_support=?,classeid_support=?,fichier_support=?,type_support=?";
  $req4=$db->dataBase->prepare($codeinsert);
  $req4->execute([$courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeid,$fichierad,$typesupport]);

echo json_encode('success');

// echo json_encode($dataretour);



}
else{

echo json_encode('unsuccess upload');
}



}



?>
