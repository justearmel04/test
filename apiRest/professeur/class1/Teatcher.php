<?php

class Teatcher{



public $db;

function __construct() {



require_once('../class/cnx.php');



  $db = new mysqlConnector();

  $this->db= $db->dataBase;

}



function getTeatcherInfobyId($idcompte)

{

  $typecompte="Teatcher";

  $req = $this->db->prepare("SELECT  * FROM compte,etablissement,enseigner,enseignant where enseignant.email_enseignant=compte.email_compte and compte.id_compte=enseigner.id_enseignant and enseigner.codeEtab=etablissement.code_etab and compte.id_compte=? and compte.type_compte=? ");

  $req->execute([$idcompte,$typecompte]);

  $data= $req->fetchAll();

  return json_encode($data);

}



function getNumberOfTeatcherClasseSchool($idclasse,$compteEtab)

{

$req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=?");

$req->execute([$compteEtab,$idclasse]);

$data=$req->fetchAll();

$nb=count($data);

return $nb;

}



function getAllTeatchersOfThisClassesEtab($code,$classeEtab)

{

  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=compte.id_compte and dispenser.codeEtab=? and dispenser.idclasse_disp=?");

  $req->execute([$code,$classeEtab]);

  return $req->fetchAll();



}



}

 ?>

