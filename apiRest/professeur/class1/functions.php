<?php
function DetermineHoursformat($hours)
{
  //nous allons dissocier les heures

  $tabhours=explode(":",$hours);
  $heures=$tabhours[0];
  $minutes=$tabhours[1];
  $concat=$heures."h".$minutes;

return $concat;

}

function obtenirLibelleMois($mois) {
switch($mois) {
    case '01': $mois = 'JANVIER'; break;
    case '02': $mois = 'FEVRIER'; break;
    case '03': $mois = 'MARS'; break;
    case '04': $mois = 'AVRIL'; break;
    case '05': $mois = 'MAI'; break;
    case '06': $mois = 'JUIN'; break;
    case '07': $mois = 'JUILLET'; break;
    case '08': $mois = 'AOUT'; break;
    case '09': $mois = 'SEPTEMBRE'; break;
    case '10': $mois = 'OCTOBRE'; break;
    case '11': $mois = 'NOVEMBRE'; break;
    case '12': $mois = 'DECEMBRE'; break;
    default: $mois =''; break;
  }
  return $mois;
}

function nom_jour($dateday) {

$jour_semaine = array(1=>"Monday", 2=>"Tuesday", 3=>"Wednesday", 4=>"Thursday", 5=>"Friday", 6=>"Saturday", 7=>"Sunday");

list($annee, $mois, $jour) = explode ("-", $dateday);

$timestamp = mktime(0,0,0, date($mois), date($jour), date($annee));
$njour = date("N",$timestamp);

return $jour_semaine[$njour];

}

function nomshort_jour($dateday) {

$jour_semaine = array(1=>"Mon", 2=>"Tue", 3=>"Wed", 4=>"Thu", 5=>"Fri", 6=>"Sat", 7=>"Sun");

list($annee, $mois, $jour) = explode ("-", $dateday);

$timestamp = mktime(0,0,0, date($mois), date($jour), date($annee));
$njour = date("N",$timestamp);

return $jour_semaine[$njour];

}

function retranscrireMois($mois) {
switch($mois) {
    case '01': $mois = '1'; break;
    case '02': $mois = '2'; break;
    case '03': $mois = '3'; break;
    case '04': $mois = '4'; break;
    case '05': $mois = '5'; break;
    case '06': $mois = '6'; break;
    case '07': $mois = '7'; break;
    case '08': $mois = '8'; break;
    case '09': $mois = '9'; break;
    case '10': $mois = '10'; break;
    case '11': $mois = '11'; break;
    case '12': $mois = '12'; break;
    default: $mois =''; break;
  }
  return $mois;
}


 ?>