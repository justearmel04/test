<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

foreach ($request as  $value):

$matricule=$value->matricule;
$id_eleve=$value->id_eleve;
$matiereid=$value->matiereid;
$libellematiere=$value->matierelib;
$teatcherid=$value->teatcherid;
$classeid=$value->classeid;
$libelleclasse=$value->classelib;
$codeEtab=$value->codeEtab;
$sessionEtab=$value->sessionEtab;
$idtypenote=$value->controleid;
$notes=$value->note;
$observation=$value->observation;
$coefficientNotes=$value->coef;
$typesessionNotes=$value->trimestre;
$nbnotessemesterclasses=$value->nbcontrole;

$typenote=1;
$datepiste=date("Y-m-d");
$controlepiste=1;
$reglementpiste=0;
$examenpiste=0;
$actionpiste=1;
$tabNameofstudents="";

//nous devons enregistrer la piste pour l'ajout de cette note qui se fera apr�s verification que nous n'avons pas encore ajouter une piste a ce controle


//nous allons ajouter les notes apr�s verification pour savoir s'il s'agit de la premi�re note ou pas


if($nbnotessemesterclasses==0)
  {

	$notescoef=$notes*$coefficientNotes;
        $moyennestudent=$notescoef/$coefficientNotes;

  //nous allons ajouter la note dans rating

$code="INSERT INTO rating SET session_rating='$sessionEtab',typsession_rating='$typesessionNotes',ideleve_rating='$id_eleve',classe_rating='$classeid',idprof_rating='$teatcherid',matiereid_rating='$matiereid',totalnotes_rating='$notes',totalnotescoef_rating='$notescoef',totalcoef_rating='$coefficientNotes',rating='$moyennestudent',codeEtab_rating='$codeEtab'";
$req=$db->dataBase->prepare($code);
$req->execute([]);

$code1="INSERT INTO  notes SET type_notes='$typenote',idtype_notes='$idtypenote',idclasse_notes='$classeid',idmat_notes='$matiereid',idprof_notes='$teatcherid',ideleve_notes='$id_eleve',codeEtab_notes='$codeEtab',valeur_notes='$notes',obser_notes='$observation',session_notes='$sessionEtab'";
$req1=$db->dataBase->prepare($code1);
$req1->execute([]);


        

  }else
  {

  $code="SELECT * FROM rating where ideleve_rating='$id_eleve' and matiereid_rating='$matiereid' and classe_rating='$classeid' and typsession_rating='$typesessionNotes' and session_rating='$sessionEtab' ";
$req=$db->dataBase->prepare($code);
$req->execute([]);
$data=$req->fetchAll();
foreach($data as $values):
  $alltotalnotes=$values->totalnotes_rating;
  $alltotalnotescoef=$values->totalnotescoef_rating;
  $totalcoefnotes=$values->totalcoef_rating;
  $ratingId=$values->id_rating;
endforeach;
  $sommesNotes=$alltotalnotes+$notes;
  $sommesNotescoef=$alltotalnotescoef+($notes*$coefficientNotes);
  $sommescoef=$totalcoefnotes+$coefficientNotes;
  $moyenne=$sommesNotescoef/$sommescoef;
  $notescoef=$notes*$coefficientNotes;

  $code1="UPDATE rating set totalnotes_rating='$sommesNotes',totalnotescoef_rating='$sommesNotescoef',totalcoef_rating='$sommescoef',rating='$moyenne' where id_rating='$ratingId' and session_rating='$sessionEtab' and typsession_rating='$typesessionNotes' and classe_rating='$classeid' and matiereid_rating='$matiereid' and ideleve_rating='$id_eleve' and codeEtab_rating='$codeEtab'";
  $req1=$db->dataBase->prepare($code1);
  $req1->execute([]);

 // ajout de la note

$code2="INSERT INTO  notes SET type_notes='$typenote',idtype_notes='$idtypenote',idclasse_notes='$classeid',idmat_notes='$matiereid',idprof_notes='$teatcherid',ideleve_notes='$id_eleve',codeEtab_notes='$codeEtab',valeur_notes='$notes',obser_notes='$observation',session_notes='$sessionEtab'";
$req2=$db->dataBase->prepare($code2);
$req2->execute([]);

$code3="SELECT * FROM rating where classe_rating='$classeid' and matiereid_rating='$matiereid' and idprof_rating='$teatcherid' and codeEtab_rating='$codeEtab' and session_rating='$sessionEtab' and typsession_rating='$typesessionNotes' order by rating DESC";
$req3=$db->dataBase->prepare($code3);
$req3->execute([]);
$data=$req3->fetchAll();
  $i=1;
foreach ($data as $value):

  $studentid=$value->ideleve_rating;
  $idrating=$value->id_rating;
  $rating=$value->rating;
  $raking=$i;

  $mention=getratingMention($rating);

  $code4="UPDATE rating SET raking_rating='$raking',mention_rating='$mention' where id_rating='$idrating' and ideleve_rating='$studentid' and classe_rating='$classeid' and matiereid_rating='$matiereid' and idprof_rating='$teatcherid' and codeEtab_rating='$codeEtab' and session_rating='$sessionEtab' and typsession_rating='$typesessionNotes'";
  $req4=$db->dataBase->prepare($code4);
  $req4->execute([]);


  $i++;
endforeach;


  }



endforeach;

echo json_encode('success');


}

function getratingMention($rating)
      {
        $mention="";
        if($rating<5)
        {
          $mention="Faible";
        }else if($rating>5 && $rating<10 )
        {
          $mention="Insuffisant";
        } else if($rating>=10 && $rating<12 )
        {
          $mention="Passable";
        }else if($rating>=12 && $rating<14 )
        {
          $mention="Assez bien";
        }else if($rating>=14 && $rating<16 )
        {
          $mention="Bien";
        }else if($rating>=16 && $rating<17.5 )
        {
          $mention="Tr�s bien";
        }else if($rating>18)
        {
          $mention="Excellent";
        }

        return $mention;

      }

?>

