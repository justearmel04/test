<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}


//require_once('../../class1/User.php');

//require_once('../../class1/cnx.php');

//require_once('../../class1/Parent.php');

//require_once('../../class1/Etablissement.php');

require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();

//$users = new User();

//$parents=new ParentX();

//$etabs=new Etab();





if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

  	$matri=$request->matri;
	$nomad=$request->nomad;
	$prenomad=$request->prenomad;
        $datenaisad=$request->datenaisad;
        $lieunais=$request->lieunais;     
        $sexe=$request->sexe;     
	$statuseleve=$request->statuseleve;
	$doublant=$request->doublant;
	$classeEtab=$request->classeEtab;
        $lastschool=$request->lastschool;
        $codeEtab=$request->codeEtab;
        $sessionEtab=$request->sessionEtab;
        $habitation=$request->habitationlocation;
        $parents=$request->parents;

	$datecrea=date("Y-m-d");
	$type_cpte="Student";
	$statut=1;
        $affecter=1;
        $numclient="";
        $allparentids="";
        $formulecantine=0;

        if(count($parents)>0)
         {

 foreach($parents as $values):

      $datecreaparent=date("Y-m-d");
      $type_cpteParent="Parent";
      $statutparent=1;

      $nomparent=$values->nomparent;
      $prenomparent=$values->prenomparent;
      $phoneparent=$values->phonebureauparent;
      $sexeparent=$values->sexeparent;
      $metierparent=$values->metierparent;
      $employeurparent=$values->employeurparent;
      $postaleadressparent=$values->postaleadressparent;
      $phonehomeparent=$values->phonehomeparent;
      $emailparent=$values->emailparent;
      $mobileparent=$values->mobileparent;
      $lieuh=$values->lieuh;
      //nous allons ajouter le compte parent

     $code="INSERT INTO  compte SET nom_compte=?,prenom_compte=?,tel_compte=?,sexe_compte=?,fonction_compte=?,societe_compte=?,adresse_compte=?,email_compte=?,datecrea_compte=?,type_compte=?,statut_compte=?,telBuro_compte=?";
     $req=$db->dataBase->prepare($code);
     $req->execute([$nomparent,$prenomparent,$mobileparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$emailparent,$datecreaparent,$type_cpteParent,$statutparent,$phoneparent]

);
     $idlastcompte=$db->dataBase->lastInsertId();

     $code1="INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,sexe_parent=?,profession_parent=?,societe_parent=?,postaleadres_parent=?,phoneBuro_parent=?,email_parent=?,statut_parent=?,idcompte_parent=?";
     $req1=$db->dataBase->prepare($code1);
     $req1->execute([$nomparent,$prenomparent,$mobileparent,$sexeparent,$metierparent,$employeurparent,$postaleadressparent,$phoneparent,$emailparent,$statutparent,$idlastcompte]);

     $code2="INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?";
     $req2=$db->dataBase->prepare($code2);
     $req2->execute([$idlastcompte,$codeEtab]);

    $allparentids=$allparentids.$idlastcompte."@";

 endforeach;



         }

      $taballparents=explode("@",$allparentids);
      $nball=count($taballparents)-1;

//insertion de l'�l�ve dans le syst�me

     $fonction="Eleve";

    $code3="INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,lieuh_compte=?";
    $req3=$db->dataBase->prepare($code3);
    $req3->execute([$nomad,$prenomad,$datenaisad,$fonction,$type_cpte,$statut,$datecrea,$habitation]);
    $studentid=$db->dataBase->lastInsertId();

    $code4="INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,codeEtab_eleve=?,idcompte_eleve=?,affecter_Id_eleve=?,numclient_eleve=?,nationalite_eleve=?";
    $req4=$db->dataBase->prepare($code4);
    $req4->execute([$matri,$nomad,$prenomad,$datenaisad,$lieunais,$sexe,$codeEtab,$studentid,$affecter,$numclient,$nationalitestudent]);

    $code5="INSERT INTO  inscription SET idclasse_inscrip=?,ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?,formulecantine_inscrip=?,previouschool_inscrip=?";
    $req5=$db->dataBase->prepare($code5);
    $req5->execute([$classeEtab,$studentid,$sessionEtab,$datecrea,$codeEtab,$formulecantine,$lastschool]);


    if($nball>0)
   {
     for($i=0;$i<$nball;$i++)
        {
          $parentid=$taballparents[$i];
         
           //nous allons verivier si le parent n'est �s deja li� a cet enfant
          //$verif=$student->DetermineNumberOfparenter($parentid,$studentid);

          $code6="SELECT * from parenter where   parentid_parenter=? and eleveid_parenter=? ";
          $req6=$db->dataBase->prepare($code6);
          $req5->execute([$parentid,$studentid]);
          $data5=$req5->fetchAll();
          $verif=count($data5);


          if($verif==0)
          {
            // echo "ajouter";
            //$student->Addparenter($parentid,$studentid);
           $code7="INSERT INTO  parenter SET parentid_parenter=?,eleveid_parenter=?";
           $req7=$db->dataBase->prepare($code7);
           $req7->execute([$parentid,$studentid]);


          }else {
            // echo "non ajouter";
          }
        }
   }


   echo json_encode('success');


 	}




	    
?>