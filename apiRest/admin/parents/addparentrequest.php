<?php

// Allow from any origin

if (isset($_SERVER['HTTP_ORIGIN'])) {

    // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one

    // you want to allow, and if so:

    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    header('Access-Control-Allow-Credentials: true');

    header('Access-Control-Max-Age: 86400');    // cache for 1 day

    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
    header("Content-type: application/json; charset=utf-8");

}

// Access-Control headers are received during OPTIONS requests

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        // may also be using PUT, PATCH, HEAD etc
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    exit(0);

}





require_once('../../class/cnx.php');

require_once('../../class1/functions.php');

$db = new mysqlConnector();



if ($_SERVER['REQUEST_METHOD'] === 'POST'){

//Recuperation des donnees de la premiere etape : Infos Client


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$codeEtab=utf8_decode($request->codeEtab);
$sessionEtab=utf8_decode($request->sessionEtab);
$nom=utf8_decode($request->nom);
$prenom=utf8_decode($request->prenom);
$datenais=utf8_decode($request->datenais);
$sexe=utf8_decode($request->sexe);
$contact=utf8_decode($request->contact);
$fonction=utf8_decode($request->fonction);
$cni=utf8_decode($request->cni);
$newparent=1;
$nbchild=1;
$nbchildsco=1;
$email=utf8_decode($request->email);
$nationalite=utf8_decode($request->nationalite);
$lieuH=utf8_decode($request->lieuhabitation);
$adressepro=utf8_decode($request->adressepro);
$societe=utf8_decode($request->societe);

$datecrea=date("Y-m-d");
$type_cpte="Parent";
$statut=1;


 $code="INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?";
 $req=$db->dataBase->prepare($code);
 $req->execute([$nom,$prenom,$datenais,$contact,$email,$fonction,$type_cpte,$statut,$datecrea]);

 $idlastcompte=$db->dataBase->lastInsertId();

 $code1="INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?,nationalite_parent=?,lieuH_parent=?,nbchild_parent=?,nbchidsco_parent=?,adressepro_parent=?,societe_parent=?";
 $req1=$db->dataBase->prepare($code1);
 $req1->execute([$nom,$prenom,$contact,$fonction,$cni,$email,$statut,$sexe,$idlastcompte,$nationalite,$lieuH,$nbchild,$nbchildsco,$adressepro,$societe]);

 $code2="INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?";
 $req2=$db->dataBase->prepare($code2);
 $req2->execute([$idlastcompte,$codeEtab]);

echo json_encode('success');
//echo json_encode($teatcherid);

}


?>

