<?php

class ParentX{



public $db;

function __construct() {

      require_once('../../class1/cnx.php');
require_once('../../class1/functions.php');
     
    $db = new mysqlConnector();

    $this->db= $db->dataBase;

      }

function getAllTeatcherOfThisClasseSchool($codeEtab,$sessionid,$classeid)
{
  $parent=new ParentX();

  $req = $this->db->prepare("SELECT distinct compte.id_compte,compte.nom_compte,compte.prenom_compte,compte.type_compte,enseignant.type_enseignant,enseignant.sexe_enseignant,enseignant.situation_enseignant,enseignant.nat_enseignant,compte.tel_compte,compte.email_compte,dispenser.id_cours,dispenser.codeEtab,dispenser.session_disp FROM compte,enseignant,dispenser where compte.id_compte=enseignant.idcompte_enseignant and dispenser.id_enseignant=enseignant.idcompte_enseignant and dispenser.idclasse_disp=? and dispenser.codeEtab=? and session_disp=? ");
  $req->execute([$classeid,$codeEtab,$sessionid]);
  $data= $req->fetchAll();
  $outp = "";
  if ($outp != "") {$outp .= ",";}
  foreach ($data as $value):
     $libmat=$parent->getMatiereLibelleByIdMat($value->id_cours,$value->codeEtab);
     $coeflat=$parent->getMatcoef($value->id_cours,$value->codeEtab,$value->session_disp);
    $outp .= '{"Id":"'  . $value->id_compte . '",';
    $outp .= '"nom":"'   . $value->nom_compte  . '",';
    $outp .= '"telephone":"'   . $value->tel_compte  . '",';
    $outp .= '"nationalite":"'   . $value->nat_enseignant  . '",';
    $outp .= '"fonction":"'   . $value->fonction_compte  . '",';
    $outp .= '"email":"'   . $value->email_compte  . '",';
    $outp .= '"situation":"'   . $value->situation_enseignant  . '",';
    $outp .= '"sexe":"'   . $value->sexe_enseignant  . '",';
    $outp .= '"typeprof":"'   . $value->type_enseignant  . '",';
    $outp .= '"codeEtab":"'   . $value->codeEtab  . '",';
    $outp .= '"session":"'   . $value->session_disp  . '",';
    $outp .= '"photo":"'   . $value->photo_compte  . '",';
    $outp .= '"matiereid":"'   . $value->id_cours  . '",';
    $outp .= '"matiere":"'   . $libmat  . '",';
    $outp .= '"coefmat":"'   . $coeflat  . '",';
    $outp .= '"typecompte":"'   . $value->type_compte  . '"}';

  endforeach;

   echo "[".$outp."]" ;


  //return json_encode($data);
}

function getMatiereLibelleByIdMat($matiereid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where id_mat=? and codeEtab_mat=?");
        $req->execute([$matiereid,$codeEtab]);
        $data=$req->fetchAll();

         $array=json_encode($data,true);
         $someArray = json_decode($array, true);

         $donnees=$someArray[0]["libelle_mat"];
           return $donnees;
      }

 function getMatcoef($matId,$codeEtabLocal,$session)
      {
        $req = $this->db->prepare("SELECT * FROM matiere  where id_mat=? and codeEtab_mat=? and session_mat=?");
        $req->execute([$matId,$codeEtabLocal,$session]);
        $data=$req->fetchAll();

         $array=json_encode($data,true);
         $someArray = json_decode($array, true);

         $donnees=$someArray[0]["coef_mat"];
           return $donnees;
      }

function getallprofsofstudents($studentid)
{
  // $this->db->query('SET SQL_BIG_SELECTS=1');
  $encours=1;
  $req = $this->db->prepare("SELECT * FROM compte,enseignant,dispenser,matiere,inscription WHERE compte.id_compte=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=dispenser.id_enseignant AND dispenser.id_cours=matiere.id_mat AND inscription.codeEtab_inscrip=dispenser.codeEtab AND dispenser.idclasse_disp=(SELECT DISTINCT classe.id_classe FROM classe,inscription,etablissement,sessions WHERE classe.id_classe=inscription.idclasse_inscrip AND etablissement.code_etab=inscription.codeEtab_inscrip AND sessions.codeEtab_sess=etablissement.code_etab AND sessions.encours_sess=? AND inscription.ideleve_inscrip=?)");
  $req->execute([$encours,$studentid]);
  $data= $req->fetchAll();


  //return json_encode($data, JSON_FORCE_OBJECT);
  return json_encode($data);

}

function getperiode($codeEtab,$session)
{
  
  $req = $this->db->prepare("SELECT distinct id_semes as Id,libelle_semes as designation,statut_semes as statut FROM sessions,semestre WHERE sessions.codeEtab_sess=semestre.codeEtab_semes and sessions.codeEtab_sess=? AND sessions.libelle_sess=? AND sessions.encours_sess=1 ORDER BY semestre.id_semes ASC");
  $req->execute([$codeEtab,$session]);
  $data= $req->fetchAll();
  return json_encode($data);
   /*$outp = "";
  if ($outp != "") {$outp .= ",";}
  foreach ($data as $value):
    $outp .= '{"Id":"'  . $value->id_semes . '",';
    $outp .= '"designation":"'   . $value->libelle_semes  . '",';
    $outp .= '"statut":"'   . $value->statut_semes  . '"}';
  endforeach;

   echo "[".$outp."]" ;*/


}


function getAlletabOfStudentParentNew($IdCompte)

{

  $session="2019-2020";

  $encours=1;

  $req = $this->db->prepare("SELECT  distinct 	code_etab,libelle_etab FROM etablissement,classe,eleve,inscription,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and parent.idcompte_parent=? and inscription.session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) ");

  $req->execute([$IdCompte,$encours]);

  $data= $req->fetchAll();

  return json_encode($data, JSON_FORCE_OBJECT);
      
}

function getallstudentsByParentId($IdCompte)
{
  $encours=1;
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from compte,eleve,parent,parenter,inscription,classe,etablissement,pays where compte.id_compte=eleve.idcompte_eleve and  parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve in(SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?))");
  $req->execute([$IdCompte,$encours,$IdCompte]);
   $data= $req->fetchAll();

  //return json_encode($data, JSON_FORCE_OBJECT);
  return json_encode($data);
}
/*
function getDifferentStudentByParentId($IdCompte)
{
    $this->db->query('SET SQL_BIG_SELECTS=1');
    $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte,inscription where compte.id_compte=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve  and parent.idcompte_parent=? ");
    $req->execute([$IdCompte]);
    return $req->fetchAll();
}*/



function getDifferentStudentByParentId($IdCompte)

{

    $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parent.idcompte_parent=? ");

    $req->execute([$IdCompte]);

   $data= $req->fetchAll();

  //return json_encode($data, JSON_FORCE_OBJECT);
return json_encode($data);
}

function getStudentCurrentlyinscription($IdCompte,$ideleve)
{
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from eleve,parent,parenter,inscription,classe,etablissement,pays where   parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve=? order by id_inscrip DESC limit 1  ");
  $req->execute([$IdCompte,$ideleve]);
   return $req->fetchAll();
}

function determineparentsons($IdCompte)
{
$req = $this->db->prepare("select DISTINCT compte.id_compte from parenter,compte where parenter.eleveid_parenter=compte.id_compte and parenter.parentid_parenter=?");
$req->execute([$IdCompte]);
$data= $req->fetchAll();

return $data;

}

function getsouscriptionStudentParent($IdCompte)

{

	$parents= new ParentX();

   /* $req = $this->db->prepare("SELECT * from eleve,parent,parenter,compte where compte.id_compte=eleve.idcompte_eleve and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and parent.idcompte_parent=? ");

    $req->execute([$IdCompte]);

   $data= $req->fetchAll();*/
   $encours=1;
  $this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * from compte,eleve,parent,parenter,inscription,classe,etablissement,pays where compte.id_compte=eleve.idcompte_eleve and  parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and classe.session_classe=inscription.session_inscrip and  inscription.idclasse_inscrip=classe.id_classe and etablissement.code_etab=classe.codeEtab_classe and etablissement.pays_etab=pays.id_pays and parent.idcompte_parent=? and eleve.idcompte_eleve in(SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?))");
  $req->execute([$IdCompte,$encours,$IdCompte]);
   $data= $req->fetchAll();


  //return json_encode($data, JSON_FORCE_OBJECT);

  $outp = "";

    foreach ($data as  $value):
    if ($outp != "") {$outp .= ",";}
   $dataSchool=$parents->getStudentCurrentlyinscription($IdCompte,$value->idcompte_eleve);
   $dateday=date("Y-m-d");
   $datasouscriptions=$parents->getsouscriptionInfosActivenb($IdCompte,$value->idcompte_eleve,$dateday);
   $nbsouscriptionactive=count($datasouscriptions);
                                            // if($nbsouscriptionactive)
   $array=json_encode($dataSchool,true);
   $someArray = json_decode($array, true);
   $donnees=$someArray[0]["codeEtab_inscrip"]."*".$someArray[0]["id_classe"]."*".$someArray[0]["libelle_etab"]."*".$someArray[0]["libelle_classe"]."*".$someArray[0]["pays_etab"]."*".$someArray[0]["devises_pays"];
   $escampe=explode("*",$donnees);
   $codeEtab=$escampe[0];
   $idclasse=$escampe[1];
   $libelleEtab=$escampe[2];
   $libelleclasse=$escampe[3];
   $idpays=$escampe[4];
   $devisespays=$escampe[5];
   $date_paiab="";
   $datevalide="";
   $statut=0;

   if($nbsouscriptionactive>0)
   {
     //nous allons verifier le statut du paiement

       $array1=json_encode($datasouscriptions,true);
       $someArray1 = json_decode($array1, true);

       if($someArray1[0]["statut_paiab"]==1)
       {
         $dataslastsous=$parents->getsouscriptionInfosActive($IdCompte,$value->idcompte_eleve,$dateday);
         $array=json_encode($dataslastsous,true);
         $someArray = json_decode($array, true);
         $date_paiab=date_format(date_create($someArray[0]["datedeb_histoabn"]),"d/m/Y");
         $datevalide=date_format(date_create($someArray[0]["datefin_histoabn"]),"d/m/Y");
         $statut=1;
       }else if($someArray1[0]["statut_paiab"]==0){
         $date_paiab=date_format(date_create($someArray1[0]["date_paiab"]),"d/m/Y");
         $datevalide="";
         $statut=0;
       }

   }

   $outp .= '{"matricule_eleve":"'.$value->matricule_eleve.'",';
   $outp .= '"photo_eleve":"'.$value->photo_compte.'",';
   $outp .= '"nom_eleve":"'.$value->nom_eleve.'",';
   $outp .= '"prenom_eleve":"'.$value->prenom_eleve.'",';
   $outp .= '"classeid_eleve":"'.$value->id_classe.'",';
   $outp .= '"classe_eleve":"'.$value->libelle_classe.'",';
   $outp .= '"codeEtab_eleve":"'.$value->code_etab.'",';
   $outp .= '"libelleEtab_eleve":"'.$value->libelle_etab.'",';
   $outp .= '"sessionEtab_eleve":"'.$value->session_inscrip.'",';
   $outp .= '"typeEtab_eleve":"'.$value->type_etab.'",';
   $outp .= '"paysidEtab_eleve":"'.$value->id_pays.'",';
   $outp .= '"devisepaysEtab_eleve":"'.$value->devises_pays.'",';
   $outp .= '"datedeb_sous":"'.$date_paiab.'",';
   $outp .= '"datefin_sous":"'.$datevalide.'",';
   $outp .= '"parent_id":"'.$IdCompte.'",';
   $outp .= '"statut_sous":"'.$statut.'",';
   $outp .= '"eleve_id":"'.$value->idcompte_eleve. '"}';

    endforeach;

   echo "[".$outp."]" ;



}
function getsouscriptionInfosActivenb($parentid,$eleveid,$dateday)
{
  //$req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=4 and parentid_paiab=? and studentsid_paiab=? and datevalide_paiab>? order by id_paiab desc");
  $req = $this->db->prepare("SELECT * FROM paiementab where (statut_paiab=1 or statut_paiab=0) and parentid_paiab=? and studentsid_paiab=? order by id_paiab desc");
  //$req->execute([$parentid,$eleveid,$dateday]);
  $req->execute([$parentid,$eleveid]);
  return $req->fetchAll();
}

function getsouscriptionInfosActive($parentid,$eleveid,$dateday)
{
  //$req = $this->db->prepare("SELECT * FROM paiementab where statut_paiab=4 and parentid_paiab=? and studentsid_paiab=? and datevalide_paiab>? order by id_paiab desc limit 1");
   $req = $this->db->prepare("SELECT * FROM paiementab,abonnementhisto WHERE paiementab.id_paiab=abonnementhisto.paieid_histoabn and statut_paiab=1 and parentid_paiab=? and studentsid_paiab=?  order by id_paiab desc limit 1");
  //$req->execute([$parentid,$eleveid,$dateday]);
  $req->execute([$parentid,$eleveid]);
  return $req->fetchAll();
}

function AddParentPaiement($datecrea,$parentidcompte,$studentidcompte,$montantchoice,$selectmobileop,$transacId,$paymentno,$statutpaie,$nbselect,$subscribeIdarray,$devisecontry)
{

$req = $this->db->prepare("INSERT INTO  paiementab SET date_paiab=?,parentid_paiab=?,studentsid_paiab=?,montant_paiab=?,operateur_paiab=?,transacid_paiab=?,number_paiab=?,statut_paiab=?,nbstudent_paiab=?,subscribes_paiab=?,devises_paiab=?");
  $req->execute([
  $datecrea,
  $parentidcompte,
  $studentidcompte,
  //$idparentcpte,
  //$studentIdentarray,
  //$montanttotale,
  $montantchoice,
  $selectmobileop,
  $transacId,
  $paymentno,
  $statutpaie,
  $nbselect,
  $subscribeIdarray,
  $devisecontry
  ]);

 $idlastcompte=$this->db->lastInsertId();

return $idlastcompte;

}

function getcountryoperators($paysid)
{
  $req = $this->db->prepare("SELECT * FROM mobileoperator,pays WHERE mobileoperator.pays_mob=pays.id_pays and pays_mob=? ");
  $req->execute([$paysid]);
  $data=$req->fetchAll();
  return json_encode($data);
}

function geNumberOfStudentByParentId($IdCompte)

{

  $encours=1;

    $req = $this->db->prepare("SELECT DISTINCT eleve.idcompte_eleve FROM eleve,parenter,inscription where eleve.idcompte_eleve=parenter.eleveid_parenter and parenter.eleveid_parenter=inscription.ideleve_inscrip and eleve.idcompte_eleve in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

    $req->execute([$encours,$IdCompte]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;

}



function getnumberofstudentparent($IdCompte)

{

  $encours=1;



  $req = $this->db->prepare("SELECT DISTINCT parenter.parentid_parenter from parenter where parenter.eleveid_parenter in(SELECT DISTINCT parenter.eleveid_parenter FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

  $req->execute([$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}



function getnumberofstudentparentpresences($IdCompte)

{

  $encours=1;

  $day=date("Y-m-d");

  $req = $this->db->prepare("SELECT DISTINCT presences.matricule_presence from presences where presences.statut_presence=1 and presences.date_presence=? and presences.matricule_presence in(SELECT DISTINCT eleve.matricule_eleve FROM parenter,inscription,eleve where parenter.eleveid_parenter=inscription.ideleve_inscrip and parenter.eleveid_parenter=eleve.idcompte_eleve and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?) ");

  $req->execute([$day,$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}

function getnumberofprofclasse($classeid,$codeEtab,$session)
{
  $req = $this->db->prepare("SELECT * FROM dispenser WHERE dispenser.idclasse_disp=? and dispenser.codeEtab=? and dispenser.session_disp=?");
  $req->execute([$classeid,$codeEtab,$session]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}



function getnumberofprofByparentId($IdCompte)

{

  $encours=1;

  $req = $this->db->prepare("SELECT DISTINCT enseignant.idcompte_enseignant from enseignant,dispenser WHERE enseignant.idcompte_enseignant=dispenser.id_enseignant and dispenser.idclasse_disp=(SELECT DISTINCT inscription.idclasse_inscrip FROM parenter,inscription where parenter.eleveid_parenter=inscription.ideleve_inscrip and session_inscrip=(SELECT distinct libelle_sess from sessions,etablissement where sessions.codeEtab_sess=etablissement.code_etab and encours_sess=? ) and parentid_parenter=?)");

  $req->execute([$encours,$IdCompte]);

  $data=$req->fetchAll();

  $nb=count($data);

  return $nb;

}



}


 ?>

