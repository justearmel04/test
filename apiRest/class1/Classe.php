<?php

class Classe{

public $db;
function __construct() {

  require_once('../class/cnx.php');

  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getAllClassesOfParentHadStudent($code,$parentid,$session)
{

  $req = $this->db->prepare("SELECT  distinct 	libelle_classe,id_classe FROM etablissement,classe,eleve,inscription,parent,parenter where parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=eleve.idcompte_eleve and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and classe.codeEtab_classe=etablissement.code_etab and  inscription.session_inscrip=? and parent.idcompte_parent=? and etablissement.code_etab=? ");
  $req->execute([$session,$parentid,$code]);
  return $req->fetchAll();
}




}

 ?>
