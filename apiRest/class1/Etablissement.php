<?php

class Etab{

  public $db;

  function __construct() {

      require_once('../../class1/cnx.php');
require_once('../../class1/functions.php');

    $db = new mysqlConnector();

    $this->db= $db->dataBase;

      }

function returnHours($hours)
{
  $tabhours=explode(":",$hours);
  $heures=$tabhours[0];
  $minutes=$tabhours[1];

  return $heures."H".$minutes;
}

function getNumberSessionEncoursOne($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $nb=count($data);
  if($nb>0)
    {
     $sessionEtab="";
	$req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
        $req->execute([$codeEtabAssigner,$encours]);
        $data=$req->fetchAll();
        foreach($data as $values):
        $sessionEtab=$values->libelle_sess;
        endforeach;
    }

 return json_encode($sessionEtab);
}

function getAllClassesTeatchers($idcompte,$codeEtab,$sessionEtab)
{

 $req=$this->db->prepare("SELECT distinct classe.id_classe,classe.libelle_classe,classe.codeEtab_classe,classe.session_classe from classe,dispenser where classe.id_classe=dispenser.idclasse_disp and dispenser.id_enseignant=? and dispenser.codeEtab=? and dispenser.session_disp=?");
 $req->execute([$idcompte,$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);

}

function getTheAllclasseifteatchers($idcompte,$codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT distinct classe.id_classe,classe.libelle_classe,classe.codeEtab_classe,classe.session_classe from classe,dispenser where classe.id_classe=dispenser.idclasse_disp and dispenser.id_enseignant=? and dispenser.codeEtab=? and dispenser.session_disp=?");
 $req->execute([$idcompte,$codeEtab,$sessionEtab]);
$data=$req->fetchAll();
return  json_encode($data);


}


function getTheAllcourses($codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and classe.id_classe=matiere.classe_mat and compte.id_compte=matiere.teatcher_mat and courses.codeEtab_courses=? and courses.sessionEtab_courses=? order by matiere.libelle_mat ASC");
 $req->execute([$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);


}

function getTheAllSalles($codeEtab)
{

 $req=$this->db->prepare("SELECT * from salle where codeEtab_salle=? order by libelle_salle ASC");
 $req->execute([$codeEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);

}


function getTheAllParents($codeEtab)

{

$req=$this->db->prepare("SELECT * from compte,parent,enregistrer where compte.id_compte=parent.idcompte_parent and compte.id_compte=enregistrer.idparent_enreg and enregistrer.codeEtab_enreg=? order by compte.nom_compte ASC,compte.prenom_compte ASC");
$req->execute([$codeEtab]);
 $data=$req->fetchAll();
return  json_encode($data);

}

function getTheUsersById($idcompte)

{

$req=$this->db->prepare("SELECT * from compte where compte.id_compte IN (?) order by nom_compte ASC,prenom_compte ASC");
$req->execute([$idcompte]);
$data=$req->fetchAll();
return  json_encode($data);

}


function getParentsOne($codeEtab)
{

$req=$this->db->prepare("SELECT * from compte where compte.id_compte IN (SELECT distinct idparent_enreg from enregistrer WHERE  enregistrer.codeEtab_enreg=?)");
$req->execute([$codeEtab]);
 $data=$req->fetchAll();
return  json_encode($data);

}


function getTheAllParentsOne($codeEtab)

{

//$etabs=new Etab();

//$req=$this->db->prepare("SELECT * from compte,enregistrer where compte.id_compte=enregistrer.idparent_enreg and compte.type_compte='Parent' and statut_compte=1 and enregistrer.codeEtab_enreg=? ");
//$req->execute([$codeEtab]);
$req=$this->db->prepare("SELECT distinct idparent_enreg from enregistrer WHERE  enregistrer.codeEtab_enreg=?");
$req->execute([$codeEtab]);
 $data=$req->fetchAll();

$outp = "";

$concat="";

foreach($data as $value):

    $idcompte=$value->idparent_enreg;
  $concat=$concat.$value->idparent_enreg.",";

  /* $datas=$etabs->getTheUsersById($idcompte);

   foreach($datas as $values):
    if ($outp != "") {$outp .= ",";}
    $nom=$values->nom_compte;
    $prenom=$values->prenom_compte;
    $telephone=$values->tel_compte;
    $email=$values->email_compte;
    $fonction=$values->fonction_compte;
    $genre=$values->sexe_compte;

    $outp .= '"nom":"'.$nom.'",';
    $outp .= '"prenom":"'.$prenom.'",';
    $outp .= '"telephone":"'.$telephone.'",';
    $outp .= '"email":"'.$email.'",';
    $outp .= '"genre":"'.$genre.'",';
    $outp .= '"fonction":"'.$fonction. '"}';
   

   endforeach;*/

endforeach;	

$concat=substr($concat,0,-1);

echo "BONJOUR MONSIEUR";


}

function getAllclassesInfos($classeid,$codeEtab,$sessionEtab)
{
  $req=$this->db->prepare("SELECT * from classe where classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=?");
  $req->execute([$classeid,$codeEtab,$sessionEtab]);
  $data=$req->fetchAll();

}


function getTheAllAdmin($codeEtab)
{
$req=$this->db->prepare("SELECT * from compte,assigner where compte.id_compte=assigner.id_adLocal and assigner.codeEtab_assign=? and compte.type_compte='Admin_locale'");
$req->execute([$codeEtab]);
 $data=$req->fetchAll();
return  json_encode($data);


}

function getAllMessagesSendAndreceive($idcompte,$codeEtab,$sessionEtab)
      {
        $req = $this->db->prepare("SELECT * from messages where messages.addby_msg=? or messages.id_msg IN(SELECT distinct idmsg_lec  from lecturemessages where idcompte_lec=? ) and messages.codeEtab_msg=? and messages.session_msg=? order by messages.id_msg DESC");
        $req->execute([$idcompte,$idcompte,$codeEtab,$sessionEtab]);
        $data=$req->fetchAll();
return  json_encode($data);

      }


function getTheAllMatieres($codeEtab,$sessionEtab)
{
$req=$this->db->prepare("SELECT * from matiere,compte where matiere.teatcher_mat=compte.id_compte and matiere.codeEtab_mat=? and matiere.session_mat=?");
$req->execute([$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);
}

function getTheAllparascolaires($codeEtab,$sessionEtab)
{
$req=$this->db->prepare("SELECT * from activites where codeEtab_act=? and session_act=? and statut_act=1");
$req->execute([$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);

}


function getThescolatitesyears($codeEtab)
{

$req=$this->db->prepare("SELECT * from sessions,semestre where sessions.id_sess=semestre.idsess_semes and sessions.codeEtab_sess=? order by sessions.id_sess DESC  ");
$req->execute([$codeEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);


}


function getTheAllquizs($codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from quiz,matiere,classe,compte where quiz.matiere_quiz=matiere.id_mat and matiere.classe_mat=classe.id_classe and compte.id_compte=matiere.teatcher_mat and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? order by matiere.libelle_mat ASC");
$req->execute([$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
/*$outp = "";
foreach($data as $values):
if ($outp != "") {$outp .= ",";}
$classeid=$values->id_classe;
$matiereid=$values->id_mat;
$libelleclasse=$values->libelle_classe;
$libellematiere=$values->libelle_mat;
$libellequiz=$values->libelle_mat;
$libellequiz=$values->libelle_quiz;
$duree=$values->duree_quiz;
endforeach;
return $data;*/

 return  json_encode($data);

}

function getthedetailsofstudents($idcompte,$classeid,$codeEtab,$sessionEtab)
{

   //nous allons rechercher le matricule 

   $tak=$this->db->prepare("SELECT matricule_eleve from eleve where idcompte_eleve=?");
   $tak->execute([$idcompte]);
   $datatak=$tak->fetchAll();
  $matricule="";
foreach($datatak as $valuetak):
$matricule=$valuetak->matricule_eleve;
endforeach;

    $req = $this->db->prepare("SELECT * from compte,eleve,inscription,classe where compte.id_compte=eleve.idcompte_eleve and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and compte.id_compte=?");
    $req->execute([$sessionEtab,$idcompte]);
    $data=$req->fetchAll();
    $outp = "";

    $req1 = $this->db->prepare("SELECT  * FROM compte,parent,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=?");
    $req1->execute([$idcompte]);
    $data1=$req1->fetchAll();
    $outp1 = "";

  foreach($data1 as $value):
if ($outp1 != "") {$outp1 .= ",";}
  $parentid=$value->id_compte;
  $nomparent=$value->nom_compte;
  $prenomparent=$value->prenom_compte;
  $telparent=$value->tel_compte;
  $emailparent=$value->email_compte;
  $fonctionparent=$value->fonction_compte;
  $sexeparent=$value->sexe_compte;
  $societe=$value->societe_compte;
  $photoparent=$value->photo_compte;
  $nationaliteparent=$value->nationalite_parent;
  $lienphotoparent="photo/user5.jpg";

  if(strlen($emailparent)>0 && strlen($photoparent)>0)
 {
  $lienphotoparent="photo/".$emailparent."/".$photoparent;
 }  


    $outp1 .= '{"parentid":"'.$parentid. '",';
    $outp1 .= '"nomparent":"'.$nomparent.'",';
    $outp1 .= '"prenomparent":"'.$prenomparent.'",';
    $outp1 .= '"telparent":"'.$telparent.'",';
    $outp1 .= '"emailparent":"'.$emailparent.'",';
    $outp1 .= '"fonctionparent":"'.$fonctionparent.'",';
    $outp1 .= '"sexeparent":"'.$sexeparent.'",';
    $outp1 .= '"societe":"'.$societe.'",';
    $outp1 .= '"photoparent":"'.$lienphotoparent.'",';
    $outp1 .= '"nationaliteparent":"'.$nationaliteparent. '"}';

  endforeach;

$outp1="[".$outp1."]";


//nous allons rechercher les absences 

$outp2 = "";

$etabs=new Etab();
	 $req2 = $this->db->prepare("SELECT * FROM presences,heure where presences.libelleheure_presence=heure.id_heure and  matricule_presence=? and classe_presence=? and codeEtab_presence=? and session_presence=? and statut_presence=0 order by date_presence DESC limit 10 ");
         $req2->execute([$matricule,$classeid,$codeEtab,$sessionEtab]);
         $data2=$req2->fetchAll();

         foreach ($data2 as $value2):

         $heures=$etabs->returnHours($value2->heuredeb_heure). " - ".$etabs->returnHours($value2->heurefin_heure);
        $matiere=$etabs->getMatiereLibelleByIdMat($value2->matiere_presence,$value2->codeEtab_presence);
        $statut="A";

          if ($outp2 != "") {$outp2 .= ",";}
         // $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
          $outp2 .= '{"date":"'.date_format(date_create($value2->date_presence), "d/m/Y").'",';
          $outp2 .= '"heure":"'.$heures.'",';
          $outp2 .= '"matiere":"'.$matiere.'",';
 	  $outp2 .= '"statut":"'.$statut. '"}';


	 endforeach;

         $outp2="[".$outp2."]";

//nous allons rechercher les derni�res notes

$notetype=1;
$listdesignation="";

    $req3 = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req3->execute([$idcompte,$classeid,$notetype,$codeEtab,$sessionEtab]);
    $data3=$req3->fetchAll();

    $outp3 = "";
    foreach ($data3 as $value3):
    if ($outp3 != "") {$outp3 .= ",";}
   
   $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$value3->idmat_notes,$classeid,$codeEtab,$sessionEtab,$value3->id_ctrl);
    $tablenotes=explode("*",$datanotes);

   $eleveid=$value3->idcompte_eleve;
    $codeEtab=$value3->codeEtab_notes;
    $session=$value3->session_notes;
    $notes=$value3->valeur_notes;
    $observation=$value3->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value3->idmat_notes;
    $classeid=$value3->idclasse_notes;
    $libelleclasse=$value3->libelle_classe;
    $libellematiere=$value3->libelle_mat;
    $professeurid=$value3->idprof_notes;
    $controleLib=$value3->libelle_ctrl;

    $outp3 .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp3 .= '"codeEtab":"'.$codeEtab.'",';
    $outp3 .= '"session":"'.$session.'",';
    $outp3 .= '"libellecontrole":"'.$controleLib.'",';
    $outp3 .= '"notes":"'.$notes.'",';
    $outp3 .= '"observation":"'.$observation.'",';
    $outp3 .= '"minimum":"'.$minimum.'",';
    $outp3 .= '"maximum":"'.$maximum.'",';
    //$outp .= '"maximum":"'.$maximum.'",';
    $outp3 .= '"moyenne":"'.$moyenne.'",';
    $outp3 .= '"matiereid":"'.$matiereid.'",';
    $outp3 .= '"classeid":"'.$classeid.'",';
    $outp3 .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp3 .= '"libellematiere":"'.$libellematiere.'",';
    $outp3 .= '"professeurid":"'.$professeurid. '"}';


   endforeach;

$outp3="[".$outp3."]";

//les diff�rents versement

        $motif="INSCRIPTIONS";
        $req4 = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? ");
        $req4->execute([$codeEtab,$sessionEtab,$idcompte,$classeid,$motif]);
        $data4=$req4->fetchAll();
    $outp4 = "";
    foreach ($data4 as $value4):
    if ($outp4 != "") {$outp4 .= ",";}

  $codeversement=$value4->code_versement;
  $dateversement=date_format(date_create($value4->date_versement),"d-m-Y");
  $montantversment=number_format($value4->montant_versement,0,',',' ');
  $resteapayer=number_format($value4->solde_versement,0,',',' ');


 $outp4 .= '{"codeversement":"'.$codeversement. '",';
    $outp4 .= '"dateversement":"'.$dateversement.'",';
    $outp4 .= '"montantversment":"'.$montantversment.'",';
    $outp4 .= '"resteapayer":"'.$resteapayer.'",';
    $outp4 .= '"motif":"'.$motif. '"}';
    endforeach;

$outp4="[".$outp4."]";

$motif1="SCOLARITES";
        $req5 = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? and classe_versement=? and motif_versement=? ");
        $req5->execute([$codeEtab,$sessionEtab,$idcompte,$classeid,$motif1]);
        $data5=$req5->fetchAll();
    $outp5 = "";
    foreach ($data5 as $value5):
    if ($outp5 != "") {$outp5 .= ",";}

  $codeversement=$value5->code_versement;
  $dateversement=date_format(date_create($value5->date_versement),"d-m-Y");
  $montantversment=number_format($value5->montant_versement,0,',',' ');
  $resteapayer=number_format($value5->solde_versement,0,',',' ');


 $outp5 .= '{"codeversement":"'.$codeversement. '",';
    $outp5 .= '"dateversement":"'.$dateversement.'",';
    $outp5 .= '"montantversment":"'.$montantversment.'",';
    $outp5 .= '"resteapayer":"'.$resteapayer.'",';
    $outp5 .= '"motif":"'.$motif1. '"}';
    endforeach;

$outp5="[".$outp5."]";


foreach($data as $values):
if ($outp != "") {$outp .= ",";}
$classeid=$values->id_classe;
$libelleclasse=$values->libelle_classe;
$matriculeeleve=$values->matricule_eleve;
$nomeleve=$values->nom_compte;
$prenomeleve=$values->prenom_compte;
$datenaiseleve=$values->datenais_eleve;
$lieunaiseleve=$values->lieunais_eleve;
$sexeleve=$values->sexe_eleve;


    $outp .= '{"matriculeeleve":"'.$matriculeeleve. '",';
    $outp .= '"nomeleve":"'.$nomeleve.'",';
    $outp .= '"prenomeleve":"'.$prenomeleve.'",';
    $outp .= '"datenaiseleve":"'.$datenaiseleve.'",';
    $outp .= '"lieunaiseleve":"'.$lieunaiseleve.'",';
    $outp .= '"sexeleve":"'.$sexeleve.'",';
$outp .= '"classeid":"'.$classeid.'",';
$outp .= '"inscriptions":'.$outp4.',';
$outp .= '"scolarites":'.$outp5.',';
$outp .= '"notes":'.$outp3.',';
$outp .= '"absences":'.$outp2.',';
$outp .= '"parents":'.$outp1.'}';

endforeach;

echo "[".$outp."]" ;









}


function getclassesDetails($classeid,$codeEtab,$sessionEtab)
{
$etabs=new Etab();

  $req=$this->db->prepare("SELECT * from classe where classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=?");
  $req->execute([$classeid,$codeEtab,$sessionEtab]);
  $data=$req->fetchAll();
  $outp = "";

  $req1=$this->db->prepare("SELECT * from matiere,compte where matiere.teatcher_mat=compte.id_compte and matiere.classe_mat=? and matiere.codeEtab_mat=? and matiere.session_mat=? order by matiere.libelle_mat ASC");
  $req1->execute([$classeid,$codeEtab,$sessionEtab]);
  $data1=$req1->fetchAll();
  $outp1 = "";

  foreach($data1 as $value):
if ($outp1 != "") {$outp1 .= ",";}
$matiereid=$value->id_mat;
$libellematiere=$value->libelle_mat;
$coefmatiere=$value->coef_mat;
$teatcherid=$value->id_compte;
$teatchername=$value->nom_compte." ".$value->prenom_compte;

    $outp1 .= '{"matiereid":"'.$matiereid. '",';
    $outp1 .= '"libellematiere":"'.$libellematiere.'",';
    $outp1 .= '"coefmatiere":"'.$coefmatiere.'",';
    $outp1 .= '"teatcherid":"'.$teatcherid.'",';
    $outp1 .= '"teatchername":"'.$teatchername.'",';
    $outp1 .= '"codeEtab":"'.$codeEtab.'",';
    $outp1 .= '"sessionEtab":"'.$sessionEtab. '"}';

    //$outp1 .= '"sessionEtab":"'.$sessionEtab.'"}';
    //$outp1 .= '"routines":'.$outp1.'}';
endforeach;

$outp1="[".$outp1."]";


$nbinscrit=$etabs->DetermineNumberOfStudentInThisClasse($classeid,$codeEtab,$sessionEtab);

foreach($data as $values):
if ($outp != "") {$outp .= ",";}
$classeid=$values->id_classe;
$libelleclasse=$values->libelle_classe;
    $outp .= '{"classeid":"'.$classeid. '",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"nbinscrit":"'.$nbinscrit.'",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"sessionEtab":"'.$sessionEtab.'",';
    $outp .= '"matieres":'.$outp1.'}';

endforeach;

echo "[".$outp."]" ;

}

function getAllcoursesdetailsEtab($courseid,$classeid,$codeEtabsession,$libellesessionencours)
      {
    $req = $this->db->prepare("SELECT * FROM courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=compte.id_compte and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
    $req->execute([$courseid,$classeid,$codeEtabsession,$libellesessionencours]);
    $coursesdetails=$req->fetchAll();
    $outp = "";

    $req1 = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req1->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

   $datasections=$req1->fetchAll();
   $outp1 = "";

   foreach($datasections as $valuesections):
if ($outp1 != "") {$outp1 .= ",";}
$libellesection=$valuesections->libelle_coursesec;
$sectionid=$valuesections->id_coursesec;
    $outp1 .= '{"sectionid":"'.$sectionid. '",';
    $outp1 .= '"libellesection":"'.$libellesection. '"}';

endforeach;

$outp1="[".$outp1."]";


    $req2 = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req2->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

   $datacomp=$req2->fetchAll();
   $outp2 = "";

  foreach($datacomp as $valuescomp):
if ($outp2 != "") {$outp2 .= ",";}
$libellecomp=$valuescomp->libelle_coursecomp;
$compid=$valuescomp->id_coursecomp;
    $outp2 .= '{"compid":"'.$compid. '",';
    $outp2 .= '"libellecomp":"'.$libellecomp. '"}';

endforeach;

$outp2="[".$outp2."]";



   $req3 = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req3->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

  $datahomeworks=$req3->fetchAll();
  $outp3 = "";

foreach($datahomeworks as $valueshome):
if ($outp3 != "") {$outp3 .= ",";}
$libellehomew=$valueshome->libelle_coursewh;
$homeid=$valueshome->id_coursewh;
    $outp3 .= '{"homeid":"'.$homeid. '",';
    $outp3 .= '"libellehomew":"'.$libellehomew. '"}';

endforeach;

$outp3="[".$outp3."]";



  $req4 = $this->db->prepare("SELECT * FROM courses,supports where courses.id_courses=supports.courseid_support and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?  and type_support='COURSES'");
        $req4->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours
        ]);

  $datasupports=$req4->fetchAll();
  $outp4 = "";

foreach($datasupports as $valuesupports):
if ($outp4 != "") {$outp4 .= ",";}
$libellesupport=$valuesupports->fichier_support;
$supportid=$valuesupports->id_support;
$supportype=$valuesupports->type_support;
    $outp4 .= '{"supportid":"'.$supportid. '",';
    $outp4 .= '"supportype":"'.$supportype. '",';
    $outp4 .= '"libellesupport":"'.$libellesupport. '"}';

endforeach;


$outp4="[".$outp4."]";



foreach ($coursesdetails as  $datacourses):
if ($outp != "") {$outp .= ",";}
  $descricourses=$datacourses->descri_courses;
  $durationcourses=$datacourses->duree_courses;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->date_courses;
  $namecourses=$datacourses->libelle_courses;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_courses;
  $filescourses=$datacourses->support_courses;
  $discussion_courses=$datacourses->discussion_courses;
  $teatcherid_courses=$datacourses->teatcher_courses;

    $outp .= '{"classeid":"'.$classeidcourses. '",';
    $outp .= '"libelleclasse":"'.$classecourses.'",';
    $outp .= '"matiereid":"'.$matiereidcoursest.'",';
    $outp .= '"libellematiere":"'.$libellematcourses.'",';
    $outp .= '"courseid":"'.$courseid.'",';
    $outp .= '"libellecourse":"'.$namecourses.'",';
    $outp .= '"datecourse":"'.$datercourses.'",';
    //$outp .= '"datecourse":"'.$datercourses.'",';
    $outp .= '"durationcourses":"'.$durationcourses.'",';
    $outp .= '"statutcourse":"'.$statutcourses.'",';
    $outp .= '"teatcherid":"'.$teatcherid_courses.'",';
    $outp .= '"teatchername":"'.$teatchercourses.'",';
    $outp .= '"sections":'.$outp1.',';
    $outp .= '"competences":'.$outp2.',';
    $outp .= '"homeworks":'.$outp3.',';
    $outp .= '"supports":'.$outp4.'}';


endforeach;

echo "[".$outp."]" ;

    }


function getTheroutinesByclassesid($classeid,$codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from classe where classe.id_classe=? and classe.codeEtab_classe=? and classe.session_classe=?");
$req->execute([$classeid,$codeEtab,$sessionEtab]);
$data=$req->fetchAll();
$concatsemaineday="LUN*MAR*MER*JEU*VEN";
$tabsemaine=explode("*",$concatsemaineday);
$outp = "";
$outp1 = "";

foreach($data as $values):
if ($outp != "") {$outp .= ",";}
$classeid=$values->id_classe;
$libelleclasse=$values->libelle_classe;
    $outp .= '{"classeid":"'.$classeid. '",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"sessionEtab":"'.$sessionEtab.'",';

for($i=0;$i<count($tabsemaine);$i++)
{
  $jour=$tabsemaine[$i];

  $req1=$this->db->prepare("SELECT * from routine,matiere,classe where routine.matiere_route=matiere.id_mat and routine.classe_route=classe.id_classe and routine.day_route=? and routine.etab_route=? and routine.session_route=? and routine.classe_route=? order by routine.debut_route ASC,fin_route ASC ");
  $req1->execute([$jour,$codeEtab,$sessionEtab,$values->id_classe]);
  $datas=$req1->fetchAll();

  foreach($datas as $valuesR):
   if ($outp1 != "") {$outp1 .= ",";}
    $libellematiere=$valuesR->libelle_mat;
    $heuredeb=$valuesR->debut_route;
    $heurefin=$valuesR->fin_route;
    $outp1 .= '{"days":"'.$jour. '",';
    $outp1 .= '"matiere":"'.$libellematiere.'",';
    $outp1 .= '"heuredeb":"'.$heuredeb.'",';
    $outp1 .= '"heurefin":"'.$heurefin. '"}';

  endforeach;


}
$outp1="[".$outp1."]" ;

  $outp .= '"routines":'.$outp1.'}';
// $outp .= '"routines":$outp1"}';
endforeach;



echo "[".$outp."]" ;

}


function getTheroutinesByclasses($codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from classe where classe.codeEtab_classe=? and classe.session_classe=?");
$req->execute([$codeEtab,$sessionEtab]);
$data=$req->fetchAll();
$concatsemaineday="LUN*MAR*MER*JEU*VEN";
$tabsemaine=explode("*",$concatsemaineday);
$outp = "";
$outp1 = "";

foreach($data as $values):
if ($outp != "") {$outp .= ",";}
$classeid=$values->id_classe;
$libelleclasse=$values->libelle_classe;
    $outp .= '{"classeid":"'.$classeid. '",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"sessionEtab":"'.$sessionEtab.'",';

for($i=0;$i<count($tabsemaine);$i++)
{
  $jour=$tabsemaine[$i];

  $req1=$this->db->prepare("SELECT * from routine,matiere,classe where routine.matiere_route=matiere.id_mat and routine.classe_route=classe.id_classe and routine.day_route=? and routine.etab_route=? and routine.session_route=? and routine.classe_route=? order by routine.debut_route ASC,fin_route ASC ");
  $req1->execute([$jour,$codeEtab,$sessionEtab,$values->id_classe]);
  $datas=$req1->fetchAll();

  foreach($datas as $valuesR):
   if ($outp1 != "") {$outp1 .= ",";}
    $libellematiere=$valuesR->libelle_mat;
    $heuredeb=$valuesR->debut_route;
    $heurefin=$valuesR->fin_route;
    $outp1 .= '{"days":"'.$jour. '",';
    $outp1 .= '"matiere":"'.$libellematiere.'",';
    $outp1 .= '"heuredeb":"'.$heuredeb.'",';
    $outp1 .= '"heurefin":"'.$heurefin. '"}';

  endforeach;


}
$outp1="[".$outp1."]" ;

  $outp .= '"routines":'.$outp1.'}';
// $outp .= '"routines":$outp1"}';
endforeach;



echo "[".$outp."]" ;

}

function getThesyllabysbymatieres($codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from syllabus,matiere,classe where syllabus.idmatiere_syllab=matiere.id_mat and matiere.classe_mat=classe.id_classe and matiere.codeEtab_mat=? and session_mat=? order by classe.libelle_classe ASC,matiere.libelle_mat ASC");
$req->execute([$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);

}

function getThesyllabysbymatieresLib($libellemat,$codeEtab,$sessionEtab)
{

$req=$this->db->prepare("SELECT * from syllabus,matiere,classe,matierelib where matierelib.libelle_matlib=matiere.libelle_mat and  syllabus.idmatiere_syllab=matiere.id_mat and matiere.classe_mat=classe.id_classe and matiere.libelle_mat=? and matiere.codeEtab_mat=? and session_mat=? order by classe.libelle_classe ASC,matiere.libelle_mat ASC");
$req->execute([$libellemat,$codeEtab,$sessionEtab]);
 $data=$req->fetchAll();
 return  json_encode($data);

}


function getAllMatieresTeatchers($idcompte,$codeEtab,$sessionEtab)
{

  $req=$this->db->prepare("SELECT * from matiere,classe,dispenser where matiere.id_mat=dispenser.id_cours and dispenser.idclasse_disp=classe.id_classe and dispenser.id_enseignant=? and matiere.codeEtab_mat=? and matiere.session_mat=?");

$req->execute([$idcompte,$codeEtab,$sessionEtab]);
  $data=$req->fetchAll();
  return  json_encode($data);

}


function getAllClassesAdmin($idcompte,$codeEtab)
{
$etabs=new Etab();

  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtab,$encours]);
  $data=$req->fetchAll();
  $sessionEtab="";
  foreach($data as $value):
   $sessionEtab=$value->libelle_sess;
  endforeach;

   $req1 = $this->db->prepare("SELECT * FROM classe,assigner,compte where classe.codeEtab_classe=assigner.codeEtab_assign and assigner.id_adLocal=compte.id_compte and compte.id_compte=? and classe.codeEtab_classe=? and classe.session_classe=? ");
    $req1->execute([$idcompte,$codeEtab,$sessionEtab]);
    //return $req->fetchAll();


    $data1=$req1->fetchAll();
  $outp = "";

  foreach ($data1 as $value):
   if ($outp != "") {$outp .= ",";}

    $classeid=$value->id_classe;
    $libelleclasse=$value->libelle_classe;
    $codeEtab=$value->codeEtab_classe;
    $sessionEtab=$value->session_classe;
    $nbinscrit=$etabs->DetermineNumberOfStudentInThisClasse($classeid,$codeEtab,$sessionEtab);
    $libelleEtab=$value->libelle_etab;

    $outp .= '{"classeid":"'.$classeid. '",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';

    $outp .= '"libelleEtab":"'.$libelleEtab.'",';

    $outp .= '"session":"'.$sessionEtab.'",';
    $outp .= '"nbinscrit":"'.$nbinscrit. '"}';

  endforeach;

echo "[".$outp."]" ;


//return $donnees;

}

function getstudentMatriculebyid($idcompte)
  {
    $req = $this->db->prepare("SELECT * FROM eleve where eleve.idcompte_eleve=?");
    $req->execute([$idcompte]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);

    $someArray = json_decode($array, true);

    $donnees=$someArray[0]["matricule_eleve"];

    return $donnees;
  }

function AllEtabsTeatchers($codeEtab)
{
  $req = $this->db->prepare("SELECT * from compte,enseigner where compte.id_compte=enseigner.id_enseignant and  enseigner.codeEtab=?");
 $req->execute([$codeEtab]);
  $data=$req->fetchAll();
  return  json_encode($data);

}

function getAllTeatchersEtabs($codeEtab)
{

$req = $this->db->prepare("SELECT * from compte,enseigner where compte.id_compte=enseigner.id_enseignant and  enseigner.codeEtab=?");
 $req->execute([$codeEtab]);
  $data=$req->fetchAll();
  return  json_encode($data);

}


function getAbsencesthisdaynbStudentclasseMatiereStudent($dateday,$codeEtabLocal,$libellesessionencours,$classeid,$matiereid,$matriculesStudent)
  {
    $req = $this->db->prepare("SELECT * FROM presences,eleve where presences.matricule_presence=eleve.matricule_eleve and  date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and matiere_presence=? and matricule_presence=? and statut_presence=0  ");

    $req->execute([$dateday,$codeEtabLocal,$libellesessionencours,$classeid,$matiereid,$matriculesStudent]);

    $data=$req->fetchAll();
    $nb=count($data);

    return $nb;
  }

function getlisteabsencesdayStudentclasseMatiereStudent($dateday,$codeEtabLocal,$libellesessionencours,$classeid,$matiereid,$matriculesStudent)
  {
    $req = $this->db->prepare("SELECT * FROM presences,eleve,heure where presences.matricule_presence=eleve.matricule_eleve and presences.libelleheure_presence=heure.id_heure  and date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and matiere_presence=? and matricule_presence=? and statut_presence=0  ");

    $req->execute([$dateday,$codeEtabLocal,$libellesessionencours,$classeid,$matiereid,$matriculesStudent]);

    $data=$req->fetchAll();


    return $data;
  }

function getAbsencesperiodes($studentid,$classeid,$matiereid,$codeEtab,$sessionEtab,$datedeb,$datefin)
{

$etabs=new Etab();

    $datetime1 = date_create($datedeb); // Date fixe
    $datetime2 = date_create($datefin); // Date fixe
    $interval = date_diff($datetime1, $datetime2);
    $nb= $interval->format('%a');
    $matriculesStudent=$etabs->getstudentMatriculebyid($studentid);
    $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
    $outp = "";
    $nbp=0;

      for($x=0;$x<=$nb;$x++)
      {
	$dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));
        $nbabsebnces=$etabs->getAbsencesthisdaynbStudentclasseMatiereStudent($dateday,$codeEtab,$sessionEtab,$classeid,$matiereid,$matriculesStudent);

        if($nbabsebnces>0)
        {
	$nbp++;
	$datas=$etabs->getAbsencesthisdaynbStudentclasseMatiereStudent($dateday,$codeEtab,$sessionEtab,$classeid,$matiereid,$matriculesStudent);

        $datepresences=date_format(date_create($dateday), "d-m-Y");

  foreach ($datas as $value):
   $heuredeb=returnHours($value->heuredeb_heure);
   $heurefin=returnHours($value->heurefin_heure);
  if ($outp != "") {$outp .= ",";}
   $outp .= '{"date":"'.$datepresences.'",';
   $outp .= '"libellematiere":"'.$libellematiere.'",';
   $outp .= '"heuredeb":"'.$heuredeb.'",';
   $outp .= '"heurefin":"'.$heurefin. '"}';



  endforeach;



        }

      }

    return $outp;

}

function getAbsencesperiodesBis($studentid,$classeid,$matiereid,$codeEtab,$sessionEtab,$datedeb,$datefin)
{

$etabs=new Etab();

    $datetime1 = date_create($datedeb); // Date fixe
    $datetime2 = date_create($datefin); // Date fixe
    $interval = date_diff($datetime1, $datetime2);
    $nb= $interval->format('%a');
    $matriculesStudent=$etabs->getstudentMatriculebyid($studentid);
    $libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
    $outp = "";
    $nbp=0;
    $nbp1="";
    $nbp2="";

      for($x=0;$x<=$nb;$x++)
      {
	$dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));
        $nbabsebnces=$etabs->getAbsencesthisdaynbStudentclasseMatiereStudent($dateday,$codeEtab,$sessionEtab,$classeid,$matiereid,$matriculesStudent);

        if($nbabsebnces>0)
        {
	$nbp++;
        $datepresences=date_format(date_create($dateday), "d-m-Y");

       $datas=$etabs->getlisteabsencesdayStudentclasseMatiereStudent($dateday,$codeEtab,$sessionEtab,$classeid,$matiereid,$matriculesStudent);

       $nbp1=$nbp1.$datepresences."*";

      foreach ($datas as $value):
   $heuredeb=$etabs->returnHours($value->heuredeb_heure);
   $heurefin=$etabs->returnHours($value->heurefin_heure);
if ($outp != "") {$outp .= ",";}
   $outp .= '{"date":"'.$datepresences.'",';
   $outp .= '"libellematiere":"'.$libellematiere.'",';
   $outp .= '"heuredeb":"'.$heuredeb.'",';
   $outp .= '"heurefin":"'.$heurefin. '"}';

  $nbp2=$nbp2.$heuredeb."*";


  endforeach;



        }

      }

    $concat="[".$outp."]";
  return $concat;


}



function DeterminationOfNotesNumberForThisclasses($matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes,$classeId)
      {
        $req = $this->db->prepare("SELECT * FROM controle,notes where controle.id_ctrl=notes.idtype_notes and controle.codeEtab_ctrl=? and controle.session_ctrl=? and controle.typesess_ctrl=? and notes.idclasse_notes=? and controle.mat_ctrl=?");
        $req->execute([$codeEtab,$sessionlibelle,$typesessionNotes,$classeId,$matiereid]);
        $data=$req->fetchAll();
        $nb=count($data);

        return $nb;

      }

function AddquizPublication($courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("UPDATE quiz SET statut_quiz=1 where id_quiz=? and teatcher_quiz=? and classe_quiz=? and matiere_quiz=? and codeEtab_quiz=? and sessionEtab_quiz=?");
          $req->execute([
          $courseid,$teatcherid,$classeid,$matiereid,$codeEtab,$sessionEtab
          ]);
      }

//nouvelles fonctions

function getAllMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe,$codeEtab,$sessionEtab)
      {
          $req = $this->db->prepare("SELECT * FROM matiere,classe where matiere.classe_mat=classe.id_classe and matiere.teatcher_mat=? and matiere.classe_mat=? and codeEtab_mat=? and session_mat=? ");
          $req->execute([$teatcherId,$classe,$codeEtab,$sessionEtab]);
          $data=$req->fetchAll();
          return json_encode($data);
      }

  function AddControleClasseSchool($controle,$datectrl,$classe,$matiere,$teatcher,$coef,$codeEtab,$session,$typesess)
{
  $statut=0;
  $req = $this->db->prepare("INSERT INTO controle SET libelle_ctrl=?,date_ctrl=?,classe_ctrl=?,mat_ctrl=?,teatcher_ctrl=?,coef_ctrl=?,codeEtab_ctrl=?,statut_ctrl=?,session_ctrl=?,typesess_ctrl=?");
  $req->execute([
  $controle,
  $datectrl,
  $classe,
  $matiere,
  $teatcher,
  $coef,
  $codeEtab,
  $statut,
  $session,
  $typesess
  ]);
}


function getActiveAllSemestrebyIdsession($sessionencoursid)
{
  $statut=1;
  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? and semestre.statut_semes=?");
  $req->execute([$sessionencoursid,$statut]);
  $data=$req->fetchAll();

    return json_encode($data);}


function getlasnotestudent($idcompte,$listdesignation,$notetype,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getCodeEtabOfStudentInscript($compteuserid)
      {
         $req = $this->db->prepare("SELECT * from inscription,classe,etablissement where inscription.idclasse_inscrip=classe.id_classe and inscription.codeEtab_inscrip=etablissement.code_etab and  inscription.ideleve_inscrip=? order by inscription.id_inscrip desc limit 1");
         $req->execute([$compteuserid]);
         $data=$req->fetchAll();

    return json_encode($data);


      }

function getAllsubjectofclassesbyIdclassesTea($classe,$code,$session,$teatcherid)
{
    // $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=? and teatcher_mat=?");
    $req = $this->db->prepare("SELECT * FROM matiere where classe_mat=? and codeEtab_mat=? and session_mat=? and teatcher_mat=? ");
    $req->execute([
    $classe,
    $code,
    $session,
    $teatcherid
  ]);
    $data=$req->fetchAll();

    return json_encode($data);



}

function DetermineNoteNumbercontroles($idcompte_eleve,$classe,$matiereid,$controleid,$codeEtabAssigner)
  {
      $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and classe.codeEtab_classe=? and notes.type_notes=1 and notes.ideleve_notes=?  ");
      $req->execute([$classe,$matiereid,$controleid,$codeEtabAssigner,$idcompte_eleve]);
      $data=$req->fetchAll();
      $nb=count($data);
      return $nb;
  }

function DetermineNoteNumberexamens($idcompte_eleve,$classe,$matiereid,$examid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and notes.idmat_notes=?  and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");

    $req->execute([$classe,$matiereid,$examid,$codeEtabAssigner,$idcompte_eleve]);
    $data=$req->fetchAll();
    $nb=count($data);
    return $nb;
  }

function getNotescontroleinformations($idcompte_eleve,$classe,$matiereid,$controleid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,controle where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and controle.id_ctrl=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and classe.id_classe=?  and matiere.id_mat=? and controle.id_ctrl=? and classe.codeEtab_classe=? and notes.type_notes=1 and notes.ideleve_notes=?  ");
    $req->execute([$classe,$matiereid,$controleid,$codeEtabAssigner,$idcompte_eleve]);
    return $req->fetchAll();
  }

function getAllstudentofthisclassesSession($classe,$session)
  {

    $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where eleve.idcompte_eleve=compte.id_compte and compte.id_compte=inscription.ideleve_inscrip and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=? and classe.id_classe=? and compte.statut_compte!=0 order by eleve.nom_eleve ASC");

    $req->execute([$session,$classe]);

    return $req->fetchAll();

  }

function getNotesexameninformations($idcompte_eleve,$classe,$matiereid,$examid,$codeEtabAssigner)
  {
    $req = $this->db->prepare("SELECT * from eleve,classe,matiere,notes,examen where notes.ideleve_notes=eleve.idcompte_eleve and notes.idclasse_notes=classe.id_classe and notes.codeEtab_notes=classe.codeEtab_classe and examen.id_exam=notes.idtype_notes and matiere.id_mat=notes.idmat_notes and  classe.id_classe=? and notes.idmat_notes=?  and examen.id_exam=?  and classe.codeEtab_classe=? and notes.type_notes=2 and notes.ideleve_notes=?  ");
    $req->execute([$classe,$matiereid,$examid,$codeEtabAssigner,$idcompte_eleve]);
    return $req->fetchAll();
  }

function getListenotesrequestLast($classeid,$codeEtab,$sessionEtab,$matiereid,$statut,$teacherid)
{
  $etabs=new Etab();

  $students=$etabs->getAllstudentofthisclassesSession($classeid,$sessionEtab);

  $outp = "";

  foreach ($students as $value):
  if ($outp != "") {$outp .= ",";}

  $nomcomplet=$value->nom_eleve." ".$value->prenom_eleve;
  $studentid=$value->idcompte_eleve;
  $matricule=$value->matricule_eleve;
  $allexamens="[]";
  $allcontroles="[]";

   $outp .= '{"studentid":"'.$studentid.'",';
   $outp .= '"matricule":"'.$matricule.'",';
   $outp .= '"nom":"'.$nomcomplet.'",';
   $outp .= '"controles":'.$allcontroles.',';
   $outp .= '"examens":'.$allexamens.',';
   $outp .= '"codeEtab":"'.$codeEtab. '"}';



  endforeach;

 $concat="[".$outp."]";
  return $concat;



}

function getListenotesrequest($classeid,$codeEtab,$sessionEtab,$matiereid,$statut,$teacherid)
{

 $etabs=new Etab();
 $type=2;

$students=$etabs->getAllstudentofthisclassesSession($classeid,$sessionEtab);

   $outp = "";
   $outp1 = "";
   $outp2 = "";

   foreach ($students as $value):
  if ($outp != "") {$outp .= ",";}

   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and session_ctrl=? and mat_ctrl=? and statut_ctrl=? and enseignant.idcompte_enseignant=?");
   $req->execute([$classeid,$codeEtab,$sessionEtab,$matiereid,$statut,$teacherid]);
   $data=$req->fetchAll();

   $req1 = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and session_notes=? and enseignant.idcompte_enseignant=? ");
   $req1->execute([$type,$classeid,$matiereid,$codeEtab,$sessionEtab,$teacherid]);
   $data1=$req1->fetchAll();

   $nomcomplet=$value->nom_eleve." ".$value->prenom_eleve;
   $studentid=$value->idcompte_eleve;
   $matricule=$value->matricule_eleve;


   foreach ($data as $values):
if ($outp1 != "") {$outp1 .= ",";}

   $evaluationid=$values->id_ctrl;
   $libelle=$values->libelle_ctrl;

   $nbnotes="";
   $datastudentsx="";
   $notes="";
   $observations="";

   $nbnotes=$etabs->DetermineNoteNumbercontroles($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);
   $datastudentsx=$etabs->getNotescontroleinformations($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);

   if($nbnotes==0)
    {
	$notes=0;
    }else
   {
     $array=json_encode($datastudentsx,true);
     $someArray = json_decode($array, true);
	$notes=$someArray[0]["valeur_notes"];
       $observations=$someArray[0]["obser_notes"];
   }



   $outp1 .= '{"controleid":"'.$evaluationid.'",';
   $outp1 .= '"libelle":"'.$libelle.'",';
   $outp1 .= '"note":"'.$notes.'",';
   $outp1 .= '"observation":"'.$observations. '"}';

   endforeach;

   $allcontroles="[".$outp1."]" ;


   foreach ($data1 as $valuex):
  if ($outp2 != "") {$outp2 .= ",";}

   $evaluationid=$valuex->id_exam;
   $libelle=$valuex->libelle_exam;

   $nbnotes="";
   $datastudentsx="";
   $notes="";
   $observations="";

  $nbnotes=$etabs->DetermineNoteNumberexamens($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);
  $datastudentsx=$etabs->getNotesexameninformations($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);

  if($nbnotes==0)
    {
	$notes=0;
    }else
   {
     $array=json_encode($datastudentsx,true);
     $someArray = json_decode($array, true);
	$notes=$someArray[0]["valeur_notes"];
       $observations=$someArray[0]["obser_notes"];
   }

   $outp2 .= '{"examenid":"'.$evaluationid.'",';
   $outp2 .= '"libelle":"'.$libelle.'",';
   $outp2 .= '"note":"'.$notes.'",';
   $outp2 .= '"observation":"'.$observations. '"}';


   endforeach;

   $allexamens="[".$outp2."]" ;



   $outp .= '{"studentid":"'.$studentid.'",';
   $outp .= '"matricule":"'.$matricule.'",';
   $outp .= '"nom":"'.$nomcomplet.'",';
   $outp .= '"controles":'.$allcontroles.',';
   $outp .= '"examens":'.$allexamens.',';
   $outp .= '"codeEtab":"'.$codeEtab. '"}';


   endforeach;

  $concat="[".$outp."]";
  return $concat;


}

function getAllnotesEvaluations($classeid,$codeEtab,$sessionEtab,$matiereid,$statut,$teacherid,$evaluationid,$typenote)
{
   $etabs=new Etab();

   $students=$etabs->getAllstudentofthisclassesSession($classeid,$sessionEtab);

   $outp = "";
   $outp1 = "";
   $outp2 = "";
   if ($outp != "") {$outp .= ",";}
   foreach ($students as $value):

   $nbnotes="";
   $datastudentsx="";
   $notes="";
   $observations="";
   $nomcomplet=$value->nom_eleve." ".$value->prenom_eleve;
   $studentid=$value->idcompte_eleve;

   if($typenote==1)
    {
	$nbnotes=$etabs->DetermineNoteNumbercontroles($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);
        $datastudentsx=$etabs->getNotescontroleinformations($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);
    }else if($typenote==2)
    {
        $nbnotes=$etabs->DetermineNoteNumberexamens($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);
        $datastudentsx=$etabs->getNotesexameninformations($studentid,$classeid,$matiereid,$evaluationid,$codeEtab);

    }

   if($nbnotes==0)
    {
	$notes=0;
    }else
   {
     $array=json_encode($datastudentsx,true);
     $someArray = json_decode($array, true);
	$notes=$someArray[0]["valeur_notes"];
       $observations=$someArray[0]["obser_notes"];
   }


   $outp .= '{"studentid":"'.$studentid.'",';
   $outp .= '"matricule":"'.$value->matricule_eleve.'",';
   $outp .= '"nom":"'.$nomcomplet.'",';
   $outp .= '"note":"'.$notes.'",';
   $outp .= '"observation":"'.$observations. '"}';


   endforeach;

echo "[".$outp."]" ;

}

function getAllControlesOfThisClassesSchoolTeatcher($classe,$code,$session,$matiere,$statut,$teatcher)
{
$statut=1;
   $req = $this->db->prepare("SELECT * FROM controle,classe,enseignant,matiere where controle.mat_ctrl=matiere.id_mat and  controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=enseignant.idcompte_enseignant and  classe_ctrl=? and codeEtab_ctrl=? and session_ctrl=? and mat_ctrl=? and statut_ctrl=? and enseignant.idcompte_enseignant=?");
   $req->execute([$classe,$code,$session,$matiere,$statut,$teatcher]);
   $data=$req->fetchAll();
    return json_encode($data);

}

function getAllExamenOfSchoolClassesTeatcher($classe,$code,$session,$matiere,$type,$teatcherid)
{
  $req = $this->db->prepare("SELECT distinct matiere.id_mat,enseignant.idcompte_enseignant,examen.id_exam,examen.libelle_exam FROM matiere,enseignant,examen,notes where notes.idmat_notes=matiere.id_mat and notes.idprof_notes=enseignant.idcompte_enseignant and  notes.idtype_notes=examen.id_exam and notes.type_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.codeEtab_notes=? and session_notes=? and enseignant.idcompte_enseignant=? ");
  $req->execute([$type,$classe,$matiere,$code,$session,$teatcherid]);
  $data=$req->fetchAll();
    return json_encode($data);

}

function getAllClassesbyschoolCodeAndsession($code,$session)
  {
        $req = $this->db->prepare("SELECT * FROM classe,etablissement WHERE classe.codeEtab_classe=etablissement.code_etab and etablissement.code_etab=? and classe.session_classe=? ");
        $req->execute([$code,$session]);
          $data=$req->fetchAll();
    return json_encode($data);

  }

/* tdllezie@ l'ancienne requete SELECT distinct classe.id_classe,classe.libelle_classe,classe.codeEtab_classe,classe.session_classe FROM classe,dispenser,compte WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=? AND classe.session_classe IN(SELECT DISTINCT sessions.libelle_sess FROM sessions,semestre,etablissement WHERE sessions.id_sess=semestre.idsess_semes AND sessions.encours_sess=1) order by classe.libelle_classe ASC*/


function getClassesOfTeatcherId($idcompte)
{

  $etabs=new Etab();
  $req = $this->db->prepare(" SELECT distinct classe.id_classe,classe.libelle_classe,classe.codeEtab_classe,classe.session_classe,etablissement.libelle_etab FROM classe,dispenser,compte,etablissement WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=? AND classe.session_classe IN(SELECT DISTINCT sessions.libelle_sess FROM sessions,semestre WHERE sessions.id_sess=semestre.idsess_semes AND sessions.encours_sess=1) order by classe.libelle_classe ASC");
//$req = $this->db->prepare("SELECT distinct classe.id_classe,classe.libelle_classe,classe.codeEtab_classe,classe.session_classe FROM classe,dispenser,compte WHERE compte.id_compte=dispenser.id_enseignant and dispenser.idclasse_disp=classe.id_classe and compte.id_compte=? AND classe.session_classe IN(SELECT DISTINCT sessions.libelle_sess FROM sessions,semestre,etablissement WHERE sessions.id_sess=semestre.idsess_semes AND sessions.encours_sess=1) order by classe.libelle_classe ASC");
$req->execute([$idcompte]);
  $data=$req->fetchAll();
  $outp = "";

  foreach ($data as $value):
   if ($outp != "") {$outp .= ",";}

    $classeid=$value->id_classe;
    $libelleclasse=$value->libelle_classe;
    $codeEtab=$value->codeEtab_classe;
    $sessionEtab=$value->session_classe;
    $nbinscrit=$etabs-> DetermineNumberOfStudentInThisClasse($classeid,$codeEtab,$sessionEtab);
    $libelleEtab=$value->libelle_etab;

    $outp .= '{"classeid":"'.$classeid. '",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';

    $outp .= '"libelleEtab":"'.$libelleEtab.'",';

    $outp .= '"session":"'.$sessionEtab.'",';
    $outp .= '"nbinscrit":"'.$nbinscrit. '"}';

  endforeach;

echo "[".$outp."]" ;


  //return json_encode($data);
}

function getAllStudentOfClassesId($classe,$session)

  {
$this->db->query('SET SQL_BIG_SELECTS=1');


    //  $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where compte.email_compte=eleve.email_eleve and classe.id_classe=inscription.idclasse_inscrip and compte.id_compte=inscription.ideleve_inscrip and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte!=0 ");

       // $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 ");

 $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and parent.idcompte_parent=parenter.parentid_parenter and parenter.eleveid_parenter=eleve.idcompte_eleve  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 order by eleve.nom_eleve ASC,eleve.prenom_eleve ASC ");

        $req->execute([$classe,$session]);

        $data=$req->fetchAll();

        return json_encode($data);

  }

function getAllStudentOfClassesIdDatass($studentid,$classe,$codeEtab,$sessionEtab,$typesession)

  {
$this->db->query('SET SQL_BIG_SELECTS=1');


    //  $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where compte.email_compte=eleve.email_eleve and classe.id_classe=inscription.idclasse_inscrip and compte.id_compte=inscription.ideleve_inscrip and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte!=0 ");

       // $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 ");

 $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and parent.idcompte_parent=parenter.parentid_parenter and parenter.eleveid_parenter=eleve.idcompte_eleve  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 and eleve.idcompte_eleve=? order by eleve.nom_eleve ASC,eleve.prenom_eleve ASC ");

        $req->execute([$classe,$sessionEtab,$studentid]);

        $data=$req->fetchAll();

        $etabs=new Etab();

         $outp = "";
         $outp1 = "";
         $outp2 = "";


     foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_classe;
    $sessionEtab=$value->session_classe;
    $classeid=$value->id_classe;
    $matricule=$value->matricule_eleve;
    $nom=$value->nom_eleve;
    $prenom=$value->prenom_eleve;
    $libelleclasse=$value->libelle_classe;

    //nous allons chercher la liste des notes de controles du trimestre
    $controles=$etabs->getNotescontrolesOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession);
    $examens=$etabs->getNotesexamensOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession);
     foreach ($controles as $values):
    if ($outp1 != "") {$outp1 .= ",";}

   $controleid=$values->id_ctrl;
   $libellecontrole=$values->libelle_ctrl;
   $date=$values->date_ctrl;
   $libellematiere=$values->libelle_mat;
   $coefmatiere=$values->coef_mat;
   $coefcontrole=$values->coef_ctrl;
   $notecontrole=$values->valeur_notes;
   $observationcontrole=$values->obser_notes;


    $outp1 .= '{"controleid":"'.$controleid. '",';
    $outp1 .= '"libellecontrole":"'.$libellecontrole.'",';
    $outp1 .= '"datecontrole":"'.$date.'",';
    $outp1 .= '"libellematiere":"'.$libellematiere.'",';
    $outp1 .= '"coefmatiere":"'.$coefmatiere.'",';
    $outp1 .= '"coefcontrole":"'.$coefcontrole.'",';
    $outp1 .= '"notecontrole":"'.$notecontrole.'",';
    $outp1 .= '"observationcontrole":"'.$observationcontrole.'",';
    $outp1 .= '"sessionEtab":"'.$sessionEtab. '"}';


     endforeach;

    foreach ($examens as $valuex):
    if ($outp2 != "") {$outp2 .= ",";}

   $controleid=$values->id_exam;
   $libellecontrole=$valuex->libelle_exam;
   //$date=$valuex->date_ctrl;
   $libellematiere=$valuex->libelle_mat;
   $coefmatiere=$valuex->coef_mat;
   //$coefcontrole=$valuex->coef_ctrl;
   $notecontrole=$valuex->valeur_notes;
   $observationcontrole=$valuex->obser_notes;


    $outp2 .= '{"controleid":"'.$controleid. '",';
    $outp2 .= '"libellecontrole":"'.$libellecontrole.'",';
    //$outp1 .= '"datecontrole":"'.$date.'",';
    $outp2 .= '"libellematiere":"'.$libellematiere.'",';
    $outp2 .= '"coefmatiere":"'.$coefmatiere.'",';
    //$outp1 .= '"coefcontrole":"'.$coefcontrole.'",';
    $outp2 .= '"notecontrole":"'.$notecontrole.'",';
    $outp2 .= '"observationcontrole":"'.$observationcontrole.'",';
    $outp2 .= '"sessionEtab":"'.$sessionEtab. '"}';


     endforeach;


  $allcontroles="[".$outp1."]" ;

  $allexamens="[".$outp2."]" ;

   //nous allons chercher la liste des notes de examens du trimestre


    $outp .= '{"eleveid":"'.$eleveid. '",';
    $outp .= '"matricule":"'.$matricule.'",';
    //$outp .= '"datecontrole":"'.$date.'",';
    $outp .= '"nom":"'.$nom.'",';
    $outp .= '"prenom":"'.$prenom.'",';
    $outp .= '"controles":'.$allcontroles.',';
    $outp .= '"examens":'.$allexamens.',';
    $outp .= '"libelleclasse":"'.$libelleclasse. '"}';



    endforeach;



  return "[".$outp."]" ;




  }


function getAllStudentOfClassesIdDatas($classe,$codeEtab,$sessionEtab,$typesession)

  {
$this->db->query('SET SQL_BIG_SELECTS=1');


    //  $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription where compte.email_compte=eleve.email_eleve and classe.id_classe=inscription.idclasse_inscrip and compte.id_compte=inscription.ideleve_inscrip and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte!=0 ");

       // $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 ");

 $req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and parent.idcompte_parent=parenter.parentid_parenter and parenter.eleveid_parenter=eleve.idcompte_eleve  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 order by eleve.nom_eleve ASC,eleve.prenom_eleve ASC ");

        $req->execute([$classe,$sessionEtab]);

        $data=$req->fetchAll();

        $etabs=new Etab();

         $outp = "";
         $outp1 = "";
         $outp2 = "";


     foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_classe;
    $sessionEtab=$value->session_classe;
    $classeid=$value->id_classe;
    $matricule=$value->matricule_eleve;
    $nom=$value->nom_eleve;
    $prenom=$value->prenom_eleve;
    $libelleclasse=$value->libelle_classe;

    //nous allons chercher la liste des notes de controles du trimestre
    $controles=$etabs->getNotescontrolesOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession);
    $examens=$etabs->getNotesexamensOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession);
     foreach ($controles as $values):
    if ($outp1 != "") {$outp1 .= ",";}

   $controleid=$values->id_ctrl;
   $libellecontrole=$values->libelle_ctrl;
   $date=$values->date_ctrl;
   $libellematiere=$values->libelle_mat;
   $coefmatiere=$values->coef_mat;
   $coefcontrole=$values->coef_ctrl;
   $notecontrole=$values->valeur_notes;
   $observationcontrole=$values->obser_notes;


    $outp1 .= '{"controleid":"'.$controleid. '",';
    $outp1 .= '"libellecontrole":"'.$libellecontrole.'",';
    $outp1 .= '"datecontrole":"'.$date.'",';
    $outp1 .= '"libellematiere":"'.$libellematiere.'",';
    $outp1 .= '"coefmatiere":"'.$coefmatiere.'",';
    $outp1 .= '"coefcontrole":"'.$coefcontrole.'",';
    $outp1 .= '"notecontrole":"'.$notecontrole.'",';
    $outp1 .= '"observationcontrole":"'.$observationcontrole.'",';
    $outp1 .= '"sessionEtab":"'.$sessionEtab. '"}';


     endforeach;

    foreach ($examens as $valuex):
    if ($outp2 != "") {$outp2 .= ",";}

   $controleid=$values->id_exam;
   $libellecontrole=$valuex->libelle_exam;
   //$date=$valuex->date_ctrl;
   $libellematiere=$valuex->libelle_mat;
   $coefmatiere=$valuex->coef_mat;
   //$coefcontrole=$valuex->coef_ctrl;
   $notecontrole=$valuex->valeur_notes;
   $observationcontrole=$valuex->obser_notes;


    $outp2 .= '{"controleid":"'.$controleid. '",';
    $outp2 .= '"libellecontrole":"'.$libellecontrole.'",';
    //$outp1 .= '"datecontrole":"'.$date.'",';
    $outp2 .= '"libellematiere":"'.$libellematiere.'",';
    $outp2 .= '"coefmatiere":"'.$coefmatiere.'",';
    //$outp1 .= '"coefcontrole":"'.$coefcontrole.'",';
    $outp2 .= '"notecontrole":"'.$notecontrole.'",';
    $outp2 .= '"observationcontrole":"'.$observationcontrole.'",';
    $outp2 .= '"sessionEtab":"'.$sessionEtab. '"}';


     endforeach;


  $allcontroles="[".$outp1."]" ;

  $allexamens="[".$outp2."]" ;

   //nous allons chercher la liste des notes de examens du trimestre


    $outp .= '{"eleveid":"'.$eleveid. '",';
    $outp .= '"matricule":"'.$matricule.'",';
    //$outp .= '"datecontrole":"'.$date.'",';
    $outp .= '"nom":"'.$nom.'",';
    $outp .= '"prenom":"'.$prenom.'",';
    $outp .= '"controles":'.$allcontroles.',';
    $outp .= '"examens":'.$allexamens.',';
    $outp .= '"libelleclasse":"'.$libelleclasse. '"}';



    endforeach;



  return "[".$outp."]" ;




  }

function getNotescontrolesOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession)
{
$this->db->query('SET SQL_BIG_SELECTS=1');
  $req = $this->db->prepare("SELECT * FROM evaluations,controle,notes,matiere where evaluations.idtype_evaltions=notes.idtype_notes and controle.id_ctrl=evaluations.idtype_evaltions and notes.idmat_notes=matiere.id_mat and notes.type_notes=1 and evaluations.type_evaltions='CONTROLES' and notes.ideleve_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and evaluations.periode_evaltions=?");
  $req->execute([$eleveid,$classeid,$codeEtab,$sessionEtab,$typesession]);
  $data=$req->fetchAll();
  return $data;

}

function getNotesexamensOfStudentByperiode($eleveid,$classeid,$codeEtab,$sessionEtab,$typesession)
{
$this->db->query('SET SQL_BIG_SELECTS=1');

  $req = $this->db->prepare("SELECT * FROM evaluations,examen,notes,matiere where evaluations.idtype_evaltions=notes.idtype_notes and examen.id_exam=evaluations.idtype_evaltions and notes.idmat_notes=matiere.id_mat and notes.type_notes=2 and evaluations.type_evaltions='EXAMENS' and notes.ideleve_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and evaluations.periode_evaltions=?");
  $req->execute([$eleveid,$classeid,$codeEtab,$sessionEtab,$typesession]);
  $data=$req->fetchAll();
  return $data;

}


function getAllprogrammesOfThisTeatcher($IdCompte,$libellesessionencours)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,classe,enseignant,enseigner,matiere where  syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=enseigner.id_enseignant and syllabus.idclasse_syllab=classe.id_classe  and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idteatcher_syllab=? and syllabus.session_syllab=?");
        $req->execute([$IdCompte,$libellesessionencours]);
        $data=$req->fetchAll();

        return json_encode($data);

      }

function getAllprogrammesOfThisTeatcherClasse($IdCompte,$libellesessionencours,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,classe,enseignant,enseigner,matiere where  syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=enseigner.id_enseignant and syllabus.idclasse_syllab=classe.id_classe  and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idteatcher_syllab=? and syllabus.session_syllab=? and idclasse_syllab=?");
        $req->execute([$IdCompte,$libellesessionencours,$classeid]);
        $data=$req->fetchAll();

        return json_encode($data);

      }

function getAllprogrammesOfThisStudentClasse($codeEtab,$libellesessionencours,$classeid)
      {
        //$req = $this->db->prepare("SELECT * FROM syllabus,classe,enseignant,enseigner,matiere where  syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and enseignant.idcompte_enseignant=enseigner.id_enseignant and syllabus.idclasse_syllab=classe.id_classe  and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.codeEtab_syllab=? and syllabus.session_syllab=? and idclasse_syllab=?");
$req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,compte,enseigner,enseignant WHERE syllabus.idteatcher_syllab=compte.id_compte and syllabus.idclasse_syllab=classe.id_classe AND syllabus.idmatiere_syllab=matiere.id_mat AND compte.id_compte=enseigner.id_enseignant AND enseignant.idcompte_enseignant=compte.id_compte AND syllabus.idclasse_syllab=? AND classe.codeEtab_classe=? AND classe.session_classe=?");

        $req->execute([$classeid,$codeEtab,$libellesessionencours]);
        $data=$req->fetchAll();

        return json_encode($data);

      }



function DetermineNumberOfStudentInThisClasse($id_classe,$codeEtab,$libellesessionencours)
{
  $req = $this->db->prepare("SELECT * FROM inscription,eleve,compte where  inscription.ideleve_inscrip=compte.id_compte and eleve.idcompte_eleve=compte.id_compte and  	idclasse_inscrip=? and codeEtab_inscrip=? and session_inscrip=?	");
  $req->execute([$id_classe,$codeEtab,$libellesessionencours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function countAttendanceclasseofDay($teacherid,$matiereid,$classeid,$codeEtab,$sessionEtab,$datechoice,$idheure)
{
  $req = $this->db->prepare("SELECT * FROM presences where teatcher_presence=? and matiere_presence=? and classe_presence=? and codeEtab_presence=? and session_presence=? and date_presence=? and libelleheure_presence=?");
   $req->execute([$teacherid,$matiereid,$classeid,$codeEtab,$sessionEtab,$datechoice,$idheure]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;

}

function getlasnotestudentNew($idcompte,$listdesignation,$notetype,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    //return json_encode($data);

  $etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";

   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$value->idmat_notes,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;
    $controleLib=$value->libelle_ctrl;

    $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $outp .= '"libellecontrole":"'.$controleLib.'",';
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    //$outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;


}

function getlasnotestudentNewP($idcompte,$listdesignation,$notetype,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    //return json_encode($data);

  $etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";

   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}

    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$value->idmat_notes,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

     var_dump($tablenotes);

   endforeach;

   //echo "[".$outp."]" ;


}



//nouvelles fonctions

function DetermineTypeEtab($codeEtabAssigner)
      {
        $req = $this->db->prepare("SELECT * FROM etablissement WHERE 	etablissement.code_etab=?");
        $req->execute([$codeEtabAssigner]);
        $data=$req->fetchAll();
        $array=json_encode($data,true);
        $someArray =json_decode($array, true);
        $donnees=$someArray[0]["type_etab"];
        return $donnees;
      }

function getSyllabusRequisInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        //return  json_encode($data);

          $outp = "";

          foreach ($data as $value):
if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabreq . '",';
            $outp .= '"libelle_requis":"'   . $value->libelle_syllabreq  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;



      }

function getSyllabusRequisInfosClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        //return  json_encode($data);

          $outp = "";

          foreach ($data as $value):
	if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabreq . '",';
            $outp .= '"libelle_requis":"'   . $value->libelle_syllabreq  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;



      }


function getSyllabusRequisInfosOne($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);





      }

function getSyllabusRequisInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabrequis where syllabrequis.idsyllab_syllabreq=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        return  json_encode($data);





      }



function getSyllabusDocInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
          if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabdoc . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;



      }

function getSyllabusDocInfosClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
          if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabdoc . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;



      }


function getSyllabusDocInfosOne($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);



      }

function getSyllabusDocInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=0  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        return  json_encode($data);



      }



function getSyllabusDocfacInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabdoc . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;


      }

function getSyllabusDocfacInfosClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabdoc . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;


      }


function getSyllabusDocfacInfosOne($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);


      }

function getSyllabusDocfacInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabdoc where syllabdoc.idsyllab_syllabdoc=syllabus.id_syllab and syllabdoc.facultatif_syllabdoc=1  and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        return  json_encode($data);


      }



function getSyllabusCalendarInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher]);
          $data=$req->fetchAll();
          //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
	    if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabcal . '",';
            $outp .= '"date":"'  . date_format(date_create($value->date_syllabcal),"d/m/Y") . '",';
            $outp .= '"seance":"'  . $value->seance_syllabcal . '",';
            $outp .= '"contenu":"'  . $value->contenu_syllabcal . '",';
            $outp .= '"prealable":"'  . $value->prealable_syllabcal . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }


function getSyllabusCalendarInfosClasse($programme,$teatcher,$codeEtab,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher,$classeid]);
          $data=$req->fetchAll();
          //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
	    if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabcal . '",';
            $outp .= '"date":"'  . date_format(date_create($value->date_syllabcal),"d/m/Y") . '",';
            $outp .= '"seance":"'  . $value->seance_syllabcal . '",';
            $outp .= '"contenu":"'  . $value->contenu_syllabcal . '",';
            $outp .= '"prealable":"'  . $value->prealable_syllabcal . '",';
            $outp .= '"libelle_doc":"'   . $value->libelle_syllabdoc . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }



function getSyllabusCalendarInfosOne($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher]);
          $data=$req->fetchAll();
          return  json_encode($data);


      }

function getSyllabusCalendarInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabcalendar where syllabcalendar.idsyllab_syllabcal=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? order by date_syllabcal Asc ");
          $req->execute([$programme,$teatcher,$classeid]);
          $data=$req->fetchAll();
          return  json_encode($data);


      }



function getSyllabusModeEvalInfos($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabeval . '",';
            $outp .= '"date":"'  . $value->date_syllabeval . '",';
            $outp .= '"type":"'  . $value->type_syllabeval . '",';
            $outp .= '"competence":"'  . $value->competence_syllabeval . '",';
            $outp .= '"ponderation":"'  . $value->ponderation_syllabeval . '"}';


          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }

function getSyllabusModeEvalInfosClasse($programme,$teatcher,$codeEtab,$claseid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher,$claseid]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabeval . '",';
            $outp .= '"date":"'  . $value->date_syllabeval . '",';
            $outp .= '"type":"'  . $value->type_syllabeval . '",';
            $outp .= '"competence":"'  . $value->competence_syllabeval . '",';
            $outp .= '"ponderation":"'  . $value->ponderation_syllabeval . '"}';


          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }


function getSyllabusModeEvalInfosOne($programme,$teatcher,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);

      }

function getSyllabusModeEvalInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabeval where syllabeval.idsyllab_syllabeval=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? order by date_syllabeval ASC");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        return  json_encode($data);

      }



	function getAllParascolairesOfThisSchool($codeEtabLocal,$libellesessionencours)
      {
        /*$req = $this->db->prepare("SELECT * FROM parascolaires where codeEtab_para=? and  session_para=? and  statut_para not in (0,3)");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();*/

        $req = $this->db->prepare("SELECT * FROM activites where codeEtab_act=? and session_act=?  and  statut_act not in (0,3)");
        $req->execute([$codeEtabLocal,$libellesessionencours]);
        return $req->fetchAll();


      }

      function getAllParascolairesOfThisSchoolclasses($id_para,$classeid,$session,$codeEtab)
      {
        /*$req = $this->db->prepare("SELECT * FROM parascolaires where id_para=? and session_para=? and codeEtab_para=? and statut_para=2");
        $req->execute([$id_para,$session,$codeEtab]);
        return $req->fetchAll();*/

	$req = $this->db->prepare("SELECT * FROM activites,etablissement,pays where activites.codeEtab_act=etablissement.code_etab and pays.id_pays=etablissement.pays_etab and activites.id_act=? and activites.session_act=? and activites.codeEtab_act=? and  activites.statut_act not in (0,3)");
        $req->execute([$id_para,$session,$codeEtab]);
        return $req->fetchAll();

      }

function getInfosofclassesbyId($classe,$session)
  {
    $req = $this->db->prepare("SELECT * FROM classe where classe.id_classe=? and classe.session_classe=?");
    $req->execute([$classe,$session]);
    $data=$req->fetchAll();
    $array=json_encode($data,true);
    $someArray = json_decode($array, true);
    $donnees=$someArray[0]["libelle_classe"];
    return $donnees;
  }

 function getParascolairesclasses($codeEtab,$session,$classe)
{

    $etabs=new Etab();
    $outp = "";

   $parascos=$etabs->getAllParascolairesOfThisSchool($codeEtab,$session);

   foreach ($parascos as $valueparas):
      //$classeselected=substr($valueparas->classes_para,0,-1);
      $classeselected=substr($valueparas->classes_act,0,-1);
      $tabclasseselected=explode("-",$classeselected);

     if (in_array($classe, $tabclasseselected)) {
	//if ($outp != "") {$outp .= ",";}
	  //$dataparas=$etabs->getAllParascolairesOfThisSchoolclasses($valueparas->id_para,$classe,$valueparas->session_para,$valueparas->codeEtab_para);
          $dataparas=$etabs->getAllParascolairesOfThisSchoolclasses($valueparas->id_act,$classe,$valueparas->session_act,$valueparas->codeEtab_act);
          foreach ($dataparas as $value):
            if ($outp != "") {$outp .= ",";}
	    $classepara=$etabs->getInfosofclassesbyId($classe,$session);
            //$outp .= '{"Libelle":"'  . $value->libelle_para . '",';
            $outp .= '{"Libelle":"'  . $value->libelle_act . '",';
            $outp .= '"parascoid":"'  . $value->id_act . '",';
            //$outp .= '"date_deb":"'  . date_format(date_create($value->debut_para),"d/m/Y") . '",';
            //$outp .= '"date_fin":"'  . date_format(date_create($value->fin_para),"d/m/Y") . '",';
            $outp .= '"date_deb":"'  . date_format(date_create($value->datedeb_act),"d/m/Y") . '",';
            $outp .= '"date_fin":"'  . date_format(date_create($value->datefin_act),"d/m/Y") . '",';

            //$outp .= '"classe":"'  . $classepara . '",';
            //$outp .= '"payant":"'  . $value->payant_para. '",';
            //$outp .= '"montant":"'   . $value->montant_para  . '"}';

            $outp .= '"classe":"'  . $classepara . '",';
            $outp .= '"payant":"'  . $value->payant_act. '",';
            $outp .= '"devise":"'  . $value->devises_pays. '",';
            $outp .= '"montant":"'   . $value->montant_act  . '"}';


	  endforeach;

      }


   endforeach;

 $retour="[".$outp."]" ;
   return $retour;


}

/*
function getParascolairesclasses($codeEtab,$session,$classe)
{
    $etabs=new Etab();
    $outp = "";


    $parascos=$etabs->getAllParascolairesOfThisSchool($codeEtab,$session);

    foreach ($parascos as $valueparas):
      $classeselected=substr($valueparas->classes_para,0,-1);
      $tabclasseselected=explode("-",$classeselected);

      if (in_array($classe, $tabclasseselected)) {
	//if ($outp != "") {$outp .= ",";}
	  //$dataparas=$etabs->getAllParascolairesOfThisSchoolclasses($valueparas->id_para,$classe,$valueparas->session_para,$valueparas->codeEtab_para);
          $dataparas=$etabs->getAllParascolairesOfThisSchoolclasses($valueparas->id_act,$classe,$valueparas->session_act,$valueparas->codeEtab_act);
          foreach ($dataparas as $value):
            if ($outp != "") {$outp .= ",";}
	    $classepara=$etabs->getInfosofclassesbyId($classe,$session);
            //$outp .= '{"Libelle":"'  . $value->libelle_para . '",';
            $outp .= '{"Libelle":"'  . $value->libelle_act . '",';
            //$outp .= '"date_deb":"'  . date_format(date_create($value->debut_para),"d/m/Y") . '",';
            //$outp .= '"date_fin":"'  . date_format(date_create($value->fin_para),"d/m/Y") . '",';
            $outp .= '"date_deb":"'  . date_format(date_create($value->datedeb_act),"d/m/Y") . '",';
            $outp .= '"date_fin":"'  . date_format(date_create($value->datefin_act),"d/m/Y") . '",';

            //$outp .= '"classe":"'  . $classepara . '",';
            //$outp .= '"payant":"'  . $value->payant_para. '",';
            //$outp .= '"montant":"'   . $value->montant_para  . '"}';

            $outp .= '"classe":"'  . $classepara . '",';
            $outp .= '"payant":"'  . $value->payant_act. '",';
            $outp .= '"montant":"'   . $value->montant_act  . '"}';


	  endforeach;

      }

    endforeach;

   $retour="[".$outp."]" ;
   return $retour;



}

*/

function getSyllabusRegleInfos($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabregle . '",';
            $outp .= '"libelle_regle":"'   . $value->libelle_syllabregle  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }

function getSyllabusRegleInfosClasse($programme,$teatcher,$codeEtab,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        //return  json_encode($data);
	$outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabregle . '",';
            $outp .= '"libelle_regle":"'   . $value->libelle_syllabregle  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

      }


function getSyllabusRegleInfosOne($programme,$teatcher,$codeEtab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
        $req->execute([$programme,$teatcher]);
        $data=$req->fetchAll();
        return  json_encode($data);

      }

function getSyllabusRegleInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus, syllabregle  where syllabregle.idsyllab_syllabregle=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
        $req->execute([$programme,$teatcher,$classeid]);
        $data=$req->fetchAll();
        return  json_encode($data);

      }



function getAllSubjectOfClasses($classeId,$codeEtabLocal,$session)
{
  $req = $this->db->prepare("SELECT * FROM matiere,classe,compte where compte.id_compte=matiere.teatcher_mat and   matiere.classe_mat=classe.id_classe and  matiere.classe_mat=? and matiere.codeEtab_mat=? and matiere.session_mat=?");
   $req->execute([$classeId,$codeEtabLocal,$session]);
   $data=$req->fetchAll();
   return  json_encode($data);
}

function getSyllabusThemesInfos($programme,$teatcher,$codeEtab)
{
      $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
      $req->execute([$programme,$teatcher]);
      $data=$req->fetchAll();
      //return  json_encode($data);
      $outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabth. '",';
            $outp .= '"libelle_theme":"'   . $value->libelle_syllabth  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

}

function getSyllabusThemesInfosClasse($programme,$teatcher,$codeEtab,$classeid)
{
      $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
      $req->execute([$programme,$teatcher,$classeid]);
      $data=$req->fetchAll();
      //return  json_encode($data);
      $outp = "";
          //if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabth. '",';
            $outp .= '"libelle_theme":"'   . $value->libelle_syllabth  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;

}


function getSyllabusThemesInfosOne($programme,$teatcher,$codeEtab)
{
      $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? ");
      $req->execute([$programme,$teatcher]);
      $data=$req->fetchAll();
      return  json_encode($data);

}

function getSyllabusThemesInfosOneClasse($programme,$teatcher,$codeEtab,$classeid)
{
      $req = $this->db->prepare("SELECT * FROM syllabus,syllabtheme where syllabtheme.idsyllab_syllabth=syllabus.id_syllab and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and syllabus.idclasse_syllab=? ");
      $req->execute([$programme,$teatcher,$classeid]);
      $data=$req->fetchAll();
      return  json_encode($data);

}



function getAllInformationsOfStudent($compte,$session)
{
  $req = $this->db->prepare("SELECT * from compte,eleve,parenter,classe,inscription where compte.id_compte=eleve.idcompte_eleve and eleve.idcompte_eleve=parenter.eleveid_parenter and inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and inscription.session_inscrip=? and parenter.eleveid_parenter=?");
    $req->execute([$session,$compte]);
    $data=$req->fetchAll();
    return json_encode($data);


}

	function ParentInfostudent($studentparentid)
{
    $req = $this->db->prepare("SELECT  * FROM compte,parent,parenter where compte.id_compte=parent.idcompte_parent and parenter.parentid_parenter=parent.idcompte_parent and parenter.eleveid_parenter=?");
    $req->execute([$studentparentid]);
    $data=$req->fetchAll();
    return json_encode($data);
}

	function DetermineScolarityStateOfStudent($codeEtab,$libellesessionencours,$idcompte)
{
  $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? order by versement.id_versement DESC");
  $req->execute([$codeEtab,$libellesessionencours,$idcompte]);
  $data=$req->fetchAll();
  //return json_encode($data);
    $outp = "";

    foreach ($data as  $value):
     if ($outp != "") {$outp .= ",";}
     $outp .= '{"id_versement":"'.$value->id_versement.'",';
     $outp .= '"code_versement":"'.$value->code_versement.'",';
     $outp .= '"date_versement":"'.date_format(date_create($value->date_versement),"d/m/Y").'",';
     $outp .= '"mode_versement":"'.$value->mode_versement.'",';
     $outp .= '"montant_versement":"'.$value->montant_versement.'",';
     $outp .= '"solde_versement":"'.$value->solde_versement.'",';
     $outp .= '"classe_versement":"'.$value->classe_versement.'",';
     $outp .= '"ideleve_versement":"'.$value->ideleve_versement.'",';
     $outp .= '"session_versement":"'.$value->session_versement.'",';
     $outp .= '"codeEtab_versement":"'.$value->codeEtab_versement.'",';
     $outp .= '"devise_versement":"'.$value->devise_versement.'",';
     $outp .= '"user_versement":"'.$value->user_versement. '"}';


     endforeach;

    echo "[".$outp."]" ;



}

function DetermineScolarityStateOfStudentLast($codeEtab,$libellesessionencours,$idcompte)
{
  $req = $this->db->prepare("SELECT * FROM versement where codeEtab_versement=? and session_versement=? and ideleve_versement=? order by versement.id_versement DESC limit 10");
  $req->execute([$codeEtab,$libellesessionencours,$idcompte]);
  $data=$req->fetchAll();
  //return json_encode($data);
    $outp = "";

    foreach ($data as  $value):
     if ($outp != "") {$outp .= ",";}
     $outp .= '{"id_versement":"'.$value->id_versement.'",';
     $outp .= '"date_versement":"'.date_format(date_create($value->date_versement),"d/m/Y").'",';
     $outp .= '"montant_versement":"'.$value->montant_versement.'",';
     $outp .= '"codeEtab_versement":"'.$value->codeEtab_versement.'",';
     $outp .= '"devise_versement":"'.$value->devise_versement.'",';
     $outp .= '"user_versement":"'.$value->user_versement. '"}';


     endforeach;

    echo "[".$outp."]" ;



}


	function getControleNotesWithoutLibctrlOther($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);




}

function getControleNotesWithoutLibctrlOtherNew($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    //return json_encode($data);

   $etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}
    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $controleLib=$value->libelle_ctrl;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;

    $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $outp .= '"libellecontrole":"'.$controleLib.'",';
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;

}


function getControleNotesWithoutLibctrlOtherLast($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getControleNotesWithoutLibctrlOtherLastNew($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? order by notes.id_notes desc limit 10 ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession]);
    $data=$req->fetchAll();

    //return json_encode($data);

   $etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}
    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;
    $controleLib=$value->libelle_ctrl;


    $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $outp .= '"libellecontrole":"'.$controleLib.'",';
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;


}


function getControleNotesWithoutLibctrlOtherLastOne($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes) order by notes.id_notes desc limit 10  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getControleNotesWithoutLibctrlOtherLastOneNew($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes) order by notes.id_notes desc limit 10  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab]);
    $data=$req->fetchAll();

    //return json_encode($data);

$etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}
    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;
    $controleLib=$value->libelle_ctrl;

    $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $outp .= '"libellecontrole":"'.$controleLib.'",';
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;


}


function getControleNotesSemester($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$semester)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=?");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab,$semester]);
    $data=$req->fetchAll();

    return json_encode($data);

}

function getminmaxmoynotescontrole($notetype,$matclasse,$classe,$codeEtab,$libellesession,$semester,$noteid)
{
  $req = $this->db->prepare("SELECT MIN(notes.valeur_notes) as min, MAX(notes.valeur_notes) as max, AVG(notes.valeur_notes) as avg from notes,controle where notes.idtype_notes=controle.id_ctrl and notes.type_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=?");
  $req->execute([$notetype,$noteid,$matclasse,$classe,$codeEtab,$libellesession,$semester]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["min"]."*".$someArray[0]["max"]."*".$someArray[0]["avg"];
  return $donnees;

}

function getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$noteid)
{
  $req = $this->db->prepare("SELECT MIN(notes.valeur_notes) as min, MAX(notes.valeur_notes) as max, AVG(notes.valeur_notes) as avg from notes,controle where notes.idtype_notes=controle.id_ctrl and notes.type_notes=? and notes.idtype_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes)");
  $req->execute([$notetype,$noteid,$matclasse,$classe,$codeEtab,$libellesession,$codeEtab]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["min"]."*".$someArray[0]["max"]."*".$someArray[0]["avg"];
  return $donnees;

}


function getControleNotesSemesterFormat($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession,$semester)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and notes.codeEtab_notes=?  and controle.typesess_ctrl=?");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab,$semester]);
    $data=$req->fetchAll();

    //return json_encode($data);

   $etabs=new Etab();



   $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
   if ($outp != "") {$outp .= ",";}
   $datanotes=$etabs->getminmaxmoynotescontrole($notetype,$matclasse,$classe,$codeEtab,$libellesession,$semester,$value->id_ctrl);
   $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;
    $controleLib=$value->libelle_ctrl;

    $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $outp .= '"libellecontrole":"'.$controleLib.'",';
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    //$outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;


}

function getControleNotesSemesterFormatOn($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and notes.codeEtab_notes=?  and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes)");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab,$codeEtab]);
    $data=$req->fetchAll();

    //return json_encode($data);



   $etabs=new Etab();

    //$datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession);
    //$tablenotes=explode("*",$datanotes);


   $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
    if ($outp != "") {$outp .= ",";}
    $datanotes=$etabs->getminmaxmoynotescontroleOn($notetype,$matclasse,$classe,$codeEtab,$libellesession,$value->id_ctrl);
    $tablenotes=explode("*",$datanotes);

    $eleveid=$value->idcompte_eleve;
    $codeEtab=$value->codeEtab_notes;
    $session=$value->session_notes;
    $notes=$value->valeur_notes;
    $observation=$value->obser_notes;
    $minimum=$tablenotes[0];
    $maximum=$tablenotes[1];
    $moyenne=$tablenotes[2];
    $matiereid=$value->idmat_notes;
    $classeid=$value->idclasse_notes;
    $libelleclasse=$value->libelle_classe;
    $libellematiere=$value->libelle_mat;
    $professeurid=$value->idprof_notes;
    $controleLib=$value->libelle_ctrl;

    $outp .= '{"libellecontrole":"'.$controleLib.'",';

    $outp .= '"idcompte_eleve":"'.$eleveid. '",';
    $outp .= '"codeEtab":"'.$codeEtab.'",';
    $outp .= '"session":"'.$session.'",';
    $controleLib=$value->libelle_ctrl;
    $outp .= '"notes":"'.$notes.'",';
    $outp .= '"observation":"'.$observation.'",';
    $outp .= '"minimum":"'.$minimum.'",';
    $outp .= '"maximum":"'.$maximum.'",';
    //$outp .= '"maximum":"'.$maximum.'",';
    $outp .= '"moyenne":"'.$moyenne.'",';
    $outp .= '"matiereid":"'.$matiereid.'",';
    $outp .= '"classeid":"'.$classeid.'",';
    $outp .= '"libelleclasse":"'.$libelleclasse.'",';
    $outp .= '"libellematiere":"'.$libellematiere.'",';
    $outp .= '"professeurid":"'.$professeurid. '"}';

   endforeach;

   echo "[".$outp."]" ;


}




  function lastattendances($matricule,$classe,$session)
      {
	$statut=0;
	$etabs=new Etab();
        $req = $this->db->prepare("SELECT * FROM presences,matiere WHERE presences.matiere_presence=matiere.id_mat AND  presences.matricule_presence=? AND presences.classe_presence=? AND presences.session_presence=? and presences.statut_presence=? ORDER BY presences.date_presence DESC LIMIT 10 ");
        $req->execute([$matricule,$classe,$session,$statut]);
        $data=$req->fetchAll();

        $outp = "";
        //if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
        if ($outp != "") {$outp .= ",";}
          $nombre=$etabs->getNbAttendanceDay($matricule,$value->date_presence,$classe);
          $statut="";
          if($nombre==0)
          {
            $statut="A";
          }else {
            $number=$etabs->getstatutAttendanceDay($matricule,$value->date_presence,$classe);
            $array=json_encode($number,true);
            $someArray = json_decode($array, true);
            $presence=$someArray[0]["statut_presence"];
            if($presence==0)
            {
              $statut="A";
            }else {
              $statut="P";
            }
          }

          if ($outp != "") {$outp .= ",";}
          $outp .= '{"date":"'.date_format(date_create($value->date_presence), "d/m/Y").'",';
          $outp .= '"matiere":"'.$value->libelle_mat.'",';
          $outp .= '"statut":"'.$statut.'"}';

        endforeach;

        return $outp;


      }


function getControleNotesWithoutLibctrlOtherOne($idcompte,$listdesignation,$notetype,$matclasse,$classe,$codeEtab,$libellesession)
{
  $req = $this->db->prepare("SELECT * from eleve,classe,controle,notes,matiere,inscription where inscription.ideleve_inscrip=eleve.idcompte_eleve and inscription.idclasse_inscrip=classe.id_classe and inscription.session_inscrip=classe.session_classe and controle.session_ctrl=classe.session_classe
    and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and notes.idclasse_notes=classe.id_classe and notes.idtype_notes=controle.id_ctrl and notes.idmat_notes=matiere.id_mat and notes.ideleve_notes=eleve.idcompte_eleve and notes.session_notes=controle.session_ctrl and notes.ideleve_notes=? and notes.idclasse_notes=?
    and notes.idmat_notes=?  and notes.type_notes=? and notes.codeEtab_notes=? and notes.session_notes=? and controle.typesess_ctrl=(SELECT DISTINCT semestre.id_semes FROM semestre,sessions,notes WHERE notes.codeEtab_notes=semestre.codeEtab_semes and semestre.idsess_semes=sessions.id_sess and semestre.codeEtab_semes=? AND semestre.statut_semes=1 ORDER BY semestre.id_semes)  ");

    $req->execute([$idcompte,$classe,$matclasse,$notetype,$codeEtab,$libellesession,$codeEtab]);
    $data=$req->fetchAll();

    return json_encode($data);



}



	function getstatutAttendanceDay($matricule,$dateday,$classe)
      {
        $req = $this->db->prepare("SELECT * FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

        $req->execute([$matricule,$classe,$dateday]);

        return $req->fetchAll();

      }

function nbreJour($date1, $date2) {

// $date1 = str_replace("/", "-", $date1);
// $date2 = str_replace("/", "-", $date2);
$date1 = strtotime($date1);
$date2 = strtotime($date2);
$nbJoursTimestamp = $date2 - $date1;
$nbJours = $nbJoursTimestamp/86400; // 86 400 = 60*60*24
return intval($nbJours+1);


}

function getminmaxratingmatclasse($classeid,$codeEtab,$session,$matclasse,$semester)
{
  $req = $this->db->prepare("SELECT MIN(rating.rating) AS minimum, MAX(rating.rating) AS maximum  FROM rating WHERE rating.classe_rating=? AND rating.codeEtab_rating=? AND rating.session_rating=? AND rating.matiereid_rating=? AND rating.typsession_rating=?");
  $req->execute([$classeid,$codeEtab,$session,$matclasse,$semester]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["minimum"]."*".$someArray[0]["maximum"];
  return $donnees;

}

function getratingmatstudent($idcompte,$classeid,$codeEtab,$session,$matclasse,$semester)
{
  $req = $this->db->prepare("SELECT * FROM rating WHERE rating.ideleve_rating=? AND rating.classe_rating=? AND rating.codeEtab_rating=? AND rating.session_rating=? AND rating.matiereid_rating=? AND rating.typsession_rating=?");
  $req->execute([$idcompte,$classeid,$codeEtab,$session,$matclasse,$semester]);
  $data=$req->fetchAll();

  $etabs=new Etab();
  $datamoy=$etabs->getminmaxratingmatclasse($classeid,$codeEtab,$session,$matclasse,$semester);
  $tablemoy=explode("*",$datamoy);



  $outp = "";
   //if ($outp != "") {$outp .= ",";}
   foreach ($data as $value):
     if ($outp != "") {$outp .= ",";}
     $moymax=$tablemoy[1];
     $moymin=$tablemoy[0];
     $moyenne=$value->rating;
     $eleveid=$value->ideleve_rating;
     $outp .= '{"idcompte_eleve":"'.$eleveid. '",';
     $outp .= '"moyenne":"'.$moyenne.'",';
     $outp .= '"max":"'.moymax.'",';
     $outp .= '"min":"'.$moymin.'"}';
     endforeach;
  echo "[".$outp."]";

}


function getListeAttendanceLast($matricule,$classe,$codeEtab,$session)
{
         $etabs=new Etab();
	 $req = $this->db->prepare("SELECT * FROM presences,heure where presences.libelleheure_presence=heure.id_heure and  matricule_presence=? and classe_presence=? and codeEtab_presence=? and session_presence=? and statut_presence=0 order by date_presence DESC limit 10 ");
         $req->execute([$matricule,$classe,$codeEtab,$session]);
         $data=$req->fetchAll();

         foreach ($data as $value):

         $heures=$etabs->returnHours($value->heuredeb_heure). " - ".$etabs->returnHours($value->heurefin_heure);
        $matiere=$etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence);
        $statut="A";

          if ($outp != "") {$outp .= ",";}
         // $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
          $outp .= '{"date":"'.date_format(date_create($value->date_presence), "d/m/Y").'",';
          $outp .= '"heure":"'.$heures.'",';
          $outp .= '"matiere":"'.$matiere.'",';
 	  $outp .= '"statut":"'.$statut. '"}';


	 endforeach;

         $concat="[".$outp."]";

        return $concat;

}


function getListeAttendance($matricule,$classe,$codeEtab,$session,$datedeb,$datefin)
{
     $etabs=new Etab();
     $nbjours=$etabs->nbreJour($datedeb,$datefin);

     for($x=0;$x<=$nbjours;$x++)
     {
	$dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));

        $nbabsebnces=$etabs->getAbsencesthisdaynb($dateday,$codeEtab,$session,$classe);

        if($nbabsebnces>0)
        {
	  $datas=$etabs->getlisteabsencesday($dateday,$codeEtab,$session,$classe);

          foreach ($datas as $value):

	$heures=$etabs->returnHours($value->heuredeb_heure). " ".$etabs->returnHours($value->heurefin_heure);
        $matiere=$etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence);
        $statut="A";

          if ($outp != "") {$outp .= ",";}
          $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
          $outp .= '"heure":"'.$heures.'",';
          $outp .= '"matiere":"'.$matiere.'",';
 	  $outp .= '"statut":"'.$statut. '"}';

          endforeach;

        $concat="[".$outp."]";

        return $concat;


        }

     }


}


function getlisteabsencesday($dateday,$codeEtabLocal,$libellesessionencour,$classe)
  {
    $req = $this->db->prepare("SELECT * FROM presences,eleve,heure where presences.matricule_presence=eleve.matricule_eleve and presences.libelleheure_presence=heure.id_heure and date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and statut_presence=0  ");

    $req->execute([$dateday,$codeEtabLocal,$libellesessionencour,$classe]);

    return $req->fetchAll();
  }

function getlisteabsencesdayStudent($dateday,$codeEtabLocal,$libellesessionencour,$classe,$matricule)
  {
    $req = $this->db->prepare("SELECT * FROM presences,eleve,heure where presences.matricule_presence=eleve.matricule_eleve and presences.libelleheure_presence=heure.id_heure and date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and presences.matricule_presence=? and statut_presence=0  ");

    $req->execute([$dateday,$codeEtabLocal,$libellesessionencour,$classe,$matricule]);

    return $req->fetchAll();
  }




function getListeAttendancesStudent($matricule,$classe,$codeEtab,$session,$datedeb,$datefin)
{
     $etabs=new Etab();
     $nbjours=$etabs->nbreJour($datedeb,$datefin);
     $outp="";
     for($x=0;$x<$nbjours;$x++)
     {
	$dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($datedeb)));
	 $nbabsebnces=$etabs->getAbsencesthisdaynb($dateday,$codeEtab,$session,$classe);

	if($nbabsebnces>0)
        {
		$datas=$etabs->getlisteabsencesdayStudent($dateday,$codeEtab,$session,$classe,$matricule);
		foreach ($datas as $value):
                  $heures=$etabs->returnHours($value->heuredeb_heure). " - ".$etabs->returnHours($value->heurefin_heure);
                  $matiere=$etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence);
                  $statut="A";
	        if ($outp != "") {$outp .= ",";}

          	 $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
                 $outp .= '"heure":"'.$heures.'",';
                 $outp .= '"matiere":"'.$matiere.'",';
 	         $outp .= '"statut":"'.$statut. '"}';

                endforeach;
        }

     }
    $concat="[".$outp."]";

        return $concat;
}

function getMatiereLibelleByIdMat($matiereid,$codeEtab)
      {
        $req = $this->db->prepare("SELECT * FROM matiere where id_mat=? and codeEtab_mat=?");
        $req->execute([$matiereid,$codeEtab]);
        $data=$req->fetchAll();

         $array=json_encode($data,true);
         $someArray = json_decode($array, true);

         $donnees=$someArray[0]["libelle_mat"];
           return $donnees;
      }



   function getAbsencesthisdaynb($dateday,$codeEtabLocal,$libellesessionencour,$classe)
  {
    $req = $this->db->prepare("SELECT * FROM presences where date_presence=? and codeEtab_presence=? and session_presence=? and classe_presence=? and statut_presence=0  ");

    $req->execute([$dateday,$codeEtabLocal,$libellesessionencour,$classe]);

    $data=$req->fetchAll();

    $nb=count($data);

    return $nb;
  }

      function getstudentattencances($matricule,$classe,$session,$datedeb,$datefin)
      {
        $etabs=new Etab();
        $nbjours=$etabs->nbreJour($datedeb,$datefin);
        $outp = "";

        //if ($outp != "") {$outp .= ",";}
        for ($i=0;$i<$nbjours;$i++)
        {
          $dateday= date("Y-m-d", strtotime("+".$i."day", strtotime($datedeb)));

	  $nombre=$etabs->getNbAttendanceDay($matricule,$dateday,$classe);
          $statut="";
          if($nombre==0)
          {
            $statut="A";
          }else {
            $number=$etabs->getstatutAttendanceDay($matricule,$dateday,$classe);
            $array=json_encode($number,true);
            $someArray = json_decode($array, true);
            $presence=$someArray[0]["statut_presence"];
            if($presence==0)
            {
              $statut="A";
            }else {
              $statut="P";
            }
          }
          if ($outp != "") {$outp .= ",";}
          $outp .= '{"date":"'.date_format(date_create($dateday), "d/m/Y").'",';
 	  $outp .= '"presence":"'.$statut. '"}';



        }

	$concat="[".$outp."]";

        return $concat;

        //return $outp;



      }

     function getstudentroutines($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=?  order by id_days ASC, debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

   //nous allons faire les routines journali�res

   function getTeatcheroutinesLun($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and daysweek.id_days=1  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseLun($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=? and daysweek.id_days=1  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }


function getStudentsroutinesClasseLun($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=1  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getStudentsroutinesClasseMar($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=2  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }




function getStudentsroutinesClasseMer($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=3  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }


function getStudentsroutinesClasseJeu($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=4  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getStudentsroutinesClasseVen($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=5  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getStudentsroutinesClasseSam($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=6  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }


function getStudentsroutinesClasseDim($codeEtab,$sessionEtab,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.codeEtab_mat=? and matiere.session_mat=? and  classe.id_classe=? and daysweek.id_days=7  order by  debut_route ASC");
          $req->execute([$codeEtab,$sessionEtab,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }


    function getstudentroutinesLun($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=1  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

	function getTeatcheroutinesMar($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=?  and daysweek.id_days=2  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseMar($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=?  and daysweek.id_days=2  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }



function getstudentroutinesMar($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=2  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }


  function getTeatcheroutinesMer($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=?  and daysweek.id_days=3  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
	      $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseMer($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=?  and daysweek.id_days=3  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
	      $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }



function getstudentroutinesMer($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=3  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

   function getTeatcheroutinesJeu($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=?  and daysweek.id_days=4  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseJeu($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=?  and daysweek.id_days=4  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }



function getstudentroutinesJeu($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=4  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

	function getTeatcheroutinesVen($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and daysweek.id_days=5  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseVen($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=? and daysweek.id_days=5  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }




	function getstudentroutinesVen($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=5  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

   function getTeatcheroutinesSam($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and daysweek.id_days=6  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getTeatcheroutinesClasseSam($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and classe.id_classe=? and daysweek.id_days=6  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }



function getstudentroutinesSam($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=6  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

	function getTeatcheroutinesDim($teatcherid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and daysweek.id_days=7  order by  debut_route ASC");
          $req->execute([$teatcherid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

function getAllTeatcherquizsOne($compteuserid,$codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and classe.id_classe=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          $outp = "";
	foreach ($data as  $value):
	$tabdate=explode("-",$value->datelimite_quiz);
        $newdatelimit=$tabdate[2]." ".ucfirst(strtolower(obtenirLibelleMois($tabdate[1])))." ".$tabdate[0];

        if ($outp != "") {$outp .= ",";}
        $outp .= '{"id_quiz":"'.$value->id_quiz.'",';
        $outp .= '"datecrea_quiz":"'.$value->datecrea_quiz.'",';
        $outp .= '"libelle_quiz":"'.$value->libelle_quiz.'",';
        $outp .= '"duree_quiz":"'.DetermineHoursformat($value->duree_quiz).'",';
        $outp .= '"instruction_quiz":"'.$value->instruction_quiz.'",';
        $outp .= '"classe_quiz":"'.$value->classe_quiz.'",';
        $outp .= '"matiere_quiz":"'.$value->matiere_quiz.'",';
        $outp .= '"teatcher_quiz":"'.$value->teatcher_quiz.'",';
        $outp .= '"codeEtab_quiz":"'.$value->codeEtab_quiz.'",';
        $outp .= '"sessionEtab_quiz":"'.$value->sessionEtab_quiz.'",';
        $outp .= '"verouiller_quiz":"'.$value->verouiller_quiz.'",';
        $outp .= '"statut_quiz":"'.$value->statut_quiz.'",';
        $outp .= '"datelimite_quiz":"'.$newdatelimit.'",';
        $outp .= '"id_mat":"'.$value->id_mat.'",';
        $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
        $outp .= '"coef_mat":"'.$value->coef_mat.'",';
        $outp .= '"classe_mat":"'.$value->classe_mat.'",';
        $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
        $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
        $outp .= '"session_mat":"'.$value->session_mat.'",';
        $outp .= '"id_classe":"'.$value->id_classe.'",';
        $outp .= '"libelle_classe":"'.$value->libelle_classe.'",';
        $outp .= '"codeEtab_classe":"'.$value->codeEtab_classe.'",';
        $outp .= '"statut_classe":"'.$value->statut_classe.'",';
        $outp .= '"datecrea_classe":"'.$value->datecrea_classe.'",';
        $outp .= '"session_classe":"'.$value->session_classe.'",';
        $outp .= '"section_classe":"'.$value->section_classe.'",';
        $outp .= '"inscriptionmont_classe":"'.$value->inscriptionmont_classe.'",';
        $outp .= '"scolarite_classe":"'.$value->scolarite_classe.'",';
        $outp .= '"scolariteaff_classe":"'.$value->scolariteaff_classe.'",';
        $outp .= '"montantaes_classe":"'.$value->montantaes_classe.'",';
        $outp .= '"type_classe":"'.$value->type_classe. '"}';



	endforeach;

      echo "[".$outp."]" ;



      }

function getDetailsquizs($quizid,$classeid,$codeEtab,$sessionEtab,$teacherid)
       {

$etabs=new Etab();

$req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and classe.id_classe=? and quiz.id_quiz=?");
          $req->execute([
                $teacherid,$codeEtab,$sessionEtab,$classeid,$quizid
          ]);

          $data=$req->fetchAll();

          $outp = "";
/*
//recuperer les questions a choix multiples

        $req1 = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=2");
        $req1->execute([
        $quizid,$classeid,$codeEtab,$sessionEtab,$teacherid
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"libellequestion":"'.$values->libelle_quest.'",';
           $outp1 .= '"pointquestion":"'.$values->point_quest.'",';
           //nous allons chercher les proposotions de solutions

        $reqdata = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=?");
        $reqdata->execute([
        $values->id_quest,$values->id_quiz
        ]);

         $datareq=$reqdata->fetchAll();

         $outpdata = "";
          foreach ($datareq as  $valuedatas):
           if ($outpdata != "") {$outpdata .= ",";}
           $outpdata .= '{"proposition":"'.$valuedatas->libelle_proprep.'",';
           $outpdata .= '"answer":"'.$valuedatas->valeur_proprep.'"}';
          endforeach;
         $propositions="[".$outpdata."]";
        $outp1 .= '"propositions":'.$propositions.'}';


         endforeach;
           }




   //recuperer les questions vrai ou faux


   $req2 = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=1");
        $req2->execute([
        $quizid,$classeid,$codeEtab,$sessionEtab,$teacherid
        ]);

          $data2=$req2->fetchAll();
          $outp2 = "";

          if(count($data2)>0)
          {
foreach ($data2 as  $values):
           if ($outp2 != "") {$outp2 .= ",";}
           $outp2 .= '{"libellequestion":"'.$values->libelle_quest.'",';
           $outp2 .= '"pointquestion":"'.$values->point_quest.'",';
           //nous allons chercher les proposotions de solutions

        $reqdata1 = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=?");
        $reqdata1->execute([
        $values->id_quest,$values->id_quiz
        ]);

         $datareq1=$reqdata1->fetchAll();

         $outpdata1 = "";
          foreach ($datareq1 as  $valuedatas):
           if ($outpdata1 != "") {$outpdata1 .= ",";}
           $outpdata1 .= '{"proposition":"'.$valuedatas->libelle_proprep.'",';
           $outpdata1 .= '"answer":"'.$valuedatas->valeur_proprep.'"}';
          endforeach;
         $propositions="[".$outpdata1."]";
         $outp2 .= '"propositions":'.$propositions.'}';


         endforeach;


          }
*/






foreach ($data as  $value):
	$tabdate=explode("-",$value->datelimite_quiz);
        $newdatelimit=$tabdate[2]." ".ucfirst(strtolower(obtenirLibelleMois($tabdate[1])))." ".$tabdate[0];

        $trueorfalse=$etabs->getDetailsquizsLast($quizid,$classeid,$codeEtab,$sessionEtab,$teacherid);
        $multiplechoice=$etabs->getDetailsquizsLastOne($quizid,$classeid,$codeEtab,$sessionEtab,$teacherid);

        if ($outp != "") {$outp .= ",";}
        $outp .= '{"id_quiz":"'.$value->id_quiz.'",';
        $outp .= '"datecrea_quiz":"'.$value->datecrea_quiz.'",';
        $outp .= '"libelle_quiz":"'.$value->libelle_quiz.'",';
        $outp .= '"duree_quiz":"'.DetermineHoursformat($value->duree_quiz).'",';
        $outp .= '"instruction_quiz":"'.$value->instruction_quiz.'",';
        $outp .= '"classe_quiz":"'.$value->classe_quiz.'",';
        $outp .= '"matiere_quiz":"'.$value->matiere_quiz.'",';
        $outp .= '"teatcher_quiz":"'.$value->teatcher_quiz.'",';
        $outp .= '"codeEtab_quiz":"'.$value->codeEtab_quiz.'",';
        $outp .= '"sessionEtab_quiz":"'.$value->sessionEtab_quiz.'",';
        $outp .= '"verouiller_quiz":"'.$value->verouiller_quiz.'",';
        $outp .= '"statut_quiz":"'.$value->statut_quiz.'",';
        $outp .= '"datelimite_quiz":"'.$newdatelimit.'",';
        $outp .= '"id_mat":"'.$value->id_mat.'",';
        $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';

        //$outp .= '"trueorfalse":'.$outp2.',';
        $outp .= '"trueorfalse":'.$trueorfalse.',';
        $outp .= '"multiplechoice":'.$multiplechoice.',';


        //$outp .= '"multiplechoice":'.$outp1.',';
        $outp .= '"libelle_classe":"'.$value->libelle_classe.'"}';




	endforeach;

 echo "[".$outp."]" ;

//return $concat;



       }


function getDetailsquizsLastOne($quizid,$classeid,$codeEtab,$sessionEtab,$teacherid)
{
//recuperer les questions a choix multiples

        $req1 = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=2");
        $req1->execute([
        $quizid,$classeid,$codeEtab,$sessionEtab,$teacherid
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"libellequestion":"'.$values->libelle_quest.'",';
           $outp1 .= '"pointquestion":"'.$values->point_quest.'",';
           $outp1 .= '"questionid":"'.$values->id_quest.'",';

           //nous allons chercher les proposotions de solutions

        $reqdata = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=?");
        $reqdata->execute([
        $values->id_quest,$values->id_quiz
        ]);

         $datareq=$reqdata->fetchAll();

         $outpdata = "";
          foreach ($datareq as  $valuedatas):
           if ($outpdata != "") {$outpdata .= ",";}
           $outpdata .= '{"proposition":"'.$valuedatas->libelle_proprep.'",';
           $outpdata .= '"propositionid":"'.$valuedatas->id_proprep.'",';
           $outpdata .= '"answer":"'.$valuedatas->valeur_proprep.'"}';
          endforeach;
         $propositions="[".$outpdata."]";
        $outp1 .= '"propositions":'.$propositions.'}';


         endforeach;
           }

//echo "[".$outp1."]";

   return "[".$outp1."]" ;

}

function getDetailsquizsLast($quizid,$classeid,$codeEtab,$sessionEtab,$teacherid)
       {


//recuperer les questions vrai ou faux


   $req2 = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=1");
        $req2->execute([
        $quizid,$classeid,$codeEtab,$sessionEtab,$teacherid
        ]);

          $data2=$req2->fetchAll();
          $outp2 = "";

          if(count($data2)>0)
          {
foreach ($data2 as  $values):
           if ($outp2 != "") {$outp2 .= ",";}
           $outp2 .= '{"libellequestion":"'.$values->libelle_quest.'",';
           $outp2 .= '"pointquestion":"'.$values->point_quest.'",';
           $outp2 .= '"questionid":"'.$values->id_quest.'",';

           //nous allons chercher les proposotions de solutions

        $reqdata1 = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=?");
        $reqdata1->execute([
        $values->id_quest,$values->id_quiz
        ]);

         $datareq1=$reqdata1->fetchAll();

         $outpdata1 = "";
          foreach ($datareq1 as  $valuedatas):
           if ($outpdata1 != "") {$outpdata1 .= ",";}
           $outpdata1 .= '{"proposition":"'.$valuedatas->libelle_proprep.'",';
           $outpdata1 .= '"propositionid":"'.$valuedatas->id_proprep.'",';
           $outpdata1 .= '"answer":"'.$valuedatas->valeur_proprep.'"}';
          endforeach;
         $propositions="[".$outpdata1."]";
         $outp2 .= '"propositions":'.$propositions.'}';


         endforeach;


          }

          return "[".$outp2."]" ;

       }

function getResultatexamens($controleid,$teacherid,$matiereid,$codeEtab,$sessionEtab,$classeid)
        {
	$req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and parent.idcompte_parent=parenter.parentid_parenter and parenter.eleveid_parenter=eleve.idcompte_eleve  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 order by eleve.nom_eleve ASC,eleve.prenom_eleve ASC");

        $req->execute([$classeid,$sessionEtab]);

        $data=$req->fetchAll();

	$outp = "";

foreach ($data as  $value):
 if ($outp != "") {$outp .= ",";}

$ideleve=$value->idcompte_eleve;

//nous recherchons la note et l'observation pour cet el�ve

$req1 = $this->db->prepare("SELECT * from notes,examen where notes.idtype_notes=examen.id_exam and notes.type_notes=2 and examen.id_exam=? and notes.ideleve_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");
$req1->execute([$controleid,$ideleve,$matiereid,$classeid,$teacherid,$codeEtab,$sessionEtab]);
$data1=$req1->fetchAll();
foreach ($data1 as  $values):
$notes=$values->valeur_notes;
$observations=$values->obser_notes;
endforeach;

$outp .= '{"matricule":"'.$value->matricule_eleve.'",';
 $outp .= '"nom":"'.$value->nom_eleve.'",';
 $outp .= '"prenoms":"'.$value->prenom_eleve.'",';
 $outp .= '"note":"'.$notes.'",';
$outp .= '"observation":"'.$observations. '"}';

endforeach;

echo "[".$outp."]" ;


        }


	function getResultatcontroles($controleid,$teacherid,$matiereid,$codeEtab,$sessionEtab,$classeid)
        {
	$req = $this->db->prepare("SELECT * FROM eleve,compte,classe,inscription,parent,parenter where inscription.idclasse_inscrip=classe.id_classe and inscription.ideleve_inscrip=compte.id_compte and compte.id_compte=eleve.idcompte_eleve and inscription.session_inscrip=classe.session_classe and parent.idcompte_parent=parenter.parentid_parenter and parenter.eleveid_parenter=eleve.idcompte_eleve  and classe.id_classe=? and inscription.session_inscrip=? and compte.statut_compte=1 order by eleve.nom_eleve ASC,eleve.prenom_eleve ASC");

        $req->execute([$classeid,$sessionEtab]);

        $data=$req->fetchAll();

	$outp = "";

foreach ($data as  $value):
 if ($outp != "") {$outp .= ",";}

//nous recherchons la note et l'observation pour cet el�ve

$ideleve=$value->idcompte_eleve;

$req1 = $this->db->prepare("SELECT * from notes,controle where notes.idtype_notes=controle.id_ctrl and notes.type_notes=1 and controle.id_ctrl=? and notes.ideleve_notes=? and notes.idmat_notes=? and notes.idclasse_notes=? and notes.idmat_notes=? and notes.idprof_notes=? and notes.codeEtab_notes=? and notes.session_notes=?  ");
$req1->execute([$controleid,$ideleve,$matiereid,$classeid,$teacherid,$codeEtab,$sessionEtab]);
$data1=$req1->fetchAll();
foreach ($data1 as  $values):
$notes=$values->valeur_notes;
$observations=$values->obser_notes;
endforeach;

$outp .= '{"matricule":"'.$value->matricule_eleve.'",';
 $outp .= '"nom":"'.$value->nom_eleve.'",';
 $outp .= '"prenoms":"'.$value->prenom_eleve.'",';
 $outp .= '"note":"'.$notes.'",';
$outp .= '"observation":"'.$observations. '"}';

endforeach;

echo "[".$outp."]" ;


        }

function getAllStudentsquizsOne($codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe  and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and classe.id_classe=? and quiz.statut_quiz!=0");
          $req->execute([
                $codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          $outp = "";
	foreach ($data as  $value):
	$tabdate=explode("-",$value->datelimite_quiz);
        $newdatelimit=$tabdate[2]." ".ucfirst(strtolower(obtenirLibelleMois($tabdate[1])))." ".$tabdate[0];

        if ($outp != "") {$outp .= ",";}
        $outp .= '{"id_quiz":"'.$value->id_quiz.'",';
        $outp .= '"datecrea_quiz":"'.$value->datecrea_quiz.'",';
        $outp .= '"libelle_quiz":"'.$value->libelle_quiz.'",';
        $outp .= '"duree_quiz":"'.DetermineHoursformat($value->duree_quiz).'",';
        $outp .= '"instruction_quiz":"'.$value->instruction_quiz.'",';
        $outp .= '"classe_quiz":"'.$value->classe_quiz.'",';
        $outp .= '"matiere_quiz":"'.$value->matiere_quiz.'",';
        $outp .= '"teatcher_quiz":"'.$value->teatcher_quiz.'",';
        $outp .= '"codeEtab_quiz":"'.$value->codeEtab_quiz.'",';
        $outp .= '"sessionEtab_quiz":"'.$value->sessionEtab_quiz.'",';
        $outp .= '"verouiller_quiz":"'.$value->verouiller_quiz.'",';
        $outp .= '"statut_quiz":"'.$value->statut_quiz.'",';
        $outp .= '"datelimite_quiz":"'.$newdatelimit.'",';
        $outp .= '"id_mat":"'.$value->id_mat.'",';
        $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
        $outp .= '"coef_mat":"'.$value->coef_mat.'",';
        $outp .= '"classe_mat":"'.$value->classe_mat.'",';
        $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
        $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
        $outp .= '"session_mat":"'.$value->session_mat.'",';
        $outp .= '"id_classe":"'.$value->id_classe.'",';
        $outp .= '"libelle_classe":"'.$value->libelle_classe.'",';
        $outp .= '"codeEtab_classe":"'.$value->codeEtab_classe.'",';
        $outp .= '"statut_classe":"'.$value->statut_classe.'",';
        $outp .= '"datecrea_classe":"'.$value->datecrea_classe.'",';
        $outp .= '"session_classe":"'.$value->session_classe.'",';
        $outp .= '"section_classe":"'.$value->section_classe.'",';
        $outp .= '"inscriptionmont_classe":"'.$value->inscriptionmont_classe.'",';
        $outp .= '"scolarite_classe":"'.$value->scolarite_classe.'",';
        $outp .= '"scolariteaff_classe":"'.$value->scolariteaff_classe.'",';
        $outp .= '"montantaes_classe":"'.$value->montantaes_classe.'",';
        $outp .= '"type_classe":"'.$value->type_classe. '"}';



	endforeach;

      echo "[".$outp."]" ;



      }



function getTeatcheroutinesClasseDim($teatcherid,$classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere,classe where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=classe.id_classe and matiere.teatcher_mat=? and daysweek.id_days=7 and classe.id_classe=?  order by  debut_route ASC");
          $req->execute([$teatcherid,$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"libelleclasse_route":"'.$value->libelle_classe.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }




function getstudentroutinesDim($classeid)
        {
  	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.id_days=7  order by  debut_route ASC");
          $req->execute([$classeid]);
          $data=$req->fetchAll();
  	//DetermineHoursformat($hours)
            $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}
            $outp .= '{"id_days":"'.$value->id_days.'",';
              $outp .= '"libelle_days":"'.$value->libelle_days.'",';
              $outp .= '"short_days":"'.$value->short_days.'",';
              $outp .= '"id_route":"'.$value->id_route.'",';
              $outp .= '"classe_route":"'.$value->classe_route.'",';
              $outp .= '"etab_route":"'.$value->etab_route.'",';
              $outp .= '"debut_route":"'.DetermineHoursformat($value->debut_route).'",';
              $outp .= '"fin_route":"'.DetermineHoursformat($value->fin_route).'",';
              $outp .= '"matiere_route":"'.$value->matiere_route.'",';
              $outp .= '"day_route":"'.$value->day_route.'",';
              $outp .= '"session_route":"'.$value->session_route.'",';
              $outp .= '"id_mat":"'.$value->id_mat.'",';
              $outp .= '"libelle_mat":"'.$value->libelle_mat.'",';
              $outp .= '"coef_mat":"'.$value->coef_mat.'",';
              $outp .= '"classe_mat":"'.$value->classe_mat.'",';
              $outp .= '"teatcher_mat":"'.$value->teatcher_mat.'",';
              $outp .= '"codeEtab_mat":"'.$value->codeEtab_mat.'",';
              $outp .= '"session_mat":"'.$value->session_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;

        }

   function getNumberSessionEncoursOn($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getSessionEncours($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

$donnees=$someArray[0]["libelle_sess"]."*".$someArray[0]["id_sess"]."*".$someArray[0]["type_sess"];
return $donnees;
}

function obtenirLibelleJours($jour) {
switch($jour) {
    case 'Sunday': $jour = 'DIM'; break;
    case 'Monday': $jour = 'LUN'; break;
    case 'Tuesday': $jour = 'MAR'; break;
    case 'Wednesday': $jour = 'MER'; break;
    case 'Thursday': $jour = 'JEU'; break;
    case 'Friday': $jour = 'VEN'; break;
    case 'Saturday': $jour = 'SAM'; break;
    default: $jour =''; break;
  }
  return $jour;
}

function nom_jour($date) {

$jour_semaine = array(1=>"Monday", 2=>"Tuesday", 3=>"Wednesday", 4=>"Thursday", 5=>"Friday", 6=>"Saturday", 7=>"Sunday");

list($annee, $mois, $jour) = explode ("-", $date);

$timestamp = mktime(0,0,0, date($mois), date($jour), date($annee));
$njour = date("N",$timestamp);

return $jour_semaine[$njour];

}

function getAllMatiereOfDaysTeaNew($teatcherid,$classeid,$codeEtab,$sessionEtab,$libellejour)
{


 $datas=$etabs->getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classeid,$codeEtab,$sessionEtab,$libellejour,$teatcherid);

  return json_encode($datas);

}


function getAllMatiereOfDaysTea($teatcherid,$classeid,$codeEtab,$datechoice)
{

$etabs=new Etab();

  $nbsessionOn=$etabs->getNumberSessionEncoursOn($codeEtab);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$etabs->getSessionEncours($codeEtab);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }


 $tabdays=explode("/",$datechoice);

  $jourday=$tabdays[0];
  $moisday=$tabdays[1];
  $anneeday=$tabdays[2 ];

  $nouvelledate=$anneeday."-".$moisday."-".$jourday;

  //$jour=$etabs->nom_jour($nouvelledate);
  $jour=nom_jour($nouvelledate);
  $libellejour=$etabs->obtenirLibelleJours($jour);

  $datas=$etabs->getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classeid,$codeEtab,$libellesessionencours,$libellejour,$teatcherid);

  return json_encode($datas);

  //return $classeid." / ".$codeEtab." / ".$libellesessionencours." / ".$libellejour." / ".$teatcherid;
}

function getallpresencesLibsIds($datechoice,$classe,$matiereid,$profid,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT distinct libelleheure_presence from presences where date_presence=? and classe_presence=? and matiere_presence=? and teatcher_presence=? and codeEtab_presence=? and session_presence=?");
        $req->execute([$datechoice,$classe,$matiereid,$profid,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        $retour="";
        if($nb>0)
        {
          foreach ($data as $value):
            $retour=$retour.$value->libelleheure_presence .",";

          endforeach;
        }

      return $retour;
      }

function getHoursMatiere($teacherid,$matiereid,$classeid,$codeEtab,$sessionEtab,$datechoice)
{

}

function getHoursMatiereOfDaysTea($teacherid,$matiereid,$classeid,$codeEtab,$sessionEtab,$datechoice)
{

  $etabs=new Etab();

  $tabtade=explode("/",$datechoice);

  $datechoice=$tabtade[2]."-".$tabtade[1]."-".$tabtade[0];

  $jour=$etabs->nom_jour($datechoice);
  $libellejour=$etabs->obtenirLibelleJours($jour);

  $nbpresencefaite=$etabs->getpresenceslibhoursnbdatechoice($datechoice,$classeid,$matiereid,$teacherid,$codeEtab,$sessionEtab);

  //return  $nbpresencefaite ." / ".$classeid." / ".$codeEtab." / ".$sessionEtab." / ".$matiereid." / ".$libellejour;



  if($nbpresencefaite>0)
  {
	  $allidsLibs=$etabs->getallpresencesLibsIds($datechoice,$classeid,$matiereid,$teacherid,$codeEtab,$sessionEtab);
          $allidsLibs=substr($allidsLibs, 0, -1);
          $tabids=explode(",",$allidsLibs);
          $datas=$etabs->getListehoursofcourseMatDiff($classeid,$codeEtab,$sessionEtab,$matiereid,$libellejour,$allidsLibs);
  }else
  {
	  $datas=$etabs->getListehoursofcourseMat($classeid,$codeEtab,$sessionEtab,$matiereid,$libellejour);
  }


    return json_encode($datas);
}

function getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);
      }

function getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);

      }

function getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and courses.teatcher_courses=?");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);
      }

function getAllControleMatiereOfThisTeatcherId($idcompte)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,controle,compte,enseignant,etablissement,classe where compte.id_compte=enseignant.idcompte_enseignant and controle.teatcher_ctrl=compte.id_compte and controle.codeEtab_ctrl=etablissement.code_etab and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=? ");
        $req->execute([$idcompte]);
        $data=$req->fetchAll();

          return json_encode($data);
      }

function getAllControleMatiereOfThisTeatcherIdClasse($idcompte,$codeEtab,$sessionEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,controle,compte,enseignant,etablissement,classe where compte.id_compte=enseignant.idcompte_enseignant and controle.teatcher_ctrl=compte.id_compte and controle.codeEtab_ctrl=etablissement.code_etab and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and controle.teatcher_ctrl=? and controle.codeEtab_ctrl=? and controle.session_ctrl=? and controle.classe_ctrl=? ");
        $req->execute([$idcompte,$codeEtab,$sessionEtab,$classeid]);
        $data=$req->fetchAll();

          return json_encode($data);
      }


function getAllControleMatiereOfThisStudentsClasse($codeEtab,$sessionEtab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM matiere,controle,compte,enseignant,etablissement,classe where compte.id_compte=enseignant.idcompte_enseignant and controle.teatcher_ctrl=compte.id_compte and controle.codeEtab_ctrl=etablissement.code_etab and controle.mat_ctrl=matiere.id_mat and controle.classe_ctrl=classe.id_classe and controle.codeEtab_ctrl=? and controle.session_ctrl=? and controle.classe_ctrl=? ");
        $req->execute([$codeEtab,$sessionEtab,$classeid]);
        $data=$req->fetchAll();

          return json_encode($data);
      }


function getAllTeatcherquizs($compteuserid,$codeEtabsession,$libellesessionencours,$classeid)
      {


$req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and classe.id_classe=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours,$classeid
          ]);
          $data=$req->fetchAll();

         return json_encode($data);





      }



function getAllTeatcherquizsPublished($compteuserid,$codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM quiz,matiere,classe where quiz.matiere_quiz=matiere.id_mat and quiz.classe_quiz=classe.id_classe and quiz.teatcher_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and classe.id_classe=? and quiz.statut_quiz=1 ");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

         return json_encode($data);



      }




function getAllTeatcherdevoirs($compteuserid,$codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe and devoirs.teatcher_dev=? and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and classe.id_classe=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          return json_encode($data);


      }

function getAllStudentsdevoirs($codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM devoirs,matiere,classe where devoirs.matiere_dev=matiere.id_mat and devoirs.classe_dev=classe.id_classe  and devoirs.codeEtab_dev=? and devoirs.sessionEtab_dev=? and classe.id_classe=?");
          $req->execute([
                $codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          return json_encode($data);


      }


function getAllquizQuestionTrueOrfalse($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=1");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);
      }

function getAllquizpropositionrepQuestion($questionid,$quizid)
{
 $req = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=?");
        $req->execute([
        $questionid,$quizid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);


}

function getAllquizpropositionrepQuestionOk($questionid,$quizid)
{
 $req = $this->db->prepare("SELECT * FROM propositionrep,question where question.id_quest=propositionrep.idquest_proprep and question.id_quest=? and question.idquiz_quest=? and propositionrep.valeur_proprep=1");
        $req->execute([
        $questionid,$quizid
        ]);

         $data=$req->fetchAll();

          return json_encode($data);


}


function getAllquizQuestionMultiplechoice($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid)
      {
        $req = $this->db->prepare("SELECT * FROM quiz,question where quiz.id_quiz=question.idquiz_quest and 	quiz.id_quiz=? and quiz.classe_quiz=? and quiz.codeEtab_quiz=? and quiz.sessionEtab_quiz=? and quiz.teatcher_quiz=? and question.mode_quest=2");
        $req->execute([
        $courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid
        ]);

          $data=$req->fetchAll();

          return json_encode($data);

}


function getAllCoursesTea($compteuserid,$codeEtabsession,$libellesessionencours)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours
          ]);

          $data=$req->fetchAll();

          return json_encode($data);


      }

function getAllCoursesTeaClasse($compteuserid,$codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and classe.id_classe=?");
          $req->execute([
                $compteuserid,$codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          return json_encode($data);


      }

function getAllCoursesClasseStudent($codeEtabsession,$libellesessionencours,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM courses,matiere,classe where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and  courses.codeEtab_courses=? and courses.sessionEtab_courses=? and classe.id_classe=?");
          $req->execute([
                $codeEtabsession,$libellesessionencours,$classeid
          ]);

          $data=$req->fetchAll();

          return json_encode($data);


      }

function getDetailscourseSections($courseid,$classeid,$codeEtab,$sessionEtab)
{


        $req1 = $this->db->prepare("SELECT * FROM courses,coursessection where courses.id_courses=coursessection.idcourse_coursesec and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req1->execute([
        $courseid,$classeid,$codeEtab,$sessionEtab
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"sectionid":"'.$values->id_coursesec.'",';
           $outp1 .= '"libellesection":"'.$values->libelle_coursesec.'"}';


         endforeach;
           }

//echo "[".$outp1."]";

   return "[".$outp1."]" ;

}

function getDetailscourseCompetences($courseid,$classeid,$codeEtab,$sessionEtab)
{


        $req1 = $this->db->prepare("SELECT * FROM courses,coursescomp where courses.id_courses=coursescomp.idcourse_coursescomp and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req1->execute([
        $courseid,$classeid,$codeEtab,$sessionEtab
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"compid":"'.$values->id_coursecomp.'",';
           $outp1 .= '"libellecomp":"'.$values->libelle_coursecomp.'"}';


         endforeach;
           }

//echo "[".$outp1."]";

   return "[".$outp1."]" ;

}

function getDetailscourseHomeworks($courseid,$classeid,$codeEtab,$sessionEtab)
{


        $req1 = $this->db->prepare("SELECT * FROM courses,coursesworkh where courses.id_courses=coursesworkh.idcourse_coursesworkh and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
        $req1->execute([
        $courseid,$classeid,$codeEtab,$sessionEtab
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"homeworkid":"'.$values->id_coursewh.'",';
           $outp1 .= '"libellehomework":"'.$values->libelle_coursewh.'"}';


         endforeach;
           }

//echo "[".$outp1."]";

   return "[".$outp1."]" ;

}

function getDetailscourseSupports($courseid,$classeid,$codeEtab,$sessionEtab)
{


        $req1 = $this->db->prepare("SELECT * FROM courses,supports where courses.id_courses=supports.courseid_support and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=? and type_support='COURSES'");
        $req1->execute([
        $courseid,$classeid,$codeEtab,$sessionEtab
        ]);

          $data1=$req1->fetchAll();
           $outp1 = "";

           if(count($data1)>0)
           {
foreach ($data1 as  $values):
           if ($outp1 != "") {$outp1 .= ",";}
           $outp1 .= '{"supportid":"'.$values->id_support.'",';
           $outp1 .= '"fichier":"'.$values->fichier_support.'"}';


         endforeach;
           }

//echo "[".$outp1."]";

   return "[".$outp1."]" ;

}





function getCoursesdetails($courseid,$codeEtab,$sessionEtab,$classeid)
{

$etabs=new Etab();

 $req = $this->db->prepare("SELECT * FROM courses,matiere,classe,compte where courses.mat_courses=matiere.id_mat and courses.classe_courses=classe.id_classe and courses.teatcher_courses=compte.id_compte and courses.id_courses=? and courses.classe_courses=? and courses.codeEtab_courses=? and courses.sessionEtab_courses=?");
          $req->execute([
                $courseid,$classeid,$codeEtab,$sessionEtab
          ]);

          $data=$req->fetchAll();

          $outp = "";


          foreach ($data as  $value):
           if ($outp != "") {$outp .= ",";}

	     $sections=$etabs->getDetailscourseSections($courseid,$classeid,$codeEtab,$sessionEtab);
	     $competences=$etabs->getDetailscourseCompetences($courseid,$classeid,$codeEtab,$sessionEtab);
             $homeworks=$etabs->getDetailscourseHomeworks($courseid,$classeid,$codeEtab,$sessionEtab);
             $supports=$etabs->getDetailscourseSupports($courseid,$classeid,$codeEtab,$sessionEtab);
             $chemin="courses/".$value->libelle_classe."/".$value->date_courses."/".$value->libelle_mat."/";

              $outp .= '{"courseid":"'.$value->id_courses.'",';
              $outp .= '"libellecourse":"'.$value->libelle_courses.'",';
              $outp .= '"datecourse":"'.$value->date_courses.'",';
              $outp .= '"descripcourse":"'.$value->descri_courses.'",';
              $outp .= '"statutcourse":"'.$value->statut_courses.'",';
              $outp .= '"dureecourse":"'.$value->duree_courses.'",';
              $outp .= '"matiereid":"'.$value->mat_courses.'",';
              $outp .= '"classeid":"'.$value->classe_courses.'",';
              $outp .= '"codeEtab":"'.$value->codeEtab_courses.'",';
              $outp .= '"sessionEtab":"'.$value->sessionEtab_courses.'",';
              $outp .= '"teatcherid":"'.$value->teatcher_courses.'",';
              $outp .= '"teatchernom":"'.$value->nom_compte.'",';
              $outp .= '"teatcherprenoms":"'.$value->prenom_compte.'",';
              $outp .= '"libelleclasse":"'.$value->libelle_classe.'",';
	      $outp .= '"sections":'.$sections.',';
              $outp .= '"competences":'.$competences.',';
              $outp .= '"homeworks":'.$homeworks.',';
              $outp .= '"supports":'.$supports.',';
              $outp .= '"cheminsupports":"'.$chemin.'",';
              $outp .= '"libellematiere":"'.$value->libelle_mat. '"}';

          endforeach;

          echo "[".$outp."]" ;


}



function getListehoursofcourseMat($classe,$codeEtab,$session,$matiereid,$libellejour)
      {
         // $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.idlib_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? ");
         $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.id_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? ");
         $req->execute([$classe,$codeEtab,$session,$matiereid,$libellejour]);
         return $req->fetchAll();
      }

function getListehoursofcourseMatDiff($classe,$codeEtab,$session,$matiereid,$libellejour,$allidsLibs)
      {
         // $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.idlib_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? and heure.idlib_heure NOT IN(?)  ");
          $req = $this->db->prepare("SELECT distinct id_heure,id_heurelib,libelle_heurelib from heure,heurelib,routine where heure.idlib_heure=heurelib.id_heurelib and heure.id_heure=routine.libelleHeure_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.matiere_route=? and routine.day_route=? and heure.id_heure NOT IN(?)  ");
         $req->execute([$classe,$codeEtab,$session,$matiereid,$libellejour,$allidsLibs]);
         return $req->fetchAll();
      }

function getpresenceslibhoursnbdatechoice($datechoice,$classe,$matiereid,$profid,$codeEtab,$session)
      {
        $req = $this->db->prepare("SELECT distinct libelleheure_presence from presences where date_presence=? and classe_presence=? and matiere_presence=? and teatcher_presence=? and codeEtab_presence=? and session_presence=?");
        $req->execute([$datechoice,$classe,$matiereid,$profid,$codeEtab,$session]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }


function getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classe,$code,$libellesessionencours,$libellejour,$teatcherid)
{
     $req = $this->db->prepare("SELECT distinct matiere.id_mat,matiere.libelle_mat,matiere.teatcher_mat,matiere.coef_mat FROM matiere,routine where matiere.id_mat=routine.matiere_route and routine.classe_route=? and routine.etab_route=? and routine.session_route=? and routine.day_route=? and matiere.teatcher_mat=? ");
     $req->execute([
     $classe,
     $code,
     $libellesessionencours,
     $libellejour,
     $teatcherid
   ]);
    return $req->fetchAll();
}

      function getNbAttendanceDay($matricule,$dateday,$classe)
      {
        $req = $this->db->prepare("SELECT statut_presence FROM presences where matricule_presence=? and classe_presence=? and date_presence=? ");

        $req->execute([$matricule,$classe,$dateday]);

        $data=$req->fetchAll();

        $nb=count($data);

        return $nb;
      }

	function getRoutinesclasses($classeid)
	{
	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days  and routine.classe_route=?  order by id_days ASC, debut_route ASC");
	 $req->execute([$classe,$short_days]);
         $data=$req->fetchAll();
	 return json_encode($data);

        }



/*
function getstudentroutines($classeid)
      {
	 $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=?  order by id_days ASC, debut_route ASC");
        $req->execute([$classeid]);
        $data=$req->fetchAll();
        return json_encode($data);

      }

*/



	/*function getstudentroutines($classeid)
      {
        $etabs=new Etab();
        $dataday=$etabs->getAllweeks();
        $tab=array();
        $i=0;
 	$outp = "";

        foreach ($dataday as $value) :
          //$tab[$i]=$etabs->getspecificRoutine($value->id_days,$value->short_days,$classeid);
          //$i++;
	$nombre=$etabs->getspecificRoutineNb($value->id_days,$value->short_days,$classeid);

	if($nombre>0)
	{
       $outp .=$etabs->getspecificRoutine($value->id_days,$value->short_days,$classeid);
        }



        endforeach;

        //return json_encode($outp);

	return $outp;
      }*/

      function getspecificRoutine($id_days,$short_days,$classe)
      {
        $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? order by debut_route ASC");
        $req->execute([$classe,$short_days]);
        $data=$req->fetchAll();
        return json_encode($data);
	//return $data;
      }

	function getspecificRoutineNb($id_days,$short_days,$classe)
      {
        $req = $this->db->prepare("SELECT * from daysweek,routine,matiere where routine.matiere_route=matiere.id_mat and  routine.day_route=daysweek.short_days and routine.classe_route=? and daysweek.short_days=? order by debut_route ASC");
        $req->execute([$classe,$short_days]);
         $data=$req->fetchAll();
        $nb=count($data);
        return $nb;      }


      function getAllweeks()
      {
        $req = $this->db->prepare("SELECT * from daysweek");
        $req->execute([]);
        return $req->fetchAll();

      }
	function getallCompentencesOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
        //$data= $req->fetchAll();
        //return json_encode($data);

	$data=$req->fetchAll();
        $outp = "";

        foreach ($data as $value):
        if ($outp != "") {$outp .= ",";}
          $outp .= '{"Id":"'.$value->id_syllabcomp.'",';
          $outp .= '"libelle_comp":"'. $value->libelle_syllabcomp.'"}';

        endforeach;

         $retour="[".$outp."]" ;
          return $retour;


      }

   function getallCompentencesOfthisSyllabusClasse($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=? and syllabus.idclasse_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid]);
        //$data= $req->fetchAll();
        //return json_encode($data);

	$data=$req->fetchAll();
        $outp = "";
        if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
          $outp .= '{"Id":"'.$value->id_syllabcomp.'",';
          $outp .= '"libelle_comp":"'. $value->libelle_syllabcomp.'"}';

        endforeach;

         $retour="[".$outp."]" ;
          return $retour;


      }


	function getallCompentencesOfthisSyllabusOne($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
        $data= $req->fetchAll();
        return json_encode($data);

	/*$data=$req->fetchAll();
        $outp = "";
        if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
          $outp .= '{"Id":"'.$value->id_syllabcomp.'",';
          $outp .= '"libelle_comp":"'. $value->libelle_syllabcomp.'"}';

        endforeach;

         $retour="[".$outp."]" ;
          return $retour;*/


      }

function getallCompentencesOfthisSyllabusOneClasse($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid)
      {
        $req = $this->db->prepare("SELECT * FROM syllabus,syllabcomp where syllabus.id_syllab=syllabcomp.idsyllab_syllabcomp and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=? and syllabus.idclasse_syllab=?");
        $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid]);
        $data= $req->fetchAll();
        return json_encode($data);

	/*$data=$req->fetchAll();
        $outp = "";
        if ($outp != "") {$outp .= ",";}
        foreach ($data as $value):
          $outp .= '{"Id":"'.$value->id_syllabcomp.'",';
          $outp .= '"libelle_comp":"'. $value->libelle_syllabcomp.'"}';

        endforeach;

         $retour="[".$outp."]" ;
          return $retour;*/


      }



      function getallObjectifsOfthisSyllabus($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
          $data= $req->fetchAll();
          //return json_encode($data);


          $outp = "";

          foreach ($data as $value):
         if ($outp != "") {$outp .= ",";}
            $outp .= '{"Id":"'  . $value->id_syllabob . '",';
            $outp .= '"libelle_object":"'   . $value->libelle_syllabob  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;
      }


   function getallObjectifsOfthisSyllabusClasse($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=? and syllabus.idclasse_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$classeid]);
          $data= $req->fetchAll();
          //return json_encode($data);


          $outp = "";
          if ($outp != "") {$outp .= ",";}
          foreach ($data as $value):
            $outp .= '{"Id":"'  . $value->id_syllabob . '",';
            $outp .= '"libelle_object":"'   . $value->libelle_syllabob  . '"}';

          endforeach;
	  $retour="[".$outp."]" ;
          return $retour;
      }


	function getallObjectifsOfthisSyllabusOne($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab]);
          $data= $req->fetchAll();
          return json_encode($data);



      }

function getallObjectifsOfthisSyllabusOneClasse($idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$claseid)
      {
          $req = $this->db->prepare("SELECT * FROM syllabus,syllabobjet where syllabus.id_syllab=syllabobjet.idsyllab_syllabob and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and  syllabus.idmatiere_syllab=? and syllabus.session_syllab=? and syllabus.idclasse_syllab=?");
          $req->execute([$idsyllab,$idteatchersyllab,$idmatieresyllab,$sessionsyllab,$claseid]);
          $data= $req->fetchAll();
          return json_encode($data);



      }



	function getSyllabusInfos($programme,$teatcher,$codeEtab)
      {

          $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere  where syllabus.idclasse_syllab=classe.id_classe and matiere.codeEtab_mat=classe.codeEtab_classe and syllabus.id_syllab=? and syllabus.idteatcher_syllab=? and classe.codeEtab_classe=?");
         $req->execute([$programme,$teatcher,$codeEtab]);
          return $req->fetchAll();
      }



      function getprogrammesallteatchersMat($classeEtab,$codeEtab,$libellesessionencours,$matiere)

      {

    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? and syllabus.idmatiere_syllab=? order by syllabus.id_syllab ASC");

    $req->execute([$classeEtab,$codeEtab,$libellesessionencours,$matiere]);

    $data= $req->fetchAll();
    return json_encode($data);


      }

function getprogrammesallteatchersMatClasse($classeEtab,$codeEtab,$libellesessionencours,$matiere,$classeid)

      {

    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? and syllabus.idmatiere_syllab=? and syllabus.idclasse_syllab=? order by syllabus.id_syllab ASC");

    $req->execute([$classeEtab,$codeEtab,$libellesessionencours,$matiere,$classeid]);

    $data= $req->fetchAll();
    return json_encode($data);


      }


 function getprogrammesingleteatchersMat($classeEtab,$codeEtab,$libellesessionencours,$matiere,$teatcherid)

      {

    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? and syllabus.idmatiere_syllab=? and syllabus.idteatcher_syllab=? order by syllabus.id_syllab ASC");

    $req->execute([$classeEtab,$codeEtab,$libellesessionencours,$matiere,$teatcherid]);

    $data= $req->fetchAll();
    return json_encode($data);


      }


function getprogrammesallteatchers($classeEtab,$codeEtab,$libellesessionencours)

      {

    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");

    $req->execute([$classeEtab,$codeEtab,$libellesessionencours]);

    $data= $req->fetchAll();
    return json_encode($data);




      }

function getprogrammesallteatchersClasse($classeEtab,$codeEtab,$libellesessionencours,$teatcherid)

      {

    $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? and syllabus.idteatcher_syllab=? order by syllabus.id_syllab ASC");

    $req->execute([$classeEtab,$codeEtab,$libellesessionencours,$teatcherid]);

    $data= $req->fetchAll();
    return json_encode($data);


      }



/*
function getprogrammesallteatchers($classeEtab,$codeEtab,$libellesessionencours)
      {
      $etabs=new Etab();
      $req = $this->db->prepare("SELECT * FROM syllabus,classe,matiere,dispenser,enseignant where dispenser.idclasse_disp=classe.id_classe and dispenser.id_cours=matiere.id_mat and dispenser.id_enseignant=enseignant.idcompte_enseignant and syllabus.idteatcher_syllab=enseignant.idcompte_enseignant and syllabus.idmatiere_syllab=matiere.id_mat and syllabus.idclasse_syllab=classe.id_classe and classe.id_classe=? and classe.codeEtab_classe=?  and  syllabus.session_syllab=? order by syllabus.id_syllab ASC");
      $req->execute([$classeEtab,$codeEtab,$libellesessionencours]);
      $data= $req->fetchAll();
      //return json_encode($data, JSON_FORCE_OBJECT);
      $outp = "";
      //if ($outp != "") {$outp .= ",";}
     foreach ($data as $value):
        if ($outp != "") {$outp .= ",";}
        $competences= $etabs->getallCompentencesOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);
        $objectifs=  $etabs->getallObjectifsOfthisSyllabus($value->id_syllab,$value->idteatcher_syllab,$value->idmatiere_syllab,$value->session_syllab);
        $outp .= '{"Id":"'  .$value->id_syllab. '",';
        $outp .= '{"matiere":"'  .$value->libelle_mat. '",';
        $outp .= '{"description":"'  .$value->descri_syllab. '",';
        $outp .= '{"objectifs":"'  .$objectifs. '",';
        $outp .= '{"competences":"'  .$competences. '",';
        $outp .= '"classe":"'.$value->libelle_classe. '"}';

      endforeach;

      return $outp;



      }


*/



}







?>
