<?php
class Sessionacade{

public $db;
function __construct() {

require_once('../class/cnx.php');


  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function getNumberSessionEncoursOn($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getSessionEncours($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

$donnees=$someArray[0]["libelle_sess"]."*".$someArray[0]["id_sess"]."*".$someArray[0]["type_sess"];
return $donnees;
}

}
 ?>
