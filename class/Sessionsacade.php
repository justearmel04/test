<?php
// session_start();

class Sessionacade{

public $db;
function __construct() {

require_once('../class/cnx.php');


  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function Updatesessionprimary($sessionid,$codeEtab,$statut,$encours)
{

    $req = $this->db->prepare("UPDATE sessions SET 	statut_sess=?,encours_sess=? WHERE id_sess=? and codeEtab_sess=?");
    $req->execute([
    $statut,$encours,$sessionid,$codeEtab
    ]);




}

function getTypesessionAcadem($codeEtab,$session,$typesession)
{
  $req = $this->db->prepare("SELECT * from semestre where codeEtab_semes=? and id_semes=?");
  $req->execute([
  $codeEtab,$typesession
  ]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);
  $donnees=$someArray[0]["libelle_semes"];
  return $donnees;


}

function UpdateSessionType($sessionid,$typesess,$status,$suivant,$statutsuivant)
{
  $req = $this->db->prepare("UPDATE semestre SET 	statut_semes=? WHERE id_semes=? and idsess_semes=?");
  $req->execute([
  $status,$typesess,$sessionid
  ]);

  //modifier le nouveau qui sera actif

  $req1 = $this->db->prepare("UPDATE semestre SET 	statut_semes=? WHERE id_semes=? and idsess_semes=?");
  $req1->execute([
  $statutsuivant,$suivant,$sessionid
  ]);




}

  function getAllsemesterOfthisSession($sessionencoursid,$codeEtabAssigner)
  {
    $statutfinal=2;
    $req = $this->db->prepare("SELECT * from semestre where codeEtab_semes=? and idsess_semes=?");
    $req->execute([$codeEtabAssigner,$sessionencoursid]);
    return $req->fetchAll();
  }

  function UpdateNextSemestre($idsemestreadd,$idsession,$codeEtab,$suivant)
  {
    $req = $this->db->prepare("UPDATE semestre SET next_semes=? WHERE idsess_semes=? and codeEtab_semes=? and id_semes=?");
    $req->execute([
    $suivant,
    $idsession,
    $codeEtab,
    $idsemestreadd
    ]);

  }

  function UpdateNextSemestreFinal($idsemestreadd,$idsession,$codeEtab,$suivant)
  {
    $req = $this->db->prepare("UPDATE semestre SET next_semes=? WHERE idsess_semes=? and codeEtab_semes=? and id_semes=?");
    $req->execute([
    $suivant,
    $idsession,
    $codeEtab,
    $idsemestreadd
    ]);



  }

  function UpdateCurrentlySemester($firstsemesterid,$encours,$libellesession,$codeEtab,$idsession)
  {
    $req = $this->db->prepare("UPDATE semestre SET statut_semes=? WHERE id_semes=? and codeEtab_semes=? and idsess_semes=?");
    $req->execute([
      $encours,
      $firstsemesterid,
      $codeEtab,
      $idsession,

    ]);
  }

function UpdateSessionSchool($sessionid,$sessionlib,$typesession,$codeEtab)
{
    $req = $this->db->prepare("UPDATE sessions SET libelle_sess=?,type_sess=? WHERE id_sess=? and codeEtab_sess=?");
    $req->execute([
      $sessionlib,
      $typesession,
      $sessionid,
      $codeEtab
    ]);

    //supprimer les semestres de cette session

    $req1= $this->db->prepare("DELETE FROM semestre where idsess_semes=? AND codeEtab_semes=?");
    $req1->execute([$sessionid,$codeEtab]);


}

function AddSemestreFirst($libellesemestre,$idsession,$statut,$codeEtab)
{
  $req = $this->db->prepare("INSERT INTO semestre SET idsess_semes=?,libelle_semes=?,statut_semes=?,codeEtab_semes=?");
  $req->execute([$idsession,$libellesemestre,$statut,$codeEtab]);
  $idlastcompte=$this->db->lastInsertId();

  return $idlastcompte;
}

function AddSemestre($libellesemestre,$idsession,$statut,$codeEtab,$next)
{
  $req = $this->db->prepare("INSERT INTO semestre SET idsess_semes=?,libelle_semes=?,statut_semes=?,codeEtab_semes=?,next_semes=?");
  $req->execute([$idsession,$libellesemestre,$statut,$codeEtab,$next]);
   $_SESSION['user']['Updateadminok']="Nouvelle Session ajouté avec succès";
   if($_SESSION['user']['profile'] == "Admin_globale") {
  header("Location:../manager/sessions.php=");
    // header("Location:../manager/addclasses.php");
      }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/sessions.php");
        }else {
        header("Location:../locale/sessions.php");
        }

  }
}

function getAllSessionsOfThisSchoolCode($codeEtabAssigner)
{
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=?");
  $req->execute([$codeEtabAssigner]);
  return $req->fetchAll();
}

function ExisteSessionForThisSchool($titre,$codeEtab)
{
  $req = $this->db->prepare("SELECT * from sessions where libelle_sess=? and codeEtab_sess=?");
  $req->execute([$titre,$codeEtab]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function AddSessionSchool($libellesession,$codeEtab,$statut,$encours,$semestre,$datedeb,$datefin)
{
  $req = $this->db->prepare("INSERT INTO sessions SET libelle_sess=?,codeEtab_sess=?,statut_sess=?,encours_sess=?,type_sess=?,datedeb_sess=?,datefin_sess=?");
  $req->execute([$libellesession,$codeEtab,$statut,$encours,$semestre,$datedeb,$datefin]);
  $idlastsession=$this->db->lastInsertId();
  return $idlastsession;

}

function AddsessionPrimary($titre,$typesession,$codeEtab,$datedeb,$datefin,$statut,$encours)
{
  $req = $this->db->prepare("INSERT INTO sessions SET libelle_sess=?,type_sess=?,codeEtab_sess=?,datedeb_sess=?,datefin_sess=?,	statut_sess=?,encours_sess=?");
  $req->execute([$titre,$typesession,$codeEtab,$datedeb,$datefin,$statut,$encours]);



}


function getNumberOfSemestreDown($sessionid,$sessiontype,$codeEtab)
{
  $statutfinal=2;
  $req = $this->db->prepare("SELECT * from semestre where idsess_semes=? and codeEtab_semes=? and statut_semes=?");
  $req->execute([$sessionid,$codeEtab,$statutfinal]);
  $data=$req->fetchAll();
  $nb=count($data);
  $content="";
    //return $nb;
    if($sessiontype==2)
    {
      //il est question d'un semestre

      if($nb==2)
      {
        $content=1;
      }else if($nb<2)
      {
        $content=0;
      }

    }else if($sessiontype==3)
    {
      //il est question d'un trimestre

      if($nb==3)
      {
  $content=1;
      }else if($nb<3)
      {
  $content=0;
      }
    }

    return $content;
}

function getNbOfSemestreDown($sessionid,$sessiontype,$codeEtab)
{
  $statutfinal=2;
  $req = $this->db->prepare("SELECT * from semestre where idsess_semes=? and codeEtab_semes=? and statut_semes=?");
  $req->execute([$sessionid,$codeEtab,$statutfinal]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getNumberSessionEncoursOn($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $nb=count($data);
  return $nb;
}

function getActiveAllSemestrebyIdsession($sessionencoursid)
{
  $statut=1;
  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? and semestre.statut_semes=?");
  $req->execute([$sessionencoursid,$statut]);
  return $req->fetchAll();
}

function getActiveAllSemestrebyIdsessionAll($sessionencoursid)
{
  $statut=1;
  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? order by libelle_semes ASC");
  $req->execute([$sessionencoursid]);
  return $req->fetchAll();
}

function getAllsemestrebyIdsession($sessionencoursid)
{

  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? order by semestre.idsess_semes DESC");
  $req->execute([$sessionencoursid]);
  return $req->fetchAll();
}

function getActiveAllSemestrebyIdsessionOff($sessionencoursid)
{
  $statut=2;
  $statut1=3;
  $req = $this->db->prepare("SELECT * from sessions,semestre where semestre.idsess_semes=sessions.id_sess and sessions.id_sess=? and semestre.statut_semes in (?,?)");
  $req->execute([$sessionencoursid,$statut,$statut1]);
  return $req->fetchAll();
}

function getSessionEncours($codeEtabAssigner)
{
  $encours=1;
  $req = $this->db->prepare("SELECT * from sessions where codeEtab_sess=? and encours_sess=?");
  $req->execute([$codeEtabAssigner,$encours]);
  $data=$req->fetchAll();
  $array=json_encode($data,true);
  $someArray = json_decode($array, true);

$donnees=$someArray[0]["libelle_sess"]."*".$someArray[0]["id_sess"]."*".$someArray[0]["type_sess"];
return $donnees;
}

function getSessionEncoursLibelle($codeEtabAssigner)
{
$encours=1;
$req = $this->db->prepare("SELECT distinct libelle_sess from sessions where codeEtab_sess=? and encours_sess=?");
$req->execute([$codeEtabAssigner,$encours]);
return $req->fetchAll();

}





}

 ?>
