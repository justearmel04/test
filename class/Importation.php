<?php
// require '../src/ci/Osms.php';
// use \Osms\Osms;
class Import{

  public $db;
  function __construct() {
      require_once('../class/cnx.php');
    $db = new mysqlConnector();
    $this->db= $db->dataBase;
      }

      function UpdateImportationFileName($fichierad,$importationid)
      {
        $req = $this->db->prepare("UPDATE importation SET file_import=? where id_import=?");
        $req->execute([$fichierad,$importationid]);
      }

      function AddImportationFile($classeid,$codeEtab,$sessionEtab,$pays,$userid,$datatype)
      {
          $req = $this->db->prepare("INSERT INTO importation SET 	classe_import=?,codeEtab_import=?,sessionEtb_import=?,pays_import=?,user_import=?,type_import=?");
          $req->execute([$classeid,$codeEtab,$sessionEtab,$pays,$userid,$datatype]);
          $idlastcompte=$this->db->lastInsertId();
          return $idlastcompte;
      }

      function AddparentExcelFile($nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$situation,$nbchild,$sexe,$societe,$adressepostale,$telburo,$codeEtab,$dateday,$typeCompte,$statut,$login,$mdp)
      {
        $req1 = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,sexe_compte=?,societe_compte=?,adresse_compte=?,telBuro_compte=?,login_compte=?,pass_compte=?");
  $req1->execute([
      $nom,
      $prenom,
      $datenais,
      $mobile,
      $email,
      $fonction,
      $typeCompte,
      $statut,
      $dateday,
      $sexe,
      $societe,
      $adressepostale,
      $telburo,
      $login,
      $mdp
  ]);

    $idlastcompte=$this->db->lastInsertId();
  //
  $req = $this->db->prepare("INSERT INTO  parent SET nom_parent=?,prenom_parent=?,tel_parent=?,profession_parent=?,cni_parent=?,email_parent=?,statut_parent=?,sexe_parent=?,idcompte_parent=?,nationalite_parent=?,lieuH_parent=?,nbchild_parent=?,adressepro_parent=?,societe_parent=?,situation_parent=?,lieunais_parent=?,postaleadres_parent=?,phoneBuro_parent=?");
  $req->execute([
    $nom,
    $prenom,
    $mobile,
    $fonction,
    $cni,
    $email,
    $statut,
    $sexe,
    $idlastcompte,
    $nationalite,
    $lieuH,
    $nbchild,
    $adressepostale,
    $societe,
    $situation,
    $lieunais,
    $adressepostale,
    $telburo

  ]);


  //insertion dans la table enregistrer

    $reqY = $this->db->prepare("INSERT INTO  enregistrer SET idparent_enreg=?,codeEtab_enreg=?");
    $reqY->execute([
      $idlastcompte,
      $codeEtab
    ]);


return $idlastcompte;

      }


      function AddStudent($matricule,$nom,$prenom,$datenais,$lieunais,$mobile,$email,$fonction,$cni,$nationalite,$lieuH,$sexe,$adressepostale,$doublant,$daffecter,$classe,$codeEtab,$sessionEtab,$typeCompte,$dateday,$statut)
      {


        $reqX = $this->db->prepare("INSERT INTO  compte SET nom_compte=?,prenom_compte=?,datenais_compte=?,tel_compte=?,email_compte=?,fonction_compte=?,type_compte=?,statut_compte=?,datecrea_compte=?,adresse_compte=?");
        $reqX->execute([
        $nom,
        $prenom,
        $datenais,
        $mobile,
        $email,
        $fonction,
        $typeCompte,
        $statut,
        $dateday,
        $adressepostale
        ]);

        // ajout des données dans la table inscription

        $idlastcompte=$this->db->lastInsertId();

        $req = $this->db->prepare("INSERT INTO  eleve SET matricule_eleve=?,nom_eleve=?,prenom_eleve=?,datenais_eleve=?,lieunais_eleve=?,sexe_eleve=?,email_eleve=?,codeEtab_eleve=?,idcompte_eleve=?,affecter_Id_eleve=?,doublant_Id_eleve=? ");
        $req->execute([
          $matricule,
          $nom,
          $prenom,
          $datenais,
          $lieunais,
          $sexe,
          $email,
          $codeEtab,
          $idlastcompte,
          $daffecter,
          $doublant
        ]);

        $reqY = $this->db->prepare("INSERT INTO  inscription SET 	idclasse_inscrip=?,	ideleve_inscrip=?,session_inscrip=?,date_inscrip=?,codeEtab_inscrip=?");

        $reqY->execute([
          $classe,
          $idlastcompte,
          $sessionEtab,
          $dateday,
          $codeEtab
        ]);



        return $idlastcompte;


      }

      function Addparenter($idparent,$studentid)
      {
        $req= $this->db->prepare("INSERT INTO  parenter SET parentid_parenter=?,eleveid_parenter=?");
        $req->execute([$idparent,$studentid]);

      }

      function parentphonechecker($telephone)
      {
        $req = $this->db->prepare("SELECT * FROM parent where tel_parent=?");
        $req->execute([$telephone]);
        $data=$req->fetchAll();
        $nb=count($data);
        return $nb;
      }






}

?>
