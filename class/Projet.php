<?php
class Projet{
public $db;
function __construct() {
  require_once('../class/cnx.php');
  $db = new mysqlConnector();
  $this->db= $db->dataBase;
}

function Addprojet($date_deb,$date_fin,$title,$content,$codeEtab,$sessionEtab,$teatcher,$status)
  {
    // $date_deb=date("d-m-Y");
    // $date_fin=date("d-m-Y");
    $req = $this->db->prepare("INSERT INTO projets SET date_deb=?, date_fin=?, projet_title=?,projet_content=?,code_etab=?, session_etab=?,id_enseignant=?,projet_status=?");
    $req->execute([$date_deb,$date_fin,$title,$content,$codeEtab,$sessionEtab,$teatcher,$status]);
    $_SESSION['user']['addprojetok']="Projet ajouté avec succès";
  }

function getProjetbyteacher($codeEtabsession,$libellesessionencours,$compteuserid){
  $req = $this->db->prepare("SELECT DISTINCT nom_compte, prenom_compte, id_projet, date_deb,date_fin, projet_title, projet_content,id_enseignant,projet_status,session_etab,code_etab FROM compte, projets WHERE projets.code_etab=? and projets.session_etab=? and compte.id_compte=projets.id_enseignant and projets.id_enseignant=?");
  $req->execute([$codeEtabsession,$libellesessionencours,$compteuserid]);
  return $req->fetchAll();
}

function getAllprojetdetails($idProj,$codeEtabsession,$libellesessionencours,$idTeach){
  $req = $this->db->prepare("SELECT * FROM projets WHERE projets.id_projet=? and projets.code_etab=? and projets.session_etab=? and projets.id_enseignant= ?");
  $req->execute([$idProj,$codeEtabsession,$libellesessionencours,$idTeach]);
  return $req->fetchAll();
}

function Updateprojets($date_deb,$date_fin,$title,$content,$codeEtab,$sessionEtab,$teatcher,$status,$idProj,$profid)
  {
    $req = $this->db->prepare("UPDATE projets SET date_deb=?, date_fin=?, projet_title=?,projet_content=?,code_etab=?, session_etab=?,id_enseignant=?,projet_status=? WHERE id_projet=? and id_enseignant= ?");
    $req->execute([$date_deb,$date_fin,$title,$content,$codeEtab,$sessionEtab,$teatcher,$status,$idProj,$profid]);
    $_SESSION['user']['addprojetok']="Projet ajouté avec succès";
  }


}

?>
