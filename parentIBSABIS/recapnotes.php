<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}


$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();



$emailUti=$_SESSION['user']['email'];



$user=new User();

$etabs=new Etab();

$localadmins= new Localadmin();

$parents=new ParentX();

$classe=new Classe();
$student=new Student();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);

$datastat=$user->getStatis();

$tabstat=explode("*",$datastat);

// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}


$parentlyStudent=$parents->getDifferentStudentByParentId($_SESSION['user']['IdCompte']);



$concatcodeEtab="";
$concatclasseids="";

foreach ($parentlyStudent as $valuex):
  $datas=$parents->getStudentdatas($valuex->id_compte);

  foreach ($datas as $valuez):
    $concatcodeEtab=$concatcodeEtab.$valuez->codeEtab_inscrip.",";
    $concatclasseids=$concatclasseids.$valuez->idclasse_inscrip.",";
  endforeach;

endforeach;


$concatcodeEtab=substr($concatcodeEtab, 0, -1);
$concatclasseids=substr($concatclasseids, 0, -1);



// $alletab=$etabs->getAllEtab();

// $locals=$localadmins->getAllAdminLocal();

// $allparents=$parents->getAllParent();


$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);



 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::AcquisEvaluation  ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::AcquisEvaluation  ?></li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="card card-topline-green">
                          <div class="card-head">
                              <header></header>
                              <div class="tools">
                                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                   <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                   <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                              </div>
                          </div>
                          <div class="card-body ">
                            <form method="post" id="recapnotes" action="recapnotes.php">
                                <div class="row">

                                  <div class="col-md-6 col-sm-6">
                                  <!-- text input -->
                                  <div class="form-group" style="margin-top:8px;">
                                      <label><?php echo L::EleveMenusingle ?></label>
                                      <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                      <select class="form-control " id="eleveid" name="eleveid" style="width:100%;">
                                        <option value=""><?php echo L::SelectOneStudent ?></option>

                                        <?php

                                        foreach ($parentlyStudent as $value):

                                         ?>

                                         <option value="<?php echo $value->id_compte; ?>"><?php echo utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)) ?></option>

                                         <?php

                                       endforeach;

                                          ?>


                                      </select>
                                  </div>


                              </div>

                              <div class="col-md-6 col-sm-6">
                              <!-- text input -->
                              <div class="form-group" style="margin-top:8px;">
                                <label><?php echo L::MatiereMenusingle ?></label>
                                  <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                  <select class="form-control " id="matclasse" name="matclasse" style="width:100%;" onchange="determinetrimestre()">
                                    <option value=""><?php echo L::SelectSubjects ?></option>


                                  </select>
                              </div>


                          </div>
                          <div class="col-md-6 col-sm-6">
                          <!-- text input -->
                          <div class="form-group" style="margin-top:8px;">
                              <label><?php echo L::Trimestre ?></label>
                              <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->

                              <select class="form-control " id="typesess" name="typesess" style="width:100%" >
                                  <option value=""><?php echo L::selectTrimestre ?></option>


                              </select>
                              <input type="hidden" id="codeEtab" name="codeEtab" value="">
                              <input type="hidden" id="sessionEtab" name="sessionEtab" value="">
                          </div>


                      </div>

                          <!-- <div class="col-md-6 col-sm-6">

                          <div class="form-group" style="margin-top:8px;">
                              <label><?php echo L::Designation ?></label>

                              <select class="form-control " id="libctrl" name="libctrl" style="width:100%;">
                                  <option value=""><?php echo L::DesignationSelected ?></option>

                              </select>
                          </div>


                      </div> -->


                              <div class="col-md-3 col-sm-3">
                              <!-- text input -->
                              <!--div class="form-group">
                                  <label style="margin-top:3px;">Date</label>
                                  <input type="text" id="datepre" name ="datepre" class="floating-label mdl-textfield__input"  placeholder="Date présence">
                                  <input type="hidden" name="search" id="search" />
                              </div-->
                              <input type="hidden" name="search" id="search" />
                              <button type="submit" class="btn btn-success"  style="width:200px;height:35px;margin-top:35px;text-align:center;"><?php echo L::Displaying ?></button>


                          </div>


                                </div>


                            </form>
                          </div>
                      </div>
                               </div>

                 </div>


                       <div class="row" style="" id="affichage">



                          <div class="offset-md-4 col-md-4"  id="affichage1">
                            <div class="card" style="">
                            <div class="card-body">
                              <h5 class="card-title"></h5>
                              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;"><?php echo L::AcquisEvaluation  ?></h4>
                              <p class="card-text" style="text-align:center;font-weight: bold;"><?php //echo $classeInfos; ?></p>
                              <p class="card-text" style="text-align:center;"></p>

                            </div>
                          </div>
                          </div>

                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                                  <div class="card card-topline-green">
                                                      <div class="card-head">
                                                          <header><?php //echo L::ListNotesClasse ?> <?php //echo $classeInfos; ?></header>
                                                          <div class="tools">
                                                              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                      <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                      <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                          </div>
                                                      </div>
                                                      <div class="card-body ">

                                                        <form method="post" action="#" id="FormAttendance">
                                                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"  id="affichage3">
                                                    <thead>
                                                        <tr>
                                                          <th style="width:30%;text-align:center;"> <?php echo L::NamestudentTab ?> </th>
                                                          <th style="width:30%;text-align:center;"> <?php echo L::MatiereMenu ?> </th>
                                                          <th style="text-align:center;width:30%"> <?php echo L::Evaluation ?> </th>
                                                          <th style="width:20%;text-align:center"> <?php echo L::Notelibelle ?> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                      if(isset($_POST['search']))
                                                      {

                                                          if(isset($_POST['eleveid'])&&strlen($_POST['eleveid'])>0)
                                                          {
                                                            if(isset($_POST['matclasse'])&&strlen($_POST['matclasse'])>0)
                                                            {
                                                              if(isset($_POST['typesess'])&&strlen($_POST['typesess'])>0)
                                                              {
                                                                $studentid=$_POST['eleveid'];
                                                                $matiereid=$_POST['matclasse'];
                                                                $trimestreid=$_POST['typesess'];
                                                                $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);
                                                                foreach ($infos as $valueI):
                                                                  $infoscodeEtab=$valueI->codeEtab_inscrip;
                                                                  $infossessionEtab=$valueI->session_inscrip;
                                                                  $infosclasseEtab=$valueI->idclasse_inscrip;

                                                                  $datas=$student->getLastnotesClassesMatieresTrimestre($studentid,$infosclasseEtab,$matiereid,$trimestreid,$infoscodeEtab,$infossessionEtab);
                                                                  foreach ($datas as $value):
                                                                    ?>
                                                                    <tr class="odd gradeX">

                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->nom_eleve." ".$value->prenom_eleve;?>
                                                                      </td>
                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->libelle_mat;?>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                      <!-- <span class="label label-primary label-md"><?php //echo $value->libelle_notes ?></span> -->
                                                                      <?php echo $value->libelle_notes ?>



                                                                            <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                        <?php echo $value->valeur_notes;?>

                            <!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                                                          <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>

                                                                    </tr>
                                                                    <?php
                                                                  endforeach;
                                                                endforeach;
                                                              }else {
                                                                $studentid=$_POST['eleveid'];
                                                                $matiereid=$_POST['matclasse'];
                                                                $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);
                                                                foreach ($infos as $valueI):
                                                                  $infoscodeEtab=$valueI->codeEtab_inscrip;
                                                                  $infossessionEtab=$valueI->session_inscrip;
                                                                  $infosclasseEtab=$valueI->idclasse_inscrip;
                                                                  $datas=$student->getLastnotesClassesMatieres($studentid,$infosclasseEtab,$matiereid,$infoscodeEtab,$infossessionEtab);
                                                                  foreach ($datas as $value):
                                                                    ?>
                                                                    <tr class="odd gradeX">

                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->nom_eleve." ".$value->prenom_eleve;?>
                                                                      </td>
                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->libelle_mat;?>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                      <!-- <span class="label label-primary label-md"><?php //echo $value->libelle_notes ?></span> -->
                                                                      <?php echo $value->libelle_notes ?>



                                                                            <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                        <?php echo $value->valeur_notes;?>

                            <!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                                                          <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>

                                                                    </tr>
                                                                    <?php
                                                                  endforeach;
                                                                endforeach;

                                                              }
                                                            }else {
                                                              if(isset($_POST['typesess'])&&strlen($_POST['typesess'])>0)
                                                              {
                                                                //les notes de toutes les matières pour ce trimestre
                                                                $studentid=$_POST['eleveid'];
                                                                $trimestreid=$_POST['typesess'];

                                                                $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);
                                                                foreach ($infos as $valueI):
                                                                  $infoscodeEtab=$valueI->codeEtab_inscrip;
                                                                  $infossessionEtab=$valueI->session_inscrip;
                                                                  $infosclasseEtab=$valueI->idclasse_inscrip;
                                                                  $datas=$student->getLastnotesClassesAllMatieresTrimestre($studentid,$infosclasseEtab,$trimestreid,$infoscodeEtab,$infossessionEtab);
                                                                  foreach ($datas as $value):
                                                                    ?>
                                                                    <tr class="odd gradeX">

                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->nom_eleve." ".$value->prenom_eleve;?>
                                                                      </td>
                                                                      <td style="text-align:center">
                                                                          <!-- <a href="#"> </a> -->
                                                                          <?php echo $value->libelle_mat;?>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                      <!-- <span class="label label-primary label-md"><?php //echo $value->libelle_notes ?></span> -->
                                                                      <?php echo $value->libelle_notes ?>



                                                                            <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>
                                                                      <td style="text-align:center">

                                                                        <?php echo $value->valeur_notes;?>

                            <!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                                                          <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                                      </td>

                                                                    </tr>
                                                                    <?php
                                                                  endforeach;

                                                                endforeach;

                                                              }else {
                                                                // les notes de toutes les matières
                                                                $studentid=$_POST['eleveid'];
                                                                $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);
                                                                  foreach ($infos as $valueI):
                                                                    $infoscodeEtab=$valueI->codeEtab_inscrip;
                                                                    $infossessionEtab=$valueI->session_inscrip;
                                                                    $infosclasseEtab=$valueI->idclasse_inscrip;
                                                                    $datas=$student->getLastnotesClassesAllMatieres($studentid,$infosclasseEtab,$infoscodeEtab,$infossessionEtab);
                                                                    foreach ($datas as $value):
                                                                      ?>
                                                                      <tr class="odd gradeX">

                                                                        <td style="text-align:center">
                                                                            <!-- <a href="#"> </a> -->
                                                                            <?php echo $value->nom_eleve." ".$value->prenom_eleve;?>
                                                                        </td>
                                                                        <td style="text-align:center">
                                                                            <!-- <a href="#"> </a> -->
                                                                            <?php echo $value->libelle_mat;?>
                                                                        </td>
                                                                        <td style="text-align:center">

                                                                        <!-- <span class="label label-primary label-md"><?php //echo $value->libelle_notes ?></span> -->
                                                                        <?php echo $value->libelle_notes ?>



                                                                              <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                                        </td>
                                                                        <td style="text-align:center">

                                                                          <?php echo $value->valeur_notes;?>

                              <!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                                                            <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                                        </td>

                                                                      </tr>
                                                                      <?php
                                                                    endforeach;
                                                                  endforeach;
                                                              }
                                                            }

                                                          }else {
                                                            // pour tous les élèves

                                                            if(isset($_POST['matclasse'])&&strlen($_POST['matclasse'])>0)
                                                            {
                                                              //pour une matière précise
                                                              if(isset($_POST['typesess'])&&strlen($_POST['typesess'])>0)
                                                              {

                                                              }else {
                                                                // code...
                                                              }
                                                            }else {
                                                              if(isset($_POST['typesess'])&&strlen($_POST['typesess'])>0)
                                                              {

                                                              }else {
                                                                // code...
                                                              }
                                                            }

                                                          }
                                                        // }

                                                      }else {

                                                          // $datas=$student->getAllsnotesofStudentinThisMatieres($_POST['classeEtab'],$_SESSION['user']['codeEtab'],$libellesessionencours,$_POST['matclasse'],$_POST['typesess']);
                                                            // $datas=$parents->getAllsnotesofparentchilds($_SESSION['user']['IdCompte']);

                                                            foreach ($parentlyStudent as $values):
                                                              $studentid=$value->id_compte;

                                                              $infos=$parents->getStudentCurrentlyinscription($_SESSION['user']['IdCompte'],$studentid);

                                                              foreach ($infos as $valueI):
                                                                $infoscodeEtab=$valueI->codeEtab_inscrip;
                                                                $infossessionEtab=$valueI->session_inscrip;
                                                                $infosclasseEtab=$valueI->idclasse_inscrip;

                                                                $datas=$student->getLastnotesClasses($studentid,$infosclasseEtab,$infoscodeEtab,$infossessionEtab);
                                                              foreach ($datas as $value):
                                                                ?>
                                                                <tr class="odd gradeX">

                                                                  <td style="text-align:center">
                                                                      <!-- <a href="#"> </a> -->
                                                                      <?php echo $value->nom_eleve." ".$value->prenom_eleve;?>
                                                                  </td>
                                                                  <td style="text-align:center">
                                                                      <!-- <a href="#"> </a> -->
                                                                      <?php echo $value->libelle_mat;?>
                                                                  </td>
                                                                  <td style="text-align:center">

                                                                  <!-- <span class="label label-primary label-md"><?php //echo $value->libelle_notes ?></span> -->
                                                                  <?php echo $value->libelle_notes ?>



                                                                        <p id="messageObservE<?php echo $value->idcompte_eleve;?>"></p>
                                                                  </td>
                                                                  <td style="text-align:center">

                                                                    <?php echo $value->valeur_notes;?>

                        <!-- <input style='text-align:center' type="number" min=0 max=20 name="noteE<?php echo $value->idcompte_eleve;?>" id="noteE<?php echo $value->idcompte_eleve;?>" value="<?php echo $value->valeur_notes;?>" style="width:100px" onclick="erasedNote(<?php echo $value->idcompte_eleve;?>)" readonly/> -->



                                                                      <p id="messageNoteE<?php echo $value->idcompte_eleve;?>"></p>
                                                                  </td>

                                                                </tr>
                                                                <?php
                                                              endforeach;

                                                              endforeach;

                                                            endforeach;

                                                      }
                                                       ?>


                                                    </tbody>
                                                  </table>



                                                  <center>


                                                  </center>
                                                </form>

                                              </div>
                                                  </div>
                                              </div>



                       </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 	<script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
    <!--Chart JS-->
    <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
    <script src="../assets2/plugins/chart-js/utils.js" ></script>
    <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
    $("#matclasse").select2();
    $("#notetype").select2();
    $("#libctrl").select2();
    $("#libsemes").select2();
    $("#eleveid").select2();
    $("#typesess").select2();

    function addFrench()
    {
      var etape=1;
      var lang="fr";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function addEnglish()
    {
      var etape=1;
      var lang="en";
      $.ajax({
        url: '../ajax/langue.php',
        type: 'POST',
        async:false,
        data: 'etape=' + etape+ '&lang=' +lang,
        dataType: 'text',
        success: function (content, statut) {

    window.location.reload();

        }
      });
    }

    function refreshing()
    {

    }



    function determinetrimestre()
    {
      var matiere=$("#recapnotes #matclasse").val();
      var etape=1;
      $.ajax({
        url: '../ajax/trimestre.php',
        type: 'POST',
        async:true,
        data: 'matiereid=' + matiere+ '&etape=' + etape,
        dataType: 'text',
        success: function (content, statut) {
          var trimestre=content.split("*")[0];
          var codeEtab=content.split("*")[1];
          var sessionEtab=content.split("*")[2];
          var classeEtab=content.split("*")[3];
          $("#typesess").html("");
          $("#typesess").html(trimestre);

          $("#recapnotes #codeEtab").val(codeEtab);
          $("#recapnotes #sessionEtab").val(sessionEtab);

          var etape=2;
          $.ajax({
            url: '../ajax/trimestre.php',
            type: 'POST',
            async:true,
            data: 'matiereid=' + matiere+ '&etape=' + etape+ '&classeid=' + classeEtab+ '&codeEtab=' + codeEtab+ '&sessionEtab=' + sessionEtab,
            dataType: 'text',
            success: function (content, statut) {

            }
          });

        }
      });
    }

    function getallmatiereClasses()
    {
      var classeid="<?php echo $concatclasseids ?>";
      // var compteid="<?php echo $_SESSION['user']['IdCompte']; ?>"
      // var etape=25;
      var etape=24;
      $.ajax({
        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'classeid=' + classeid+ '&etape=' + etape,
        dataType: 'text',
        success: function (content, statut) {


          $("#matclasse").html("");
          $("#matclasse").html(content);

        }
      });
      // $.ajax({
      //   url: '../ajax/matiere.php',
      //   type: 'POST',
      //   async:true,
      //   data: 'compteid=' + compteid+ '&etape=' + etape,
      //   dataType: 'text',
      //   success: function (content, statut) {
      //
      //
      //     $("#matclasse").html("");
      //     $("#matclasse").html(content);
      //
      //   }
      // });
    }


   $(document).ready(function() {

     getallmatiereClasses();

     $("#recapnotes").validate({
       errorPlacement: function(label, element) {
       label.addClass('mt-2 text-danger');
       label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
       $(element).parent().addClass('has-danger')
       $(element).addClass('form-control-danger')
      },
      success: function (e) {
           $(e).closest('.control-group').removeClass('error').addClass('info');
           $(e).remove();
       },
       rules:{
          // matclasse:"required",
          notetype:"required",
          libsemes:"required",
          libctrl:"required"
       },
       messages: {
         // matclasse:"<?php //echo L::PleaseselectSubjects ?>",
         notetype:"Merci de selectionner le type de Note",
         libsemes:"<?php echo L::pleaseselecttrimestre ?>",
         libctrl:"<?php echo L::PleaseselectCompetence ?>"
       },
       submitHandler: function(form) {
         form.submit();
       }

     });



   });

   </script>
    <!-- end js include path -->
  </body>

</html>
