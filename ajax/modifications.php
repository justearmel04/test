<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$matiere= new Matiere();
$etabs=new Etab();
$classex= new Classe();
$student= new Student();



if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$matiereid=htmlspecialchars($_POST['matiere']);
$classeid=htmlspecialchars($_POST['classe']);
$newnote=htmlspecialchars($_POST['newnote']);
$oldnote=htmlspecialchars($_POST['oldnote']);
$noteid=htmlspecialchars($_POST['notesid']);
$typesessionEtab=htmlspecialchars($_POST['trimestre']);
$eleveid=htmlspecialchars($_POST['ideleve']);
$matricule=htmlspecialchars($_POST['matricule']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$typenote=1;
$userid=$_SESSION['user']['IdCompte'];
//nous allons chercher les élements de la note a modifier

$observation=getObservationsLib($newnote);

$notesdatas=$etabs->getnotescrontroleInformationsecondaryOne($noteid,$eleveid,$codeEtab,$typenote);

$tabnotesdatas=explode("*",$notesdatas);
$idtypenote=htmlspecialchars($tabnotesdatas[0]);
$teatcherid=htmlspecialchars($tabnotesdatas[3]);
$coefficientNotes=htmlspecialchars($tabnotesdatas[6]);;

//recuperation des informations sur la moyenne dans la matiere

//nous allons recuperer le totalnotes,totalnotescoef,totalcoef

$LastratingInfos=$etabs->getRatingtypesessionInfosNewOne($eleveid,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeid,$typesessionEtab);
foreach ($LastratingInfos as $values):
       // $tabLastInfos=explode("*",$LastratingInfos);
       $alltotalnotes=$values->totalnotes_rating;
       $alltotalnotescoef=$values->totalnotescoef_rating;
       $totalcoefnotes=$values->totalcoef_rating;
       $ratingId=$values->id_rating;
     endforeach;

     //nouvelles Informations

     $sommesNotes=$alltotalnotes+$newnote;
     $sommesNotescoef=$alltotalnotescoef+($newnote*$coefficientNotes);
     $sommescoef=$totalcoefnotes+$coefficientNotes;
     $moyenne=$sommesNotescoef/$sommescoef;

     $datemodif=date("Y-m-d");

     $etabs->UpdateRatingStudent($sessionEtab,$typesessionEtab,$eleveid,$classeid,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

     $firstnote=1;
     $statut=2;

     // $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$newnote,$eleveid);
     // $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
     // $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);

     $student->UpdateNotesOfStudentOne($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation,$sessionEtab,$typesessionEtab,$matiereid,$teatcherid);

     $etabs->RecalculstudentRang($classeid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$typesessionEtab);




// echo $content;
}



 ?>
