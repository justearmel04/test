<?php
session_start();
require_once('../class/Teatcher.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$teatcher= new Teatcher();
$userteatch=new User();
$etabs=new Etab();
$students=new Student();
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  $content="";

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $datas=$students->Listeinscritsthisyear($codeEtab,$sessionEtab);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::Nostudent." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SelectOneStudent."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  $content="";
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $userid=htmlspecialchars($_POST['userid']);

  $datas=$students->ListeinscritsthisyearEtabs($userid,$sessionEtab);
  
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::Nostudent." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SelectOneStudent."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_compte ."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}
 ?>
