<?php
session_start();
require_once('../class/Parent.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$parent= new ParentX();
$userparent=new User();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
$content="";
//recupération des variables

$login=htmlspecialchars(addslashes($_POST['login']));
$email=htmlspecialchars(addslashes($_POST['email']));
$cni=htmlspecialchars(addslashes($_POST['cni']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));

$loginstatus=$userparent->CheckingLoginCni($login,$cni);

if($loginstatus>0)
{
  // $content=1;
//nous allons verifier si c'est le login ou le mot de passe

$statuslogin=$userparent->CheckingLogin($login);
if($statuslogin>0)
{
  $content=1;
}else if($statuslogin==0)
{
  $content=2;
}


}else if($loginstatus==0)
{
  $content=0;
}

//nous allons verifier si un parent existe deja avec ce numéro cni

// $checkcni=$parent->ExistwithCniId($cni,$codeEtab);
//
// if($checkcni==1)
// {
//     $content=1;
//     //un parent existe avec cette cni et est enregistrer dans cet Etablissement
// }else {
//   //nous allons verifier si un parent existe deja avec cette adresse email
//
//   /*$checkparentmail=$parent->ExistParentwithMail($email);
//
//   if($checkparentmail==1)
//   {
//     $content=2;
//     //nous avons un parent avec cette adresse email
//   }else {
//     //nous allns verifier si nous avons un compte avec cette adresse email
//
//     $parentcptebymail=$parent->parentcptemail($email);
//
//     if($parentcptebymail==1)
//     {
//       $content=3;
//       //cette adresse email existe deja dans le système
//     }else {
//         $content=0;
//
//     }
//
//   }*/
//
//   $content=0;
// }

  echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['code']));
  $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
  $content="";

  $content.="<option selected value=''>".L::PleaseSelectparent."</option>";

  $data=$parent->getSelectionParentBySchoolCode($codeEtab,$idcompte);

  foreach ($data as $value):
$content .= "<option  value='". utf8_encode(utf8_decode($value->id_compte))."' >" . utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)). "</option>";

endforeach;

echo $content;




}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recupération desvariables

  $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));

  $content="";

  $content.="<option selected value=''>".L::PleaseSelectparent."</option>";

  $data=$parent->getSelectionParentById($idcompte);

  foreach ($data as $value):
$content .= "<option  value='". utf8_encode(utf8_decode($value->id_compte))."' >" . utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)). "</option>";

endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $codeEtab=htmlspecialchars(addslashes($_POST['code']));
  $idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
  $content="";

  $datas=$parent->determineParentOfStudent($idcompte,$codeEtab);

  $nbligne=count($datas);
  if($nbligne==0)
  {
$content.="<option selected value=''>".L::Noparent."</option>";
  }else {
    $content.="<option selected value=''>".L::PleaseSelectparent."</option>";

    foreach ($datas as $value):
  $content .= "<option  value='". utf8_encode(utf8_decode($value->id_compte))."' >" . utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)). "</option>";

  endforeach;

  }

  echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $phonenumber=htmlspecialchars($_POST['phonenumber']);

  $datas=$parent->determineParentOfStudentbymobilephone($phonenumber,$codeEtab);

  $nbligne=count($datas);

  echo $nbligne;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
$nom=htmlspecialchars($_POST['nom']);
$telephone=htmlspecialchars($_POST['telephone']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$concatoldparents=htmlspecialchars($_POST['concatoldparents']);
$concatoldparents=substr($concatoldparents, 0, -1);
$tabolds=explode("@",$concatoldparents);
$nbtabols=count($tabolds);
$content="";

if(strlen($nom)>0&&strlen($telephone)>0)
{
  //recherche par le nom et par le telephone

  //nous allons compter le nombre de parent avec ce numero de telephone dans le systeme

  $datas=$parent->getAllparentsbyNameTel($nom,$telephone,$codeEtab);

}else {
  if(strlen($nom)>0&&strlen($telephone)==0)
  {
    // echo "nom";
    $datas=$parent->getAllparentsbyName($nom,$codeEtab);
    // echo $datas;
  }else if(strlen($nom)==0&&strlen($telephone)>0)
  {
    // echo "telephone</br>";
    $datas=$parent->getAllparentsbyTel($telephone,$codeEtab);
  }
}

// var_dump($datas);

if(count($datas)==0)
{
  //nous allons rechercher dans tout le système

  if(strlen($nom)>0&&strlen($telephone)>0)
  {
    $datas=$parent->getAllparentsWiththisTelOrName($nom,$telephone);
    $nbdep=count($datas);
    if($nbdep==0)
    {
      $content.="<tr>";
      $content.="<td colspan=\"4\">".L::NoLigne."</td>";
      $content.="</tr>";
    }else {
      foreach ($datas as $value):

        if($nbtabols>0)
        {
          if (in_array($value->id_compte,$tabolds))
          {

          }else {
            $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
            $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
            $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
            $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";

            if(strlen($value->email_compte)>0)
            {
                $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
            }else {
                 $content.="<td></td>";
            }


            $content.="</tr>";
          }
        }else {
          $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
          $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
          $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
          $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";
          if(strlen($value->email_compte)>0)
          {
              $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
          }else {
               $content.="<td></td>";
          }
          $content.="</tr>";
        }


      endforeach;
    }
  }else {
    if(strlen($nom)>0&&strlen($telephone)==0)
    {
      // echo "nom";
      $datas=$parent->getAllparentsbyNameEqual($nom,$codeEtab);
      // echo $datas;
    }else if(strlen($nom)==0&&strlen($telephone)>0)
    {
      // echo "telephone</br>";
      $datas=$parent->getAllparentsbyTelEqual($telephone,$codeEtab);
    }

    $nbdep=count($datas);
    if($nbdep==0)
    {
      $content.="<tr>";
      $content.="<td colspan=\"4\">".L::NoLigne."</td>";
      $content.="</tr>";
    }else {
      foreach ($datas as $value):
        if($nbtabols>0)
        {
          if (in_array($value->id_compte,$tabolds))
          {

          }else {
            $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
            $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
            $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
            $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";
            if(strlen($value->email_compte)>0)
            {
                $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
            }else {
                 $content.="<td></td>";
            }
            $content.="</tr>";
          }
        }else {
          $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
          $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
          $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
          $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";
          if(strlen($value->email_compte)>0)
          {
              $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
          }else {
               $content.="<td></td>";
          }
          $content.="</tr>";
        }
      endforeach;
    }

  }



}else {
  foreach ($datas as $value):
    if($nbtabols>0)
    {
      if (in_array($value->id_compte,$tabolds))
      {

      }else {
        $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
        $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
        $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
        $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";
        $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
        $content.="</tr>";
      }
    }else {
      $content.="<tr ondblclick=\"afficherInfos(".$value->id_compte.")\">";
      $content.="<td><span class=\"label label-md label-info\">".$value->nom_compte."</span></td>";
      $content.="<td><span class=\"label label-md label-info\">".$value->prenom_compte."</span></td>";
      $content.="<td><span class=\"label label-md label-info\">".$value->tel_compte."</span></td>";
      $content.="<td><span class=\"label label-md label-info\">".$value->email_compte."</span></td>";
      $content.="</tr>";
    }
  endforeach;
}
  echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{

$userid=htmlspecialchars($_POST['userid']);
$idcompte=htmlspecialchars(addslashes($_POST['idcompte']));
$content="";

$datas=$parent->determineParentOfStudentEtabs($idcompte,$userid);


$nbligne=count($datas);
if($nbligne==0)
{
$content.="<option selected value=''>".L::Noparent."</option>";
}else {
  $content.="<option selected value=''>".L::PleaseSelectparent."</option>";

  foreach ($datas as $value):
$content .= "<option  value='". utf8_encode(utf8_decode($value->id_compte))."' >" . utf8_encode(utf8_decode($value->nom_compte." ".$value->prenom_compte)). "</option>";

endforeach;

}

echo $content;


}


 ?>
