<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$objectif=htmlspecialchars($_POST['objectif']);
$check=$etabs->ExistesyllabusObjectifs($syllabusid,$objectif);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Addobjectisyllabus($syllabusid,$objectif,$dateday);

$_SESSION['user']['addclasseok']=L::AnObjectifsuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$requis=htmlspecialchars($_POST['requis']);
$check=$etabs->ExistesyllabusRequis($syllabusid,$requis);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Addthemesyllabus($syllabusid,$requis,$dateday);

$_SESSION['user']['addclasseok']=L::Aprerequissuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$contenu=htmlspecialchars($_POST['contenu']);
$check=$etabs->ExistesyllabusThemes($syllabusid,$contenu);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Addthemesyllabus($syllabusid,$contenu,$dateday);

$_SESSION['user']['addclasseok']=L::Acontenussuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$competence=htmlspecialchars($_POST['competence']);
$check=$etabs->ExistesyllabusCompetences($syllabusid,$competence);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Addcompsyllabus($syllabusid,$competence,$dateday);

$_SESSION['user']['addclasseok']=L::NewCompetenceAddsuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$newdate=htmlspecialchars(dateFormat($_POST['newdate']));
$newseance=htmlspecialchars($_POST['newseance']);
$newseancecont=htmlspecialchars($_POST['newseancecont']);
$newseanceprea=htmlspecialchars($_POST['newseanceprea']);

$check=$etabs->ExistesyllabusCalanedars($syllabusid,$newdate,$newseance);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Addcalendarsyllabus($syllabusid,$newdate,$newseance,$newseancecont,$newseanceprea,$dateday);

$_SESSION['user']['addclasseok']=L::AnewdateAddsuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$regle=htmlspecialchars(dateFormat($_POST['regle']));


$check=$etabs->ExistesyllabusRegles($syllabusid,$regle);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->AddReglesyllabus($syllabusid,$regle,$dateday);

$_SESSION['user']['addclasseok']=L::AnewregleAddsuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{

//recupération des variables

$content="";

$syllabusid=htmlspecialchars($_POST['syllabusid']);
$newdoc=htmlspecialchars($_POST['newdoc']);
$typedocs=htmlspecialchars($_POST['typedocs']);


$check=$etabs->ExistesyllabusDocuments($syllabusid,$newdoc,$typedocs);
$dateday=date("Y-m-d");

if($check==0)
{
  $content=0;



}else if($check>0)
{
$content=1;
}

if($content==0)
{
  //nous allons ajouter ces infromation dans le système

$etabs->Adddocfacsyllabus($syllabusid,$newdoc,$typedocs,$dateday);

$_SESSION['user']['addclasseok']=L::AnewdocAddsuccessfully;

}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $requis=htmlspecialchars($_POST['requis']);
  $dateday=date("Y-m-d");
  $content="";
  $check=$etabs->ExistesyllabusRequis($syllabusid,$requis);

  if($check==0)
  {
    $content=0;



  }else if($check>0)
  {
  $content=1;
  }

  $etabs->Addrequissyllabus($syllabusid,$requis,$dateday);
  $_SESSION['user']['addclasseok']=L::Aprerequissuccessfully;
}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $competence=htmlspecialchars($_POST['competence']);
  $dateday=date("Y-m-d");
  $content="";
  $check=$etabs->ExistesyllabusCompetences($syllabusid,$competence);

  if($check==0)
  {
    $content=0;



  }else if($check>0)
  {
  $content=1;
  }

  $etabs->Addcompsyllabus($syllabusid,$competence,$dateday);
  $_SESSION['user']['addclasseok']=L::NewcompetencesAddedOk;
}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $ojectifid=htmlspecialchars($_POST['ojectifid']);
  $etabs->DeletedObjectives($syllabusid,$ojectifid);
  $_SESSION['user']['addclasseok']=L::AnObjectifDeletesuccessfully;
}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $contenuid=htmlspecialchars($_POST['contenuid']);
  $etabs->DeletedContenus($syllabusid,$contenuid);
  $_SESSION['user']['addclasseok']=L::AnContenuDeletesuccessfully;
}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $requisid=htmlspecialchars($_POST['requisid']);
  $etabs->DeletedRequis($syllabusid,$requisid);
  $_SESSION['user']['addclasseok']=L::AnprerequisDeletesuccessfully;
}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{
  $syllabusid=htmlspecialchars($_POST['syllabusid']);
  $compid=htmlspecialchars($_POST['compid']);
  $etabs->DeletedCompe($syllabusid,$compid);
  $_SESSION['user']['addclasseok']=L::CompetencesDeletedOk;
}


 ?>
