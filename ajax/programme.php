<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";


$programme=htmlspecialchars(addslashes($_POST['programme']));
$classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
$matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
$tabmat=explode('-',$matclasse);
$matiereid=$tabmat[0];
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$teatcher=htmlspecialchars(addslashes($_POST['teatcher']));

//nous allons verifier si nous avons deja un programme pour cette classe et pour cette matiere

$check=$etab->getNumberofPgrmeClasse($classeEtab,$matiereid,$codeEtab,$teatcher);

if($check==0)
{
$content=0;
}else if($check>0)
{
$content=1;
}

echo $content;
}




 ?>
