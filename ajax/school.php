<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
//nous allons verifier si le code etablissement existe deja dans la base de données
// $etabs=new Etab();
$content="";

$codeetab=htmlspecialchars($_POST['codeetab']);
$pays=htmlspecialchars($_POST['pays']);

// $check=$etabs->existEtab($codeetab);

$check=$etabs->existEtabCountry($codeetab,$pays);

if($check==0)
{
  $content=0;
}else {
  $content=1;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

  //recupération du code établissment


  $content="";

  $codeetab=htmlspecialchars($_POST['code']);

  $libelles=$etabs->getEtablissementbyCodeEtab($codeetab);

  foreach ($libelles as $value):
$content .= "<option selected value='". utf8_encode(utf8_decode($value->libelle_etab))."' >" . utf8_encode(utf8_decode($value->libelle_etab)). "</option>";

endforeach;

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{

  $content="";
  $pays=htmlspecialchars($_POST['pays']);
  $datas=$etabs->getAllEtabByCountry($pays);
  $nb=count($datas);

  if($nb>0)
  {
    $content.="<option value=''>".L::PleaseEtabs."</option>";
      foreach ($datas as $value):
          $content .= "<option value='". $value->code_etab ."*".$value->libelle_sess."' >" . utf8_encode(utf8_decode($value->libelle_etab)). "</option>";
      endforeach;
  }else if($nb==0)
  {
      $content.="<option value=''>".L::NoEtab."</option>";
  }

echo $content;

}



 ?>
