<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/Parent.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$student= new Student();
$classeSchool= new Classe();
$parentx=new ParentX();
$etabs= new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables

  $content="";
  $login=htmlspecialchars($_POST['login']);
  $password=htmlspecialchars($_POST['password']);

  $nbparent=$parentx->LoginExiste($login);

  if($nbparent>0)
  {
$content=1;
  }else {
$content=0;
  }

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $telephone=htmlspecialchars($_POST['telephone']);

  $nbphone=$parentx->parentphonechecker($telephone);

  echo $nbphone;

}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $telephone=htmlspecialchars($_POST['telephone']);

  $parentcompteid=$parentx->getparentidcomptebyphone($telephone);

  echo $parentcompteid;

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recuperation des variables

  $parentid=htmlspecialchars($_POST['parentid']);

  $datainfos=$parentx->getParentInfosbyId($parentid);

  echo $datainfos;
  //
  // echo $parentcompteid;

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

  $telephone=htmlspecialchars($_POST['telephone']);

  //nous voulons retrouver le parent ayant ce numero de telephone

  $datainfos=$parentx->getParentInfosbyphoneId($telephone);

echo $datainfos;
}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $content="";

  $scolarite=htmlspecialchars($_POST['scolarite']);
  $montantverser=htmlspecialchars($_POST['montantverser']);

  if($montantverser>$scolarite)
  {
    $content=1;
  }else {
    $content=0;
  }



echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
//recuperation des variables
$donnees="";

$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['session']);
$parascolaireid=htmlspecialchars($_POST['parascolaire']);

$data=$etabs->getAllParascolairesActivityForcalendar($codeEtab,$sessionEtab,$parascolaireid);
$array=json_encode($data,true);
$someArray = json_decode($array, true);

$donnees=$someArray[0]["libelle_act"]."*".$someArray[0]["datedeb_act"]."*".$someArray[0]["heuredeb_act"]."*".$someArray[0]["datefin_act"]."*".$someArray[0]["heurefin_act"];
$donnees.="*".$someArray[0]["lieuact_act"]."*".$someArray[0]["respo_act"]."*".$someArray[0]["payant_act"]."*".$someArray[0]["montant_act"]."*".$someArray[0]["classes_act"]."*".$someArray[0]["description_act"];

echo  $donnees;

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{



}


 ?>
