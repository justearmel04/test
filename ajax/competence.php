<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$matierex= new Matiere();
$etab=new Etab();
$classex= new Classe();



if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$competence=htmlspecialchars($_POST['matiere']);
$teatcherid=htmlspecialchars($_POST['teatcherid']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

$datas=$etab->getAllcompetenceOfTeatchers($competence,$teatcherid,$codeEtab,$sessionEtab);



$nb=count($datas);

if($nb==0)
{
//nous allons créer la compétence

$etab->AddcompetenceOfTeatchers($competence,$teatcherid,$codeEtab,$sessionEtab);

$_SESSION['user']['messages']=L::AddTheLibellecompetencesuccess;

}else {
  $_SESSION['user']['messagesnonok']=L::CompetenceAllreadyexiste;
}

echo $nb;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $competence=htmlspecialchars($_POST['competence']);
  $matiere=htmlspecialchars($_POST['matiere']);
  $teatcherid=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classe=htmlspecialchars($_POST['classe']);

  $tabmatiere=explode("-",$matiere);
  $matiereid=$tabmatiere[0];

  $libellematiere=$matierex->getMatiereLibelleByIdMat($matiereid,$codeEtab);

  //nous allons verifier si la compétence est déja ajouter dans cette matiere

  $nb=$etab->verficationCompetenceExiste($competence,$teatcherid,$classe,$matiereid,$codeEtab,$sessionEtab);

  if($nb>0)
  {
    $_SESSION['user']['messagesnonok']=L::CompetenceMatiereAllreadyexiste;
  }else {
    // insertion de la compétence pour cette matière

    $etab->AddcompetencesMatieres($competence,$teatcherid,$classe,$matiereid,$codeEtab,$sessionEtab,$libellematiere);
    $_SESSION['user']['messages']=L::CompetenceMatiereAddsucess;
  }


  // $etab->AddcompetencesEtabs();


}


 ?>
