<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Classe.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$sallex= new Salle();
$classex=new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$libetab=htmlspecialchars(addslashes($_POST['libetab']));
$salle=htmlspecialchars(addslashes($_POST['salle']));

  $check=$sallex->ExisteSalles($libetab,$salle);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $content="";
  $code=htmlspecialchars(addslashes($_POST['code']));

  $datas=$sallex->getAllSallesbyschoolCode($code);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoSalle." </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>".L::SelectSalle."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_salle ."' >" . utf8_encode(utf8_decode($value->libelle_salle)). "</option>";
    endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  $sallecapacite=htmlspecialchars($_POST['sallecapacite']);
  $salle=htmlspecialchars($_POST['salle']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEab=htmlspecialchars($_POST['sessionEab']);
  $salleid=htmlspecialchars($_POST['salleid']);

  $classex->UpdateClassesInfos($salle,$sallecapacite,$salleid,$codeEtab,$sessionEab);

$_SESSION['user']['addsalleok']=L::SalleUpdatesuccessfully;


}



 ?>
