<?php
session_start();
require_once('../class/Message.php');
require_once('../class/Parent.php');
require_once('../class/Student.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$messages = new Message();
$etabs=new Etab();

//recuperation des variables


if(isset($_POST['email'])&&($_POST['phone'])&&($_POST['name'])&&($_POST['message']))
{
  $name=$_POST['name'];
  $email=$_POST['email'];
  $phone=$_POST['phone'];
  $message=$_POST['message'];

$check=$messages->MessageDemo($name,$email,$phone,$message);

echo $check;



}
if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $content="";

  $datas=$etabs->getNotificationInfos($notifid);

  echo $datas;



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables smssender+'&emailsender='+emailsender
  $smssender=htmlspecialchars(addslashes($_POST['smssender']));
  $emailsender=htmlspecialchars(addslashes($_POST['emailsender']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $destinataires=htmlspecialchars(addslashes($_POST['destinataires']));
  $classes=htmlspecialchars(addslashes($_POST['classes']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));

  $tabdatadestinataires=explode("-",$destinataires);
  $nbtabdestinataires=count($tabdatadestinataires);
  $cptedestinatairesval=$nbtabdestinataires-1;

  $tabclasses=explode("-",$classes);
  $nbtabclasses=count($tabclasses);
  $cpteclassesval=$nbtabclasses-1;

  //determiner l'indicatif de ce pays

  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

  $destimails="";
  $destiphone="";


    for($i=0;$i<$cpteclassesval;$i++)
    {


      for($j=0;$j<$cptedestinatairesval;$j++)
      {
          // echo $tabclasses[$i]."-".$tabdatadestinataires[$j].'<br>';
          if($tabdatadestinataires[$j]=="Parent")
          {
              // echo "Parent de la classe ".$tabclasses[$i]."<br>";
              //rechercher le mail des parents dont un eleve appartient à cette classeEtab

              $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);
               //var_dump($dataParents);
               $ka=1;
               foreach ($dataParents as $parents):

                 $destimails=$destimails.$parents->email_parent."*";
                 $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


                 $ka++;
               endforeach;

          }else if($tabdatadestinataires[$j]=="Student")
          {
            // echo "Student de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des eleves de cette classeEtab
            $dataStudents=$etabs->getEmailsOfStudentOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($dataStudents);

            $ka=1;
            foreach ($dataStudents as $students):

              $destimails=$destimails.$students->email_eleve."*";
              $destiphone=$destiphone.$indicatifEtab.$students->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Teatcher")
          {
            // echo "Teatcher de la classe ".$tabclasses[$i]."<br>";
            // rechercher le mail des professeurs de cette classe

            $datateatchers=$etabs->getEmailsOfTeatcherOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
            //var_dump($datateatchers);

            $ka=1;
            foreach ($datateatchers as $teatchers):

              $destimails=$destimails.$teatchers->email_compte."*";
              $destiphone=$destiphone.$indicatifEtab.$teatchers->tel_compte."*";


              $ka++;
            endforeach;

          }else if($tabdatadestinataires[$j]=="Admin_locale")
          {
            // echo "Admin de la classe ".$tabclasses[$i]."<br>";
            // rechercher la liste local_admin de cet etablissement
            $datalocaladmins=$etabs->getEmailOfLocaladminOfThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab);
          //  var_dump($datalocaladmins);

          $ka=1;
          foreach ($datalocaladmins as $locals):

            $destimails=$destimails.$locals->email_compte."*";
            $destiphone=$destiphone.$indicatifEtab.$locals->tel_compte."*";


            $ka++;
          endforeach;

          }
      }
    }

    echo $destimails."/".$destiphone;



}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $content="";

  // $datas=$etabs->getNotificationInfos($notifid);

  $datas=$etabs->getMessagesInfos($notifid);

  echo $datas;
}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  $smssender=htmlspecialchars(addslashes($_POST['smssender']));
  $emailsender=htmlspecialchars(addslashes($_POST['emailsender']));
  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $destinataires=htmlspecialchars(addslashes($_POST['destinataires']));
  $classes=htmlspecialchars(addslashes($_POST['classes']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));
  $eleves=htmlspecialchars(addslashes($_POST['eleves']));

  $tabdatadestinataires=explode("-",$destinataires);
  $nbtabdestinataires=count($tabdatadestinataires);
  $cptedestinatairesval=$nbtabdestinataires-1;

  $tabclasses=explode("-",$classes);
  $nbtabclasses=count($tabclasses);
  $cpteclassesval=$nbtabclasses-1;

  //determiner l'indicatif de ce pays

  $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

  $destimails="";
  $destiphone="";

  if($precis==0)
  {
  //la liste des parents de toutes les classes selectionnées

  for($i=0;$i<$cpteclassesval;$i++)
  {
    for($j=0;$j<$cptedestinatairesval;$j++)
    {

    if($tabdatadestinataires[$j]=="Parent")
    {
        // echo "Parent de la classe ".$tabclasses[$i]."<br>";
        //rechercher le mail des parents dont un eleve appartient à cette classeEtab

        // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);
        

        $dataParents=$etabs->getEmailsOfParentOfStudentInThisClassesParenter($tabclasses[$i],$tabdatadestinataires[$j],$codeEtab,$sessionEtab);


         // var_dump($dataParents);
         $ka=1;
         foreach ($dataParents as $parents):

           $destimails=$destimails.$parents->email_parent."*";
           $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


           $ka++;
         endforeach;

    }
  }
  }



  }else if($precis==1)
  {
      //listes des parents en fonction des eleves selectionnées

      $tabeleves=explode("-",$eleves);
      $nbtabeleves=count($tabeleves);
      $cpteelevesval=$nbtabeleves-1;

      // echo $classes." ".$eleves;

      $classes=substr($classes, 0, -1);
      $eleves=substr($eleves, 0, -1);

      $classes = str_replace("-", ",",$classes);
      $eleves=str_replace("-", ",",$eleves);

      // $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclasses($classes,$eleves,$sessionEtab,$codeEtab);
      $dataParents=$etabs->getEmailsAndPhoneOfParentOfStudentclassesParenter($classes,$eleves,$sessionEtab,$codeEtab);


      // var_dump($dataParents);
      $ka=1;
      foreach ($dataParents as $parents):

        $destimails=$destimails.$parents->email_parent."*";
        $destiphone=$destiphone.$indicatifEtab.$parents->tel_parent."*";


        $ka++;
      endforeach;




  }

    echo $destimails."/".$destiphone;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeetab=htmlspecialchars(addslashes($_POST['codeetab']));
  $eleves=htmlspecialchars(addslashes($_POST['eleves']));
  $message=htmlspecialchars(addslashes($_POST['message']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));
  $precis=htmlspecialchars(addslashes($_POST['precis']));

  //nous allons chercher à savoir qui a ecrit cette note

  $typecompteadder=$etabs->getTypecompteOfuser($addby);

  //nous allons essayer de voir si il est question d'une activité parascolaire ou note d'observation

    $datamessages=$etabs->getMessagesType($message,$addby);
    $tabmessages=explode("*",$datamessages);
    $parascolaire=$tabmessages[0];
    $scolarite=$tabmessages[1];



  if($typecompteadder=="Teatcher")
  {
    // $lien=$etabs->creationOfObservationNoteTeatcher($classe,$session,$codeetab,$eleves,$message);

    if($parascolaire==0)
    {
      if($precis==0)
      {
$lien=$etabs->creationOfObservationNoteTeatcher($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
$lien=$etabs->creationOfObservationNoteTeatcherPrecis($classe,$session,$codeetab,$eleves,$message);
      }
    }

  }else if($typecompteadder=="Admin_locale")
  {
    if($parascolaire==1)
    {
      // $lien=$etabs->creationOfObservationNote($classe,$session,$codeetab,$eleves,$message);
      if($precis==0)
      {
        $lien=$etabs->creationOfObservationNoteAllselect($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
        $lien=$etabs->creationOfObservationNote($classe,$session,$codeetab,$eleves,$message);

      }


    }else if($parascolaire==0)
    {
      if($precis==0)
      {
        $lien=$etabs->createMessageEtatForAll($classe,$session,$codeetab,$eleves,$message);
      }else if($precis==1)
      {
          $lien=$etabs->createMessageEtatForPrecise($classe,$session,$codeetab,$eleves,$message);
      }
    }

  }

echo $lien;


}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recuperation des variables

  $notifid=htmlspecialchars(addslashes($_POST['notifid']));
  $paraid=htmlspecialchars(addslashes($_POST['paraid']));
  $scolaire=htmlspecialchars(addslashes($_POST['scolaire']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['sessionEtab']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));

  if($paraid==1)
  {
    //il est question d'une activité parascolaire
    $datas=$etabs->getMessagesInfos($notifid);
  }else if($scolaire==1)
  {
    //il est question du paiement de scolarité

  }else if($paraid==0&&$scolaire==0)
  {
    //il est question d'un message

    $datas=$etabs->getMessagesInfosOnly($notifid);
  }

echo $datas;
}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  $notifid=htmlspecialchars($_POST['notifid']);
  $destimails=htmlspecialchars($_POST['destimails']);
  $destiphones=htmlspecialchars($_POST['destiphones']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $smssender=htmlspecialchars($_POST['smssender']);
  $emailsender=htmlspecialchars($_POST['emailsender']);
  $precis=htmlspecialchars($_POST['precis']);
  $eleves=htmlspecialchars($_POST['eleves']);
  $paraid=htmlspecialchars($_POST['paraid']);
  $scolarities=htmlspecialchars($_POST['scolarities']);
  $notificationsStatus=1;
  $parascolaireStatus=2;
  $mailsofetab=$etabs->getEtabMailler($codeEtab);
  if(strlen($mailsofetab)==0)
  {
    $mailsofetab="infos@xschoolink.com";
  }

  $messagesInfos=$etabs->getMessagesInformationsDetails($notifid,$codeEtab,$sessionEtab);
  $commentairesmessage="";
  $messageobjetsend="";
  $file_msg="";
  $addby_msg="";
  $date_msg="";
  $summernote="";

  foreach ($messagesInfos as $value):
    $commentairesmessage=$value->letters_msg;
    $file_msg=$value->file_msg;
    $addby_msg=$value->addby_msg;
    $date_msg=$value->date_msg;
    $summernote=$value->letters_msg;
  if($value->parascolaire_msg==1)
  {
    // echo "parascolaire";
    $messageobjetsend=$etabs->getparacolaireDesignation($value->id_msg);
  }else if($value->scola_msg==1)
  {
    // echo "scolarite";
  }else if(($value->parascolaire_msg==0)&&($value->scola_msg==0))
  {
    // echo "note observation";
    if($value->objet_msg==8)
    {
      $messageobjetsend=$value->other_msg;
    }else {
      $messageobjetsend=$value->libelle_msg;
    }
  }

  endforeach;

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);

$destinataires=$etabs->getMessagesdestinaires($notifid,$codeEtab,$sessionEtab);
$indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

// var_dump($destinataires);

$notificationsStatus=1;
// $etabs->UpdateNotificationStatusActivities($notifid,$notificationsStatus,$codeEtab);

  $ids="";

  $destimails="";
  $destiphones="";

  foreach ($destinataires as $value):
    $ids=$ids.$value->idcompte_lec.",";

    $datas=$etabs->getUtilsateursAlls($ids);
foreach ($datas as $values):
  if($values->type_compte=="Parent")
  {
    $receivers=$etabs->getparentInfos($values->id_compte);

    foreach ($receivers as $values):
      if(strlen($values->email_parent)>0 && $values->email_parent!="")
      {
        $destimails=$destimails.$values->email_parent."*";
      }

      if(strlen($values->tel_parent)>0 && $values->tel_parent!="")
      {
        $destiphones=$destiphones.$indicatifEtab.$values->tel_parent."*";
      }


    endforeach;

  }else if($values->type_compte=="Teatcher")
  {
    $receivers=$etabs->getEmailsOfTeatchersInThisclassesByIds($values->type_compte,$values->id_compte);
    foreach ($receivers as $values):
      if(strlen($values->email_enseignant)>0 && $values->email_enseignant!="")
      {
        $destimails=$destimails.$values->email_enseignant."*";
      }

      if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
      {
        $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
      }
    endforeach;

  }else if($values->type_compte=="Admin_locale")
  {
    if(strlen($values->tel_compte)>0 && $values->tel_compte!="")
    {
      $destiphones=$destiphones.$indicatifEtab.$values->tel_compte."*";
    }

    if(strlen($values->email_compte)>0 && $values->email_compte!="")
    {
      $destimails=$destimails.$values->email_compte."*";
    }
  }

  // echo $values->email_compte."/".$file_msg."/".$mailsofetab."</br>";

  if($file_msg==1)
  {
    $fichierad=$etabs->getFilesmessages($notifid);
    $lienfiles="../messages/".$libelleEtab."/".$codeEtab."/".$sessionEtab."/".$fichierad;
    $etabs->SenderEmailwithFiles($messageobjetsend,$values->email_compte,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab,$lienfiles);
  }else {
    $etabs->SenderEmailOne($messageobjetsend,$values->email_compte,$summernote,$sessionEtab,$codeEtab,$libelleEtab,$logoEtab,$mailsofetab);
  
  //   echo $messageobjetsend." ".$values->email_compte." ".$summernote." ".$sessionEtab." ".$codeEtab." ".$libelleEtab." ".$logoEtab." ".$mailsofetab;
  
  //       require_once('../PHPMailer/class.phpmailer.php');
  //       require_once('../PHPMailer/class.smtp.php');
  //       require_once('../controller/functions.php');
  //       $client1="justearmel04@gmail.com";
  //       $client2="fabienekoute@gmail.com";
	// 	$mail = new PHPMailer(true);
	// 	// $mail->SMTPDebug = 0;
	// 	$mail->isHTML(true);
	// 	$mail->CharSet="UTF-8";
  //       $mail->isSMTP();
  //       $mail->SMTPOptions = array (
  //      'ssl' => array(
  //      'verify_peer'  => false,
  //      'verify_peer_name'  => false,
  //      'allow_self_signed' => true));
  //      $mail->Host='mail.xschoolink.com';
  //      $mail->SMTPAuth = true;
  //      $mail->Port = 587;
  //      $mail->SMTPSecure = "tls";
  //      $mail->Username = "mailler@xschoolink.com";
  //      $mail->Password ="123psa@456";
  //      $mail->From="ibsa@xschoolink.com";
  //      $mail->FromName=$libelleEtab;
  //     //  $mail->AddAddress($client1);
  //      $mail->AddAddress($values->email_compte);
  //      $mail->Subject =$messageobjetsend;
	//      $mail->Body =".$summernote.";
  //      $mail->Body .="<img src='cid:mon_logo' style='width:20%;'>";
  //      $mail->AddEmbeddedImage("../logo_etab/".$codeEtab."/".$logoEtab,"mon_logo");
  //      $mail->Send();
    
  //      if(!$mail->Send())

  //      {

  //      $msg="nok";

  //      }else
  //      {

  //      $msg="ok";

	//    }
	   
	   

  // echo  $msg;

  }


endforeach;





  endforeach;

$ids=substr($ids, 0, -1);
//  echo $ids."/".$messageobjetsend;


 $_SESSION['user']['updateteaok']=L::NotificationAddMessageSuccess;

 








}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  $notifid=htmlspecialchars($_POST['notifid']);
  $paraid=htmlspecialchars($_POST['paraid']);
  $scolaid=htmlspecialchars($_POST['scolaid']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $statusNotification=-1;
  $statusparasco=0;
  $statutsolde=4;

  if($paraid>0)
    {
      //echo "bonjour";
  //$etabs->DeleteNotificationParascoActivity($paraid,$notifid,$statusNotification,$statusparasco);
    }

    if($scolaid>0)
      {
        //echo "bonjour";
    //$etabs->DeleteNotificationScolariteActivities($scolaid,$notifid,$statusNotification,$statutsolde);
      }

    if($paraid==0 && $scolaid==0 )
    {
      // echo "selut";
      $etabs->DeleteNotificationActivitiesMixte($notifid,$statusNotification);
    }

    $_SESSION['user']['updateteaok']=L::NotificationDeleteMessageSuccess;

}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{

  $notifid=htmlspecialchars($_POST['notifid']);
  $paraid=htmlspecialchars($_POST['paraid']);
  $scolarid=htmlspecialchars($_POST['scolarid']);
  $session=htmlspecialchars($_POST['sessionEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $notificationstatus=2;
  $notificationsparastatus=3;
  $notificationsoldesatatus=3;


  if($paraid>0)
  {
    //activité parascolaire

    // echo "para";

    $etabs->ArchivedParascolaireActivities($paraid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsparastatus);

    $_SESSION['user']['updateteaok']="Le Message a été archivé avec succèss";
    
  }


  if($scolarid>0)
  {
    //soldenotification
    // echo "aurevoir";
  $etabs->ArchivedScolaritesoldeActivities($scolarid,$notifid,$session,$codeEtab,$notificationstatus,$notificationsoldesatatus);

  $_SESSION['user']['updateteaok']="Le Message a été archivé avec succès";
  

  }

  if($paraid==0 && $scolarid==0)
  {
    //notification simple
    $etabs->ArchivedNotificationActivities($notificationstatus,$notifid,$session,$codeEtab);

    $_SESSION['user']['updateteaok']="Le Message a été archivé avec succès";
    
  }



}



 ?>
