<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$users = new User();
$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$studentid=htmlspecialchars($_POST['compte']);

//nous allons recuperer les différentes modalité de paiement

$datas=$etab->getStudentModalitesPaiement($studentid,$codeEtab,$sessionEtab);

$nbdep=count($datas);
if($nbdep==0)
{
  $content.="<tr>";
  $content.="<td colspan=\"3\">".L::NoLigne."</td>";
  $content.="</tr>";
}else {
  foreach ($datas as $value):
    $libelle="";
    $libellepayeur="";
    $statut=$value->statut_modalvplan;
    $payeur=$value->payeur_vplan;
    if($statut==0)
    {
    $libelle=L::nonpayer;
    }else {
      $libelle=L::payer;
    }

    if($payeur=="FAMILLE")
    {
      $libellepayeur=L::Moimem;
    }else {
      $libellepayeur=$value->libellepayeur_vplan;
    }



    $content.="<tr>";
    $content.="<td><span>".date_format(date_create($value->date_modalvplan),"d/m/Y")."</span></td>";
    $content.="<td><span >".number_format($value->montant_modalvplan,0,',',' ')."</span></td>";
    $content.="<td><span >".$libelle."</span></td>";
    $content.="<td><span >".$libellepayeur."</span></td>";
    $content.="</tr>";
  endforeach;
}

echo $content;

}
 ?>
