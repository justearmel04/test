<?php
session_start();
require_once('../class/Admin.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recupération des variables

  $content="";



  $login=htmlspecialchars(addslashes($_POST['login']));
  $email=htmlspecialchars(addslashes($_POST['email']));

  $admin= new Admin();

  $check=$admin->existAdmin($login,$email);
  //$check=$admin=checklocalcompte($login,$email);

  if($check==0)
  {
    $content=0;
  }else {
    $content=1;
  }

  echo $content;

}


 ?>
