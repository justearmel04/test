<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$classeEtab=htmlspecialchars(addslashes($_POST['classeEtab']));
$matclasse=htmlspecialchars(addslashes($_POST['matclasse']));
$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$teatcher=htmlspecialchars(addslashes($_POST['teatcher']));
$details=trim(htmlspecialchars(addslashes($_POST['details'])));



$tabmat=explode('-',$matclasse);
$matiereid=$tabmat[0];
// $details=substr($details, 0, -1);
//
 $tabdetails=explode(';',$details);
 $nbdetails=count($tabdetails);
if($nbdetails==1)
{

}

//echo $nbdetails;substr($a, 0, -1)



}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  // data: 'ficheid=' + ficheid+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,

  //recuperation des variables
  $ficheid=htmlspecialchars(addslashes($_POST['ficheid']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiere=htmlspecialchars(addslashes($_POST['matiere']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $content="";

  $nb=$etab->getNumberOfSousFiches($ficheid);

  if($nb==1)
  {
    $content=1;
  }else if($nb>1)
  {
    $content=0;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables
  $ficheid=htmlspecialchars(addslashes($_POST['ficheid']));
  $desi=utf8_decode(htmlspecialchars(addslashes($_POST['desi'])));
  $sousficheid=htmlspecialchars(addslashes($_POST['sousficheid']));

  //mise e jour de sousfiche

  $etab->UpdateSousFiches($desi,$ficheid,$sousficheid);

  $_SESSION['user']['addprogra']="Element modifié avec succès";

  $content=1;

  echo $content;

}




 ?>
