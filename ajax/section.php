<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();
$classex= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$section=htmlspecialchars(addslashes($_POST['section']));
$session=htmlspecialchars(addslashes($_POST['session']));

$check=$classex->ExisteSections($section,$codeEtab,$session);

  // $check=$classex->ExisteClasses($libetab,$classe);

  if($check==0)
  {
$content=0;
  }else {
    $content=1;
  }

echo $content;

}



 ?>
