<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Etablissement.php');
require_once('../class/User.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$users = new User();
$matiere= new Matiere();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$login=htmlspecialchars($_POST['login']);
$pass=htmlspecialchars($_POST['pass']);
$studentid=htmlspecialchars($_POST['studentid']);
$type="Student";

$check=$users->CheckCompteExisteOtherStudentMixte($login,$pass,$type,$studentid);



if($check==0)
{
  // $content=0;

//nous allons verifier si le login et le mot de passe existe pour un autre type de compte

$check1=$users->CheckCompteAndPassMixte($login,$pass);

if($check1==0)
{
$content=0;
}else if($check1>0) {
  $content=2;
}

}else if($check>0) {
  $content=1;
}

echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{

//recupération des variables

$content="";

$login=htmlspecialchars($_POST['login']);
$pass=htmlspecialchars($_POST['pass']);
$studentid=htmlspecialchars($_POST['studentid']);
$type="Teatcher";

$check=$users->CheckCompteExisteOtherStudentMixte($login,$pass,$type,$studentid);



if($check==0)
{
  // $content=0;

//nous allons verifier si le login et le mot de passe existe pour un autre type de compte

$check1=$users->CheckCompteAndPassMixte($login,$pass);

if($check1==0)
{
$content=0;
}else if($check1>0) {
  $content=2;
}

}else if($check>0) {
  $content=1;
}

echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

    $compteid=htmlspecialchars($_POST['compteid']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $etab->setofflineUser($compteid);
    $lien="";
    $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
    $typeetab=$etab->DetermineTypeEtab($codeEtab);

     if($typeetab==5)
     {
       $lien="../signup".strtolower($libelleEtab).".php";
     }else {
       $lien="../signup.php";
     }

     echo $lien;

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

    $compteid=htmlspecialchars($_POST['compteid']);
    // $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $etab->setofflineUser($compteid);
    $lien="../signupibsa.php";
    // $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
    // $typeetab=$etab->DetermineTypeEtab($codeEtab);
    //
    //  if($typeetab==5)
    //  {
    //    $lien="../signup".$libelleEtab.".php";
    //  }else {
    //    $lien="../signup.php";
    //  }

     echo $lien;

}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  $login=htmlspecialchars($_POST['login']);
  $pass=htmlspecialchars($_POST['pass']);
  $studentid=htmlspecialchars($_POST['studentid']);
echo $content=0;
  $check=$users->getNbusernameexist($login,$studentid);

  if($check==0)
  {
echo $content=0;
  }else {
    echo $content=1;
  }

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  //recuperation des variables

  $studentid=htmlspecialchars($_POST['studentid']);
  $numclient=htmlspecialchars($_POST['numclient']);

  $etab->UpdateNumclient($numclient,$studentid);

  $_SESSION['user']['addStudok']=L::AddClientNumbersuccesfully;


}else if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  //recuperation des variables

  $studentid=htmlspecialchars($_POST['studentid']);
  $numclient=htmlspecialchars($_POST['numclient']);

  $etab->UpdateNumclient($numclient,$studentid);

  $_SESSION['user']['addStudok']=L::ModClientNumbersuccesfully;


}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  $studentid=htmlspecialchars($_POST['studentid']);
  $numclient=htmlspecialchars($_POST['numclient']);
  $oldnumber=htmlspecialchars($_POST['oldnumber']);

  $tempcontrat="../photo/Contrats/".$oldnumber."/";
  $tempcarnet="../photo/Carnets/".$oldnumber."/";
  $tempphoto="../photo/Students/".$oldnumber."/";

  if(is_dir($tempphoto))
  {
    rename($tempphoto,"../photo/Students/".$numclient."/");
  }

  if(is_dir($tempcarnet))
  {
    rename($tempcarnet,"../photo/Carnets/".$numclient."/");
  }

  if(is_dir($tempcontrat))
  {
    rename($tempcontrat,"../photo/Contrats/".$numclient."/");
  }



  $etab->UpdateMatrcicule($numclient,$studentid);

  // rename('../temp/' . $fichierTemp ,"../devoirs/Etablissements/".$codeEtab."/".str_replace(' ', '_',$libellematiere)."/".str_replace(' ', '_',$libelleclasse)."/".$fichierad);

  //nous devons renomer le dossier



  $_SESSION['user']['addStudok']=L::MatriculeUpdateokay;
}
 ?>
