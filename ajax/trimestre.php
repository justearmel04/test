<?php
session_start();
require_once('../class/Salle.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Etablissement.php');
require_once('../intl/i18n.class.php');


if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();
$etabs=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$matiere=htmlspecialchars($_POST['matiereid']);
$data=$etabs->getMatiereInformations($matiere);
foreach ($data as $value):
  $codeEtab=$value->codeEtab_mat;
  $sessionEtab=$value->session_mat;
  $classeEtab=$value->classe_mat;
endforeach;

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtab);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtab);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}
//nous allons recuperer les différents trimestre en cours dans cet etablissement

$datas=$session->getActiveAllSemestrebyIdsessionAll($sessionencoursid);

// var_dump($datas);
$nbligne=count($datas);

if($nbligne>0)
{
  $content.="<option selected value='' >".L::selectTrimestre ."</option>";

  foreach ($datas as  $values):
    $content .= "<option value='". $values->id_semes."' >" . utf8_encode(utf8_decode($values->libelle_semes)). "</option>";
  endforeach;

}else {
  $content.="<option selected value='' >".L::selectTrimestre ."</option>";
}

echo $content."*".$codeEtab."*".$sessionEtab."*".$classeEtab;


}



 ?>
