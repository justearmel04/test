<?php
session_start();
require_once('../class/Matiere.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$matiere= new Matiere();
$etab=new Etab();
$classex= new Classe();



if(isset($_POST['etape'])&&($_POST['etape']==1))
{

//recupération des variables

$content="";

$matiere=htmlspecialchars($_POST['matiere']);
$classe=htmlspecialchars($_POST['classe']);
$teatcher=htmlspecialchars($_POST['teatcher']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$coefficient=htmlspecialchars($_POST['coefficient']);

//$check=$matiere->AllreadyExistMatiere();

//$content=$teatcher->getAllTeatchersByschoolCode($codeEtab);

//verifier si la matière existe dejà

//$check=$matiere->AllreadyExistMatiere();

$check=$etab->ExistMatiereAllready($matiere,$classe,$codeEtab);


if($check==0)
{
  //cette matière n'existe pas encore pour cette classe
  $content=0;
}else {
//cette matière existe deja pour cette classe
//nous allons verifier si il est question du meme professeur
$check1=$etab->ExistMatiereWithSameProf($matiere,$classe,$codeEtab,$teatcher);

if($check1==0)
{
  //il est question d'un nouveau professeur pour cette matière

  $content=1;
}else {
  // il s'agit du meme professeur pour cette matiere
  $content=2;
}

}

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recupération des variables

$code=htmlspecialchars($_POST['code']);
$classe=htmlspecialchars($_POST['classe']);
$nbsessionOn=$session->getNumberSessionEncoursOn($code);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($code);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}
$content="";

$datas=$etab->getAllsubjectofclassesbyIdclasses($classe,$code,$libellesessionencours);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>".L::NoMatiere." </option>";

}else {
  //var_dump($datas);
  $content.="<option selected value='' >".L::SelectSubjects."</option>";


  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
}

echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
    //recupération des variables

    $classe=htmlspecialchars($_POST['classe']);
    $matiere=htmlspecialchars($_POST['matiere']);
    $day=htmlspecialchars($_POST['day']);
    $heuredeb=htmlspecialchars($_POST['heuredeb']);
    $heurefin=htmlspecialchars($_POST['heurefin']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    $content="";

    //verifier si nous avons deja une routine dans cette période

    $check=$etab->verifyRoutineExistInTimePeriode($heuredeb,$heurefin,$day,$classe,$codeEtab);

    if($check>0)
    {
      //nous avons une matière dans la période choisie

      //nous allons verifier si il est question de la meme matière ou d'une autre matière
      $check1=$etab->verifyRoutineExistwithThisSubject($heuredeb,$heurefin,$day,$classe,$codeEtab,$matiere);

      if($check1>0)
      {
        //il est question de la même matiere
        $content=1;
      }else {
        //il est question d'une nouvelle matière or nous avons une matière a cette période
        $content=2;
      }

    }else {
      // nous n'avons pas de matière dans la période choisie
      $content=0;
    }

echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
  //recupération des variables

$code=htmlspecialchars($_POST['code']);
$classe=htmlspecialchars($_POST['classe']);
$nbsessionOn=$session->getNumberSessionEncoursOn($code);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($code);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
}
$content="";

$datas=$etab->getAllsubjectofclassesbyIdclassesAndTeatcherId($classe,$code,$libellesessionencours);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>".L::NoMatiere." </option>";

}else {
  //var_dump($datas);
  $content.="<option selected value='' >".L::SelectSubjects."</option>";


  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
}

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recupération des variables

  $code=htmlspecialchars($_POST['code']);
  $classe=htmlspecialchars($_POST['classe']);
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $content="";

  $datas=$etab->getAllsubjectofclassesbyIdclasses($classe,$code,$libellesessionencours);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";

  }else {
  //var_dump($datas);
$content.="<option selected value=''  >".L::SelectSubjects."</option>";


  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
  }

  echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recupération des variables

  $content="";

  $teatcherId=htmlspecialchars($_POST['teatcherId']);
  $classe=htmlspecialchars($_POST['classe']);

  //afficher la liste des matière de cette classe le professeur est teatcherId

  $datas=$etab->getAllMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
  //var_dump($datas);
  $content.="<option selected value='' >".L::SelectSubjects."</option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
  }

  echo $content;


}if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //recupération des variables

  $content="";
  $matiere=htmlspecialchars($_POST['matiere']);
  $tabMat=explode('-',$matiere);
  $matiereid=$tabMat[0];
  $teatcherId=htmlspecialchars($_POST['teatcherId']);
  $classe=htmlspecialchars($_POST['classe']);
  $content=$etab->getCodeEtabofMatiereChoosen($teatcherId,$classe,$matiereid);

  echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==8))
{
  //recuperation des variables
  $classe=htmlspecialchars($_POST['classe']);
  $code=htmlspecialchars($_POST['code']);
  $teatcherId=htmlspecialchars($_POST['teatcher']);
  $content="";

  $nbligne=$etab->getNbMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe,$code);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else if($nbligne>0)
  {
    $datas=$etab->getAllMatiereTeatchByThisTeatcherOfClassesSchool($teatcherId,$classe,$code);
    $content.="<option selected value='' >".L::SelectSubjects."</option>";
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    endforeach;
  }
  echo $content;
}if(isset($_POST['etape'])&&($_POST['etape']==9))
{
  //recuperation des variables



}else if(isset($_POST['etape'])&&($_POST['etape']==10))
{
  //recuperation des variables

    $classe=htmlspecialchars($_POST['classe']);
    $session=htmlspecialchars($_POST['session']);
    $codeEtab=htmlspecialchars($_POST['codeEtab']);
    //nous allons retourner cette matiere

    $content="";

    $datas=$etab->getClassesByschoolCodewithIdspec($codeEtab,$classe,$session);
$content.="<option  value=''>Selectionner une Classe </option>";
    foreach ($datas as $value):
        $content .= "<option value='". $value->id_classe."' selected >". utf8_encode(utf8_decode($value->libelle_classe)). "</option>";
    endforeach;

    echo $content;





}else if(isset($_POST['etape'])&&($_POST['etape']==11))
{
  //recuperation des variables

  $classe=htmlspecialchars($_POST['classe']);
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $typecompte=1;

  $content="";

  $datas=$etab->getTearcherPrimaire($classe,$session,$codeEtab,$typecompte);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>Aucun Enseignant Titulaire </option>";
  }else {
    $content.="<option  value=''>Selectionner un Enseignant Titulaire </option>";
        foreach ($datas as $value):
            $content .= "<option value='". $value->idcompte_enseignant."' >". utf8_encode(utf8_decode($value->nom_enseignant.' - '.$value->nom_enseignant)). "</option>";
        endforeach;

  }

  echo $content;



}else if(isset($_POST['etape'])&&($_POST['etape']==12))
{
  //recuperation des variables
  $matiere=htmlspecialchars($_POST['matiere']);
  $classe=htmlspecialchars($_POST['classe']);
  $teatcher=htmlspecialchars($_POST['teatcher']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $coefficient=htmlspecialchars($_POST['coefficient']);

  $tabclasse=explode(",",$classe);
  $nbclasse=count($tabclasse);
  $content="";

  // echo $nbclasse;

  if($nbclasse==1)
  {

    $classe=$tabclasse[0];

    $check=$etab->ExistMatiereAllready($matiere,$classe,$codeEtab);


    if($check==0)
    {
      //cette matière n'existe pas encore pour cette classe
      $content=0;
    }else {
    //cette matière existe deja pour cette classe
    //nous allons verifier si il est question du meme professeur
    $check1=$etab->ExistMatiereWithSameProf($matiere,$classe,$codeEtab,$teatcher);

    if($check1==0)
    {
      //il est question d'un nouveau professeur pour cette matière

      $content=1;
    }else {
      // il s'agit du meme professeur pour cette matiere
      $content=2;
    }

    }

    echo $content;


  }else if($nbclasse>1)
  {
      // for($i=0;$i<$nbclasse;$i++)
      // {
      //
      // }

    $content1=0;

    echo $content1;
  }



}else if(isset($_POST['etape'])&&($_POST['etape']==13))
{
  //recuperation des variables

  $matiere=htmlspecialchars(utf8_encode($_POST['matiere']));
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $session=htmlspecialchars($_POST['session']);
  $content=0;
  //verifier si le libelle de cette matiere existe deja

  // echo utf8_decode($matiere);

  $check=$etab->LibellematiereExiste($matiere,$codeEtab,$session);

  if($check==0)
  {
    $content=0;
  }else if($check>0){
    $content=1;
  }

  echo $check;

}else if(isset($_POST['etape'])&&($_POST['etape']==14))
{
  $code=htmlspecialchars(addslashes($_POST['code']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);
  $datechoice=htmlspecialchars(addslashes($_POST['datechoice']));

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $content="";

  $tabdays=explode("/",$datechoice);

  $jourday=$tabdays[0];
  $moisday=$tabdays[1];
  $anneeday=$tabdays[2 ];

  $nouvelledate=$anneeday."-".$moisday."-".$jourday;

  // echo $nouvelledate;

  // $jour=date("l");
  $jour=nom_jour($nouvelledate);
  $libellejour=obtenirLibelleJours($jour);

  // echo $libellejour;

  // $libellejour="LUN";

  $datas=$etab->getAllsubjectofclassesbyIdclassesAndTeatcherIdDays($classe,$code,$libellesessionencours,$libellejour);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
    //var_dump($datas);
    $content.="<option selected value='' >".L::SelectSubjects."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==15))
{
  $code=htmlspecialchars($_POST['code']);
  $classe=htmlspecialchars($_POST['classe']);
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);
  $datechoice=htmlspecialchars($_POST['datechoice']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);

  if($nbsessionOn>0)
  {
    //recuperer la session en cours
    $sessionencours=$session->getSessionEncours($code);
    $tabsessionencours=explode("*",$sessionencours);
    $libellesessionencours=$tabsessionencours[0];
    $sessionencoursid=$tabsessionencours[1];
    $typesessionencours=$tabsessionencours[2];
  }
  $content="";

  $tabdays=explode("/",$datechoice);

  $jourday=$tabdays[0];
  $moisday=$tabdays[1];
  $anneeday=$tabdays[2 ];

  $nouvelledate=$anneeday."-".$moisday."-".$jourday;

  // echo $nouvelledate;

  // $jour=date("l");
  $jour=nom_jour($nouvelledate);
  $libellejour=obtenirLibelleJours($jour);

  // echo $libellejour;

  // $libellejour="LUN";

  $datas=$etab->getAllsubjectofclassesbyIdclassesAndTeatcherIdDaysTea($classe,$code,$libellesessionencours,$libellejour,$teatcherid);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
    //var_dump($datas);
    $content.="<option selected value='' >".L::SelectSubjects."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==16))
{
  //recupération des variables

  $code=htmlspecialchars($_POST['code']);
  $classe=htmlspecialchars($_POST['classe']);
  $nbsessionOn=$session->getNumberSessionEncoursOn($code);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $libellesessionencours=htmlspecialchars($_POST['session']);


  $content="";

  $datas=$etab->getAllsubjectofclassesbyIdclassesTea($classe,$code,$libellesessionencours,$teatcherid);

  // var_dump($datas);

  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
  //var_dump($datas);
  $content.="<option selected value='' >".L::SelectSubjects."</option>";

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;
  }

  echo $content;
}else if(isset($_POST['etape'])&&($_POST['etape']==17))
{
  //recupération des variables

  $content="";

  $teatcherId=htmlspecialchars(addslashes($_POST['teatcherId']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));

  //afficher la liste des matière de cette classe le professeur est teatcherId

  $datas=$etab->getAllMatiereTeatchByThisTeatcherOfClasses($teatcherId,$classe);
  $nbligne=count($datas);

  if($nbligne==0)
  {
    $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
  //var_dump($datas);
$content.="<option selected value='' >".L::SelectSubjects."</option>";

  foreach ($datas as $value):
    if($value->id_mat==$matiereid)
    {
  $content .= "<option selected value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    }else {
        $content .= "<option value='". $value->id_mat."-".$value->teatcher_mat."' >". utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    }

  endforeach;
  }

  echo $content;


}else if(isset($_POST['etape'])&&($_POST['etape']==18))
{
  //recupération des variables

$code=htmlspecialchars(addslashes($_POST['code']));
$libellesessionencours=htmlspecialchars(addslashes($_POST['session']));
$teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$nbsessionOn=$session->getNumberSessionEncoursOn($code);


$content="";

$datas=$etab->getAllsubjectofclassesbyIdclassesTea($classe,$code,$libellesessionencours,$teatcherid);

$nbligne=count($datas);

if($nbligne==0)
{
  $content.="<option selected value=''>".L::NoMatiere." </option>";
}else {
  //var_dump($datas);
  $content.="<option selected value='' >".L::SelectSubjects."</option>";
  }

  foreach ($datas as $value):
      $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
  endforeach;

  echo $content;
}



else if(isset($_POST['etape'])&&($_POST['etape']==19))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classe=htmlspecialchars($_POST['classe']);

  $content="";

  $datas=$etab->getAllsubjectofclassesbyIdclassesTea($classe,$codeEtab,$sessionEtab,$teatcherid);

  $nbligne=count($datas);

  if($nbligne==0)
  {
      $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
    //var_dump($datas);
    $content.="<option selected value='' >".L::SelectSubjects."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat)). "</option>";
    endforeach;
  }

  echo $content;

}else if(isset($_POST['etape'])&&($_POST['etape']==20))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classe=htmlspecialchars($_POST['classe']);
  $matiere=htmlspecialchars($_POST['matiere']);

  $etab->DeleteMatiereClassiques($matiere,$classe,$codeEtab,$sessionEtab);

  if($teatcherid>0)
  {
    //nous allons supprimer dans dispenser et dans enseigner

    $etab->DeletedEnseignerAndispenser($teatcherid,$matiere,$classe,$codeEtab,$sessionEtab);
  }

$_SESSION['user']['messages']=L::TheSubjectsDelsuccessfully;


}else if(isset($_POST['etape'])&&($_POST['etape']==21))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  // $teatcherid=htmlspecialchars($_POST['teatcherid']);
  $classe=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiereid']);
  $matieremod=htmlspecialchars($_POST['matieremod']);
  $coefmod=htmlspecialchars($_POST['coefmod']);

  $matiere->UpdateMatiereSingle($coefmod,$classe,$matiereid,$codeEtab,$sessionEtab);

  $_SESSION['user']['messages']=L::TheSubjectsModsuccessfully;


}else if(isset($_POST['etape'])&&($_POST['etape']==22))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classe=htmlspecialchars($_POST['classe']);
  $matiereid=htmlspecialchars($_POST['matiereid']);
  $content="";
  $content1="";

  $datas=$matiere->GetAllMatiereInfos($matiereid,$codeEtab,$sessionEtab);

  foreach ($datas as $value):
    $content=$content.$value->libelle_mat."*".$value->coef_mat;
  endforeach;

  $dataz=$classex->getAllclasseEtabBysession($codeEtab,$sessionEtab);
  $nbligne=count($dataz);

  if($nbligne==0)
  {
    $content.="<option value=''>".L::NoClasse."</option>";
  }else if($nbligne>0)
  {
    $content1.="<option value=''>".L::Selectclasses."</option>";
    foreach ($dataz as $values):
      if($values->id_classe==$classe)
      {
          $content1 .= "<option value='". $values->id_classe ."' selected >" . utf8_encode(utf8_decode($values->libelle_classe)). "</option>";
      }else {
          $content1 .= "<option value='". $values->id_classe ."' >" . utf8_encode(utf8_decode($values->libelle_classe)). "</option>";
      }

    endforeach;
  }

echo $content."*".$content1;

}else if(isset($_POST['etape'])&&($_POST['etape']==23))
{
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $classe=htmlspecialchars($_POST['classeid']);
  $matiereid=htmlspecialchars($_POST['idmatiere']);
  $teatcherid=htmlspecialchars($_POST['teatcherid']);
   //echo $_SESSION['user']['primaire'];

if($_SESSION['user']['primaire']==1)
{
  //rechercher la langue de cette matiere

  //echo $matiereid." ".$codeEtab." ".$sessionEtab;

  $datax=$matiere->getLangueMatiereTeatcher($matiereid,$codeEtab,$sessionEtab);

  $langueteatcher="";

  foreach ($datax as  $valuec):
    $langueteatcher=$valuec->langue_mat;
  endforeach;
  //echo $langueteatcher;

  //la liste des matieres de la classe selon la langue

  $datas=$matiere->getAllmatieresLangueClasse($classe,$codeEtab,$sessionEtab,$langueteatcher);

  //var_dump($datas);

  foreach ($datas as $values) :
    $matiereid=$values->id_mat;
    $matiere->AssignerteatcherToMatiere($teatcherid,$matiereid,$classe,$codeEtab,$sessionEtab);
  endforeach;



}else
{
$matiere->AssignerteatcherToMatiere($teatcherid,$matiereid,$classe,$codeEtab,$sessionEtab);
}


$_SESSION['user']['messages']=L::TheSubjectAssignatesuccessfully;

}else if(isset($_POST['etape'])&&($_POST['etape']==24))
{
  $classeid=htmlspecialchars($_POST['classeid']);

  $content="";

  $datas=$matiere->DeterminesAllMatieresOfclasses($classeid);

  $nbligne=count($datas);

  if($nbligne==0)
  {
      $content.="<option selected value=''>".L::NoMatiere." </option>";
  }else {
    //var_dump($datas);
    $content.="<option selected value='' >".L::SelectSubjects."</option>";

    foreach ($datas as $value):
        $content .= "<option value='". $value->id_mat."' >" . utf8_encode(utf8_decode($value->libelle_mat.' - '.$value->libelle_classe)). "</option>";
    endforeach;
  }

  echo $content;


}



 ?>
