<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Tache.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$tachex = new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$lasttachesassignated=$tachex->gettacherassignerTouserlast($_SESSION['user']['IdCompte']);

$lasttachesassignatedby=$tachex->gettacherassignerTouserlastby($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);


  // var_dump($lasttachesassignated);

// echo $libellesessionencours."/".$_SESSION['user']['codeEtab'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <link href="../assets2/css/style1.css" rel="stylesheet" type="text/css" />
    <style type="text/css">

#upload-button {
	width: 150px;
	display: block;
	margin: 20px auto;
}

#file-to-upload {
	display: none;
}

#pdf-main-container {
	width: 400px;
	margin: 20px auto;
}

#pdf-loader {
	display: none;
	text-align: center;
	color: #999999;
	font-size: 13px;
	line-height: 100px;
	height: 100px;
}

#pdf-contents {
	display: none;
}

#pdf-meta {
	overflow: hidden;
	margin: 0 0 20px 0;
}

#pdf-buttons {
	float: left;
}

#page-count-container {
	float: right;
}

#pdf-current-page {
	display: inline;
}

#pdf-total-pages {
	display: inline;
}

#pdf-canvas {
	border: 1px solid rgba(0,0,0,0.2);
	box-sizing: border-box;
}

#page-loader {
	height: 100px;
	line-height: 100px;
	text-align: center;
	display: none;
	color: #999999;
	font-size: 13px;
}

#download-image {
	width: 150px;
	display: block;
	margin: 20px auto 0 auto;
	font-size: 13px;
	text-align: center;
}

</style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Calendrierscolaire?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::Calendrierscolaire ?></li>
                            </ol>
                        </div>

                    </div>




					<!-- start widget -->




					<div class="state-overview">

						</div>

            <?php

                  if(isset($_SESSION['user']['tachesok']))
                  {

                    ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <?php
                  echo $_SESSION['user']['tachesok'];
                  ?>
                  <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                     </a>
                  </div>



                    <?php
                    unset($_SESSION['user']['tachesok']);
                  }

                   ?>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


          <div class="row">


            <div id="pdf-main-container">
            	<div id="pdf-loader">Loading document ...</div>
            	<div id="pdf-contents">
            		<div id="pdf-meta">
            			<div id="pdf-buttons">
            				<button id="pdf-prev"><?php echo L::PreNextimes ?></button>
            				<button id="pdf-next"><?php echo L::Nextimes ?></button>
            			</div>
            			<div id="page-count-container">Page <div id="pdf-current-page"></div> <?php echo L::SurNextimes ?> <div id="pdf-total-pages"></div></div>
            		</div>
            		<canvas id="pdf-canvas" width="500" style="margin-left:-70px" ></canvas>
            		<div id="page-loader">Loading page ...</div>
            		<a id="download-image" href="#"><?php echo L::Downloadpng ?></a>
            	</div>
            </div>

          </div>




                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
  <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets/js/pdf.js"></script>
<script src="../assets/js/pdf.worker.js"></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
  <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
  <script src="../assets2/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets2/plugins/material/material.min.js"></script>

  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->
    <script>

     jQuery(document).ready(function() {

       var etape=2;
       $.ajax({
         url: '../ajax/taches.php',
         type: 'POST',
         async:false,
         data: 'etape=' + etape,
         dataType: 'text',
         success: function (content, statut) {

           // var pdffile="../calendriers/"+String(<?php echo $_SESSION['user']['codeEtab']  ?>)+"/calendrier20202021.pdf";
           showPDF(content);

         }
       });
     });





var __PDF_DOC,
	__CURRENT_PAGE,
	__TOTAL_PAGES,
	__PAGE_RENDERING_IN_PROGRESS = 0,
	__CANVAS = $('#pdf-canvas').get(0),
	__CANVAS_CTX = __CANVAS.getContext('2d');

function showPDF(pdf_url) {
	$("#pdf-loader").show();

	PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
		__PDF_DOC = pdf_doc;
		__TOTAL_PAGES = __PDF_DOC.numPages;

		// Hide the pdf loader and show pdf container in HTML
		$("#pdf-loader").hide();
		$("#pdf-contents").show();
		$("#pdf-total-pages").text(__TOTAL_PAGES);

		// Show the first page
		showPage(1);
	}).catch(function(error) {
		// If error re-show the upload button
		$("#pdf-loader").hide();
		$("#upload-button").show();

		alert(error.message);
	});;
}

function showPage(page_no) {
	__PAGE_RENDERING_IN_PROGRESS = 1;
	__CURRENT_PAGE = page_no;

	// Disable Prev & Next buttons while page is being loaded
	$("#pdf-next, #pdf-prev").attr('disabled', 'disabled');

	// While page is being rendered hide the canvas and show a loading message
	$("#pdf-canvas").hide();
	$("#page-loader").show();
	$("#download-image").hide();

	// Update current page in HTML
	$("#pdf-current-page").text(page_no);

	// Fetch the page
	__PDF_DOC.getPage(page_no).then(function(page) {
		// As the canvas is of a fixed width we need to set the scale of the viewport accordingly
		var scale_required = __CANVAS.width / page.getViewport(1).width;

		// Get viewport of the page at required scale
		var viewport = page.getViewport(scale_required);

		// Set canvas height
		__CANVAS.height = viewport.height;

		var renderContext = {
			canvasContext: __CANVAS_CTX,
			viewport: viewport
		};

		// Render the page contents in the canvas
		page.render(renderContext).then(function() {
			__PAGE_RENDERING_IN_PROGRESS = 0;

			// Re-enable Prev & Next buttons
			$("#pdf-next, #pdf-prev").removeAttr('disabled');

			// Show the canvas and hide the page loader
			$("#pdf-canvas").show();
			$("#page-loader").hide();
			$("#download-image").show();
		});
	});
}

// Upon click this should should trigger click on the #file-to-upload file input element
// This is better than showing the not-good-looking file input element
$("#upload-button").on('click', function() {
	$("#file-to-upload").trigger('click');
});

// When user chooses a PDF file
$("#file-to-upload").on('change', function() {
	// Validate whether PDF
    if(['application/pdf'].indexOf($("#file-to-upload").get(0).files[0].type) == -1) {
        alert('Error : Not a PDF');
        return;
    }

	$("#upload-button").hide();

	// Send the object url of the pdf
  // var pdffile="../calendriers/0001/calendrier20202021.pdf";
  // showPDF(pdffile);
  // alert($("#file-to-upload").get(0).files[0]);
  // showPDF(URL.createObjectURL(pdffile));
	// showPDF(URL.createObjectURL($("#file-to-upload").get(0).files[0]));
});

// Previous page of the PDF
$("#pdf-prev").on('click', function() {
	if(__CURRENT_PAGE != 1)
		showPage(--__CURRENT_PAGE);
});

// Next page of the PDF
$("#pdf-next").on('click', function() {
	if(__CURRENT_PAGE != __TOTAL_PAGES)
		showPage(++__CURRENT_PAGE);
});

// Download button
$("#download-image").on('click', function() {
	$(this).attr('href', __CANVAS.toDataURL()).attr('download', 'calendrier<?php echo $_SESSION['user']['codeEtab'] ?>');
});

</script>

   <script>



   function terminer(idcompte,idtache)
   {
     // alert(idcompte);
     var etape=1;

     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::Haveyoufinishtasks ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::True ?>',
cancelButtonText: '<?php echo L::False ?>',
}).then((result) => {
if (result.value) {

  $.ajax({
    url: '../ajax/taches.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&idcompte=' +idcompte+'&idtache='+idtache,
    dataType: 'text',
    success: function (content, statut) {

    // location.reload();

    }
  });

}else {

}
})
   }

   function addmessages(senderid,receiverid)
   {

     var etape=1;
     $.ajax({
       url: '../ajax/chat.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&senderid=' +senderid+'&receiverid='+receiverid,
       dataType: 'text',
       success: function (content, statut) {

       document.location.href="chats.php";

       }
     });
   }

   // function SetcodeEtab(codeEtab)
   // {
   //   var etape=3;
   //   $.ajax({
   //     url: '../ajax/sessions.php',
   //     type: 'POST',
   //     async:false,
   //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
   //     dataType: 'text',
   //     success: function (content, statut) {
   //
   // window.location.reload();
   //
   //     }
   //   });
   // }

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $_SESSION['user']['codeEtab'];?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);

     var MONTHS = ['<?php echo L::JanvLib?>','<?php echo L::FevLib ?>','<?php echo L::MarsLib ?>','<?php echo L::AvriLib ?>','<?php echo L::MaiLib ?>','<?php echo L::JuneLib ?>','<?php echo L::JulLib ?>','<?php echo L::AoutLib ?>','<?php echo L::SeptLib?>','<?php echo L::OctobLib?>','<?php echo L::NovbLib?>','<?php echo L::DecemLib?>'];
        var config = {
            type: 'line',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($_SESSION['user']['codeEtab'],$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "<?php echo L::Filles ?>",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "<?php echo L::Garcons ?>"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


   });


   </script>
    <!-- end js include path -->
  </body>

</html>
