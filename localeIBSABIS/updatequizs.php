<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/Tache.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}


$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$tachex = new Tache();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;




         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);

$lasttachesassignated=$tachex->gettacherassignerTouserlast($_SESSION['user']['IdCompte']);

$lasttachesassignatedby=$tachex->gettacherassignerTouserlastby($_SESSION['user']['IdCompte']);

$nbnotificationstandby=$parents->getParentnotificationstandbyNb($compteuserid);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($compteuserid);

$courses=$etabs->getAllTeatcherquizsEtab($codeEtabAssigner,$libellesessionencours);

$courseid=$_GET['courseid'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getAllquizsdetailsEtab($courseid,$classeid,$_SESSION['user']['codeEtab'],$libellesessionencours);

foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->instruction_quiz;
  $durationcourses=$datacourses->duree_quiz;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->datelimite_quiz;
  $namecourses=$datacourses->libelle_quiz;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_quiz;
  $verouillercourses=$datacourses->verouiller_quiz;
  $teatcheridcourses=$datacourses->id_compte;
  $filescourses="";

endforeach;

// echo $teatcheridcourses;

// Nous allons recuperer les questions du quiz

  $questions=$etabs->getAllquizQuestionEtab($courseid,$classeid,$_SESSION['user']['codeEtab'],$libellesessionencours);

  $questiontrueorfalse=$etabs->getAllquizQuestionTrueOrfalse($courseid,$classeid,$_SESSION['user']['codeEtab'],$libellesessionencours,$teatcheridcourses);

  $questionmultiplechoice=$etabs->getAllquizQuestionMultiplechoice($courseid,$classeid,$_SESSION['user']['codeEtab'],$libellesessionencours,$teatcheridcourses);




  // var_dump($lasttachesassignated);

// echo $libellesessionencours."/".$_SESSION['user']['codeEtab'];

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    #radioBtn2 .active{
    color: #f0f1f3;
    background-color: #28a745;

    }
    /* #radioBtn2 .notActive{
    color: #f0f1f3;
    background-color: #e8091e;
    }

     */
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title">Modifier un quiz</div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#">Quizs</a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active">Modifier un quiz</li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       Vous devez definir la Sessionn scolaire

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


              <div class="row">
                  <div class="col-md-12 col-sm-12">
                      <div class="card card-box">
                          <div class="card-head">
                              <header></header>

                          </div>

                          <div class="card-body" id="bar-parent">
                              <form  id="FormAddAcademique" class="form-horizontal" action="../controller/quizs.php" method="post"  >
                                  <div class="form-body">


                                    	<div class="row">
          <div class="col-md-12">
          <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-info-circle"> </i> INFORMATIONS DU QUIZ</span>
        </div></br></br>
                                          <div class="col-md-12">
                                            <div class="row">

              <div class="col-md-6">
  							<div class="form-group">
         <label for="classeEtab">Classe<span class="required">*</span> :</label>
         <select class="form-control input-height" id="classeEtab" name="classeEtab" onchange="searchmatiere(<?php echo $_SESSION['user']['IdCompte']; ?>)">
           <option value=""><?php echo L::Selectclasses ?></option>
           <?php
           $i=1;
             foreach ($classes as $value):
             ?>
             <option <?php if($classeid==$value->id_classe){ echo "selected";} ?> value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

             <?php
           $i++;
          endforeach;
            ?>
         </select>
                   </div>
  							 </div>


  									 <div class="col-md-6">
  	 									<div class="form-group">
  	 			<label for="matclasse">Matière <span class="required">*</span> :</label>
          <select class="form-control input-height" id="matclasse" name="matclasse" style="width:100%;" onchange="searchcodeEtab(<?php echo $_SESSION['user']['IdCompte']; ?>)">
              <option value=""><?php echo L::SelectSubjects ?></option>


          </select>
          <input type="hidden" name="etape" id="etape" value="2">
          <input type="hidden" name="codeEtab" name="codeEtab" value="<?php echo $_SESSION['user']['codeEtab']; ?>">
          <input type="hidden" name="sessionEtab" name="sessionEtab" value="<?php echo $libellesessionencours; ?>">
          <input type="hidden" name="courseid" name="courseid" value="<?php echo $courseid; ?>">
  	                      </div>
  	 									 </div>
                       <div class="col-md-6">
                        <div class="form-group">
            <label for="libellecourse">Libellé du devoir<span class="required">*</span> :</label>
                   <input type="text" class="form-control" name="libellecourse" id="libellecourse" value="<?php echo utf8_decode(utf8_encode($namecourses)); ?>" size="32" maxlength="225" />

                            </div>
                         </div>
                         <div class="col-md-6">
                          <div class="form-group">
              <label for="durationcourse">Durée du quiz<span class="required">*</span> :</label>
                     <input type="text" class="form-control" name="durationcourse" id="durationcourse" value="<?php echo $durationcourses; ?>" size="32" maxlength="225" />

                              </div>
                           </div>
  										 <div class="col-md-6">
  		 									<div class="form-group">
  		 			<label for="datecourse">Date limite quiz <span class="required">*</span> :</label>
  		             <input type="text" class="form-control" name="datecourse" id="datecourse" data-mask="99/99/9999" value="<?php echo date_format(date_create($datercourses), "d/m/Y");?>" size="32" maxlength="225" />
                   <span class="help-block"><?php echo L::Datesymbole ?></span>
                            </div>
  		 									 </div>
                         <div class="col-md-6">
                          <div class="form-group">
              <label for="datecourse">verrouiller le quiz du côté de l’élève  après l’échéance <span class="required">*</span> :</label>
              <div class="form-group row" id="RowbtnInscrip">

               <div class="col-sm-7 col-md-7">
               <div class="input-group">
               <div id="radioBtn" class="btn-group">
               <a class="btn  btn-sm active" id="btn1" data-toggle="verouiller" data-title="1" onclick="verouillerOui()">OUI</a>
               <a class="btn  btn-sm notActive" id="btn2" data-toggle="verouiller" data-title="0" onclick="verouillerNon()">NON</a>
               </div>
               <input type="hidden" name="verouiller" id="verouiller" value="<?php echo $statutcourses; ?>">
               </div>
               </div>
               </div>
                              </div>
                           </div>

                           <div class="col-md-12">
                            <div class="form-group">
                <label for="detailscourse">Instruction du quiz <span class="required">*</span> :</label>

                       <textarea class="form-control" rows="8" name="detailscourse" id="detailscourse" placeholder=""><?php echo $descricourses;  ?></textarea>
                                </div>
                             </div>
                             <div class="col-md-12">
                             <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-list"> </i> LISTE des questions VRAI / FAUX</span>
                           </div></br></br>
                           <div class="col-md-12">
                             <button type="button" data-toggle="modal" data-target="#mediumModel" class="btn btn-success pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouvelle question</button>
                            </div></br></br>
                            <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
                              <thead>
                                  <tr>
                                    <th> N° </th>
                                    <th style="text-align:center"> QUESTIONS </th>
                                    <th style="text-align:center"> SOLUTIONS </th>
                                    <th style="text-align:center"> ACTIONS </th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php
                                $i=1;
                                foreach ($questiontrueorfalse as  $value):
                                  $solutionquestion=$etabs->getPropositionsOfQuestionsolution($value->id_quest);
                                  foreach ($solutionquestion as $solution):
                                    $libellesolution=$solution->libelle_proprep;
                                    $solutionid=$solution->id_proprep;
                                  endforeach;
                                  ?>
                                  <tr>
                                    <td><?php echo $i; ?></td>
                                    <td style="text-align:center"><?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></td>
                                    <td style="text-align:center">

                                      <?php
                                      if($libellesolution=="VRAI")
                                      {
                                        ?>
                                        <span class="label label-sm label-success"><?php echo $libellesolution ?></span>
                                        <?php
                                      }else {
                                        ?>
                                        <span class="label label-sm label-danger"><?php echo $libellesolution ?></span>
                                        <?php
                                      }
                                       ?>
                                    </td>
                                    <td style="text-align:center">
                                      <?php
                                      if($libellesolution=="VRAI")
                                      {
                                        ?>
                                        <button type="button" onclick="changetofalse(<?php echo $solutionid ?>,<?php echo $value->id_quest ?>)"  class="btn btn-warning  btn-xs" title="Modifier en FAUX"> <i class="fa fa-times"></i> </button>
                                        <?php
                                      }else {
                                        ?>
                                        <button type="button" onclick="changetotrue(<?php echo $solutionid ?>,<?php echo $value->id_quest ?>)"  class="btn btn-success  btn-xs" title="Modifier en VRAI"> <i class="fa fa-check"></i> </button>
                                        <?php
                                      }
                                       ?>

                                      <button type="button" onclick=""  class="btn btn-danger btn-xs" title="Supprimer la question"><i class="fa fa-trash"></i> </button>
                                    </td>
                                  </tr>
                                  <?php
                                  $i++;
                                endforeach;
                                 ?>
                              </tbody>

                            </table>

                             </div>

                           <div class="col-md-12">
                           <span class="label label-lg label-warning"  style="text-align:center;"> <i class="fa fa-list"> </i> LISTE des questions a choix multiple</span>
                         </div></br></br>
                         <div class="col-md-12">
                           <button type="button" data-toggle="modal" data-target="#mediumModel1" class="btn btn-success pull-right" style="border-radius:5px;height:35px"> <i class="fa fa-plus"></i>Nouvelle question</button>
                          </div></br></br>
                          <div class="col-md-12">
                          <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="">
                            <thead>
                                <tr>
                                  <th> N° </th>
                                  <th style="text-align:center"> PROPOSITIONS REPONSES </th>
                                  <th style="text-align:center"> SOLUTIONS </th>
                                  <th style="text-align:center"> ACTIONS </th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php

                              foreach ($questionmultiplechoice as  $value):

                                ?>
                                <tr style="background-color: #87c2de;">

                                  <td style="text-align:center" colspan="4"><?php echo utf8_decode(utf8_encode($value->libelle_quest)) ?></td>
                                </tr>

                                <?php
                                //nous allons recuperer les propositions
                                  $propositionsreponses=$etabs->getPropositionsOfQuestions($value->id_quest);
                                  $i=1;
                                  foreach ($propositionsreponses as $propositions):
                                    $cocher=$propositions->valeur_proprep;
                                    ?>
                                    <tr>
                                      <td><?php echo $i; ?></td>
                                      <td style="text-align:center"><?php echo utf8_decode(utf8_encode($propositions->libelle_proprep)) ?></td>
                                      <td style="text-align:center">
                                        <?php
                                        if($cocher==1)
                                        {
                                          ?>
                                          <i class="fa fa-check" style="color:green"></i>
                                          <?php
                                        }else {
                                          ?>
                                          <i class="fa fa-window-close-o" style="color:red;"></i>
                                          <?php
                                        }
                                         ?>
                                      </td>

                                        <td style="text-align:center">
                                          <?php
                                          if($cocher==1)
                                          {
                                            ?>
                                            <button type="button" onclick="decocherproposition(<?php echo $propositions->id_proprep ?>,<?php echo $value->id_quest ?>)"  class="btn btn-warning btn-xs" title="décocher la proposition de réponse"> <i class="fa fa-window-close-o"></i></button>

                                            <?php
                                          }else {
                                            ?>
                                            <button type="button" onclick="cocherproposition(<?php echo $propositions->id_proprep ?>,<?php echo $value->id_quest ?>)"  class="btn btn-success btn-xs" title="cocher la proposition de réponse"> <i class="fa fa-check"></i> </button>
                                            <?php
                                          }
                                           ?>
                                          <button type="button" onclick=""  class="btn btn-danger btn-xs" title="supprimer la proposition de réponse"> <i class="fa fa-trash"></i> </button>
                                        </td>

                                    </tr>
                                    <?php
                                    $i++;
                                  endforeach;

                              endforeach;
                               ?>
                            </tbody>

                          </table>

                           </div>







  								 </div>
                                          </div>

                                    </div>














              </div>
              <div class="form-actions">
                                    <div class="row">
                                        <div class="offset-md-3 col-md-9">
                                          <button type="submit" class="btn btn-info">Modifier</button>

                                            <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                        </div>
                                      </div>
                                   </div>
                              </form>
                              <div class="modal fade" id="mediumModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-header">
					                <h4 class="modal-title" id="exampleModalLabel">AJOUTER UNE NOUVELLE QUESTION</h4>
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                    <span aria-hidden="true">&times;</span>
					                </button>
					            </div>
					            <div class="modal-body">
					               <form class="" action="../controller/quizs.php" method="post" id="FormTrueOrFalse">
                           <div class="col-md-12">
                            <div class="form-group">
                <label for="libellesquestTrue">Libellé de la question<span class="required">*</span> :</label>
                       <input type="text" class="form-control" name="libellesquestTrue" id="libellesquestTrue" value="" size="32" maxlength="225" />
                       <input type="hidden" name="idquiztrue" id="idquiztrue" value="<?php echo $_GET['courseid']; ?>">
                       <input type="hidden" name="etape" id="etape" value="3">
                       <input type="hidden" name="classequiztrue" id="classequiztrue"  value="<?php echo $_GET['classeid']; ?>">
                                </div>
                             </div>
                             <div class="col-md-12">
                              <div class="form-group">
                  <label for="pointquestTrue">Point<span class="required">*</span> :</label>
                         <input type="text" class="form-control" name="pointquestTrue" id="pointquestTrue" value="" size="32" maxlength="225" />

                                  </div>
                               </div>
                               <div class="col-sm-7 col-md-7">
                               <div class="input-group">
                               <div id="radioBtnchoice" class="btn-group">
                               <a class="btn  btn-sm active" id="btnTrue" data-toggle="answer" data-title="1" onclick="vrai()">OUI</a>
                               <a class="btn  btn-sm notActive" id="btnFalse" data-toggle="answer" data-title="0" onclick="faux()">NON</a>
                               </div>
                               <input type="hidden" name="answer" id="answer" value="1">
                               </div>
                               </div>
                               </div>
                             <div class="form-actions">
                                                   <div class="row">
                                                       <div class="offset-md-3 col-md-9">
                                                         <button type="submit" class="btn btn-info"><?php echo L::AddMenu?></button>

                                                           <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Annuler</button>
                                                       </div>
                                                     </div>
                                                  </div>

                         </form>
					            </div>

					        </div>
					    </div>
					</div>


          <div class="modal fade" id="mediumModel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
      <h4 class="modal-title" id="exampleModalLabel">AJOUTER UNE NOUVELLE QUESTION</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
     <form class="" action="../controller/quizs.php" method="post" id="FormMultiple">
       <div class="col-md-12">
        <div class="form-group">
<label for="libellesquestmuluti">Libellé de la question<span class="required">*</span> :</label>
   <input type="text" class="form-control" name="libellesquestmuluti" id="libellesquestmuluti" value="" size="32" maxlength="225" />
   <input type="hidden" name="idquizmulti" id="idquizmulti" value="<?php echo $_GET['courseid']; ?>">
   <input type="hidden" name="classequizmulti" id="classequizmulti"  value="<?php echo $_GET['classeid']; ?>">
   <input type="hidden" name="etape" id="etape" value="4">
            </div>
         </div>
         <div class="col-md-12">
          <div class="form-group">
<label for="pointquestMulti">Point<span class="required">*</span> :</label>
     <input type="text" class="form-control" name="pointquestMulti" id="pointquestMulti" value="" size="32" maxlength="225" />

              </div>
           </div>
           <div class="col-md-12">
            <div class="form-group">
  <label for="propositionNbMulti">Proposition de réponse (Nombre)<span class="required">*</span> :</label>
       <input type="text" class="form-control" name="propositionNbMulti" id="propositionNbMulti" value="" onchange="createMultiple()" size="32" maxlength="225" />
       <input type="hidden" id="nbmultipleprop" name="nbmultipleprop" value="0">
       <input type="hidden" name="concatmultipleprop" id="concatmultipleprop" value="">
       <input type="hidden" name="concatnbmultipleprop" id="concatnbmultipleprop" value="">
                </div>
             </div>
             <div class="col-md-12">
                <table class="table" id="fields_proposition" border=0>  </table>
             </div>
         <div class="form-actions">
                               <div class="row">
                                   <div class="offset-md-3 col-md-9">
                                     <button type="submit" class="btn btn-info"><?php echo L::AddMenu?></button>

                                       <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Annuler</button>
                                   </div>
                                 </div>
                              </div>

     </form>
  </div>

</div>
</div>
</div>


<div class="modal fade" id="mediumModel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="exampleModalLabel">AJOUTER UN NOUVEL EXERCICE</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<form class="" action="../controller/courses.php" method="post" id="FormHomecourses">
<div class="col-md-12">
<div class="form-group">
<label for="libellehome">Libellé de l'exercice<span class="required">*</span> :</label>
<input type="text" class="form-control" name="libellehome" id="libellehome" value="" size="32" maxlength="225" />

  </div>
</div>
<div class="form-actions">
                     <div class="row">
                         <div class="offset-md-3 col-md-9">
                           <button type="submit" class="btn btn-info"><?php echo L::AddMenu?></button>

                             <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Annuler</button>
                         </div>
                       </div>
                    </div>

</form>
</div>

</div>
</div>
</div>



                          </div>
                      </div>
                  </div>
              </div>
          </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <!-- start chat sidebar -->

                        <div class="chat-sidebar-container" data-close-on-body-click="false">
                        <div class="chat-sidebar">
                          <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                                  class="material-icons">
                                  chat</i>Chat
                                <!-- <span class="badge badge-danger">4</span> -->
                              </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <!-- Start User Chat -->
                            <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                              id="quick_sidebar_tab_1"> -->
                              <div class="chat-sidebar-chat "
                                >
                              <div class="chat-sidebar-list">
                                <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                                  data-wrapper-class="chat-sidebar-list">
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($onlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                                      {
                                        ?>
                                        <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                            width="35" height="35" alt="...">
                                          <i class="online dot red"></i>
                                          <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                            <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                            <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                          </div>
                                        </li>
                                        <?php
                                      }
                                      ?>

                                      <?php
                                    endforeach;
                                     ?>

                                  </ul>
                                  <div class="chat-header">
                                    <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                                  </div>
                                  <ul class="media-list list-items">
                                    <?php

                                    foreach ($offlineUsers as  $valueUsers):
                                      $tofuser=$valueUsers->photo_compte;
                                      if(strlen($tofuser)>0)
                                      {
                                        $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                                      }else {
                                        $lientofuser="../photo/user5.jpg";
                                      }
                                      ?>
                                      <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                          width="35" height="35" alt="...">
                                        <i class="offline dot"></i>
                                        <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                          <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                          <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                                        </div>
                                      </li>
                                      <?php
                                    endforeach;
                                     ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <!-- End User Chat -->
                          </div>
                        </div>
                      </div>
                        <!-- end chat sidebar -->
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/dropify.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function cocherproposition(propositionid,questionid)
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment coher la proposition de réponse de cette question",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Cocher',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/admin.php?etape=3&compte="+id;


var valeurproposition=1;
var etape=5;

$.ajax({
  url: '../ajax/quizs.php',
          type: 'POST',
          async:false,
          data: 'etape=' + etape+'&propositionid='+propositionid+'&questionid='+questionid+'&valeurproposition='+valeurproposition,
          dataType: 'text',
          success: function (content, statut) {

             location.reload();

          }
});

}else {

}
})
   }

   function decocherproposition(propositionid,questionid)
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment décoher la proposition de réponse de cette question",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Décocher',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/admin.php?etape=3&compte="+id;


var valeurproposition=0;
var etape=4;

$.ajax({
  url: '../ajax/quizs.php',
          type: 'POST',
          async:false,
          data: 'etape=' + etape+'&propositionid='+propositionid+'&questionid='+questionid+'&valeurproposition='+valeurproposition,
          dataType: 'text',
          success: function (content, statut) {

             location.reload();

          }
});

}else {

}
})
   }

   function changetofalse(propositionid,questionid)
   {


     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment modifier la réponse de cette question par FAUX",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/admin.php?etape=3&compte="+id;

var newlibelle="FAUX";
var valeurproposition=1;
var etape=2;

$.ajax({
  url: '../ajax/quizs.php',
          type: 'POST',
          async:false,
          data: 'libelle=' +newlibelle+ '&etape=' + etape+'&propositionid='+propositionid+'&questionid='+questionid+'&valeurproposition='+valeurproposition,
          dataType: 'text',
          success: function (content, statut) {

             location.reload();

          }
});

}else {

}
})
   }

   function changetotrue(propositionid,questionid)
   {
     Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment modifier la réponse de cette question par FAUX",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="../controller/admin.php?etape=3&compte="+id;

var newlibelle="VRAI";
var valeurproposition=1;
var etape=3;

$.ajax({
  url: '../ajax/quizs.php',
          type: 'POST',
          async:false,
          data: 'libelle=' +newlibelle+ '&etape=' + etape+'&propositionid='+propositionid+'&questionid='+questionid+'&valeurproposition='+valeurproposition,
          dataType: 'text',
          success: function (content, statut) {

             location.reload();

          }
});

}else {

}
})
   }

   function CheckProp(nouveau)
   {
     if( $('#chk_proposition'+nouveau).is(':checked') ){
       $('#chk_proposition'+nouveau).val(1);
   } else {
       $('#chk_proposition'+nouveau).val(0);
   }
   }

   function addpropositions(nouveau)
   {
     var ligne="";
     var concat="";

     for(var i=1;i<=nouveau;i++)
     {
       concat=concat+i+"@";
       ligne=ligne+"<tr id=\"LigneProposition"+i+"\">";

       ligne=ligne+"<td>";
       ligne=ligne+"<input type=\"checkbox\" id=\"chk_proposition"+i+"\" name=\"chk_proposition"+i+"\" value=\"0\" onclick=\"CheckProp("+i+")\"/>";
       ligne=ligne+"</td>";

       ligne=ligne+"<td>";
         ligne=ligne+"<input type=\"text\" class=\"form-control\" name=\"libelle_proposition"+i+"\" value=\"\" />";
       ligne=ligne+"</td>";

       ligne=ligne+"</tr>";
     }

     $("#fields_proposition").append(ligne);

     $("#nbmultipleprop").val(nouveau);
     $("#concatmultipleprop").val(concat);

     $('#libelle_proposition'+nouveau).rules( "add", {
         required: true,
         messages: {
         required: "Merci de renseigner la proposition de reponse"
   }
       });

   }

   function createMultiple()
   {
     var nouveau=$("#propositionNbMulti").val();
     var nboldproposition=$("#nbmultipleprop").val();
     var concatmultipleprop=$("#concatmultipleprop").val();
     if(nboldproposition==0)
     {
       //nouveau donc nous allons ajouter
       addpropositions(nouveau);

     }else {
       //ancien donc nous allons supprimer les anciens et ajouter le nouveau

       for(var i=1;i<=nboldproposition;i++)
       {
         $("#LigneProposition"+i).remove();
       }
        addpropositions(nouveau);
     }
   }


   function verouillerOui()
   {
      $("#verouiller").val(1);
     // $("#btn1").removeClass('notActive').addClass('active');
     // $("#btn2").removeClass('active').addClass('notActive');
     $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#28a745"});
     $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#e8091e"});
   }

   function verouillerNon()
   {
     $("#verouiller").val(0);
     // $("#btn2").removeClass('notActive').addClass('active');
     // $("#btn1").removeClass('active').addClass('notActive');
     $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#28a745"});
     $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#e8091e"});
   }



   function searchmatiereInit()
   {

     var classe="<?php echo $classeid; ?>";
     var teatcherId="<?php echo $teatcheridcourses; ?>";
     var matiereid="<?php echo $matiereidcourses; ?>";
     var etape=17;


   $.ajax({

        url: '../ajax/matiere.php',
        type: 'POST',
        async:true,
        data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiereid='+matiereid,
        dataType: 'text',
        success: function (content, statut) {


          $("#matclasse").html("");
          $("#matclasse").html(content);

        }
      });
   }
 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}

function defaultOne()
{
  $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

function defaultTwo()
{
  $("#radioBtn #btn2").css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtn #btn1").css({"color": "#f0f1f3", "background-color": "#e8091e"});
}

$('#durationcourse').bootstrapMaterialDatePicker
({
  date: false,
  shortTime: false,
  format: 'HH:mm',
  lang: 'fr',
 cancelText: '<?php echo L::AnnulerBtn ?>',
 okText: 'OK',
 clearText: '<?php echo L::Eraser ?>',
 nowText: '<?php echo L::Now ?>'

});

function  verouillerInit()
{
  var statutcourses="<?php echo $verouillercourses; ?>";

  if(statutcourses==1)
  {
    defaultOne();
  }else {
    defaultTwo();
  }

}


$('#mediumModel').on('shown.bs.modal', function () {

// console.log("bonjour");

$('#mediumModel #libellesquestTrue').val("");

$('#mediumModel #pointquestTrue').val(0);

$("#radioBtnchoice #btnTrue").css({"color": "#f0f1f3", "background-color": "#28a745"});
$("#radioBtnchoice #btnFalse").css({"color": "#f0f1f3", "background-color": "#e8091e"});








});

$('#mediumModel1').on('shown.bs.modal', function () {

// console.log("bonjour");

$('#mediumModel1 #libellesquestmuluti').val("");

$('#mediumModel1 #pointquestMulti').val(0);
$('#mediumModel1 #propositionNbMulti').val(0);








});

function vrai()
{
  $("#answer").val(1);
  $("#radioBtnchoice #btnTrue").css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtnchoice #btnFalse").css({"color": "#f0f1f3", "background-color": "#e8091e"});

}

function faux()
{
  $("#answer").val(0);
  $("#radioBtnchoice #btnFalse").css({"color": "#f0f1f3", "background-color": "#28a745"});
  $("#radioBtnchoice #btnTrue").css({"color": "#f0f1f3", "background-color": "#e8091e"});

}



 $(document).ready(function() {

   searchmatiereInit();

   verouillerInit();

   $("#FormMultiple").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{


       libellesquestmuluti:"required",
       pointquestMulti:{
         required: true,
         number: true,
          min:1
       },
       propositionNbMulti:{
         required: true,
         number: true,
          min:1
       }


     },
   messages: {
     libellesquestmuluti:"Merci de renseigner le libellé de la question",
     pointquestMulti:{
       required:"Merci de renseigner le nombre de point",
       number:"Merci de renseigner uniquement des chiffres",
       min:"Le nombre de point doit etre supérieur à 0"
     },
     propositionNbMulti:{
       required:"Merci de renseigner le nombre de proposition de réponse",
       number:"Merci de renseigner uniquement des chiffres",
       min:"Le nombre de proposition doit etre supérieur à 0"
     }

   },
   submitHandler: function(form) {

   form.submit();



   }

   });

$("#FormTrueOrFalse").validate({

  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    libellesquestTrue:"required",
    pointquestTrue:{
      required: true,
      number: true,
       min:1
    }

  },
messages: {
  libellesquestTrue:"Merci de renseigner le libellé de la question",
  pointquestTrue:{
    required:"Merci de renseigner le nombre de point",
    number:"Merci de renseigner uniquement des chiffres",
    min:"Le nombre de point doit etre supérieur à 0"
  }
},
submitHandler: function(form) {

form.submit();



}

});

   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required",
       libellecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"Merci de renseigner la durée du quiz",
     detailscourse:"Merci de renseigner les instructions du quiz",
     datecourse:"Merci de renseigner la date limite du quiz",
     libellecourse:"Merci de renseigner le libellé du quiz"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });








 });

 </script>
    <!-- end js include path -->
  </body>

</html>
