<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../controller/functions.php');
require_once('../class/ChiffresEnLettres.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$codeEtab=$_GET['codeEtab'] ;
$idClasse=$_GET['classeEtab'];
$session=$_GET['sessionEtab'];

$etabs=new Etab();
$student=new Student();
$classe=new Classe();
$lettre=new ChiffreEnLettre();

$libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($idClasse,$session);
$logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
$etablissementType=$etabs->DetermineTypeEtab($codeEtab);


$students=$student->getAllStudentOfClassesId($idClasse,$session);

//nous allons faire nos différents calculs

//le nombre d'eleve de cette classe


$nbclassestudents=$classe->getAllStudentOfThisClassesNb($idClasse,$session);

//le nombre de fille affecté

$nbaffectefilles=$classe->getAllStudentFilleAffectOfThisClassesNb($idClasse,$session);

//nombre de fille non Affecté

$nbnonaffectefilles=$classe->getAllStudentFilleNAffectOfThisClassesNb($idClasse,$session);

//nombre de mec affectés

$nbaffectemecs=$classe->getAllStudentMecAffectOfThisClassesNb($idClasse,$session);

//nombre de mec non affectés

$nbnonaffectemecs=$classe->getAllStudentMecNAffectOfThisClassesNb($idClasse,$session);

//nombre de fille redoublante

$nbredoubfilles=$classe->getAllStudentFilleRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublante fille

$nbnredoubfilles=$classe->getAllStudentFilleNRedtOfThisClassesNb($idClasse,$session);

//nombre de mec redoublant

$nbredoubmecs=$classe->getAllStudentMecRedtOfThisClassesNb($idClasse,$session);

//nombre non redoublant mec

$nbnredoubmecs=$classe->getAllStudentMecNRedtOfThisClassesNb($idClasse,$session);

$montantpaiebaseclasses=$student->getmontantpaiebaseclasse($idClasse,$session);

foreach ($montantpaiebaseclasses as  $amountvalue) :

$montantinscriptionsclasses=$amountvalue->inscriptionmont_classe;
// $montantreinscriptionsclasses=$amountvalue->
$montantscolaritesclasses=$amountvalue->scolarite_classe;

endforeach;


 $versementStudent=$student->getStudentversementAllInfosInscription($codeEtab,$session,$_GET['compte']);

 foreach ($versementStudent as  $valueVersement):
   $deposant=$valueVersement->deposant_versement;
   $motif=$valueVersement->motif_versement;
   $mode=$valueVersement->mode_versement;
   $montant=$valueVersement->montant_versement;
   $solde=$valueVersement->solde_versement;
   $code=$valueVersement->code_versement;
   $dateversement=$valueVersement->date_versement;
   $beneficiaire=$valueVersement->nom_eleve." ".$valueVersement->prenom_eleve;
   $beneficiairematri=$valueVersement->matricule_eleve;
   $beneficiaireclasse=$valueVersement->libelle_classe;
   $scolaritesmontant=$valueVersement->scolarite_classe;
   $motifid=$valueVersement->motifid_versement;
   // $libelleactivities="";
   endforeach;


   $versementStudent2=$student->getStudentversementAllInfoScolarite($codeEtab,$session,$_GET['compte']);

   foreach ($versementStudent2 as  $value):
     $deposants=$value->deposant_versement;
     $modes=$value->mode_versement;
     $montants=$value->montant_versement;
     $soldescol=$value->solde_versement;
     $codes=$value->code_versement;
     $dateversements=$value->date_versement;
     $beneficiairematris=$value->matricule_eleve;
     $beneficiaireclasses=$value->libelle_classe;
     $scolaritesmontants=$value->scolarite_classe;
     $motifids=$value->motifid_versement;
     // $libelleactivities="";
   endforeach;

require('fpdf/fpdf.php');

class PDF extends FPDF
{



function Footer()
{
    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro et nombre de pages
    // $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    // $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

// Tableau simple
function BasicTable($header, $data)
{
    // En-tête
    foreach($header as $col)
        $this->Cell(40,7,$col,1);
    $this->Ln();
    // Données
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }
}
}


$pdf = new FPDF();
$pdf->AddPage('P', 'A5');
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('Arial', '', 12);
$pdf->SetTopMargin(10);
$pdf->SetLeftMargin(10);
$pdf->SetRightMargin(10);

$pdf->Ln(17);
$pdf -> SetX(2);
$pdf->Image("../logo_etab/".$codeEtab."/".$logoEtab,-1,3,27,27);


/* --- Cell --- */
$pdf->SetXY(27, 12);
$pdf->SetFont('Arial', 'B', 16);
$pdf->Cell(115, 8, utf8_decode('FACTURE: N°001220'), 1, 1, 'C', false);
/* --- Cell --- */
$pdf->SetXY(6, 35);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(38, 10, 'ELEVE', 0, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(30, 35);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(38, 10, ': '.utf8_decode($beneficiaire), 0, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(6, 43);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(37, 10,utf8_decode('N°CLIENT'), 0, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(30, 43);
$pdf->SetTextColor(255, 0, 0);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(37, 10, ': 001', 0, 1, 'L', false);
$pdf->SetTextColor(0);
/* --- Cell --- */
$pdf->SetXY(6, 51);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(33, 7, 'CLASSE', 0, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(30, 51);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(33, 7, ': '.utf8_decode($beneficiaireclasse), 0, 1, 'L', false);
//////////////////////////////////////////////////////
//LIGNE FRAIS INSCRIPTION//////
/* --- Cell --- */
$pdf->SetXY(5, 65);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(138, 7,utf8_decode(strtoupper('frais inscription')) , 1, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(5, 72);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(69, 25,utf8_decode('Versement') , 1, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(74, 72);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(69, 25,number_format($montant,0,',',' ')  , 1, 1, 'L', false);
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////
//LIGNE FRAIS SCOLARITE//////
/* --- Cell --- */
$pdf->SetXY(5, 97);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(138, 7,utf8_decode(strtoupper('frais scolarite')) , 1, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(5, 104);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(69, 25,utf8_decode('Versement') , 1, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(74, 104);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(69, 25,number_format($soldescol,0,',',' ')  , 1, 1, 'L', false);
///////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
//LIGNE FRAIS DU TOTAL//////
/* --- Cell --- */
$pdf->SetXY(5, 129);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(69, 7,utf8_decode(strtoupper('total')) , 0, 1, 'R', false);
/* --- Cell --- */
$total=$montant+$soldescol;
$pdf->SetXY(74, 129);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(69, 7,number_format($total,0,',',' ').' '.'FCFA'  , 1, 1, 'R', false);
///////////////////////////////////////////////////////////


/////////////////////////////////////////////////////
//LIGNE ARRETE LA FACTURE//////
/* --- Cell --- */
$pdf->SetXY(10, 140);
$pdf->SetFont('Arial', 'I', 11);
$pdf->MultiCell(0,6,utf8_decode(ucfirst('Arrêté la présente facture à la somme de:')) .' '.utf8_decode(ucwords($lettre->Conversion($total))).' Francs CFA',1,'L',0);
// $pdf->MultiCell(69, 12,utf8_decode(ucfirst('Arrêté la présente facture à la somme de:')) .' '.utf8_decode(ucwords($lettre->Conversion($total))).' Francs CFA' , 0, 1, 'L');
$pdf->SetXY(10, 14);
// $pdf->Cell(105, 20, 'Arreté la présente facture la somme de un million deux cent cinquante mille', 0, 1, 'L', false);
/* --- Cell --- */
$pdf->SetXY(74, 155);
$pdf->SetFont('Arial', 'BU', 13);
$pdf->Cell(69, 7,utf8_decode(strtoupper('service comptabilite')) , 0, 1, 'C', false);
///////////////////////////////////////////////////////////

$pdf->Output();
?>
