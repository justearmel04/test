<?php
session_start();

require_once('../class/User.php');

require_once('../class/Etablissement.php');

require_once('../class/LocalAdmin.php');

require_once('../class/Parent.php');

require_once('../class/Classe.php');

require_once('../class/Etablissement.php');

require_once('../class/Teatcher.php');

require_once('../class/Matiere.php');

require_once('../class/Sessionsacade.php');

require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();



$session= new Sessionacade();



$emailUti=$_SESSION['user']['email'];

$userId=$_SESSION['user']['IdCompte'];

$classe=new Classe();

$user=new User();

$etabs=new Etab();

$teatcher=new Teatcher();

$student=new Student();

$matiere=new Matiere();

$localadmins= new Localadmin();

$parents=new ParentX();

$compteuserid=$_SESSION['user']['IdCompte'];

$imageprofile=$user->getImageProfilebyId($compteuserid);

$logindata=$user->getLoginProfilebyId($compteuserid);

$tablogin=explode("*",$logindata);





if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}



$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);

$datastat=$user->getStatisById($codeEtabAssigner);

$tabstat=explode("*",$datastat);



//le nombre des eleves de cet etablissement

$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);



$alletab=$etabs->getAllEtab();

$locals=$localadmins->getAllAdminLocal();

$allparents=$parents->getAllParent();

$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$codesEtab=$schoolsofassign;

$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);

$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);

$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);

$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);



//recuperer tous les cntrole de cet etablissement







$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);

$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);





if($nbsessionOn>0)

{

  //recuperer la session en cours

  $sessionencours=$session->getSessionEncours($codeEtabLocal);

  $tabsessionencours=explode("*",$sessionencours);

  $libellesessionencours=$tabsessionencours[0];

  $sessionencoursid=$tabsessionencours[1];

  $typesessionencours=$tabsessionencours[2];

  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);

  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);

  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);

}



$controles=$matiere->getAllControleMatiereOfThisSchool($codeEtabLocal,$libellesessionencours);



$scolarites=$etabs->DetermineAllversementstudent($codeEtabLocal,$libellesessionencours,$_GET['student'],$_GET['classe']);

$transportations=$etabs->getAlltransportationSession($codeEtabLocal,$libellesessionencours);









 ?>

<!DOCTYPE html>

<html lang="en">

<!-- BEGIN HEAD -->



<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <title><?php echo L::Titlesite ?></title>

    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



    <!-- google font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

	<!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

	<!--bootstrap -->

  <!--bootstrap -->

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

    <!-- data tables -->

        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

  <!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />

  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

 </head>

 <!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">

    <div class="page-wrapper">

        <!-- start header -->

		<?php

    include("header.php");



    ?>

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

 			<!-- start sidebar menu -->

 			<?php

				include("menu.php");

			?>

			 <!-- end sidebar menu -->

			<!-- start page content -->

      <div class="page-content-wrapper">

          <div class="page-content">

              <div class="page-bar">

                  <div class="page-title-breadcrumb">

                      <div class=" pull-left">

                          <div class="page-title"><?php echo L::Versements ?></div>

                      </div>

                      <ol class="breadcrumb page-breadcrumb pull-right">

                        <li><a class="parent-item" href="#"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                          <li><a class="parent-item" href="#"><?php echo L::FichestudMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>

                          </li>

                          <li class="active"><?php echo L::Versements ?></li>

                      </ol>

                  </div>

              </div>



              <?php



                    if(isset($_SESSION['user']['addctrleok']))

                    {



                      ?>

                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">

                    <?php

                    //echo $_SESSION['user']['addetabok'];

                    ?>

                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                       </a>

                    </div-->

              <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

              <script src="../assets/js/sweetalert2.min.js"></script>



                <script>



                Swal.fire({

                type: 'success',

                title: '<?php echo L::Felicitations ?>',

                text: '<?php echo $_SESSION['user']['addctrleok']; ?>',



                })

                </script>

                      <?php

                      unset($_SESSION['user']['addctrleok']);

                    }



                     ?>









              <?php



                    if(isset($_SESSION['user']['deletesubjectok']))

                    {



                      ?>

                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">

                    <?php

                    //echo $_SESSION['user']['addetabok'];

                    ?>

                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                       </a>

                    </div-->

  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <script src="../assets/js/sweetalert2.min.js"></script>



                <script>



                Swal.fire({

              title: '<?php echo L::Felicitations ?>',

              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",

              type: 'success',

              showCancelButton: false,

              confirmButtonColor: '#3085d6',

              cancelButtonColor: '#d33',

              confirmButtonText: '<?php echo L::Okay ?>',



              }).then((result) => {

              if (result.value) {

              window.location.reload();

              }

              })





                </script>

                      <?php

                      unset($_SESSION['user']['deletesubjectok']);

                    }



                     ?>





              <?php



                    if(isset($_SESSION['user']['updatesubjectok']))

                    {



                      ?>

                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">

                    <?php

                    //echo $_SESSION['user']['addetabok'];

                    ?>

                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                       </a>

                    </div-->

  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <script src="../assets/js/sweetalert2.min.js"></script>



                <script>



                Swal.fire({

              title: '<?php echo L::Felicitations ?> ',

              text: "<?php echo $_SESSION['user']['updatesubjectok']; ?>",

              type: 'success',

              showCancelButton: false,

              confirmButtonColor: '#3085d6',

              cancelButtonColor: '#d33',

              confirmButtonText: '<?php echo L::Okay ?>',



              }).then((result) => {

              if (result.value) {

              window.location.reload();

              }

              })





                </script>

                      <?php

                      unset($_SESSION['user']['updatesubjectok']);

                    }



                     ?>

              <?php



                    if(isset($_SESSION['user']['addsubjectok']))

                    {



                      ?>

                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">

                    <?php

                    //echo $_SESSION['user']['addetabok'];

                    ?>

                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                       </a>

                    </div-->

  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <script src="../assets/js/sweetalert2.min.js"></script>



                <script>





                Swal.fire({

              title: '<?php echo L::Felicitations ?>',

              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",

              type: 'success',

              showCancelButton: false,

              confirmButtonColor: '#3085d6',

              cancelButtonColor: '#d33',

              confirmButtonText: '<?php echo L::Okay ?>',



              }).then((result) => {

              if (result.value) {

              window.location.reload();

              }

              })

                </script>

                      <?php

                      unset($_SESSION['user']['addsubjectok']);

                    }



                     ?>





                     <div class="col-md-12 col-sm-12">

                                   <div class="panel tab-border card-box">

                                       <header class="panel-heading panel-heading-gray custom-tab ">

                                           <ul class="nav nav-tabs">

                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::VersementsLists ?></a>

                                               </li>



                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddVersementss ?></a>

                                               </li>







                                           </ul>

                                       </header>

                                       <div class="panel-body">

                                           <div class="tab-content">

                                               <div class="tab-pane active" id="home">

                                                 <div class="row">

                        <div class="col-md-12">

                            <div class="card  card-box">

                                <div class="card-head">

                                    <header></header>

                                    <div class="tools">

                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                    </div>

                                </div>

                                <div class="card-body ">



                                    <!-- <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4"> -->
<div class="table-scrollable">
  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example45">
                                          <thead>

                                              <tr>



                                                  <th style="width:150px;"> <?php echo L::Versements ?> </th>

                                                  <th> <?php echo L::DateVersementss ?></th>

                                                  <th> <?php echo L::MotifVersementss ?></th>

                                                  <th> <?php echo L::MontantVersementss ?> </th>

                                                  <th><?php echo L::BeneficiaireVersementss ?></th>

                                                    <th class="noExport"><?php echo L::Actions ?></th>



                                              </tr>

                                          </thead>

                                          <tbody>

                                            <?php

                                              //$matieres



                                              $i=1;

                                                foreach ($scolarites as $value):

                                             ?>

                                              <tr class="odd gradeX">



                                                  <td> <?php echo $value->code_versement;?></td>

                                                  <td>

                                                      <?php echo date_format(date_create($value->date_versement),"d/m/Y");?>

                                                  </td>
                                                  <td>
                                                    <span class="label label-sm label-info" style=""><?php echo $value->motif_versement; ?></span>
                                                  </td>

                                                  <td>

                                                    <?php echo $value->montant_versement." ".$value->devise_versement;?>

                                                  </td>

                                                  <td>

                                                    <?php

                                                    $donnees=$student->getEmailParentOfThisStudentByID($value->ideleve_versement,$value->session_versement);

                                                    $studentInfos=$student->getAllInformationsOfStudentOne($value->ideleve_versement,$value->session_versement);

                                                    // var_dump($studentInfos);

                                                    $tabStudent=array();

                                                    foreach ($studentInfos as $personnal):
                                                      $tabStudent[0]= $personnal->id_compte;
                                                      $tabStudent[1]=$personnal->matricule_eleve;
                                                      $tabStudent[2]= $personnal->nom_eleve;
                                                      $tabStudent[3]=$personnal->prenom_eleve;
                                                      $tabStudent[4]= $personnal->datenais_eleve;
                                                      $tabStudent[5]=$personnal->lieunais_eleve;
                                                      $tabStudent[6]= $personnal->sexe_eleve;
                                                      $tabStudent[7]=$personnal->email_eleve;
                                                      $tabStudent[8]=$personnal->email_eleve;
                                                      $tabStudent[9]= $personnal->libelle_classe;
                                                      $tabStudent[10]=$personnal->codeEtab_classe;
                                                      $tabStudent[11]= $personnal->photo_compte;
                                                      $tabStudent[12]=$personnal->tel_compte;
                                                      $tabStudent[13]= $personnal->login_compte;
                                                      $tabStudent[14]=$personnal->codeEtab_inscrip;
                                                      $tabStudent[15]= $personnal->id_classe;
                                                      $tabStudent[16]=$personnal->allergie_eleve;
                                                      $tabStudent[17]=$personnal->condphy_eleve;
                                                    endforeach;

                                                    $tabdata=explode('*',$donnees);

                                                    $emailparent=$tabdata[0];

                                                    echo $nomEleve=$tabStudent[2]." ".$tabStudent[3];

                                                    $classeName=$tabStudent[9];

                                                    $EtabName=$tabdata[4];

                                                    $imageEtab=$tabdata[5];

                                                    ?>

                                                  </td>

                                                  <td style="text-align:center">

                                                    <!-- <a href="#" class="btn btn-warning  btn-md " style="border-radius:3px;" onclick="generatefichevers(<?php //echo $value->ideleve_versement;?>,'<?php //echo $value->session_versement;?>','<?php //echo $value->codeEtab_versement; ?>','<?php //echo $value->id_versement ?>')">  <i class="fa fa-print"></i></a> -->

                                                      <a target="_blank" href="ficheversement.php?compte=<?php echo $value->ideleve_versement?>&codeEtab=<?php echo $value->codeEtab_versement; ?>&sessionEtab=<?php echo $value->session_versement;?>&classeEtab=<?php echo $value->classe_versement ?>&versementid=<?php echo $value->id_versement ?>" class="btn btn-warning  btn-xs " style="border-radius:3px;">  <i class="fa fa-print"></i></a>
                                                  </td>



                                                  <?php

                                                  /*

                                                   ?>

                                                  <td class="valigntop">

                                                    <a href="#"  data-toggle="modal" data-target="#exampleModal<?php echo $value->id_mat?>"class="btn btn-info  btn-md " style="border-radius:3px;">

                                                      <i class="fa fa-pencil"></i>

                                                    </a>

                                                    <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">

                                                      <i class="fa fa-pencil"></i>

                                                    </a-->

                                                    <a href="#"  onclick="deleted(<?php echo $value->id_mat;?>)" class="btn btn-danger  btn-md " style="border-radius:3px;">

                                                      <i class="fa fa-trash-o"></i>

                                                    </a>

                                                  </td>

                                                  <?php

                                                  */

                                                   ?>

                                              </tr>

                                              <?php

                                              $i++;

                                              endforeach;

                                              ?>





                                          </tbody>

                                      </table>
</div>


                                </div>

                            </div>

                        </div>

                    </div>

                                               </div>

                                               <div class="tab-pane" id="about">

                                                 <?php

                                                 if($nbsessionOn>0)

                                                 {

                                                  ?>

                                                  <div class="row">

                                                      <div class="col-md-12 col-sm-12">

                                                          <div class="card card-box">

                                                              <div class="card-head">

                                                                  <header></header>



                                                              </div>



                                                              <div class="card-body" id="bar-parent">

                                                                  <form  id="FormAddCtrl" class="form-horizontal" action="../controller/scolarites.php" method="post">

                                                                      <div class="form-body">

                                                                        <div class="form-group row">

                                                                                <label class="control-label col-md-3"><?php echo L::ClassesMenu ?>

                                                                                    <span class="required"> * </span>

                                                                                </label>

                                                                                <div class="col-md-5">

                                                                                    <select class="form-control " name="classe" id="classe"  style="width:100%" >

                                                                                        <!-- <option value="" selected >Selectionner une classe</option> -->





                                                                                    </select>

                                                                            </div>

                                                                          </div>

                                                                        <div class="form-group row">

                                                                                <label class="control-label col-md-3"><?php echo L::studMenu ?>

                                                                                    <span class="required"> * </span>

                                                                                </label>

                                                                                <div class="col-md-5">

                                                                                    <!--input type="text" name="matiere" id="matiere" data-required="1" placeholder="Entrer la classe" class="form-control " /-->
                                                                                    <select class="form-control " name="student" id="student"  style="width:100%" onchange='dueamount()'>

                                                                                        <option value=""><?php echo L::SelectAnEleve ?></option>



                                                                                    </select>

                                                                                    <input type="hidden" name="etape" id="etape" value="3"/>

                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>

                                                                                      <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">

                                                                                  </div>



                                                                         </div>
                                                                         <div class="form-group row">

                                                                                 <label class="control-label col-md-3"><?php echo L::Deposant ?>

                                                                                     <span class="required"> * </span>

                                                                                 </label>

                                                                                 <div class="col-md-5">

                                                                                     <input type="text" class="form-control" name="deposant" id="deposant" value="">

                                                                             </div>

                                                                           </div>


                                                                         <div class="form-group row">

                                                                                 <label class="control-label col-md-3"><?php echo L::MotifVersementss ?>

                                                                                     <span class="required"> * </span>

                                                                                 </label>

                                                                                 <div class="col-md-5">

                                                                                   <select class="form-control " name="motifpaie" id="motifpaie"  style="width:100%" onchange="determineMontant()">

                                                                                       <option value="" selected><?php echo L::SelectMotifDpot ?></option>
                                                                                       <option value="1"><?php echo L::InscriptionDues ?></option>
                                                                                       <option value="2"><?php echo L::ScolarDues ?></option>
                                                                                       <!-- <option value="3">Frais de Cantine</option> -->
                                                                                       <option value="4"><?php echo L::AESDues ?></option>
                                                                                       <option value="5"><?php echo L::TPfees ?></option>



                                                                                   </select>



                                                                                   </div>



                                                                          </div>

                                                                          <div class="form-group row" id="transportRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::TP ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                    <select class="form-control " name="transports" id="transports"   style="width:100%" onchange="TransportAmount()">
                                                                                      <option value="" selected><?php echo L::TransportationFeesSelect ?></option>

                                                                                      <?php

                                                                                      foreach ($transportations as  $values):
                                                                                        ?>
                                                                                        <option value="<?php echo $values->frais_trans."-".$values->devises_trans."-".$values->type_trans ?>"><?php echo $values->type_trans ?></option>
                                                                                        <?php
                                                                                      endforeach;
                                                                                       ?>




                                                                                    </select>



                                                                                    </div>



                                                                           </div>

                                                                          <div class="form-group row" id="activitiesRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::Activitysingle ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                    <select class="form-control " name="activities" id="activities"   style="width:100%" onchange="ActivitiesAmount()">






                                                                                    </select>



                                                                                    </div>



                                                                           </div>

                                                                          <div class="form-group row" id="monthpaieRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::MonthLibsingle ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                    <select class="form-control " name="monthpaie[]" id="monthpaie" multiple="multiple"  style="width:100%" onchange="checkLimited()">


                                                                                        <option value="JANVIER"><?php echo L::JanvLibcaps ?></option>
                                                                                        <option value="FEVRIER"><?php echo L::FevLibcaps ?></option>
                                                                                        <option value="MARS"><?php echo L::MarsLibcaps ?></option>
                                                                                        <option value="AVRIL"><?php echo L::AvriLibcaps ?></option>
                                                                                        <option value="MAI"><?php echo L::MaiLibcaps ?></option>
                                                                                        <option value="JUIN"><?php echo L::JuneLibcaps ?></option>
                                                                                        <option value="JUILLET"><?php echo L::JulLibcaps ?></option>
                                                                                        <option value="AOUT"><?php echo L::AoutLibcaps ?></option>
                                                                                        <option value="SEPTEMBRE"><?php echo L::SeptLibcaps ?></option>
                                                                                        <option value="OCTOBRE"><?php echo L::OctobLibcaps ?></option>
                                                                                        <option value="NOVEMBRE"><?php echo L::NovbLibcaps ?></option>
                                                                                        <option value="DECEMBRE"><?php echo L::DecemLibcaps ?></option>



                                                                                    </select>
                                                                                    <input type="hidden" name="limitmonth" id="limitmonth" value="0">


                                                                                    </div>



                                                                           </div>


                                                                         <div class="form-group row" id="modepaieRow">

                                                                                 <label class="control-label col-md-3"><?php echo L::PaiementMode ?>

                                                                                     <span class="required"> * </span>

                                                                                 </label>

                                                                                 <div class="col-md-5">

                                                                                   <select class="form-control " name="modepaie" id="modepaie"  style="width:100%" onchange="chooseModepaie()">

                                                                                       <option value="" selected><?php echo L::SelectPaiementMode ?></option>
                                                                                       <option value="1"><?php echo L::ChequesPaiementMode ?></option>
                                                                                       <option value="2"><?php echo L::EspecesPaiementMode ?></option>
                                                                                       <option value="3"><?php echo L::TpePaiementMode ?></option>
                                                                                       <option value="4"><?php echo L::BancairePaiementMode ?></option>



                                                                                   </select>



                                                                                   </div>



                                                                          </div>

                                                                          <div class="form-group row" id="duRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::DuPaiementMode ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">


                                                                                    <input type="text" class="form-control" name="dupaie" id="dupaie" value="">



                                                                                    </div>



                                                                           </div>

                                                                          <div class="form-group row" id="montantapayerRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::AmountToPaiement ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                      <input type="number" min="1" name="montapayer" id="montapayer" data-required="1" placeholder="" class="form-control " readonly /> </div>

                                                                           </div>

                                                                         <div class="form-group row" id="montantversementRow">

                                                                                 <label class="control-label col-md-3"><?php echo L::MontantVersementss ?>

                                                                                     <span class="required"> * </span>

                                                                                 </label>

                                                                                 <div class="col-md-5">

                                                                                     <input type="text"  name="montvers" id="montvers" data-required="1" placeholder="<?php echo L::EnterMontantVersementss ?>" class="form-control " onkeyup="recalculrest()" /> </div>

                                                                          </div>
                                                                          <div class="form-group row" id="montantversementcantRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::MontantVersementss ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                      <input type="number" min="1" name="montverscant" id="montverscant" data-required="1" placeholder="<?php echo L::EnterMontantVersementss ?>" class="form-control " onkeyup="recalculrest()" /> </div>

                                                                           </div>

                                                                          <div class="form-group row" id="resteapayerRow">

                                                                                  <label class="control-label col-md-3"><?php echo L::RestAmountToPaiement ?>

                                                                                      <span class="required"> * </span>

                                                                                  </label>

                                                                                  <div class="col-md-5">

                                                                                      <input type="number" min="0" name="montrest" id="montrest" data-required="1" placeholder="" class="form-control " readonly /> </div>

                                                                           </div>

                                                                           <div class="form-group row" id="devisesRow">

                                                                                   <label class="control-label col-md-3"><?php echo L::DeviseMontantSection ?>

                                                                                       <span class="required"> * </span>

                                                                                   </label>

                                                                                   <div class="col-md-5">

                                                                                       <input type="text"  name="devisepaie" id="devisepaie" data-required="1" placeholder="" class="form-control " readonly /> </div>

                                                                            </div>















                                                    <div class="form-actions">

                                                                          <div class="row">

                                                                              <div class="offset-md-3 col-md-9">



                                                                                  <button type="submit" class="btn btn-info"><?php echo L::Saving ?></button>

                                                                                  <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>

                                                                              </div>

                                                                            </div>

                                                                         </div>

                                                  </div>

                                                                  </form>

                                                              </div>

                                                          </div>

                                                      </div>



                                                  </div>





                                                  <?php

                                                }

                                                  ?>







                                                </div>





                                           </div>

                                       </div>

                                   </div>

                               </div>

          </div>

      </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->



  <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 <script src="../assets2/plugins/popper/popper.min.js" ></script>

   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>

   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>

   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>





   <!-- bootstrap -->

   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>



   <!-- data tables -->

   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
 <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
 	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
 	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>

   <script src="../assets2/js/pages/table/table_data.js" ></script>

   <!-- Common js-->
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

 <script src="../assets2/js/app.js" ></script>

   <script src="../assets2/js/pages/validation/form-validation.js" ></script>

   <script src="../assets2/js/layout.js" ></script>

 <script src="../assets2/js/theme-color.js" ></script>

 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>

 <script src="../assets2/js/dropify.js"></script>

 <script src="../assets2/plugins/select2/js/select2.js" ></script>

 <script src="../assets2/js/pages/select2/select2-init.js" ></script>

 <!-- Material -->

 <script src="../assets2/plugins/material/material.min.js"></script>

 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script src="../assets/js/formatter/jquery.formatter.min.js"></script>



 <script>

 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function chooseModepaie()
 {
   var modepaie=$("#modepaie").val();
   if(modepaie==3||modepaie==4)
   {
     $("#duRow").show();
   }else {
     $("#duRow").hide();
   }
 }

function checkLimited()
{
  var limitmonth=$("#limitmonth").val();
  var months=$("#limitmonth").val();
}

 function selectionclasses()

 {

   var classeid="<?php echo $_GET['classe'] ?>";

   var session="<?php echo $libellesessionencours ?>";

   var codeEtab="<?php echo $codeEtabAssigner; ?>";

   var etape=11;



   $.ajax({

     url: '../ajax/classe.php',

     type: 'POST',

     async:false,

     data: 'codeEtab='+codeEtab+'&etape='+etape+'&classe='+classeid+'&session='+session,

     dataType: 'text',

     success: function (response, statut) {



       $("#classe").html("");

       $("#classe").html(response);



     }

   });





 }



 function determinestudent()

 {

   var matricule=$("#matricule").val();

   var codeEtab=$("#etablissement").val();

   var etape=1;



   $.ajax({

     url: 'ajax/determination.php',

     type: 'POST',

     async:false,

     data: 'codeEtab='+codeEtab+'&etape='+etape+'&matricule='+matricule,

     dataType: 'text',

     success: function (response, statut) {







     }

   });

 }



function generatefichevers(ideleve,session,codeEtab,id_versement)

{

  // var etape=2;
  //
  // $.ajax({
  //
  //   url: '../ajax/etat.php',
  //
  //   type: 'POST',
  //
  //   async:false,
  //
  //   data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&eleveid='+ideleve+'&versementid='+id_versement,
  //
  //   dataType: 'text',
  //
  //   success: function (response, statut) {
  //
  //
  //
  //       window.open(response, '_blank');
  //
  //
  //
  //   }
  //
  // });




}



 function recalculrest()

 {

   var montantapayer=$("#montapayer").val();

   var montantpayer=$("#montvers").val();

   var etape=5;

   if(montantapayer!="")

   {

   $.ajax({

     url: '../ajax/admission.php',

     type: 'POST',

     async:true,

     data: 'montantapayer=' + montantapayer+ '&etape=' + etape+'&montantpayer='+montantpayer,

     dataType: 'text',

     success: function (content, statut) {

       var content = $.trim(content);
       if(content>=0)

         {


           $("#montrest").val(content);

         }else {

           if(montantpayer!="")

           {

             Swal.fire({

             type: 'warning',

             title: '<?php echo L::WarningLib ?>',

             text: '<?php echo L::AmountToPaiementgiveIsSuperiorToDue ?>',



             })



             $("#montrest").val(montantapayer);

           }



           $("#montvers").val(0);

         }



     }

   });



 }else {

   $("#montvers").val(0);

 }



   // if(montantapayer!="")

   // {

   //   var montrest=montantapayer-montantpayer;

   //   if(montrest>0)

   //   {

   //     $("#montrest").val(montrest);

   //   }

   // }





 }



function determinedevise()
{
   var codeEtab=$("#codeEtab").val();
   var etape=1;

   $.ajax({
     url: '../ajax/devises.php',

     type: 'POST',

     async:true,

     data: 'codeEtab=' + codeEtab+ '&etape=' + etape,

     dataType: 'text',

     success: function (content, statut) {

       var cut = $.trim(content);

       $("#devisepaie").val(cut);


     }
   });

}

function activepaiementinputTransport()
{
  $("#transportRow").show();
  $("#monthpaieRow").show();
  $("#modepaieRow").show();
  $("#modepaieRow").show();
  $("#montantapayerRow").show();
  $("#devisesRow").show();

}

function desactivepaiementinput()
{
  $("#monthpaieRow").show();
  $("#modepaieRow").show();
  $("#montantapayerRow").show();
  $("#devisesRow").show();
  $("#montantversementcantRow").hide();
  $("#montantversementRow").hide();
  $("#resteapayerRow").hide();
  $("#activitiesRow").hide();
  // $("#transportRow").hide();




}

function activitiesinput()
{
  $("#activitiesRow").show();
  $("#modepaieRow").show();
  $("#montantapayerRow").show();
  $("#devisesRow").show();
  $("#montantversementcantRow").hide();
  $("#montantversementRow").hide();
  $("#resteapayerRow").hide();
  $("#monthpaieRow").hide();
  $("#transportRow").hide();
}

function facteurCommun()
{

$("#modepaieRow").show();
$("#montantapayerRow").show();
$("#devisesRow").show();

$("#montapayer").val("");
// $("#devisepaie").val("");
}

function facteurActivites()
{
  facteurCommun();
  $("#activitiesRow").show();

  $("#transportRow").hide();
  $("#monthpaieRow").hide();
  $("#resteapayerRow").hide();

}

function facteurTransport()
{
  facteurCommun();
  $("#transportRow").show();
  $("#monthpaieRow").show();

  $("#activitiesRow").hide();
  $("#resteapayerRow").hide();
  $("#montantversementRow").hide();




}

function facteurInscriptAndScolarity()
{
  facteurCommun();
  $("#resteapayerRow").show();
  $("#montantversementRow").show();
  $("#montrest").val("");
  $("#montvers").val("");
  $("#transportRow").hide();
  $("#activitiesRow").hide();
  $("#monthpaieRow").hide();
}

function activepaiementinput()
{
  //nous allons afficher les imputs qui nous permettent de faire le paiement des frais de scolarités et d'inscription


  $("#modepaieRow").show();
  $("#montantapayerRow").show();
  $("#montantversementRow").show();
  $("#resteapayerRow").show();
  $("#devisesRow").show();

  $("#monthpaieRow").hide();
  $("#montantversementcantRow").hide();
  $("#activitiesRow").hide();


  // $("#monthpaieRow").show();
  // $("#modepaieRow").show();
  // $("#montantapayerRow").show();
  // $("#devisesRow").show();

}

function TransportAmount()
{
  var transports=$("#transports").val();
  var tab=transports.split("-");
  var montantapayer=tab[0];
  var devises=tab[1];
  var transportype=tab[2];
  var codeEtab=$("#FormAddCtrl #codeEtab").val();
  var sessionEtab=$("#FormAddCtrl #libellesession").val();
  var student=$("#FormAddCtrl #student").val();
  var classeid=$("#FormAddCtrl #classe").val();
  var motif=$("#FormAddCtrl #motifpaie").val();



   $("#montapayer").val(montantapayer);
   $("#devisepaie").val(devises);


var etape=29;

$.ajax({

  url: '../ajax/admission.php',
  type: 'POST',
  async:false,
  data: 'codeEtab='+codeEtab+'&etape='+etape+'&student='+student+'&session='+sessionEtab+'&transportype='+transportype+'&classeid='+classeid+'&motif='+motif,
  dataType: 'text',

  success: function (response, statut) {



    var cut = $.trim(response);
    tabcontent=cut.split("-");

    $("#monthpaie").select2({
  maximumSelectionLength: tabcontent[0],
  language: "fr"
  });

var retour=tabcontent[1];

  if(retour==0)
  {
    // alert("bonjour");
    //on laisse comme ça
  }else {
    // alert("bonsoir");
    //nous devons retirer de la liste les autres mois

    var etape=30;

    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:false,
      data: 'codeEtab='+codeEtab+'&etape='+etape+'&student='+student+'&session='+sessionEtab+'&transportype='+transportype+'&classeid='+classeid+'&motif='+motif,
      dataType: 'text',

      success: function (response, statut) {

         $("#monthpaie").html("");
         $("#monthpaie").html(response);

      }
    });


  }




  }

});


}

function ActivitiesAmount()
{
  var activities=$("#activities").val();
  var codeEtab=$("#codeEtab").val();
  var session=$("#libellesession").val();
  var classe=$("#classe").val();
 var etape=27;
 $.ajax({
   url: '../ajax/admission.php',
   type: 'POST',
   async:false,
   data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&activities='+activities,
   dataType: 'text',
   success: function (response, statut) {

     var cut = $.trim(response);


   $("#montapayer").val(cut);

   }
 });
}



function determineMontant()
{
  var codeEtab=$("#codeEtab").val();
  var session=$("#libellesession").val();
  var classe=$("#classe").val();
  var motif=$("#motifpaie").val();
  var student="<?php echo $_GET['student'] ?>";

  $("#montrest").val("");
  $("#montvers").val("");


  if(motif==1)
  {
    //alert("inscription");

    // activepaiementinput();
    facteurInscriptAndScolarity();
    $("#etape").val(3);

    etape=20;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:false,
      data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
      dataType: 'text',
      success: function (response, statut) {

        var cut = $.trim(response);


      $("#montapayer").val(cut);


      }
    });

  }else if(motif==2)
  {
    // alert("scolarité");

    // activepaiementinput();
    facteurInscriptAndScolarity();
    $("#etape").val(3);

    etape=21;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:false,
      data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
      dataType: 'text',
      success: function (response, statut) {

        var cut = $.trim(response);


      $("#montapayer").val(cut);


      }
    });

  }else if(motif==3)
  {
    // alert("cantine");

    desactivepaiementinput();
    $("#etape").val(4);

    etape=22;
    $.ajax({
      url: '../ajax/admission.php',
      type: 'POST',
      async:false,
      data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
      dataType: 'text',
      success: function (response, statut) {

      var cut = $.trim(response);
      // alert(cut);

      if(cut==0)
      {
        //il n'y as pas encore eu de paiement pour les frais de cantine
        //nous allons recuperer le montant a payer ainsi que le nombre de mois à selectionner

        var etape=23;

        $.ajax({
          url: '../ajax/admission.php',
          type: 'POST',
          async:false,
          data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
          dataType: 'text',
          success: function (response, statut) {

            var cut = $.trim(response);
            $tabcontent=cut.split("-");

            $("#montapayer").val($tabcontent[0]);
            $("#monthpaie").select2({
        maximumSelectionLength: $tabcontent[1],
        language: "fr"
    });


            // $("#limitmonth").val($tabcontent[1]);

          }
        });

      }else {
        //nous avons deja eu des paiement pour les frais de cantine
        //nous allons donc chercher la liste des mois pour lesquelles le paiement à été fait
          var etape=24;
          $.ajax({
            url: '../ajax/admission.php',
            type: 'POST',
            async:false,
            data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
            dataType: 'text',
            success: function (response, statut) {
                var cut = $.trim(response);
                $tabcontent=cut.split("-");
              $("#monthpaie").html("");
              $("#montapayer").val($tabcontent[2]);
              $("#monthpaie").html($tabcontent[0]);
              $("#monthpaie").select2({
          maximumSelectionLength: $tabcontent[1],
          language: "fr"
      });


            }
          });

      }
      // $("#montapayer").val(cut);




      }
    });
  }else if(motif==4)
  {
      // activitiesinput();
      facteurActivites();
      $("#etape").val(5);

      etape=26;
      $.ajax({
        url: '../ajax/admission.php',
        type: 'POST',
        async:false,
        data: 'codeEtab='+codeEtab+'&etape='+etape+'&classeid='+classe+'&session='+session+'&student='+student+'&motif='+motif,
        dataType: 'text',
        success: function (response, statut) {
          //nous allons rechercher la liste des activites extra scolaire de cette classe
            var cut = $.trim(response);
            $("#activities").html("");
            $("#activities").html(cut);

        }
      });

  } else if(motif==5)
  {
  facteurTransport();
    $("#etape").val(6);




  }

}

 function dueamount()

 {

   var codeEtab=$("#codeEtab").val();

   var session=$("#libellesession").val();

   var classe=$("#classe").val();

   //var student=$("#student").val();

   var student="<?php echo $_GET['student'] ?>";

    var etape=4;



    $.ajax({

      url: '../ajax/admission.php',

      type: 'POST',

      async:true,

      data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&session='+session+'&student='+student,

      dataType: 'text',

      success: function (content, statut) {

            var cut = $.trim(content);
            //alert(cut);


       $tabcontent=cut.split("-");



         $("#montvers").val("");

       $("#montapayer").val($tabcontent[0]);

       $("#montrest").val($tabcontent[0]);

       $("#devisepaie").val($tabcontent[1]);



      }

    });



 }



 function searchstudent()

 {

   var codeEtab=$("#codeEtab").val();

   var session=$("#libellesession").val();

   var classe=$("#classe").val();

   var studentid="<?php echo $_GET['student'] ?>";

   var etape=9;



   $.ajax({

     url: '../ajax/admission.php',

     type: 'POST',

     async:true,

     data: 'codeEtab=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&session='+session+'&studentid='+studentid,

     dataType: 'text',

     success: function (content, statut) {



       $("#student").html("");
        var cut = $.trim(content);
      //alert(cut);

       $("#student").html(cut);



     }

   });



 }



 function searchmatiere()

 {

     var codeEtab="<?php echo $codeEtabLocal;?>";

     var classe=$("#classe").val();

     var etape=2;

      $.ajax({



        url: '../ajax/matiere.php',

        type: 'POST',

        async:true,

        data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe,

        dataType: 'text',

        success: function (content, statut) {



          $("#matiere").html("");

          $("#matiere").html(content);



        }

      });

 }



 function searchprofesseur()

 {

   var codeEtab="<?php echo $codeEtabLocal;?>";

   var classe=$("#classe").val();

   var matiere=$("#matiere").val();

   var etape=4;

   $.ajax({



     url: '../ajax/teatcher.php',

     type: 'POST',

     async:true,

     data: 'code=' + codeEtab+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,

     dataType: 'text',

     success: function (content, statut) {



       // alert(content);

       $("#teatcher").html("");

       $("#teatcher").html(content);



     }

   });

 }



 function checksms()

 {





   if($('#smssender').prop('checked') == true){

    $("#smsvalue").val(1);

  }

  else {



    $("#smsvalue").val(0);

  }



 }



 function checkmail()

 {





   if($('#emailsender').prop('checked') == true){

    $("#emailvalue").val(1);

  }

  else {



    $("#emailvalue").val(0);

  }







 }





 $("#classeEtab").select2({

   tags: true,

 tokenSeparators: [',', ' ']

 });



 $("#destinataires").select2({

   tags: true,

 tokenSeparators: [',', ' ']

 });



 $('.joinfile').dropify({

     messages: {

         'default': 'Selectionner un fichier joint',

         'replace': 'Remplacer le fichier joint',

         'remove':  'Retirer',

         'error':   'Ooops, Une erreur est survenue.'

     }

 });





 jQuery(document).ready(function() {

$("#montvers").formatter({pattern:"{{999}}{{999}}{{999}}{{999}}"});

selectionclasses();

searchstudent();

// dueamount();

determinedevise();

$('#example45').DataTable( {

  "scrollX": true,
  "language": {
      "lengthMenu": "_MENU_  ",
      "zeroRecords": "Aucune correspondance",
      "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
      "infoEmpty": "Aucun enregistrement disponible",
      "infoFiltered": "(filtered from _MAX_ total records)",
      "sEmptyTable":"Aucune donnée disponible dans le tableau",
       "sSearch":"Rechercher :",
       "oPaginate": {
  "sFirst":    "Premier",
  "sLast":     "Dernier",
  "sNext":     "Suivant",
  "sPrevious": "Précédent"
}
},

    dom: 'Bfrtip',
    buttons: [
        // 'copyHtml5',

        // 'excelHtml5',
        {
          extend: 'excelHtml5',
          title: 'Data export',
          exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
        }
        // 'csvHtml5',
        // 'pdfHtml5'
    ]
} );



$("#classe").select2();

$("#teatcher").select2();

$("#classeEtab").select2();

$("#matiere").select2();

$("#typesess").select2();

$("#student").select2();

$("#modepaie").select2();

$("#motifpaie").select2();

$("#monthpaie").select2();
$("#activities").select2();
$("#transports").select2();



$("#monthpaieRow").hide();
$("#modepaieRow").hide();
$("#montantapayerRow").hide();
$("#montantversementRow").hide();
$("#montantversementcantRow").hide();
$("#resteapayerRow").hide();
$("#devisesRow").hide();
$("#activitiesRow").hide();
$("#transportRow").hide();
$("#duRow").hide();






   $("#FormAddCtrl").validate({



     errorPlacement: function(label, element) {

     label.addClass('mt-2 text-danger');

     label.insertAfter(element);

   },

   highlight: function(element, errorClass) {

     $(element).parent().addClass('has-danger')

     $(element).addClass('form-control-danger')



   },

   success: function (e) {

         $(e).closest('.control-group').removeClass('error').addClass('info');

         $(e).remove();

     },

      rules:{



        // matiere:"required",

        // classe:"required",

        // teatcher:"required",

        // coef:"required",

        classe:"required",

        student:"required",

        modepaie:"required",
        monthpaie:"required",
        deposant:"required",
        dupaie:"required",



        montvers:{

          'required': {

              depends: function (element) {

                  return ($('#montapayer').val() !=''|| $('#montrest').val() !='' );



              }

          }

        },





      },

      messages: {


        // matiere:"Merci de renseigner la matière",

        // classe:"<?php echo L::PleaseSelectclasserequired ?>",

        // teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",

        // coef:"Merci de renseigner le coefficient de la matière"
          monthpaie:"<?php echo L::SelectMonthDuesTo ?>",
          classe:"<?php echo L::PleaseSelectclasserequired ?>",
          montvers:"<?php echo L::PleaseEnterMontantVersementss ?>",
          student:"<?php echo L::PleaseSelectAnEleve ?>",
          deposant:"<?php echo L::PleaseEnterdeposantName ?>",
          modepaie:"<?php echo L::PleaseSelectPaiementMode ?>",
          dupaie:"<?php echo L::RequiredChamp ?>",





      },

      submitHandler: function(form) {





// nous allons verifier un controle similaire n'existe pas



  form.submit();





             }





           });

      });













 </script>

    <!-- end js include path -->

  </body>



</html>
