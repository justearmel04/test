<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Diplome.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

// echo $_SESSION['user']['fonctionuser'];
if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();
$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$diplome=new Diplome();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
// echo $emailUti;
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

  if($_SESSION['user']['groupe']==1)
  {
    if($_SESSION['user']['fonctionuser']=="Administrateur"||$_SESSION['user']['fonctionuser']=="DG"||$_SESSION['user']['fonctionuser']=="PCG"||$_SESSION['user']['fonctionuser']=="Accountant"||$_SESSION['user']['fonctionuser']=="Accountant")
    {
      // echo "bonsoir";
      // $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);

      //nous allons chercher la liste des etablissements du groupe

      $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
        }

        if(strlen($_SESSION['user']['codeEtab'])>0)
        {

        }else {
          $_SESSION['user']['codeEtab']=$codeEtabAssigner;
        }

        $_SESSION['user']['session']=$libellesessionencours;





         // var_dump($allcodeEtabs);

        $notifications=$etabs->getAllMessagesendingEtabs($compteuserid,$libellesessionencours);

        $classes=$classe->getAllclassesOfassignatedEtabs($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$_SESSION['user']['session']);


    }else if($_SESSION['user']['fonctionuser']=="Coordinnateur"||$_SESSION['user']['fonctionuser']=="Comptable"||$_SESSION['user']['fonctionuser']=="Directeur")
    {
      // echo "bonjour";
      // $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
      if(strlen($_SESSION['user']['codeEtab'])>0)
      {
        $codeEtabAssigner=$_SESSION['user']['codeEtab'];
      }else {
        $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
      }
      $datastat=$user->getStatisById($codeEtabAssigner);
      $tabstat=explode("*",$datastat);
      $etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
      $agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
      $nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
      $libellesessionencours="";

        if($etablissementType==1||$etablissementType==3)
        {
          if($nbsessionOn>0){
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];

          }

        }else {
          if($nbsessionOn>0)
          {
            //recuperer la session en cours
            $sessionencours=$session->getSessionEncours($codeEtabAssigner);
            $tabsessionencours=explode("*",$sessionencours);
            $libellesessionencours=$tabsessionencours[0];
            $sessionencoursid=$tabsessionencours[1];
            $typesessionencours=$tabsessionencours[2];
            $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
            $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
            $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
          }
          $_SESSION['user']['session']=$libellesessionencours;

          if(strlen($_SESSION['user']['codeEtab'])>0)
          {

          }else {
            $_SESSION['user']['codeEtab']=$codeEtabAssigner;
          }

          $notifications=$etabs->getAllMessagesending($codeEtabAssigner,$libellesessionencours);

          // $classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
          $classes=$classe->getAllclassesOfassignatedNew($_SESSION['user']['IdCompte'],$_SESSION['user']['codeEtab'],$libellesessionencours);
          $allcodeEtabs=$etabs->getAllcodeEtabs($_SESSION['user']['IdCompte']);

          // var_dump($classes);
        }

    }
  }

  $detailsTeach=$teatcher->getTeatcherInfobyId($_GET['compte']);

  $tabteatcher=explode("*",$detailsTeach);

  // var_dump($tabteatcher);

  $nbTeatcherchild=$teatcher->getNumberOfChild($_GET['compte']);
  $diplomes=$diplome->getDiplomebyTeatcherId($_GET['compte']);

  // var_dump($diplomes);

  $teatcherid=$_GET['compte'];

  $nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

  //les 10 dernières notifications
  $lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);

  // var_dump($allcodeEtabs);

// echo $libellesessionencours;
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

    <!-- data tables -->

        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

  <!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />

  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::DetailsTeatchers ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::ProfsMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::DetailsTeatchers ?></li>
                      </ol>
                  </div>
              </div>

              <?php





                    if(isset($_SESSION['user']['addteaok']))
                    {
// echo "dddddddd";
                      ?>

  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
    title: '<?php echo L::Felicitations ?>',
    text: "<?php echo $_SESSION['user']['addteaok']; ?>",
    type: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    // confirmButtonText: 'Ajouter les diplomes',
    cancelButtonText: '<?php echo L::Okay ?>',
    }).then((result) => {
    if (result.value) {
    // document.location.href="adddiplomes.php?compte="+<?php //echo $_GET['compte'] ?>;
    }else {
    // document.location.href="index.php";
    }
    })
                </script>
                      <?php
                      unset($_SESSION['user']['addteaok']);
                    }

                     ?>

              <div class="row">
                <div class="col-sm-12">
              <div class="card-box">
                <div class="card-head">
                  <header></header>
                </div>
                <div class="card-body ">
                      <div class = "mdl-tabs mdl-js-tabs">
                         <div class = "mdl-tabs__tab-bar tab-left-side">
                            <a href = "#tab4-panel" class = "mdl-tabs__tab is-active"><?php echo L::TeatcherInfosPerso ?></a>
                            <a href = "#tab5-panel" class = "mdl-tabs__tab"><?php echo L::ProfessionalInfos ?></a>
                            <a href = "#tab6-panel" class = "mdl-tabs__tab"><?php echo L::DocsAndDiplomes ?></a>
                            <a href = "#tab7-panel" class = "mdl-tabs__tab"><?php echo L::AccountInfos ?></a>
                         </div>
                         <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormUpdatePersonalInfo" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                           <div class="form-body">
                                             <div class="row">
                             <div class="col-md-12">

         <fieldset style="background-color:#007bff;">

         <center><legend style="color:white;"><?php echo L::TeatcherInfosPerso ?></legend></center>
         </fieldset>


         </div>
                           </div>
                           <br/>
                           <div class="row">
                           <div class="col-md-6">
                           <div class="form-group">
                             <?php
                             if(strlen($tabteatcher[9]))
                             {
                               $lien="../photo/".$tabteatcher[8]."/".$tabteatcher[10];
                             }else {
                               $lien="../photo/user5.jpg";
                             }
                             ?>
                           <input  type="file" id="photoTea" name="photoTea" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php echo $lien; ?>" data-allowed-file-extensions="gif png jpg jpeg pjpeg" />
                           <label for=""><b><?php echo L::Image ?><span class="required"> * </span>: </b></label>
                           </div>

                           </div>


                                         </div>
                           <div class="row">
                                               <div class="col-md-6">
                           <div class="form-group">
                           <label for=""><b><?php echo L::Name ?><span class="required"> * </span>: </b></label>

                         <input type="text"  name="nomTea" id="nomTea" value="<?php echo $tabteatcher[1];?>" data-required="1" placeholder="<?php echo L::EnterName ?>" class="form-control " />
                           </div>

                           </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                  <label for=""><b><?php echo L::PreName ?> : </b></label>
                                   <input type="text"  name="prenomTea" id="prenomTea" value="<?php echo $tabteatcher[2];?>" data-required="1" placeholder="<?php echo L::Enterparentprename ?>" class="form-control " />
                                 </div>


                            </div>

                                         </div>
                                         <div class="row">
                                                             <div class="col-md-6">
                                         <div class="form-group">
                                         <label for=""><b><?php echo L::BirthstudentTab ?><span class="required"> * </span>: </b></label>

                                         <input type="text"  placeholder="<?php echo L::EnterBirthstudentTab ?>" value="<?php  echo date_format(date_create($tabteatcher[3]),"d/m/Y");?>" name="datenaisTea" id="datenaisTea" data-mask="99/99/9999" class="form-control ">
                                           <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                         </div>

                                         </div>
                                         <div class="col-md-6">
                                              <div class="form-group">
                                                <label for=""><b><?php echo L::BirthLocation ?>: </b></label>
                                                 <input type="text"  name="lieunaisTea" id="lieunaisTea" data-required="1" value="<?php echo $tabteatcher[11];?>" placeholder="<?php echo L::BirthLocation ?>" class="form-control " />
                                               </div>


                                          </div>

                                                       </div>
                                                       <div class="row">
                                                                           <div class="col-md-6">
                                                       <div class="form-group">
                                                       <label for=""><b><?php echo L::Gender ?> <span class="required"> * </span>: </b></label>

                                                       <select class="form-control "  name="sexeTea" id="sexeTea">

                                                           <option <?php if($tabteatcher[4]=="M"){echo "selected";} ?> value="M"><?php echo L::SexeM ?></option>
                                                           <option <?php if($tabteatcher[4]=="F"){echo "selected";} ?>  value="F"><?php echo L::SexeF ?></option>
                                                       </select>
                                                       </div>

                                                       </div>
                                                       <div class="col-md-6">
                                                         <div class="form-group">
                                                         <label for=""><b><?php echo L::EmailstudentTab ?> <span class="required"> * </span>: </b></label>

                                                       <input type="text"  class="form-control " value="<?php echo $tabteatcher[8];?>"  name="emailTea" id="emailTea" placeholder="<?php echo L::EnterEmailAdress ?>">
                                                         </div>


                                                        </div>

                                                                     </div>
                                                                     <div class="row">
                                                                       <div class="col-md-6">
                                                   <div class="form-group">
                                                   <label for=""><b><?php echo L::Nationalite ?> <span class="required"> * </span>: </b></label>

                                             <input name="natTea" id="natTea" type="text" placeholder="<?php echo L::EnterNationalites ?> " value="<?php echo $tabteatcher[6];?>" class="form-control " />
                                                   </div>

                                                   </div>
                                                                       <div class="col-md-6">
                                                                            <div class="form-group">
                                                                              <label for=""><b><?php echo L::PhonestudentTab ?></b></label>
                                                                             <input  name="contactTea" id="contactTea" value="<?php echo $tabteatcher[5];?>" type="text" placeholder="<?php echo L::EnterPhoneNumber ?> " class="form-control
                                                                             " />
                                                                             </div>


                                                                        </div>



                                                                                   </div>
                                                                                   <div class="row">
                                                                                                       <div class="col-md-6">
                                                                                   <div class="form-group">
                                                                                   <label for=""><b><?php echo L::MatrimonialeSituation ?> <span class="required"> * </span>: </b></label>

                                                                                   <select  class="form-control " name="situationTea" id="situationTea">

                                                                                       <option  value=""><?php echo L::SelectMatrimonialeSituation ?></option>
                                                                                       <option <?php if($tabteatcher[12]=="CELIBATAIRE"){echo "selected";}?> value="CELIBATAIRE"><?php echo L::Celib ?></option>
                                                                                       <option <?php if($tabteatcher[12]=="MARIE(E)"){echo "selected";}?> value="MARIE(E)"><?php echo L::Mariee ?></option>
                                                                                       <option <?php if($tabteatcher[12]=="DIVORCE(E)"){echo "selected";}?> value="DIVORCE(E)"><?php echo L::Divorcee ?></option>
                                                                                       <option <?php if($tabteatcher[12]=="VEUF(VE)"){echo "selected";}?> value="VEUF(VE)"><?php echo L::Veufvee ?></option>
                                                                                   </select>
                                                                                   </div>

                                                                                   </div>
                                                                                   <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                          <label for=""><b><?php echo L::ChildNb ?></b></label>
                                                                                         <input  name="nbchildTea" id="nbchildTea" value="<?php echo $nbTeatcherchild;?>" type="number" min="0" placeholder="<?php echo L::EnterChildNb ?> " class="form-control" readonly />
                                                                                         <input type="hidden" name="etape" id="etape" value="2"/>
                                                                                         <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                                                                         <input type="hidden" name="oldimage" id="oldimage" value="<?php echo $tabteatcher[10];?>" />

                                                                                         </div>


                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                <div class="form-group">
                                                                <label for=""><b><?php echo L::TeatcherType ?> : </b></label>

                                                                <select class="form-control " name="typeTea" id="typeTea">

                                                                    <option selected value=""><?php echo L::SelectTeatcherType ?></option>
                                                                    <option <?php if($tabteatcher[20]==1){echo "selected";}?> value="1"><?php echo L::Titulaire ?></option>
                                                                    <option <?php if($tabteatcher[20]==2){echo "selected";}?> value="2"><?php echo L::Interimaire ?></option>
                                                                </select>
                                                                </div>

                                                                </div>

                                                                                                 </div>
                                                                                               </br>

                                                                                                 <div class="row" id="infantilesRow">
                                                                                 <div class="col-md-12">

                                                                       <fieldset style="background-color:#007bff;">

                                                                       <center><legend style="color:white;"><?php echo L::ChildsInfos ?></legend></center>
                                                                       </fieldset>


                                                                       </div>

                                                                     </div><br/></br>
                                                                    <!-- <div class="row"> -->
                                                                    <div class="pull-right">
                                                                      <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus-o"></i><?php echo L::AddAnChildren ?> </a>
                                                                    </div> </br>

                                                                      <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example45">
                                                                        <thead>
                                                                          <tr>
                                                                           <th style="width:250px;text-align:center;"><?php echo L::NamestudentTab ?></th>
                                                                           <th style="width:150px;text-align:center;"><?php echo L::BirthstudentTab ?></th>
                                                                           <th style="width:200px;text-align:center;"><?php echo L::BirthLocation ?></th>
                                                                           <th style="text-align:center;"><?php echo L::Actions ?></th>

                                                                          </tr>
                                                                        </thead>
                                                                        <tbody id="tablesons">
                                                                          <?php

                                                                          $datas=$teatcher->getAllTeatcherSons($_SESSION['user']['codeEtab'],$libellesessionencours,$teatcherid);
                                                                          foreach ($datas as $value):
                                                                            $i=1;
                                                                           ?>
                                                                          <tr ondblclick="deleted(<?php echo $value->id_childt; ?>)">
                                                                            <td><?php echo utf8_encode(utf8_decode($value->nom_childt)); ?></td>
                                                                              <td><?php echo date_format(date_create($value->datenais_childt),"d/m/Y"); ?></td>
                                                                                <td><?php echo utf8_encode(utf8_decode($value->lieunais_childt)); ?></td>
                                                                                  <td style="text-align:center">
                                                                                    <?php
                                                                                    $filesons=$teatcher->getFileOfSons($value->id_childt);
                                                                                    $lienextrait="";
                                                                                    if(strlen($filesons)>0)
                                                                                    {
                                                                                      $libelledossier=str_replace('\\',"",$teatcher->getNameofTeatcherById($teatcherid))."/Enfants/";
                                                                                      $dossier="../Dossiers/professeurs/";
                                                                                      $dossier1="../Dossiers/professeurs/".$libelledossier;

                                                                                      if(strlen($filesons)>0)
                                                                                      {
                                                                                        $lienextrait=$dossier1.$filesons;
                                                                                      }
                                                                                      ?>
                                                                                      <!-- <a href="#" class="btn btn-info  btn-xs " style="border-radius:3px;" data-toggle="modal" data-target="#exampleModal<?php //echo $value->id_childt; ?>" >  <i class="fa fa-pencil"></i></a> -->
                                                                                      <a href="updatchild.php?compte=<?php echo $value->teatcher_childt; ?>&child=<?php echo $value->id_childt; ?>" class="btn btn-info  btn-xs " style="border-radius:3px;" >  <i class="fa fa-pencil"></i></a>
                                                                                      <a href="#" class="btn btn-danger  btn-xs " style="border-radius:3px;" onclick="deleted(<?php echo $value->id_childt; ?>)" >  <i class="fa fa-trash"></i></a>

                                                                                      <a href="<?php echo $lienextrait; ?>" target="_blank" class="btn btn-warning  btn-xs " style="border-radius:3px;" >  <i class="fa fa-file-o"></i></a>
                                                                                      <?php
                                                                                    }else {
                                                                                      ?>
                                                                                      <a href="#" class="btn btn-info  btn-xs " style="border-radius:3px;" data-toggle="modal" data-target="#exampleModal<?php echo $value->id_childt; ?>" >  <i class="fa fa-pencil"></i></a>
                                                                                      <!-- <a href="#" class="btn btn-info  btn-xs " style="border-radius:3px;" data-toggle="modal" data-target="#exampleModald" onclick="modified(<?php //echo $value->id_childt; ?>)">  <i class="fa fa-pencil"></i></a> -->
                                                                                      <a href="#" class="btn btn-danger  btn-xs " style="border-radius:3px;" onclick="deleted(<?php echo $value->id_childt; ?>)" >  <i class="fa fa-trash"></i></a>

                                                                                      <?php
                                                                                    }
                                                                                     ?>

                                                                                  </td>
                                                                          </tr>
                                                                          <!--div class="modal fade" id="exampleModal<?php //echo $value->id_childt; ?>"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                            <div class="modal-dialog modal-lg" role="document">

                                                                              <div class="modal-content">

                                                                                <div class="modal-header">
                                                                                   <h4 class="modal-title" id="exampleModalLabel">Modifier un enfant</h4>
                                                                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                       <span aria-hidden="true">&times;</span>
                                                                                   </button>
                                                                               </div>
                                                                               <div class="modal-body">


                                                                                 <!-- <form action="../controller/teatcher.php" id="FormUpson" method="post" enctype="multipart/form-data"> -->
                                                                                   <!--form class="" action="../controller/teatcher.php" method="post" id="FormUpson" enctype="multipart/form-data">
                                                                                 <div class="form-group row">
                                                                                         <label class="control-label col-md-4">Nom & prénoms
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                         <div class="col-md-6">
                                                                                             <input type="text" name="namecomplet" id="namecomplet" data-required="1" placeholder="Entrer le nom complet de l'enfant" value="<?php echo $value->nom_childt; ?>" class="form-control" /> </div>
                                                                                     </div>
                                                                                     <div class="form-group row">
                                                                                         <label class="control-label col-md-4">Date de naissance
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                         <div class="col-md-6">
                                                                                           <input type="text" placeholder="Entrer la date de naissance de l'enfant" name="datenaischild" id="datenaischild" data-mask="99/99/9999" class="form-control " value="<?php echo date_format(date_create($value->datenais_childt),"d/m/Y"); ?>">
                                                                                             <span class="help-block"><?php echo L::Datesymbole ?></span></div>
                                                                                           </div>
                                                                                     <div class="form-group row">
                                                                                         <label class="control-label col-md-4">Lieu de naissance
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                         <div class="col-md-6">
                                                                                             <input type="text" name="lieunaischild" id="lieunaischild" data-required="1" placeholder="Entrer le lieu de naissance" class="form-control " value="<?php //echo $value->lieunais_childt; ?>" /> </div>
                                                                                     </div>
                                                                                     <div class="form-group row">
                                                                                         <label class="control-label col-md-4">Genre
                                                                                             <span class="required"> * </span>
                                                                                         </label>
                                                                                         <div class="col-md-6">
                                                                                           <select class="form-control" name="sexechild" id="sexechild" style="width:100%">
                                                                                             <option value="">Merci de selectionner le genre</option>
                                                                                             <option <?php //if($value->sexe_childt=="M"){echo "selected";} ?> value="M">Masculin</option>
                                                                                             <option <?php //if($value->sexe_childt=="F"){echo "selected";} ?> value="F">Féminin</option>
                                                                                           </select>
                                                                                         </div>
                                                                                     </div>


                                                                                           <div class="form-group row">
                                                                                               <label class="control-label col-md-4">Fichier
                                                                                                   <span class="required">  </span>
                                                                                               </label>
                                                                                               <div class="col-md-6">
                                                                                                 <?php

                                                                                                  ?>
                                                                                               <!-- <input type="file" id="fichierExt<?php //echo $value->id_childt; ?>" name="fichierExt<?php //echo $value->id_childt; ?>" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php //echo $lienextrait; ?>" data-allowed-file-extensions="pdf doc docx" /> -->
                                                                                               <!-- <input type="file" id="fichierExt<?php //echo $value->id_childt; ?>" name="fichierExt<?php //echo $value->id_childt; ?>" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb" data-default-file="<?php //echo $lienextrait; ?>" data-allowed-file-extensions="pdf doc docx" /> -->

                                                                                               <!-- <input type="hidden" name="etape" id="etape" value="8"/> -->
                                                                                               <!-- <input type="hidden" name="idcompe" id="idcompte" value="<?php //echo $_GET['compte']?>"/> -->
                                                                                               <!-- <input type="hidden" name="childid" id="childid" value="<?php //echo $value->id_childt; ?>"/> -->
                                                                                               <!-- <input type="hidden" name="codeEtab_upson" id="codeEtab_upson" value="<?php //echo $codeEtabAssigner?>"/> -->
                                                                                               <!-- <input type="hidden" name="oldextrait" id="oldextrait" value="<?php //echo strlen($filesons);?>"/> -->
                                                                                               <!-- <input type="hidden" name="oldfilelien" id="oldextrait" value="<?php //echo $filesons;?>"/> -->
                                                                                             <!-- </div> -->
                                                                                                 <!-- </div> -->
                                                                                                 <!--div class="form-actions">
                                                                                                                       <div class="row">
                                                                                                                           <div class="offset-md-3 col-md-9">
                                                                                                                               <button type="submit" class="btn btn-info">Modifier</button>
                                                                                                                               <button type="button" class="btn btn-danger"><?php echo L::Closebtn?></button>
                                                                                                                           </div>
                                                                                                                         </div>
                                                                                                                      </div-->
                                                                                   </form-->


                                                                               </div>


                                                                              </div>

                                                                            </div>

                                                                          </div-->
                                                                          <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
                                                                          <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                                                          <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
                                                                          <script src="../assets2/js/dropify.js"></script>
                                                                          <script type="text/javascript">
                                                                          $("#fichierExt<?php echo $value->id_childt; ?>").dropify({
                                                                            messages: {
                                                                                "default": "<?php echo L::PleaseSelectExtraitOfchildrens ?>",
                                                                                "replace": "<?php echo L::ChangeFileBirthPaper ?>",
                                                                                "remove" : "<?php echo L::removeFileBirthPaper ?>",
                                                                                "error"  : "<?php echo L::Error ?>"
                                                                            }
                                                                          });
                                                                          </script>
                                                                          <?php
                                                                          $i++;
                                                                        endforeach;

                                                                        // $j=$i+1;
                                                                           ?>
                                                                        </tbody>

                                                                      </table>
                                                                      <input type="hidden" name="nbtablechild" id="nbtablechild" value="<?php echo $i; ?>">
                                                                    <!-- </dhiv> -->
                                                                     <div id="dynamic_field">

                                                                     </div>


                                       <div class="form-actions">
                                                             <div class="row">
                                                                 <div class="col-md-12">
                                                                   <button class="btn btn-success" type="submit"><?php echo L::ModifierBtn ?></button>
                                                                   <!--a class="btn btn-info" href="updateteatcher.php?compte=<?php //echo $_GET['compte']?>" >Modifier</a-->
                                                                   <a class="btn btn-danger" href="teatchers.php" ><?php echo L::AnnulerBtn ?></a>
                                                                 </div>
                                                               </div>
                                                            </div>
                                     </div>
                                 </form>
                                   </div>
                               </div>
                           </div>
                         </div>
                         <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormUpdateProfessionalInfos" class="form-horizontal" action="../controller/teatcher.php" method="POST" >
                                           <div class="form-body">
                                                  <div class="row">
                                                                                 <div class="col-md-12">

                                                                       <fieldset style="background-color:#007bff;">

                                                                       <center><legend style="color:white;"><?php echo L::ProfessionalInfos ?></legend></center>
                                                                       </fieldset>


                                                                       </div>
                                                                     </div><br/>
                                                                               <div class="row">
                                                                                                   <div class="col-md-6">
                                                                               <div class="form-group">
                                                                               <label for=""><b><?php echo L::Cnps ?> : </b></label>

                                                                             <input type="text" name="cnpsTea" id="cnpsTea" value="<?php echo $tabteatcher[14];?>" data-required="1" placeholder="<?php echo L::EnterCnps ?>" class="form-control " />
                                                                               </div>

                                                                               </div>
                                                                               <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                      <label for=""><b><?php echo L::MatriculestudentTab ?> : </b></label>
                                                                                       <input type="text"  name="matTea" id="matTea" value="<?php echo $tabteatcher[15];?>" data-required="1" placeholder="<?php echo L::EnterMatriculestudentTab ?>" class="form-control " />
                                                                                     </div>


                                                                                </div>

                                                                                             </div>
                                                                                             <div class="row">
                                                                                                                 <div class="col-md-6">
                                                                                             <div class="form-group">
                                                                                             <label for=""><b><?php echo L::Mutuelle ?> : </b></label>

                                                                                           <input type="text"  name="mutuelTea" id="mutuelTea" value="<?php echo $tabteatcher[16];?>" data-required="1" placeholder="<?php echo L::EnterMutuelle ?>" class="form-control " />
                                                                                             </div>

                                                                                             </div>
                                                                                             <div class="col-md-6">
                                                                                                  <div class="form-group">
                                                                                                    <label for=""><b><?php echo L::EmbaucheDate ?> : </b></label>
                                                                                                    <input type="text"  placeholder="<?php echo L::EnterEmbaucheDate ?>" name="dateEmbTea" value="<?php  echo date_format(date_create($tabteatcher[3]),"d/m/Y");?>"  id="dateEmbTea" data-mask="99/99/9999" class="form-control ">
                                                                                                      <span class="help-block"><?php echo L::Datesymbole ?></span>
                                                                                                   </div>


                                                                                              </div>

                                                                                                           </div>
                                                                                                           <div class="row">
                                                                                                                               <div class="col-md-6">
                                                                                                           <div class="form-group">
                                                                                                           <label for=""><b><?php echo L::BruteSalaire ?><span class="required"> * </span>: </b></label>

                                                                                                         <input type="number"  min="1" name="brutTea" value="<?php echo $tabteatcher[17];?>" id="brutTea" data-required="1" placeholder="<?php echo L::EnterBruteSalaire ?>" class="form-control " />
                                                                                                           </div>

                                                                                                           </div>
                                                                                                           <div class="col-md-6">
                                                                                                                <div class="form-group">
                                                                                                                  <label for=""><b><?php echo L::NutSalaire ?> : </b></label>
                                                                                                               <input type="number"  min="1" name="netTea" id="netTea" value="<?php echo $tabteatcher[18];?>" data-required="1" placeholder="<?php echo L::EnterNutSalaire ?>" class="form-control " />
                                                                                                               <input type="hidden" name="etape" id="etape" value="3"/>
                                                                                                               <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>
                                                                                                                 </div>


                                                                                                            </div>

                                                                                                                         </div>

                                       <div class="form-actions">
                                                             <div class="row">
                                                                 <div class=" col-md-12">
                                                                   <button type="submit" class="btn btn-success"><?php echo L::ModifierBtn ?></button>
                                                                   <!--a class="btn btn-info" href="updateteatcher.php?compte=<?php //echo $_GET['compte']?>" >Modifier</a-->
                                                                   <a class="btn btn-danger" href="teatchers.php" ><?php echo L::AnnulerBtn ?></a>
                                                                 </div>
                                                               </div>
                                                            </div>
                                     </div>
                                 </form>
                                   </div>
                               </div>
                           </div>
                         </div>
                         <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">
                           <div class="col-md-12 col-sm-12">
                               <div class="card card-box">
                                   <div class="card-head">
                                       <header></header>

                                   </div>

                                   <div class="card-body" id="bar-parent">
                                       <form  id="FormAddLocalAd" class="form-horizontal"  >
                                           <div class="form-body">
                                             <div class="row">
                             <div class="col-md-12">

                   <fieldset style="background-color:#007bff;">

                   <center><legend style="color:white;"><?php echo L::DiplomesTeatchers ?></legend></center>
                   </fieldset>


                   </div>
                 </div><br/>
                 <!-- <div class="row"> -->


                   <div class="pull-right">
                     <!-- <a href="adddiplomes.php?compte=<?php //echo $_GET['compte']?>" class="btn btn-warning btn-md"><i class="fa fa-plus"></i>Nouveau diplome</a> -->
                     <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#exampleModald"><i class="fa fa-plus"></i><?php echo L::DiplomesTeatchersingle ?></a>
                   </div


                 <br/>
                 <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                  <thead>
                      <tr>


                          <th style="text-align:center"> <?php echo L::DiplomesTeatchersingleCards ?> </th>
                          <!-- <th> <?php echo L::Ecole?> </th> -->
                          <th style="text-align:center"> <?php echo L::Speciality ?> </th>
                          <th style="text-align:center"> <?php echo L::Level ?></th>
                          <!-- <th> <?php echo L::DateObtain?> </th> -->
                          <th style="text-align:center"> <?php echo L::File ?> </th>

                      </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1;
                      foreach ($diplomes as $value):
                      ?>
                      <tr class="odd gradeX">

                        <td style="text-align:center">
                          <?php echo $value->libelle_diplo;?>
                        </td>
                          <!-- <td>
            <?php //echo $value->ecole_diplo;?>
                          </td> -->
                          <td style="text-align:center">
                              <span class="label label-md label-default"><?php echo $value->specialite_diplo;?>  </span>
                          </td>
                          <td style="text-align:center">
            <?php echo $value->niveau_diplo;?>
                          </td>
                          <!-- <td>
                            <?php //echo date_format(date_create($value->dateobt_diplo),"d/m/Y");?>
                          </td> -->
                            <td>
                                <a href="#" onclick="deleteDiplome(<?php echo $value->id_diplo; ?>)" class="btn btn-danger btn-xs" title="<?php echo L::DiplomesTeatchersingleDeleted ?>"><span class="fa fa-trash"></span></a>
                              <?php
                              if(strlen($value->fichier_diplo)>0)
                              {
                                $emailTea=$diplome->getTeatcherMail($_GET['compte']);
                                $lien="../diplomes/professeur/".$emailTea."/".$value->fichier_diplo;
                                $libelledossier=str_replace('\\',"",$teatcher->getNameofTeatcherById($teatcherid))."/diplomes/";
                                $dossier="../Dossiers/professeurs/";
                                $dossier1="../Dossiers/professeurs/".$libelledossier;
                                $lienfile=$dossier1.$value->fichier_diplo;
                              ?>
                             <a href="<?php echo $lienfile;?>" target="_blank"  class="btn btn-warning btn-xs" title="<?php echo L::SeeFile ?>" ><span class="fa fa-file-o"></span></a>
                              <?php
                              }else {
                                ?>
                             <a href="#" target="_blank" class="btn btn-danger" title="<?php echo L::SeeFile ?>"><span class="fa fa-times"></span></a>
                                <?php
                              }
                               ?>
                            </td>

                      </tr>
                      <?php
                                                       $i++;
                                                       endforeach;
                                                       ?>


                  </tbody>
              </table>
                 <!-- </div> -->
                 <div class="form-actions">
                                       <div class="row">
                                           <div class="offset-md-3 col-md-9">
                                             <a class="btn btn-info" href="updateteatcher.php?compte=<?php echo $_GET['compte']?>" ><?php echo L::ModifierBtn ?></a>
                                             <a class="btn btn-danger" href="teatchers.php" ><?php echo L::AnnulerBtn ?></a>
                                           </div>
                                         </div>
                                      </div>

                                                </div>


                                     <!-- </div> -->
                                 </form>
                                   </div>
                               </div>
                           </div>
                           </div>
                           <div class = "mdl-tabs__panel p-t-20" id = "tab7-panel">
                             <div class="col-md-12 col-sm-12">
                                 <div class="card card-box">
                                     <div class="card-head">
                                         <header></header>
                                          <!--button id = "panel-button"
                                        class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded = ",MaterialButton">
                                        <i class = "material-icons">more_vert</i>
                                     </button>
                                     <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                        data-mdl-for = "panel-button">
                                        <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                        <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                     </ul-->
                                     </div>

                                     <div class="card-body" id="bar-parent">
                                         <form  id="FormUpdatecompte" class="form-horizontal" action="../controller/teatcher.php" method="post" enctype="multipart/form-data">
                                             <div class="form-body">

                                                 <div class="row">
                                               <div class="col-md-12">
                                                 <fieldset style="background-color:#007bff;">

                                               <center><legend style="color:white;"><?php echo L::AccountInfos ?></legend></center>
                                                 </fieldset>


                                                                                                 </div>
                                                                                               </div><br/>
                                                                                                  <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Logincnx ?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="loginTea" id="loginTea" type="text" placeholder="<?php echo L::LoginEnter ?> " <?php echo $tabteatcher[19];?> class="form-control t" /> </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::Passcnx ?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="passTea" id="passTea" type="password" placeholder="<?php echo L::PasswordEnter ?> " class="form-control " /> </div>
                                                                                                   </div>
                                                                                                   <div class="form-group row">
                                                                                                       <label class="control-label col-md-3"><?php echo L::ConfirmPasscnx ?>
                                                                                                           <span class="required">*  </span>
                                                                                                       </label>
                                                                                                       <div class="col-md-5">
                                                                                                           <input name="confirmTea" id="confirmTea" type="password" placeholder="<?php echo L::ConfirmPassword ?> " class="form-control " /> </div>
                                                                                                           <input type="hidden" name="etape" id="etape" value="4"/>
                                                                                                            <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $_GET['compte'];?>"/>

                                                                                                   </div>
                                                                                                   <div class="form-actions">
                                                                                                                         <div class="row">
                                                                                                                             <div class="offset-md-3 col-md-9">
                                                                                                                                 <button class="btn btn-success" type="submit"><?php echo L::ModifierBtn ?></button>
                                                                                                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                                                                             </div>
                                                                                                                           </div>
                                                                                                                        </div>

                                             </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                             </div>
                         </div>
                      </div>
                </div>
              </div>
            </div>
              </div>
          </div>
      </div>
            <!-- debut modal champ -->
            <!-- fin modal champ -->
            <div class="modal fade" id="exampleModal"  tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">

                <div class="modal-content">

                  <div class="modal-header">
                     <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddAnChildrensingle ?></h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">


                   <form  action="../controller/teatcher.php" id="FormAddson" method="post" enctype="multipart/form-data">

                   <div class="form-group row">
                           <label class="control-label col-md-4"><?php echo L::NamestudentTab ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-6">
                               <input type="text" name="namecomplet" id="namecomplet" data-required="1" placeholder="<?php echo L::EnterChildCompletName ?>" class="form-control" /> </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-4"><?php echo L::BirthstudentTab ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-6">
                             <input type="text" placeholder="<?php echo L::EnterChildDateNaiss ?>" name="datenaischild" id="datenaischild" data-mask="99/99/9999" class="form-control ">
                               <span class="help-block"><?php echo L::Datesymbolesecond ?></span></div>
                             </div>
                       <div class="form-group row">
                           <label class="control-label col-md-4"><?php echo L::BirthLocation ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-6">
                               <input type="text" name="lieunaischild" id="lieunaischild" data-required="1" placeholder="<?php echo L::EnterBirthLocation ?>" class="form-control " /> </div>
                       </div>
                       <div class="form-group row">
                           <label class="control-label col-md-4"><?php echo L::Gender ?>
                               <span class="required"> * </span>
                           </label>
                           <div class="col-md-6">
                             <select class="form-control" name="sexechild" id="sexechild" style="width:100%">
                               <option value=""><?php echo L::PleaseSelectGender ?></option>
                               <option value="M"><?php echo L::SexeM ?></option>
                               <option value="F"><?php echo L::SexeF ?></option>
                             </select>
                           </div>
                       </div>


                             <div class="form-group row">
                                 <label class="control-label col-md-4"><?php echo L::File ?>
                                     <span class="required">  </span>
                                 </label>
                                 <div class="col-md-6">
                                 <input type="file" id="fichierExt" name="fichierExt" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb"  data-allowed-file-extensions="pdf doc docx" />
                                 <input type="hidden" name="etape" id="etape" value="6"/>
                                 <input type="hidden" name="idcompe" id="idcompte" value="<?php echo $_GET['compte']?>"/>
                                 <input type="hidden" name="codeEtab_addson" id="codeEtab_addson" value="<?php echo $_SESSION['user']['codeEtab']?>"/>
                               </div>
                                   </div>
                                   <div class="form-actions">
                                                         <div class="row">
                                                             <div class="offset-md-3 col-md-9">
                                                                 <button type="submit" class="btn btn-info"><?php echo L::AddMenu ?></button>
                                                                 <button type="button" class="btn btn-danger"><?php echo L::Closebtn ?></button>
                                                             </div>
                                                           </div>
                                                        </div>
                     </form>


                 </div>


                </div>

              </div>

            </div>


            <div class="modal fade" id="exampleModald" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          					    <div class="modal-dialog modal-lg" role="document">
          					        <div class="modal-content">
          					            <div class="modal-header">
          					                <h4 class="modal-title" id="exampleModalLabel"><?php echo L::AddaDegree ?></h4>
          					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          					                    <span aria-hidden="true">&times;</span>
          					                </button>
          					            </div>
          					            <div class="modal-body">
                                  <form action="../controller/diplomes.php" id="FormDiplAdd" method="post" enctype="multipart/form-data">
                                  <div class="form-group row">
                                          <label class="control-label col-md-4"><?php echo L::DegreeDesignation ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input type="text" name="libellediplo" id="libellediplo" data-required="1" placeholder="<?php echo L::EnterDegreeDesignation ?>" class="form-control" /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-4"><?php echo L::Ecole ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input type="text" name="libecole" id="libecole" data-required="1" placeholder="<?php echo L::EnterEcole ?>" class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-4"><?php echo L::Speciality ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input type="text" name="speciale" id="speciale" data-required="1" placeholder="<?php echo L::EnterSpeciality ?>" class="form-control " /> </div>
                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-4"><?php echo L::Level ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-6">
                                              <input type="text" name="nivodiplo" id="nivodiplo" data-required="1" placeholder="Entrer le Niveau" class="form-control " />
                                              <input type="hidden" name="etape" id="etape" value="1"/>
                                              <input type="hidden" name="idcompe" id="idcompte" value="<?php echo $_GET['compte']?>"/>
                                              <input type="hidden" name="codeEtab_diplo" id="codeEtab_diplo" value="<?php echo $_SESSION['user']['codeEtab'];?>"/>
                                            </div>

                                      </div>
                                      <div class="form-group row">
                                          <label class="control-label col-md-4"><?php echo L::DateObtain ?>
                                              <span class="required"> * </span>
                                          </label>
                                          <div class="col-md-6">
                                            <input type="text" placeholder="Entrer la date d'Obtention" name="dateo" id="dateo" data-mask="99/99/9999" class="form-control ">
                                              <span class="help-block"><?php echo L::Datesymbolesecond ?></span></div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-4"><?php echo L::File ?>
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                <input type="file" id="fichier" name="fichier" class="default" class="dropify"  data-show-loader="true" data-max-file-size="1mb"  data-allowed-file-extensions="pdf doc docx" />
                                              </div>
                                                  </div>
                                                  <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="offset-md-3 col-md-9">
                                                                                <button type="submit" class="btn btn-info"<?php echo L::AddMenu ?></button>
                                                                                <button type="button" class="btn btn-danger"><?php echo L::Closebtn ?></button>
                                                                            </div>
                                                                          </div>
                                                                       </div>
                                    </form>
          					            </div>

          					        </div>
          					    </div>
          					</div>

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

   <script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

     <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>

     <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>

     <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>





     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>



     <!-- data tables -->

     <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
   <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" ></script>
      <script src="../assets2/js/pages/table/table_data.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" ></script>
   	<script src="../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" ></script>
   	<script src="../cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" ></script>



     <!-- Common js-->
     <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>

     <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
     <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <script src="../assets2/js/app.js" ></script>

     <script src="../assets2/js/pages/validation/form-validation.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

   <script src="../assets2/js/theme-color.js" ></script>

   <script src="../assets2/dropify/dist/js/dropify.min.js"></script>

   <script src="../assets2/js/dropify.js"></script>

   <script src="../assets2/plugins/select2/js/select2.js" ></script>

   <script src="../assets2/js/pages/select2/select2-init.js" ></script>

   <!-- Material -->

   <script src="../assets2/plugins/material/material.min.js"></script>

   <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

   <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

$("#sexechild").select2();
 function modify(id)
 {
   Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "<?php echo L::DoyouReallyModifyingChildInfos ?>",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: '<?php echo L::ModifierBtn ?>',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
 }).then((result) => {
 if (result.value) {
 document.location.href="updateteatcher.php?compte="+id;
 }else {

 }
 })
 }

function modified(id)
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyDeletedChildInfos ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
// document.location.href="updateteatcher.php?compte="+id;
}else {

}
})
}

function recalculNbchild()
{
  var nbtablechild=$("#nbtablechild").val();

  $("#nbchildTea").val(nbtablechild);

}


function deleted(id)
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyDeletedChilds ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
var etape=7;
var teatcherid="<?php echo $teatcherid; ?>";
var childid=id;
  $.ajax({
    url: '../ajax/teatcher.php',
    type: 'POST',
    async:false,
    data: 'teatcherid=' +teatcherid+ '&etape=' + etape+'&childid='+childid,
    dataType: 'text',
    success: function (response, statut) {

      location.reload();

      // $("#tablesons").html("");
      // $("#tablesons").html(response);

    }
  });

}else {

}
})
}


function deleteDiplome(id)
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouReallyDeletedDiplomes ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::DeleteLib ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
var etape=8;
var teatcherid="<?php echo $teatcherid; ?>";
var diplomeid=id;
  $.ajax({
    url: '../ajax/teatcher.php',
    type: 'POST',
    async:false,
    data: 'teatcherid=' +teatcherid+ '&etape=' + etape+'&diplomeid='+diplomeid,
    dataType: 'text',
    success: function (response, statut) {

      location.reload();

      // $("#tablesons").html("");
      // $("#tablesons").html(response);

    }
  });

}else {

}
})
}





 function getallteatcherchild()
 {
    var nbchild=$("#nbchildTea").val();
    var teatcherid="<?php echo $teatcherid; ?>";
    var session="<?php  echo $libellesessionencours; ?>";
    var codeEtab="<?php echo $_SESSION['user']['codeEtab']; ?>";
    var etape=6;
    if(nbchild>0)
    {
      $("#infantilesRow").show();
      //nous allons chercher a ajouter la liste des enfants de cet enseignant
      $.ajax({
        url: '../ajax/teatcher.php',
        type: 'POST',
        async:false,
        data: 'session=' +session+ '&etape=' + etape+'&codeEtab='+codeEtab+'&nbchild='+nbchild+'&teatcherid='+teatcherid,
        dataType: 'text',
        success: function (response, statut) {

          $("#tablesons").html("");
          $("#tablesons").html(response);

          // var nbligne=$("#tablesons tr").length;

          // alert(nbligne);

        }
      });

    }
 }

 $('#example45').DataTable( {
   "scrollX": true,
   "language": {
       "lengthMenu": "_MENU_  ",
       "zeroRecords": "Aucune correspondance",
       "info": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
       "infoEmpty": "Aucun enregistrement disponible",
       "infoFiltered": "(filtered from _MAX_ total records)",
       "sEmptyTable":"Aucune donnée disponible dans le tableau",
        "sSearch":"Rechercher :",
        "oPaginate": {
   "sFirst":    "Premier",
   "sLast":     "Dernier",
   "sNext":     "Suivant",
   "sPrevious": "Précédent"
}
   }
 } );

 jQuery(document).ready(function() {



   // $("#infantilesRow").hide();

   // getallteatcherchild();
    // recalculNbchild();

   $("#FormDiplAdd").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{


        libellediplo:"required",
        libecole:"required",
        speciale:"required",
        nivodiplo:"required",
        dateo:"required",
        // fichier:"required"

      },
      messages: {

        libellediplo:"<?php echo L::PleaseEnterDegreeLib ?>",
        libecole:"<?php echo L::PleaseEnterEcoleLib ?> ",
        speciale:"<?php echo L::PleaseEnterSpeciality ?>",
        nivodiplo:"<?php echo L::PleaseEnterLevelDegree ?>",
        dateo:"<?php echo L::PleaseEnterDateObtainDegree ?>",
        // fichier:"<?php echo L::PleasechooseFileDegree?>"
       },

       submitHandler: function(form) {

        form.submit();


       }


   });

   $("#FormUpson").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       namecomplet:"required",
       datenaischild:"required",
       lieunaischild:"required",
       sexechild:"required"
     },
     messages:{
       namecomplet:"<?php echo L::PleaseEnterChildName ?>",
       datenaischild:"<?php echo L::PleaseEnterPhonestudentTab ?>",
       lieunaischild:"<?php echo L::PleaseEnterBirthLocation ?>",
       sexechild:"<?php echo L::PleaseSelectGender ?>"
     }

   });

   $("#FormAddson").validate({
     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{
       namecomplet:"required",
       datenaischild:"required",
       lieunaischild:"required",
       sexechild:"required"
     },
     messages:{
       namecomplet:"<?php echo L::PleaseEnterChildName ?>",
       datenaischild:"<?php echo L::PleaseEnterPhonestudentTab ?>",
       lieunaischild:"<?php echo L::PleaseEnterBirthLocation ?>",
       sexechild:"<?php echo L::PleaseSelectGender ?>"
     }

   });

   $("#FormUpdatePersonalInfo").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        nomTea:"required",
        prenomTea:"required",
        datenaisTea:"required",
        lieunaisTea:"required",
        sexeTea:"required",
        emailTea:"required",
        natTea:"required",
        contactTea:"required",
        situationTea:"required",
        nbchildTea:"required",

      },
      messages: {
        lieunaisTea:"<?php echo L::PleaseEnterBirthLocation ?>",
        natTea:"<?php echo L::PleaseEnterNationalite ?>",
        situationTea:"<?php echo L::PleaseSelectMatrimonialeSituation ?>",
        nbchildTea:"<?php echo L::PleaseEnterChildNb ?>",
        emailTea:"<?php echo L::PleaseEnterEmailAdress ?>",
        contactTea:"<?php echo L::PleaseEnterPhoneNumber ?>",
        datenaisTea:"<?php echo L::PleaseEnterPhonestudentTab ?>",
        prenomTea:"<?php echo L::PleaseEnterPrename ?>",
        nomTea:"<?php echo L::PleaseEnterName ?> ",
        sexeTea:"<?php echo L::PleaseEnterSexe ?>",


      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
       form.submit();

      }


   });


   $("#FormUpdateProfessionalInfos").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // cnpsTea
        // matTea
        // mutuelTea
        dateEmbTea:"required",
        brutTea:"required",
        netTea:"required"

      },
      messages: {
        dateEmbTea:"<?php echo L::EnterDateEmbauche ?>",
          brutTea:"<?php echo L::PleaseEnterBruteSalaire ?>",
          netTea:"<?php echo L::PleaseEnterNutSalaire ?>"


      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
       form.submit();

      }


   });

   $("#FormUpdatecompte").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        // cnpsTea
        // matTea
        // mutuelTea
        loginTea:"required",
        passTea: {
            required: true,
            minlength: 6
        },
        confirmTea:{
            required: true,
            minlength: 6,
            equalTo:'#passTea'
        },


      },
      messages: {
        loginTea:"<?php echo L::Logincheck ?>",
        confirmTea:{
            required:"<?php echo L::Confirmcheck ?>",
            minlength:"<?php echo L::Passmincheck ?>",
            equalTo: "<?php echo L::ConfirmSamecheck ?>"
        },
        passTea: {
            required:"<?php echo L::Passcheck ?>",
            minlength:"<?php echo L::Passmincheck ?>"
        }

      },
      submitHandler: function(form) {
        //verifier si ce compte n'existe pas encore dans la base de données
       form.submit();

      }


   });


 });
 </script>
    <!-- end js include path -->
  </body>

</html>
