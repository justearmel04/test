
<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

 $codeEtabAssigner =$_GET['$codeEtabAssigner'] ;

 $idClasse=$_GET['$idClasse'];

 $etabs=new Etab();

 $etabs->getEtabLibellebyCodeEtab($codeEtabAssigner);



try{
      $bdd = new PDO('mysql:host=www.proximity-cm.com;dbname=proximi5_xschool', 'proximi5_xschool', 'Psa@123456');

   }
catch(PDoExeption $e)
{
   $masseErreur='Erreur PDO dans'.$e->getMessage();
    die($masseErreur);
}


/*
  try{
        $bdd = new PDO('mysql:host=localhost;dbname=xschool', 'root', '');

     }
  catch(PDoExeption $e)
  {
     $masseErreur='Erreur PDO dans'.$e->getMessage();
      die($masseErreur);
  }
  */


 // recuperer l'etablissement des eleves


      $ps=$bdd->prepare("select * from etablissement where code_etab=?");
      $parametre=array($codeEtabAssigner);
      $ps->execute($parametre);
      $donnees=$ps->fetch();





 // recuperer la classe des  eleves


      $ps1=$bdd->prepare("select * from classe where id_classe=?");
      $parametre=array($idClasse);
      $ps1->execute($parametre);
      $donnees1=$ps1->fetch();


require('fpdf/fpdf.php');


$pdf = new FPDF();
$pdf->SetFont('Times','B',  16);
$pdf->AddPage();


      $pdf->Ln(20);
      $pdf-> SetFont('Times','B',  15);
      $pdf->Cell(176,5,$donnees['libelle_etab'],0,0,'C');
      $pdf->Ln(10);
      $pdf-> SetFont('Times','',  13);
      $pdf->Cell(176,5,utf8_decode(L::CompletesListeStudents),0,0,'C');
      $pdf->Ln(10);

      $pdf-> SetFont('Times','',  11);
      $pdf->Cell(176,5,L::ScolaryyearMenu.' : 2019-2020',0,0,'C');
      $pdf->Ln(10);

      $pdf-> SetFont('Times','',  11);
      $pdf->Cell(176,5,$donnees1['libelle_classe'],0,0,'C');
      $pdf->Ln(20);

      $pdf->SetFillColor(230,230,0);
      $pdf->SetLineWidth(.3);
      $pdf->SetFont('Times','B',12);
      $pdf->Cell(50,8,L::Name,1,0,'C');
      $pdf->Cell(50,8,utf8_decode(L::PreName),1,0,'C');
      $pdf->Cell(50,8,L::EmailstudentTab,1,0,'C');
      $pdf->Cell(20,8,L::MatriculestudentTab,1,0,'C');
      $pdf->Cell(20,8,L::BirthstudentTabShort,1,0,'C');

      $pdf->Ln();


      $pdf->SetFont('Times','B',6);
      $pdf->SetFillColor(96,96,96);

      $stmt = $bdd->query('select * from eleve');

      while ( $data=$stmt->fetch()){

        $pdf->Cell(50,5,$data['nom_eleve'],1,0,'L');
        $pdf->Cell(50,5,$data['prenom_eleve'],1,0,'L');
        $pdf->Cell(50,5,$data['email_eleve'],1,0,'L');
        $pdf->Cell(20,5,$data['matricule_eleve'],1,0,'L');
        $pdf->Cell(20,5,$data['datenais_eleve'],1,0,'L');

       $pdf->Ln();
     }



      $pdf->Output();

?>
