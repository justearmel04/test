<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Teatcher.php');
require_once('../class/Matiere.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$teatcher=new Teatcher();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

// $codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
if(strlen($_SESSION['user']['codeEtab'])>0)
{
  $codeEtabAssigner=$_SESSION['user']['codeEtab'];
}else {
  $codeEtabAssigner=$etabs->getcodeEtabFirstByLocalId($_SESSION['user']['IdCompte']);
}
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement
$schoolsofassign=$etabs->getEtablissementbyCodeEtab($codeEtabAssigner);

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codesEtab=$schoolsofassign;
$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$classes=$classe->getAllClassesbyschoolCode($codeEtabLocal);
$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabLocal);
$matieres=$matiere->getAllMatiereOfThisSchool($codeEtabLocal);

$examens=$etabs->getAllExamensOfThisSchool($codeEtabLocal);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
}



// session_start();
// require_once('../class/User.php');
// require_once('../class/Parent.php');
// require_once('../class/Classe.php');
//
// require_once('../class/Teatcher.php');
// require_once('../class/Matiere.php');
// $etab=new Etab();
// $parent=new ParentX();
// $user=new User();
// $classe=new Classe();

// $emailUti=$_SESSION['user']['email'];
//
// $imageprofile=$user->getImageProfile($emailUti);
// $logindata=$user->getLoginProfile($emailUti);
// $tablogin=explode("*",$logindata);
// $datastat=$user->getStatis();
// $tabstat=explode("*",$datastat);
// $classeId=$_GET['classe'];
//
//
// if(strlen($imageprofile)>0)
// {
//   $lienphoto="../photo/".$emailUti."/".$imageprofile;
// }else {
//   $lienphoto="../photo/user5.jpg";
// }
//
// $parents=$parent->getAllParent();
//
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);

//les 10 dernières notifications
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
$onlineUsers=$etabs->getonlinesUsers($_SESSION['user']['codeEtab']);
$offlineUsers=$etabs->getofflinesUsers($_SESSION['user']['codeEtab']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
        <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::GestiondesExamens ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="#"><?php echo L::MatiereMenusingle ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::ExamssMenu ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['deletesubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['deletesubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })


                </script>
                      <?php
                      unset($_SESSION['user']['deletesubjectok']);
                    }

                     ?>


              <?php

                    if(isset($_SESSION['user']['updateExamok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>

                Swal.fire({
                type: 'success',
                title: '<?php echo L::Felicitations ?>',
                text: "<?php echo $_SESSION['user']['updateExamok'];?>",

              })


                </script>
                      <?php
                      unset($_SESSION['user']['updateExamok']);
                    }

                     ?>
              <?php

                    if(isset($_SESSION['user']['addsubjectok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>


                Swal.fire({
              title: '<?php echo L::Felicitations ?>',
              text: "<?php echo $_SESSION['user']['addsubjectok']; ?>",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: '<?php echo L::Okay ?>',

              }).then((result) => {
              if (result.value) {
              window.location.reload();
              }
              })
                </script>
                      <?php
                      unset($_SESSION['user']['addsubjectok']);
                    }

                     ?>

                     <div class="col-md-12 col-sm-12">
                                   <div class="panel tab-border card-box">
                                       <header class="panel-heading panel-heading-gray custom-tab ">
                                           <ul class="nav nav-tabs">
                                               <li class="nav-item"><a href="#home" data-toggle="tab" class="active"><i class="fa fa-bars"></i> <?php echo L::ListsExamens ?></a>
                                               </li>
                                               <li class="nav-item"><a href="#about" data-toggle="tab"><i class="fa fa-plus-circle"></i> <?php echo L::AddExamenscoming ?></a>
                                               </li>

                                           </ul>
                                       </header>
                                       <div class="panel-body">
                                           <div class="tab-content">
                                               <div class="tab-pane active" id="home">
                                                 <div class="row">
                        <div class="col-md-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header></header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                        <thead>
                                            <tr>

                                                <th> <?php echo L::ExamssMenusingle ?> </th>
                                                <th> <?php echo L::DatedebLib ?></th>
                                                <th> <?php echo L::DatefinLib ?> </th>

                                                <th> <?php echo L::Actions ?> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            //$matieres

                                            $i=1;
                                              foreach ($examens as $value):
                                           ?>
                                            <tr class="odd gradeX">

                                                <td> <?php echo $value->libelle_exam;?></td>
                                                <td>
                                                    <?php echo date_format(date_create($value->du_exam),"d/m/Y");?>
                                                </td>
                                                <td>
                                                  <?php echo date_format(date_create($value->au_exam),"d/m/Y");?>
                                                </td>

                                                <td class="valigntop">
                                                  <a href="#"  data-toggle="modal" data-target="#exampleModal<?php echo $value->id_exam?>"class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a>
                                                  <!--a href="#"  onclick="modify(<?php //echo $value->id_mat;?>)" class="btn btn-info  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-pencil"></i>
                                                  </a-->
                                                  <a href="#"  onclick="deleted(<?php echo $value->id_exam;?>)" class="btn btn-danger  btn-md " style="border-radius:3px;">
                                                    <i class="fa fa-trash-o"></i>
                                                  </a>
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="exampleModal<?php echo $value->id_exam;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php echo $value->id_exam;?>" aria-hidden="true">
                                            					    <div class="modal-dialog" role="document">
                                            					        <div class="modal-content">
                                            					            <div class="modal-header">
                                            					                <h4 class="modal-title" id="exampleModalLabel<?php echo $value->id_exam;?>"><?php echo L::ModificationExamen ?></h4>
                                            					                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            					                    <span aria-hidden="true">&times;</span>
                                            					                </button>
                                            					            </div>
                                            					            <div class="modal-body">
                                                                    <form  id="FormUpdateExam<?php echo $value->id_exam;?>" class="form-horizontal" action="../controller/examen.php" method="post">
                                                                        <div class="form-body">
                                                                          <div class="form-group row">
                                                                                  <label class="control-label col-md-3"><?php echo L::ExamssMenusingle ?>
                                                                                      <span class="required"> * </span>
                                                                                  </label>
                                                                                  <div class="col-md-8">
                                                                                      <input type="text" name="exam<?php echo $value->id_exam;?>" id="exam<?php echo $value->id_exam;?>" onchange="erasedExam(<?php echo $value->id_exam;?>)" data-required="1" value="<?php echo $value->libelle_exam;?>" placeholder="Entrer la classe" class="form-control input-height" />
                                                                                      <p id="messageExam<?php echo $value->id_exam;?>"></p>
                                                                                     </div>
                                                                           </div>
                                                                           <div class="form-group row">
                                                                             <label class="control-label col-md-3"><?php echo L::DatedebLib ?>
                                                                                 <span class="required"> * </span>
                                                                             </label>
                                                                                 <div class="col-md-8">
                                                                                     <input type="date"  placeholder="<?php echo L::EnterDatedebLib ?>" name="datedeb<?php echo $value->id_exam;?>" id="datedeb<?php echo $value->id_exam ;?>"  value="<?php echo $value->du_exam;?>" class="form-control input-height">
                                                                                     <p id="messageDeb<?php echo $value->id_exam;?>"></p>
                                                                                 </div>
                                                                             </div>
                                                                             <div class="form-group row">
                                                                               <label class="control-label col-md-3"><?php echo L::DatefinLib ?>
                                                                                   <span class="required"> * </span>
                                                                               </label>
                                                                                   <div class="col-md-8">
                                                                                       <input type="date" placeholder="<?php echo L::EnterDatefinLib ?>" name="datefin<?php echo $value->id_exam;?>" id="datefin<?php echo $value->id_exam;?>" value="<?php echo  $value->du_exam;?>"  class="form-control input-height">
                                                                                       <p id="messageFin<?php echo $value->id_exam;?>"></p>
                                                                                       <input type="hidden" name="etape" id="etape" value="2"/>
                                                                                       <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                       <input type="hidden" name="idexam" id="idexam" value="<?php echo $value->id_exam; ?>"/>
                                                                                   </div>
                                                                               </div>


                                                      <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="offset-md-3 col-md-9">
                                                                                    <button type="submit" onclick="check(<?php echo $value->id_exam;?>)" class="btn btn-info"><?php echo L::ModifierBtn ?></button>
                                                                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::AnnulerBtn ?></button>
                                                                                </div>
                                                                              </div>
                                                                           </div>
                                                    </div>
                                                                    </form>
                                            					            </div>

                                            					        </div>
                                            					    </div>
                                            					</div>

                                            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                            <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
                                            <!-- <script src="../assets2/plugins/select2/js/select2.js" ></script>
                                            <script src="../assets2/js/pages/select2/select2-init.js" ></script> -->
                                            <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
                                            <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                            <script>

                                            function soumettre()
                                            {
                                              $("#FormUpdateExam<?php echo $value->id_exam;?>").submit();
                                            }

                                            function myFunction(idcompte)
                                            {
                                              //var url="detailslocal.php?compte="+idcompte;
                                            document.location.href="detailsadmin.php?compte="+idcompte;
                                            }

                                            function erasedExam(id)
                                            {
                                              document.getElementById("messageExam"+id).innerHTML = "";

                                            }

                                            function erasedCoef(id)
                                            {
                                              document.getElementById("messageCoef"+id).innerHTML = "";
                                            }

                                            function erasedClasse(id)
                                            {
                                              document.getElementById("messageClasse"+id).innerHTML = "";
                                            }

                                            function erasedTeatcher(id)
                                            {
                                              document.getElementById("messageTeatcher"+id).innerHTML = "";
                                            }

                                            function modify(id)
                                            {


                                              Swal.fire({
                                title: '<?php echo L::WarningLib ?>',
                                text: "<?php echo L::DoyouReallyModifyingSubjects ?>",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '<?php echo L::ModifierBtn ?>',
                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="updatesubject.php?compte="+id;
                                }else {

                                }
                              })
                                            }

                                            function deleted(id)
                                            {
                                              //var examen="<?php echo $value->id_exam;?>";
                                              var codeEtab="<?php echo $codeEtabAssigner;?>";
                                              Swal.fire({
                                title: '<?php echo L::WarningLib ?>',
                                text: "<?php echo L::DoyouDeletingThisExamens ?>",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: '<?php echo L::DeleteLib ?>',
                                cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                              }).then((result) => {
                                if (result.value) {
                                  document.location.href="../controller/examen.php?etape=4&examen="+id+"&codeEtab="+codeEtab;
                                }else {

                                }
                              })
                                            }

                                            function check(id)
                                            {
                                              //recuperation des variables
                                              var exam=$("#exam"+id).val();
                                              var datedeb=$("#datedeb"+id).val();
                                              var datefin=$("#datefin"+id).val();

                                              event.preventDefault();

                                              if(exam==""||datedeb==""||datefin=="")
                                              {
                                                if(exam=="")
                                                {
                                                   document.getElementById("messageExam"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseEnterExamenLibelle ?></font>";
                                                }

                                                if(datedeb=="")
                                                {
                                                  document.getElementById("messageDeb"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseEnterParascoActivityDateStart ?></font>";
                                                }

                                                if(datefin=="")
                                                {
                                                  document.getElementById("messageFin"+id).innerHTML = "<font color=\"red\"><?php echo L::PleaseEnterParascoActivityDateEnd ?> !</font>";
                                                }






                                            }else {

                                                  soumettre();
                                            }

                                            }



















                                            </script>

                                            <?php
                                            $i++;
                                            endforeach;
                                            ?>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                                               </div>
                                               <div class="tab-pane" id="about">
                                                 <?php
                                                 if($nbsessionOn>0)
                                                 {
                                                  ?>
                                                 <div class="row">
                                                     <div class="col-md-12 col-sm-12">
                                                         <div class="card card-box">
                                                             <div class="card-head">
                                                                 <header></header>

                                                             </div>

                                                             <div class="card-body" id="bar-parent">
                                                                 <form  id="FormAddExam" class="form-horizontal" action="../controller/examen.php" method="post">
                                                                     <div class="form-body">
                                                                       <div class="form-group row">
                                                                               <label class="control-label col-md-3"><?php echo L::ExamssMenusingle ?>
                                                                                   <span class="required"> * </span>
                                                                               </label>
                                                                               <div class="col-md-5">
                                                                                   <input type="text" name="examen" id="examen" data-required="1" placeholder="<?php echo L::EnterExamenLibelle ?>" class="form-control " />
                                                                                 </div>
                                                                        </div>
                                                                        <div class="form-group row">
                                                                                <label class="control-label col-md-3"><?php echo L::Period ?>
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-5">

                                                                                  <select class="form-control input-height" id="typesess" name="typesess" style="width:100%" >
                                                                                      <option value=""><?php echo L::SelectedPeriod ?></option>
                                                                                      <?php
                                                                                      $i=1;
                                                                                        foreach ($typesemestre as $value):
                                                                                        ?>
                                                                                        <option value="<?php echo $value->id_semes?>" selected><?php echo utf8_encode(utf8_decode($value->libelle_semes)) ?></option>

                                                                                        <?php
                                                                                                                         $i++;
                                                                                                                         endforeach;
                                                                                                                         ?>

                                                                                  </select>

                                                                               </div>
                                                                            </div>

                                                                        <div class="form-group row">
                                                                          <label class="control-label col-md-3"><?php echo L::DatedebLib ?>
                                                                              <span class="required"> * </span>
                                                                          </label>
                                                                              <div class="col-md-5">
                                                                                  <input type="text"  placeholder="<?php echo L::EnterDatedebLib ?>" name="datedeb" id="datedeb" data-mask="99/99/9999"   value="" class="form-control ">
                                                                                  <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                                                              </div>
                                                                          </div>
                                                                          <div class="form-group row">
                                                                            <label class="control-label col-md-3"><?php echo L::DatefinLib ?>
                                                                                <span class="required"> * </span>
                                                                            </label>
                                                                                <div class="col-md-5">
                                                                                    <input type="text" placeholder="<?php echo L::EnterDatefinLib ?>" name="datefin" id="datefin" value="" data-mask="99/99/9999"  class="form-control ">
                                                                                    <span class="help-block"><?php echo L::Datesymbolesecond ?></span>
                                                                                    <input type="hidden" name="etape" id="etape" value="3"/>
                                                                                    <input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal; ?>"/>
                                                                                    <input type="hidden" name="libellesession" id="libellesession" value="<?php echo $libellesessionencours; ?>">
                                                                                </div>
                                                                            </div>


                                                   <div class="form-actions">
                                                                         <div class="row">
                                                                             <div class="offset-md-3 col-md-9">
                                                                                 <button type="submit" class="btn btn-info"><?php echo L::Saving; ?></button>
                                                                                 <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                                                             </div>
                                                                           </div>
                                                                        </div>
                                                 </div>
                                                                 </form>
                                                             </div>
                                                         </div>
                                                     </div>

                                                 </div>
                                                 <?php
                                               }
                                                 ?>


                                                </div>


                                           </div>
                                       </div>
                                   </div>
                               </div>
          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
            <div class="chat-sidebar">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i
                      class="material-icons">
                      chat</i>Chat
                    <!-- <span class="badge badge-danger">4</span> -->
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <!-- Start User Chat -->
                <!-- <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel"
                  id="quick_sidebar_tab_1"> -->
                  <div class="chat-sidebar-chat "
                    >
                  <div class="chat-sidebar-list">
                    <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd"
                      data-wrapper-class="chat-sidebar-list">
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Online) ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($onlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          if($valueUsers->id_compte!=$_SESSION['user']['IdCompte'])
                          {
                            ?>
                            <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                                width="35" height="35" alt="...">
                              <i class="online dot red"></i>
                              <div class="media-body" onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                                <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                                <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                              </div>
                            </li>
                            <?php
                          }
                          ?>

                          <?php
                        endforeach;
                         ?>

                      </ul>
                      <div class="chat-header">
                        <h5 class="list-heading"><?php echo strtoupper(L::Offline);  ?></h5>
                      </div>
                      <ul class="media-list list-items">
                        <?php

                        foreach ($offlineUsers as  $valueUsers):
                          $tofuser=$valueUsers->photo_compte;
                          if(strlen($tofuser)>0)
                          {
                            $lientofuser="../photo/".$valueUsers->email_compte."/".$valueUsers->photo_compte;
                          }else {
                            $lientofuser="../photo/user5.jpg";
                          }
                          ?>
                          <li class="media"><img class="media-object" src="<?php echo $lientofuser ?>"
                              width="35" height="35" alt="...">
                            <i class="offline dot"></i>
                            <div class="media-body"  onclick="addmessages(<?php echo $_SESSION['user']['IdCompte'] ?>,<?php echo $valueUsers->id_compte ?>)">
                              <h5 class="media-heading"><?php echo $valueUsers->nom_compte." ".$valueUsers->prenom_compte; ?></h5>
                              <div class="media-heading-sub"><?php echo $valueUsers->fonction_compte ?></div>
                            </div>
                          </li>
                          <?php
                        endforeach;
                         ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End User Chat -->
              </div>
            </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 <script src="../assets2/plugins/popper/popper.min.js" ></script>
   <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
   <script src="../assets2/plugins/jquery-validation/js/additional-methods.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
   <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>

   <!-- bootstrap -->
   <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
   <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
   <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
   <!-- Common js-->
 <script src="../assets2/js/app.js" ></script>
   <script src="../assets2/js/pages/validation/form-validation.js" ></script>
   <script src="../assets2/js/layout.js" ></script>
 <script src="../assets2/js/theme-color.js" ></script>
 <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
 <script src="../assets2/js/dropify.js"></script>
 <script src="../assets2/plugins/select2/js/select2.js" ></script>
 <script src="../assets2/js/pages/select2/select2-init.js" ></script>
 <!-- Material -->
 <script src="../assets2/plugins/material/material.min.js"></script>
 <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
 <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
 <script>
 // function SetcodeEtab(codeEtab)
 // {
 //   var etape=3;
 //   $.ajax({
 //     url: '../ajax/sessions.php',
 //     type: 'POST',
 //     async:false,
 //     data: 'etape=' + etape+ '&codeEtab=' +codeEtab,
 //     dataType: 'text',
 //     success: function (content, statut) {
 //
 // window.location.reload();
 //
 //     }
 //   });
 // }
 function addFrench()
 {
   var etape=1;
   var lang="fr";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 function addEnglish()
 {
   var etape=1;
   var lang="en";
   $.ajax({
     url: '../ajax/langue.php',
     type: 'POST',
     async:false,
     data: 'etape=' + etape+ '&lang=' +lang,
     dataType: 'text',
     success: function (content, statut) {

 window.location.reload();

     }
   });
 }

 jQuery(document).ready(function() {




$("#classe").select2();
$("#teatcher").select2();
$("#classeEtab").select2();
$("#typesess").select2();


   $("#FormAddExam").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
   success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
      rules:{

        matiere:"required",
        classe:"required",
        teatcher:"required",
        coef:"required",
        examen:"required",
        datedeb:"required",
        datefin:"required",
        typesess:"required"


      },
      messages: {
        matiere:"<?php echo L::PleaseEnterMatiere ?>",
        classe:"<?php echo L::PleaseSelectclasserequired ?>",
        teatcher:"<?php echo L::SubjectTeaSelectedrequired ?>",
        coef:"<?php echo L::SubjectcoefSelectedrequired ?>",
        examen:"<?php echo L::PleaseEnterExamLib ?>",
        datedeb:"<?php echo L::PleaseEnterParascoActivityDateStart ?>",
        datefin:"<?php echo L::PleaseEnterParascoActivityDateEnd ?>",
        typesess:"<?php echo L::PeriodRequired ?>"

      },
      submitHandler: function(form) {


        var etape=1;

        $.ajax({
          url: '../ajax/examen.php',
          type: 'POST',
          async:true,
          data: 'examen=' + $("#examen").val()+ '&etape=' + etape+'&datedeb='+$("#datedeb").val()+'&datefin='+$("#datefin").val()+'&codeEtab='+$("#codeEtab").val(),
          dataType: 'text',
          success: function (content, statut) {

              if(content==0)
              {
                form.submit();
              }else if(content==1)
              {

                Swal.fire({
                type: 'warning',
                title: '<?php echo L::WarningLib ?>',
                text: "<?php echo L::ExamenAllreadyExiste ?>",

              })
              }

          }
        });


         /*$.ajax({
           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'matiere=' + $("#matiere").val()+ '&etape=' + etape+'&classe='+$("#classe").val()+'&teatcher='+$("#teatcher").val()+'&codeEtab='+$("#codeEtab").val()+'&coefficient='+$("#coef").val(),
           dataType: 'text',
           success: function (content, statut) {


             if(content==0)
             {
               //cette matière n'existe pas encore pour cette classe

               form.submit();

             }else if(content==1)
             {
               //il est question d'un nouveau professeur pour cette matière
               Swal.fire({
 title: '<?php echo L::WarningLib ?>',
 text: "Cette Matière est dejà dispenser par un Professeur",
 type: 'warning',
 showCancelButton: true,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: 'Modifier le Professeur',
 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
 if (result.value) {

//nous allons modifier la valeur de etape pour 4
$("#FormAddSubject #etape").val(4);
$("#FormAddSubject").submit();

 }else {

 }
})

             }else if(content==2)
             {
               // il s'agit du meme professeur pour cette matiere

               Swal.fire({
               type: 'warning',
               title: '<?php echo L::WarningLib ?>',
               text: "Cette Matière existe deja dans le système",

             })
             }

           }
         });*/

             }


           });
      });






 </script>
    <!-- end js include path -->
  </body>

</html>
