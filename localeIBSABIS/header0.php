<div class="page-header navbar navbar-fixed-top">
           <div class="page-header-inner ">
                <!-- logo start -->
                <div class="page-logo">
                    <a href="index.html">
                    <img alt="" src="../assets/img/logo.png">
                    <span class="logo-default" >Sunray</span> </a>
                </div>
                <!-- logo end -->
				<ul class="nav navbar-nav navbar-left in">
					<li><a href="#" class="menu-toggler sidebar-toggler font-size-20"><i class="fa fa-exchange" aria-hidden="true"></i></a></li>
				</ul>
                 <!-- Start Apps Dropdown -->
                 <ul class="nav navbar-nav navbar-left in">
				 	<li class="dropdown dropdown-extended dropdown-notification" >
                            <a href="javascript:;" class="dropdown-toggle app-list-icon font-size-20" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="fa fa-th" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu app-icon">
                            	<li class="app-dropdown-header">
                                    <p><span class="bold">Applications</span></p>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list app-icon-dropdown" data-handle-color="#637283">
										<li>
											<a href="add_patient.html" class="patient-icon">
											<i class="material-icons">local_hotel</i>
											<span class="block">Add Patient</span>
											</a>
										</li>
										<li>
											<a href="email_inbox.html" class="email-icon">
											<i class="material-icons">drafts</i>
											<span class="block">Email</span>
											</a>
										</li>
										<li>
											<a href="view_appointment.html" class="appoint-icon">
											<i class="material-icons">assignment</i>
											<span class="block">Appointment</span>
											</a>
										</li>
										<li>
											<a href="all_doctors.html" class="doctor-icon">
											<i class="material-icons">people</i>
											<span class="block">Doctors</span>
											</a>
										</li>
										<li>
											<a href="google_maps.html" class="map-icon">
											<i class="material-icons">map</i>
											<span class="block">Map</span>
											</a>
										</li>
										<li>
											<a href="payments.html" class="payment-icon">
											<i class="material-icons">monetization_on</i>
											<span class="block">Payments</span>
											</a>
										</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                 </ul>
                 <!-- End Apps Dropdown -->
                <ul class="nav navbar-nav navbar-left in">
                	<!-- start full screen button -->
                    <li><a href="javascript:;" class="fullscreen-click font-size-20"><i class="fa fa-arrows-alt"></i></a></li>
                    <!-- end full screen button -->
                </ul>
                <!-- start mobile menu -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
               <!-- end mobile menu -->
                <!-- start header menu -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <!-- start notification dropdown -->
                        <li class="dropdown dropdown-extended dropdown-notification" >
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="material-icons">notifications</i>
                                <span class="notify"></span>
                                <span class="heartbeat"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3><span class="bold">Notifications</span></h3>
                                    <span class="notification-label purple-bgcolor">New 6</span>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list small-slimscroll-style" data-handle-color="#637283">
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">just now</span>
                                                <span class="details">
                                                <span class="notification-icon circle deepPink-bgcolor"><i class="fa fa-check"></i></span> Congratulations!. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 mins</span>
                                                <span class="details">
                                                <span class="notification-icon circle purple-bgcolor"><i class="fa fa-user o"></i></span>
                                                <b>John Micle </b>is now following you. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">7 mins</span>
                                                <span class="details">
                                                <span class="notification-icon circle blue-bgcolor"><i class="fa fa-comments-o"></i></span>
                                                <b>Sneha Jogi </b>sent you a message. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">12 mins</span>
                                                <span class="details">
                                                <span class="notification-icon circle pink"><i class="fa fa-heart"></i></span>
                                                <b>Ravi Patel </b>like your photo. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">15 mins</span>
                                                <span class="details">
                                                <span class="notification-icon circle yellow"><i class="fa fa-warning"></i></span> Warning! </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">10 hrs</span>
                                                <span class="details">
                                                <span class="notification-icon circle red"><i class="fa fa-times"></i></span> Application error. </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="dropdown-menu-footer">
                                        <a href="javascript:void(0)"> All notifications </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- end notification dropdown -->
                        <!-- start message dropdown -->
 						<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="material-icons">question_answer</i>
                                <span class="notify"></span>
                                <span class="heartbeat"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3><span class="bold">Messages</span></h3>
                                    <span class="notification-label cyan-bgcolor">New 2</span>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list small-slimscroll-style" data-handle-color="#637283">
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                	<img src="../assets/img/doc/doc2.jpg" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                	<span class="from"> Sarah Smith </span>
                                                	<span class="time">Just Now </span>
                                                </span>
                                                <span class="message"> Jatin I found you on LinkedIn... </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                	<img src="../assets/img/doc/doc3.jpg" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                	<span class="from"> John Deo </span>
                                                	<span class="time">16 mins </span>
                                                </span>
                                                <span class="message"> Fwd: Important Notice Regarding Your Domain Name... </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                	<img src="../assets/img/doc/doc1.jpg" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                	<span class="from"> Rajesh </span>
                                                	<span class="time">2 hrs </span>
                                                </span>
                                                <span class="message"> pls take a print of attachments. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                	<img src="../assets/img/doc/doc8.jpg" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                	<span class="from"> Lina Smith </span>
                                                	<span class="time">40 mins </span>
                                                </span>
                                                <span class="message"> Apply for Ortho Surgeon </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                	<img src="../assets/img/doc/doc5.jpg" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                	<span class="from"> Jacob Ryan </span>
                                                	<span class="time">46 mins </span>
                                                </span>
                                                <span class="message"> Request for leave application. </span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="dropdown-menu-footer">
                                        <a href="#"> All Messages </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- end message dropdown -->
 						<!-- start manage user dropdown -->
 						<li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle " src="../assets/img/dp.jpg" />
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="user_profile.html">
                                        <i class="fa fa-user"></i><?php echo echo L::ProfileLib ?>  </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-cogs"></i> Settings
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-question-circle"></i> Help
                                    </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="lock_screen.html">
                                        <i class="fa fa-lock"></i> Lock
                                    </a>
                                </li>
                                <li>
                                    <a href="login.html">
                                        <i class="fa fa-sign-out"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end manage user dropdown -->
                        <li class="dropdown dropdown-quick-sidebar-toggler">
                             <a id="headerSettingButton" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton">
	                           <i class="material-icons">settings</i>
	                        </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="navbar-custom">
				<div class="hor-menu hidden-sm hidden-xs">
                    <ul class="nav navbar-nav">
                        <li class="mega-menu-dropdown ">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">dashboard</i>  Dashboard
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                    <li>
                                                        <a href="index.html" class="nav-link "> <span class="title">Dashboard 1</span></a>
                                                    </li>
                                                   <li>
                                                        <a href="dashboard2.html" class="nav-link "> <span class="title">Dashboard 2</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="classic-menu-dropdown mega-menu-dropdown">
                                <a href="#" class=" megamenu-dropdown" data-close-others="true"> <i class="material-icons">assignment</i>  Manage
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu pull-left">

                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-address-card-o"></i> Appointments</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="schedule.html" class="nav-link "> <span class="title">Doctor Schedule</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="book_appointment.html" class="nav-link "> <span class="title">Book Appointment</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="book_appointment_material.html" class="nav-link "> <span class="title">Book Appointment Material</span>
		                                    </a>
		                                </li>
		                                 <li class="nav-item  ">
		                                    <a href="edit_appointment.html" class="nav-link "> <span class="title">Edit Appointment</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="view_appointment.html" class="nav-link "> <span class="title">View All Appointment</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-user-md" aria-hidden="true"></i> Doctors</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="all_doctors.html" class="nav-link "> <span class="title">All Doctor</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_doctor.html" class="nav-link "> <span class="title">Add Doctor</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_doctor_material.html" class="nav-link "> <span class="title">Add Doctor Material</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="edit_doctor.html" class="nav-link "> <span class="title">Edit Doctor</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="doctor_profile.html" class="nav-link "> <span class="title">About Doctor</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-users" aria-hidden="true"></i>Other Staff</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="all_staffs.html" class="nav-link "> <span class="title">All Staff</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_staff.html" class="nav-link "> <span class="title">Add Staff</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_staff_material.html" class="nav-link "> <span class="title">Add Staff Material</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="edit_staff.html" class="nav-link "> <span class="title">Edit Staff</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="staff_profile.html" class="nav-link "> <span class="title">Staff Profile</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-wheelchair" aria-hidden="true"></i>Patients</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="all_patients.html" class="nav-link "> <span class="title">All Patients</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_patient.html" class="nav-link "> <span class="title">Add Patient</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_patient_material.html" class="nav-link "> <span class="title">Add Patient Material</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="edit_patient.html" class="nav-link "> <span class="title">Edit Patient</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="patient_profile.html" class="nav-link "> <span class="title">Patient Profile</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-hotel" aria-hidden="true"></i>Room Allotment</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="room_allotment.html" class="nav-link "> <span class="title">Alloted Rooms</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_room_allotment.html" class="nav-link "> <span class="title">New Allotment</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="add_room_allotment_material.html" class="nav-link "> <span class="title">New Allotment Material</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="edit_room_allotment.html" class="nav-link "> <span class="title">Edit Allotment</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown ">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">email</i>  Email
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                    <li>
                                                        <a href="email_inbox.html" class="nav-link "> <span class="title">Inbox</span></a>
                                                    </li>
                                                   <li>
                                                        <a href="email_view.html" class="nav-link "> <span class="title">View Email</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="email_compose.html" class="nav-link "><span class="title">Compose Mail</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="mega-menu-dropdown mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">dvr</i>UI Elements
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu pull-left">

                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-briefcase"></i> Bootstrap Elements</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
		                                    <a href="ui_buttons.html" class="nav-link ">
		                                        <span class="title">Buttons</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_tabs_accordions_navs.html" class="nav-link ">
		                                        <span class="title">Tabs &amp; Accordions</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_typography.html" class="nav-link ">
		                                        <span class="title">Typography</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_icons.html" class="nav-link ">
		                                        <span class="title">Icons</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_panels.html" class="nav-link ">
		                                        <span class="title">Panels</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_grid.html" class="nav-link ">
		                                        <span class="title">Grids</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="calendar.html" class="nav-link ">
		                                        <span class="title">Calender</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_tree.html" class="nav-link ">
		                                        <span class="title">Tree View</span>
		                                    </a>
		                                </li>
		                                <li class="nav-item  ">
		                                    <a href="ui_carousel.html" class="nav-link ">
		                                        <span class="title">Carousel</span>
		                                    </a>
		                                </li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="javascript:;">
                                        <i class="fa fa-google"></i> Material Elements</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item  ">
	                                    <a href="material_button.html" class="nav-link ">
	                                        <span class="title">Buttons</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_tab.html" class="nav-link ">
	                                        <span class="title">Tabs</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_chips.html" class="nav-link ">
	                                        <span class="title">Chips</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_grid.html" class="nav-link ">
	                                        <span class="title">Grid</span>
	                                    </a>
	                                </li>

	                                <li class="nav-item  ">
	                                    <a href="material_form.html" class="nav-link ">
	                                        <span class="title">Form</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_datepicker.html" class="nav-link ">
	                                        <span class="title">DatePicker</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_select.html" class="nav-link ">
	                                        <span class="title">Select Item</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_loading.html" class="nav-link ">
	                                        <span class="title">Loading</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_menu.html" class="nav-link ">
	                                        <span class="title">Menu</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_slider.html" class="nav-link ">
	                                        <span class="title">Slider</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_tables.html" class="nav-link ">
	                                        <span class="title">Tables</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_toggle.html" class="nav-link ">
	                                        <span class="title">Toggle</span>
	                                    </a>
	                                </li>
	                                <li class="nav-item  ">
	                                    <a href="material_badges.html" class="nav-link ">
	                                        <span class="title">Badges</span>
	                                    </a>
	                                </li>
                                    </ul>
                                </li>
                              </ul>
                        </li>
                        <li class="mega-menu-dropdown ">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">monetization_on</i>Payments
                                <i class="fa fa-angle-down"></i>
                                <span class="arrow "></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  ">
					                                    <a href="payments.html" class="nav-link "> <span class="title">Payments</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="add_payment.html" class="nav-link "> <span class="title">Add Payment</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="invoice_payment.html" class="nav-link "> <span class="title">Payment Invoice</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown active open">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">desktop_mac</i>Layout
                                <i class="fa fa-angle-down"></i>
                                <span class="selected"></span>
                                	<span class="arrow open"></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  ">
					                                    <a href="layout_boxed.html" class="nav-link "> <span class="title">Boxed</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item active open ">
					                                    <a href="layout_full_width.html" class="nav-link "> <span class="title">Full Width</span>
					                                    <span class="selected"></span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="layout_right_sidebar.html" class="nav-link "> <span class="title">Right Sidebar</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="layout_collapse.html" class="nav-link "> <span class="title">Collapse Menu</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="layout_sidebar_hover_menu.html" class="nav-link "> <span class="title">Hover Sidebar Menu</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="layout_mega_menu.html" class="nav-link "> <span class="title">Mega Menu</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">subtitles</i>Forms
                                <i class="fa fa-angle-down"></i>
                                	<span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  ">
					                                    <a href="layouts_form.html" class="nav-link ">
					                                        <span class="title">Form Layout</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="advance_form.html" class="nav-link ">
					                                        <span class="title">Advance Component</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="wizard_form.html" class="nav-link ">
					                                        <span class="title">Form Wizard</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="validation_form.html" class="nav-link ">
					                                        <span class="title">Form Validation</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="editable_form.html" class="nav-link ">
					                                        <span class="title">Editor</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">list</i>Data Tables
                                <i class="fa fa-angle-down"></i>
                                	<span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  ">
					                                    <a href="basic_table.html" class="nav-link ">
					                                        <span class="title">Basic Tables</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="advanced_table.html" class="nav-link ">
					                                        <span class="title">Advance Tables</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="group_table.html" class="nav-link ">
					                                        <span class="title">Grouping</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="editable_table.html" class="nav-link ">
					                                        <span class="title">Editable Table</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="tableData.html" class="nav-link ">
					                                        <span class="title">Tables With Sourced Data</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">timeline</i>Charts
                                <i class="fa fa-angle-down"></i>
                                	<span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  ">
					                                    <a href="charts_echarts.html" class="nav-link ">
					                                        <span class="title">eCharts</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="charts_morris.html" class="nav-link ">
					                                        <span class="title">Morris Charts</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="charts_chartjs.html" class="nav-link ">
					                                        <span class="title">Chartjs</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="mega-menu-dropdown">
                            <a href="#" class="dropdown-toggle"> <i class="material-icons">description</i>Extra Pages
                                <i class="fa fa-angle-down"></i>
                                	<span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu" style="min-width: 200px;">
                                <li>
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="mega-menu-submenu">
                                                   <li class="nav-item  "><a href="user_profile.html" class="nav-link "><span class="title">Profile</span>
													</a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="contact.html" class="nav-link "> <span class="title">Contact Us</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="login.html" class="nav-link "> <span class="title">Login</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="sign_up.html" class="nav-link "> <span class="title">Sign Up</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="forgot_password.html" class="nav-link "> <span class="title">Forgot Password</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="lock_screen.html" class="nav-link "> <span class="title">Lock Screen</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="page-404.html" class="nav-link "> <span class="title">404 Page</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item  ">
					                                    <a href="page-500.html" class="nav-link "> <span class="title">500 Page</span>
					                                    </a>
					                                </li>
					                                <li class="nav-item">
					                                    <a href="blank_page.html" class="nav-link "> <span class="title">Blank Page</span>
					                                    </a>
					                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
			</div>
        </div>
