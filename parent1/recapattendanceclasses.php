<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";
}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$student=new Student();
$localadmins= new Localadmin();
$parents=new ParentX();
$classe=new Classe();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
// $classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);
// $nbclasse=$classe->getNumberClassesOfTeatcherId($_SESSION['user']['IdCompte']);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

if(isset($_POST['codeEtab']))
{
  $codeEtab=$_POST['codeEtab'];
}
if(isset($_GET['codeEtab']))
{
$codeEtab=$_GET['codeEtab'];
}

if(isset($_POST['classe']))
{
  $classeEtab=$_POST['classe'];
}
if(isset($_GET['classe']))
{
$classeEtab=$_GET['classe'];
}

if(isset($_POST['idcompte']))
{
  $idcompte=$_POST['idcompte'];
}
if(isset($_GET['idcompte']))
{
$idcompte=$_GET['idcompte'];
}

// $alletab=$etabs->getAllEtab();
// $locals=$localadmins->getAllAdminLocal();
// $allparents=$parents->getAllParent();

//retrouver les classes ou le parent à au moins un enfant

$classesOfStudent=$parents->getAllclassesOfStudentParent($_SESSION['user']['IdCompte']);



$etabofStudent=$parents->getAlletabOfStudentParent($_SESSION['user']['IdCompte']);

// var_dump($etabofStudent);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Récap des Présences - Classe :</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.html">Présences</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Récap présences</li>
                          </ol>
                      </div>
                  </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->

					        <!-- /.col -->
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>


                 <div class="row">
                   <div class="col-md-12 col-sm-12">
                                             <div class="card card-box">
                                                 <div class="card-head">
                                                     <header>Récapitulatif des Présences</header>

                                                 </div>
                                                 <div class="card-body " id="bar-parent">
                                                   <form method="post" id="FormTeatcherSearch" name="recapattendanceclasses.php">
                                                       <div class="row">

                                                     <div class="col-md-6 col-sm-6">
                                                     <!-- text input -->

                                                     <div class="form-group" style="margin-top:8px;">
                                                         <label>Mois</label>
                                                         <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                                         <select class="form-control input-height" id="month" name="month" style="width:100%;">
                                                             <option selected value="">Selectionner un mois</option>
                                                             <option  value="JANVIER-1">JANVIER</option>
                                                             <option  value="FEVRIER-2">FEVRIER</option>
                                                             <option  value="MARS-3">MARS</option>
                                                             <option  value="AVRIL-4">AVRIL</option>
                                                             <option  value="MAI-5">MAI</option>
                                                             <option  value="JUIN-6">JUIN</option>
                                                             <option  value="JUILLET-7">JUILLET</option>
                                                             <option  value="AOUT-8">AOUT</option>
                                                             <option  value="SEPTEMBRE-9">SEPTEMBRE</option>
                                                             <option  value="OCTOBRE-10">OCTOBRE</option>
                                                             <option  value="NOVEMBRE-11">NOVEMBRE</option>
                                                             <option  value="DECEMBRE-12">DECEMBRE</option>


                                                         </select>

                                                         <input type="hidden" name="search" id="search"/>
                                                         <input type="hidden" name="codeEtab" id="codeEtab"  value="<?php echo $codeEtab; ?>">
                                                         <input type="hidden" name="idcompte" id="idcompte" value="<?php echo $idcompte; ?>">
                                                         <input type="hidden" name="classeEtab" id="classeEtab" value="<?php echo $classeEtab; ?>">
                                                     </div>


                                                 </div>
                                                 <div class="col-md-6 col-sm-6">
                                                 <!-- text input -->
                                                 <div class="form-group" style="margin-top:8px;">
                                                     <label>Année de session</label>
                                                     <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                                                     <select class="form-control input-height" id="yearsession" name="yearsession" style="width:100%;">
                                                         <option selected value="">Selectionner une année</option>
                                                         <option  value="<?php echo date('Y');?>"><?php echo date('Y');?></option>
                                                         <option  value="<?php echo date("Y")+1;?>"><?php echo date("Y")+1;?></option>



                                                     </select>
                                                 </div>




                                             </div>
                                             <div class="col-md-6 col-sm-6">

                                                  <div class="form-group" style="margin-top:35px;">
                                                      <button type="submit" class="btn btn-success"  style=""><?php echo L::AfficherleRecap ?></button>
                                                  </div>
                                             </div>
                                                       </div>

                                                   </form>
                                                 </div>
                                             </div>
                                         </div>
                 </div>
                 <div class="row">

                   <?php
                   if(isset($_POST['search']))
                   {
                     if(isset($_POST['codeEtab'])&&isset($_POST['classeEtab'])&&isset($_POST['month'])&&isset($_POST['yearsession']))
                     {
                         //nous devons recupérer la liste des jours du mois selectionner

                          //$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);

                          $moisconcat=$_POST['month'];
                          $annee=$_POST['yearsession'];
                          $tabmoisconcat=explode("-",$moisconcat);
                          $mois=$tabmoisconcat[0];
                          $numbermois=$tabmoisconcat[1];
                          $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);

                          $concat=$annee."-".regiveMois($numbermois)."-";

                          $infosclasses=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

                          //nous allons chercher la liste des eleves de cette classe

                          if($_POST['idcompte']&& strlen($_POST['idcompte'])>0)
                          {
                            $students=$parents->getstudentofthisclassesSchoolParent($_POST['classeEtab'],$_POST['codeEtab'],$_POST['idcompte'],$_SESSION['user']['IdCompte']);
                          }else {
                            $students=$parents->getAllstudentofthisclassesParentSchool($_POST['classeEtab'],$_POST['codeEtab'],$_SESSION['user']['IdCompte']);
                          }


                          //echo $num;
                         ///var_dump($students);

                     }
                     ?>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  id="affichage2">
                                             <div class="card card-topline-green">
                                                 <div class="card-head">
                                                     <header>Présences du mois  <?php echo @$mois." ".$annee?></header>
                                                     <div class="tools">
                                                         <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                 <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                 <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                                     </div>
                                                 </div>
                                                 <div class="card-body ">


                                             <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                                                 <thead>
                                                     <tr>
                                                         <!--th style="width:15%;height:10px;padding:3%;margin:15%"-->
                                                           <th style="padding:50px;;">
                                                             # JOURS
                                                         </th>
                                                         <?php
                                                           for($x=1;$x<=$num;$x++)
                                                           {
                                                           ?>
                                                           <th style="">
                                                               <?php echo $x; ?>
                                                           </th>
                                                         <?php
                                                           }
                                                         ?>


                                                     </tr>
                                                 </thead>
                                                 <tbody>
                                                   <?php
                                                   $i=1;

                                                   foreach ($students as $value):
                                                    ?>
                                                   <tr>
                                                     <td style=""><?php  echo $value->nom_eleve." ".$value->prenom_eleve?></td>
                                                     <?php
                                                       for($x=1;$x<=$num;$x++)
                                                       {
                                                       ?>
                                                       <td style="">
                                                           <?php
                                                             if(strlen($x)==1)
                                                             {
                                                               $x="0".$x;
                                                             }
                                                            $data=$concat.$x;
                                                           $matricule=$value->matricule_eleve;
                                                            $nombre=$student->getNbAttendance($matricule,$data,$_POST['classeEtab']);

                                                             if($nombre==0)
                                                             {
                                                               echo $nombre;
                                                             }else {
                                                               $number=$student->getstatutAttendance($matricule,$data,$_POST['classeEtab']);
                                                               $array=json_encode($number,true);
                                                               $someArray = json_decode($array, true);
                                                               echo $someArray[0]["statut_presence"];
                                                               //var_dump($number[0]['statut_presence']);
                                                             }

                                                             ?>
                                                       </td>
                                                     <?php
                                                       }
                                                     ?>
                                                  </tr>
                                                  <?php
                                                  $i++;

                                                      endforeach;
                                                   ?>

                                                 </tbody>
                                             </table>



                                         </div>
                                             </div>
                                         </div>
                     <?php


                   }

                   ?>



                 </div>


                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- counterup -->
    <script src="../assets2/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets2/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- Common js-->
	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
    <script src="../assets2/js/theme-color.js" ></script>
    <!-- material -->
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>

    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   $("#codeEtab").select2();
   $("#classeEtab").select2();
   $("#idcompte").select2();
   $("#month").select2();
   $("#yearsession").select2();

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function searchclasseandteatcher()
   {
     var etape=5;
     var parentid="<?php echo $_SESSION['user']['IdCompte'];?>";
     $.ajax({
       url: '../ajax/classe.php',
       type: 'POST',
       async:false,
       data: 'code='+ $("#codeEtab").val()+'&etape='+etape+'&parentid='+parentid,
       dataType: 'text',
       success: function (content, statut) {

         $("#classeEtab").html("");
         $("#classeEtab").html(content);

       }
     });
   }
   function searchstudent()
   {
     //rechercher la liste des eleves dont le parent est cet utilisateur et se trouvant dans cette clase
     var parentid="<?php echo $_SESSION['user']['IdCompte'];?>";
     var codeEtab=$("#codeEtab").val();
     var classeEtab=$("#classeEtab").val();
      var etape=6;
      $.ajax({
        url: '../ajax/classe.php',
        type: 'POST',
        async:false,
        data: 'code='+ codeEtab+'&etape='+etape+'&parentid='+parentid+'&classeEtab='+classeEtab,
        dataType: 'text',
        success: function (content, statut) {

          // alert(content);
          $("#idcompte").html("");
          $("#idcompte").html(content);

        }
      });


   }
   $(document).ready(function() {

$("#FormTeatcherSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
},
highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
},
success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{
    codeEtab:"required",
    classeEtab:"required",
    month:"required",
    yearsession:"required"
  },
  messages: {
    codeEtab:"Merci de selectioner un Etablissement",
    classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selectionner une année"
  }
});

   });

   </script>
    <!-- end js include path -->
  </body>

</html>
