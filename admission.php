<?php
session_start();
require_once('../class/Student.php');
require_once('../class/Classe.php');
$student= new Student();
$classeSchool= new Classe();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //nous allons verifier si cet eleve existe deja dans la base de données

  //recupération des variables

  $content="";
  $matricule=htmlspecialchars(addslashes($_POST['matricule']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $parent=htmlspecialchars(addslashes($_POST['parent']));

  $years=date('Y');
  $nextyears=date("Y")+1;
  $session=$years."-".$nextyears;

  //verifier si cet elève existe dans la base

  //$check=$student->ExisteStudent($matricule,$classe,$parent,$session);
  $check=$student->ExisteStudent($matricule);

  if($check==1)
  {
    //cet eleve est deja présent dans la base de données
    //nous allons verifier si celui ci est inscrit dans cette classe
    //$content=1;

    $check1=$student->InscriptionAllReady($matricule,$classe,$parent,$session);

    if($check1==1)
    {
      $content=1;
    }else {
      //cet eleve est deja dans la base mais n'est pas encore inscrit pour l'année encours
      $content=2;
    }

  }else {

    $content=0;

  }


}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  // $idcompte=htmlspecialchars(addslashes($_POST['compte']));
  // $parent=htmlspecialchars(addslashes($_POST['codeEtab']));
  //
  // $lien=$student->generateficheLocalpdf($compte,$codeEtab);
  //
  // echo $lien;
}if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $classe=htmlspecialchars(addslashes($_POST['classe']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  $content="";


  $datastudent=$student->DetermineAllstudentofthisclassesSessionT($classe,$codeEtab,$session);

  $nbligne=count($datastudent);

  if($nbligne==0)
  {
    $content.="<option value=''>Aucun Elève </option>";
  }else {
    //var_dump($datas);
    $content.="<option value='' selected>Selectionner un Elève</option>";

    foreach ($datastudent as $value):
        $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
    endforeach;
  }

  echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==4))
{
// recuperation des variables

$codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
$classe=htmlspecialchars(addslashes($_POST['classe']));
$session=htmlspecialchars(addslashes($_POST['session']));
$studentid=htmlspecialchars(addslashes($_POST['student']));
$content="";


//retrouvons le montant à payer

//nous allons compter le nombre de versement effectué pour ce elève au cours de cette année academique

  $nbversement=$student->DetermineNumberOfversement($codeEtab,$classe,$session,$studentid);

  if($nbversement==0)
  {
    //le parent n'a pas encore fait de paiement alors nous allons recuperer le montant de la scolarite pour cette section
    $datavers=$classeSchool->getAllInformationsOfclassesByClasseId($classe,$session);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[7]."-".$tabdatavers[6];
  }else if($nbversement>0)
  {
    //nous avons au moins un versement alors nous allons chercher les informations du dernier versement

    $datavers=$student->SelectInformationsOfLastVersement($codeEtab,$classe,$session,$studentid);
    $tabdatavers=explode("*",$datavers);
    $content=$tabdatavers[0]."-".$tabdatavers[1];
  }

echo $content;

}if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recupération des variables

  $montantapayer=htmlspecialchars(addslashes($_POST['montantapayer']));
  $montantpayer=htmlspecialchars(addslashes($_POST['montantpayer']));

  $restepaie=$montantapayer-$montantpayer;

  echo $restepaie;
}if(isset($_POST['etape'])&&($_POST['etape']==6))
{
    //recuperation des variables
    $classe=htmlspecialchars(addslashes($_POST['classe']));
    $session=htmlspecialchars(addslashes($_POST['session']));
    $content="";

    //nous allons determiner la liste des eleves des classes selectionnées

    $datas=$student->getAllstudentofMulticlassesSession($classe,$session);

    $nbligne=count($datas);

    if($nbligne==0)
    {
      $content.="<option value=''>Aucun Elève </option>";
    }else {
      //var_dump($datas);
      $content.="<option value=''>Selectionner un Elève</option>";

      foreach ($datas as $value):
          $content .= "<option value='".$value->id_compte."' >" . utf8_encode(utf8_decode($value->nom_compte." - ".$value->prenom_compte)). "</option>";
      endforeach;
    }

    echo $content;

    // $tabclasse=explode(",",$classe);
    //
    // $nbtable=count($tabclasse);
    //
    // if($nbtable==1)
    // {
    //   echo "egale a 0";
    // }else if($nbtable>1)
    // {
    //   echo "superieur à 1";
    // }



}


 ?>
