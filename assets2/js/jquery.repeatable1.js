(function ($) {

	$.fn.repeatable = function (userSettings1) {

		/**
		 * Default settings
		 * @type {Object}
		 */
		var defaults1 = {
			addTrigger: "addcontenu",
			deleteTrigger: "deletecontenu",
			max: 5,
			min: 1,
			template: null,
			itemContainer: ".field-group",
			beforeAdd: function () {},
			afterAdd: function (item) {},
			beforeDelete: function (item) {},
			afterDelete: function () {}
		};



		/**
		 * Iterator used to make each added
		 * repeatable element unique
		 * @type {Number}
		 */
		var i = 1;

		/**
		 * DOM element into which repeatable
		 * items will be added
		 * @type {jQuery object}
		 */
		var target = $(this);

		/**
		 * Blend passed user settings with defauly settings
		 * @type {array}
		 */
		var settings1 = $.extend({}, defaults1, userSettings1);



		/**
		 * Total templated items found on the page
		 * at load. These may be created by server-side
		 * scripts.
		 * @return null
		 */
		var total1 = function () {
			return $(target).find(settings1.itemContainer).length;
		}();




		/**
		 * Add an element to the target
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var addOnecontenu = function (e) {
			var nb=$("#nbcontenu").val();
			var nouveau= parseInt(nb)+1;

			$("#nbcontenu").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			settings1.beforeAdd.call(this);
			var item = createOnecontenu();
			settings1.afterAdd.call(this, item);
		};



		/**
		 * Delete the parent element
		 * and call the callback function
		 * @param  object e Event
		 * @return null
		 */
		var deleteOnecontenu = function (e) {

			var nb=$("#nbcontenu").val();
			var nouveau= parseInt(nb)-1;

			$("#nbcontenu").val(nouveau);

			// alert(nouveau);
			e.preventDefault();
			if (total1 === settings1.min) return;
			var item = $(this).parents(settings1.itemContainer).first();
			settings1.beforeDelete.call(this, item);
			item.remove();
			total1--;
			maintainAddBtn1();
			settings1.afterDelete.call(this);
		};



		/**
		 * Add an element to the target
		 * @return null
		 */
		var createOnecontenu = function() {
			var item = getUniqueTemplate1();
			item.appendTo(target);
			total1++;
			maintainAddBtn1();
			return item;
		};



		/**
		 * Alter the given template to make
		 * each form field name unique
		 * @return {jQuery object}
		 */
		var getUniqueTemplate1 = function () {
			var template1 = $(settings1.template).html();
			// template = template.replace(/{\?}/g, "new" + i++); 	// {?} => iterated placeholder
			template1 = template1.replace(/{\?}/g, i++);
			template1 = template1.replace(/\{[^\?\}]*\}/g, ""); 	// {valuePlaceholder} => ""
			return $(template1);
		};



		/**
		 * Determines if the add trigger
		 * needs to be disabled
		 * @return null
		 */
		var maintainAddBtn1 = function () {
			if (!settings1.max) {
				return;
			}

			if (total1 === settings1.max) {
				$(settings1.addTrigger).attr("disabled", "disabled");
			} else if (total1 < settings1.max) {
				$(settings1.addTrigger).removeAttr("disabled");
			}
		};



		/**
		 * Setup the repeater
		 * @return null
		 */
		(function () {
			$(settings1.addTrigger).on("click", addOnecontenu);
			$("form").on("click", settings1.deleteTrigger, deleteOnecontenu);

			if (!total1) {
				var toCreate = settings1.min - total1;
				for (var j = 0; j < toCreate; j++) {
					createOnecontenu();
				}
			}

		})();




	};





})(jQuery);
