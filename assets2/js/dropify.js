(function($) {
  'use strict';
  $('.dropify').dropify();

  $("#photoparent").dropify({
    messages: {
        "default": "Merci de selectionner la photo de profile",
        "replace": "changer l'image",
        "remove" : "Retirer",
        "error"  : "Erreur"
    }
  });
  $("#logoetab").dropify({
    messages: {
        "default": "Merci de selectionner le logo",
        "replace": "changer l'image",
        "remove" : "Retirer",
        "error"  : "Erreur"
    }
  });
  $("#photoad").dropify({
    messages: {
        "default": "Merci de selectionner la photo de profile",
        "replace": "changer l'image",
        "remove" : "Retirer",
        "error"  : "Erreur"
    }
  });
  $("#photoparent").dropify({
    messages: {
        "default": "Merci de selectionner la photo de profile",
        "replace": "changer l'image",
        "remove" : "Retirer",
        "error"  : "Erreur"
    }
  });
  $("#fichierdiplo").dropify({
    messages: {
        "default": "Merci de selectionner le fichier diplôme",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    }
  });

  $("#fichier").dropify({
    messages: {
        "default": "Merci de selectionner le fichier diplôme",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    }
  });

  $("#fichier1").dropify({
    messages: {
        "default": "Merci de selectionner le fichier diplôme",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    },
    error: {
    'fileSize': 'La taille du fichier est trop grande ({{ value }} max).',
    'minWidth': 'La largeur de l\'image est trop petite ({{ value }}}px min).',
    'maxWidth': 'La largeur de l\'image est trop grande ({{ value }}}px max).',
    'minHeight': 'La hauteur de l\'image est trop faible ({{ value }}}px min).',
    'maxHeight': 'La hauteur de l \'image est trop grande ({{ value }}px max).',
    'imageFormat': 'Le format d\'image n\'est pas autorisé ({{ value }} seulement).',
    'fileFormat': 'Le format de fichier n\'est pas autorisé ({{ value }} seulement).'
    }
  });

  $("#fichierExt").dropify({
    messages: {
        "default": "Merci de selectionner l'extrait de naissance de l'enfant",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    }
  });

  $("#carnet").dropify({
    messages: {
        "default": "Merci de selectionner les pages du carnet de vaccination",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    }
  });

  $("#contrat").dropify({
    messages: {
        "default": "Merci de selectionner le contrat parental",
        "replace": "changer le fichier",
        "remove" : "Retirer le fichier",
        "error"  : "Erreur"
    }
  });


  $("#photoTea").dropify({
    messages: {
        "default": "Merci de selectionner la photo de profile",
        "replace": "changer l'image",
        "remove" : "Retirer",
        "error"  : "Erreur"
    }
  });


})(jQuery);
