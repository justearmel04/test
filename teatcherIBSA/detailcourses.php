<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Teatcher.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');
require_once('../intl/i18n.class.php');

if(!isset($_SESSION['user']['lang']))
{
  $_SESSION['user']['lang']="fr";

}

$i18n = new i18n();
$i18n->setCachePath('../langcache');
$i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
$i18n->setFallbackLang($_SESSION['user']['lang']);
$i18n->setPrefix('L');
$i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
$i18n->setSectionSeperator('_');
$i18n->setMergeFallback(false);
$i18n->init();


$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$matiere=new Matiere();
$student=new Student();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getClassesOfTeatcherId($_SESSION['user']['IdCompte']);

$matieres=$matiere->getAllSubjectTeatchByTeatcherId($_SESSION['user']['IdCompte']);
$controles=$matiere->getAllControleMatiereOfThisTeatcherId($_SESSION['user']['IdCompte']);


//$dataclasses=$classe->getAllClassesByClasseId($classeschoolid);

$codeEtabsession=$teatcher->getTeatcherEtabCode($_SESSION['user']['IdCompte']);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbclasse=$classe->getClassesNumberOfTeatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);
  $programmes=$etabs->getAllprogrammesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

  // $fiches=$etabs->getAllFicesOfTeatcherClasses($_SESSION['user']['IdCompte'],$libellesessionencours);

$fiches=$etabs->getAllcahiersOfteatcherId($_SESSION['user']['IdCompte'],$libellesessionencours);

}else {
  $nbclasse=0;
}

//la liste des cours de ce professeur

$courseid=$_GET['course'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$datacoursescomments=$etabs->getAllcoursescommentsTea($courseid,$classeid,$codeEtabsession,$libellesessionencours);
$nbcomments=count($datacoursescomments);

$datacoursescommentonlines=$etabs->getAllcoursescommentonlinesTea($courseid,$classeid,$codeEtabsession,$libellesessionencours);
$nbonlines=count($datacoursescommentonlines);

// echo $nbonlines;


foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->descri_courses;
  $durationcourses=$datacourses->duree_courses;
  $teatchercourses=$datacourses->nom_compte;
  $datercourses=$datacourses->date_courses;
  $namecourses=$datacourses->libelle_courses;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;
  $statutcourses=$datacourses->statut_courses;
  $filescourses=$datacourses->support_courses;
  $discussion_courses=$datacourses->discussion_courses;
  $teatcherid_courses=$datacourses->teatcher_courses;

endforeach;



// Nous allons recuperer les sections du cours

$coursesSections=$etabs->getAllcoursesSection($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$coursescompetences=$etabs->getAllcoursesComp($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$courseshomeworks=$etabs->getAllcoursesHomeworks($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

$coursesupports=$etabs->getAllsuppourts($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);
// var_dump($coursescompetences);
$nbnotificationstandby=$parents->getParentnotificationstandbyNb($_SESSION['user']['IdCompte']);
$lastnotificationstandby=$parents->getParentnotificationstandbyLast($_SESSION['user']['IdCompte']);
 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
	<!--bootstrap -->
  <!--bootstrap -->
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
	<!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/dropify/dist/css/dropify.min.css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
    <style>
    #radioBtn .notActive{
    color: #3276b1;
    background-color: #fff;
    }

    #radioBtn1 .notActive{
    color: #3276b1;
    background-color: #fff;
    }
    </style>
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white white-sidebar-color">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
    include("header.php");

    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
      <div class="page-content-wrapper">
          <div class="page-content">
              <div class="page-bar">
                  <div class="page-title-breadcrumb">
                      <div class=" pull-left">
                          <div class="page-title"><?php echo L::detailscourse ?></div>
                      </div>
                      <ol class="breadcrumb page-breadcrumb pull-right">
                          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li><a class="parent-item" href="listcourses.php"><?php echo L::CourseMenu ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                          </li>
                          <li class="active"><?php echo L::detailscourse ?></li>
                      </ol>
                  </div>
              </div>
              <?php

                    if(isset($_SESSION['user']['addclasseok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
  <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
  <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addclasseok'] ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addclasseok']);
                    }

                     ?>

                     <?php
                     if($nbsessionOn==0)
                     {
                       ?>
                       <div class="alert alert-danger alert-dismissible fade show" role="alert">

                       <?php echo L::RequiredScolaireYear ?>

                       <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                        </a>
                       </div>
                       <?php
                     }
                      ?>


                      <div class="row">
						<div class="col-md-12">
							<!-- BEGIN PROFILE SIDEBAR -->
							<div class="profile-sidebar">
								<div class="card card-topline-aqua">
									<div class="card-body no-padding height-9">
										<div class="row">
											<div class="course-picture">
												<img src="../photo/course2.jpg" class="img-responsive"
													alt=""> </div>
										</div>
										<div class="profile-usertitle">
											<div class="profile-usertitle-name"> <?php echo utf8_decode(utf8_encode($namecourses));?> </div>
										</div>
										<!-- END SIDEBAR USER TITLE -->
									</div>
								</div>
								<div class="card">
									<div class="card-head card-topline-aqua" style="text-align:center">
										<header><?php echo L::aproposcourse ?></header>
									</div>
									<div class="card-body no-padding height-9">
										<div class="profile-desc">
											<?php //echo utf8_decode(utf8_encode($descricourses)); ?>
										</div>
										<ul class="list-group list-group-unbordered">
                      <li class="list-group-item">
                        <b><i class=""></i> <?php echo L::MatiereMenusingle ?></b>
                        <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($libellematcourses)); ?>  </div>
                      </li>
                      <li class="list-group-item">
												<b><i class="fa fa-university"></i> <?php echo L::ClasseMenu ?></b>
												<div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($classecourses)); ?></div>
											</li>
                      <li class="list-group-item">
												<b><i class="fa fa-calendar"></i> <?php echo L::LittleDateVersements ?></b>
												<div class="profile-desc-item pull-right">
                          <?php
                          $tabdate=explode("-",$datercourses);

                          echo $tabdate[2]." ".obtenirLibelleMois($tabdate[1])." ".$tabdate[0];
                          ?>
                        </div>
											</li>
											<li class="list-group-item">
												<b><i class="fa fa-clock-o"></i> <?php echo L::Duration ?> </b>
												<div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($durationcourses)); ?></div>
											</li>

											<li class="list-group-item">
												<b><i class="fa fa-user-circle-o"></i> <?php echo L::ProfsMenusingle ?></b>
												<div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($teatchercourses)); ?>  </div>
											</li>

										</ul>
										<div class="row list-separated profile-stat">
											<!-- <div class="col-md-4 col-sm-4 col-6">
												<div class="uppercase profile-stat-title"> 0 </div>
												<div class="uppercase profile-stat-text">  <a href="#" title="Téléchargement support de cours"><i class="fa fa-download fa-2x"></i></a>  </div>
											</div> -->
											<div class="col-md-6 col-sm-4 col-6">
												<div class="uppercase profile-stat-title"> <?php echo  $classe->DetermineNumberOfStudentInThisClasse($matiereidcourses,$codeEtabsession,$libellesessionencours); ?> </div>
												<div class="uppercase profile-stat-text"> <a href="#" title="<?php echo L::NBstudentInThisclass ?>"><i class="fa fa-group fa-2x"></i></a> </div>
											</div>
											<div class="col-md-6 col-sm-4 col-6">
												<div class="uppercase profile-stat-title"> <span id="commentspan"><?php echo $nbcomments; ?></span> </div>
												<div class="uppercase profile-stat-text">  <a href="#" title="<?php echo L::Coursecomment ?>"><i class="fa fa-comments fa-2x"></i></a> </div>
											</div>

										</div>
                    <div class="row list-separated profile-stat">
                      <div class="col-md-6 col-sm-4 col-6">
                        <?php
                        if($statutcourses==0)
                        {
                         ?>
                        <!-- <div class="col-md-12" style=""> -->
                          <a href="updatecourses.php?courseid=<?php echo $courseid ?>&classeid=<?php echo $classeid; ?>" title="Modifier le cours" class="btn btn-md btn-primary" style="border-radius:5px;"> <i class="fa fa-pencil"></i><?php echo L::UpdateCourseBtn ?></a>
                        <!-- </div> -->
                        <?php
                      }
                         ?>
											</div>
                      <div class="col-md-6 col-sm-4 col-6">
                        <?php
                        if($statutcourses==0)
                        {
                          ?>
                            <a href="#" title="Publier le cours" class="btn btn-success btn-md" onclick="publicationcourse(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabsession; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )"> <i class="fa fa-send"></i><?php echo L::CoursePublishBtn ?></a>
                          <?php
                        }else {
                          if($discussion_courses==0)
                          {
                            ?>
   <a href="#" class="btn btn-success btn-md" style="margin-left:-100px" title="<?php echo L::StartDiscussion ?>" onclick="startdiscussioncourse(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabsession; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )" > <?php echo L::StartDiscussion ?> <i class="fa fa-play "></i></a>
                            <?php
                          }else if($discussion_courses==1) {
                            ?>
   <a href="#" title="<?php echo L::StopDiscussion ?>" style="margin-left:-100px" class="btn btn-danger btn-md" onclick="stopdiscussioncourse(<?php echo $courseid;?>,<?php echo $classeid;  ?>,<?php echo $compteuserid; ?>,'<?php echo $codeEtabsession; ?>','<?php echo $libellesessionencours; ?>',<?php echo $matiereidcourses; ?> )" > <?php echo L::StopDiscussion ?> <i class="fa fa-stop "></i></a>
                            <?php
                          }else if($discussion_courses==2)
                          {
                            ?>
   <a href="#" title="<?php echo L::StoppedDiscussion ?>" style="margin-left:-100px" class="btn btn-success btn-md"  > <i class="fa fa-stop "></i><?php echo L::StoppedDiscussion ?></a>
                                                        <?php

                          }
                          ?>





                          <?php
                        }
                         ?>
											</div>




                    </div>

									</div>
								</div>
							</div>
							<!-- END BEGIN PROFILE SIDEBAR -->
							<!-- BEGIN PROFILE CONTENT -->
							<div class="profile-content">
                <div class="row">

                  <div class="col-md-12">
                    <div class="card card-topline-aqua">
                      <div class="card-head" style="text-align:center">
                          <header> <i class="fa fa-file"></i> <span style="text-align:center;"><?php echo L::CourseSupport ?> ( <?php echo count($coursesupports); ?> )</span> </header>

                      </div>
    									<div class="card-body no-padding height-9">
    									<!-- <a href="#" class="btn btn-primary btn-md"><i class="fa fa-pencil"></i> Modiffier</a> -->

                      <div class="" >
                        <?php
                        $i=1;
                        foreach ($coursesupports as $value):
                          if(strlen($value->fichier_support)>0)
                          {
                            $lien="../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$value->fichier_support;
                            ?>
                              <a href="<?php echo $lien;  ?>" target="_blank" class="btn btn-warning btn-md" ><i class="fa fa-download"></i> <?php echo L::LibelleSupport ?> <?php echo $i; ?></a>
                            <?php
                          }
                          $i++;
                        endforeach;
                         ?>
                      </div>

                      <!-- <?php
                    //  $lien="../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$filescourses;
                       ?>
                      <a href="<?php //echo $lien;  ?>" target="_blank" class="btn btn-warning btn-md" ><i class="fa fa-download"></i>Telecharger</a> -->
    										<!-- END SIDEBAR USER TITLE -->
    									</div>
    								</div>
                  </div>
                </div>
								<div class="row">
									<div class="col-md-12">
                    <div class="card">
  										<div class="card-topline-aqua">
  											<header></header>
  										</div>
  										<div class="white-box">
  											<!-- Nav tabs -->
  											<!-- Tab panes -->
  											<div class="tab-content">
  												<div class="tab-pane active fontawesome-demo">
  													<div id="biography" class="col-md-12">

  														<h4 class="font-bold"><?php echo L::CourseTheme ?></h4>
  														<hr>
  														<ul>
                                <?php
                                foreach ($coursesSections as  $value):
                                  ?>
                                  <li><?php echo utf8_decode(utf8_encode($value->libelle_coursesec)); ?></li>
                                  <?php
                                endforeach;
                                 ?>

  														</ul>
  														<br>
                              <h4 class="font-bold"><?php echo L::CourseAble ?>: </h4>
  														<hr>
  														<ul>
                                <?php
                                foreach ($coursescompetences as  $value):
                                  ?>
                                  <li><?php echo utf8_decode(utf8_encode($value->libelle_coursecomp)); ?></li>

                                  <?php
                                endforeach;
                                 ?>

  														</ul>
                              <br>
                              <h4 class="font-bold"><?php echo L::CourseHomeworksOrExercice ?> </h4>
  														<hr>
  														<ul>
                                <?php
                                foreach ($courseshomeworks as $value):
                                  ?>
                                  <li><?php echo utf8_decode(utf8_encode($value->libelle_coursewh)); ?></li>

                                  <?php
                                endforeach;
                                 ?>

  														</ul>
  													</div>
  												</div>
  											</div>
  										</div>
  									</div>
                  </div>
								</div>

							</div>
							<!-- END PROFILE CONTENT -->
						</div>
					</div>
          <?php
          if($discussion_courses==0)
          {

          }else {
            ?>
            <div class="row">
                    <div class="col-md-12">

                      <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                          <div class="card  card-box">
                                <div class="card-head">
                                    <header><?php echo L::Comments ?></header>
                                      <input type="hidden" name="nbcomments" id="nbcomments" value="<?php echo $nbcomments  ?>">
                                      <input type="hidden" name="nbonlines" id="nbonlines"  value="<?php echo $nbonlines; ?>">
                                    <?php

                                     ?>
                                    <div class="tools">

                                    </div>
                                </div>
                                <div class="card-body no-padding height-10">
                                    <div class="row">
                                        <ul class="chat nice-chat small-slimscroll-style" id="rowcomment">
                                          <?php
                                          if($nbcomments>0)
                                          {

                                            foreach ($datacoursescomments as $value):

                                              if($value->addby_commentc==$compteuserid)
                                              {
                                                $datateatchers=$teatcher->getTeatcherInforamtionsById($compteuserid);
                                                foreach ($datateatchers as $valueTea):
                                                  $emailteatchers=$valueTea->email_compte;
                                                  $phototeatchers=$valueTea->photo_compte;
                                                endforeach;
                                                $lienphoto="";
                                                if(strlen($phototeatchers)>0)
                                                {
                                                  $lienphoto="../photo/".$emailteatchers."/".$phototeatchers;
                                                }else {
                                                  $lienphoto="../photo/user5.jpg";
                                                }
                                                ?>
                                                <li class="in"><img src="<?php echo $lienphoto; ?>" class="avatar" alt="">
                                                    <div class="message">
                                                      <?php
                                                      $libesexe="";
                                                      $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
                                                      $dateadded=$value->date_commentc;
                                                      $tabdateadded=explode(" ",$dateadded);

                                                      $dateinsert=$tabdateadded[0];
                                                      $tabdateinsert=explode("-",$dateinsert);
                                                      $yearsinsert=$tabdateinsert[0];
                                                      $monthinsert=$tabdateinsert[1];
                                                      $daysinsert=$tabdateinsert[2];
                                                      $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

                                                      if($sexe=="F")
                                                      {
                                                        $libesexe="Mme";
                                                      }else {
                                                        $libesexe="Mr";
                                                      }

                                                       ?>
                                                        <span class="arrow"></span> <a class="name" href="#"><?php echo $libesexe." ".tronquer(utf8_decode(utf8_encode($etabs->getUtilisateurName($value->addby_commentc))),20); ?></a> <span class="datetime"><?php echo $newdate." ".$tabdateadded[1]; ?></span>
                                                        <span class="body"><?php echo utf8_decode(utf8_encode($value->message_commentc)); ?>  </span>
                                                    </div>
                                                </li>
                                                <?php
                                              }else {
                                                $studentInfos=$student->getAllInformationsOfStudentOne($value->addby_commentc,$libellesessionencours);
                                                foreach ($studentInfos as $personnal):
                                                  $matriculestudents=$personnal->matricule_eleve;
                                                  $photostudents=$personnal->photo_compte;
                                                endforeach;
                                                $lienphoto="";

                                                if(strlen($photostudents)>0)
                                                {
                                                  $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                }else {
                                                  $lienphoto="../photo/user5.jpg";
                                                }

                                                ?>
                                                <li class="out"><img src="<?php echo $lienphoto; ?>" class="avatar" alt="">
                                                  <div class="message">
                                                    <?php
                                                    $libesexe="";
                                                    $sexe=$etabs->getUtilisateurSexe($value->addby_commentc);
                                                    $dateadded=$value->date_commentc;
                                                    $tabdateadded=explode(" ",$dateadded);

                                                    $dateinsert=$tabdateadded[0];
                                                    $tabdateinsert=explode("-",$dateinsert);
                                                    $yearsinsert=$tabdateinsert[0];
                                                    $monthinsert=$tabdateinsert[1];
                                                    $daysinsert=$tabdateinsert[2];
                                                    $newdate=$daysinsert."-".$monthinsert."-".$yearsinsert;

                                                    if($sexe=="F")
                                                    {
                                                      $libesexe="Mme";
                                                    }else {
                                                      $libesexe="Mr";
                                                    }

                                                     ?>
                                                      <span class="arrow"></span> <a class="name" href="#"><?php echo $libesexe." ".$etabs->getUtilisateurName($value->addby_commentc);
                                                       ?></a> <span class="datetime"><?php echo $newdate." ".$tabdateadded[1]; ?></span>
                                                      <span class="body"><?php echo utf8_decode(utf8_encode($value->message_commentc)); ?>  </span>
                                                  </div>
                                                </li>
                                                <?php
                                              }

                                            endforeach;
                                            ?>

                                            <?php
                                          }else {
                                            ?>

                                            <?php
                                          }
                                           ?>

                                        </ul>
                                        <div class="box-footer chat-box-submit">
                                          <?php
                                          if($discussion_courses==2)
                                          {

                                          }else {
                                            ?>
                                            <form id="commentForm">
                                              <div class="input-group">
                                                <div class="col-md-12">
                           		 									<div class="form-group">

                                                  <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" name="message"  id="message"placeholder="Entrer le commentaire" class="form-control input-height">
                                                    </div><br><br>
                                                    <div class="col-md-12">
                                                      <span class="input-group-btn pull-right">
                                                      <button type="submit" class="btn btn-success btn-md" style="height:40px"><?php echo L::Sendbutton ?> <i class="fa fa-arrow-right"></i></button>
                                                      </span>
                                                    </div>
                                                  </div>




                                                     </div>
                           		 									 </div>

                                                 </div>
                                            </form>
                                            <?php
                                          }
                                           ?>

                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                          <div class="card  card-box" style="height:460px;">
                             <div class="card-head">
                                 <header><?php echo L::Online ?></header>
                                 <div class="tools">

                                 </div>
                             </div>
                             <div class="card-body no-padding height-10">
                                 <div class="row">
                                     <div class="noti-information notification-menu">
                                         <div class="notification-list mail-list not-list small-slimscroll-style" id="rowconnexion">

                                           <?php
                                           if($nbonlines>0 && $nbonlines==1)
                                           {

                                             ?>
                                             <?php
                                              foreach ($datacoursescommentonlines as  $value):
                                                $studentInfos=$student->getAllInformationsOfStudentOne($value->id_compte,$libellesessionencours);
                                                foreach ($studentInfos as $personnal):
                                                  $matriculestudents=$personnal->matricule_eleve;
                                                  $photostudents=$personnal->photo_compte;
                                                endforeach;
                                                $lienphoto="";

                                                if(strlen($photostudents)>0)
                                                {
                                                  $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                                }else {
                                                  $lienphoto="../photo/user5.jpg";
                                                }
                                              ?>
                                             <div class="row">
                                               <div class="col-md-8">
                                                 <span class="text-purple" style="width:1500px"><?php echo utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte))); ?></span>
                                                 <div>
                                                  <span class="clsAvailable"><?php echo L::Online ?></span>
                                                </div>
                                               </div>
                                               <div class="col-md-4">
                                                 <a href="javascript:;" class="single-mail"> <img src="<?php echo $lienphoto; ?>" alt="" width="40" height="40" class="pull-right" style="border-radius:10px;margin-left:200px">

                                                 </a>
                                               </div>

                                             </div>
                                             <?php
                                           endforeach;
                                              ?>

                                             <?php
                                           }else if($nbonlines>0 && $nbonlines>1)
                                           {
                                             foreach ($datacoursescommentonlines as  $value):

                                               $studentInfos=$student->getAllInformationsOfStudentOne($value->id_compte,$libellesessionencours);
                                               foreach ($studentInfos as $personnal):
                                                 $matriculestudents=$personnal->matricule_eleve;
                                                 $photostudents=$personnal->photo_compte;
                                               endforeach;
                                               $lienphoto="";

                                               if(strlen($photostudents)>0)
                                               {
                                                 $lienphoto="../photo/Students/".$matriculestudents."/".$photostudents;
                                               }else {
                                                 $lienphoto="../photo/user5.jpg";
                                               }

                                               ?>
                                               <a href="javascript:;" class="single-mail"> <img src="<?php echo $lienphoto; ?>" alt="" width="40" height="40" class="pull-right" style="border-radius:10px"> <span class="text-purple"><?php echo utf8_decode(utf8_encode($etabs->getUtilisateurName($value->id_compte))); ?></span>
                                                 <div>
                                                             <span class="clsAvailable"><?php echo L::Online ?></span>
                                                         </div>
                                               </a>
                                               <?php
                                             endforeach;
                                           }
                                            ?>





                                         </div>

                                     </div>
                                 </div>
                             </div>
                         </div>
            </div>

                      </div>

                    </div>
            </div>
            <?php
          }
           ?>

          </div>
      </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   	<script src="../assets2/plugins/popper/popper.min.js" ></script>
    <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
       <!-- bootstrap -->
    <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" ></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets2/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets2/js/pages/table/table_data.js" ></script>
    <script src="../assets2/plugins/select2/js/select2.js" ></script>
    <script src="../assets2/js/pages/select2/select2-init.js" ></script>
    <!--script src="../assets2/plugins/summernote/summernote.js" ></script>
    <script src="../assets2/plugins/summernote/lang/summernote-fr-FR.js" ></script-->
       <!-- calendar -->
       <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>
    <script src="../assets2/plugins/moment/moment.min.js" ></script>
    <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
       <!-- Common js-->
   	<script src="../assets2/js/app.js" ></script>
    <script src="../assets2/js/layout.js" ></script>
   	<script src="../assets2/js/theme-color.js" ></script>
   	<!-- Material -->
   	<script src="../assets2/plugins/material/material.min.js"></script>
    <script src="../assets2/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets2/js/dropify.js"></script>
    <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>

 <script>
 /*$('#summernote').summernote({
     placeholder: '',
     tabsize: 2,
     height: 200,
      lang: 'fr-FR'
   });*/

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function Testing() {
     var courseid="<?php echo $courseid; ?>";
     var classeid="<?php echo $classeid; ?>";
     var codeEtab="<?php echo $codeEtabsession ?>";
     var sessionEtab="<?php echo $libellesessionencours ?>";
     var teatcherid="<?php echo $teatcherid_courses;  ?>";
     var matiereid="<?php echo $matiereidcourses; ?>";
     var oldonlines=$("#nbonlines").val();
     var oldcomment=$("#nbcomments").val();

     //nous allons verifier si le nombre de connectés a augmenter

      var etape=12;

      $.ajax({

           url: '../ajax/courses.php',
           type: 'POST',
           async:true,
           data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&oldonlines='+oldonlines,
           dataType: 'text',
           success: function (content, statut) {

             var tabcontent=content.split("*");
             var tabone=tabcontent[0];
             var newOnlineNb=tabcontent[1];


             if(tabone==0)
             {
               //nous devons mettre a jour la liste des connectés

               var etape=13;

               $.ajax({

                    url: '../ajax/courses.php',
                    type: 'POST',
                    async:true,
                    data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&oldonlines='+oldonlines,
                    dataType: 'text',
                    success: function (content, statut) {

                      $("#rowconnexion").html("");
                      $("#rowconnexion").append(content);

                      $("#nbonlines").val(newOnlineNb);


                    }
                  });

             }

             var etape=14;

             $.ajax({

                  url: '../ajax/courses.php',
                  type: 'POST',
                  async:true,
                  data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&oldonlines='+oldcomment,
                  dataType: 'text',
                  success: function (content, statut) {

                    var tabcontent=content.split("*");
                    var tabtwo=tabcontent[0];
                    var newcommentNb=tabcontent[1];

                    if(tabone==0)
                    {
                      //nous devons mettre a jour la liste des connectés

                      var etape=15;

                      $.ajax({

                           url: '../ajax/courses.php',
                           type: 'POST',
                           async:true,
                           data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&oldonlines='+oldonlines,
                           dataType: 'text',
                           success: function (content, statut) {

                             $("#rowcomment").html("");
                             $("#rowcomment").append(content);

                             $("#oldcomment").val(newcommentNb);
                             // $("#commentspan").text(newcommentNb);

                             $('#commentspan" span').contents().not(this).eq(0).replaceWith('Do it again ');




                           }
                         });

                    }


                  }
                });


           }
         });


   // alert("Hello !!!");
   }
   setInterval(Testing, 1*2*1000 );

   function stopdiscussioncourse(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
   {
     Swal.fire({
     title: '<?php echo L::WarningLib ?>',
     text: "<?php echo L::DoyouStopdiscuss ?>",
     type: 'warning',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: '<?php echo L::Stopped ?>',
     cancelButtonText: '<?php echo L::AnnulerBtn ?>',
     }).then((result) => {
     if (result.value) {

     var etape=9;

     $.ajax({

          url: '../ajax/courses.php',
          type: 'POST',
          async:true,
          data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
          dataType: 'text',
          success: function (content, statut) {



             location.reload();

          }
        });

     }else {

     }
     })

   }


function startdiscussioncourse(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
{
  Swal.fire({
  title: '<?php echo L::WarningLib ?>',
  text: "<?php echo L::DoyouStartdiscuss ?>",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: '<?php echo L::Started ?>',
  cancelButtonText: '<?php echo L::AnnulerBtn ?>',
  }).then((result) => {
  if (result.value) {

  var etape=8;

  $.ajax({

       url: '../ajax/courses.php',
       type: 'POST',
       async:true,
       data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
       dataType: 'text',
       success: function (content, statut) {



          location.reload();

       }
     });

  }else {

  }
  })

}


function publicationcourse(courseid,classeid,teatcherid,codeEtab,sessionEtab,matiereid)
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "<?php echo L::DoyouPublishcourse ?>",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::Published ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {

  var etape=1;

  $.ajax({

       url: '../ajax/courses.php',
       type: 'POST',
       async:true,
       data: 'teatcherid=' + teatcherid+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid,
       dataType: 'text',
       success: function (content, statut) {

          // $("#FormAddAcademique #codeEtab").val(content);

          location.reload();

       }
     });

}else {

}
})
}

 $("#fichier3").dropify({
   messages: {
       "default": "Merci de selectionner le support",
       "replace": "Modifier le support",
       "remove" : "Supprimer le support",
       "error"  : "Erreur"
   }
 });

$("#matclasse").select2();
$("#classeEtab").select2();

function searchcodeEtab(id)
{
var classe=$("#classeEtab").val();
var teatcherId=id;
var etape=7;
var matiere=$("#matclasse").val();

$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
     dataType: 'text',
     success: function (content, statut) {

        $("#FormAddAcademique #codeEtab").val(content);

     }
   });

}

function searchmatiere(id)
{

  var classe=$("#classeEtab").val();
  var teatcherId=id;
  var etape=6;


$.ajax({

     url: '../ajax/matiere.php',
     type: 'POST',
     async:true,
     data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
     dataType: 'text',
     success: function (content, statut) {


       $("#matclasse").html("");
       $("#matclasse").html(content);

     }
   });
}

function deletedTache(id)
{
var concattache=$("#concattache").val();

$("#concattache").val($("#concattache").val().replace(id+"@", ""));

 $('#rowTache'+id+'').remove();

 recalcultachenb();
}

function deletedComp(id)
{
var concatcomp=$("#concatcomp").val();

$("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

 $('#rowComp'+id+'').remove();

 // recalculsectionnb();

 var concatcomp=$("#concatcomp").val();

 var tab=concatcomp.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbcomp").val(nbtabnew);

}

function deletedTaches(id)
{
var concattaches=$("#concattaches").val();

$("#concattaches").val($("#concattaches").val().replace(id+"@", ""));

 $('#rowTaches'+id+'').remove();

 // recalculsectionnb();

 var concattaches=$("#concattaches").val();

 var tab=concattaches.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbtaches").val(nbtabnew);

}


function deletedSection(id)
{
var concatsection=$("#concatsection").val();

$("#concatsection").val($("#concatsection").val().replace(id+"@", ""));

 $('#rowSection'+id+'').remove();

 // recalculsectionnb();

 var concatsection=$("#concatsection").val();

 var tab=concatsection.split("@");

 var nbtab=tab.length;

 var nbtabnew=parseInt(nbtab)-1;

 $("#concatnbsection").val(nbtabnew);

}

function recalcultachenb()
{
var concattache=$("#concattache").val();

var tab=concattache.split("@");

var nbtab=tab.length;

var nbtabnew=parseInt(nbtab)-1;

$("#concatnbtache").val(nbtabnew);
}

function recalculsectionnb()
{

}


function AddtachesRow()
{
var nb=$("#nbtaches").val();
var nouveau= parseInt(nb)+1;
$("#nbtaches").val(nouveau);

  var concattaches=$("#concattaches").val();
  $("#concattaches").val(concattaches+nouveau+"@");

  var concattaches=$("#concattaches").val();

  var tab=concattaches.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbtaches").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field2').append('<tr id="rowTaches'+nouveau+'"><td><input type="text" name="taches_'+nouveau+'" id="taches_'+nouveau+'" placeholder="<?php echo L::EnterNewExercice ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteTaches'+nouveau+'" id="deleteTaches'+nouveau+'"  onclick="deletedTaches('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#taches_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddNewexerciceplease ?>"
}
      });
  }


}

function AddcompRow()
{
var nb=$("#nbcomp").val();
var nouveau= parseInt(nb)+1;
$("#nbcomp").val(nouveau);

  var concatcomp=$("#concatcomp").val();
  $("#concatcomp").val(concatcomp+nouveau+"@");

  var concatcomp=$("#concatcomp").val();

  var tab=concatcomp.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbcomp").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field1').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control competence_list" /></td><td><button type="button" id="deleteComp'+nouveau+'" id="deleteComp'+nouveau+'"  onclick="deletedComp('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#comp_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddCompetenceViseeplease ?>"
}
      });
  }


}

function AddsectionRow()
{
var nb=$("#nb").val();
var nouveau= parseInt(nb)+1;
$("#nb").val(nouveau);

  var concatsection=$("#concatsection").val();
  $("#concatsection").val(concatsection+nouveau+"@");

  var concatsection=$("#concatsection").val();

  var tab=concatsection.split("@");

  var nbtab=tab.length;

  var nbtabnew=parseInt(nbtab)-1;

  $("#concatnbsection").val(nbtabnew);

  // recalculsectionnb();

  if(nbtabnew==1)
  {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove" disabled="disabled">X</button></td></tr>');
  }else {
    $('#dynamic_field').append('<tr id="rowSection'+nouveau+'"><td><input type="text" name="section_'+nouveau+'" id="section_'+nouveau+'" placeholder="<?php echo L::EnterSection ?>" class="form-control objectif_list" /></td><td><button type="button" id="deleteSection'+nouveau+'" id="deleteSection'+nouveau+'"  onclick="deletedSection('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');
  }



  for(var i=0;i<nbtabnew;i++)
  {
    var indice=tab[i];
    // alert(indice);
    $("#section_"+indice).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::AddSectionplease ?>"
}
      });
  }


}

function AddtacheRow()
{
var nb=$("#nbtache").val();
var nouveau= parseInt(nb)+1;
$("#nbtache").val(nouveau);

  var concattache=$("#concattache").val();
  $("#concattache").val(concattache+nouveau+"@");

  recalcultachenb();

  $('#dynamic_field1').append('<tr id="rowTache'+nouveau+'"><td><input type="text" name="tache_'+nouveau+'" id="tache_'+nouveau+'" placeholder="Entrer une tache" class="form-control objectif_list" /></td><td><button type="button" id="deleteTache'+nouveau+'" id="deleteTache'+nouveau+'"  onclick="deletedTache('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

  for(var i=1;i<=nouveau;i++)
  {
    $("#tache_"+i).rules( "add", {
        required: true,
        messages: {
        required: "<?php echo L::RequiredChamp ?>"
}
      });
  }


}

 $(document).ready(function() {

   $("#commentForm").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertBefore(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required",
       message:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"<?php echo L::DurationcourseRequired ?>",
     detailscourse:"<?php echo L::DetailscourseRequired ?>",
     datecourse:"<?php echo L::DatecourseRequired ?>",
     message:"<?php echo L::PleaseEnterCommentMessage?>"

   },
   submitHandler: function(form) {


     var commentaire=$("#message").val();
     var courseid="<?php echo $courseid; ?>";
     var classeid="<?php echo $classeid; ?>";
     var codeEtab="<?php echo $codeEtabsession ?>";
     var sessionEtab="<?php echo $libellesessionencours ?>";
     var addby="<?php echo $compteuserid; ?>";
     var matiereid="<?php echo $matiereidcourses; ?>";
     var teatcherid="<?php echo $teatcherid_courses; ?>";

     var etape=10;

     $.ajax({

          url: '../ajax/courses.php',
          type: 'POST',
          async:true,
          data: 'addby=' + addby+ '&etape=' + etape+'&classe='+classeid+'&matiere='+matiereid+'&codeEtab='+codeEtab+'&sessionEtab='+sessionEtab+'&courseid='+courseid+'&commentaire='+commentaire+'&teatcherid='+teatcherid,
          dataType: 'text',
          success: function (content, statut) {

            $("#message").val("");

             // location.reload();

          }
        });


   }

   });

   $("#FormAddAcademique").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
    },
    highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
    },
    success: function (e) {
         $(e).closest('.control-group').removeClass('error').addClass('info');
         $(e).remove();
     },
     rules:{

       programme:"required",
       descri:"required",
       classeEtab:"required",
       matclasse:"required",
       fichier:"required",
       durationcourse:"required",
       detailscourse:"required",
       datecourse:"required"

     },
   messages: {
     programme:"Merci de renseigner le libellé du programme",
     descri:"Merci de renseigner le détails de la fiche ",
     classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
     matclasse:"Merci de <?php echo L::SelectSubjects ?>",
     fichier:"Merci de selectionner le fichier du programme",
     durationcourse:"<?php echo L::DurationcourseRequired ?>",
     detailscourse:"<?php echo L::DetailscourseRequired ?>",
     datecourse:"<?php echo L::DatecourseRequired ?>"
   },
   submitHandler: function(form) {

   form.submit();



   }

   });

   // AddsectionRow();
   //  AddcompRow();

   $('#add').click(function(){

     //creation d'une ligne de section

     AddsectionRow();

   });

   $('#addcomp').click(function(){

     //creation d'une ligne de section

     AddcompRow();

   });

   $('#addtache').click(function(){

     //creation d'une ligne de section

     AddtachesRow();

   });






 });

 </script>
    <!-- end js include path -->
  </body>

</html>
