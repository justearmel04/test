

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Free mobile app HTML landing page template to help you build a great online presence for your app which will convert visitors into users">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="" /> <!-- website name -->
    <meta property="og:site" content="" /> <!-- website link -->
    <meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" /> <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>  Application de communication et d'administration  des écoles  </title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    
    <!-- Favicon  -->
    <link rel="icon" href="images/favicon.png">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/webfonts/fa-brands-400.woff2"></script>
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
    <div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top" style="background-color:white;">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Leno</a> -->

        <!-- Image Logo --> 

        <a  href="index.php" style="text-decoration:none;font-size:10px; color:#FAB71D;"><img src="images/logo0.jpg" width="60%" height="30%">
        </a> 
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <p></p>

          <div class="collapse navbar-collapse" id="navbarsExampleDefault" >
            <ul class="navbar-nav ml-auto">
            
                 <li class="nav-item">
                    <a class="nav-link page-scroll" href="#temoignage" style="color:#FAB71D"> Pourquoi <span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span></a>
                </li>  

                <li class="nav-item dropdown">
                    <a class="nav-link page-scroll"  href="#features" style="color:#FAB71D"> Fonctionnalités</a>

                </li> 

                 <li class="nav-item">
                    <a class="nav-link page-scroll"  href="#details" style="color:#FAB71D"> Bénéfices</a>
                </li>   


                <li class="nav-item">
                    <a class="nav-link page-scroll"  href="#contact" style="color:#FAB71D"> Commandez une démo </a>
                </li>    
                
           
                 <li class="nav-item nav-link page-scroll" style="padding-top:0px">

                    <button type="button" class="actual-reg-btn " style="background-color: #2F4187"> <a href="signup.php" style="padding-left:2px; font-size:12px; color:#FAB71D;  font-weight:bold; text-decoration:none; "> Connexion  </a>
                 </button>


                </li> 


                 <li class="nav-item nav-link page-scroll" style="padding-top:0px">

                    <button type="button" class="actual-reg-btn" style="background-color: #2F4187"> <a href="subscribe.php" style="padding-left:7px; font-size:12px; color:#FAB71D;  font-weight:bold; text-decoration:none;"> Souscrire </a>
                 </button>


                </li>

                <li class="dropdown language-switch" style="color:#FAB71D;" style="padding-left: 200px;">


                    <span style="padding-top:100px"> 
                        
                        <a class="dropdown-toggle" > <img 
                        src="images/french_flag.jpg" style="width:20px; color:#FAB71D; "  class="position-left" alt=""  > <span> Francais </span>
                      </a>

                    </span>
                 
                    
                    
                            <ul class="dropdown-menu" style="background-color:#2F4187">
                                <li onclick="addFrench()" style="font-size:12px">
                                    <a class="french" style="color:white"><img src="images/french_flag.jpg" alt="" style="width:17px;" > Francais </a>
                                </li> <hr style="background-color: white">

                                <li onclick="addEnglish()" style="font-size:12px">
                                    <a class="english" style="color:white"><img src="images/gb.png" alt="" > English</a>
                                </li>


                            </ul>
                </li>

        </div>

       
    </nav> <br><!-- end of navbar -->



</script>
    <!-- end of navbar -->


   <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" data-pause="false">

    <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" style="height:750px;">
    <div class="carousel-item active" data-interval="3000">
      <img src="images/header15.png" class="d-block w-100" alt="..." style="width: 100% height:auto">
    </div>
    <div class="carousel-item"data-interval="2000">
      <img src="images/header18.jpg" class="d-block w-100" alt="..." style="width: 100% height:auto">

    </div>
    <div class="carousel-item"data-interval="2000">
      <img src="images/header19.jpg" class="d-block w-100" alt="..." style="width: 100% height:auto">
      
    </div>
      
    </div>

  </div> 



  <!-- Presentation -->


  <!-- Download -->
    <div class="basic-4"id="presentation">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-5"> <p></p>
                    <h2  style="color:#FAB71D"> Présentation </h2>
                   
                        
                        <p class="p-large"> <i class="fas fa-check" style="color: #FAB71D"></i>  <span  style="color: #FAB71D;"> <span style="color: #575756">X</span>school'ink </span> est une application multilingue destinée aux écoles maternelles et primaires, écoles secondaires et établissements d’enseignement supérieur</p>
                        <p class="p-large">  <span  style="color: #FAB71D;"> <i class="fas fa-check" style="color: #FAB71D"></i>  <span style="color: #575756">X</span>school'ink </span> est une application 3 en 1 qui permet :

                            <p  class="p-large"style="padding-left:25px;">  d'automatiser la gestion administrative et académique des établissements académiques. </p>

                            <p class="p-large"  style="padding-left:25px;">  de faciliter la communication entre l’école et les parents d’élèves. </p>

                             <p class="p-large" style="padding-left:25px;">  d'administrer les cours à distance.</p>
                             
                         </p>

                         <p class="p-large"> <i class="fas fa-check" style="color: #FAB71D"></i>  <span  style="color: #FAB71D;"> <span style="color: #575756">X</span>school'ink </span>  est disponible en version web, mais aussi en version mobile.</p>


                        <a style="background-color: #00C9DB"  target = "_blank"  href="https://play.google.com/store/apps/details?id=cm.ftg.xschool"><span style="color: white"> <img src="images/telechargement0.png" width="36%"></span></a>
                       <a style="background-color: #00C9DB"  target = "_blank"  href="https://play.google.com/store/apps/details?id=cm.ftg.xschool"><span style="color: white"> <img src="images/telechargement1.png" width="40%"></span></a>


                   
                </div> <!-- end of col -->
                <div class="col-lg-6 col-xl-7">
                    <div class="image-container">
                        <img class="img-fluid" src="images/download.png" alt="alternative">
                    </div> <!-- end of img-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-4 -->
    <!-- end of download -->




    <!-- who are we-->

  <div class="basic-3" style="background-color: white">
        <div class="second" id="temoignage">
            <div class="container" >
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-container" >
                            <h2 style="color: #2F4187; "> Pourquoi <span style="color:#575756">X</span><span style="color:#FAB71D;font-size:32px">school’ink</span> ? </h2>
                            <h6 style="color: #FAB71D;"> <i class="fas fa-check" style="color: #FAB71D"></i>   Intuitif et attractif </h6>

                            <p style="color: #2F4187;"> Son interface attractive et ergonomique est facile à prendre en main aussi bien pour les utilisateurs initiés que pour les débutants. </p>


                            <h6 style="color: #FAB71D;"> <i class="fas fa-check" style="color: #FAB71D"></i>   Modulable et extensible </h6>

                            <p style="color: #2F4187;"> Sa conception par modules vous permet de faire évoluer l’application et l’étendre à de nouvelles fonctionnalités selon les besoins de votre établissement.  <span style="font-size:10px">  </span> </p>


                            <h6 style="color: #FAB71D;"> <i class="fas fa-check" style="color: #FAB71D"></i>   Multiplateforme </h6>

                            <p style="color: #2F4187;"> Vous pouvez utiliser <span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span> à partir de votre ordinateur, de votre téléphone ou de votre tablette. </p>

                      <h6 style="color: #FAB71D;"> <i class="fas fa-check" style="color: #FAB71D"></i> Securisé  </h6>

                            <p style="color: #2F4187;"> Des protocoles  très stricts sont mis en œuvre afin de garantir un niveau maximal de sécurité et de confidentialité pendant l’accès, le stockage et le transfert des données. </p>

                            
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6">
                        <img class="img-fluid" src="images/details-2-iphone.png" alt="alternative">
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of second -->
    </div> <!-- end of basic-3 --> 


    <!-- Features -->

    <div id="features" class="tabs" style="background-color:#2F4187;" >
        <div class="container">
            <div class="row">
                
                <div class="col-lg-12">
                    <h2 style="color:#FAB71D;text-align: center"> Fonctionnalités </h2>
                    <div class="p-heading p-large">  </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">

                <!-- Tabs Links -->
                <ul class="nav nav-tabs" id="lenoTabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " id="nav-tab-3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false"><i class="fas fa-laptop-house"></i> Administration des écoles   </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " id="nav-tab-2" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true"><i class="far fa-envelope"></i>  Communication écoles et parents </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active " id="nav-tab-1" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false"><i class="fas fa-tv"></i> Cours à distance   </a>
                    </li>
                    
                </ul>
                <!-- end of tabs links -->


                <!-- Tabs Content-->
                <div class="tab-content" id="lenoTabsContent">
                    
                    <!-- Tab -->
                    <div class="tab-pane " id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                        <div class="container">
                            <div class="text-area">

                                        <h3> <span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span></h3>
                                        
                                        <h5> Un outil numérique efficace de communication avec les parents d’élèves</h5><br>
                            </div> <!-- end of text-area -->





                            <div class="row">
                                
                                <!-- Icon Cards Pane -->
                                <div class="col-lg-4">
                                    <div class="card left-pane first">
                                        <div class="card-body">
                                            <div class="text-wrapper">
                                               
                                                <p>  Envoi de mails groupés aux parents, aux élèves et  aux enseignants</p>
                                            </div>
                                            <div class="card-icon" style="background-color:#FAB71D"><br>
                                                <i class="far fa-compass" style="font-size:30px"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card left-pane">
                                        <div class="card-body">
                                            <div class="text-wrapper">
                                                
                                                <p> Diffusion instantanée des informations sur les activités parascolaires  </p>
                                            </div>
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-file-code"  style="font-size:30px"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card left-pane">
                                        <div class="card-body">
                                            <div class="text-wrapper">
                                                
                                                <p>   Suivi des performances (assiduité, discipline et notes) en temps réel</p>
                                            </div>
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-gem"  style="font-size:30px"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card left-pane">
                                        <div class="card-body">
                                            <div class="text-wrapper">
                                               
                                                <p>   Envoi de notifications personnalisées aux parents </p>
                                            </div>
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-gem" style="font-size:30px"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of icon cards pane -->

                                <!-- Image Pane -->
                                <div class="col-lg-4">
                                    <img class="img-fluid" src="images/features-iphone-1.png" alt="alternative">
                                </div>
                                <!-- end of image pane -->
                                
                                <!-- Icon Cards Pane -->
                                <div class="col-lg-4">
                                    <div class="card right-pane first">
                                        <div class="card-body">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-calendar-check" style="font-size:30px"></i>
                                            </div>
                                            <div class="text-wrapper">
                                            
                                                <p> Facilitation du  travail collaboratif</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card right-pane">
                                        <div class="card-body">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-bookmark"style="font-size:30px"></i>
                                            </div>
                                            <div class="text-wrapper">
                                                
                                                <p> Transmistion d'informations sur les comptes  scolaires des apprenants </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card right-pane">
                                        <div class="card-body">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="fas fa-cube"style="font-size:30px"></i>
                                            </div>
                                            <div class="text-wrapper">
                                               
                                                <p> Partage d'informations sur la vie de la classe et de l’établissement</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card right-pane">
                                        <div class="card-body">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="fas fa-cube"style="font-size:30px"></i>
                                            </div>
                                            <div class="text-wrapper">
                                                
                                                <p> Transmission, consultation ou demande d'informations générales sur la vie scolaire et les performances des apprenants </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end of icon cards pane -->

                            </div> <!-- end of row -->
                        </div> <!-- end of container -->
                    </div> <!-- end of tab-pane -->
                    <!-- end of tab -->

                    <!-- Tab -->
                    <div class="tab-pane fade show active" id="tab-2" role="tabpanel" aria-labelledby="tab-2">
                        <div class="container">
                            <div class="row">

                                <!-- Image Pane -->
                                <div class="col-md-4">
                                    <img class="img-fluid" src="images/features-iphone-2.png" alt="alternative">
                                </div>
                                <!-- end of image pane -->
                                
                                <!-- Text And Icon Cards Area -->
                                <div class="col-md-8">
                                    <div class="text-area">
                                        <h3><span style="color:#575756"> X</span><span style="color:#FAB71D">school’ink</span> </h3>
                                        <h5>Un levier essentiel pour l’organisation des cours à distance.</h5>
                                        <p><span style="color:#575756"> X</span><span style="color:#FAB71D">school’ink</span>  est le meilleur support de l’enseignement numérique qui permet d’organiser et animer les cours interactifs en ligne, poster, surveiller et noter les devoirs et examens en ligne.</p>
                                    </div> <!-- end of text-area -->
                                    
                                    <div class="icon-cards-area">
                                            <div class="card">
                                                <div class="card-icon" style="background-color:#FAB71D"><br>
                                                    <i class="fas fa-cube" style="font-size:30px"></i>
                                                </div>
                                            <div class="card-body">
                                              
                                                <p>   Publication des cours et des devoirs en ligne</p>
                                            </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-icon" style="background-color:#FAB71D"><br>
                                                    <i class="far fa-bookmark" style="font-size:30px"></i>
                                                </div>
                                            <div class="card-body">
                                                
                                                <p>   Animation des sessions interactives de cours</p>
                                            </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-icon" style="background-color:#FAB71D"><br>
                                                    <i class="far fa-calendar-check" style="font-size:30px"></i>
                                                </div>
                                            <div class="card-body">
                                                
                                                <p>   Activation des quizzes en ligne </p>
                                            </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-icon" style="background-color:#FAB71D"><br>
                                                    <i class="far fa-file-code" style="font-size:30px"></i>
                                                </div>
                                            <div class="card-body">
                                               
                                                <p>  Organisation des évaluations en ligne</p>
                                            </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-icon" style="background-color:#FAB71D"><br>
                                                    <i class="far fa-file-code" style="font-size:30px"></i>
                                                </div>
                                                <div class="text-area">
                                                    
                                                    <p>  Publication des notes à la suite d’une évaluation</p>
                                                </div> <!-- end of text-area -->
                                            </div>

                                    </div> <!-- end of icon cards area -->
                                </div> <!-- end of col-md-8 -->
                                <!-- end of text and icon cards area -->

                            </div> <!-- end of row -->
                        </div> <!-- end of container -->
                    </div> <!-- end of tab-pane -->
                    <!-- end of tab -->

                    <!-- Tab -->
                    <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="tab-3">
                        <div class="container">
                            <div class="row">

                                <!-- Text And Icon Cards Area -->
                                <div class="col-md-8">
                                     <div class="text-area">
                                        <h3><span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span>  </h3>
                                        <h5> C'est un outil de gestion des processus métiers des établissements d’enseignement.</h5>
                                    </div> <!-- end of text-area -->
                                    <div class="icon-cards-area">
                                        <div class="card" >
                                            <div class="card-icon" style="background-color:#FAB71D"><br>
                                                <i class="far fa-calendar-check" style="font-size:30px"></i>
                                            </div>
                                            <div class="card-body">
                                                
                                                <p>  Stockage, traitement et à l'analyse numérique de toutes les informations administratives et académiques.</p>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-icon" style="background-color:#FAB71D"><br>
                                                <i class="far fa-file-code" style="font-size:30px"></i>
                                            </div>
                                            <div class="card-body">
                                                
                                                <p>   Edition des tableaux de bord et  rapports.</p>

                                            </div>
                                        </div>
                                        
                                        <div class="card">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="fas fa-cube" style="font-size:30px"></i>
                                            </div>
                                            <div class="card-body">
                                                
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Enregistrement des inscriptions.  </p>
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Suivi du recouvrement des frais de scolarité.</p>
                                                <p> <i class="fas fa-check" style="color:#FAB71D"></i> Edition des cahiers de texte numérique. </p>
                                                <p>  <i class="fas fa-check" style="color:#FAB71D"></i> Formalisation des syllabus. </p>
                                                <p>  <i class="fas fa-check" style="color:#FAB71D"></i> Suivi de la discipline.</p>
                                                <p>  <i class="fas fa-check" style="color:#FAB71D"></i>  Etablissement, diffusion des emplois de  temps.</p>
                                                
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-icon"style="background-color:#FAB71D"><br>
                                                <i class="far fa-bookmark" style="font-size:30px"></i>
                                            </div>
                                            <div class="card-body">
                                                
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Mise à disposition des cours.</p>
                                                <p>  <i class="fas fa-check" style="color:#FAB71D"></i> Programmation des examens et évaluations.</p>
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Édition des bulletins de notes.  </p>
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Consignation des notes et calcul automatique des moyennes. </p>
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i>  Gestion des absences.</p>
                                                <p>   <i class="fas fa-check" style="color:#FAB71D"></i> Edition des tableaux de bord et des rapports de performances.</p>
                                            </div>
                                        </div>
                                    </div> <!-- end of icon cards area -->
                                    
                                    
                                </div> <!-- end of col-md-8 -->
                                <!-- end of text and icon cards area -->

                                <!-- Image Pane -->
                                <div class="col-md-4">
                                    <img class="img-fluid" src="images/features-iphone-3.png" alt="alternative">
                                </div>
                                <!-- end of image pane -->
                                    
                            </div> <!-- end of row -->
                        </div> <!-- end of container -->
                    </div><!-- end of tab-pane -->
                    <!-- end of tab -->

                </div> <!-- end of tab-content -->
                <!-- end of tabs content -->

            </div> <!-- end of row --> 
        </div> <!-- end of container --> 
    </div> <!-- end of tabs -->
    <!-- end of features -->


    <!-- Video --> 
   <div id="preview" class="basic-1" >
        <div class="container" style="background-color:white">
            <div class="row">
                <div class="col-lg-12"> 
                   <h2 style="color:#FAB71D"> Vidéo de présentation  </h2> 
                    
                </div><br> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Video Preview -->
                    <div class="image-container">
                        <div class="video-wrapper">
                            <a class="popup-youtube" href="" data-effect="fadeIn">
                                <img class="img-fluid" src="images/video-frame.jpg" alt="alternative">
                                <span class="video-play-button">
                                    <span></span>
                                </span>
                            </a>
                        </div> <!-- end of video-wrapper -->
                    </div> <!-- end of image-container -->
                    <!-- end of video preview -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of video -->




    <!-- Details 1 -->
    <div id="details" class="basic-2" style="background-color:#2F4187;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img class="img-fluid" src="images/details-1-iphone.png" alt="alternative">
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container"> 
                        <h3 style="color:#FAB71D"> Bénéfices pour les écoles   </h3>

                        <p>Une école plus compétitive !</p>
                        <p> <span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span> est une application multilingue destinée aux écoles maternelles et primaires, écoles secondaires et établissements d’enseignement supérieur.</p>
                        
                        <a class="btn-solid-reg popup-with-move-anim" href="#details-lightbox-1"> BENEFICES POUR L'ECOLE</a>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 1 -->


    <!-- Details 2 -->
    <div class="basic-3" style="background-color: white">
        <div class="second">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-container">
                            <h3 style="color:#FAB71D"> Bénéfices pour les parents </h3>
                            <p style="color: #2F4187;"> Des parents plus informés, plus impliqués et plus alertes !</p>
                            <a class="btn-solid-reg popup-with-move-anim" href="#details-lightbox-2"> BENEFICES POUR LE PARENT </a>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6">
                        <img class="img-fluid" src="images/details-2-iphone.png" alt="alternative">
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of second -->
    </div> <!-- end of basic-3 -->    
    <!-- end of details 2 -->


    <!-- Details Lightboxes -->
    <!-- Lightbox -->
    <div id="details-lightbox-1" class="lightbox-basic zoom-anim-dialog mfp-hide" style="background-color:#2F4187">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-6">
                <img class="img-fluid" src="images/details-lightbox-1.png" alt="alternative">
            </div>
            <div class="col-lg-6">
                <h4>  Une école plus compétitive ! </h4>
                <hr>
                
            
                    <p class="icon-cell"><i class="fas fa-desktop"></i> Réduction des frais de communication papier (impression, photocopies, logistique de distribution);</p>
                    <p class="icon-cell"><i class="fas fa-bullhorn"></i> Communication continue, personnalisée et en temps réel avec les parents</p>
                    <p class="icon-cell"><i class="fas fa-image"></i> Meilleure expérience d’enseignement par la facilitation du travail des enseignants Réduction de la Redondance des tâches </p>
                    <p class="icon-cell"><i class="fas fa-envelope"></i> Facilitation de la prise de décisions grâce à la centralisation des informations sur une plateforme unique</p>
                    <p class="icon-cell"><i class="fab fa-font-awesome-flag"></i>Automatisation des opérations de reporting</p>
                    <p class="icon-cell"><i class="fas fa-code"></i>Dématérialisation totale documents académiques</p>
                
                 <a class="btn-outline-reg mfp-close as-button" href="#details">FERMER</a> 
            </div>
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->
    
    <!-- Lightbox -->
    <div id="details-lightbox-2" class="lightbox-basic zoom-anim-dialog mfp-hide" style="background-color:#2F4187">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">×</button>
            <div class="col-lg-6">
                <img class="img-fluid" src="images/details-lightbox-2.png" alt="alternative">
            </div>
            <div class="col-lg-6">
                <h5> Des parents plus informés, plus impliqués et plus alertes !</h5>
                <hr>
                
                <table>
                    <tr><td class="icon-cell"><i class="fas fa-desktop"></i></td><td> Suivi amélioré des performances et de la vie scolaire des enfants </td></tr>
                    <tr><td class="icon-cell"><i class="fas fa-bullhorn"></i></td><td> Meilleure adéquation entre le l’accompagnement à domicile et le programme des enseignants pour l’acquisition des connaissances</td></tr>
                    <tr><td class="icon-cell"><i class="fas fa-image"></i></td><td>Réduction des ruptures de communication et aléas consécutifs aux déplacements</td></tr>
                    <tr><td class="icon-cell"><i class="fas fa-envelope"></i></td><td> Accès aux performances des enfants en temps réel</td></tr>
                    
                </table>
                <a class="btn-outline-reg mfp-close as-button" href="#details">Retour</a>
            </div>
        </div> <!-- end of row -->
    </div> <!-- end of lightbox-basic -->
    <!-- end of lightbox -->
    <!-- end of details lightboxes -->


    <!-- Screenshots -->
    <div class="slider-2" style="background-color:#2F4187;">

        <div class="container">


            <div class="row">
                <div class="col-lg-12">

                    <!-- Image Slider -->
                    <div class="slider-container">
                        <div class="swiper-container image-slider">
                            <div class="swiper-wrapper">
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-1.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-1.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-2.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-2.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-3.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-3.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-4.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-4.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-5.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-5.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-6.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-6.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-7.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-7.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-8.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-8.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-9.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-9.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-10.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-10.png" alt="alternative">
                                    </a>
                                </div>
                                <!-- end of slide -->
                                
                            </div> <!-- end of swiper-wrapper -->

                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->

                        </div> <!-- end of swiper-container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of image slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-2 -->

    <!-- Statistics -->
    <div class="counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <h2  style="color:#FAB71D "> Ils nous ont fait confiance  </h2> <br>
                    
                    <!-- Counter -->
                    <div id="counter">
                        <div class="cell">
                            <div class="counter-value number-count" data-count="310">1</div>
                            <p class="counter-info" style="color:#FAB71D ">ECOLES</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="8500">1</div>
                            <p class="counter-info" style="color:#FAB71D ">ELEVES</p>
                        </div>
                        <div class="cell">
                            <div class="counter-value number-count" data-count="359">1</div>
                            <p class="counter-info" style="color:#FAB71D ">PARENTS</p>
                        </div>

                    
                        
                    </div>
                    <!-- end of counter -->
                    
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of counter -->
    <!-- end of statistics -->

    <!-- Testimonials -->
    <div class="slider-1" style="background-color:white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                      <h2  style="color:#FAB71D;text-align:center;"> Témoignages  </h2> <br>

                    <!-- Card Slider -->
                    <div class="slider-container">
                        <div class="swiper-container card-slider">
                            <div class="swiper-wrapper">
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="images/testimonial-1.jpg" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text" style="color:#2F4187">Une application solide et stable qui répond au besoin de suivi l’enseignement au sein des établissements scolaires de notre réseau. Merci à vos équipes pour leur réactivité !.</p>
                                            <p class="testimonial-author" style="color:#FAB71D;font-size:13px;">Valerie Zoudiet Coulibaly, Directrice d’ISBA (International Bilingal Schools of Africa).</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="images/testimonial-2.jpg" alt="alternative">
                                        <div class="card-body" style="color:#2F4187"><span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span> provides all in one platform for students and parents and also for school, gives best solution for communication with all data maintaining on one place. Very supportive team. Through it help, <span style="color:#575756">X</span><span style="color:#FAB71D">school’ink</span> become understandable and very easy to handle.</p>
                                            <p class="testimonial-author" style="color:#FAB71D;font-size:13px;">André Check, parent d’élève.</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="images/testimonial-3.jpg" alt="alternative">
                                        <div class="card-body" >
                                            <p class="testimonial-text" style="color:#2F4187; border-radius:20%">Nous n'avons pas à nous soucier de nos données, chaque information est disponible sur une plateforme unique, à partir de nos téléphones et nos tablettes. Facilité d'utilisation, explicite, meilleure évolution vers les écoles numériques.</p>
                                            <p class="testimonial-author" style="color:#FAB71D;font-size:13px;">Justin Kouadio, Enseignant d’ISBA.</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                
                            </div> <!-- end of swiper-wrapper -->
        
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->
        
                        </div> <!-- end of swiper-container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of card slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-1 -->
    <!-- end of testimonials -->


    <!-- Contact -->
    <div id="contact" class="form" style="background-color:#2F4187">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 style="color:#FAB71D"> Commandez une démo  </h2>
                    
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6 offset-lg-3"><br>

                    <form>
                          <div class="form-row">
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Pays">
                            </div>
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Ville">
                            </div>
                          </div> <br>

                          <div class="form-row">
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Nom de l'établissement">
                            </div>
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Nom">
                            </div>
                          </div> <br>

                           <div class="form-row">
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Téléphone">
                            </div>
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Email">
                            </div>
                          </div> <br>

                           <div class="form-row">
                           
                            <textarea class="form-control"  required placeholder="Votre méssage"></textarea>
                        
                          </div> <br>


                          <div class="form-row">


                            <button type="submit" class="form-control-submit-button" style="background-color:#FAB71D">ENVOYEZ </button>
                           
                          </div> <br>

                    </form>
            
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->

            <div class="row">

                <div class="col"> <p> <span style="color: #FAB71D">  Cameroun </span> <br> +237 695955064 <br> +237  677427931 </p> </div>
                <div class="col"> <p> <span style="color: #FAB71D"> Burkina Faso </span>   <br> +226 77600139 <br> +226 76600139 </p> </div>
                <div class="col"> <p>  <span style="color: #FAB71D"> Côte d'Ivoire</span>  <br> +225 40207053  </p> </div>
                <div class="col"> <p>  <span style="color: #FAB71D"> Burkina Faso  </span> <br> +226 77600139 <br> +226 76600139 </p> </div>
                <div class="col"> <p> <span style="color: #FAB71D"> Congo   </span><br> +242 066664144 <br> +242 055233623 </p> </div>
                <div class="col"> <p> <span style="color: #FAB71D">  RDC </span>  <br> +243 997353563 </p> </div>
                <div class="col"> <p>  <span style="color: #FAB71D">  Burkina Faso   </span> <br> +243 997353563 </p> 
                </div>


            </div>

           
        </div> <!-- end of container -->
    </div> <!-- end of form -->
    <!-- end of contact -->


   
    <!-- Footer -->

    <div class="footer">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <div class="footer-col">

                        <h4> <span style="color:#575756">X</span><span style="color:#FAB71D">school'ink</span> </h4>

                        

                    </div>

                </div> <!-- end of col -->

                <div class="col-md-4">

                    <div class="footer-col middle">

                        <h4 > Liens Importants </h4>

                        <ul class="list-unstyled li-space-lg">

                            <li class="media">

                                <i class="fas fa-square"></i>

                                <div class="media-body"><a class="turquoise" href="http://xschool.proximity-cm.com">http://xschool.proximity-cm.com</a></div>

                            </li>

                            <li class="media">

                                <i class="fas fa-square"></i>

                                <div class="media-body"><a class="turquoise" href="terms-conditions.html">Conditions d'utilisation</a>, <a class="turquoise" href="privacy-policy.html">Politique de confidentialité</a></div>

                            </li>

                        </ul>

                    </div>

                </div> <!-- end of col -->

                <div class="col-md-4">

                    <div class="footer-col last">

                         <h4> Réseaux sociaux  </h4>

                        <span class="fa-stack">

                            <a href="https://www.facebook.com/Proximity-SA-168936656574928" target=_blank>

                               
                                <i class="fab fa-facebook-f fa-stack-1x" style="color: white"></i>

                            </a>

                        </span>

                        <span class="fa-stack">

                            <a href="https://twitter.com/ProximitySA" target=_blank>

                                

                                <i class="fab fa-twitter fa-stack-1x" style="color: white"></i>

                            </a>

                        </span>

                        <span class="fa-stack">

                            <a href="https://www.linkedin.com/company/proximity-sa/" target=_blank>

                               

                                <i class="fab fa-linkedin-in fa-stack-1x" style="color: white"></i>

                            </a>

                        </span>

                    </div> 

                </div> <!-- end of col -->

            </div> <!-- end of row -->

        </div> <!-- end of container -->

    </div> <!-- end of footer -->  

    <!-- end of footer -->




    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small"> Copyright © 76 AVENUE DE L'INDÉPENDANCE, BP 4791 DOUALA CAMEROUN  <a href="http://www.inovatik.com">  </a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
        
    <!-- Scripts -->

    <script type="text/javascript">
        

           function ChangeColor() {

           }


    </script>

    

    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/morphext.min.js"></script> <!-- Morphtext rotating text in the header -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->

    <script type="text/javascript">

        $(document).ready(function() {

             $('.carousel').carousel({

            pause:"false"
        })          
    });

function addFrench()
{
  var etape=1;
  var lang="fr";
  $.ajax({
    url: 'langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang, 
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}

function addEnglish()
{
  var etape=1;
  var lang="en";
  $.ajax({
    url: 'langue.php',
    type: 'POST',
    async:false,
    data: 'etape=' + etape+ '&lang=' +lang,
    dataType: 'text',
    success: function (content, statut) {

window.location.reload();

    }
  });
}
       

    </script>
</body>
</html>