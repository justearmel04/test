<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../class/Fileuploader.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
$libellecourse=htmlspecialchars($_POST['libellecourse']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$durationcourse=htmlspecialchars($_POST['durationcourse']);
$detailscourse=htmlspecialchars($_POST['detailscourse']);
$concatmatiere=htmlspecialchars($_POST['matclasse']);
$tabmatiere=explode("-",$concatmatiere);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));
$libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

// insertion dans la table courses

$courseid=$etab->Addcourses($libellecourse,$datecourse,$matiereid,$teatcherid,$classeEtab,$codeEtab,$sessionEtab,$detailscourse,$durationcourse);

//gestion des sections
$concatsection=htmlspecialchars($_POST['concatsection']);
$tabsection=explode("@",$concatsection);
$nbsection=count($tabsection)-1;

if($nbsection>0)
{
  for($i=0;$i<$nbsection;$i++)
  {
    $section=htmlspecialchars($_POST['section_'.$tabsection[$i]]);

    $etab->AddCourseSection($section,$courseid);
  }
}

//gestion des competences

$concatcomp=htmlspecialchars($_POST['concatcomp']);
$tabcomp=explode("@",$concatcomp);
$nbcomp=count($tabcomp)-1;

if($nbcomp>0)
{
  for($i=0;$i<$nbcomp;$i++)
  {
    $competence=htmlspecialchars($_POST['comp_'.$tabcomp[$i]]);

    $etab->AddCourseComp($competence,$courseid);
  }
}

//gestion des exercice

$concattaches=htmlspecialchars($_POST['concattaches']);
$tabtache=explode("@",$concattaches);
$nbtache=count($tabtache)-1;

if($nbtache>0)
{
  for($i=0;$i<$nbtache;$i++)
  {
    $tache=htmlspecialchars($_POST['taches_'.$tabtache[$i]]);

    $etab->AddCourseHomework($tache,$courseid);
  }
}

//gestion du support de cours

$fileError = $_FILES['fichier3']['error'];
if($fileError==0)
{
  //fichier uploader
  $file_name = @$_FILES['fichier3']['name'];

  $_SESSION["fichier3"] = $file_name;

  $file_size =@$_FILES['fichier3']['size'];

  $file_tmp =@$_FILES['fichier3']['tmp_name'];

  $file_type=@$_FILES['fichier3']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

  $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

  $fichierad=$transactionId.".".$file_ext;

  move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

  $dossier="../courses/".$libelleclasse."/";

  $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

  $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

  if(!is_dir($dossier)) {
        //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
        @mkdir($dossier);
        @mkdir($dossier1);
        @mkdir($dossier2);

            }else
            {

       @mkdir($dossier1);
       @mkdir($dossier2);

            }

      @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                 //Suppression du fichier se trouvant dans le dossier temp

      @unlink("../temp/" . $fichierTemp);

      //nous allons mettre à jour le support du cours

      $etab->UpdatecoursesFilesupport($fichierad,$courseid);


}else {
  // fichier non uploader
}




 // $_SESSION['user']['addclasseok']="Un nouveau cours a été ajouté avec succès";
 $_SESSION['user']['addclasseok']=L::AddcourseMessageSuccess;


 if($_SESSION['user']['profile'] == "Teatcher") {

     // header("Location:../teatcher/listcourses.php");

     $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
                 $typeetab=$etab->DetermineTypeEtab($codeEtab);
                 $IBSAschool=$etab->getIbsaschools();
                 $codeetabs="";
                 foreach ($IBSAschool as  $value):
                   $codeetabs=$codeetabs.$value->code_etab."*";
                 endforeach;
                 $tabetabs=explode("*",$codeetabs);

                 $nbdispenser=0;

                 $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

                 foreach ($dispenserdatas as $value):
                   $code=$value->codeEtab;
                   if (in_array($code, $tabetabs)) {
                     $nbdispenser++;
                    }
                 endforeach;

                 echo $nbdispenser;

                 header("Location:../teatcher".$libelleEtab."/listcourses.php");

                 // if($nbdispenser>0)
                 // {
                 //   header("Location:../teatcher".$libelleEtab."/listcourses.php");
                 // }else {
                 //   header("Location:../teatcher/listcourses.php");
                 // }






     }

}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  $courseid=htmlspecialchars($_POST['courseid']);
  $libellecourse=htmlspecialchars($_POST['libellecourse']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $durationcourse=htmlspecialchars($_POST['durationcourse']);
  $detailscourse=htmlspecialchars($_POST['detailscourse']);
  $concatmatiere=htmlspecialchars($_POST['matclasse']);
  $tabmatiere=explode("-",$concatmatiere);
  $matiereid=$tabmatiere[0];
  $teatcherid=$tabmatiere[1];
  $datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));

  $libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

  $fileError = $_FILES['fichier3']['error'];
  if($fileError==0)
  {
    // echo "fichier uploadé";

    $file_name = @$_FILES['fichier3']['name'];

    $_SESSION["fichier3"] = $file_name;

    $file_size =@$_FILES['fichier3']['size'];

    $file_tmp =@$_FILES['fichier3']['tmp_name'];

    $file_type=@$_FILES['fichier3']['type'];

    @$file_ext=strtolower(end(explode('.',@$_FILES['fichier3']['name'])));

    $fichierTemp = uniqid() . "." . $file_ext;

 $etab->UpdateCoursesDetails($libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab);

 $oldfilename=$etab->getCoursesFileName($courseid,$teatcherid,$codeEtab,$sessionEtab);

 if(strlen($oldfilename)>0)
 {
@unlink("../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$oldfilename);

$transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

$fichierad=$transactionId.".".$file_ext;

   move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

   $dossier="../courses/".$libelleclasse."/";

   $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

   $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }

       @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                  //Suppression du fichier se trouvant dans le dossier temp

       @unlink("../temp/" . $fichierTemp);

       //nous allons mettre à jour le support du cours

       $etab->UpdatecoursesFilesupport($fichierad,$courseid);
 }else {
   $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

   $fichierad=$transactionId.".".$file_ext;

   move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

   $dossier="../courses/".$libelleclasse."/";

   $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

   $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }

       @rename('../temp/' . $fichierTemp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

                  //Suppression du fichier se trouvant dans le dossier temp

       @unlink("../temp/" . $fichierTemp);

       //nous allons mettre à jour le support du cours

       $etab->UpdatecoursesFilesupport($fichierad,$courseid);
 }

  }else {
    // echo "fichier non uploadé";

    $etab->UpdateCoursesDetails($libellecourse,$detailscourse,$durationcourse,$classeEtab,$matiereid,$datecourse,$courseid,$teatcherid,$codeEtab,$sessionEtab);

  }

  // $_SESSION['user']['addclasseok']="Le cours a été modifié avec succès";
  $_SESSION['user']['addclasseok']=L::ModcourseMessageSuccess ;


  if($_SESSION['user']['profile'] == "Teatcher") {

      // header("Location:../teatcher/listcourses.php");

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
                  $typeetab=$etab->DetermineTypeEtab($codeEtab);
                  $IBSAschool=$etab->getIbsaschools();
                  $codeetabs="";
                  foreach ($IBSAschool as  $value):
                    $codeetabs=$codeetabs.$value->code_etab."*";
                  endforeach;
                  $tabetabs=explode("*",$codeetabs);

                  $nbdispenser=0;

                  $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

                  foreach ($dispenserdatas as $value):
                    $code=$value->codeEtab;
                    if (in_array($code, $tabetabs)) {
                      $nbdispenser++;
                     }
                  endforeach;

                  echo $nbdispenser;

                  header("Location:../teatcher".$libelleEtab."/listcourses.php");

                  // if($nbdispenser>0)
                  // {
                  //   header("Location:../teatcher".$libelleEtab."/listcourses.php");
                  // }else {
                  //   header("Location:../teatcher/listcourses.php");
                  // }






      }

}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{




  // initialize FileUploader
      $FileUploader = new FileUploader('files', array(
          'uploadDir' => '../temp/',
      ));

      $data = $FileUploader->upload();

      $libellecourse=htmlspecialchars($_POST['libellecourse']);
      $classeEtab=htmlspecialchars($_POST['classeEtab']);
      $codeEtab=htmlspecialchars($_POST['codeEtab']);
      $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
      $durationcourse=htmlspecialchars($_POST['durationcourse']);
      $detailscourse=htmlspecialchars($_POST['detailscourse']);
      $concatmatiere=htmlspecialchars($_POST['matclasse']);
      $tabmatiere=explode("-",$concatmatiere);
      $matiereid=$tabmatiere[0];
      $teatcherid=$tabmatiere[1];
      $datecourse=dateFormat(htmlspecialchars($_POST['datecourse']));

      $libellematiere=$etab->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      $libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);

      // insertion dans la table courses

      $courseid=$etab->Addcourses($libellecourse,$datecourse,$matiereid,$teatcherid,$classeEtab,$codeEtab,$sessionEtab,$detailscourse,$durationcourse);

      //gestion des sections
      $concatsection=htmlspecialchars($_POST['concatsection']);
      $tabsection=explode("@",$concatsection);
      $nbsection=count($tabsection)-1;

      if($nbsection>0)
      {
        for($i=0;$i<$nbsection;$i++)
        {
          $section=htmlspecialchars($_POST['section_'.$tabsection[$i]]);

          $etab->AddCourseSection($section,$courseid);
        }
      }

      //gestion des competences

      $concatcomp=htmlspecialchars($_POST['concatcomp']);
      $tabcomp=explode("@",$concatcomp);
      $nbcomp=count($tabcomp)-1;

      if($nbcomp>0)
      {
        for($i=0;$i<$nbcomp;$i++)
        {
          $competence=htmlspecialchars($_POST['comp_'.$tabcomp[$i]]);

          $etab->AddCourseComp($competence,$courseid);
        }
      }

      //gestion des exercice

      $concattaches=htmlspecialchars($_POST['concattaches']);
      $tabtache=explode("@",$concattaches);
      $nbtache=count($tabtache)-1;

      if($nbtache>0)
      {
        for($i=0;$i<$nbtache;$i++)
        {
          $tache=htmlspecialchars($_POST['taches_'.$tabtache[$i]]);

          $etab->AddCourseHomework($tache,$courseid);
        }
      }





      if($data['isSuccess'] && count($data['files']) > 0) {



         $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematiere, 0, 3)).$courseid;

        $dossier="../courses/".$libelleclasse."/";

        $dossier1="../courses/".$libelleclasse."/".$datecourse."/";

        $dossier2="../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/";

        if(!is_dir($dossier)) {
              //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
              @mkdir($dossier);
              @mkdir($dossier1);
              @mkdir($dossier2);

                  }else
                  {

             @mkdir($dossier1);
             @mkdir($dossier2);

                  }


        $datas=$data['files'];
        $i=1;
      foreach ($datas as $value) {

        var_dump($value);
        // echo $value['date'];

        $file_ext=$value['extension'];
        $file_tmp=$value['file'];
        $typesupport="COURSES";

        $fichierad=$transactionId.$i.".".$file_ext;

        @rename($file_tmp , "../courses/".$libelleclasse."/".$datecourse."/".$libellematiere."/".$fichierad);

             //insertion dans la base de données

             $etab->Addsupports($courseid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);


                   //Suppression du fichier se trouvant dans le dossier temp

        @unlink($file_tmp);

        $i++;
      }

      // $_SESSION['user']['addclasseok']="Un nouveau cours a été ajouté avec succès";

     }


     $_SESSION['user']['addclasseok']=L::AddcourseMessageSuccess;

     if($_SESSION['user']['profile'] == "Teatcher") {

         // header("Location:../teatcher/listcourses.php");

         $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
                     $typeetab=$etab->DetermineTypeEtab($codeEtab);
                     $IBSAschool=$etab->getIbsaschools();
                     $codeetabs="";
                     foreach ($IBSAschool as  $value):
                       $codeetabs=$codeetabs.$value->code_etab."*";
                     endforeach;
                     $tabetabs=explode("*",$codeetabs);

                     $nbdispenser=0;

                     $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

                     foreach ($dispenserdatas as $value):
                       $code=$value->codeEtab;
                       if (in_array($code, $tabetabs)) {
                         $nbdispenser++;
                        }
                     endforeach;

                     echo $nbdispenser;

                     header("Location:../teatcher".$libelleEtab."/listcourses.php");

                     // if($nbdispenser>0)
                     // {
                     //   header("Location:../teatcher".$libelleEtab."/listcourses.php");
                     // }else {
                     //   header("Location:../teatcher/listcourses.php");
                     // }






         }





}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{



}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{

  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $courseid=htmlspecialchars($_POST['coursesid']);

  //nous allons recuperer les informations du cours

  $coursesdetails=$etab->getAllcoursesdetailsofClasses($courseid,$classeEtab,$codeEtab,$sessionEtab);



  // var_dump($coursesdetails);
  foreach ($coursesdetails as  $datacourses):
    $descricourses=$datacourses->descri_courses;
    $durationcourses=$datacourses->duree_courses;
    $teatchercourses=$datacourses->nom_compte;
    $datercourses=$datacourses->date_courses;
    $namecourses=$datacourses->libelle_courses;
    $classecourses=$datacourses->libelle_classe;
    $classeidcourses=$datacourses->id_classe;
    $matiereidcourses=$datacourses->id_mat;
    $libellematcourses=$datacourses->libelle_mat;
    $statutcourses=$datacourses->statut_courses;
    $filescourses=$datacourses->support_courses;
    $discussion_courses=$datacourses->discussion_courses;
    $teatcherid_courses=$datacourses->teatcher_courses;

  endforeach;

  $FileUploader = new FileUploader('files', array(
      'uploadDir' => '../temp/',
  ));

  $data = $FileUploader->upload();



  if($data['isSuccess'] && count($data['files']) > 0) {

    $transactionId =  "COURSES".date("Y") . date("m").strtoupper(substr($libellematcourses, 0, 3)).$courseid;

   $dossier="../courses/".$classecourses."/";

   $dossier1="../courses/".$classecourses."/".$datercourses."/";

   $dossier2="../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/";

   if(!is_dir($dossier)) {
         //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation
         @mkdir($dossier);
         @mkdir($dossier1);
         @mkdir($dossier2);

             }else
             {

        @mkdir($dossier1);
        @mkdir($dossier2);

             }


             $datas=$data['files'];
             $typecourses="COURSES";
             //nous allons rechercher le dernier support de ce cours
             $lastidsupport=$etab->getLastsupportFilesCourses($courseid,$codeEtab,$sessionEtab,$classeEtab,$typecourses,$libellematcourses);
             echo $lastidsupport;
             $i=1;
             foreach ($datas as $value) {

               var_dump($value);
               // echo $value['date'];
               $somme=$lastidsupport+$i;

               $file_ext=$value['extension'];
               $file_tmp=$value['file'];
               $typesupport="COURSES";

               // $fichierad=$transactionId.$i.".".$file_ext;

               $fichierad=$transactionId.$somme.".".$file_ext;

               @rename($file_tmp , "../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$fichierad);

                    //insertion dans la base de données

                    $etab->Addsupports($courseid,$matiereidcourses,$teatcherid_courses,$codeEtab,$sessionEtab,$classeEtab,$fichierad,$typesupport);

                    echo "../courses/".$classecourses."/".$datercourses."/".$libellematcourses."/".$fichierad;
                          //Suppression du fichier se trouvant dans le dossier temp

               @unlink($file_tmp);

               $i++;
             }

             $_SESSION['user']['addclasseok']=L::AddsupportcourseMessageSuccess;

             if($_SESSION['user']['profile'] == "Teatcher") {


              $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
              $typeetab=$etab->DetermineTypeEtab($codeEtab);
              $IBSAschool=$etab->getIbsaschools();
              $codeetabs="";
              foreach ($IBSAschool as  $value):
                $codeetabs=$codeetabs.$value->code_etab."*";
              endforeach;
              $tabetabs=explode("*",$codeetabs);

              $nbdispenser=0;

              $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

              foreach ($dispenserdatas as $value):
                $code=$value->codeEtab;
                if (in_array($code, $tabetabs)) {
                  $nbdispenser++;
                 }
              endforeach;

              echo $nbdispenser;

                 header("Location:../teatcher".$libelleEtab."/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);

              // if($nbdispenser>0)
              // {
              //   // header("Location:../teatcher".$libelleEtab."/listdevoirs.php");
              //    header("Location:../teatcher".$libelleEtab."/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);
              //
              // }else {
              //   // header("Location:../teatcher/listdevoirs.php");
              //    header("Location:../teatcher/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);
              // }


               /*  header("Location:../teatcher/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);*/

               }else  if($_SESSION['user']['profile'] == "Admin_locale") {

                 $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
                 $typeetab=$etab->DetermineTypeEtab($codeEtab);

                 if($typeetab==5)
                 {
                     header("Location:../locale".$libelleEtab."/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab."&teatcherid=".$teatcherid_courses);
                 }else {
                     header("Location:../locale/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab."&teatcherid=".$teatcherid_courses);
                 }


               }


  }

  if($data['hasWarnings']) {

    // $_SESSION['user']['addclasseok']="Le nouveau cours n'a pas été ajouté avec succès";
    $_SESSION['user']['addclasseok']=L::AddsupportcourseMessageUnSuccess;

    if($_SESSION['user']['profile'] == "Teatcher") {

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
            $typeetab=$etab->DetermineTypeEtab($codeEtab);
            $IBSAschool=$etab->getIbsaschools();
            $codeetabs="";
            foreach ($IBSAschool as  $value):
              $codeetabs=$codeetabs.$value->code_etab."*";
            endforeach;
            $tabetabs=explode("*",$codeetabs);

            $nbdispenser=0;

            $dispenserdatas=$etab->getdispenserTeatcher($_SESSION['user']['IdCompte']);

            foreach ($dispenserdatas as $value):
              $code=$value->codeEtab;
              if (in_array($code, $tabetabs)) {
                $nbdispenser++;
               }
            endforeach;

            echo $nbdispenser;

              header("Location:../teatcher".$libelleEtab."/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);

            // if($nbdispenser>0)
            // {
            //   header("Location:../teatcher".$libelleEtab."/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);
            // }else {
            //   header("Location:../teatcher/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);
            // }






        // header("Location:../teatcher/updatecourses.php?courseid=".$courseid."&classeid=".$classeEtab);

        }
 }



}







 ?>
