<?php
session_start();

require_once('../class/Tache.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$tachex = new Tache();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{

  //recupération des variables
  $libelletache=htmlspecialchars(addslashes($_POST['tache']));
  $description=htmlspecialchars(addslashes($_POST['desc']));
  $addby=htmlspecialchars(addslashes($_POST['addby']));
  $deadline=htmlspecialchars(addslashes($_POST['datelimite']));
  $deadline=dateFormat($deadline);
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['session']));
  $dateday=date("Y-m-d");
  $status=0;


  $tachex->Addtache($libelletache,$description,$deadline,$status, $dateday,$codeEtab,$sessionEtab,$addby);

  $_SESSION['user']['addsalleok']=L::Tachesaddsuccessfully;

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localecmr/taches.php");
      // }else {
      //   header("Location:../localeIBSA/taches.php");
      // }

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/taches.php");
      }else {
        header("Location:../locale/taches.php");
      }


}
}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //ajout du diplôme

  //recupération des variables
  $libelletache=htmlspecialchars(addslashes($_POST['tache']));
  $description=htmlspecialchars(addslashes($_POST['desc']));
  $deadline=htmlspecialchars(addslashes($_POST['datelimite']));
  $deadline=dateFormat($deadline);
  $codeEtab=htmlspecialchars(addslashes($_POST['codeetab']));
  $idtache=htmlspecialchars(addslashes($_POST['classeid']));

  //
  $tachex->Updatetache($libelletache,$description,$deadline,$codeEtab,$idtache);

  $_SESSION['user']['addsalleok']=L::TachesModsuccessfully;

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localecmr/taches.php");
      // }else {
      //   header("Location:../localeIBSA/taches.php");
      // }

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/taches.php");
      }else {
        header("Location:../locale/taches.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recupération des variables

  $tacheid=htmlspecialchars(addslashes($_POST['idtache']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
   $assign=htmlspecialchars(addslashes($_POST['assign']));
   $sessionEtab=htmlspecialchars(addslashes($_POST['session']));
   $status=1;
  //
  // $tachex->Assigntaches($tacheid,$codeEtab,$assign);
  $tachex->AssignatedUsertotask($status,$assign,$tacheid,$codeEtab,$sessionEtab);
  //$tachex->Assignedtache($tacheid,$codeEtab,$assigned);

  $_SESSION['user']['addsalleok']=L::TachesAssignersuccessfully;

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //  header("Location:../localecmr/taches.php");
      // }else {
      //   header("Location:../localeIBSA/taches.php");
      // }

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/taches.php");
      }else {
        header("Location:../locale/taches.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //ajout du diplôme

  //recupération des variables

  $idtache=htmlspecialchars(addslashes($_GET['compte']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));

  //
  $tachex->Deletedtache($idtache,$codeEtab);

  $_SESSION['user']['addsalleok']=L::Tachesdeletesuccessfully;

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      // if($_SESSION['user']['paysid']==4)
      // {
      //   header("Location:../localecmr/taches.php");
      // }else {
      //   header("Location:../localeIBSA/taches.php");
      // }

      $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);
      $typeetab=$etab->DetermineTypeEtab($codeEtab);

      if($typeetab==5)
      {
        header("Location:../locale".$libelleEtab."/taches.php");
      }else {
        header("Location:../locale/taches.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}

/*if(isset($_GET['etape'])&&($_GET['etape']==4))
{
  //ajout du diplôme

  //recupération des variables

  $idtache=htmlspecialchars(addslashes($_GET['compte']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeetab']));
   $assign=htmlspecialchars(addslashes($_GET['assign']));

  //
  $tachex->Assigntache($idtache,$codeEtab);
  $tachex->Assignedtache($idtache,$codeEtab);

  $_SESSION['user']['addsalleok']="Tache assignée avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {

    header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
// header("Location:../manager/addsalle.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/taches.php");
      }else {
        header("Location:../localeIBSA/taches.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}
*/

?>
