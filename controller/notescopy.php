<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/Student.php');
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$student=new Student();
$etabs=new Etab();
$matierestud= new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //il est question des notes de controle

  //reciprération des variables
  // $Note28: 15
  // $obser28: TRES BIEN
  // $studentmat: 28*
  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  $datepiste=date("Y-m-d");
  $controlepiste=1;
  $reglementpiste=0;
  $examenpiste=0;
  $actionpiste=1;
  $tabNameofstudents="";

  //nous allons verifier si nous sommes dans le cas d'un etablissement primaire ou secondaire

  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  if($etablissementType==1||$etablissementType==3)
  {


    if($_SESSION['user']['paysid']==4)
    {
      // cas d'un etablissement primaire ou maternelle au cameroun

            //nous allons rechercher le coefficient du controle
          $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle);
          $tabdata=explode("*",$dataNotes);
          $coefficientNotes=$tabdata[0];
          $typesessionNotes=$tabdata[1];

          $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
          $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
          $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

          //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere
          //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere

            $nbnotessemesterclasses=$etabs->DeterminationOfNotesNumberForThisclasses($matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes,$classeId);

            if($nbnotessemesterclasses==0)
            {
              //c'est la première notes que nous ajoutons pour cette classe pour ce semetre

              for($i=0;$i<$nbstudent;$i++)
              {

                $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
                $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
                //insertion dans la table note

                $ideleve=$tabstudent[$i];
                $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
                $studentData=explode("*",$studentDatas);
                $nomcompletStudent=$studentData[2]." ".$studentData[3];
                $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";
                //$libelleclasse=$studentData[9];

                $notescoef=$notes*$coefficientNotes;

                $moyennestudent=$notescoef/$coefficientNotes;

                //nous allons ajouter la note dans rating

                $etabs->AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);

                //insertion dans la table pisteNotes
                $firstnote=1;

                $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

                //retrouver les informations du parent à savoir son email et son contact

                $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

                $tabdatadestinataires="Parent";
                //nous allons rechercher l'email du parent
                // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
                $dataParents=$student->getEmailParentOfThisStudentBoucle($ideleve);
                $ka=1;
                foreach ($dataParents as $parents):

                  $destimails=$parents->email_parent;
                  $destiphone=$indicatifEtab.$parents->tel_parent;
                  //sms mis en commentaire
                  $etabs->Addnotecontrolessmssending($destiphone,$libellematiere,$notes,$nomcompletStudent);

                  $ka++;
                endforeach;

                //$etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);

                $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);
                //envoi de la note de l'eleve par sms



                // echo $observation;
              }


            }else if($nbnotessemesterclasses>0)
            {

              for($i=0;$i<$nbstudent;$i++)
              {
                $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
                $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
                //insertion dans la table note
                $ideleve=$tabstudent[$i];
                $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
                $studentData=explode("*",$studentDatas);
                $nomcompletStudent=$studentData[2]." ".$studentData[3];

                $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

                //nous allons recuperer le totalnotes,totalnotescoef,totalcoef

                $LastratingInfos=$etabs->getRatingtypesessionInfosNew($ideleve,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$classeId,$typesessionNotes);
                foreach ($LastratingInfos as $values):
             // $tabLastInfos=explode("*",$LastratingInfos);
             $alltotalnotes=$values->totalnotes_rating;
             $alltotalnotescoef=$values->totalnotescoef_rating;
             $totalcoefnotes=$values->totalcoef_rating;
             $ratingId=$values->id_rating;
           endforeach;

                //nouvelles Informations

                $sommesNotes=$alltotalnotes+$notes;
                $sommesNotescoef=$alltotalnotescoef+($notes*$coefficientNotes);
                $sommescoef=$totalcoefnotes+$coefficientNotes;
                $moyenne=$sommesNotescoef/$sommescoef;

                $etabs->UpdateRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

                $firstnote=1;

                $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

              }

            }


    }else {
      // cas etablissement primaire ou maternelle dans un autre pays que le cameroun

      //insertion dans la table piste

      $idpiste=$etabs->AddpistePrimary($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));
      $destiphone="";

      //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere

      for($i=0;$i<$nbstudent;$i++)
     {
        $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        //nous allons verifier pour voir si l'eleve a deja une note pour ce controle precis

        //nous allons donc ajouter la note dans le système


      $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
      $studentData=explode("*",$studentDatas);
      $nomcompletStudent=$studentData[2]." ".$studentData[3];
      $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

      //insertion dans la table pisteNotes
      $firstnote=1;
      $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

      //retrouver les informations du parent à savoir son email et son contact

      $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

      $tabdatadestinataires="Parent";
      //nous allons rechercher l'email du parent
      // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$ideleve);
      $dataParents=$student->getEmailParentOfThisStudentBoucle($ideleve);
      $ka=1;
      foreach ($dataParents as $parents):

        $destimails=$parents->email_parent;
        $destiphone=$indicatifEtab.$parents->tel_parent;
          //sms mis en commentaire
        $etabs->Addnotecontrolessmssending($destiphone,$libellematiere,$notes,$nomcompletStudent);
      $ka++;
      endforeach;

        $nbnotes=$student->DetermineNoteNumbercontroles($tabstudent[$i],$classeId,$matiereid,$idtypenote,$codeEtab);
        if($nbnotes==0)
        {
          //cet eleve n'a pas deja de note enregistrer pour ce controle





        $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

        // echo $observation;


        }else if($nbnotes>0)
        {
          //cet eleve à deja une note enregistrer pour ce controle

          //nous allons donc modifier la note de cet eleve pour ce controle

          $student->UpdatenotesPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
        }

        // $etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);


     }


    }

    $etabs->RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes);


  }else
  {
    //cas etablissement secondaire

      //nous allons rechercher le coefficient du controle
    $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle);
    $tabdata=explode("*",$dataNotes);
    $coefficientNotes=$tabdata[0];
    $typesessionNotes=$tabdata[1];

    $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
    $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
    $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

    //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere
    //nous allons voir si nous avons au moins notes pour ce pour les eleves de cette classe pour ce semestre et pour cette matiere

      $nbnotessemesterclasses=$etabs->DeterminationOfNotesNumberForThisclasses($matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes,$classeId);

      if($nbnotessemesterclasses==0)
      {
        //c'est la première notes que nous ajoutons pour cette classe pour ce semetre

        for($i=0;$i<$nbstudent;$i++)
        {

          $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
          $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
          //insertion dans la table note

          $ideleve=$tabstudent[$i];
          $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
          $studentData=explode("*",$studentDatas);
          $nomcompletStudent=$studentData[2]." ".$studentData[3];
          $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";
          //$libelleclasse=$studentData[9];

          $notescoef=$notes*$coefficientNotes;

          $moyennestudent=$notescoef/$coefficientNotes;

          //nous allons ajouter la note dans rating

          $etabs->AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);

          //classement des eleves dans cette matiere




          //insertion dans la table pisteNotes
          $firstnote=1;

          $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

          //retrouver les informations du parent à savoir son email et son contact

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          // $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
          $dataParents=$student->getEmailParentOfThisStudentBoucle($ideleve);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;
              //sms mis en commentaire
            $etabs->Addnotecontrolessmssending($destiphone,$libellematiere,$notes,$nomcompletStudent);

            $ka++;
          endforeach;

          //$etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);

          $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);
          //envoi de la note de l'eleve par sms



          // echo $observation;
        }

        $etabs->RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes);

      }else if($nbnotessemesterclasses>0)
      {

        for($i=0;$i<$nbstudent;$i++)
        {
          $notes=htmlspecialchars(addslashes($_POST['note'.$tabstudent[$i]]));
          $observation=htmlspecialchars(addslashes($_POST['obser'.$tabstudent[$i]]));
          //insertion dans la table note
          $ideleve=$tabstudent[$i];
          $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
          $studentData=explode("*",$studentDatas);
          $nomcompletStudent=$studentData[2]." ".$studentData[3];

          $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

          //nous allons recuperer le totalnotes,totalnotescoef,totalcoef

          $LastratingInfos=$etabs->getRatingtypesessionInfosNew($ideleve,$idtypenote,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$classeId,$typesessionNotes);
          foreach ($LastratingInfos as $values):
            // $tabLastInfos=explode("*",$LastratingInfos);
            $alltotalnotes=$values->totalnotes_rating;
            $alltotalnotescoef=$values->totalnotescoef_rating;
            $totalcoefnotes=$values->totalcoef_rating;
            $ratingId=$values->id_rating;
          endforeach;



          //nouvelles Informations

          $sommesNotes=$alltotalnotes+$notes;
          $sommesNotescoef=$alltotalnotescoef+($notes*$coefficientNotes);
          $sommescoef=$totalcoefnotes+$coefficientNotes;
          $moyenne=$sommesNotescoef/$sommescoef;
          $notescoef=$notes*$coefficientNotes;

          $etabs->UpdateRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

          $firstnote=1;

          $etabs->AddNotesPiste($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

            $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$ideleve,$codeEtab,$notes,$observation,$sessionlibelle);


        }

      }

      $etabs->RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$sessionlibelle,$typesessionNotes);

  }





  // $_SESSION['user']['addattendailyok']="Une nouvelle notes de classe a bien été ajoutée avec succès";
  $_SESSION['user']['addattendailyok']=L::NotesclassesAddedMessageSuccess;



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/notes.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      // header("Location:../localecmr/notes.php");
    }else {

      $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

      if($etablissementType==5)
      {
        $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
        header("Location:../locale".$libelleEtab."/notes.php");

      }else {
        header("Location:../locale/notes.php");
      }

    }


}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question des notes d'examen

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  $datepiste=date("Y-m-d");
  $controlepiste=0;
  $reglementpiste=0;
  $examenpiste=1;
  $actionpiste=1;

  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);

  $tabNameofstudents="";

  if($etablissementType==1||$etablissementType==3)
  {
    //etablissement primaire et maternelle

    if($_SESSION['user']['paysid']==4)
    {
      // etablissement primaire et maternelle dans un pays qui est pas le cameroun

      $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libelleclasse,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {
        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];

        $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

        $etabs->AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

       //retrouver les informations du parent à savoir son email et son contact

       $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

       $tabdatadestinataires="Parent";
     //nous allons rechercher l'email du parent
       $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
       $ka=1;
       foreach ($dataParents as $parents):

         $destimails=$parents->email_parent;
         $destiphone=$indicatifEtab.$parents->tel_parent;


         $ka++;
       endforeach;

       $nbnotes=$student->DetermineNoteNumberexamen($tabstudent[$i],$classeId,$idtypenote,$matiereid,$teatcherid,$codeEtab);

       // echo $nbnotes."<br>";

       if($nbnotes==0)
       {
         //cet eleve n'a pas deja de note enregistrer pour ce controle


     $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

       // echo $observation;


       }else if($nbnotes>0)
       {
         //cet eleve à deja une note enregistrer pour ce controle

         //nous allons donc modifier la note de cet eleve pour ce controle

         $student->UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
       }


      }

      // $etabs->SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab);


    }else {
      // etablissement primaire et maternelle dans un pays qui n'est pas le cameroun

      $idpiste=$etabs->AddpistePrimary($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$sessionlibelle,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
      // echo $idpiste;
      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {
        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        //nous allons verifier pour voir si l'eleve a deja une note pour ce controle precis

        //nous allons donc ajouter la note dans le système

        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];

        $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

        //insertion dans la table pisteNotes
        $firstnote=1;
        $etabs->AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

        //retrouver les informations du parent à savoir son email et son contact

        $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

        $tabdatadestinataires="Parent";
        //nous allons rechercher l'email du parent
        $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$ideleve);
        $ka=1;
        foreach ($dataParents as $parents):

          $destimails=$parents->email_parent;
          $destiphone=$indicatifEtab.$parents->tel_parent;
        $ka++;
        endforeach;


        // $nbnotes=$student->DetermineNoteNumbercontroles($tabstudent[$i],$classeId,$matiereid,$idtypenote,$codeEtab);
        $nbnotes=$student->DetermineNoteNumberexamen($tabstudent[$i],$classeId,$idtypenote,$matiereid,$teatcherid,$codeEtab);

        // echo $nbnotes."<br>";

        if($nbnotes==0)
        {
          //cet eleve n'a pas deja de note enregistrer pour ce controle


      $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

        // echo $observation;


        }else if($nbnotes>0)
        {
          //cet eleve à deja une note enregistrer pour ce controle

          //nous allons donc modifier la note de cet eleve pour ce controle

          $student->UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
        }



      }

      // $etabs->SendAddedControleNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab,$libellematiere);



    }




  }else {
    //etablissement secondaire

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libelleclasse,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

     for($i=0;$i<$nbstudent;$i++)
     {
       $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
       $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
       $ideleve=$tabstudent[$i];

       $studentDatas=$student->getAllInformationsOfStudent($ideleve,$sessionlibelle);
       $studentData=explode("*",$studentDatas);
       $nomcompletStudent=$studentData[2]." ".$studentData[3];

       $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

       $etabs->AddNotesPisteExam($idpiste,$idtypenote,$firstnote,$notes,$ideleve);

      //retrouver les informations du parent à savoir son email et son contact

      $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

      $tabdatadestinataires="Parent";
    //nous allons rechercher l'email du parent
      $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$sessionlibelle,$libellematiere,$codeEtab);
      $ka=1;
      foreach ($dataParents as $parents):

        $destimails=$parents->email_parent;
        $destiphone=$indicatifEtab.$parents->tel_parent;


        $ka++;
      endforeach;

      $nbnotes=$student->DetermineNoteNumberexamen($tabstudent[$i],$classeId,$idtypenote,$matiereid,$teatcherid,$codeEtab);

      // echo $nbnotes."<br>";

      if($nbnotes==0)
      {
        //cet eleve n'a pas deja de note enregistrer pour ce controle


    $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$sessionlibelle);

      // echo $observation;


      }else if($nbnotes>0)
      {
        //cet eleve à deja une note enregistrer pour ce controle

        //nous allons donc modifier la note de cet eleve pour ce controle

        $student->UpdatenoteExamPrimary($ideleve,$idtypenote,$notes,$observation,$classeId,$matiereid,$codeEtab,$teatcherid,$sessionlibelle);
      }


     }

     // $etabs->SendAddedExamenNotesMailerToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$sessionlibelle,$codeEtab);

  }

  // $_SESSION['user']['addattendailyok']="Notes de classe ajouter avec succès";
    $_SESSION['user']['addattendailyok']=L::NotesclassesAddedMessageSuccess;



  if($_SESSION['user']['profile'] == "Admin_globale")

  {

header("Location:../manager/notes.php?codeEtab=".$codeEtab);

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../locale/notes.php");
    }else {
      header("Location:../locale/notes.php");
    }


}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }






}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
  //recupération des variables
  $noteid=htmlspecialchars(addslashes($_POST['noteid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $studentid=htmlspecialchars(addslashes($_POST['studentid']));
  $classeid=htmlspecialchars(addslashes($_POST['classeid']));
  $note=htmlspecialchars(addslashes($_POST['note']));
  $observation=htmlspecialchars(addslashes($_POST['observation']));

  $student->UpdateNotesOfStudent($noteid,$codeEtab,$studentid,$classeid,$note,$observation);

  $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {
header("Location:../manager/notes.php?codeEtab=".$codeEtab);
//header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/notes.php");
    }else {
      header("Location:../locale/notes.php");
    }



}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{
    if($_POST['typenote']==1)
    {
      //il est question d'un controle
      //recuperation des variables

      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
      $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);
      $datepiste=date("Y-m-d");
      $controlepiste=1;
      $reglementpiste=0;
      $examenpiste=0;
      $actionpiste=2;

      $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
      $tabdata=explode("*",$dataNotes);

      $coefficientNotes=$tabdata[0];
      $typesessionNotes=$tabdata[1];

      $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libelleclasse,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);


      //$typesessionNotes=$etabs->getSessionTypeExam($idtypenote,$codeEtab,$libellesession);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
      // $oldnote=htmlspecialchars(addslashes($_POST['oldnote']));

      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      //mise à jour des notes

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        //$libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour ce controle si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservControleNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

         // echo $check."<br>";

        if($check==0)
        {
          //ajout de la note

          $notescoef=$notes*$coefficientNotes;

          $moyennestudent=$notescoef/$coefficientNotes;

          //nous allons ajouter la note dans rating

          $etabs->AddOldRatingStudent($sessionlibelle,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$notes,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);

          $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);

        }else if($check>0)
        {
          //notification aux parents puis modifications de notes

          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          //$etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }
        // echo "bonjour";
        //nous allons recuperer les informations de la table rating (id_rating,	totalnotes_rating,totalnotescoef_rating,	totalcoef_rating,rating)
        $ratinginformations=$etabs->getStudentRatingInfosNew($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

        foreach ($ratinginformations as $values):
          $ratingId=$values->id_rating;
          $totalnotesrating=$values->totalnotes_rating;
          $totalnotescoefrating=$values->totalnotescoef_rating;
          $totalcoefrating=$values->totalcoef_rating;
          $rating=$values->rating;
        endforeach;

        // $tabInfosrating=explode("*",$ratinginformations);

        //var_dump($tabInfosrating);
        //recuperation des informations



        //retrouver la note correspndant a ce controle

        $oldnotes=$etabs->getLastNotescontroleAddedd($ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

        //nouvelle operation

        // echo $oldnotes."<br>";

        $sommesNotes=($totalnotesrating-$oldnotes)+$notes;
        $sommesNotescoef=$totalnotescoefrating-($oldnotes*$coefficientNotes)+($notes*$coefficientNotes);
        $sommescoef=$totalcoefrating;
        $moyenne=$sommesNotescoef/$totalcoefrating;

        echo "ancienne informations<br>";
        echo $totalnotesrating." ".$totalnotescoefrating." ".$totalcoefrating." ".$rating."<br>";
        echo "nouvelle informations<br>";
        echo $sommesNotes." ".$sommesNotescoef." ".$sommescoef." ".$moyenne." ".$ratingId." ".$oldnotes." <br>";
        echo "calcul<br>";
        echo $sommesNotescoef/$totalcoefrating."<br>";

        //insertion dans la tables pisteNotes

        //$etabs->UpdatepisteNotes();

        $etabs->UpdateRatingStudent($libellesession,$typesessionNotes,$ideleve,$classeId,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);
        //
         $firstnote=0;
        //
         // $etabs->AddNotesPisteAfter($idpiste,$idtypenote,$firstnote,$notes,$ideleve,$oldnotes);



        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }

      $etabs->RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

    }else if($_POST['typenote']==2)
    {
      //il est question d'un examen
      $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
      $classeId=htmlspecialchars(addslashes($_POST['classeId']));
      $typenote=htmlspecialchars(addslashes($_POST['typenote']));
      $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
      $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
      $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
      $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
      $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
      $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
      $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
      $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);
      $datepiste=date("Y-m-d");
      $controlepiste=0;
      $reglementpiste=0;
      $examenpiste=1;
      $actionpiste=2;
      $typesessionNotes=$etabs->getSessionTypeExam($idtypenote,$codeEtab,$libellesession);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);




      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentmat'])));

      for($i=0;$i<$nbstudent;$i++)
      {

        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];
        $studentDatas=$student->getAllInformationsOfStudent($ideleve,$libellesession);
        $studentData=explode("*",$studentDatas);
        $nomcompletStudent=$studentData[2]." ".$studentData[3];
        //$libelleclasse=$studentData[9];

        //nous allons verifier si cet eleve à deja une note pour cet examen si non alors nous allons ajouter la notes

        $check=$student->getNotesAndObservExamNb($ideleve,$typenote,$classeId,$idtypenote,$matiereid,$teatcherid,$libellesession,$codeEtab);

        if($check==0)
        {
  $student->AddNotesExamen($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation,$libellesession);
        }else if($check>0)
        {
          $indicatifEtab=$etabs->getIndictatifOfThisSchool($codeEtab);

          $tabdatadestinataires="Parent";
          //nous allons rechercher l'email du parent
          $dataParents=$etabs->getEmailsOfParentOfStudentInThisClasses($classeId,$tabdatadestinataires,$codeEtab,$libellesession);
          $ka=1;
          foreach ($dataParents as $parents):

            $destimails=$parents->email_parent;
            $destiphone=$indicatifEtab.$parents->tel_parent;


            $ka++;
          endforeach;

          $etabs->SendModificationNotesToParent($destimails,$libelleEtab,$logoEtab,$nomcompletStudent,$libelleclasse,$libellematiere,$codeEtab);


        }

        //insertion dans la table note

        // $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);

        $student->UpdateControleNotes($notes,$observation,$ideleve,$typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$codeEtab);

        // echo $observation;
      }
        $etabs->RecalculstudentRang($classeId,$matiereid,$teatcherid,$codeEtab,$libellesession,$typesessionNotes);

    }

    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
    // ?codeEtab=C00123
    header("Location:../manager/notes.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/notes.php");
      }else {
        header("Location:../locale/notes.php");
      }



    }

    else if($_SESSION['user']['profile'] == "Teatcher")

    {

    header("Location:../teatcher/notes.php");

    }

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //nous avons initier une modification de note qui sera soumise à validation

  if($_POST['typenote']==1)
  {
    //il est question d'un controle

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=1;
     $reglementpiste=0;
     $examenpiste=0;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

      $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

      $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

       $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

       //stocker la valeur à modifier pour etre en attente de validation

       for($i=0;$i<$nbstudent;$i++)
      {
        $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
        $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
        $ideleve=$tabstudent[$i];

        //ajout dans la table  notesmodification

        $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

      }

      // $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";
      $_SESSION['user']['addattendailyok']=L::NotesclassesForConfirmMessageSuccess;

      if($_SESSION['user']['profile'] == "Admin_globale")

      {

    header("Location:../manager/notes.php?codeEtab=".$codeEtab);

      }else if($_SESSION['user']['profile'] == "Admin_locale")

      {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/notes.php");
        }else {
          header("Location:../locale/notes.php");
        }


  }else if($_SESSION['user']['profile'] == "Teatcher")

      {

    header("Location:../locale/notes.php");

      }

  }else if($_POST['typenote']==2)
  {
    //il est question d'un examen

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=0;
     $reglementpiste=0;
     $examenpiste=1;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

     //stocker la valeur à modifier pour etre en attente de validation

     for($i=0;$i<$nbstudent;$i++)
    {
      $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
      $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
      $ideleve=$tabstudent[$i];

      //ajout dans la table  notesmodification

      $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

    }

    // $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";

    $_SESSION['user']['addattendailyok']=L::NotesclassesForConfirmMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/notes.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/notes.php");
      }else {
        header("Location:../locale/notes.php");
      }


}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../locale/notes.php");

    }



  }




}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{

  if($_POST['typenote']==1)
  {
    //il est question d'un controle

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=1;
     $reglementpiste=0;
     $examenpiste=0;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);

     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);

      $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

      //stocker la valeur à modifier pour etre en attente de validation

      for($i=0;$i<$nbstudent;$i++)
     {
       $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
       $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
       $ideleve=$tabstudent[$i];

       //ajout dans la table  notesmodification

       $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

     }

     // $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";
     $_SESSION['user']['addattendailyok']=L::NotesclassesForConfirmMessageSuccess;

     if($_SESSION['user']['profile'] == "Admin_globale")

     {

   header("Location:../manager/noteseleves.php?codeEtab=".$codeEtab);

     }else if($_SESSION['user']['profile'] == "Admin_locale")

     {
       if($_SESSION['user']['paysid']==4)
       {
         header("Location:../localecmr/noteseleves.php");
       }else {
         header("Location:../locale/noteseleves.php");
       }


 }else if($_SESSION['user']['profile'] == "Teatcher")

     {

   header("Location:../locale/noteseleves.php");

     }





  }else if($_POST['typenote']==2)
  {
    //il est question d'un examen

    $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
    $classeId=htmlspecialchars(addslashes($_POST['classeId']));
    $typenote=htmlspecialchars(addslashes($_POST['typenote']));
    $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
    $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
    $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
    $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
    $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));

    $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
    $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
    $libelleclasse=$classe->getInfosofclassesbyId($classeId,$libellesession);

     $datepiste=date("Y-m-d");
     $controlepiste=0;
     $reglementpiste=0;
     $examenpiste=1;
     $actionpiste=2;
     $validationstatus=0;

     $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$libellesession);
     $tabdata=explode("*",$dataNotes);
     $coefficientNotes=$tabdata[0];
     $typesessionNotes=$tabdata[1];

     $idpiste=$etabs->Addpiste($datepiste,$_SESSION['user']['IdCompte'],$classeId,$codeEtab,$libellesession,$typesessionNotes,$controlepiste,$reglementpiste,$examenpiste,$actionpiste);
     $libellematiere=$matierestud->getMatiereLibelleByIdMat($matiereid,$codeEtab);
     $tabstudent=explode("*",htmlspecialchars(addslashes($_POST['studentids'])));

     //stocker la valeur à modifier pour etre en attente de validation

     for($i=0;$i<$nbstudent;$i++)
    {
      $notes=htmlspecialchars(addslashes($_POST['noteE'.$tabstudent[$i]]));
      $observation=htmlspecialchars(addslashes($_POST['obserE'.$tabstudent[$i]]));
      $ideleve=$tabstudent[$i];

      //ajout dans la table  notesmodification

      $student->AddstandbyUpdatenotes($idpiste,$controlepiste,$examenpiste,$idtypenote,$matiereid,$teatcherid,$classeId,$validationstatus,$ideleve,$notes,$observation);

    }

    // $_SESSION['user']['addattendailyok']="Modification de note soumise et en attente de validation";
    $_SESSION['user']['addattendailyok']=L::NotesclassesForConfirmMessageSuccess;

    if($_SESSION['user']['profile'] == "Admin_globale")

    {

  header("Location:../manager/noteseleves.php?codeEtab=".$codeEtab);

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/noteseleves.php");
      }else {
        header("Location:../locale/noteseleves.php");
      }


}else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../locale/noteseleves.php");

    }
  }


}else if(isset($_POST['etape'])&&($_POST['etape']==7))
{
  //ajouter une note pour IBSA

  echo "bonjour";

  $nbstudent=htmlspecialchars(addslashes($_POST['nbstudent']));
  $classeId=htmlspecialchars(addslashes($_POST['classeId']));
  $typenote=htmlspecialchars(addslashes($_POST['typenote']));
  $idtypenote=htmlspecialchars(addslashes($_POST['idtypenote']));
  $matiereid=htmlspecialchars(addslashes($_POST['matiereid']));
  $teatcherid=htmlspecialchars(addslashes($_POST['teatcherid']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionlibelle=htmlspecialchars(addslashes($_POST['sessionlibelle']));

  $libelleEtab=$etabs->getEtabLibellebyCodeEtab($codeEtab);
  $logoEtab=$etabs->getEtabLogobyCodeEtab($codeEtab);
  $libelleclasse=$classe->getInfosofclassesbyId($classeId,$sessionlibelle);
  $datepiste=date("Y-m-d");
  $controlepiste=1;
  $reglementpiste=0;
  $examenpiste=0;
  $actionpiste=1;
  $tabNameofstudents="";

  //nous allons verifier si nous sommes dans le cas d'un etablissement primaire ou secondaire

  $etablissementType=$etabs->DetermineTypeEtab($codeEtab);



}

//fin method POST

//methode GET

if(isset($_GET['etape'])&&($_GET['etape']==7))
{
  //recuperation des variables

$noteid=htmlspecialchars(addslashes($_GET['noteid']));
$oldnote=htmlspecialchars(addslashes($_GET['olnote']));
$newnote=htmlspecialchars(addslashes($_GET['newnote']));
$eleveid=htmlspecialchars(addslashes($_GET['eleveid']));
$pisteid=htmlspecialchars(addslashes($_GET['pisteid']));
$observation=htmlspecialchars(addslashes($_GET['observation']));
$typeEtab=htmlspecialchars(addslashes($_GET['typeEtab']));
$codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
$controle=htmlspecialchars(addslashes($_GET['controle']));
$examen=htmlspecialchars(addslashes($_GET['examen']));
$userid=$_SESSION['user']['IdCompte'];
$datemodif=date("Y-m-d");
$firstnote=0;
$typenote="";
$statut=2;



//modification de la note

//nous allons verifier le type d'etablissement avant de faire l'action
//if($etablissementType==1||$etablissementType==3)

if($typeEtab==1||$typeEtab==3)
{
  //cas des etablissement primaire et maternelle
  //nous allons modifier simplement la note

  if($controle==1&&$examen==0)
  {
    //cas d'un controle
    $typenote=1;

    //nous allons chercher les informations idcontrole,classeid,matiereid,teatcherid

    $notesdatas=$etabs->getnotescrontroleInformations($noteid,$eleveid,$codeEtab,$typenote);

    $tabnotesdatas=explode("*",$notesdatas);
    $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
    $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
    $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
    $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
    $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));


  }else if($controle==0&&$examen==1)
  {
    //cas d'un examen

    //nous allons chercher les informations idcontrole,classeid,matiereid,teatcherid

    $typenote=2;

    $notesdatas=$etabs->getnotesexamenInformations($noteid,$eleveid,$codeEtab,$typenote);
    $tabnotesdatas=explode("*",$notesdatas);
    $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
    $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
    $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
    $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
    $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));

  }

  //mise à jour de la note dans le système

  $etabs->AddNotesPisteAfter($pisteid,$idtypenote,$firstnote,$newnote,$eleveid,$oldnote);

  //nous allons apporter des modification au niveau de la table notesmodification

  if($controle==1&&$examen==0)
  {
$etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
  }else if($controle==0&&$examen==1)
  {
$etabs->Updatenotesmodificationexam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
  }


  $student->UpdateNotesOfStudent($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation);

  $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/notes.php");
    }else {
      header("Location:../locale/notes.php");
    }



  }else if($_SESSION['user']['profile'] == "Teatcher")

  {

  header("Location:../teatcher/notes.php");

  }


}else {
  // cas des etablissements secondaires et universitaires

  //en plus de modifier la note nous devons faire des ajustement

  if($controle==1&&$examen==0)
  {
    //cas d'un controle
    $typenote=1;

    $notesdatas=$etabs->getnotescrontroleInformationsecondary($noteid,$eleveid,$codeEtab,$typenote);

    $tabnotesdatas=explode("*",$notesdatas);
    $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
    $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
    $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
    $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
    $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));
    $typesessionEtab=htmlspecialchars(addslashes($tabnotesdatas[5]));

    //recuperation des informations sur la moyenne dans la matiere

    $ratinginformations=$etabs->getStudentRatingInfos($eleveid,$typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$typesessionEtab);
    $tabInfosrating=explode("*",$ratinginformations);
    //recuperation des informations

    $ratingId=$tabInfosrating[0];
    $totalnotesrating=$tabInfosrating[1];
    $totalnotescoefrating=$tabInfosrating[2];
    $totalcoefrating=$tabInfosrating[3];
    $rating=$tabInfosrating[4];

    //nouvelle operation

    $sommesNotes=($totalnotesrating-$oldnote)+$newnote;
    $sommesNotescoef=$totalnotescoefrating-($oldnote*$coefficientNotes)+($newnote*$coefficientNotes);
    $sommescoef=$totalcoefrating;
    $moyenne=$sommesNotescoef/$totalcoefrating;

    $etabs->UpdateRatingStudent($sessionEtab,$typesessionEtab,$eleveid,$classeid,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);
    //insertion dans la tables pisteNotes
    $etabs->AddNotesPisteAfter($pisteid,$idtypenote,$firstnote,$newnote,$eleveid,$oldnote);
    $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
    $student->UpdateNotesOfStudent($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation);

    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/notes.php");
      }else {
        header("Location:../locale/notes.php");
      }



  }else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }



  }else if($controle==0&&$examen==1)
  {
    //cas d'un examen
    $typenote=2;

    $notesdatas=$etabs->getnotesexamenInformationsecondary($noteid,$eleveid,$codeEtab,$typenote);
    $tabnotesdatas=explode("*",$notesdatas);
    $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
    $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
    $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
    $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
    $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));
    $typesessionEtab=htmlspecialchars(addslashes($tabnotesdatas[5]));

    //insertion dans la tables pisteNotes
    $etabs->AddNotesPisteAfter($pisteid,$idtypenote,$firstnote,$newnote,$eleveid,$oldnote);

    $etabs->Updatenotesmodificationexam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);

    $student->UpdateNotesOfStudent($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation);

    $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



    if($_SESSION['user']['profile'] == "Admin_globale")

    {
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

    }else if($_SESSION['user']['profile'] == "Admin_locale")

    {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/notes.php");
      }else {
        header("Location:../locale/notes.php");
      }



  }else if($_SESSION['user']['profile'] == "Teatcher")

    {

  header("Location:../teatcher/notes.php");

    }


  }

    //mise à jour de la note dans le système



  // $student->UpdateNotesOfStudent($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation);

}



// $student->AddNotesControle($typenote,$idtypenote,$classeId,$matiereid,$teatcherid,$tabstudent[$i],$codeEtab,$notes,$observation);




}else if(isset($_GET['etape'])&&($_GET['etape']==8))
{
  //recuperation des variables

  $modifnoteid=htmlspecialchars(addslashes($_GET['modifnoteid']));
  $nouvellenote=htmlspecialchars(addslashes($_GET['nouvellenote']));
  $eleveid=htmlspecialchars(addslashes($_GET['eleveid']));
  $pisteid=htmlspecialchars(addslashes($_GET['pisteid']));
  $observation=htmlspecialchars(addslashes($_GET['observation']));
  $typeEtab=htmlspecialchars(addslashes($_GET['typeEtab']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $controle=htmlspecialchars(addslashes($_GET['controle']));
  $examen=htmlspecialchars(addslashes($_GET['examen']));
  $session=htmlspecialchars(addslashes($_GET['session']));
  $userid=$_SESSION['user']['IdCompte'];
  $datemodif=date("Y-m-d");
  $firstnote=0;
  $typenote=1;
  $statut=2;
  $tabNameofstudents="";





  if($typeEtab==1||$typeEtab==3)
  {
    //cas d'un etablissement primaire ou maternelle
    if($_SESSION['user']['paysid']==4)
    {
      //nous allons verifier s'il s'agit de la premirer note dans cette matiere et dans ce trimestre

      $datasmodificationnotes=$etabs->gettypenoteidbymodifnoteidsecondary($modifnoteid,$eleveid,$codeEtab,$session);
      $tabdatasmodif=explode("*",$datasmodificationnotes);
      $idtypenote=$tabdatasmodif[0];
      $classeid=$tabdatasmodif[1];
      $matiereid=$tabdatasmodif[2];
      $teatcherid=$tabdatasmodif[3];
      $typesession=$tabdatasmodif[4];


      $nbnotes=$student->DetermineNoteNumbercontroles($eleveid,$classeid,$matiereid,$idtypenote,$codeEtab);

      if($nbnotes==0)
      {
          //il est question de la première note dans cette matiere

          //nous allons rechercher le coefficient du controle
          $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$session);
          $tabdata=explode("*",$dataNotes);
          $coefficientNotes=$tabdata[0];
          $typesessionNotes=$tabdata[1];

          $notescoef=$nouvellenote*$coefficientNotes;

          $moyennestudent=$notescoef/$coefficientNotes;

          //nous allons ajouter la note dans rating

          $etabs->AddOldRatingStudent($session,$typesession,$eleveid,$classeid,$teatcherid,$matiereid,$nouvellenote,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);
          //insertion dans la table pisteNotes
          $firstnote=1;

          $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$nouvellenote,$eleveid);
          $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
          $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);



      }else if($nbnotes>0)
      {
        //nous allons rechercher le coefficient du controle
        $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$session);
        $tabdata=explode("*",$dataNotes);
        $coefficientNotes=$tabdata[0];
        $typesessionNotes=$tabdata[1];

        //nous allons recuperer le totalnotes,totalnotescoef,totalcoef

        $LastratingInfos=$etabs->getRatingtypesessionInfosNew($eleveid,$idtypenote,$matiereid,$teatcherid,$codeEtab,$session,$classeid,$typesession);
        foreach ($LastratingInfos as $values):
             // $tabLastInfos=explode("*",$LastratingInfos);
             $alltotalnotes=$values->totalnotes_rating;
             $alltotalnotescoef=$values->totalnotescoef_rating;
             $totalcoefnotes=$values->totalcoef_rating;
             $ratingId=$values->id_rating;
           endforeach;

        //nouvelles Informations

        $sommesNotes=$alltotalnotes+$nouvellenote;
        $sommesNotescoef=$alltotalnotescoef+($nouvellenote*$coefficientNotes);
        $sommescoef=$totalcoefnotes+$coefficientNotes;
        $moyenne=$sommesNotescoef/$sommescoef;

        $etabs->UpdateRatingStudent($session,$typesession,$eleveid,$classeid,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

        $firstnote=1;
        $statut=2;

        $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$notes,$eleveid);
        $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
        $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);



      }


    }else {
      //il est question d'une nouvelle note de controle

      $datasmodificationnotes=$etabs->gettypenoteidbymodifnoteid($modifnoteid,$eleveid,$codeEtab,$session);
      $tabdatasmodif=explode("*",$datasmodificationnotes);
      $idtypenote=$tabdatasmodif[0];
      $classeid=$tabdatasmodif[1];
      $matiereid=$tabdatasmodif[2];
      $teatcherid=$tabdatasmodif[3];

      $studentDatas=$student->getAllInformationsOfStudent($eleveid,$session);
      $studentData=explode("*",$studentDatas);
      $nomcompletStudent=$studentData[2]." ".$studentData[3];
      $tabNameofstudents=$tabNameofstudents.$nomcompletStudent."*";

      //insertion dans la table pisteNotes
      $firstnote=1;

      $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$nouvellenote,$eleveid);
      $etabs->UpdatenotesmodificationNew($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid,$modifnoteid);
      $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);


    }



  }else {
    //cas d'un etablissement secondaire ou universitaire

    //nous allons verifier s'il s'agit de la premirer note dans cette matiere et dans ce trimestre

    $datasmodificationnotes=$etabs->gettypenoteidbymodifnoteidsecondary($modifnoteid,$eleveid,$codeEtab,$session);
    $tabdatasmodif=explode("*",$datasmodificationnotes);
    $idtypenote=$tabdatasmodif[0];
    $classeid=$tabdatasmodif[1];
    $matiereid=$tabdatasmodif[2];
    $teatcherid=$tabdatasmodif[3];
    $typesession=$tabdatasmodif[4];


    $nbnotes=$student->DetermineNoteNumbercontroles($eleveid,$classeid,$matiereid,$idtypenote,$codeEtab);

    if($nbnotes==0)
    {
        //il est question de la première note dans cette matiere

        //nous allons rechercher le coefficient du controle
        $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$session);
        $tabdata=explode("*",$dataNotes);
        $coefficientNotes=$tabdata[0];
        $typesessionNotes=$tabdata[1];

        $notescoef=$nouvellenote*$coefficientNotes;

        $moyennestudent=$notescoef/$coefficientNotes;

        //nous allons ajouter la note dans rating

        $etabs->AddOldRatingStudent($session,$typesession,$eleveid,$classeid,$teatcherid,$matiereid,$nouvellenote,$notescoef,$coefficientNotes,$moyennestudent,$codeEtab);
        //insertion dans la table pisteNotes
        $firstnote=1;

        $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$nouvellenote,$eleveid);
        $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
        $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);



    }else if($nbnotes>0)
    {
      //nous allons rechercher le coefficient du controle
      $dataNotes=$etabs->getControlecoef($idtypenote,$matiereid,$teatcherid,$codeEtab,$session);
      $tabdata=explode("*",$dataNotes);
      $coefficientNotes=$tabdata[0];
      $typesessionNotes=$tabdata[1];

      //nous allons recuperer le totalnotes,totalnotescoef,totalcoef

      $LastratingInfos=$etabs->getRatingtypesessionInfosNew($eleveid,$idtypenote,$matiereid,$teatcherid,$codeEtab,$session,$classeid,$typesession);
      foreach ($LastratingInfos as $values):
             // $tabLastInfos=explode("*",$LastratingInfos);
             $alltotalnotes=$values->totalnotes_rating;
             $alltotalnotescoef=$values->totalnotescoef_rating;
             $totalcoefnotes=$values->totalcoef_rating;
             $ratingId=$values->id_rating;
           endforeach;

      //nouvelles Informations

      $sommesNotes=$alltotalnotes+$nouvellenote;
      $sommesNotescoef=$alltotalnotescoef+($nouvellenote*$coefficientNotes);
      $sommescoef=$totalcoefnotes+$coefficientNotes;
      $moyenne=$sommesNotescoef/$sommescoef;

      $etabs->UpdateRatingStudent($session,$typesession,$eleveid,$classeid,$teatcherid,$matiereid,$sommesNotes,$sommesNotescoef,$sommescoef,$moyenne,$ratingId,$codeEtab);

      $firstnote=1;
      $statut=2;

      $etabs->AddNotesPiste($pisteid,$idtypenote,$firstnote,$notes,$eleveid);
      $etabs->Updatenotesmodification($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);
      $student->AddNotesControleAftermod($typenote,$idtypenote,$classeid,$matiereid,$teatcherid,$eleveid,$codeEtab,$nouvellenote,$observation,$session);



    }



  }

  // $_SESSION['user']['addattendailyok']="Notes modifier avec succès";
  $_SESSION['user']['addattendailyok']=L::NotesclassesModifyOKMessageSuccess;



  if($_SESSION['user']['profile'] == "Admin_globale")

  {
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);


  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      // header("Location:../localecmr/notes.php");
    }else {
      // header("Location:../locale/notes.php");
    }


  }else if($_SESSION['user']['profile'] == "Teatcher")

  {

  header("Location:../teatcher/notes.php");

  }

}else if(isset($_GET['etape'])&&($_GET['etape']==9))
{
  //recupeation des variables

  $noteid=htmlspecialchars(addslashes($_GET['noteid']));
  $oldnote=htmlspecialchars(addslashes($_GET['oldnote']));
  $newnote=htmlspecialchars(addslashes($_GET['newnote']));
  $eleveid=htmlspecialchars(addslashes($_GET['eleveid']));
  $pisteid=htmlspecialchars(addslashes($_GET['pisteid']));
  $observation=htmlspecialchars(addslashes($_GET['observation']));
  $typeEtab=htmlspecialchars(addslashes($_GET['typeEtab']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $controle=htmlspecialchars(addslashes($_GET['controle']));
  $examen=htmlspecialchars(addslashes($_GET['examen']));
  $userid=$_SESSION['user']['IdCompte'];
  $datemodif=date("Y-m-d");
  $firstnote=0;
  $typenote=1;
  $statut=-1;

  //nous allons chercher les informations idcontrole,classeid,matiereid,teatcherid

  $notesdatas=$etabs->getnotescrontroleInformations($noteid,$eleveid,$codeEtab,$typenote);

  $tabnotesdatas=explode("*",$notesdatas);
  $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
  $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
  $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
  $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
  $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));

  //nous allons changer le statut de modificationnote à -1
  $etabs->DesactivationmodificationNotes($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);

  // $_SESSION['user']['addattendailyok']="Modification note rejeter avec succès";
  $_SESSION['user']['addattendailyok']=L::NotesclassesModifyNONMessageSuccess;



  if($_SESSION['user']['profile'] == "Admin_globale")

  {
  header("Location:../manager/notes.php?codeEtab=".$codeEtab);
  //header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/notes.php");
    }else {
      header("Location:../locale/notes.php");
    }


  }else if($_SESSION['user']['profile'] == "Teatcher")

  {

  header("Location:../teatcher/notes.php");

  }

}else if(isset($_GET['etape'])&&($_GET['etape']==10))
{

}else if(isset($_GET['etape'])&&($_GET['etape']==11))
{
  //valisation de la modification de la note concernant l'examen
  //recuperation des variables

  $noteid=htmlspecialchars(addslashes($_GET['noteid']));
  $oldnote=htmlspecialchars(addslashes($_GET['olnote']));
  $newnote=htmlspecialchars(addslashes($_GET['newnote']));
  $eleveid=htmlspecialchars(addslashes($_GET['eleveid']));
  $pisteid=htmlspecialchars(addslashes($_GET['pisteid']));
  $observation=htmlspecialchars(addslashes($_GET['observation']));
  $typeEtab=htmlspecialchars(addslashes($_GET['typeEtab']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $controle=htmlspecialchars(addslashes($_GET['controle']));
  $examen=htmlspecialchars(addslashes($_GET['examen']));
  $userid=$_SESSION['user']['IdCompte'];
  $datemodif=date("Y-m-d");
  $firstnote=0;
  $typenote="";
  $statut=2;

  $typenote=2;

  $notesdatas=$etabs->getnotesexamenInformations($noteid,$eleveid,$codeEtab,$typenote);
  $tabnotesdatas=explode("*",$notesdatas);
  $idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
  $classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
  $matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
  $teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
  $sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));

  //insertion dans la tables pisteNotes
  $etabs->AddNotesPisteAfter($pisteid,$idtypenote,$firstnote,$newnote,$eleveid,$oldnote);

  // echo $statut."*".$datemodif."*".$userid."*".$pisteid."*".$idtypenote."*".$typenote."*".$eleveid."*".$classeid."*".$teatcherid;

  $etabs->Updatenotesmodificationexam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);

  $student->UpdateNotesOfStudent($noteid,$codeEtab,$eleveid,$classeid,$newnote,$observation);

  $_SESSION['user']['addattendailyok']="Notes modifier avec succès";



  if($_SESSION['user']['profile'] == "Admin_globale")

  {
header("Location:../manager/notes.php?codeEtab=".$codeEtab);
//header("Location:../manager/notes.php");

  }else if($_SESSION['user']['profile'] == "Admin_locale")

  {
    if($_SESSION['user']['paysid']==4)
    {
      header("Location:../localecmr/notes.php");
    }else {
      header("Location:../locale/notes.php");
    }



}else if($_SESSION['user']['profile'] == "Teatcher")

  {

header("Location:../teatcher/notes.php");

  }




}else if(isset($_GET['etape'])&&($_GET['etape']==12))
{

}else if(isset($_GET['etape'])&&($_GET['etape']==13))
{
  //rejet de la validation de note de l'examen
  //recuperation des variables

$noteid=htmlspecialchars(addslashes($_GET['noteid']));
$oldnote=htmlspecialchars(addslashes($_GET['oldnote']));
$newnote=htmlspecialchars(addslashes($_GET['newnote']));
$eleveid=htmlspecialchars(addslashes($_GET['eleveid']));
$pisteid=htmlspecialchars(addslashes($_GET['pisteid']));
$observation=htmlspecialchars(addslashes($_GET['observation']));
$typeEtab=htmlspecialchars(addslashes($_GET['typeEtab']));
$codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
$controle=htmlspecialchars(addslashes($_GET['controle']));
$examen=htmlspecialchars(addslashes($_GET['examen']));
$userid=$_SESSION['user']['IdCompte'];
$datemodif=date("Y-m-d");
$firstnote=0;
$typenote=2;
$statut=-1;

$notesdatas=$etabs->getnotescrontroleInformations($noteid,$eleveid,$codeEtab,$typenote);

$tabnotesdatas=explode("*",$notesdatas);
$idtypenote=htmlspecialchars(addslashes($tabnotesdatas[0]));
$classeid=htmlspecialchars(addslashes($tabnotesdatas[1]));
$matiereid=htmlspecialchars(addslashes($tabnotesdatas[2]));
$teatcherid=htmlspecialchars(addslashes($tabnotesdatas[3]));
$sessionEtab=htmlspecialchars(addslashes($tabnotesdatas[4]));

$etabs->DesactivationmodificationNotesExam($statut,$datemodif,$userid,$pisteid,$idtypenote,$typenote,$eleveid,$classeid,$teatcherid);

// $_SESSION['user']['addattendailyok']="Validation de la note rejeter avec succès";

$_SESSION['user']['addattendailyok']=L::NotesclassesModifyNONMessageSuccess;



if($_SESSION['user']['profile'] == "Admin_globale")

{
header("Location:../manager/notes.php?codeEtab=".$codeEtab);
//header("Location:../manager/notes.php");

}else if($_SESSION['user']['profile'] == "Admin_locale")

{
  if($_SESSION['user']['paysid']==4)
  {
    header("Location:../locale/notes.php");
  }else {
    header("Location:../locale/notes.php");
  }


}else if($_SESSION['user']['profile'] == "Teatcher")

{

header("Location:../teatcher/notes.php");

}


}
