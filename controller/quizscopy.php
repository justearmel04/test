<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../class/Matiere.php');
require_once('../controller/functions.php');
$classe = new Classe();
$etabs=new Etab();
$matiere=new Matiere();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recuperation des variables
$libellequiz=htmlspecialchars($_POST['libellecourse']);
$classeEtab=htmlspecialchars($_POST['classeEtab']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);
$durationquiz=htmlspecialchars($_POST['durationcourse']);
$instructionquiz=htmlspecialchars($_POST['detailscourse']);
$concatmatiere=htmlspecialchars($_POST['matclasse']);
$datelimite=dateFormat(htmlspecialchars($_POST['datecourse']));
$tabmatiere=explode("-",$concatmatiere);
$matiereid=$tabmatiere[0];
$teatcherid=$tabmatiere[1];
$verouillerquiz=htmlspecialchars($_POST['verouiller']);
$statutquiz=0;

$libellematiere=$etabs->getMatiereLibelleByIdMat($matiereid,$codeEtab);
$libelleclasse=$classe->getInfosofclassesbyId($classeEtab,$sessionEtab);


//insertion dans la table quiz

$quizid=$etabs->Addquiz($libellequiz,$durationquiz,$instructionquiz,$classeEtab,$matiereid,$teatcherid,$codeEtab,$sessionEtab,$verouillerquiz,$statutquiz,$datelimite);

//nous allons renseigner les trueorfalse

$concatquesttrueorfalse=htmlspecialchars($_POST['concatquesttrueorfalse']);

$concatquesttrueorfalse=substr($concatquesttrueorfalse, 0, -1);

$tabconcatquesttrueorfalse=explode("@",$concatquesttrueorfalse);

$nb=count($tabconcatquesttrueorfalse);

for($i=0;$i<$nb;$i++)
{
  //il est question d'une question trueorfalse

$libellequestion=htmlspecialchars($_POST['libellequestion'.$tabconcatquesttrueorfalse[$i]]);
$pointquestion=htmlspecialchars($_POST['pointquestion'.$tabconcatquesttrueorfalse[$i]]);
$answer=htmlspecialchars($_POST['answer'.$tabconcatquesttrueorfalse[$i]]);
$moderep=htmlspecialchars($_POST['moderep'.$tabconcatquesttrueorfalse[$i]]);

// echo $libellequestion." /".$pointquestion." /".$answer." /".$moderep;


$questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);


if($answer==1)
{
  //cas de vrai
  $etabs->AddPropositionTrueActive($questionid);
  $etabs->AddPropositionFalse($questionid);

}else {
  // cos de faux
  $etabs->AddPropositionTrue($questionid);
  $etabs->AddPropositionFalseActive($questionid);
}



}

//nous allons renseigner les choix multiple


$concatquestmultiple=htmlspecialchars($_POST['concatquestmultiple']);
$concatquestmultiple=substr($concatquestmultiple, 0, -1);
$tabconcatquestmultiple=explode("@",$concatquestmultiple);

$nbMulti=count($tabconcatquestmultiple);

for($i=0;$i<$nbMulti;$i++)
{

  $libellequestion=htmlspecialchars($_POST['libellequestion'.$tabconcatquestmultiple[$i]]);
  $pointquestion=htmlspecialchars($_POST['pointquestion'.$tabconcatquestmultiple[$i]]);
  // $answer=htmlspecialchars($_POST['answer'.$tabconcatquesttrueorfalse[$i]]);
  $moderep=htmlspecialchars($_POST['moderep'.$tabconcatquestmultiple[$i]]);
  $propositionNbquestion=htmlspecialchars($_POST['propositionNbquestion'.$tabconcatquestmultiple[$i]]);

  $questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

  for($j=1;$j<=$propositionNbquestion;$j++)
  {

    if(isset($_POST['libelle_proposition'.$tabconcatquestmultiple[$i].$j]))
    {
      $libelle_proposition=htmlspecialchars($_POST['libelle_proposition'.$tabconcatquestmultiple[$i].$j]);
      $proposiionid=$etabs->AddProposition($libelle_proposition,$questionid);

      if(isset($_POST['chk_proposition'.$tabconcatquestmultiple[$i].$j]))
      {
        $chk_proposition=$_POST['chk_proposition'.$tabconcatquestmultiple[$i].$j];
        $etabs->UpdatePropositionValue($chk_proposition,$proposiionid);
      }
    }


  }


  $_SESSION['user']['addclasseok']="Le quiz a été ajouté avec succès";

  if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/listquizs.php");

      }


}


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //il est question de modifier le quiz

  $libellequiz=htmlspecialchars($_POST['libellecourse']);
  $classeEtab=htmlspecialchars($_POST['classeEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $durationquiz=htmlspecialchars($_POST['durationcourse']);
  $instructionquiz=htmlspecialchars($_POST['detailscourse']);
  $concatmatiere=htmlspecialchars($_POST['matclasse']);
  $datelimite=dateFormat(htmlspecialchars($_POST['datecourse']));
  $tabmatiere=explode("-",$concatmatiere);
  $matiereid=$tabmatiere[0];
  $teatcherid=$tabmatiere[1];
  $verouillerquiz=htmlspecialchars($_POST['verouiller']);
  $idquiz=htmlspecialchars($_POST['courseid']);

// echo $verouillerquiz;

  $etabs->UpdateQuizInfos($libellequiz,$classeEtab,$codeEtab,$sessionEtab,$durationquiz,$instructionquiz,$datelimite,$matiereid,$teatcherid,$verouillerquiz,$idquiz);

  $_SESSION['user']['addclasseok']="Le quiz a été modifié avec succès";

  if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/updatequizs.php?courseid=".$idquiz."&classeid=".$classeEtab);

      }


}else if(isset($_POST['etape'])&&($_POST['etape']==3))
{
//recuperation des variables

$libellequestion=htmlspecialchars($_POST['libellesquestTrue']);
$quizid=htmlspecialchars($_POST['idquiztrue']);
$pointquestion=htmlspecialchars($_POST['pointquestTrue']);
$answer=htmlspecialchars($_POST['answer']);
$classeid=htmlspecialchars($_POST['classequiztrue']);

$moderep=1;

$questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

if($answer==1)
{
  //cas de vrai
  $etabs->AddPropositionTrueActive($questionid);
  $etabs->AddPropositionFalse($questionid);

}else {
  // cos de faux
  $etabs->AddPropositionTrue($questionid);
  $etabs->AddPropositionFalseActive($questionid);
}

$_SESSION['user']['addclasseok']="Une nouvelle question a été ajoutée au quiz avec succès";

if($_SESSION['user']['profile'] == "Teatcher") {

    header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);

    }

}else if(isset($_POST['etape'])&&($_POST['etape']==4))
{

  $libellequestion=htmlspecialchars($_POST['libellesquestmuluti']);
  $quizid=htmlspecialchars($_POST['idquizmulti']);
  $pointquestion=htmlspecialchars($_POST['pointquestMulti']);
  $classeid=htmlspecialchars($_POST['classequizmulti']);

  $moderep=2;

  $questionid=$etabs->AddQuestion($libellequestion,$moderep,$pointquestion,$quizid);

  $concatquestmultiple=htmlspecialchars($_POST['concatmultipleprop']);
  $concatquestmultiple=substr($concatquestmultiple, 0, -1);
  $tabconcatquestmultiple=explode("@",$concatquestmultiple);

  $nbMulti=count($tabconcatquestmultiple);

  for($i=0;$i<$nbMulti;$i++)
  {
    $libelle_proposition=htmlspecialchars($_POST['libelle_proposition'.$tabconcatquestmultiple[$i]]);


    $proposiionid=$etabs->AddProposition($libelle_proposition,$questionid);

    if(isset($_POST['chk_proposition'.$tabconcatquestmultiple[$i]]))
    {
      $chk_proposition=htmlspecialchars($_POST['chk_proposition'.$tabconcatquestmultiple[$i]]);
      $etabs->UpdatePropositionValue($chk_proposition,$proposiionid);
    }

  }

  $_SESSION['user']['addclasseok']="Une nouvelle question a été ajoutée au quiz avec succès";

  if($_SESSION['user']['profile'] == "Teatcher") {

      header("Location:../teatcher/updatequizs.php?courseid=".$quizid."&classeid=".$classeid);

      }

}else if(isset($_POST['etape'])&&($_POST['etape']==5))
{
  //recuperation des variables

$concattrueorfalseid=htmlspecialchars($_POST['concattrueorfalseid']);
$studentid=htmlspecialchars($_POST['studentid']);
$quizid=htmlspecialchars($_POST['quizid']);
$classeid=htmlspecialchars($_POST['classeid']);
$matiereid=htmlspecialchars($_POST['matiereid']);
$concatqestmulti=htmlspecialchars($_POST['concatqestmulti']);
$codeEtab=htmlspecialchars($_POST['codeEtab']);
$sessionEtab=htmlspecialchars($_POST['sessionEtab']);

//reponses des questions trueorfalse

$tabtrueorfalseid=explode("@",$concattrueorfalseid);
$nbtrueorfalse=count($tabtrueorfalseid)-1;
// echo $nbtrueorfalse;

for($i=0;$i<$nbtrueorfalse;$i++)
{

  $answer=htmlspecialchars($_POST['answer'.$tabtrueorfalseid[$i]]);


  if($answer==1)
  {
    $answer1=0;
    // echo "true";
    //nous allons chercher id de la proposition vrai et proposition faux

    $idtrue=$etabs->getPropositionTrueidofQuestion($tabtrueorfalseid[$i]);
    $idfalse=$etabs->getPropositionFalseidofQuestion($tabtrueorfalseid[$i]);


    $etabs->AddreponsequizStudentTrue($quizid,$studentid,$tabtrueorfalseid[$i],$idtrue,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    $etabs->AddreponsequizStudentFalse($quizid,$studentid,$tabtrueorfalseid[$i],$idfalse,$answer1,$matiereid,$classeid,$codeEtab,$sessionEtab);

    // echo $tabtrueorfalseid[$i]." - ".$idtrue." - ".$idfalse."</br>";

  }else if($answer==0)
  {
    $answer1=1;
    $idtrue=$etabs->getPropositionTrueidofQuestion($tabtrueorfalseid[$i]);
    $idfalse=$etabs->getPropositionFalseidofQuestion($tabtrueorfalseid[$i]);

    $etabs->AddreponsequizStudentTrue($quizid,$studentid,$tabtrueorfalseid[$i],$idtrue,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    $etabs->AddreponsequizStudentFalse($quizid,$studentid,$tabtrueorfalseid[$i],$idfalse,$answer1,$matiereid,$classeid,$codeEtab,$sessionEtab);


    // echo $tabtrueorfalseid[$i]." - ".$idtrue." - ".$idfalse."</br>";

  }


    //

}

//reponse des questions multiples

$tabconcatqestmulti=explode("@",$concatqestmulti);
$nbmultiple=count($tabconcatqestmulti)-1;

for($j=0;$j<$nbmultiple;$j++)
{

// echo $tabconcatqestmulti[$j]."</br>";
$cocher=htmlspecialchars($_POST['concatquestmulticocher'.$tabconcatqestmulti[$j]]);
$cocher=substr($cocher, 0, -1);
$tabcocher=explode("@",$cocher);
$nbtabcocher=count($tabcocher);

  $questionid=$tabconcatqestmulti[$j];
  //nous allons rechercher la liste des propositions pour cette question

  $datapropos=$etabs->getPropositionofQuestion($questionid);
  $concatpropos="";

  foreach ($datapropos as $value):
    $concatpropos=$concatpropos.$value->id_proprep."@";
  endforeach;

  $concatpropos=substr($concatpropos, 0, -1);
  $tabconcatpropos=explode("@",$concatpropos);
  $nbconcatpropos=count($tabconcatpropos);



for($k=0;$k<$nbconcatpropos;$k++)
{


     $propositionid=$tabconcatpropos[$k];

     if(in_array($propositionid,$tabcocher))
     {
       $answer=1;
         $etabs->AddreponsequizStudentTrue($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
    // echo $propositionid."</br>";
      }else{
        $answer=0;
      // echo "pas trouvé"." -".$propositionid."</br>";
       $etabs->AddreponsequizStudentTrue($quizid,$studentid,$questionid,$propositionid,$answer,$matiereid,$classeid,$codeEtab,$sessionEtab);
     }





}

// echo $nbtabcocher."</br>";


// var_dump($tabcocher);

}

$_SESSION['user']['addclasseok']="Le quiz a été soumis avec succès";


if($_SESSION['user']['profile'] == "Student") {

    header("Location:../student/detailquizs.php?course=".$quizid."&classeid=".$classeid);

    }

}







 ?>
