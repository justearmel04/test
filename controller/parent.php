<?php
session_start();
require_once('../class/User.php');
require_once('../class/Parent.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$parent = new ParentX();
$routine = new Etab();
/*
<input type="hidden" name="codeEtab" id="codeEtab" value="<?php echo $codeEtabLocal;?>"/>
<input type="hidden" name="newparent" id="newparent" value="1"/>
*/

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //recupération des variables

  $nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
  $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
  $datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
  $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
  $contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
  $fonction=htmlspecialchars(addslashes($_POST['fonctionTea']));
  $cni=htmlspecialchars(addslashes($_POST['cniTea']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $newparent=htmlspecialchars(addslashes($_POST['newparent']));
  $emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
  $nationalite=htmlspecialchars(addslashes($_POST['nation']));
  $lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
  $nbchild=htmlspecialchars(addslashes($_POST['nbchild']));
  $nbchildsco=htmlspecialchars(addslashes($_POST['nbchildsco']));
  $adrespro=htmlspecialchars(addslashes($_POST['adrespro']));
  $societe=htmlspecialchars(addslashes($_POST['societe']));
  $session=htmlspecialchars(addslashes($_POST['session']));

  // echo $datenaisTea;

  $datecrea=date("Y-m-d");
  $type_cpte="Parent";
  $statut=1;

  if($newparent==0)
  {
    //nous sommes dans un cas d'un ancien parent
    //recuperation de l'id compte du parent
    $idcompte=$parent->getIdcompteParentByCniandEmail($cni,$emailTea,$codeEtab);
    // echo "ancien";
    //enregistrer l'ancien parent
    $parent->Addoldparent($idcompte,$codeEtab);



  }else {
    //nous sommes dans le cas d'un nouveau parent
  // $parent->AddParentwithfile($nomTea,$prenomTea,$contactTea,$fonction,$cni,$emailTea,$datenaisTea,$loginTea,$passTea,$type_cpte,$statut,$datecrea,$fichierad,$sexeTea,$codeEtab);

  $parent->AddParent($nomTea,$prenomTea,$datenaisTea,$emailTea,$sexeTea,$contactTea,$fonction,$cni,$codeEtab,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$datecrea,$type_cpte,$statut);


  }

  $_SESSION['user']['addparok']="Parent ajouté avec succès";

    if($_SESSION['user']['profile'] == "Admin_globale")
    {
  header("Location:../manager/addparent.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale")
    {
      $etablissementType=$routine->DetermineTypeEtab($codeEtab);
      $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

      if($etablissementType==5)
      {

    header("Location:../locale".$libelleEtab."/addparent.php");
      }else {
        header("Location:../locale/addparent.php");
      }



    }

}else if(isset($_POST['etape'])&&($_POST['etape']==2)) {

  //modification du compte parent

  $nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
  $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
  $datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
  $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
  $contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
  $fonction=htmlspecialchars(addslashes($_POST['fonctionTea']));
  $cni=htmlspecialchars(addslashes($_POST['cniTea']));
  $emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
  $idcompte=htmlspecialchars(addslashes($_POST['compte']));
  // $loginTea=htmlspecialchars(addslashes($_POST['loginTea']));
  // $passTea=htmlspecialchars(addslashes($_POST['passTea']));
  $nationalite=htmlspecialchars(addslashes($_POST['nation']));
  $lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
  $nbchild=htmlspecialchars(addslashes($_POST['nbchild']));
  $nbchildsco=htmlspecialchars(addslashes($_POST['nbchildsco']));
  $adrespro=htmlspecialchars(addslashes($_POST['adrespro']));
  $societe=htmlspecialchars(addslashes($_POST['societe']));
  $session=htmlspecialchars(addslashes($_POST['session']));
  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));


$parent->UpdateParent($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$nationalite,$lieuH,$nbchild,$nbchildsco,$adrespro,$societe,$session,$codeEtab,$idcompte);

//echo date_format(date_create($datenaisTea),"Y-m-d");

$_SESSION['user']['updateparentok']="Parent modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale")
{
header("Location:../manager/parents.php");
}else if($_SESSION['user']['profile'] == "Admin_locale")
{
  $etablissementType=$routine->DetermineTypeEtab($codeEtab);
  $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);



  if($etablissementType==5)
  {

header("Location:../locale".$libelleEtab."/parents.php");
  }else {
    header("Location:../locale/parents.php");
  }

}


// $parent->UpdateParentwithOutfilePassNot($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonction,$cni,$emailTea,$loginTea,$idcompte);




}else if(isset($_GET['etape'])&&($_GET['etape']==3)) {

  //recuperation des variables

  $compte=htmlspecialchars(addslashes($_GET['compte']));
  $codeEtab=htmlspecialchars(addslashes($_GET['codeEtab']));
  $statut=0;
  $parent->Deleteparent($compte,$statut);

  $_SESSION['user']['updateparentok']="Compte supprimer avec succès";
  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/parents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {

    $etablissementType=$routine->DetermineTypeEtab($codeEtab);
    $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);


// echo $etablissementType;

    if($etablissementType==5)
    {

  header("Location:../locale".$libelleEtab."/parents.php");
    }else {
      header("Location:../locale/parents.php");
    }

  }



}else if(isset($_POST['etape'])&&($_POST['etape']==4)) {

//recuperation des variables



$idparent=htmlspecialchars(addslashes($_POST['idparent']));
$nomTea=htmlspecialchars(addslashes($_POST['nomTea']));
$prenomTea=htmlspecialchars(addslashes($_POST['prenomTea']));
$datenaisTea=htmlspecialchars(addslashes(dateFormat($_POST['datenaisTea'])));
$lieunaisTea=htmlspecialchars(addslashes($_POST['lieunaisTea']));
$lieuH=htmlspecialchars(addslashes($_POST['lieuH']));
$sexeTea=htmlspecialchars(addslashes($_POST['sexeTea']));
$natTea=htmlspecialchars(addslashes($_POST['natTea']));
$emailTea=htmlspecialchars(addslashes($_POST['emailTea']));
$contactTea=htmlspecialchars(addslashes($_POST['contactTea']));
$situationTea=htmlspecialchars(addslashes($_POST['situationTea']));
$nbchildscoTea=htmlspecialchars(addslashes($_POST['nbchildscoTea']));
$nbchildTea=htmlspecialchars(addslashes($_POST['nbchildTea']));
$societe=htmlspecialchars(addslashes($_POST['societe']));
$adresspro=htmlspecialchars(addslashes($_POST['adresspro']));
$fonctionad=htmlspecialchars(addslashes($_POST['fonctionad']));

//echo $datenaisTea;
$parent->UpdateParentAll($nomTea,$prenomTea,$datenaisTea,$lieunaisTea,$lieuH,$sexeTea,$natTea,$emailTea,$contactTea,$situationTea,$nbchildscoTea,$nbchildTea,$societe,$adresspro,$fonctionad,$idparent);

$_SESSION['user']['updateparentok']="Parent modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale")
{
header("Location:../manager/parents.php");
}else if($_SESSION['user']['profile'] == "Admin_locale")
{
  if($_SESSION['user']['paysid']==4)
  {
    header("Location:../localecmr/parents.php");
  }else {
    header("Location:../locale/parents.php");
  }

}

}else if(isset($_POST['etape'])&&($_POST['etape']==5)) {

//recuperation des des variables

$loginTea=htmlspecialchars(addslashes($_POST['loginTea']));
$passTea=htmlspecialchars(addslashes($_POST['passTea']));
$idparent=htmlspecialchars(addslashes($_POST['idparent']));

$parent->UpdateConnexionInfos($loginTea,$passTea,$idparent);

$_SESSION['user']['updateparentok']="Parent modifier avec succès";

if($_SESSION['user']['profile'] == "Admin_globale")
{
header("Location:../manager/parents.php");
}else if($_SESSION['user']['profile'] == "Admin_locale")
{
  if($_SESSION['user']['paysid']==4)
  {
    header("Location:../localecmr/parents.php");
  }else {
  header("Location:../locale/parents.php");
  }

}

}else if(isset($_POST['etape'])&&($_POST['etape']==6))
{
  //recuperation des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['codeEtab']));
  $sessionEtab=htmlspecialchars(addslashes($_POST['session']));
  $concatparents=htmlspecialchars(addslashes($_POST['concatparents']));
  $datecrea=date("Y-m-d");
  $type_cpte="Parent";
  $statut=1;
  $concatparents=substr($concatparents, 0, -1);
  $dataparent=explode("@",$concatparents);
  $nbcountparent=count($dataparent);

  for($i=0;$i<$nbcountparent;$i++)
  {
    $nomTea=htmlspecialchars(addslashes($_POST['nomTea'.$dataparent[$i]]));
    $prenomTea=htmlspecialchars(addslashes($_POST['prenomTea'.$dataparent[$i]]));
    $sexeTea=htmlspecialchars(addslashes($_POST['sexeTea'.$dataparent[$i]]));
    $contactTea=htmlspecialchars(addslashes($_POST['contactTea'.$dataparent[$i]]));

    $parent->AddParentBoucle($nomTea,$prenomTea,$sexeTea,$contactTea,$type_cpte,$codeEtab,$sessionEtab,$statut,$datecrea);

  }

  // $_SESSION['user']['addparok']="Parent ajouté avec succès";
  $_SESSION['user']['addparok']=L::ParentAddMessageSuccess;

  if($_SESSION['user']['profile'] == "Admin_globale")
  {
header("Location:../manager/addallparents.php");
  }else if($_SESSION['user']['profile'] == "Admin_locale")
  {

    $etablissementType=$routine->DetermineTypeEtab($codeEtab);
    $libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

    if($etablissementType==5)
    {

  header("Location:../locale".$libelleEtab."/addallparents.php");
    }else {
      header("Location:../locale/addallparents.php");
    }



  }

}else if(isset($_POST['etape'])&&($_POST['etape']==7)) {

  // photoTea:
  // oldphoto:
  // etape: 7
  $oldphoto=htmlspecialchars($_POST['oldphoto']);
  $cniTea=htmlspecialchars($_POST['cniTea']);
  $nationTea=htmlspecialchars($_POST['nationTea']);
  $nomTea=htmlspecialchars($_POST['nomTea']);
  $prenomTea=htmlspecialchars($_POST['prenomTea']);
  $sexeTea=htmlspecialchars($_POST['sexeTea']);
  $datenaisTea=dateFormat(htmlspecialchars($_POST['datenaisTea']));
  $lieunaisTea=htmlspecialchars($_POST['lieunaisTea']);
  $emailTea=htmlspecialchars($_POST['emailTea']);
  $contactTea=htmlspecialchars($_POST['contactTea']);
  $situationTea=htmlspecialchars($_POST['situationTea']);
  $session=htmlspecialchars($_POST['session']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $nbchieldTea=htmlspecialchars($_POST['nbchieldTea']);
  $nbchieldscoTea=htmlspecialchars($_POST['nbchieldscoTea']);
  $lieuHTea=htmlspecialchars($_POST['lieuHTea']);
  $adrespostaleTea=htmlspecialchars($_POST['adrespostaleTea']);
  $employeurTea=htmlspecialchars($_POST['employeurTea']);
  $fonctionTea=htmlspecialchars($_POST['fonctionTea']);
  $telburoTea=htmlspecialchars($_POST['telburoTea']);
  $adresproTea=htmlspecialchars($_POST['adresproTea']);
  $idcompte=htmlspecialchars($_POST['compteid']);


//nous allons modifier le compte du parent

$parent->UpdateParentPro($nomTea,$prenomTea,$datenaisTea,$sexeTea,$contactTea,$fonctionTea,$cniTea,$emailTea,$nationTea,$lieuHTea,$nbchieldTea,$nbchieldscoTea,$adresproTea,$employeurTea,$telburoTea,$adrespostaleTea,$situationTea,$lieunaisTea,$session,$codeEtab,$idcompte);

$fileError = $_FILES['photoTea']['error'];

// echo $fileError;

if($fileError==0)
{

  $file_name = @$_FILES['photoTea']['name'];

  $_SESSION["photoTea"] = $file_name;

  $file_size =@$_FILES['photoTea']['size'];

  $file_tmp =@$_FILES['photoTea']['tmp_name'];

  $file_type=@$_FILES['photoTea']['type'];

  @$file_ext=strtolower(end(explode('.',@$_FILES['photoTea']['name'])));

  $fichierTemp = uniqid() . "." . $file_ext;

  if(strlen($oldphoto)>0)
  {

    $tabphoto=explode(".",$oldphoto);
    $transactionId=$tabphoto[0];
    $fichierad=$transactionId.".".$file_ext;
  }else {
    $transactionId =  "PARENT".date("Y") . date("m").strtoupper(substr($emailTea, 0, 2));

     $fichierad=$transactionId.".".$file_ext;

  }

  move_uploaded_file($file_tmp,"../temp/".$fichierTemp);

  $dossier="../photo/";

  $dossier1="../photo/".$emailTea;

  if(!is_dir($dossier)) {

        //Le dossier n'existe pas - In procÃ¨de Ã  ssa crÃ©ation

        @mkdir($dossier);

        @mkdir($dossier1);


            }else
            {
               @mkdir($dossier1);
            }
  @rename('../temp/' . $fichierTemp , "../photo/" . $emailTea ."/".$fichierad);

    @unlink("../temp/" . $fichierTemp);


$parent->UpdateParentPhoto($fichierad,$idcompte);







}

$_SESSION['user']['addparok']=L::ParentInfosModOk;


$etablissementType=$routine->DetermineTypeEtab($codeEtab);
$libelleEtab=$routine->getEtabLibellebyCodeEtab($codeEtab);

if($etablissementType==5)
{

header("Location:../locale".$libelleEtab."/detailsparents.php?compte=".$idcompte);
}else {
  header("Location:../locale/detailsparents.php?compte=".$idcompte);
}



}else if(isset($_GET['etape'])&&($_GET['etape']==8)) 
{

}



 ?>
