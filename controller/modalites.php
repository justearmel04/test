<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
require_once('../controller/functions.php');

$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //Modification du compte pour un etablissement mixte

  //recupération des variables
  $studentid=htmlspecialchars($_POST['studentid']);
  $paiementplan=htmlspecialchars($_POST['paiementplan']);
  $sessionEtab=htmlspecialchars($_POST['sessionEtab']);
  $codeEtab=htmlspecialchars($_POST['codeEtab']);
  $payeur=htmlspecialchars($_POST['payeur']);
  $employeurName=htmlspecialchars($_POST['employeurName']);
  $nbmodalities=htmlspecialchars($_POST['nbmodalities']);
  $classeid=htmlspecialchars($_POST['classeid']);

  $libelleEtab=$etab->getEtabLibellebyCodeEtab($codeEtab);

  //nous allons ajouter le plan choisi par le parent

  $planid=$etab->AddplanVersement($studentid,$sessionEtab,$codeEtab,$paiementplan,$nbmodalities,$payeur,$employeurName);

  //ajouter les modalites de paiements

 for($i=1;$i<=$nbmodalities;$i++)
 {
   $datemodalite=dateFormat($_POST['date'.$i]);
   $montantmodalite=$_POST['montant'.$i];

   $etab->AddModalitesPlan($planid,$datemodalite,$montantmodalite);

 }

  // $_SESSION['user']['addteaok']="Les paramètres du compte ont bien été modifiés avec succès";
  $_SESSION['user']['updateteaok']=L::ComptesAddMessageSuccess;

  // $etab->UpdateStudentAccountCnx($loginTea,$passTea,$idcompte);

  if($_SESSION['user']['profile'] == "Admin_globale") {

  // header("Location:../manager/adddiplomes.php?compte=".$idcompte);
    }else if($_SESSION['user']['profile'] == "Admin_locale") {


        $etablissementType=$etab->DetermineTypeEtab($codeEtab);

        if($etablissementType==5)
        {
          header("Location:../locale".$libelleEtab."/paiementplanstudent.php?classe=".$classeid."&codeEtab=".$codeEtab);
        }else {
          header("Location:../locale/paiementplanstudent.php?classe=".$classeid."&codeEtab=".$codeEtab);
        }


        // header("Location:../locale/adddiplomes.php?compte=".$idcompte);



    }


}else if(isset($_POST['etape'])&&($_POST['etape']==2))
{



}

?>
