<?php
session_start();
require_once('../class/Classe.php');
require_once('../class/Etablissement.php');
// require_once('../intl/i18n.class.php');
//
// if(!isset($_SESSION['user']['lang']))
// {
//   $_SESSION['user']['lang']="fr";
// }
//
// $i18n = new i18n();
// $i18n->setCachePath('../langcache');
// $i18n->setFilePath('../intl/lang/lang_{LANGUAGE}.ini'); // language file path
// $i18n->setFallbackLang($_SESSION['user']['lang']);
// $i18n->setPrefix('L');
// $i18n->setForcedLang($_SESSION['user']['lang']); // force english, even if another user language is available
// $i18n->setSectionSeperator('_');
// $i18n->setMergeFallback(false);
// $i18n->init();
$classe = new Classe();
$etab=new Etab();

if(isset($_POST['etape'])&&($_POST['etape']==1))
{
  //ajout du diplôme

  //recupération des variables

  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));
  $libellesection=htmlspecialchars(addslashes($_POST['section']));
  $libellesession=htmlspecialchars(addslashes($_POST['libellesession']));
  $montantsection=htmlspecialchars(addslashes($_POST['montantsection']));
  $devise=htmlspecialchars(addslashes($_POST['devise']));
  $dateday=date("Y-m-d");
  $statut=1;

  $classe->AddSection($libellesection,$codeEtab,$libellesession,$montantsection,$devise,$dateday,$statut);

  $_SESSION['user']['addclasseok']="Nouvelle section ajoutée avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
  // header("Location:../manager/addclasses.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
        if($_SESSION['user']['paysid']==4)
        {
          header("Location:../localecmr/sections.php");
        }else {
          header("Location:../locale/sections.php");
        }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }



}if(isset($_POST['etape'])&&($_POST['etape']==2))
{
  //recuperation des variables

  $section=htmlspecialchars(addslashes($_POST['section']));
  $codeEtab=htmlspecialchars(addslashes($_POST['libetab']));
  $idsection=htmlspecialchars(addslashes($_POST['idsection']));
  $montantsection=htmlspecialchars(addslashes($_POST['montantsection']));
  $session=htmlspecialchars(addslashes($_POST['libellesession']));

  $classe->UpdateSection($section,$montantsection,$codeEtab,$session,$idsection);

  $_SESSION['user']['updateclasseok']="Section modifiée avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
  // header("Location:../manager/addclasses.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/sections.php");
      }else {
        header("Location:../locale/sections.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

}

if(isset($_GET['etape'])&&($_GET['etape']==3))
{
  //recuperation des variables


  $idsection=htmlspecialchars(addslashes($_GET['compte']));
  $statut=0;
  $classe->DeletedSection($idsection,$statut);

  $_SESSION['user']['updateclasseok']="Section supprimer avec succès";

  if($_SESSION['user']['profile'] == "Admin_globale") {
header("Location:../manager/schoolInfos.php?compte=".$codeEtab);
  // header("Location:../manager/addclasses.php");
    }else if($_SESSION['user']['profile'] == "Admin_locale") {
      if($_SESSION['user']['paysid']==4)
      {
        header("Location:../localecmr/sections.php");
      }else {
        header("Location:../locale/sections.php");
      }


      }else if($_SESSION['user']['profile'] == "Teatcher") {

            header("Location:../teatcher/index.php");

        }else if($_SESSION['user']['profile'] == "Student") {

              header("Location:../student/index.php");

          }else if($_SESSION['user']['profile'] == "Parent") {

                header("Location:../parent/index.php");

            }

}

?>
