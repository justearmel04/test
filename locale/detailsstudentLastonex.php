<?php

session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Matiere.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$mat=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();

$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);


if(strlen($imageprofile)>0)

{

  $lienphoto="../photo/".$emailUti."/".$imageprofile;

}else {

  $lienphoto="../photo/user5.jpg";

}




$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)

{

  //recuperer la session en cours

  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
  // $studentInfos=$student->getAllInformationsOfStudent($_GET['compte'],$libellesessionencours);
  $studentInfos=$student->getAllInformationsOfStudentOne($_GET['compte'],$libellesessionencours);
  $datascolarity=$etabs->DetermineScolarityStateOfStudent($codeEtabAssigner,$libellesessionencours,$_GET['compte']);

}

// $tabStudent=explode("*",$studentInfos);
// $studentparentid=$tabStudent[8];
$tabStudent=array();

foreach ($studentInfos as $personnal):
  $tabStudent[0]= $personnal->id_compte;
  $tabStudent[1]=$personnal->matricule_eleve;
  $tabStudent[2]= $personnal->nom_eleve;
  $tabStudent[3]=$personnal->prenom_eleve;
  $tabStudent[4]= $personnal->datenais_eleve;
  $tabStudent[5]=$personnal->lieunais_eleve;
  $tabStudent[6]= $personnal->sexe_eleve;
  $tabStudent[7]=$personnal->email_eleve;
  $tabStudent[8]=$personnal->email_eleve;
  $tabStudent[9]= $personnal->libelle_classe;
  $tabStudent[10]=$personnal->codeEtab_classe;
  $tabStudent[11]= $personnal->photo_compte;
  $tabStudent[12]=$personnal->tel_compte;
  $tabStudent[13]= $personnal->login_compte;
  $tabStudent[14]=$personnal->codeEtab_inscrip;
  $tabStudent[15]= $personnal->id_classe;
  $tabStudent[16]=$personnal->allergie_eleve;
  $tabStudent[17]=$personnal->condphy_eleve;
endforeach;
$infosparents=$parents->ParentInfostudent($_GET['compte']);

$allabsencesLast=$etabs->getListeAttendanceLast($tabStudent[1],$tabStudent[15],$codeEtabAssigner,$libellesessionencours);

// var_dump($allabsencesLast);
 ?>

<!DOCTYPE html>

<html lang="en">

<!-- BEGIN HEAD -->



<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <title>Application de communication écoles et parents</title>

    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">

    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">



    <!-- google font -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />

	<!-- icons -->

    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

	<!--bootstrap -->

   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />

	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- data tables -->

   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <!-- Material Design Lite CSS -->

	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >

	<link href="../assets2/css/material_style.css" rel="stylesheet">

	<!-- morris chart -->

    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />



	<!-- Theme Styles -->

    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />

    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>



	<!-- favicon -->

    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />

 </head>

 <!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">

    <div class="page-wrapper">

        <!-- start header -->

		<?php

include("header.php");

    ?>

        <!-- end header -->

        <!-- start page container -->

        <div class="page-container">

 			<!-- start sidebar menu -->

 			<?php

				include("menu.php");

			?>

			 <!-- end sidebar menu -->

			<!-- start page content -->

            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-bar">

                        <div class="page-title-breadcrumb">

                            <div class=" pull-left">

                                <div class="page-title">Fiche Elève</div>

                            </div>

                            <ol class="breadcrumb page-breadcrumb pull-right">

                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>

                                </li>

                                <li class="">Elèves</li>&nbsp;<i class="fa fa-angle-right"></i>

                                <li class="active">Fiche Elève</li>

                            </ol>

                        </div>

                    </div>

					<!-- start widget -->

					<div class="state-overview">

						<div class="row">



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->



					        <!-- /.col -->

					      </div>

						</div>

					<!-- end widget -->

          <?php



                if(isset($_SESSION['user']['addetabexist']))

                {



                  ?>

                  <div class="alert alert-danger alert-dismissible fade show" role="alert">

                <?php

                echo $_SESSION['user']['addetabexist'];

                ?>

                <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                <span aria-hidden="true">&times;</span>

                   </a>

                </div>







                  <?php

                  unset($_SESSION['user']['addetabexist']);

                }



                 ?>





          <div class="row">



            <div class="col-sm-4">

              <div class="card ">

                                    <div class="card-body no-padding height-9">

                                        <div class="row">

                                            <div class="profile-userpic">

                                              <?php

                                                  if(strlen($tabStudent[11])>0)

                                                  {

                                                    $lien="../photo/Students/".$tabStudent[1]."/".$tabStudent[11];

                                                  }else {

                                                    $lien="../photo/user9.jpg";

                                                  }

                                               ?>

                                                <img src="<?php echo $lien;?>" class="img-responsive" alt=""> </div>

                                        </div>

                                        <div class="profile-usertitle">

                                            <div class="profile-usertitle-name"><?php echo $tabStudent[2]." ".$tabStudent[3];?></div>

                                            <div class="profile-usertitle-job"> <?php echo $tabStudent[1];?></div>

                                        </div>

                                        <ul class="list-group list-group-unbordered">

                                            <li class="list-group-item">

                                                <b>Moyenne Trimestre</b> <a class="pull-right"></a>

                                            </li>

                                            <li class="list-group-item">

                                                <b>Conduite Trimestre</b> <a class="pull-right"></a>

                                            </li>



                                        </ul>

                                        <!-- END SIDEBAR USER TITLE -->

                                        <!-- SIDEBAR BUTTONS -->

                                        <div class="profile-userbuttons">

			<a  target="_blank" class="btn btn-circle btn-warning btn-sm" href="listePersonnelEleve.php?compte=<?php echo $_GET['compte']?>&sessionEtab=<?php echo $libellesessionencours; ?>"> <i class="fa fa-print"></i>Imprimer la  fiche Eleve
                        </a>


					<!--button type="button" class="btn btn-circle red btn-sm">Message</button-->

                                        </div>

                                        <!-- END SIDEBAR BUTTONS -->

                                    </div>

                                </div>

            </div>

            <div class="col-sm-8">

            								<div class="card-box">

            									<!--div class="card-head">

            										<header>INFORMATIONS</header>

            									</div-->

            									<div class="card-body ">

            						            <div class = "mdl-tabs mdl-js-tabs">

            						               <div class = "mdl-tabs__tab-bar tab-left-side">

            						                  <a href = "#tab4-panel" class = "mdl-tabs__tab is-active ">Informations générales </a>
                                          <a href = "#tab8-panel" class = "mdl-tabs__tab " >Scolarité</a>
                                          <a href = "#tab6-panel" class = "mdl-tabs__tab ">Notes</a>
                                          <a href = "#tab5-panel" class = "mdl-tabs__tab ">Absences</a>



            						               </div>

            						               <div class = "mdl-tabs__panel is-active p-t-20" id = "tab4-panel">

                                         <!-- <div class="card-body "style="margin-left:-22px;"> -->
                                         <div class="row">
                                           <div class="col-md-12">
                                             <fieldset style="">
                                               <legend style="text-align:center;">Informations de l'élève</legend>
                                             </fieldset>
                                           </div>

                                         </div>

                                        <div class="table-responsive">

                                            <table class="table table-striped custom-table table-hover" >

                                                <thead>



                                                </thead>

                                                <tbody>

                                                  <tr>

                                                      <td><a href="#">Matricule :</a>

                                                      </td>

                                                      <td ><?php echo $tabStudent[1];?></td>



                                                  </tr>

                                                    <tr>

                                                        <td><a href="#">Nom & Prénoms :</a>

                                                        </td>

                                                        <td ><?php echo $tabStudent[2]." ".$tabStudent[3];?></td>



                                                    </tr>



                                                    <tr>

                                                        <td><a href="#"> Classe : </a>

                                                        </td>

                                                        <td ><?php echo $tabStudent[9];?></td>



                                                    </tr>

                                                    <tr>

                                                        <td><a href="#"> Email : </a>

                                                        </td>

                                                        <td ><?php echo $tabStudent[7];?></td>



                                                    </tr>

                                                    <tr>

                                                        <td><a href="#">téléphone :</a>

                                                        </td>

                                                        <td ><?php echo $tabStudent[12];?></td>



                                                    </tr>

                                                    <tr>

                                                        <td><a href="#"> Sexe : </a>

                                                        </td>

                                                        <td ><?php

                                                        $sexe=$tabStudent[6];

                                                        if($sexe=="M")

                                                        {

                                                          echo "Masculin";

                                                        }else {

                                                          echo "Féminin";

                                                        }

                                                        ?></td>



                                                    </tr>

                                                    <tr>

                                                      <td><a href="#">Date de naissance :</a>

                                                        </td>

                                                        <td ><?php echo date_format(date_create($tabStudent[4]),"d/m/Y");?></td>



                                                    </tr>



                                                </tbody>

                                            </table>

                                            </div>

                                            <div class="row">
                                              <div class="col-md-12">
                                                <fieldset style="">
                                                  <legend style="text-align:center;">Informations Parents</legend>
                                                </fieldset>
                                              </div>

                                            </div>
                                            <div class="table-responsive">

                                                <table class="table table-striped custom-table table-hover">

                                                    <thead>
                                                      <th>Nom & Prénoms :</th>
                                                      <th>Email :</th>
                                                      <th>Contact :</th>
                                                      <th>Profession:</th>


                                                    </thead>

                                                    <tbody>
                                                      <?php
                                                      foreach ($infosparents as $value):
                                                       ?>

                                                        <tr>

                                                            <td><?php echo $value->nom_compte." ".$value->prenom_compte; ?></td>
                                                            <td><?php echo $value->email_compte; ?></td>
                                                            <td ><?php echo $value->tel_compte; ?></td>
                                                            <td ><?php echo $value->fonction_compte; ?></td>






                                                        </tr>

                                                      <?php
                                                    endforeach;
                                                       ?>
                                                    </tbody>

                                                </table>

                                                </div>

                                        <!-- </div> -->

            						               </div>

            						               <div class = "mdl-tabs__panel p-t-20" id = "tab5-panel">

                                         <div class="card-body " style="margin-left:-22px;">

                                        <div class="table-responsive">

                                            <table class="table table-striped custom-table table-hover">

                                                <thead>
                                                  <th>Date:</th>
                                                  <th>Matière :</th>
                                                  <!-- <th>Coefficient:</th> -->
                                                  <th>Heure début :</th>
                                                  <th>Heure fin:</th>



                                                </thead>

                                                <tbody>
                                                  <?php
                                                  foreach ($allabsencesLast as $value):
                                                   ?>

                                                    <tr>

                                                        <td><?php echo $value->date_presence; ?></td>
                                                        <td><?php echo $etabs->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence); ?></td>
                                                        <!-- <td ><?php //echo $value->tel_compte; ?></td> -->
                                                        <td ><?php echo $value->heuredeb_heure; ?></td>
                                                        <td ><?php echo $value->heurefin_heure; ?></td>






                                                    </tr>

                                                  <?php
                                                endforeach;
                                                   ?>
                                                </tbody>

                                            </table>

                                            </div>

                                        </div>

            						               </div>

                                       <div class = "mdl-tabs__panel p-t-20" id = "tab6-panel">

            						                 <?php

                                         //verifier si cet eleve à au moins une note dans un examen



                                         $numberOfExamen=$student->getNumberOfExamNoteOfStudent($_GET['compte']);



                                         if($numberOfExamen)

                                         {

                                           //nous allons afficher la liste des examens ainsi aue les notes et observations



                                           $examenalls=$student->getExameNotesOfStudent($_GET['compte']);

                                           ?>

                                           <div class="row">

                        <div class="col-md-12">

                            <div class="card card-box">

                                <div class="card-head">

                                    <header></header>

                                    <div class="tools">

                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                    </div>

                                </div>

                                <div class="card-body ">

                                  <div class="row">
                                    <div class="col-md-12">
                                      <fieldset style="">
                                        <legend style="text-align:center;">Notes Examen</legend>
                                      </fieldset>
                                    </div>

                                  </div>

                                    <table id="example1" style="width:100%;">

                                        <thead>

                                            <tr>

                                                <th>Examens </th>

                                                <th>Note</th>

                                                <th>Observation</th>



                                        </thead>

                                        <tbody>

                                          <?php

                                          $j=1;

                                          foreach ($examenalls as $value):

                                           ?>

                                            <tr>

                                                <td><span class="label label-sm label-success"> <?php echo $value->libelle_exam ?></span></td>

                                                <td><?php echo $value->valeur_notes; ?></td>

                                                <td><?php echo $value->obser_notes; ?></td>



                                            </tr>

                                          <?php

                                          $j++;

                                          endforeach;

                                           ?>



                                        </tbody>

                                    </table>



                                </div>

                            </div>

                        </div>

                    </div>

                                           <?php

                                         }else {

                                             ?>

                                             <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                          Aucune Note d'examen à ce jour

                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                           <span aria-hidden="true">&times;</span>

                                              </a>

                                           </div>

                                           <?php

                                         }



                                          ?>

                                          <div class="row">
                                            <div class="col-md-12">
                                              <fieldset style="">
                                                <legend style="text-align:center;">Notes Controle</legend>
                                              </fieldset>
                                            </div>

                                          </div>
                                          <?php

                                          //verifier si cet eleve à au moins une note dans un examen



                                          $numberOfControle=$student->getNumberOfControleNoteOfStudent($_GET['compte']);



                                          if($numberOfControle>0)

                                          {

                                            //$controlealls=$student->getControleNotesOfStudent($_GET['compte']);
                                            $controlealls=$student->getControleNotesOfStudentLimited($_GET['compte']);

                                           ?>

                                           <div class="row">

                                           <div class="col-md-12">

                                           <div class="card card-box">

                                           <div class="card-head">

                                           <header></header>

                                           <!-- <div class="tools">

                                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                           <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                           <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                           </div> -->

                                           </div>

                                           <div class="card-body ">

                                           <table id="example1" style="width:100%;">

                                           <thead>

                                            <tr>
                                                <th>Matière</th>
                                                <th>Controle </th>
                                                <th>Note</th>
                                                <th>Observation</th>
                                              </tr>




                                           </thead>

                                           <tbody>

                                           <?php

                                           $j=1;

                                           foreach ($controlealls as $value):

                                           ?>

                                            <tr>
                                               <td><span class="label label-sm label-success"> <?php echo $mat->getMatiereLibelleByIdMat($value->mat_ctrl,$value->codeEtab_ctrl); ?></span></td>
                                                <td><span class="label label-sm label-success"> <?php echo $value->libelle_ctrl; ?></span></td>

                                                <td><?php echo $value->valeur_notes; ?></td>

                                                <td><?php echo $value->obser_notes; ?></td>



                                            </tr>

                                           <?php

                                           $j++;

                                           endforeach;

                                           ?>



                                           </tbody>

                                           </table>

                                           </div>

                                           </div>

                                           </div>

                                           </div>



                                          <?php





                                          }else if($numberOfControle==0)

                                          {

                                            ?>

                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                          Aucune Note de contrôle à ce jour

                                           <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                           <span aria-hidden="true">&times;</span>

                                              </a>

                                           </div>

                                            <?php

                                          }



                                           ?>




            						               </div>



                                      <div class = "mdl-tabs__panel p-t-20" id = "tab8-panel">

                                      <?php

                                      $nbversement=count($datascolarity);

                                      if($nbversement==0)

                                      {

                                        ?>
                                        <a href="scolaritestudent.php?student=<?php echo $_GET['compte'] ?>&classe=<?php echo $tabStudent[15]; ?>" class="btn btn-primary btn-md"><i class="fa fa-plus"></i> Versement</a>
                                        <br><br>

                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">

                                     Aucun Paiement de scolarité à ce jour

                                      <a href="#" class="close" data-dismiss="alert" aria-label="Close">

                                      <span aria-hidden="true">&times;</span>

                                         </a>

                                      </div>


                                        <?php

                                      }else if($nbversement>0)

                                      {

                                        ?>

                                        <div class="row">

                     <div class="col-md-12">

                         <div class="card card-box">

                             <div class="card-head">

                                 <header></header>

                                 <div class="tools">

                                     <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                                   <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

                                   <a class="t-close btn-color fa fa-times" href="javascript:;"></a>

                                 </div>

                             </div>

                             <div class="card-body ">
                               <a href="scolaritestudent.php?student=<?php echo $_GET['compte'] ?>&classe=<?php echo $tabStudent[15]; ?>" class="btn btn-primary btn-md"><i class="fa fa-plus"></i> Versement</a>
                               <br><br>

                                 <table id="example1" style="width:100%;">

                                     <thead>

                                         <tr>

                                             <th>Versements </th>

                                             <th>Date</th>

                                             <th>Montant Versé</th>

                                             <th>Reste a payer</th>



                                     </thead>

                                     <tbody>

                                       <?php

                                       $j=1;

                                       foreach ($datascolarity as $value):

                                        ?>

                                         <tr>

                                             <td><span class="label label-sm label-info"><?php echo $value->code_versement; ?></span></td>

                                             <td><?php echo date_format(date_create($value->date_versement),"d-m-Y")?></td>

                                             <td><?php echo $value->montant_versement. " ".$value->devise_versement;?></td>

                                             <td><?php

                                             $resteapayer=$value->solde_versement;

                                             if($resteapayer==0)

                                             {

                                               ?>

                                               <span class="label label-sm label-success">Soldé</span>

                                               <?php

                                             }else if($resteapayer>0)

                                             {

                                               ?>

                                                <span class="label label-sm label-danger"><?php echo $value->solde_versement. " ".$value->devise_versement;?></span>

                                               <?php

                                             }

                                             ?></td>



                                         </tr>

                                       <?php

                                       $j++;

                                       endforeach;

                                        ?>



                                     </tbody>

                                 </table>

                             </div>

                         </div>

                     </div>

                 </div>

                                        <?php



                                      }

                                       ?>

                                     </div>



            						            </div>

            									</div>

            								</div>

            							</div>
                          <div class="col-md-4">

                          </div>
                          <div class="col-sm-8">

                          								<div class="card-box">

                          									<div class="card-head">

                          										<header>INFORMATIONS DE COMPTE & CONNEXION</header>

                          									</div>

                          									<div class="card-body ">



                          									</div>

                          								</div>

                          							</div>

          </div>





                     <!-- start new patient list -->



                    <!-- end new patient list -->



                </div>

            </div>

            <!-- end page content -->

            <!-- start chat sidebar -->



            <!-- end chat sidebar -->

        </div>

        <!-- end page container -->

        <!-- start footer -->

        <div class="page-footer">

            <div class="page-footer-inner"> 2019 &copy;

            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>

            </div>

            <div class="scroll-to-top">

                <i class="material-icons">eject</i>

            </div>

        </div>

        <!-- end footer -->

    </div>

    <!-- start js include path -->

    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>

 	<script src="../assets2/plugins/popper/popper.min.js" ></script>

     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>

 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>

     <!-- bootstrap -->

     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>

     <!-- calendar -->

     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>

     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>

     <!-- Common js-->

 	<script src="../assets2/js/app.js" ></script>

     <script src="../assets2/js/layout.js" ></script>

 	<script src="../assets2/js/theme-color.js" ></script>

 	<!-- Material -->

 	<script src="../assets2/plugins/material/material.min.js"></script>









    <!-- morris chart -->

    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>

    <script src="../assets2/plugins/morris/raphael-min.js" ></script>

    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->



   <script>





   function generatefichepdf(idcompte)

   {

     var codeEtab="<?php echo $codeEtabAssigner ?>";

      var etape=2;

       $.ajax({

         url: '../ajax/admission.php',

         type: 'POST',

         async:false,

         data: 'compte=' +idcompte+ '&etape=' + etape+'&codeEtab='+ codeEtab,

         dataType: 'text',

         success: function (content, statut) {



          window.open(content, '_blank');



         }

       });

   }



   $(document).ready(function() {







   });



   </script>

    <!-- end js include path -->

  </body>



</html>
