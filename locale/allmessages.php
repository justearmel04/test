<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);


if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllClassesbyschoolCode($codeEtabAssigner);


$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  // $notifications=$etabs->getAllNotificationbySchoolCode($codeEtabAssigner,$libellesessionencours);
  // $notifications=$etabs->getAllNotificationActivitybySchoolCode($codeEtabAssigner,$libellesessionencours);
  $notifications=$etabs->getAllMessages($codeEtabAssigner,$libellesessionencours);
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
  <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
  <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets2/css/material_style.css" rel="stylesheet">
  <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

  <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

  <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Liste des Notifications : </div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li>&nbsp;<a class="parent-item" href="#">Notifications</a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>

                              <li class="active">Liste des Notifications</li>
                          </ol>
                      </div>
                  </div>

					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['updateteaok']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
        <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
        <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['updateteaok']; ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['updateteaok']);
                }

                 ?>
          <!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

                 <div class="row">
                                         <div class="col-md-12">
                                             <div class="tabbable-line">
                                                <!--ul class="nav nav-pills nav-pills-rose">
                 									<li class="nav-item tab-all"><a class="nav-link active show"
                 										href="#tab1" data-toggle="tab">Liste</a></li>
                 									<li class="nav-item tab-all"><a class="nav-link" href="#tab2"
                 										data-toggle="tab">Grille</a></li>
                 								</ul-->
                                                 <div class="tab-content">
                                                     <div class="tab-pane active fontawesome-demo" id="tab1">
                                                         <div class="row">
                 					                        <div class="col-md-12">
                 					                            <div class="card  card-box">
                 					                                <div class="card-head">
                 					                                    <header></header>
                 					                                    <div class="tools">
                 					                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                 						                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                 						                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                 					                                    </div>
                 					                                </div>

                 					                                <div class="card-body ">



                 					                                  <div class="table-scrollable">
                                                              <div class="pull-right">
                                                                  <a class="btn btn-primary " style="border-radius:5px;" href="addmessages.php"><i class="fa fa-plus"></i> Nouvelle notification</a>

                                                              </div>
                                                              <br><br>
                                                               <?php

                                                                ?>
                 					                                    <table class="table table-hover table-checkable order-column full-width" id="example4">
                 					                                        <thead>
                 					                                            <tr>

                 					                                                <th> Id</th>
                 					                                                <th>Libelle Notification </th>
                 					                                                <th> Classes </th>
                                                                            <th>Destinataires</th>
                 					                                                 <th> Type Envoi </th>
                                                                            <th> Statut </th>
                                                                            <th> <?php echo L::Actions?> </th>
                 					                                            </tr>
                 					                                        </thead>
                                                                   <?php
                                                                   if($nbsessionOn>0)
                                                                   {
                                                                    ?>
                                                                   <tbody>
                                                                     <?php
                                                                     $i=1;
                                                                     foreach ($notifications as $value):
                                                                      ?>
                                                                       <tr class="odd gradeX">
                                                                       <td><?php echo $i ?></td>
                                                                       <td>
                                                                         <?php

                                                                         if($value->parascolaire_msg==1)
                                                                         {
                                                                           // echo "parascolaire";
                                                                           echo $etabs->getparacolaireDesignation($value->id_msg);
                                                                         }else if($value->scola_msg==1)
                                                                         {
                                                                           // echo "scolarite";
                                                                         }else if(($value->parascolaire_msg==0)&&($value->scola_msg==0))
                                                                         {
                                                                           // echo "note observation";
                                                                           if($value->objet_msg==8)
                                                                           {
                                                                             echo $value->other_msg;
                                                                           }else {
                                                                             echo $value->libelle_msg;
                                                                           }
                                                                         }
                                                                          ?>
                                                                       </td>
                                                                       <td><?php
                                                                        $dataclasse=$value->classes_msg;
                                                                        $tabdataclasse=explode("-",$dataclasse);
                                                                        $nbtabdataclasse=count($tabdataclasse);
                                                                        $limitdataclasse=$nbtabdataclasse-1;
                                                                        // echo $limitdataclasse;

                                                                        for($z=0;$z<$limitdataclasse;$z++)
                                                                        {
                                                                          ?>

                                                                            <span class="label label-sm label-success" style=""> <?php echo $classe->getInfosofclassesbyId($tabdataclasse[$z],$libellesessionencours); ?> </span>


                                                                         <?php

                                                                        }
                                                                        ?>
                                                                       </td>
                                                                       <td>
                                                                         <?php
                                                                         $datadestinataire=$value->destinataires_msg;
                                                                         $tabdatadestinataires=explode("-",$datadestinataire);
                                                                         $nbtabdatadestinataires=count($tabdatadestinataires);
                                                                         $limitdatadestinataires=$nbtabdatadestinataires-1;

                                                                         for($k=0;$k<$limitdatadestinataires;$k++)
                                                                         {
                                                                           ?>
                                                                           <span class="label label-sm label-warning"> <?php echo $tabdatadestinataires[$k]; ?> </span>
                                                                          <?php

                                                                         }
                                                                         ?>
                                                                       </td>
                                                                       <td>
                                                                         <?php
                                                                           $envoitype="";
                                                                           if($value->sms_msg==0)
                                                                           {
                                                                             if($value->email_msg==0)
                                                                             {

                                                                             }else if($value->email_msg==1)
                                                                             {
                                                                               echo "EMAIL";
                                                                             }
                                                                           }else if($value->sms_msg==1){
                                                                             if($value->email_msg==0)
                                                                             {
                                                                               echo "SMS";
                                                                             }else if($value->email_msg==1)
                                                                             {
                                                                               echo "SMS / EMAIL";
                                                                             }
                                                                           }
                                                                          ?>
                                                                       </td>
                                                                       <td>
                                                                         <?php
                                                                           $statutnotif=$value->statut_msg;

                                                                           if($statutnotif==0)
                                                                           {
                                                                             ?>
                                             <button type="button" class="btn btn-circle btn-danger btn-xs m-b-10">En attente</button>
                                                                             <?php

                                                                           }else if($statutnotif==1)
                                                                           {
                                                                             ?>
                                           <button type="button" class="btn btn-circle btn-success btn-xs m-b-10">Envoyé</button>
                                                                             <?php
                                                                           }else if($statutnotif==2)
                                                                           {
                                                                             ?>

                                                                             <?php
                                                                           }
                                                                        ?>
                                                                       </td>
                                                                       <td>
                                                                         <?php
                                                                           $statutnotif=$value->statut_msg;
                                                                           $parascolairestatus=$value->parascolaire_msg;
                                                                           $scolastatus=$value->scola_msg;

                                                                           if($statutnotif==0)
                                                                           {
                                                                             ?>
                                                                             <a href="#"  title="Envoyer" onclick="send(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,'<?php echo $value->session_msg ?>',<?php echo $value->addby_msg?>)" class="btn btn-info btn-circle ">
                                                                               <i class="fa fa-send"></i>
                                                                             </a>
                                                                             <a class="btn btn-danger btn-circle  " onclick="deleted(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)"  title="Supprimer">
                                                                               <i class="fa fa-trash-o "></i>
                                                                             </a>
                                                                             <?php
                                                                             if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                                             {
                                                                              ?>
                                                                             <a href="#" class="btn btn-warning btn-circle  " onclick="etatmessage(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,<?php echo $value->precis_msg ?>,<?php echo $value->addby_msg?>)"> <i class="fa fa-print"></i> </a>
                                                                             <?php
                                                                           }
                                                                              ?>
                                                                              <?php

                                                                           }else if($statutnotif==1)
                                                                           {
                                                                             ?>
                                                                             <?php
                                                                             if(($parascolairestatus==1)||(($parascolairestatus==0)&&($scolastatus==0)))
                                                                             {
                                                                              ?>
                                                                             <a href="#" class="btn btn-info btn-circle  " onclick="etatmessage(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>,<?php echo $value->precis_msg ?>,<?php echo $value->addby_msg?>)"> <i class="fa fa-print"></i> </a>
                                                                             <?php
                                                                           }
                                                                              ?>

                                                                             <a class="btn btn-warning btn-circle  " title="archiver" onclick="archiver(<?php echo $value->id_msg; ?>,<?php echo $value->parascolaire_msg; ?>,<?php echo $value->scola_msg; ?>)">
                                                                               <i class="fa fa-repeat "></i>
                                                                             </a>

                                                                             <?php
                                                                           }else if($statutnotif==2)
                                                                           {
                                                                             ?>

                                                                             <?php
                                                                           }
                                                                        ?>
                                                                       </td>
                                                                       </tr>
                                                                       <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                                                                       <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                                                                       <script type="text/javascript">

                                                                       function etatmessage(id_msg,parascolaire_msg,scola_msg,precis_msg,addby_msg)
                                                                       {
                                                                         document.location.href="etatmessages.php?message="+id_msg+"&parascolaire="+parascolaire_msg+"&precis="+precis_msg+"&addby="+addby_msg;
                                                                       }

                                                                       function archiver(id,paraid,scolarid)
                                                                       {
                                                                         var session="<?php echo $libellesessionencours; ?>";
                                                                         var codeEtab="<?php echo $codeEtabAssigner; ?>";
                                                                         Swal.fire({
                                                           title: '<?php echo L::WarningLib ?>',
                                                           text: "<?php echo L::DoyouReallyArchivedMessages ?>",
                                                           type: 'warning',
                                                           showCancelButton: true,
                                                           confirmButtonColor: '#2CA8FF',
                                                           cancelButtonColor: '#d33',
                                                           confirmButtonText: '<?php echo L::Archived ?>',
                                                           cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                                         }).then((result) => {
                                                           if (result.value) {
                                                             document.location.href="../controller/messaging.php?etape=5&notifid="+id+'&paraid='+paraid+'&scolarid='+scolarid+'&session='+session+'&codeEtab='+codeEtab;

                                                           }else {

                                                           }
                                                         })
                                                                       }

                                                                       function deleted(id,paraid,scolaid)
                                                                       {
                                                                         // var notificationid="<?php //echo $value->id_notif; ?>";
                                                                         // var destinataires="<?php //echo $value->destinataires_notif; ?>";
                                                                         // var classes="<?php //echo  $value->classes_notif;?>";
                                                                         var etape=4;
                                                                         // var codeEtab="<?php  //echo $value->codeEtab_notif;?>";

                                                                         Swal.fire({
                                                           title: '<?php echo L::WarningLib ?>',
                                                           text: "<?php echo L::DoyouReallyDeletedMessages ?>",
                                                           type: 'warning',
                                                           showCancelButton: true,
                                                           confirmButtonColor: '#2CA8FF',
                                                           cancelButtonColor: '#d33',
                                                           confirmButtonText: 'Supprmer',
                                                           cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                                         }).then((result) => {
                                                           if (result.value) {
                                                             document.location.href="../controller/messaging.php?etape=4&notifid="+id+"&paraid="+paraid+'&scolaid='+scolaid;

                                                           }else {

                                                           }
                                                         })
                                                                       }


                                                                       function send(id,paraid,scolaire,sessionEtab,addby)
                                                                       {
                                                                         // var sessionEtab="<?php //echo $libellesessionencours  ?>";
                                                                         //nous allons recuperer les elements essentiels pour l'envoi de mail ou sms
                                                                         // var etape1=4;
                                                                         var etape1=7;
                                                                         $.ajax({
                                                                           url: '../ajax/message.php',
                                                                           type: 'POST',
                                                                           async:false,
                                                                             data: 'notifid='+id+'&etape='+etape1+'&paraid='+paraid+'&scolaire='+scolaire+'&sessionEtab='+sessionEtab+'&addby='+addby,
                                                                           dataType: 'text',
                                                                             success: function (content, statut) {


                                                                               var tabcontent=content.split("*");
                                                                               var destinataires=tabcontent[0];
                                                                               var classes=tabcontent[1];
                                                                               var codeEtab=tabcontent[2];
                                                                               var smssender=tabcontent[3];
                                                                               var emailsender=tabcontent[4];
                                                                               var precis=tabcontent[5];
                                                                               var eleves=tabcontent[6];
                                                                               var scolarities=tabcontent[9];
                                                                               var etape2=5;

                                                                               var sessionEtab="<?php echo $libellesessionencours; ?>";

                                                                               Swal.fire({
                                                                 title: '<?php echo L::WarningLib ?>',
                                                                 text: "<?php echo L::DoyouReallySendingThisMessages ?>",
                                                                 type: 'warning',
                                                                 showCancelButton: true,
                                                                 confirmButtonColor: '#2CA8FF',
                                                                 cancelButtonColor: '#d33',
                                                                 confirmButtonText: '<?php echo L::Sendbutton ?>',
                                                                 cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                                                               }).then((result) => {
                                                                 if (result.value) {
                                                                   // document.location.href="../controller/messages/etape=3&notifid="+notificationid+"&destinataires="+destinataires+"&classes="+classes;
                                                                   $.ajax({
                                                                     url: '../ajax/message.php',
                                                                     type: 'POST',
                                                                     async:false,
                                                                     data: 'notifid='+id+'&etape='+etape2+'&codeEtab='+codeEtab+'&destinataires='+destinataires+'&classes='+classes+'&smssender='+smssender+'&emailsender='+emailsender+"&sessionEtab="+sessionEtab+'&precis='+precis+'&eleves='+eleves,
                                                                     dataType: 'text',
                                                                       success: function (content, statut) {

                                                                         var tab=content.split("/");
                                                                         var destimails=tab[0];
                                                                         var destiphones=tab[1];

                                                               document.location.href="../controller/messaging.php?etape=3&notifid="+id+"&destimails="+destimails+"&destiphones="+destiphones+"&codeEtab="+codeEtab+"&smssender="+smssender+"&emailsender="+emailsender+"&precis="+precis+"&eleves="+eleves+"&paraid="+paraid+"&scolarities="+scolarities;


                                                                       }
                                                                   });
                                                                 }else {

                                                                 }
                                                               })

                                                                             }
                                                                         });







                                                                       }

                                                                       </script>

                                                                       <?php
                                                                       $i++;
                                                                       endforeach
                                                                        ?>



                                                                   </tbody>
                                                                   <?php
                                                                 }
                                                                   ?>
                 					                                    </table>
                 					                                    </div>
                 					                                </div>
                 					                            </div>
                 					                        </div>
                 					                    </div>
                                                     </div>

                                                 </div>
                                             </div>
                                         </div>
                                     </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);



     var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: [<?php  echo $etabs->getMonthRevenu($codeEtabAssigner,$libellesessionencours); ?>],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                      <?php echo $etabs->getMonthRevenustandby($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                    <?php echo $etabs->getMonthRevenureceive($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($codeEtabAssigner,$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($codeEtabAssigner,$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "Filles",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "Garçons"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


     var calendar = $('#calendar1').fullCalendar({
       monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
       monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun','Jul','Aot','Sep','Oct','Nov','Dec'],
       dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
       dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
       timeFormat:'H(:mm)',
       timeZone: 'UTC',
       allDayText:'tous les jours',
       // minTime: "08:00",
       // maxTime: "20:00",
       buttonText:{
  today:    'AUJOURDHUI',
  month:    'MOIS',
  week:     'SEMAINE',
  day:      'JOUR',
  list:     'list'
},
    locale: 'fr',
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events:'../ajax/load.php?view=1',
   // events:loader(),
    // events: 'load.php?codeEtab='+codeEtab+'&session='+session+'&etape='+etape,
    selectable:false,
    selectHelper:false,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:false,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
   },
   eventClick:function(event)
   {
     var id = event.id;
     var etape=7;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";
     // alert(id);
     $.ajax({
       url: '../ajax/login.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&parascolaire='+id,
       dataType: 'text',
       success: function (content, statut) {

         // var data=content;
         var cut = $.trim(content);
         $tabcontent=cut.split("*");

         $("#oldparentname").val($tabcontent[0]);
         $("#oldparentprename").val($tabcontent[1]+" "+$tabcontent[2]);
         $("#oldparentsexe").val($tabcontent[3]+" "+$tabcontent[4]);
         $("#oldparentphone").val($tabcontent[5]);
         $("#descri").html($tabcontent[10]);

         var gratuit=$tabcontent[7];
         var montant=$tabcontent[8];

         if(gratuit==0)
         {
           $("#rowgratuit").show();
           $("#rowpayant").hide();

         }else {
           $("#rowpayant").show();
           $("#rowgratuit").hide();
           $("#montpayant").html(montant+" FCFA");
         }

         // alert($tabcontent[11]);

         // callback(data);

          $("#parentOld").click();

       }
     });

   },

    // ,
    //
    // eventClick:function(event)
    // {
    //  if(confirm("Are you sure you want to remove it?"))
    //  {
    //   var id = event.id;
    //   $.ajax({
    //    url:"delete.php",
    //    type:"POST",
    //    data:{id:id},
    //    success:function()
    //    {
    //     calendar.fullCalendar('refetchEvents');
    //     alert("Event Removed");
    //    }
    //   })
    //  }
    // },

   });

   });

   </script>
     <!-- end js include path -->
</body>


</html>
