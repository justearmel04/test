<?php
session_start();
require_once('../controller/functions.php');
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Matiere.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$userId=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$matiere=new Matiere();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$codeEtabLocal=$etabs->getcodeEtabByLocalId($userId);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabLocal);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabLocal);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabLocal);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabLocal,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabLocal,$libellesessionencours);
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets2/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::Reacpsdesabsences ?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.php"><?php echo L::Presences ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Récapitulatif des absences</li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
					<div class="state-overview">
						<div class="row">
              <?php

                    if(isset($_SESSION['user']['addattendailyok']))
                    {

                      ?>
                      <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                    <?php
                    //echo $_SESSION['user']['addetabok'];
                    ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                       </a>
                    </div-->
            <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
            <script src="../assets/js/sweetalert2.min.js"></script>

                <script>
                Swal.fire({
                type: 'success',
                title: 'Félicitations',
                text: '<?php echo $_SESSION['user']['addattendailyok']; ?>',

                })
                </script>
                      <?php
                      unset($_SESSION['user']['addattendailyok']);
                    }

                     ?>
					      </div>
						</div>
					<!-- end widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>

<br/>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="card card-topline-green">
                   <div class="card-head">
                       <header><?php echo L::Reacpsdesabsences ?></header>
                       <div class="tools">
                           <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                       </div>
                   </div>
                   <div class="card-body ">
                     <form method="post" id="FormSearch" action="recapattendanceclassematiere.php">
                         <div class="row">
                           <div class="col-md-6 col-sm-6">
                           <!-- text input -->
                           <div class="form-group" style="margin-top:8px;">
                               <label>Classe</label>
                               <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                               <select class="form-control input-height" id="classeEtab" name="classeEtab" style="width:100%;" onchange="searchmatiere()">
                                   <option value=""><?php echo L::Selectclasses ?></option>
                                   <?php
                                   $i=1;
                                     foreach ($classes as $value):
                                     ?>
                                     <option value="<?php echo utf8_encode(utf8_decode($value->id_classe)); ?>"><?php echo utf8_encode(utf8_decode($value->libelle_classe)); ?></option>

                                     <?php
                                                                      $i++;
                                                                      endforeach;
                                                                      ?>

                               </select>
                           </div>


                       </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::MatiereMenusingle ?></label>
                           <select class="form-control " name="matiereid" id="matiereid" style="width:100%;" >

                           </select>

                       </div>


                   </div>
                       <div class="col-md-6 col-sm-6">
                       <!-- text input -->
                       <div class="form-group" style="margin-top:8px;">
                           <label><?php echo L::DatedebLib ?></label>
                           <input type="text" id="datedeb" name="datedeb" class="form-control" placeholder="<?php echo L::EntersDatedebLib ?>">

                           <input type="hidden" name="search" id="search" />
                       </div>


                   </div>
                   <div class="col-md-6 col-sm-6">
                   <!-- text input -->
                   <div class="form-group" style="margin-top:8px;">
                       <label><?php echo L::DatefinLib ?></label>
                       <!--input type="text" id="codeetab" name="codeetab" class="form-control" placeholder="Enter ..."-->
                       <input type="text" id="datefin" name="datefin" class="form-control" placeholder="<?php echo L::EntersDatefinLib ?>">

                   </div>


               </div>
                   <button type="submit" class="btn btn-success"  style="width:150px;height:35px;margin-top:33px;"><?php echo L::AfficherleRecap ?></button>

                         </div>


                     </form>
                   </div>
               </div>
                        </div>

          </div>
<!-- element à faire apparaitre au clique du bouton rechercher -->
<div class="row" style="" id="affichage">
  <?php
      if(isset($_POST['search']))
      {
          if(isset($_POST['classeEtab'])&&isset($_POST['datedeb'])&&isset($_POST['datefin'])&&isset($_POST['matiereid']))
          {
              //nous devons recupérer la liste des jours du mois selectionner

               //$num = cal_days_in_month(CAL_GREGORIAN, retranscrireMois($mois), $annee);

               // $moisconcat=$_POST['month'];
               // $annee=$_POST['yearsession'];
               // $tabmoisconcat=explode("-",$moisconcat);
               // $mois=$tabmoisconcat[0];
               // $numbermois=$tabmoisconcat[1];
               // $num = cal_days_in_month(CAL_GREGORIAN,$numbermois, $annee);

               // $concat=$annee."-".regiveMois($numbermois)."-";

               $datetime1 = date_create($_POST['datedeb']); // Date fixe
               $datetime2 = date_create($_POST['datefin']); // Date fixe
               $interval = date_diff($datetime1, $datetime2);
               $nb= $interval->format('%a');
               // $datedebut=new DateTime($_POST['datedeb']);
               // $datefin=new DateTime($_POST['datefin']);
               //
               // $interval = date_diff($datedebut,$datefin);

               $infosclasses=$classe->getInfosofclassesbyId($_POST['classeEtab'],$libellesessionencours);

               //nous allons chercher la liste des eleves de cette classe
               // $students=$student->getAllstudentofthisclasses($_POST['classeEtab']);
               $students=$student->getAllstudentofthisclassesSession($_POST['classeEtab'],$libellesessionencours);

               // $students=$student->getTheSpecificStudentForSchool($codeEtabLocal,$_POST['classeEtab'],$_POST['eleveid']);


               // getTheSpecificStudentForSchool($compteEtab,$classeId,$idstudent)

              ///var_dump($students);

              $tabmatiere=explode("-",$_POST['matiereid']);
              $matiereid=$tabmatiere[0];


          }
          ?>

          <div class="offset-md-4 col-md-4"  id="affichage1">
            <div class="card" style="">
            <div class="card-body">
              <h5 class="card-title"></h5>
              <h4 style="font-style: italic;font-weight: bold;text-align:center;font-size:font-size: large;">Récapitulatif des absences Matière</h4>
              <p class="card-text" style="text-align:center;font-weight: bold;"><?php //echo @$infosclasses; ?></p>
              <p class="card-text" style="text-align:center;font-weight: bold"><?php echo $_POST['datedeb'].' au '.$_POST['datefin'] ?></p>

            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card card-box">
                  <div class="card-head">
                      <header></header>
                      <div class="tools">
                          <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                      </div>
                  </div>
                  <div class="card-body ">
                    <div class="pull-right">
                      <a href="#" class="btn btn-md btn-warning" onclick="recapattendanceDay('<?php echo $_POST['datedeb']; ?>','<?php echo $nb;  ?>',<?php echo $_POST['classeEtab']  ?>,'<?php echo $libellesessionencours ?>','<?php echo $codeEtabLocal  ?>','<?php echo $matiereid; ?>')"><i class="fa fa-print"></i>Imprimer </a>
                    </div>
                      <table id="tableGroup" class="display" style="width: 100%">
          <thead>
              <tr>
                  <th>Matricule</th>
                  <th>Nom & prénoms</th>
                  <th>Office</th>
                  <th>Matière</th>
                  <th>Heure début</th>
                  <th>Heure fin</th>
              </tr>
          </thead>
          <tbody>
            <?php
            for($x=0;$x<=$nb;$x++)
            {
                $dateday= date("Y-m-d", strtotime("+".$x."day", strtotime($_POST['datedeb'])));


                //la liste des absences du jour
                //nb absences du jour

                $nbabsebnces=$student->getAbsencesthisdaynbMat($dateday,$codeEtabLocal,$libellesessionencours,$_POST['classeEtab'],$_POST['matiereid']);

                if($nbabsebnces>0)
                {
                  //nous allons chercher la liste

                  $datas=$student->getlisteabsencesdayMat($dateday,$codeEtabLocal,$libellesessionencours,$_POST['classeEtab'],$_POST['matiereid']);

                  foreach ($datas as $value):
                    ?>
                    <tr>
                      <td><?php echo $value->matricule_presence ?></td>
                      <td><?php echo $value->nom_eleve." ".$value->prenom_eleve ?></td>
                      <td><?php echo date_format(date_create($dateday), "d-m-Y"); ?></td>
                      <td><?php echo $matiere->getMatiereLibelleByIdMat($value->matiere_presence,$value->codeEtab_presence); ?></td>
                      <td><?php echo returnHours($value->heuredeb_heure); ?></td>
                      <td><?php echo returnHours($value->heurefin_heure); ?></td>
                    </tr>
                    <?php
                  endforeach;

                }
            }
             ?>



          </tbody>
      </table>
                  </div>
              </div>
          </div>


          <?php
              }
          ?>



          </div>

<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
<?php
  if(isset($_POST))
  {
    ?>

<?php
  }
 ?>

 function searchmatiere()
 {
   var classe=$("#classeEtab").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $codeEtabLocal; ?>";
   var etape=5;

   $.ajax({
     url: '../ajax/matiere.php',
     type: 'POST',
     async:false,
     data: 'code='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classe,
     dataType: 'text',
     success: function (response, statut) {

         $("#matiereid").html("");

         $("#matiereid").html(response);



     }
   });
 }

 function searstudent()
 {
   var classe=$("#classeEtab").val();
   var session="<?php echo $libellesessionencours; ?>";
   var codeEtab="<?php echo $codeEtabLocal; ?>";
   var etape=3;

   $.ajax({
     url: '../ajax/admission.php',
     type: 'POST',
     async:false,
     data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classe='+classe,
     dataType: 'text',
     success: function (response, statut) {

         $("#eleveid").html("");

         $("#eleveid").html(response);
     }
   });

 }

 var date = new Date();
var newDate = new Date(date.setTime( date.getTime() + (0 * 86400000)));

 $('#datedeb').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 $('#datefin').bootstrapMaterialDatePicker
 ({
   date:true,
   shortTime: false,
   time:false,
   maxDate:newDate,
   format :'DD-MM-YYYY',
   lang: 'fr',
  cancelText: '<?php echo L::AnnulerBtn ?>',
  okText: '<?php echo L::Okay ?>',
  clearText: '<?php echo L::Eraser ?>',
  nowText: '<?php echo L::Now ?>'

 });

 function recapattendanceDay(datedeb,nb,classeEtab,session,codeEtab,matiere)
 {
     // var etape=7;
     // $.ajax({
     //   url: '../ajax/etat.php',
     //   type: 'POST',
     //   async:false,
     //   data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classeEtab='+classeEtab+'&datedeb='+datedeb+'&nb='+nb,
     //   dataType: 'text',
     //   success: function (response, statut) {
     //
     //       window.open(response, '_blank');
     //
     //   }
     // });

     var url='recapclasseliste.php?codeEtab='+codeEtab+'&session='+session+'&classeEtab='+classeEtab+'&datedeb='+datedeb+'&nb='+nb+'&matiere='+matiere;

     window.open(url, '_blank');
 }


function recapattendance(moisconcat,annee,classeEtab,session,codeEtab)
{
  var etape=4;
  $.ajax({
    url: '../ajax/etat.php',
    type: 'POST',
    async:false,
    data: 'codeEtab='+codeEtab+'&etape='+etape+'&session='+session+'&classeEtab='+classeEtab+'&moisconcat='+moisconcat+'&annee='+annee,
    dataType: 'text',
    success: function (response, statut) {

        window.open(response, '_blank');

    }
  });
}

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#matiereid").select2();

   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });
   $(document).ready(function() {

//
$("#FormSearch").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    month:"required",
    yearsession:"required",
    datedeb:"required",
    datefin:"required",
    eleveid:"required",
    matiereid:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    month:"<?php echo L::PleaseSelectAnMonth ?>",
    yearsession:"Merci de selection l'année de session",
    datedeb:"Merci de choisir la date de debut",
    datefin:"Merci de choisir la date de fin",
    eleveid:"Merci de selectionner un eleve",
    matiereid:"Merci de selectionner une matiere"

  },
  submitHandler: function(form) {
    form.submit();
  }
});




   });

   </script>
    <!-- end js include path -->
  </body>

</html>
