<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$emailUti=$_SESSION['user']['email'];
$compteuserid=$_SESSION['user']['IdCompte'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}

$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$datastat=$user->getStatisById($codeEtabAssigner);
$tabstat=explode("*",$datastat);

//le nombre des eleves de cet etablissement

$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);
$agendasEtab=$etabs->DetermineAgendaEtab($codeEtabAssigner);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$libellesessionencours="";

  if($etablissementType==1||$etablissementType==3)
  {
    if($nbsessionOn>0){
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      // $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      // $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      // $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }

  }else {
    if($nbsessionOn>0)
    {
      //recuperer la session en cours
      $sessionencours=$session->getSessionEncours($codeEtabAssigner);
      $tabsessionencours=explode("*",$sessionencours);
      $libellesessionencours=$tabsessionencours[0];
      $sessionencoursid=$tabsessionencours[1];
      $typesessionencours=$tabsessionencours[2];
      $typesemestre=$session->getActiveAllSemestrebyIdsession($sessionencoursid);
      $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
      $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
    }
  }

$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$_SESSION['user']['session']=$libellesessionencours;

$_SESSION['user']['codeEtab']=$codeEtabAssigner;

// echo $etabs->getMonthRevenu($codeEtabAssigner,$libellesessionencours);

// $tabdate=$etabs->getMonthRevenureceive($codeEtabAssigner,$libellesessionencours);

// echo $etabs->getMonthRevenustandby($codeEtabAssigner,$libellesessionencours);

// var_dump($tabdate);

//echo $codeEtabAssigner." ".$libellesessionencours;

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title"><?php echo L::dashb  ?></div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><?php echo L::dashb  ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row ">
                      <div class="col-md-12 ">
                        <div class="alert alert-dismissible fade show" role="alert" style="background-color:#71d40f">
                        BIENVENUE A L' ESPACE D'ADMINISTRATION DE <?php echo $etabs->getEtabNamebyCodeEtab($codeEtabAssigner); ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                         </a>
                        </div>
                      </div>

                        </div>
					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <div class="row">

                    <div class="col-md-12 col-sm-12">

                      <div class="state-overview">
            						<div class="row">

            					        <!-- /.col -->
                             <div class="col-lg-4 col-sm-4">
            								<div class="overview-panel orange">
            									<div class="symbol">
            										<i class="material-icons f-left">group</i>
            									</div>
            									<div class="value white">
            										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php if(strlen($libellesessionencours)>0){echo $user->getNbofStudentforThisschoolSession($codeEtabAssigner,$libellesessionencours);}else{echo "0";} ?>"><?php echo $user->getNbofStudentforThisschoolSession($codeEtabAssigner,$libellesessionencours);?></p>
            										<p><?php echo strtoupper("Eleves") ?></p>
            									</div>
            								</div>
            							</div>
            					        <!-- /.col -->
                              <div class="col-lg-4 col-sm-4">
            								<div class="overview-panel deepPink-bgcolor">
            									<div class="symbol">
            										<i class="material-icons f-left">school</i>
            									</div>
            									<div class="value white">
            										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[2];?>"><?php echo $user->getNumberofTeatcherforThisschool($codeEtabAssigner);?></p>
            										<p><?php echo strtoupper("Enseignants") ?></p>
            									</div>
            								</div>
            							</div>
            					        <!-- /.col -->
                              <div class="col-lg-4 col-sm-4">
            								<div class="overview-panel blue-bgcolor">
            									<div class="symbol">
            											<i class="material-icons f-left">person</i>
            									</div>
            									<div class="value white">
            										<p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php echo $tabstat[3];?>"><?php echo $user->getNumberofParentforThisschool($codeEtabAssigner)?></p>
            									<p><?php echo strtoupper("Parents") ?></p>
            									</div>
            								</div>
            							</div>
                         <!--div class="col-lg-12 col-sm-12">
                      <div class="overview-panel purple">
                        <div class="symbol">
                            <i class="material-icons f-left">person</i>
                        </div>
                        <div class="value white">
                          <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?php //echo $tabstat[3];?>"><?php //echo $user->getNumberStudentAttendanceToday($codeEtabAssigner)?></p>
                        <p><?php //echo strtoupper("Presences")."<br/>Présence du jour"; ?></p>
                        </div>
                      </div>
                    </div-->
            					        <!-- /.col -->
            					      </div>
                           <div class="row">
                                 <div class="col-md-4">
                                     <div class="card card-box">
                                         <div class="card-head">
                                             <header>ELEVES</header>
                                             <!--div class="tools">
                                                 <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
         	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
         	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                             </div-->
                                         </div>
                                         <div class="card-body " id="chartjs_doughnut_parent">
                                             <div class="row">
                                                 <canvas id="chartjs_doughnut" height="320"></canvas>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-8" style="">
                                   <div class="card  card-box">
                                       <div class="card-head">
                                           <header>Revenus</header>
                                           <div class="tools">
                                               <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
       	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
       	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                           </div>
                                       </div>
                                       <div class="card-body " id="chartjs_line_parent">
                                           <div class="row">
                                               <canvas id="chartjs_line" ></canvas>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                             </div>
            						</div>


                    </div>
                    <div class="col-md-12 col-sm-12">
                         <div class="card-box">
                             <div class="card-head">

                             </div>
                             <div class="card-body ">
                              <div class="panel-body">

                     <div id="calendar1" class="has-toolbar"> </div>

                                <?php
                              // }
                                  ?>
                                    </div>
                             </div>
                         </div>
                     </div>
                           </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);



     var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: [<?php  echo $etabs->getMonthRevenu($codeEtabAssigner,$libellesessionencours); ?>],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                      <?php echo $etabs->getMonthRevenustandby($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                    <?php echo $etabs->getMonthRevenureceive($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($codeEtabAssigner,$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($codeEtabAssigner,$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "Filles",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "Garçons"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


     var calendar = $('#calendar1').fullCalendar({
       monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
       monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun','Jul','Aot','Sep','Oct','Nov','Dec'],
       dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
       dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
       timeFormat:'H(:mm)',
       timeZone: 'UTC',
       allDayText:'tous les jours',
       // minTime: "08:00",
       // maxTime: "20:00",
       buttonText:{
  today:    'AUJOURDHUI',
  month:    'MOIS',
  week:     'SEMAINE',
  day:      'JOUR',
  list:     'list'
},
    locale: 'fr',
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events:'../ajax/load.php?view=1',
   // events:loader(),
    // events: 'load.php?codeEtab='+codeEtab+'&session='+session+'&etape='+etape,
    selectable:false,
    selectHelper:false,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:false,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
   },
   eventClick:function(event)
   {
     var id = event.id;
     var etape=7;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";
     // alert(id);
     $.ajax({
       url: '../ajax/login.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&parascolaire='+id,
       dataType: 'text',
       success: function (content, statut) {

         // var data=content;
         var cut = $.trim(content);
         $tabcontent=cut.split("*");

         $("#oldparentname").val($tabcontent[0]);
         $("#oldparentprename").val($tabcontent[1]+" "+$tabcontent[2]);
         $("#oldparentsexe").val($tabcontent[3]+" "+$tabcontent[4]);
         $("#oldparentphone").val($tabcontent[5]);
         $("#descri").html($tabcontent[10]);

         var gratuit=$tabcontent[7];
         var montant=$tabcontent[8];

         if(gratuit==0)
         {
           $("#rowgratuit").show();
           $("#rowpayant").hide();

         }else {
           $("#rowpayant").show();
           $("#rowgratuit").hide();
           $("#montpayant").html(montant+" FCFA");
         }

         // alert($tabcontent[11]);

         // callback(data);

          $("#parentOld").click();

       }
     });

   },

    // ,
    //
    // eventClick:function(event)
    // {
    //  if(confirm("Are you sure you want to remove it?"))
    //  {
    //   var id = event.id;
    //   $.ajax({
    //    url:"delete.php",
    //    type:"POST",
    //    data:{id:id},
    //    success:function()
    //    {
    //     calendar.fullCalendar('refetchEvents');
    //     alert("Event Removed");
    //    }
    //   })
    //  }
    // },

   });

   });

   </script>
     <!-- end js include path -->
</body>


</html>
