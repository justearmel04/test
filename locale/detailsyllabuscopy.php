<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Teatcher.php');
require_once('../class/Sessionsacade.php');
require_once('../class/Classe.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];

$user=new User();
$etabs=new Etab();
$admin= new Localadmin();
$classe=new Classe();
$localadmins= new Localadmin();
$parents=new ParentX();
$teatcher=new Teatcher();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

$admins=$admin->getAllAdminLocal();
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);



if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$codesEtab=$etabs->getAllcodesEtab();
$codesEtab=$etabs->getAllcodesEtabBycodeEtab($codeEtabAssigner);
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$teatchers=$teatcher->getAllTeatchers();

$teatchers=$teatcher->getAllTeatchersBySchoolCode($codeEtabAssigner);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);

}
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$courseid=$_GET['programme'];
$classeid=$_GET['classeid'];

// $courses=$etabs->getAllTeatchercourses($compteuserid,$codeEtabsession,$libellesessionencours);
$coursesdetails=$etabs->getSyllabusInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);
// $coursesdetails=$etabs->getAllcoursesdetails($courseid,$classeid,$codeEtabsession,$libellesessionencours,$compteuserid);

// var_dump($coursesdetails);


foreach ($coursesdetails as  $datacourses):
  $descricourses=$datacourses->descri_syllab;

  $teatchercourses=$datacourses->nom_compte;
  $classecourses=$datacourses->libelle_classe;
  $classeidcourses=$datacourses->id_classe;
  $matiereidcourses=$datacourses->id_mat;
  $libellematcourses=$datacourses->libelle_mat;


endforeach;


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
 <!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
 <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 <!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
 <link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
 <link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
 <link href="../assets2/css/material_style.css" rel="stylesheet">
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
 <!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

 <!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

 <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title"><?php echo L::DetailsSylabus ?></div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li><a class="parent-item" <?php echo L::SyllabrMenu ?>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active"><?php echo L::DetailsSylabus ?></li>
                          </ol>
                      </div>
                  </div>

					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addetabexist']))
                {

                  ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php
                echo $_SESSION['user']['addetabexist'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div>



                  <?php
                  unset($_SESSION['user']['addetabexist']);
                }

                 ?>
                 <div class="row">
       <div class="col-md-12">
         <!-- BEGIN PROFILE SIDEBAR -->
         <div class="profile-sidebar">
           <div class="card card-topline-aqua">
             <div class="card-body no-padding height-9">
               <div class="row">
                 <div class="course-picture">
                   <img src="../photo/course2.jpg" class="img-responsive"
                     alt=""> </div>
               </div>
               <div class="profile-usertitle">
                 <div class="profile-usertitle-name"> <?php //echo utf8_decode(utf8_encode($namecourses));?> </div>
               </div>
               <!-- END SIDEBAR USER TITLE -->
             </div>
           </div>
           <div class="card">
             <div class="card-head card-topline-aqua">
               <header></header>
             </div>
             <div class="card-body no-padding height-9">

               <ul class="list-group list-group-unbordered">

                 <li class="list-group-item">
                   <b><i class="fa fa-university"></i> <?php echo L::ClasseMenu  ?> </b>
                   <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($classecourses)); ?></div>
                 </li>
                 <li class="list-group-item">
                   <b><i class="fa fa-user-circle-o"></i> <?php echo L::ProfsMenusingle  ?></b>
                   <div class="profile-desc-item pull-right"><?php echo utf8_decode(utf8_encode($teatchercourses)); ?>  </div>
                 </li>
                 <li class="list-group-item">
                   <b><i class="fa fa-graduation-cap"></i> <?php echo L::MatiereMenusingle  ?></b>
                   <div class=" pull-right"  ><?php echo utf8_decode(utf8_encode($libellematcourses)); ?>  </div>
                 </li>

               </ul>
             <br>




             </div>
           </div>
         </div>
         <!-- END BEGIN PROFILE SIDEBAR -->
         <!-- BEGIN PROFILE CONTENT -->
         <div class="profile-content">

           <div class="row">
             <div class="col-md-12 col-lg-12">
<div class="card card-topline-aqua">
 <div class="card-head">
   <header><?php echo mb_strtoupper(L::GeneralInfoTab)  ?></header>
   <div class="tools">
     <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
     <a class="t-collapse btn-color fa fa-chevron-down"
       href="javascript:;"></a>
     <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
   </div>
 </div>

 <div class="card-body ">
    <span class="label label-lg label-warning"  style="text-align:center;">  DESCRIPTION DU SYLLABUS </span>

   <hr>
   <!-- <div class="col-md-12"> -->
<div class="form-group">

<textarea class="form-control descri" rows="5" placeholder="Entrer la description du cours" disabled="disabled"><?php echo $descricourses; ?></textarea>

</div>
<span class="label label-lg label-warning"  style="text-align:center;">  OBJECTIFS DU SYLLABUS </span>

<hr>
<?php
$objectifs=$etabs->getSyllabusObjectifsInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($objectifs as  $value):
?>
<li><?php echo utf8_decode(utf8_encode($value->libelle_syllabob )); ?></li>
<?php
endforeach;
?>

</ul>
<span class="label label-lg label-warning"  style="text-align:center;">  CONTENU DU SYLLABUS </span>

<hr>
<?php
$themes=$etabs->getSyllabusThemesInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($themes as  $value):
 ?>
 <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabth)); ?></li>
 <?php
endforeach;
?>

</ul>
<span class="label label-lg label-warning"  style="text-align:center;">  PREREQUIS NECESSAIRES DU SYLLABUS </span>

<hr>
<?php
$Requis=$etabs->getSyllabusRequisInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($Requis as  $value):
  ?>
  <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabreq)); ?></li>
  <?php
endforeach;
 ?>

</ul>
<span class="label label-lg label-warning"  style="text-align:center;">  COMPETENCES VISEES DU SYLLABUS </span>

<hr>
<?php
 $Comp=$etabs->getSyllabusCompInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
 <?php
 foreach ($Comp as  $value):
   ?>
   <li><?php echo utf8_decode(utf8_encode($value->libelle_syllabcomp)); ?></li>
   <?php
 endforeach;
  ?>

</ul>

<!-- </div> -->
 </div>
</div>
</div>

<div class="col-md-12 col-lg-12">
<div class="card card-topline-aqua">
<div class="card-head">
<header>DOCUMENTS DU COURS</header>
<div class="tools">
<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
<a class="t-collapse btn-color fa fa-chevron-down"
href="javascript:;"></a>
<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
</div>
</div>

<div class="card-body ">

<span class="label label-lg label-warning"  style="text-align:center;">DOCUMENTS OBLIGATOIRES </span>

<hr>
<?php
$Docs=$etabs->getSyllabusDocInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($Docs as  $value):
?>
<li><?php echo utf8_decode(utf8_encode($value->libelle_syllabdoc )); ?></li>
<?php
endforeach;
?>

</ul>
<span class="label label-lg label-warning"  style="text-align:center;">  DOCUMENTS FACULTATIFS </span>

<hr>
<?php
$Docfacs=$etabs->getSyllabusDocfacInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($Docfacs as  $value):
?>
<li><?php echo utf8_decode(utf8_encode($value->libelle_syllabdoc)); ?></li>
<?php
endforeach;
?>

</ul>



<!-- </div> -->
</div>
</div>
</div>

<div class="col-md-12 col-lg-12">
<div class="card card-topline-aqua">
<div class="card-head">
<header>REGLES DE FONCTIONNEMENT DU COURS</header>
<div class="tools">
<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
<a class="t-collapse btn-color fa fa-chevron-down"
href="javascript:;"></a>
<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
</div>
</div>

<div class="card-body ">


<?php
$Regles=$etabs->getSyllabusRegleInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

?>
<ul>
<?php
foreach ($Regles as  $value):
?>
<li><?php echo utf8_decode(utf8_encode($value->libelle_syllabregle)); ?></li>
<?php
endforeach;
?>

</ul>




<!-- </div> -->
</div>
</div>
</div>





           </div>


         </div>


         <div class="col-md-12 col-lg-12">
         <div class="card card-topline-aqua">
         <div class="card-head">
         <header>CALENDRIER DES COURS</header>
         <div class="tools">
         <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
         <a class="t-collapse btn-color fa fa-chevron-down"
         href="javascript:;"></a>
         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
         </div>
         </div>

         <div class="card-body ">


         <?php
          $Calendars=$etabs->getSyllabusCalendarInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

         ?>
         <table  class="table table-hover table-checkable order-column full-width" >
            <thead>
                <tr>

                     <th style="text-align:center;">Date</th>
                    <th style="text-align:center;">No Séance </th>
                     <th style="text-align:center;">Contenu pédagogique </th>
                     <th style="text-align:center;">Travail préalable </th>
                     <!--th> Actions </th-->
                </tr>
            </thead>
            <tbody id="tabStationCalendar">
              <?php foreach ($Calendars as $value): ?>
             <tr>
             <td style="text-align:center;"><?php echo $value->date_syllabcal?></td>
             <td style="text-align:center;"><?php echo $value->seance_syllabcal ?></td>
             <td style="text-align:center;"><?php echo $value->contenu_syllabcal?></td>
             <td style="text-align:center;"><?php echo $value->prealable_syllabcal ?></td>

             </tr>
              <?php endforeach; ?>
            </tbody>

        </table>




         <!-- </div> -->
         </div>
         </div>
         </div>

         <div class="col-md-12 col-lg-12">
         <div class="card card-topline-aqua">
         <div class="card-head">
         <header>MODE EVALUATION</header>
         <div class="tools">
         <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
         <a class="t-collapse btn-color fa fa-chevron-down"
         href="javascript:;"></a>
         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
         </div>
         </div>

         <div class="card-body ">


         <?php
           $Calendars=$etabs->getSyllabusModeEvalInfos($_GET['programme'],$_GET['teatcher'],$_GET['codeEtab']);

         ?>
         <table class="table table-hover table-checkable order-column full-width">
             <thead>
                 <tr>

                     <th style="text-align:center;width:200px">Date</th>
                     <th style="text-align:center;">Type d’évaluation </th>
                     <th style="text-align:center;">Compétences visées </th>
                     <th style="text-align:center;">Pondération (%)</th>
                     <!--th style="text-align:center;">Actions</th-->

                 </tr>
             </thead>
             <tbody id="tabStationEvaluation">
               <?php foreach ($Calendars as $value): ?>
              <tr>
              <td style="text-align:center;"><?php echo $value->date_syllabeval?></td>
              <td style="text-align:center;"><?php echo $value->type_syllabeval ?></td>
              <td style="text-align:center;"><?php echo $value->competence_syllabeval?></td>
              <td style="text-align:center;"><?php echo $value->ponderation_syllabeval ?></td>

              </tr>
               <?php endforeach; ?>
             </tbody>

         </table>




         <!-- </div> -->
         </div>
         </div>
         </div>

         <!-- END PROFILE CONTENT -->
       </div>
     </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
   <script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
   <script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <!-- Common js-->
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
     <script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
     <script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
   <script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
     <script src="../assets2/js/jquery-dateformat.js" ></script>
   <script src="../assets2/js/theme-color.js" ></script>
   <!-- Material -->
   <script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/libs/form-validator/jquery.validate.min.js"></script>







    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->


    <script type="text/javascript">

    $("#classe").select2();
    $("#matiere").select2();


    function RetourForm(teatcher,codeEtab)
    {
       var lien="programmelistes.php?teatcher="+teatcher+"&codeEtab="+codeEtab;


       //window.location.lien;

       document.location.href="programmelistes.php?teatcher="+teatcher+"&codeEtab="+codeEtab
    }


    function searchmatiere(id)
    {

        var classe=$("#classe").val();
        var teatcherId=id;
        var etape=6;


      $.ajax({

           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe,
           dataType: 'text',
           success: function (content, statut) {


             $("#matiere").html("");
             $("#matiere").html(content);

           }
         });
    }

    function searchcodeEtab(id)
    {
      var classe=$("#classe").val();
      var teatcherId=id;
      var etape=7;
      var matiere=$("#matiere").val();

      $.ajax({

           url: '../ajax/matiere.php',
           type: 'POST',
           async:true,
           data: 'teatcherId=' + teatcherId+ '&etape=' + etape+'&classe='+classe+'&matiere='+matiere,
           dataType: 'text',
           success: function (content, statut) {


              $("#codeEtab").val(content);

           }
         });

    }

    function recalculobjectifnb()
    {
      var concatobjectif=$("#concatobjectif").val();

      var tab=concatobjectif.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbobjectif").val(nbtabnew);


    }

    function AddobjectifRow()
    {
      var nb=$("#nb").val();
      var nouveau= parseInt(nb)+1;
      $("#nb").val(nouveau);

      var concatobjectif=$("#concatobjectif").val();

      $("#concatobjectif").val(concatobjectif+nouveau+"@");

      recalculobjectifnb();



      $('#dynamic_field').append('<tr id="rowObj'+nouveau+'"><td><input type="text" name="objectif_'+nouveau+'" id="objectif_'+nouveau+'" placeholder="Entrer un Objectif" class="form-control objectif_list" /></td><td><button type="button" id="deleteObj'+nouveau+'" id="deleteObj'+nouveau+'"  onclick="deletedObj('+nouveau+')"  class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#objectif_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }
    }

    function recalculthemenb()
    {
      var concatcontenu=$("#concatcontenu").val();

      var tab=concatcontenu.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbcontenu").val(nbtabnew);
    }

    function deletedTheme(id)
    {
      var concatcontenu=$("#concatcontenu").val();

      $("#concatcontenu").val($("#concatcontenu").val().replace(id+"@", ""));

       $('#rowThem'+id+'').remove();

       recalculthemenb();
    }

    function AddthemeRow()
    {
      var nb=$("#nbcontenu").val();
      var nouveau= parseInt(nb)+1;
      $("#nbcontenu").val(nouveau);

      var concatcontenu=$("#concatcontenu").val();

      $("#concatcontenu").val(concatcontenu+nouveau+"@");

      recalculthemenb();


      $('#dynamic_field1').append('<tr id="rowThem'+nouveau+'"><td><input type="text" name="theme_'+nouveau+'" id="theme_'+nouveau+'" placeholder="Entrer un Thème" class="form-control theme_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedTheme('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#theme_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }
    }

    function recalculrequisnb()
    {
      var concatrequis=$("#concatrequis").val();

      var tab=concatrequis.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbrequis").val(nbtabnew);
    }

    function deletedRequis(id)
    {
      var concatrequis=$("#concatrequis").val();

      $("#concatrequis").val($("#concatrequis").val().replace(id+"@", ""));

       $('#rowRequis'+id+'').remove();

       recalculrequisnb();
    }


    function AddrequisRow()
    {
      var nb=$("#nbrequis").val();
      var nouveau= parseInt(nb)+1;
      $("#nbrequis").val(nouveau);

      var concatrequis=$("#concatrequis").val();

      $("#concatrequis").val(concatrequis+nouveau+"@");

      recalculrequisnb();



      $('#dynamic_field2').append('<tr id="rowRequis'+nouveau+'"><td><input type="text" name="requis_'+nouveau+'" id="requis_'+nouveau+'" placeholder="Entrer un Prérequis" class="form-control requis_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedRequis('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#requis_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }
    }

    function recalculcompnb()
    {
      var concatcomp=$("#concatcomp").val();

      var tab=concatcomp.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbcomp").val(nbtabnew);
    }

    function deletedComp(id)
    {
      var concatcomp=$("#concatcomp").val();

      $("#concatcomp").val($("#concatcomp").val().replace(id+"@", ""));

       $('#rowComp'+id+'').remove();

       recalculcompnb();
    }

    function AddcompRow()
    {
      var nb=$("#nbcomp").val();
      var nouveau= parseInt(nb)+1;
      $("#nbcomp").val(nouveau);

      var concatcomp=$("#concatcomp").val();

      $("#concatcomp").val(concatcomp+nouveau+"@");

      recalculcompnb();



      $('#dynamic_field3').append('<tr id="rowComp'+nouveau+'"><td><input type="text" name="comp_'+nouveau+'" id="comp_'+nouveau+'" placeholder="<?php echo L::AddCompViseeRequired ?>" class="form-control comp_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedComp('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#comp_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }
    }

    // function deletedDoc

    function recalculdocnb()
    {
      var concatdoc=$("#concatdoc").val();

      var tab=concatdoc.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbdoc").val(nbtabnew);
    }

    function deletedDoc(id)
    {
      var concatdoc=$("#concatdoc").val();

      $("#concatdoc").val($("#concatdoc").val().replace(id+"@", ""));

       $('#rowDoc'+id+'').remove();

       recalculdocnb();
    }

    function AddDocumentRow()
    {
      var nb=$("#nbdoc").val();
      var nouveau= parseInt(nb)+1;
      $("#nbdoc").val(nouveau);


      var concatdoc=$("#concatdoc").val();

      $("#concatdoc").val(concatdoc+nouveau+"@");

      recalculdocnb();


      $('#dynamic_field4').append('<tr id="rowDoc'+nouveau+'"><td><input type="text" name="doc_'+nouveau+'" id="doc_'+nouveau+'" placeholder="Entrer un document" class="form-control doc_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedDoc('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#doc_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }

    }

    function retirerCalendar(id)
    {
      var concatcalendar=$("#concatcalendar").val();

      $("#concatcalendar").val($("#concatcalendar").val().replace(id+"@", ""));

       $('#ligneCalendar'+id+'').remove();

      recalculcalendarnb();
    }

    function deletedfacdoc(id)
    {
      var concatdocfac=$("#concatdocfac").val();

      $("#concatdocfac").val($("#concatdocfac").val().replace(id+"@", ""));

       $('#rowDocfac'+id+'').remove();

       recalculdocfacnb();
    }

    function deletedRegle(id)
    {
      var concatregl=$("#concatregl").val();

      $("#concatregl").val($("#concatregl").val().replace(id+"@", ""));

       $('#rowRegl'+id+'').remove();

       recalculreglenb();
    }

    function recalculcalendarnb()
    {
      var concatcalendar=$("#concatcalendar").val();

      var tab=concatcalendar.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbcalandar").val(nbtabnew);
    }

    function recalculdocfacnb()
    {
      var concatdocfac=$("#concatdocfac").val();

      var tab=concatdocfac.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbdocfac").val(nbtabnew);
    }

    function recalculevalnb()
    {
      var concatevaluation=$("#concatevaluation").val();

      var tab=concatevaluation.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbevaluation").val(nbtabnew);
    }

    function recalculreglenb()
    {
      var concatregl=$("#concatregl").val();

      var tab=concatregl.split("@");

      var nbtab=tab.length;

      var nbtabnew=parseInt(nbtab)-1;

      $("#concatnbregl").val(nbtabnew);
    }

    function AddfacDocumentRow()
    {
      var nb=$("#nbdocfac").val();
      var nouveau= parseInt(nb)+1;
      $("#nbdocfac").val(nouveau);

      var concatdocfac=$("#concatdocfac").val();

      $("#concatdocfac").val(concatdocfac+nouveau+"@");

      recalculdocfacnb();


      $('#dynamic_field5').append('<tr id="rowDocfac'+nouveau+'"><td><input type="text" name="docfac_'+nouveau+'" id="docfac_'+nouveau+'" placeholder="Entrer un document" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+nouveau+'"  onclick="deletedfacdoc('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#docfac_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }

    }

    function AddEvaluationRow()
    {
      var nb=$("#nbevaluation").val();
      var nouveau= parseInt(nb)+1;

         var ligne = "<tr id=\"ligneEval"+nouveau+"\" ondblclick=\"retirerEval("+nouveau+")\">";
         ligne = ligne + $("#ligne" +nouveau).html();
          ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"dateEvaluation_"+nouveau+"\" name=\"dateEvaluation_"+nouveau+"\" class=\"dateEval\"></td>";

         // ligne = ligne + "<td align=\"center\"><input type=\"text\" name=\"dateEvaluation"+nouveau+"\"></td>";
         ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"typeEvaluation_"+nouveau+"\" name=\"typeEvaluation_"+nouveau+"\" class=\"typeEval\"></td>";
         ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"competenceEvaluation_"+nouveau+"\" name=\"competenceEvaluation_"+nouveau+"\" class=\"compEval\"></td>";
         ligne = ligne + "<td align=\"center\"><input type=\"number\" min=\"1\" max=\"100\" id=\"ponderationEvaluation_"+nouveau+"\" name=\"ponderationEvaluation_"+nouveau+"\" class=\"pondEval\"></td>";
    // ligne = ligne + "<td align=\"center\"> <button type=\"button\" name=\"btn_remove"+nouveau+"\" id=\"btn_remove"+nouveau+"\" class=\"btn btn-danger btn_remove\" >X</button></td>";
         ligne = ligne + "</tr>";

         $('#tabStationEvaluation').append(ligne);

         $("#nbevaluation").val(nouveau);

         var concatevaluation=$("#concatevaluation").val();

         $("#concatevaluation").val(concatevaluation+nouveau+"@");

         recalculevalnb();


         for(var i=1;i<=nouveau;i++)
         {
           $("#dateEvaluation_"+i).rules( "add", {
               required: true,
               messages: {
               required: "<?php echo L::RequiredChamp ?>"
     }
             });


             $("#dateEvaluation_"+i).bootstrapMaterialDatePicker
             ({
               date:true,
               shortTime: false,
               format: 'DD-MM-YYYY',
               lang: 'fr',
              cancelText: '<?php echo L::AnnulerBtn ?>',
              okText: '<?php echo L::Okay ?>',
              clearText: '<?php echo L::Eraser ?>',
              nowText: '<?php echo L::Now ?>'

             });

             $("#typeEvaluation_"+i).rules( "add", {
                 required: true,
                 messages: {
                 required: "<?php echo L::RequiredChamp ?>"
       }
               });

               $("#competenceEvaluation_"+i).rules( "add", {
                   required: true,
                   messages: {
                   required: "<?php echo L::RequiredChamp ?>"
         }
                 });

                 $("#ponderationEvaluation_"+i).rules( "add", {
                     required: true,
                     messages: {
                     required: "<?php echo L::RequiredChamp ?>"
           }
                   });
         }

    }

    function AddCalendarRow()
    {
      var nb=$("#nbcalandar").val();
      var nouveau= parseInt(nb)+1;

         var ligne = "<tr id=\"ligneCalendar"+nouveau+"\" ondblclick=\"retirerCalendar("+nouveau+")\">";
         ligne = ligne + $("#ligne" +nouveau).html();
          ligne = ligne + "<td align=\"center\" ><input type=\"\" id=\"dateCalandar_"+nouveau+"\" name=\"dateCalandar_"+nouveau+"\"  ></td>";
          //
         // ligne = ligne + "<td align=\"center\"><input type=\"text\" name=\"dateEvaluation"+nouveau+"\"></td>";
          // ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"contentCalandar_"+nouveau+"\" name=\"contentCalandar_"+nouveau+"\" ></td>";
         // ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"workCalandar_"+nouveau+"\" name=\"workCalandar_"+nouveau+"\" ></td>";
         ligne = ligne + "<td align=\"center\"><input type=\"text\" id=\"seanceCalandar_"+nouveau+"\" name=\"seanceCalandar_"+nouveau+"\" ></td>";
         ligne = ligne + "<td align=\"center\"><textarea id=\"contentCalandar_"+nouveau+"\" name=\"contentCalandar_"+nouveau+"\" rows=\"5\" cols=\"25\"></textarea></td>";
         ligne = ligne + "<td align=\"center\"><textarea id=\"workCalandar_"+nouveau+"\" name=\"workCalandar_"+nouveau+"\" rows=\"5\" cols=\"25\"></textarea></td>";
         // ligne = ligne + "<td align=\"center\"> <button type=\"button\" name=\"btn_remove"+nouveau+"\" id=\"btn_remove"+nouveau+"\" class=\"btn btn-danger btn_remove\" onclick=\"retirerCalendar("+nouveau+")\">X</button></td>";
         // ligne = ligne + "</tr>";

         $('#tabStationCalendar').append(ligne);

         $("#nbcalandar").val(nouveau);

         var concatcalendar=$("#concatcalendar").val();

         $("#concatcalendar").val(concatcalendar+nouveau+"@");

         recalculcalendarnb();



         for(var i=1;i<=nouveau;i++)
         {
           $("#dateCalandar_"+i).rules( "add", {
               required: true,
               messages: {
               required: "<?php echo L::RequiredChamp ?>"
     }
             });

             $("#dateCalandar_"+i).bootstrapMaterialDatePicker
             ({
               date:true,
               shortTime: false,
               format: 'DD-MM-YYYY',
               lang: 'fr',
              cancelText: '<?php echo L::AnnulerBtn ?>',
              okText: '<?php echo L::Okay ?>',
              clearText: '<?php echo L::Eraser ?>',
              nowText: '<?php echo L::Now ?>'

             });

             $("#seanceCalandar_"+i).rules( "add", {
                 required: true,
                 messages: {
                 required: "<?php echo L::RequiredChamp ?>"
       }
               });

               $("#contentCalandar_"+i).rules( "add", {
                   required: true,
                   messages: {
                   required: "<?php echo L::RequiredChamp ?>"
         }
                 });

                 $("#workCalandar_"+i).rules( "add", {
                     required: true,
                     messages: {
                     required: "<?php echo L::RequiredChamp ?>"
           }
                   });
         }

    }

    function AddRegleRow()
    {
      var nb=$("#nbregl").val();
      var nouveau= parseInt(nb)+1;
      $("#nbregl").val(nouveau);

      $("#nbcalandar").val(nouveau);

      var concatregl=$("#concatregl").val();

      $("#concatregl").val(concatregl+nouveau+"@");

      recalculreglenb();

      $('#dynamic_field6').append('<tr id="rowRegl'+nouveau+'"><td><input type="text" name="regle_'+nouveau+'" id="regle_'+nouveau+'" placeholder="Entrer une règle" class="form-control regle_list" /></td><td><button type="button" name="remove" id="'+nouveau+'" onclick="deletedRegle('+nouveau+')" class="btn btn-danger btn_remove">X</button></td></tr>');

      for(var i=1;i<=nouveau;i++)
      {
        $("#regle_"+i).rules( "add", {
            required: true,
            messages: {
            required: "<?php echo L::RequiredChamp ?>"
  }
          });
      }

    }

    function retirerEval(id)
    {
      var concatevaluation=$("#concatevaluation").val();

      $("#concatevaluation").val($("#concatevaluation").val().replace(id+"@", ""));

       $('#ligneEval'+id+'').remove();

       recalculevalnb();
    }



function valideCalendar()
{


    var dateseance=$("#dateseance").val();

    if(dateseance=="")
     {
       document.getElementById("messagedateseance").innerHTML = "<font color=\"red\">Merci de renseigner la date de la séance</font>";

     }

     var seance=$("#seance").val();

     if(seance=="")
      {
        document.getElementById("messageseance").innerHTML = "<font color=\"red\">Merci de renseigner la séance</font>";

      }

      var contepeda=$("#contepeda").val();

      if(contepeda=="")
       {
         document.getElementById("messagecontepeda").innerHTML = "<font color=\"red\">Merci de renseigner le contenu pedegagogique</font>";

       }

       var travailpre=$("#travailpre").val();

       if(travailpre=="")
        {
          document.getElementById("messagetravailpre").innerHTML = "<font color=\"red\">Merci de renseigner le travail préalable</font>";

        }


        if(travailpre!="" && contepeda!="" && seance!="" && dateseance!="")
        {
          var nbcalandar=$("#nbcalandar").val();
          var nouveaunbcalandar=parseInt(nbcalandar)+1;
          var prealabtab=$("#prealabtab").val();
          var pedagotab=$("#pedagotab").val();
          var seancetab=$("#seancetab").val();
          var dateseancetab=$("#dateseancetab").val();

          var concattravailpre=nouveaunbcalandar+"@"+travailpre;
          var concatcontepeda=nouveaunbcalandar+"@"+contepeda;
          var concatseance=nouveaunbcalandar+"@"+seance;
          var concatdateseance=nouveaunbcalandar+"@"+dateseance;

// date.format("YYYY-MM-DD")
           var ligne = "<tr id=\"ligneselect"+nouveaunbcalandar+"\">";
            ligne = ligne + $("#ligne" +nouveaunbcalandar).html();
            ligne = ligne + "<td align=\"center\">"+ dateseance+"</td>"
            ligne = ligne + "<td align=\"center\">"+ seance+"</td>"
            ligne = ligne + "<td align=\"center\">"+ contepeda+"</td>"
            ligne = ligne + "<td align=\"center\">"+ travailpre+"</td>"
            ligne = ligne + "<td class=\"visible-lg\"><a onclick=\"retirerStation("+nouveaunbcalandar+",'"+dateseance+"','"+seance+"','"+contepeda+"','"+travailpre+"')\" style=\"color:red;font-weight:normal; cursor:pointer\"><span class=\"fa fa-minus-circle\"></span>&nbsp;Retirer</a> </td>";
            ligne = ligne + "</tr>"

            $("#prealabtab").val($("#prealabtab").val()+concattravailpre+",");
            $("#pedagotab").val($("#pedagotab").val()+concatcontepeda+",");

            $("#seancetab").val($("#seancetab").val()+concatseance+",");
            $("#dateseancetab").val($("#dateseancetab").val()+concatdateseance+",");

            $("#dateseance").val("");
            $("#seance").val("");
            $("#contepeda").val("");
            $("#travailpre").val("");


             $("#aucuneLinge").slideUp();

            $("#tabStationSelectBody").append(ligne);

            $("#divafficherinfosstation").css("display", "none");
            $("#largeModel .close").click();




        }


}

function retirerStation(nouveaunbcalandar,dateseance,seance,contepeda,travailpre)
{
  var nbcalandar=$("#nbcalandar").val();
  var nouveaunbcalandararetirer=parseInt(nbcalandar)-1;

  var concattravailpre=nouveaunbcalandar+"@"+travailpre;
  var concatcontepeda=nouveaunbcalandar+"@"+contepeda;
  var concatseance=nouveaunbcalandar+"@"+seance;
  var concatdateseance=nouveaunbcalandar+"@"+dateseance;

  var lignearetirer=$("#ligneselect"+nouveaunbcalandar).val();

  $("#prealabtab").val($("#prealabtab").val().replace(concattravailpre+",", ""));

  // $("#prealabtab").val($("#prealabtab").val().replace(concattravailpre+",", ""));
  // $("#pedagotab").val($("#pedagotab").val().replace(concatcontepeda+",", ""));
  //
  // $("#seancetab").val($("#seancetab").replace(concatseance+",", ""));
  // $("#dateseancetab").val($("#dateseancetab").val().replace(concatdateseance+",", ""));

  $("#ligneselect"+nouveaunbcalandar).remove();

  if(nouveaunbcalandararetirer==0)
  {
    var ligne = "<tr>";
    ligne = ligne + "<td colspan=5>Aucune données</td>";
    ligne = ligne + "</tr>";

      $("#tabStationSelectBody").append(ligne);
  }

}

function deletedObj(id)
{
  var concatobjectif=$("#concatobjectif").val();

  $("#concatobjectif").val($("#concatobjectif").val().replace(id+"@", ""));

   $('#rowObj'+id+'').remove();

   recalculobjectifnb();

}


   $(document).ready(function() {

     var i=1;

     // AddobjectifRow();
     // AddthemeRow();
     // AddrequisRow();
     // AddcompRow();
     // AddDocumentRow();
     // AddEvaluationRow();
     // AddRegleRow();
     // AddCalendarRow();

       $('#add').click(function(){

         AddobjectifRow();

       });
       // $(document).on('click', '.btn_remove', function(){
       //      var button_id = $(this).attr("id");
       //      $('#row'+button_id+'').remove();
       //      var nb=$("#nb").val();
       //    	var nouveau= parseInt(nb)-1;
       //      $("#nb").val(nouveau);
       // });

       //Contenu

       $('#addtheme').click(function(){
         AddthemeRow();


       });
       // $(document).on('click', '.btn_remove', function(){
       //      var button_id = $(this).attr("id");
       //      $('#row'+button_id+'').remove();
       //      var nb=$("#nbcontenu").val();
       //    	var nouveau= parseInt(nb)-1;
       //      $("#nbcontenu").val(nouveau);
       // });

       //prerequis

       $('#addrequis').click(function(){
            AddrequisRow();

       });
       // $(document).on('click', '.btn_remove', function(){
       //      var button_id = $(this).attr("id");
       //      $('#row'+button_id+'').remove();
       //      var nb=$("#nbrequis").val();
       //    	var nouveau= parseInt(nb)-1;
       //      $("#nbrequis").val(nouveau);
       // });

       //competences visées

       $('#addcomp').click(function(){
            AddcompRow();

       });
       // $(document).on('click', '.btn_remove', function(){
       //      var button_id = $(this).attr("id");
       //      $('#row'+button_id+'').remove();
       //      var nb=$("#nbcomp").val();
       //     var nouveau= parseInt(nb)-1;
       //      $("#nbcomp").val(nouveau);
       // });
// documents obligatoires




$('#adddoc').click(function(){
  AddDocumentRow();

});
// $(document).on('click', '.btn_remove', function(){
//      var button_id = $(this).attr("id");
//      $('#row'+button_id+'').remove();
//      var nb=$("#nbdoc").val();
//     var nouveau= parseInt(nb)-1;
//      $("#nbdoc").val(nouveau);
// });

//document facultatif

$('#adddocfac').click(function(){
  AddfacDocumentRow();

});
// $(document).on('click', '.btn_remove', function(){
//      var button_id = $(this).attr("id");
//      $('#row'+button_id+'').remove();
//      var nb=$("#nbdocfac").val();
//     var nouveau= parseInt(nb)-1;
//      $("#nbdocfac").val(nouveau);
// });

$('#addregle').click(function(){

AddRegleRow();
});
$(document).on('click', '.btn_remove', function(){
     var button_id = $(this).attr("id");
     $('#row'+button_id+'').remove();
     var nb=$("#nbregl").val();
    var nouveau= parseInt(nb)-1;
     $("#nbregl").val(nouveau);
});



$('#addbuttonEval').click(function(){

AddEvaluationRow();
});

$('#addcalendarbtn').click(function(){
  AddCalendarRow();

});



   });

   $("#syllabForm").validate({

     errorPlacement: function(label, element) {
     label.addClass('mt-2 text-danger');
     label.insertAfter(element);
   },
   highlight: function(element, errorClass) {
     $(element).parent().addClass('has-danger')
     $(element).addClass('form-control-danger')
   },
    rules:{
      descri:"required",
      classe:"required",
      matiere:"required"
    },
    messages:{
      descri:"Champ Obligatoire",
        classe:"Champ Obligatoire",
        matiere:"Champ Obligatoire"
    }

   });

   // $('.objectif_list').each(function() {
   //       $(this).rules("add",
   //           {
   //               required: true,
   //               messages: {
   //                   required: "<?php echo L::RequiredChamp ?>",
   //               }
   //           });
   //
   //
   //   });

$("#syllabForm").on('submit', function(event) {

  $('.typeEval').each(function() {
        $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "<?php echo L::RequiredChamp ?>",
                }
            });
    });
    //
    $('.pondEval').each(function() {
          $(this).rules("add",
              {
                  required: true,
                  messages: {
                      required: "<?php echo L::RequiredChamp ?>",
                  }
              });
      });

      $('.compEval').each(function() {
            $(this).rules("add",
                {
                    required: true,
                    messages: {
                        required: "<?php echo L::RequiredChamp ?>",
                    }
                });
        });

        $('.dateEval').each(function() {
              $(this).rules("add",
                  {
                      required: true,
                      messages: {
                          required: "<?php echo L::RequiredChamp ?>",
                      }
                  });
          });

          //Objectifs



    //
    //         $('.theme_list').each(function() {
    //               $(this).rules("add",
    //                   {
    //                       required: true,
    //                       messages: {
    //                           required: "<?php echo L::RequiredChamp ?>",
    //                       }
    //                   });
    //           });
    //
    //           $('.requis_list').each(function() {
    //                 $(this).rules("add",
    //                     {
    //                         required: true,
    //                         messages: {
    //                             required: "<?php echo L::RequiredChamp ?>",
    //                         }
    //                     });
    //             });
    //
    //             $('.comp_list').each(function() {
    //                   $(this).rules("add",
    //                       {
    //                           required: true,
    //                           messages: {
    //                               required: "<?php echo L::RequiredChamp ?>",
    //                           }
    //                       });
    //
    //                       $(this).css( "backgroundColor", "yellow" );
    //               });
    //
    //               $('.doc_list').each(function() {
    //                     $(this).rules("add",
    //                         {
    //                             required: true,
    //                             messages: {
    //                                 required: "<?php echo L::RequiredChamp ?>",
    //                             }
    //                         });
    //                 });
    //
    //                 $('.regle_list').each(function() {
    //                       $(this).rules("add",
    //                           {
    //                               required: true,
    //                               messages: {
    //                                   required: "<?php echo L::RequiredChamp ?>",
    //                               }
    //                           });
    //                   });
    //
    //                   $('.objectifdep').each(function() {
    //                         $(this).rules("add",
    //                             {
    //                                 required: true,
    //                                 messages: {
    //                                     required: "<?php echo L::RequiredChamp ?>",
    //                                 }
    //                             });
    //                     });
    //
    //
    //
    //
    //


});












   </script>
    <!-- end js include path -->
  </body>

</html>
