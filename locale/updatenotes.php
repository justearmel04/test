<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../class/Student.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();

$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$student=new Student();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);

if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);
$codeEtabAssigner=$etabs->getcodeEtabByLocalId($_SESSION['user']['IdCompte']);
$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabAssigner);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabAssigner);

if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabAssigner);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabAssigner,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabAssigner,$libellesessionencours);
}

//je vais mettre lid de leleve dans typenote et tynenote dans elve

$ideleve=$_GET['tyepenote'];
$typenote=$_GET['eleveid'];
$noteid=$_GET['noteid'];

$studentInfosNote=$student->getInformationsOfNotesStudent($noteid,$typenote,$ideleve,$codeEtabAssigner);
$tabinfos=explode("*",$studentInfosNote);

if($typenote==1)
{
  //controle
  $exmanorcontrole=$student->getControleInfos($tabinfos[9]);
}else if($typenote==2)
{
  //examen
}


 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlepage?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />

  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
   <link href="../assets2/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
   <link href="../assets2/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- data tables -->
     <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>


	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
 </head>
 <!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
		<?php
include("header.php");
    ?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
 			<!-- start sidebar menu -->
 			<?php
				include("menu.php");
			?>
			 <!-- end sidebar menu -->
			<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Modification Notes - Elève : <?php echo $exmanorcontrole; ?> </div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><!--i class="fa fa-home"></i-->&nbsp;<a class="parent-item" href="index.html">Gestion des Notes</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Modification Notes </li>
                            </ol>
                        </div>
                    </div>
					<!-- start widget -->
          <div class="row">
              <div class="col-md-12 col-sm-12">
                  <div class="card card-box">
                      <div class="card-head">
                          <header></header>
                           <!--button id = "panel-button"
                         class = "mdl-button mdl-js-button mdl-button--icon pull-right"
                         data-upgraded = ",MaterialButton">
                         <i class = "material-icons">more_vert</i>
                      </button>
                      <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                         data-mdl-for = "panel-button">
                         <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                         <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                         <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                      </ul-->
                      </div>

                      <div class="card-body" id="bar-parent">
                          <form  id="FormUpdateNote" class="form-horizontal" action="../controller/notes.php" method="post" enctype="multipart/form-data">
                              <div class="form-body">

                                <div class="form-group row">
                                        <label class="control-label col-md-3">Matricule
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-5">
                                            <input type="text" readonly style="text-align:center" name="matricule" id="matricule" data-required="1" value="<?php echo $tabinfos[0];?>" class="form-control input-height" /> </div>
                                    </div>
                              <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::Name?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="nomTea" id="nomTea" data-required="1" value="<?php echo $tabinfos[1];?>" readonly style="text-align:center" class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::PreName?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="prenomTea" id="prenomTea" data-required="1" value="<?php echo $tabinfos[2];?>" readonly style="text-align:center" class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3"><?php echo L::ClasseMenu ?>
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="text" name="prenomTea" id="prenomTea" data-required="1" value="<?php echo $tabinfos[3];?>" readonly style="text-align:center" class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3">Note
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                          <input type="number" min=0 max=20 name="note" id="note" data-required="1" value="<?php echo $tabinfos[5];?>"class="form-control input-height" /> </div>
                                  </div>
                                  <div class="form-group row">
                                      <label class="control-label col-md-3">Observation
                                          <span class="required"> * </span>
                                      </label>
                                      <div class="col-md-5">
                                        <textarea name="observation" id="observation" rows="3" cols="50" ><?php echo $tabinfos[6];?></textarea>
                                        <input type="hidden" id="noteid" name="noteid"  value="<?php echo $tabinfos[8];?>">
                                        <input type="hidden" id="codeEtab" name="codeEtab" value="<?php echo $tabinfos[7];?>">
                                        <input type="hidden" id="studentid"  name="studentid" value="<?php echo $tabinfos[10];?>">
                                        <input type="hidden" id="classeid"  name="classeid" value="<?php echo $tabinfos[4];?>">
                                        <input type="hidden" id="etape"  name="etape" value="3">

                                         </div>
                                  </div>


            <div class="form-actions">
                                  <div class="row">
                                      <div class="offset-md-3 col-md-9">
                                          <button type="button" onclick="modify()" class="btn btn-info">Modifier</button>
                                          <button type="button" class="btn btn-danger"><?php echo L::AnnulerBtn ?></button>
                                      </div>
                                    </div>
                                 </div>
          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
					<!-- end widget -->




<!-- element à faire apparaitre au clique du bouton rechercher -->


<!-- fin affichage du bouton rechercher -->

                     <!-- start new patient list -->

                    <!-- end new patient list -->

                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy;
            <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
            </div>
            <div class="scroll-to-top">
                <i class="material-icons">eject</i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
      <script src="../assets2/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <script src="../assets2/plugins/select2/js/select2.js" ></script>
     <script src="../assets2/js/pages/select2/select2-init.js" ></script>
     <script src="../assets2/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
   	<script src="../assets2/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- data tables -->
   <script src="../assets2/plugins/datatables/jquery.dataTables.min.js" ></script>
 <script src="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
   <script src="../assets2/js/pages/table/table_data.js" ></script>

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>
  <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>

   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

function modify()
{
  Swal.fire({
title: '<?php echo L::WarningLib ?>',
text: "Voulez vous vraiment modifier cette note",
type: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: '<?php echo L::ModifierBtn ?>',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
$("#FormUpdateNote").submit();
}else {

}
})
}


<?php
  if(isset($_POST))
  {
    ?>
    function touspresent(idclasse)
    {

       var nb="<?php echo @$nb;?>";
       var matricules="<?php echo @$matricules?>";

       var array=matricules.split('*');

       for(i=1;i<nb;i++)
       {
         //alert(array[i]);
         $("statut"+array[i]).val(1);
         //$("P"+array[i]).prop("checked", true);
         document.getElementById("P"+array[i]).checked = true;

       }
       $("#allpresent").val(1);

    }

    function tousabsent(idclasse)
    {
      var nb="<?php echo @$nb;?>";
      var matricules="<?php echo @$matricules?>";

      var array=matricules.split('*');

      for(i=1;i<nb;i++)
      {
        $("statut"+array[i]).val(0);
        document.getElementById("A"+array[i]).checked = true;

      }
      $("#allpresent").val(0);
    }
<?php
  }
 ?>


   function present(id)
   {
    $("statut"+id).val(1);
   }

   function absent(id)
   {
    $("statut"+id).val(0);
   }

   $('#example5').DataTable( {
       "scrollX": true

   } );
   $("#classeEtab").select2();
   $("#notetype").select2();
   $("#libctrl").select2();
   $('#datepre').bootstrapMaterialDatePicker
   ({
     date: true,
     time: false,
     format: 'DD/MM/YYYY',
     lang: 'fr',
     minDate : new Date(),
    cancelText: '<?php echo L::AnnulerBtn ?>',
    okText: '<?php echo L::Okay ?>',
    clearText: '<?php echo L::Eraser ?>',
    nowText: '<?php echo L::Now ?>'

   });

function searchNotesClasses()
{
  var notetype=$("#notetype").val();
  var classeEtab=$("#classeEtab").val();
  var codeEtab="<?php echo $codeEtabAssigner;  ?>";

  //nous allons rechercher la liste des designation de notes par classe

    if(notetype==""||classeEtab=="")
    {
      if(notetype=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "Merci de selectionner le type de note",

      })
      }

      if(classeEtab=="")
      {
        Swal.fire({
        type: 'warning',
        title: '<?php echo L::WarningLib ?>',
        text: "Merci de selectionner une classe",

      })
      }
    }else {

        if(notetype==1)
        {
          var etape=1;
        }else if(notetype==2){
          var etape=2;
        }

      $.ajax({

        url: '../ajax/notesSearch.php',
        type: 'POST',
        async:true,
         data: 'notetype=' + notetype+ '&etape=' + etape+'&classeEtab='+classeEtab+'&codeEtab='+codeEtab,
         dataType: 'text',
         success: function (content, statut) {

           $("#libctrl").html("");
           $("#libctrl").html(content);

         }

      });
    }



}

   $(document).ready(function() {

//
$("#FormUpdateNote").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
 },
 highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
 },
 success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required",
    notetype:"required",
    classeEtab:"required",
    libctrl:"required",
    note:"required",
    observation:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence",
    notetype:"Merci de selectionner le type de note",
    classeEtab:"<?php echo L::PleaseSelectclasseOnerequired ?>",
    libctrl:"Merci de selectionner une désignation",
    note:"Merci de renseigner la note",
    observation:"Merci de renseigner une observation"

  },
  submitHandler: function(form) {
    form.submit();
  }
});


$("#FormAttendance").validate({
  errorPlacement: function(label, element) {
  label.addClass('mt-2 text-danger');
  label.insertAfter(element);
  },
  highlight: function(element, errorClass) {
  $(element).parent().addClass('has-danger')
  $(element).addClass('form-control-danger')
  },
  success: function (e) {
      $(e).closest('.control-group').removeClass('error').addClass('info');
      $(e).remove();
  },
  rules:{


    classeEtab:"required",
    datepre:"required"



  },
  messages: {
    classeEtab:"<?php echo L::PleaseSelectclasserequired ?>",
    datepre:"Merci de choisir la date de présence"

  },
  submitHandler: function(form) {
    //form.submit();
    //classeId
//datePresence
    var etape=1;


    $.ajax({

      url: '../ajax/attendance.php',
      type: 'POST',
      async:false,
      data: 'classe=' + $("#classeId").val()+'&datepre='+$("#datePresence").val()+'&etape='+etape,
      dataType: 'text',
      success: function (content, statut)
      {
          if(content==0)
          {
            form.submit();

          }else if(content==1) {
            Swal.fire({
            type: 'warning',
            title: '<?php echo L::WarningLib ?>',
            text: "La présence de cette classe existe dejà dans le système pour cette date",

          })
        }
      }

    });
  }


});


   });

   </script>
    <!-- end js include path -->
  </body>

</html>
