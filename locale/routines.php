<?php
session_start();
require_once('../class/User.php');
require_once('../class/Etablissement.php');
require_once('../class/LocalAdmin.php');
require_once('../class/Parent.php');
require_once('../class/Classe.php');
require_once('../controller/functions.php');
require_once('../class/Sessionsacade.php');

$session= new Sessionacade();
$userId=$_SESSION['user']['IdCompte'];
$emailUti=$_SESSION['user']['email'];
$classe=new Classe();
$user=new User();
$etabs=new Etab();
$localadmins= new Localadmin();
$parents=new ParentX();
$compteuserid=$_SESSION['user']['IdCompte'];
$imageprofile=$user->getImageProfilebyId($compteuserid);
$logindata=$user->getLoginProfilebyId($compteuserid);
$tablogin=explode("*",$logindata);
$datastat=$user->getStatis();
$tabstat=explode("*",$datastat);
if(strlen($imageprofile)>0)
{
  $lienphoto="../photo/".$emailUti."/".$imageprofile;
}else {
  $lienphoto="../photo/user5.jpg";
}



$alletab=$etabs->getAllEtab();
$locals=$localadmins->getAllAdminLocal();
$allparents=$parents->getAllParent();
$classes=$classe->getAllclassesOfassignated($_SESSION['user']['IdCompte']);

$codeEtabsession=$etabs->getcodeEtabByLocalId($userId);

$nbsessionOn=$session->getNumberSessionEncoursOn($codeEtabsession);
$etablissementType=$etabs->DetermineTypeEtab($codeEtabsession);


if($nbsessionOn>0)
{
  //recuperer la session en cours
  $sessionencours=$session->getSessionEncours($codeEtabsession);
  $tabsessionencours=explode("*",$sessionencours);
  $libellesessionencours=$tabsessionencours[0];
  $sessionencoursid=$tabsessionencours[1];
  $typesessionencours=$tabsessionencours[2];
  $nbcloturetypese=$classe->getNumberOfCloturetypeSchhol($codeEtabsession,$libellesessionencours);
  $nbcalculmoytypese=$classe->getNumberOfCalculmoySchool($codeEtabsession,$libellesessionencours);
}

//la liste des heures de cours

$allLibellesHours=$etabs->getHoursAllLibs($codeEtabsession,$libellesessionencours);

 ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title><?php echo L::Titlesite ?></title>
    <meta name="Description" content="Xschool est l'application de communication pour les écoles, les enseignants et les parents, la plus flexible et facile d'utilisation.Elle à été conçue pour répondre à des problèmes que nous observons et pour suivre de près l'évolution de nos enfants, alors commençons maintenant.">
    <meta name="Keywords" content="Application du domaine éducatif,Application de communication parents établissment , Application android et Desktop pour Etablissement">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="../assets2/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!--bootstrap -->
   <link href="../assets2/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
	<link href="../assets2/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- data tables -->
   <link href="../assets2/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
	<link href="../assets2/plugins/material/material.min.css" rel="stylesheet" >
	<link href="../assets2/css/material_style.css" rel="stylesheet">
	<!-- morris chart -->
    <!--link href="../assets2/plugins/morris/morris.css" rel="stylesheet" type="text/css" /-->

	<!-- Theme Styles -->
    <link href="../assets2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets2/css/theme-color.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>

	<!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo/logo1.png" />
    <style media="screen">
    .navbar-custom {
  background: #71d40f;
  float: left;
  width: 100%;
}


//theme color css

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
  color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li.mega-menu-dropdown>.dropdown-menu .mega-menu-content .mega-menu-submenu li>h3, .header-white .page-header.navbar .hor-menu .navbar-nav>li>a {
    color: #f8f9fa;
}

.header-white .page-header.navbar .hor-menu .navbar-nav>li>a>i {
    color: #f8f9fa;
}
    </style>
 </head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
        <?php
include("menu1.php")
         ?>
        <!-- end header -->
        <!-- start page container -->
      <?php
      include('submenu.php');
       ?>
        <!-- end sidebar menu -->
			<!-- start page content -->
        <div class="page-content-wrapper">
                <div class="page-content">
                  <div class="page-bar">
                      <div class="page-title-breadcrumb">
                          <div class=" pull-left">
                              <div class="page-title">Emploi du temps</div>
                          </div>
                          <ol class="breadcrumb page-breadcrumb pull-right">
                              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.php"><?php echo L::Homestartindex ?></a>&nbsp;<i class="fa fa-angle-right"></i>
                              </li>
                              <li class="active">Emploi du temps</li>
                          </ol>
                      </div>
                  </div>

					<!-- start widget -->
          <?php

                if(isset($_SESSION['user']['addprogra']))
                {

                  ?>
                  <!--div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php
                //echo $_SESSION['user']['addetabok'];
                ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                   </a>
                </div-->
          <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
          <script src="../assets/js/sweetalert2.min.js"></script>

            <script>
            Swal.fire({
            type: 'success',
            title: 'Félicitations',
            text: '<?php echo $_SESSION['user']['addprogra'] ?>',

            })
            </script>
                  <?php
                  unset($_SESSION['user']['addprogra']);
                }

                 ?>
<!-- start widget -->
<?php

      if(isset($_SESSION['user']['updateroutineok']))
      {

        ?>
        <!--div class="alert alert-success alert-dismissible fade show" role="alert">
      <?php
      //echo $_SESSION['user']['addetabok'];
      ?>
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
         </a>
      </div-->
<link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
<script src="../assets/js/sweetalert2.min.js"></script>

  <script>
  Swal.fire({
title: '<?php echo L::Felicitations ?>',
text: "<?php echo $_SESSION['user']['updateroutineok']; ?>",
type: 'success',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Ajouter une nouveau cours',
cancelButtonText: '<?php echo L::AnnulerBtn ?>',
}).then((result) => {
if (result.value) {
document.location.href="routinesadd.php";
}else {

}
})
  </script>
        <?php
        unset($_SESSION['user']['updateroutineok']);
      }

       ?>
<!-- end widget -->
<?php

      if(isset($_SESSION['user']['addetabexist']))
      {

        ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <?php
      echo $_SESSION['user']['addetabexist'];
      ?>
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
         </a>
      </div>



        <?php
        unset($_SESSION['user']['addetabexist']);
      }

       ?>
       <div class="pull-right">
         <a class="btn btn-primary " href="routinesadd.php"><i class="fa fa-plus-circle"> Ajouter Cours</i></a>
          <a class="btn btn-warning" target="_blank" href="scheduletabclasse.php?classe=<?php echo $_GET['classe']; ?>&codeEtab=<?php echo $codeEtabsession; ?>&session=<?php echo $libellesessionencours; ?>" ><i class="fa fa-print"> Emploi du temps</i></a>
         <!-- <a class="btn btn-warning " href="#" onclick="generetedroutines(<?php //echo $_GET['classe']; ?>,'<?php //echo $codeEtabsession; ?>','<?php //echo $libellesessionencours; ?>')"><i class="fa fa-print"> Emploi du temps</i></a> -->
         <a class="btn btn-success " href="#" data-toggle="modal" data-target="#smallModel" ><i class="fa fa-send"> Envoyer</i></a>

         <div class="modal fade" id="smallModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="color: Dodgerblue;"> <i class="fa fa-send "></i> par mail </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label class="control-label col-md-4">Destinataire
                        <span class="required">*  </span>
                    </label>
                    <div class="col-md-8">
                      <select class="form-control" id="desti" name="desti" style="width:100%" onchange="searchmail()">
                          <option value="">Selectionner un Destinataire</option>
                          <option value="1">Admin</option>
                          <option value="2">Parent</option>
                          <option value="3">Enseignant</option>


                      </select>
                      </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-4">Nom & prénoms
                        <span class="required">*  </span>
                    </label>
                    <div class="col-md-8">
                      <select class="form-control" id="destimail" name="destimail" style="width:100%" onchange="selectmail()">
                          <option value="">Selectionner un destinataire</option>
                      </select>
                      </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-4">Email
                        <span class="required">*  </span>
                    </label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" id="mail" name="mail"  style="width:100%" readonly>

                      </div>
                </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="sendbutton" disabled onclick="send()">Envoyer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo L::Closebtn  ?></button>

            </div>
        </div>
    </div>
</div>
       </div>

<br/><br/>

<div class="row">

  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="card card-topline-green">
                                          <div class="card-head">
                                              <header>Emploi du temps - Classe</header>
                                              <div class="tools">
                                                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                         <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                         <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                              </div>
                                          </div>
                                          <div class="card-body ">
                              <div class="table-responsive">
                                  <table class="table table-striped custom-table table-hover">
                                      <thead>

                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Horaires</td>
                                          <?php
                                            $dataday=$etabs->getAllweeks();
                                            $tabLibday=array();
                                            $i=0;
                                              foreach ($dataday as $value):
                                                $tabLibday[$i]=$value->short_days;
                                                ?>
                                                <td>
                                                  <?php echo $value->libelle_days;?>
                                                </td>
                                                <?php
                                                $i++;
                                              endforeach;
                                           ?>
                                        </tr>
                                        <?php
                                        // var_dump($tabLibday);
                                        $nblibday=count($tabLibday);
                                        // echo $nblibday;
                                          foreach ($allLibellesHours as $value1):
                                            $idheure=$value1->id_heure;
                                            ?>
                                            <tr>
                                              <td><?php echo returnHours($value1->heuredeb_heure). " - ".returnHours($value1->heurefin_heure) ?></td>
                                              <?php
                                                for($j=0;$j<$nblibday;$j++)
                                                {
                                                  ?>
                                                  <td>
                                                    <?php
                                                      $nbmatiereday=$etabs->getnumberofmatierethisday($idheure,$tabLibday[$j],$codeEtabsession,$libellesessionencours,$_GET['classe']);
                                                      // echo $nbmatiereday;
                                                      if($nbmatiereday>0)
                                                      {
                                                        $datas=$etabs->getMatierethisday($idheure,$tabLibday[$j],$codeEtabsession,$libellesessionencours,$_GET['classe']);
                                                        $tabdatas=explode("*",$datas);
                                                        ?>
                                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">


                        <div class="btn-group" role="group">
                          <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $tabdatas[0];?>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="#" onclick="modify(<?php echo $tabdatas[1] ?>)"><i class="fa fa-pencil"></i> Modifier</a>
                            <a class="dropdown-item" href="#" onclick="deleted(<?php echo $tabdatas[1] ?>,<?php echo $_GET['classe'] ?>)"><i class="fa fa-trash-o"></i> Supprimer</a>

                          </div>
                        </div>
                      </div>
                      <link rel="stylesheet" href="../assets/css/sweetalert2.min.css"/>
                      <script type="text/javascript" src="../assets/js/sweetalert2.min.js"></script>
                      <script>

                      function myFunction(idcompte)
                      {
                        //var url="detailslocal.php?compte="+idcompte;
                      document.location.href="detailsadmin.php?compte="+idcompte;
                      }

                      function modify(id)
                      {
                        var classeid="<?php echo $_GET['classe']; ?>";

                        Swal.fire({
                      title: '<?php echo L::WarningLib ?>',
                      text: "Voulez vous vraiment modifier ce cours",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: '<?php echo L::ModifierBtn ?>',
                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                      }).then((result) => {
                      if (result.value) {
                      document.location.href="updateroutine.php?compte="+id+"&classeid="+classeid;
                      }else {

                      }
                      })
                      }

                      function deleted(id,classeid)
{
var codeEtab="<?php echo $codeEtabsession; ?>";
var sessionEtab="<?php echo $libellesessionencours; ?>";

                        Swal.fire({
                      title: '<?php echo L::WarningLib ?>',
                      text: "Voulez vous vraiment supprimer ce cours",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: '<?php echo L::DeleteLib ?>',
                      cancelButtonText: '<?php echo L::AnnulerBtn ?>',
                      }).then((result) => {
                      if (result.value) {

                      document.location.href="../controller/routine.php?etape=5&compte="+id+"&classe="+classeid+"&codeEtab="+codeEtab+"&sessionEtab="+sessionEtab;
                      }else {

                      }
                      })
                      }

                      </script>
                                                        <?php
                                                      }
                                                     ?>
                                                  </td>
                                                  <?php
                                                }
                                               ?>
                                            </tr>
                                            <?php
                                          endforeach;
                                         ?>
                                    </tbody>
                                  </table>
                                  </div>

                              </div>
                                      </div>
</div>


           <!-- start new patient list -->

          <!-- end new patient list -->

      </div>
					<!-- end widget -->
					<!-- chart start -->



                </div>
            </div>
        <!-- end page content -->
        <!-- start chat sidebar -->

        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2019 &copy;
        <a href="#" target="_blank" class="makerCss">PROXIMITY SA</a>
        </div>
        <div class="scroll-to-top">
            <i class="material-icons">eject</i>
        </div>
    </div>
    <!-- end footer -->
    <!-- start js include path -->
    <script src="../assets2/plugins/jquery/jquery.min.js" ></script>
 	<script src="../assets2/plugins/popper/popper.min.js" ></script>
     <script src="../assets2/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
 	<script src="../assets2/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
     <script src="../assets2/plugins/jquery-ui/jquery-ui.min.js" ></script>
     <!-- bootstrap -->
     <script src="../assets2/plugins/bootstrap/js/bootstrap.min.js" ></script>
     <!-- morris chart -->

     <!-- calendar -->
     <script src="../assets2/plugins/moment/moment.min.js" ></script>

     <script src="../assets2/plugins/chart-js/Chart.bundle.js" ></script>
     <script src="../assets2/plugins/chart-js/utils.js" ></script>
     <!-- <script src="../assets2/js/pages/chart/chartjs/chartjs-data.js" ></script> -->
     <script src="../assets2/plugins/fullcalendar/fullcalendar.min.js" ></script>
     <script src="../assets2/js/pages/calendar/calendar.min.js" ></script>
     <!-- Common js-->
 	<script src="../assets2/js/app.js" ></script>
     <script src="../assets2/js/layout.js" ></script>
 	<script src="../assets2/js/theme-color.js" ></script>
 	<!-- Material -->
 	<script src="../assets2/plugins/material/material.min.js"></script>




    <!-- morris chart -->
    <!--script src="../assets2/plugins/morris/morris.min.js" ></script>
    <script src="../assets2/plugins/morris/raphael-min.js" ></script>
    <script src="../assets2/js/pages/chart/morris/morris-home-data.js" ></script-->

   <script>
   function addFrench()
   {
     var etape=1;
     var lang="fr";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }

   function addEnglish()
   {
     var etape=1;
     var lang="en";
     $.ajax({
       url: '../ajax/langue.php',
       type: 'POST',
       async:false,
       data: 'etape=' + etape+ '&lang=' +lang,
       dataType: 'text',
       success: function (content, statut) {

   window.location.reload();

       }
     });
   }
   function loader()
   {
     var etape=1;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";

     $.ajax({
       url: '../ajax/load.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session,
       dataType: 'text',
       success: function (content, statut) {

         var data=content;

         // callback(data);

       }
     });

   }

   $(document).ready(function() {

     // alert(session);



     var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var config = {
            type: 'line',
            data: {
                labels: [<?php  echo $etabs->getMonthRevenu($codeEtabAssigner,$libellesessionencours); ?>],
                datasets: [{
                    label: "Paiements Attendus",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                      <?php echo $etabs->getMonthRevenustandby($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                    fill: false,
                }, {
                    label: "Paiements reçus",
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                    <?php echo $etabs->getMonthRevenureceive($codeEtabAssigner,$libellesessionencours); ?>
                    ],
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mois'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }]
                }
            }
        };
        var ctx = document.getElementById("chartjs_line").getContext("2d");
        window.myLine = new Chart(ctx, config);

     // var randomScalingFactor = function() {
     //       return Math.round(Math.random() * 100);
     //   };

       var config = {
           type: 'doughnut',
           data: {
               datasets: [{
                   data: [
                       <?php echo $etabs->getnumberOfwomenchild($codeEtabAssigner,$libellesessionencours); ?>,
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       // randomScalingFactor(),
                       <?php echo $etabs->getnumberOfmenchild($codeEtabAssigner,$libellesessionencours); ?>,
                   ],
                   backgroundColor: [
                       window.chartColors.red,
                       // window.chartColors.orange,
                       // window.chartColors.yellow,
                       // window.chartColors.green,
                       window.chartColors.blue,
                   ],
                   label: 'Dataset 1'
               }],
               labels: [
                   "Filles",
                   // "Orange",
                   // "Yellow",
                   // "Green",
                   "Garçons"
               ]
           },
           options: {
               responsive: true,
               legend: {
                   position: 'top',
               },
               title: {
                   display: true,
                   text: ''
               },
               animation: {
                   animateScale: true,
                   animateRotate: true
               }
           }
       };

           var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
           window.myDoughnut = new Chart(ctx, config);


     var calendar = $('#calendar1').fullCalendar({
       monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
       monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun','Jul','Aot','Sep','Oct','Nov','Dec'],
       dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
       dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
       timeFormat:'H(:mm)',
       timeZone: 'UTC',
       allDayText:'tous les jours',
       // minTime: "08:00",
       // maxTime: "20:00",
       buttonText:{
  today:    'AUJOURDHUI',
  month:    'MOIS',
  week:     'SEMAINE',
  day:      'JOUR',
  list:     'list'
},
    locale: 'fr',
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events:'../ajax/load.php?view=1',
   // events:loader(),
    // events: 'load.php?codeEtab='+codeEtab+'&session='+session+'&etape='+etape,
    selectable:false,
    selectHelper:false,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:false,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
   },
   eventClick:function(event)
   {
     var id = event.id;
     var etape=7;
     var codeEtab="<?php echo $codeEtabAssigner;?>";
     var session="<?php echo $libellesessionencours ; ?>";
     // alert(id);
     $.ajax({
       url: '../ajax/login.php',
       type: 'POST',
       async:false,
       data: 'codeEtab=' +codeEtab+ '&etape='+etape+'&session='+session+'&parascolaire='+id,
       dataType: 'text',
       success: function (content, statut) {

         // var data=content;
         var cut = $.trim(content);
         $tabcontent=cut.split("*");

         $("#oldparentname").val($tabcontent[0]);
         $("#oldparentprename").val($tabcontent[1]+" "+$tabcontent[2]);
         $("#oldparentsexe").val($tabcontent[3]+" "+$tabcontent[4]);
         $("#oldparentphone").val($tabcontent[5]);
         $("#descri").html($tabcontent[10]);

         var gratuit=$tabcontent[7];
         var montant=$tabcontent[8];

         if(gratuit==0)
         {
           $("#rowgratuit").show();
           $("#rowpayant").hide();

         }else {
           $("#rowpayant").show();
           $("#rowgratuit").hide();
           $("#montpayant").html(montant+" FCFA");
         }

         // alert($tabcontent[11]);

         // callback(data);

          $("#parentOld").click();

       }
     });

   },

    // ,
    //
    // eventClick:function(event)
    // {
    //  if(confirm("Are you sure you want to remove it?"))
    //  {
    //   var id = event.id;
    //   $.ajax({
    //    url:"delete.php",
    //    type:"POST",
    //    data:{id:id},
    //    success:function()
    //    {
    //     calendar.fullCalendar('refetchEvents');
    //     alert("Event Removed");
    //    }
    //   })
    //  }
    // },

   });

   });

   </script>
     <!-- end js include path -->
</body>


</html>
